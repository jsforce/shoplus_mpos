package com.teampls.shoplus.silverlib.database;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseUserDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.silverlib.R;

/**
 * Created by Medivh on 2017-04-25.
 */

public class UserDBAdapter extends BaseUserDBAdapter {
    public static final int ItemCartView = 0;

    public UserDBAdapter(Context context) {
        super(context);
    }

    @Override
    public BaseViewHolder getViewHolder() {
        switch (viewType) {
            default:
                return new DefaultViewHolder();
            case ItemCartView:
                return new ItemCartViewHolder();
        }
    }

    class ItemCartViewHolder extends BaseViewHolder {
        private TextView tvName, tvPhone;
        private CheckBox checked;

        @Override
        public int getThisView() {
            return R.layout.row_counterpart_checkable;
        }

        @Override
        public void init(View view) {
            tvName = (TextView) view.findViewById(R.id.row_counterpart_name);
            tvPhone = (TextView) view.findViewById(R.id.row_counterpart_phone);
            checked = (CheckBox) view.findViewById(R.id.row_counterpart_checked);
        }

        @Override
        public void update(int position) {
            UserRecord record = getRecord(position);
            BaseUtils.setText(tvName, record.getName());
            BaseUtils.setText(tvPhone, BaseUtils.toPhoneFormat(record.phoneNumber));
            checked.setChecked(clickedPositions.contains(position));
            if (retailUserEnabled) {
                if (retailPhoneNumbers.contains(record.phoneNumber)) {
                    record.priceType = ContactBuyerType.RETAIL;
                    tvName.setText(record.getNameWithRetail());
                    tvName.setTextColor(ColorType.RetailColor.colorInt);
                } else {
                    tvName.setTextColor(ColorType.Black.colorInt);
                    tvPhone.setTextColor(ColorType.Black.colorInt);
                }
            }
        }
    }
}
