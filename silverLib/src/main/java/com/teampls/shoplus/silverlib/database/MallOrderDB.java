package com.teampls.shoplus.silverlib.database;

import android.content.Context;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.SlipItemDB;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotOrderData;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderState;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotOrderDataProtocol;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2019-02-09.
 */

public class MallOrderDB extends com.teampls.shoplus.lib.database_memory.BaseMapDB<String, ChatbotOrderData> {
    private static MallOrderDB instance;
    public MallOrderLocalItemDB localItems;

    public static MallOrderDB getInstance(Context context) {
        if (instance == null)
            instance = new MallOrderDB(context);
        return instance;
    }

    private MallOrderDB(Context context) {
        super(context, "MallOrderDB");
        localItems = MallOrderLocalItemDB.getInstance(context);
    }


    @Override
    public String getKeyValue(ChatbotOrderData record) {
        return record.getOrderId();
    }


    public void set(List<ChatbotOrderDataProtocol> protocols) {
        clear();
        List<String> validOrderIds = new ArrayList<>();
        for (ChatbotOrderDataProtocol protocol : protocols) {
            insert(new ChatbotOrderData(protocol));
            validOrderIds.add(protocol.getOrderId());
        }
        localItems.deleteInvalids(validOrderIds);
    }

    @Override
    protected ChatbotOrderData getEmptyRecord() {
        return new ChatbotOrderData();
    }

    public MyMap<String, ChatbotOrderData> getMap() {
        MyMap<String, ChatbotOrderData> results = new MyMap<>(new ChatbotOrderData());
        for (ChatbotOrderData record : getRecords())
            results.put(record.getOrderId(), record);
        return results;
    }

    public List<SlipItemRecord> getItems(String orderId) {
        return getRecordByKey(orderId).getEditingItems(localItems.getRecordsBy(orderId));
    }

    public int getNextSerial(String orderId) {
        List<Integer> serials = new ArrayList<>();
        for (SlipItemRecord record : getItems(orderId))
            serials.add(record.serial);
        return BaseUtils.getMax(serials) + 1;
    }

    public void addItem(String orderId, SlipItemRecord record) {
        List<SlipItemRecord> records = getItems(orderId);
        records.add(record);
        setLocalItems(orderId, records);
    }

    public void deleteItem(String orderId, List<SlipItemRecord> records, int position) {
        List<SlipItemRecord> results = new ArrayList<>();
        int i = 0;
        for (SlipItemRecord record : records) {
            if (i != position) // equal 비교는 문제가 생긴다 (수동끼리는 itemId, color, size, slipkey가 모두 동일)
                results.add(record);
            i++;
        }
        setLocalItems(orderId, results);
    }

    public void deleteItems(String orderId) {
        setLocalItems(orderId, new ArrayList<SlipItemRecord>());
    }

    public void updateItem(String orderId, List<SlipItemRecord> records,  int position, SlipItemRecord targetRecord) {
        List<SlipItemRecord> results = new ArrayList<>();
        int i = 0;
        for (SlipItemRecord record : records) {
            if (i == position)
                results.add(targetRecord);
            else
                results.add(record);
            i++;
        }
        setLocalItems(orderId, results);
    }

    public void updateItems(String orderId, List<SlipItemRecord> targetRecords) {
        setLocalItems(orderId, targetRecords);
    }

    private void setLocalItems(String orderId, List<SlipItemRecord> records) {
        localItems.deleteAll(SlipItemDB.Column.slipKey, orderId); // 모두 지우고 새로
        int serial = 0;
        for (SlipItemRecord record : records) {
            record.serial = serial;
            localItems.insert(record);
            serial++;
        }
    }

    public List<ChatbotOrderData> getRequested() {
        List<ChatbotOrderData> result = new ArrayList<>();
        for (ChatbotOrderData record : getRecords()) {
            if (record.getState() == ChatbotOrderState.REQUESTED)
                result.add(record);
        }
        return result;
    }

}
