package com.teampls.shoplus.silverlib.view_silver;

import android.os.Bundle;
import android.view.View;

import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseItemFullViewPager;
import com.teampls.shoplus.silverlib.common.MyServiceTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-03-27.
 */

abstract public class SilverItemFullViewPager extends BaseItemFullViewPager {
    protected MyOnCancelClick onCancelClick;
    protected MyOnFunctionClick onFunctionClick; // save options
    protected static MyOnTask onTask;
    protected static boolean doEnableCart = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        doFinishForResult();
    }

    @Override
    public void onFinish() {
        if (itemDB.options.hasOnRevision(getVisitedItemIds())) {
            new MyAlertDialog(context, "수정 작업 중", "수정 중인 작업이 있습니다. 저장 하시겠습니까?") {
                @Override
                public void yes() {
                    saveItemOptions(true);
                }
                public void no() {
                    itemDB.options.clearOnRevisions(getVisitedItemIds());
                    doFinishForResult();
                    if (onTask != null)
                        onTask.onTaskDone("");
                }
            };
        } else {
            SilverMainViewPager.adapter.refreshFragments();
            doFinishForResult();
            if (onTask != null)
                onTask.onTaskDone("");
        }
    }

    public class MyOnCancelClick implements MyOnClick {
        @Override
        public void onMyClick(View view, Object record) {
            onFinish();
        }
    }

    public List<Integer> getVisitedItemIds() {
        List<Integer> results = new ArrayList<>();
        for (Integer position : visitedPositions)
            results.add(viewAdapter.getRecord(position).itemId);
        return results;
    }

    public class MyOnFunctionClick implements MyOnClick {

        @Override
        public void onMyClick(View view, Object record) {
            saveItemOptions(false);
        }
    }

    protected void saveItemOptions(final boolean doFinish) {
        if (itemDB.options.hasOnRevision(getVisitedItemIds()) == false) {
            MyUI.toastSHORT(context, String.format("수정된 내용이 없습니다"));
            return;
        }

        new MyServiceTask(context, "saveItemOptions") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<Integer> visitedItemIds = getVisitedItemIds();
                itemService.updateItemStock(userSettingData.getUserShopInfo(), itemDB.options.getOnRevisionBundle(visitedItemIds));
                itemDB.options.confirmRevisions(visitedItemIds);
            }

            @Override
            public void onPostExecutionUI() {
                if (doFinish) {
                    onFinish();
                } else {
                    MyUI.toastSHORT(context, String.format("저장 및 재정렬 완료"));
                    for (BaseViewHolder viewHolder : viewAdapter.getFullViews()) {
                        viewHolder.refresh();
                        viewHolder.refreshAdapter();
                    }
                }
            }
        };
    }

}
