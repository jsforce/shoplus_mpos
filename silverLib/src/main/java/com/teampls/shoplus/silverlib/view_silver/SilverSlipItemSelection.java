package com.teampls.shoplus.silverlib.view_silver;

import android.content.Context;

import com.teampls.shoplus.lib.adapter.BaseItemOptionDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.silverlib.common.MyServiceTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2018-06-24.
 */
// 이름으로 검색과 이미지로 검색 양쪽에서 공통으로 쓰기 위해 추가된 클래스
public class SilverSlipItemSelection {
    private Context context;
    private ItemDB itemDB;
    private UserSettingData userSettingData;

    public SilverSlipItemSelection(Context context, ItemDB itemDB) {
        this.context = context;
        this.itemDB = itemDB;
        this.userSettingData = UserSettingData.getInstance(context);
    }

    public void addDefaultOption(final ItemRecord itemRecord, final MyOnTask onTask) {

        new MyServiceTask(context, "saveItemOptions", "기본옵션추가") {
            ItemOptionRecord result;
            @Override
            public void doInBackground() throws MyServiceFailureException {
                result = new ItemOptionRecord(itemRecord.itemId);
                Map<ItemStockKey, Integer> defaultOptionRecordMap = new HashMap<>();
                defaultOptionRecordMap.put(result.toStockKey(), 0);
                itemService.updateItemStock(userSettingData.getUserShopInfo(), defaultOptionRecordMap);
                itemDB.options.insert(result );
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone(result);
            }
        };
    }

    public void onAddFastClick(ItemRecord selectedItem, final BaseItemOptionDBAdapter optionAdapter, final List<ItemOptionRecord> options, final MyOnTask<List<ItemOptionRecord>> onTask) {
        if (options.size() > 0) {
            if (onTask != null)
                onTask.onTaskDone(options);
        } else {
            // 기본 옵션으로 추가하고 개수는 1로 해서 끝낸다
            if (optionAdapter.getCount() == 0) {
                addDefaultOption(selectedItem, new MyOnTask<ItemOptionRecord>() {
                    @Override
                    public void onTaskDone(ItemOptionRecord result) {
                        result.revStock = 1;
                        options.add(result);
                        if (onTask != null)
                            onTask.onTaskDone(options);
                    }
                });

                // 옵션이 1개면서 클릭하지 않았을때는 자동으로 선택하고 개수를 1로 해준다
            } else if (optionAdapter.getCount() == 1) {
                ItemOptionRecord result = optionAdapter.getRecord(0);
                result.revStock = 1;
                options.add(result);
                if (onTask != null)
                    onTask.onTaskDone(options);
            } else {
                final int hint = 1;
                new UpdateValueDialog(context, "전체 선택", "모든 컬러-사이즈를 아래 개수만큼 추가하시겠습니까?", 0, 1) {
                    @Override
                    public void onApplyClick(String newValue) {
                        if (newValue.isEmpty())
                            newValue = Integer.toString(hint);
                        int revStock = BaseUtils.toInt(newValue);
                        for (ItemOptionRecord record : optionAdapter.getRecords()) {
                            record.revStock = revStock;
                            options.add(record);
                        }
                        if (onTask != null)
                            onTask.onTaskDone(options);
                    }
                };
            }
        }

    }

    public void selectAllOptions(final BaseItemOptionDBAdapter optionAdapter, final MyOnTask onTask) {
        if (optionAdapter.getSortedClickedRecords().size() == 0) {
            final int hint = 1;
            new UpdateValueDialog(context, "전체 선택", "모든 컬러-사이즈를 아래 개수만큼 추가하시겠습니까?", 0, hint) {
                @Override
                public void onApplyClick(String newValue) {
                    if (newValue.isEmpty())
                        newValue = Integer.toString(hint);
                    int revStock = BaseUtils.toInt(newValue);
                    List<ItemOptionRecord> options = new ArrayList<>();
                    for (ItemOptionRecord record : optionAdapter.getRecords()) {
                        record.revStock = revStock;
                        options.add(record);
                    }
                    if (revStock != 0)
                        optionAdapter.setAllClicked();
                    optionAdapter.notifyDataSetChanged();
                    if (onTask != null)
                        onTask.onTaskDone("");
                }
            };
        } else {
            new MyAlertDialog(context, "선택해제", String.format("현재 선택하신 %d개를 모두 해제하시겠습니까?", optionAdapter.getSortedClickedRecords().size())) {
                @Override
                public void yes() {
                    optionAdapter.clearClicked();
                    for (ItemOptionRecord record : optionAdapter.getRecords())
                        record.revStock = 0;
                    optionAdapter.notifyDataSetChanged();
                }
            };
        }

    }

}
