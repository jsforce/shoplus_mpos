package com.teampls.shoplus.silverlib.enums;

/**
 * 결산 보조 데이터 타입
 *
 * @author lucidite
 */
public enum SettlementAuxilityDataType {
    EXPENSE("expense", "현금비용"),
    AMOUNT_ON_HAND("on-hand", "현금시재");

    private String remoteStr;
    private String uiStr;

    SettlementAuxilityDataType(String remoteStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
    }

    public static SettlementAuxilityDataType fromRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty()) {
            return EXPENSE;
        }
        for (SettlementAuxilityDataType type: SettlementAuxilityDataType.values()) {
            if (type.remoteStr.equals(remoteStr)) {
                return type;
            }
        }
        return EXPENSE;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }

    public String toUiStr() {
        return this.uiStr;
    }
}
