package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.dialog.DeleteSafelyDialog;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view_base.BaseItemSelectionView;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.database.ItemDBAdapter;

import java.util.List;

/**
 * Created by Medivh on 2017-08-10.
 */

public class ItemSelectionView extends BaseItemSelectionView {
    private static MyOnClick<List<ItemRecord>> onSendClick;

    public static void startActivity(Context context, MyOnClick<List<ItemRecord>> onSendClick) {
        ItemSelectionView.onSendClick = onSendClick;
        context.startActivity(new Intent(context, ItemSelectionView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        refresh();
    }

    @Override
    protected void setUiComponents() {
        super.setUiComponents();
        setVisibility(View.GONE, R.id.item_selection_filter_container);
    }

    @Override
    protected void setAdaptor() {
        if (adapter == null)
            adapter = new ItemDBAdapter(context);
        adapter.showName().showDate().setSelectable();
        adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_MULTIPLE);
        adapter.setViewType(ItemDBAdapter.ItemSelection);
    }

    @Override
    public void onApplyClick(final View view) {
        new DeleteSafelyDialog(context, "선택 아이템 삭제") {
            @Override
            public void onDeleteClick() {
                if (onSendClick != null)
                    onSendClick.onMyClick(view, adapter.getSortedClickedRecords());
                finish();
            }
        };
    }
}