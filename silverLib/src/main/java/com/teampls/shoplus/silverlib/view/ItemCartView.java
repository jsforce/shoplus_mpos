package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnClickTriple;
import com.teampls.shoplus.lib.view.MyAllUsersView;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.composition.UserComposition;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.database.UserDBAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-04-25.
 */

public class ItemCartView extends BaseActivity implements AdapterView.OnItemClickListener {
    private ListView lvCounterparts;
    private UserDBAdapter adapter;
    private PosSlipDB posSlipDB;
    private List<UserRecord> slipUsers = new ArrayList<>();
    private MyRadioGroup<SlipItemType> itemTypes;
    private static String title = "";
    private static MyOnClickTriple<UserRecord, SlipItemType, Integer> onCartClick;
    private final String KEY_RECENT_COUNTERPART = "itemCartView.recentCounterpart",
            KEY_RECENT_ITEMTYPE = "itemCartView.slipItemType";
    private MyCurrencyEditText etUnitPrice;
    private static ItemRecord itemRecord;
    private UserComposition userComposition;

    public static void startActivity(Context context, String title, ItemRecord itemRecord, MyOnClickTriple<UserRecord, SlipItemType, Integer> onCartClick) {
        ItemCartView.onCartClick = onCartClick;
        ItemCartView.itemRecord = itemRecord;
        ItemCartView.title = title;
        context.startActivity(new Intent(context, ItemCartView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        userComposition = new UserComposition(context);

        adapter = new UserDBAdapter(context);
        adapter.setViewType(UserDBAdapter.ItemCartView);
        adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_SINGLE);
        adapter.refreshRetailUsers();

        posSlipDB = PosSlipDB.getInstance(context);
        myActionBar.hide();
        findTextViewById(R.id.item_cart_title, String.format("장바구니 : %s", title));

        slipUsers = userComposition.getRecords(posSlipDB.getPhoneNumbers(false));
        adapter.setRecords(slipUsers); // 시간순 sorting?

        etUnitPrice = new MyCurrencyEditText(context, getView(), R.id.item_cart_unitPrice, R.id.item_cart_quantity_clear);
        etUnitPrice.setPlusMinusButton(R.id.item_cart_quantity_plus, R.id.item_cart_quantity_minus);
        etUnitPrice.setTextViewAndBasicUnit(R.id.item_cart_quantity_basicUnit, userSettingData.getBasicUnit());
        etUnitPrice.setOriginalValue(itemRecord.unitPrice);
        lvCounterparts = (ListView) findViewById(R.id.item_cart_counterparts);
        lvCounterparts.setOnItemClickListener(this);
        lvCounterparts.setAdapter(adapter);

        setOnClick(R.id.item_cart_cancel, R.id.item_cart_apply, R.id.item_cart_addCounterpart);

        SlipItemType recentItemType = SlipItemType.valueOf(keyValueDB.getValue(KEY_RECENT_ITEMTYPE, SlipItemType.SALES.toString()));
        itemTypes = new MyRadioGroup<>(context, getView(), 1.0, null);
        itemTypes.add(R.id.item_cart_type_sales, SlipItemType.SALES).add(R.id.item_cart_type_return, SlipItemType.RETURN).add(R.id.item_cart_type_deposit, SlipItemType.DEPOSIT).add(R.id.item_cart_type_withdrawal, SlipItemType.WITHDRAWAL)
                .add(R.id.item_cart_type_advance, SlipItemType.ADVANCE).add(R.id.item_cart_type_advance_clear, SlipItemType.ADVANCE_CLEAR).add(R.id.item_cart_type_advance_withdral, SlipItemType.ADVANCE_SUBTRACTION)
                .add(R.id.item_cart_type_consign, SlipItemType.CONSIGN).add(R.id.item_cart_type_consign_clear, SlipItemType.CONSIGN_CLEAR).add(R.id.item_cart_type_consign_return, SlipItemType.CONSIGN_RETURN)
                .add(R.id.item_cart_type_sample, SlipItemType.SAMPLE).add(R.id.item_cart_type_sample_clear, SlipItemType.SAMPLE_CLEAR).add(R.id.item_cart_type_sample_return, SlipItemType.SAMPLE_RETURN)
                .add(R.id.item_cart_type_preorder, SlipItemType.PREORDER).add(R.id.item_cart_type_preorder_clear, SlipItemType.PREORDER_CLEAR).add(R.id.item_cart_type_preorder_cancel, SlipItemType.PREORDER_WITHDRAW);
        itemTypes.setChecked(recentItemType);

        String recentPhoneNumber = keyValueDB.getValue(KEY_RECENT_COUNTERPART);
        int position = 0;
        for (UserRecord record : adapter.getRecords()) {
            if (record.phoneNumber.equals(recentPhoneNumber)) {
                adapter.setClickedPosition(position);
                adapter.notifyDataSetChanged();
                break;
            }
            position++;
        }

        if (adapter.getRecords().size() == 0) {
            onCounterpartClick();
            MyUI.toastSHORT(context, String.format("주문 중인 고객이 없습니다.  고객 지정이 필요합니다"));
        }

    }

    @Override
    public int getThisView() {
        return R.layout.activity_item_cart;
    }

    @Override
    public void onMyMenuCreate() {
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        adapter.onItemClick(view, position);
        UserRecord userRecord = adapter.getRecord(position);
        if (userSettingData.getProtocol().isRetailOn() && userRecord.priceType == ContactBuyerType.RETAIL) {
            etUnitPrice.setOriginalValue(itemRecord.retailPrice);
            MyUI.toastSHORT(context, String.format("소매가로 변경"));
        } else {
            etUnitPrice.setOriginalValue(itemRecord.unitPrice);
        }
    }

    private void onCounterpartClick() {
        MyAllUsersView.startActivity(context, new MyOnClick<UserRecord>() {
            @Override
            public void onMyClick(View view, UserRecord record) {
                adapter.addRecord(0, record);
                adapter.onItemClick(new View(context), 0);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.item_cart_addCounterpart) {
            onCounterpartClick();
        } else if (view.getId() == R.id.item_cart_cancel) {
            finish();
        } else if (view.getId() == R.id.item_cart_apply) {
            if (adapter.getClickedPositions().size() == 0) {
                MyUI.toastSHORT(context, String.format("거래처를 선택해주세요"));
                return;
            }
            if (itemTypes.hasCheckedItem() == false) {
                MyUI.toastSHORT(context, String.format("거래 종류를 선택해주세요"));
                return;
            }
            UserRecord counterpart = adapter.getRecord(adapter.getClickedPositions().get(0));
            SlipItemType slipItemType = itemTypes.getCheckedItem();
            keyValueDB.put(KEY_RECENT_COUNTERPART, counterpart.phoneNumber);
            keyValueDB.put(KEY_RECENT_ITEMTYPE, slipItemType.toString());
            if (onCartClick != null)
                onCartClick.onMyClick(view, counterpart, slipItemType, etUnitPrice.getValue());
            finish();
        }
    }

}
