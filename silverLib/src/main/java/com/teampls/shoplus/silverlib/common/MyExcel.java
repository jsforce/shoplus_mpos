package com.teampls.shoplus.silverlib.common;

import android.content.Context;

import com.teampls.shoplus.lib.common.BaseExcel;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.silverlib.database.ItemStockRecord;
import com.teampls.shoplus.silverlib.database.StockHistoryDB;

import org.apache.poi.ss.usermodel.Sheet;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-07-19.
 */

public class MyExcel extends BaseExcel {

    public MyExcel(Context context) {
        super(context);
    }

    public String createStockHistory(List<DateTime> dateTimes, List<ItemStockRecord> records, MyMap<ItemOptionRecord, MyMap<DateTime, Integer>> optionDateTimeMap) {
        Sheet sheet = addSheet("재고 기록");

        addFirstRow(sheet, new ExcelCell(workbook, String.format("%s 작성", DateTime.now().toString(BaseUtils.fullFormat))));
        setColumnWidth(sheet, dateTimes.size() + 2, 3); // 번호, 이름, 시간별 재고....

        // header
        List<ExcelCell> cells = new ArrayList<>();
        cells.add(new ExcelCell(workbook, "번호"));
        cells.add(new ExcelCell(workbook, "상품명"));
        for (DateTime dateTime : dateTimes)
            cells.add(new ExcelCell(workbook, String.format("%s %s", dateTime.toString(BaseUtils.MD_Format), dateTime.toString(BaseUtils.Hm_Format))));
        addRow(sheet, cells);
        autoSizeColumnWidth(sheet, cells);

        int num = 0;
        for (ItemStockRecord record : records) {
            cells.clear();
            num++;
            cells.add(new ExcelCell(workbook, Integer.toString(num)));
            cells.add(new ExcelCell(workbook, record.itemName));
            for (DateTime dateTime : dateTimes)
                cells.add(new ExcelCell(workbook, record.stockSums.get(dateTime)));
            addRow(sheet, cells);

            for (ItemOptionRecord optionRecord : StockHistoryDB.getInstance(context).getOptions(record.itemId)) {
                cells.clear();
                cells.add(new ExcelCell(workbook, "  "));
                cells.add(new ExcelCell(workbook, optionRecord.toStringWithDefault(" / ")));

                MyMap<DateTime, Integer> dateStockMap = optionDateTimeMap.get(optionRecord);
                for (DateTime dateTime : dateTimes)
                    cells.add(new ExcelCell(workbook, dateStockMap.get(dateTime)));
                addRow(sheet, cells);
            }
            addRow(sheet, new ArrayList<ExcelCell>());
        }

        return saveAsFile();
    }
}
