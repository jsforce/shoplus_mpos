package com.teampls.shoplus.silverlib.database;

import android.util.Pair;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_memory.MemorySlipItemDB;
import com.teampls.shoplus.lib.enums.WorkingTime;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2019-06-26.
 */

public class SettlementRecord {
    public Pair<DateTime, DateTime> period;
    public int amount = 0, cash = 0, online = 0, entrust = 0;
    private List<SlipRecord> trans = new ArrayList<>();
    public SlipSummaryRecord summaryRecord = new SlipSummaryRecord();

    public void setTransAndSummarize(List<SlipRecord> trans, MemorySlipItemDB transItemDB) {
        this.trans = trans;
        online = 0;
        entrust = 0;
        for (SlipRecord slipRecord : this.trans) {
            online = online + slipRecord.onlinePayment;
            entrust = entrust + slipRecord.entrustPayment;
        }
        summaryRecord = new SlipSummaryRecord(transItemDB.getRecordsBy(trans));
        this.cash = summaryRecord.amount - online - entrust;
        this.amount = summaryRecord.amount;
    }

    public void setPeriod(Pair<DateTime, DateTime> period) {
        this.period = period;
    }

    public Pair<DateTime, DateTime> getPeriod() {
        if (period == null)
            return Pair.create(Empty.dateTime, Empty.dateTime);
        else
            return period;
    }

    public List<SlipRecord> getTrans() {
        return  trans;
    }

    public SettlementRecord() {

    }



}
