package com.teampls.shoplus.silverlib.view_silver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseItemDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseItemHeader;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_memory.MItemDB;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.dialog.PickMonthDialog;
import com.teampls.shoplus.lib.enums.ItemSortingKey;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.SimpleItemDataProtocol;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseItemsActivity;
import com.teampls.shoplus.lib.view_base.MyGridView;
import com.teampls.shoplus.lib.view_module.BaseFloatingButtons;
import com.teampls.shoplus.lib.view_module.MyDateTimeNavigator;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.database.ItemDBAdapter;
import com.teampls.shoplus.silverlib.database.PreviousThumbImageDB;

import org.joda.time.DateTime;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2018-09-04.
 */

abstract public class SilverPreviousItemsView extends BaseItemsActivity implements AdapterView.OnItemClickListener {
    protected ItemHeader itemHeader;
    protected ItemDBAdapter adapter;
    public MyGridView gridView;
    private ItemsFloatingButtons floatingButtons;
    protected MItemDB mItemDB;
    protected MyDateTimeNavigator navigator;
    protected Set<DateTime> downloadedMonth = new HashSet<>();
    protected TextView tvSelectedCount, tvNotice;
    protected MyEmptyGuide myEmptyGuide;
    protected ItemRecord clickedRecord = new ItemRecord();

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    public void doFinishForResult() {
        super.doFinishForResult();
        PreviousThumbImageDB.getInstance(context).clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        mItemDB = MItemDB.getInstance(context);
        myActionBar.hide();
        tvSelectedCount = findTextViewById(R.id.previous_items_selectedCount);
        tvNotice = findTextViewById(R.id.previous_items_notice);

        setAdaptor();
        setDateTimeNavigator();
        setMyEmptyGuide();

        itemHeader = new ItemHeader(this, getView(), adapter);
        itemHeader.setSortingKeys(ItemSortingKey.ItemId,
                ItemSortingKey.Name);
        floatingButtons = new ItemsFloatingButtons(getView());

        gridView = new MyGridView(context, getView(), R.id.previous_items_gridview);
        gridView.setAdapter(adapter);
        gridView.getThis().setOnItemClickListener(this);
        gridView.setOnScroll(new MyOnTask<Boolean>() {
            @Override
            public void onTaskDone(Boolean isFirstItemVisible) {
                if (floatingButtons != null)
                    floatingButtons.ivMoveToPosition0.setVisibility(isFirstItemVisible ? View.GONE : View.VISIBLE);
            }
        });
        setOnClick(R.id.previous_items_close);
    }

    protected void setDateTimeNavigator() {
        DateTime month = BaseUtils.toMonth(ItemDB.getInstance(context).getDownloadedFirstItem().getCreatedDatetime());
        navigator = new MyDateTimeNavigator(context, month, getView(),
                R.id.monthly_header_prev, R.id.monthly_header_next);
        navigator.setAsMonthMode(getView(), R.id.monthly_year, R.id.monthly_month);
        navigator.setOnApplyClick(new MyOnClick<DateTime>() {
            @Override
            public void onMyClick(View view, DateTime record) {
                navigator.setDateTime(record);
                onTargetMonthSelected(record);
            }
        });
        navigator.setLeftButton(R.id.monthly_change_month, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DateTime month = navigator.getDateTime();
                new PickMonthDialog(context, month.getYear(), month.getMonthOfYear(), new MyOnClick<DateTime>() {
                    @Override
                    public void onMyClick(View view, DateTime dateTime) {
                        navigator.setDateTime(dateTime);
                        onTargetMonthSelected(dateTime);
                    }
                });
            }
        });
        navigator.setRightButton(R.id.monthly_navigation_refresh, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadedMonth.clear();
                mItemDB.clear();
                downloadItems(navigator.getDateTime());
            }
        });
    }

    private void setMyEmptyGuide() {
        myEmptyGuide = new MyEmptyGuide(context, getView(), adapter, findViewById(R.id.previous_items_container),
                R.id.module_empty_container, R.id.module_empty_message, R.id.module_empty_contactUs);
        myEmptyGuide.setMessage("원하시는 월을\n선택해주세요");
        myEmptyGuide.setContactUsButton(String.format("%s검색", navigator.getDateTime().toString(BaseUtils.YM_Kor_Format)), new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                downloadItems(navigator.getDateTime());
            }
        });
        myEmptyGuide.refresh();
    }

    protected void setAdaptor() {
        adapter = new ItemDBAdapter(context, mItemDB);
        adapter.showName().hideDate().showPrice().showState(); // header의 setAdapterBySortingKey에 의해 유지가 안됨
        adapter.setSortingKey(MSortingKey.ID);
        adapter.setViewType(ItemDBAdapter.PreviousItemsView);
    }

    @Override
    public int getThisView() {
        return R.layout.activity_previous_items;
    }

    protected void onTargetMonthSelected(DateTime dateTime) {
        if (downloadedMonth.contains(dateTime)) {
            adapter.filterMonths(dateTime);
            refresh();
        } else {
            downloadItems(dateTime);
        }
    }

    private void downloadItems(final DateTime month) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(String.format("%s 상품 다운 중...", month.toString(BaseUtils.YM_Kor_Format)));
        progressDialog.show();

        new MyServiceTask(context, "downloadItems") {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<SimpleItemDataProtocol> protocolList = itemService.getMontlyItemList(userSettingData.getUserShopInfo(), month);
                for (SimpleItemDataProtocol  protocol : protocolList)
                    mItemDB.updateOrInsert(new ItemRecord(protocol, month.plusSeconds(protocol.getItemId())));
                adapter.filterMonths(month);
                downloadedMonth.add(month);
                MyUI.toastSHORT(context, String.format("%d개 발견", protocolList.size()));
            }

            @Override
            public void onPostExecutionUI() {
                progressDialog.dismiss();
                refresh();
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                progressDialog.dismiss();
            }
        };
    }


    @Override
    public void refresh() {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                adapter.generate(false);
            }

            @Override
            public void onPostExecutionUI() {
                adapter.applyGeneratedRecords();
                adapter.notifyDataSetChanged();
                itemHeader.refresh();
                floatingButtons.refresh();
                myEmptyGuide.refresh();
                refreshHeader();
            }
        };
    }

    abstract protected void refreshHeader();

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ItemRecord record = adapter.getRecord(position);
        ItemDB.getInstance(context).searchRecord(context, record.itemId, new MyOnTask<ItemRecord>() {
            @Override
            public void onTaskDone(ItemRecord result) {
                clickedRecord = result;
                startItemFullViewPager(clickedRecord);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BaseGlobal.RequestRefresh:
                // 클릭하는 순간 itemDB에 확실히 저장함
                ItemRecord fileRecord = ItemDB.getInstance(context).getRecordBy(clickedRecord.itemId);
                clickedRecord = fileRecord;
                mItemDB.updateOrInsert(fileRecord); // 설정한대로 view에서도 보이게
                refresh();
                break;
        }
    }

    abstract public void startItemFullViewPager(ItemRecord itemRecord);

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.previous_items_close) {
            doFinishForResult();
        }
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    class ItemHeader extends BaseItemHeader {

        public ItemHeader(BaseItemsActivity activity, View view, BaseItemDBAdapter adapter) {
            super(activity, view, adapter);
            setVisibility(View.GONE, R.id.item_header_name, R.id.item_header_category); //  이름도 생각이 안나서 찾아보는 건데 이름은 제공하지 말자
        }


        @Override
        public void onSortingClicked(ItemSortingKey sortingKey) {
            super.onSortingClicked(sortingKey);
            adapter.hideDate();
        }

        @Override
        public void refreshAdapter() {
            SilverPreviousItemsView.this.refresh();
        }

        @Override
        public void onFilterNameClick(final int position) {
            gridView.getThis().post(new Runnable() {
                @Override
                public void run() {
                    gridView.getThis().smoothScrollToPosition(position);
                }
            });
        }
    }

    class ItemsFloatingButtons extends BaseFloatingButtons {
        protected ImageView ivResetFilter, ivMoveToPosition0;

        public ItemsFloatingButtons(View view) {
            super(view);
            add(R.id.items_reset_filter, R.id.items_to_position0);
            ivResetFilter = (ImageView) view.findViewById(R.id.items_reset_filter);
            ivMoveToPosition0 = (ImageView) view.findViewById(R.id.items_to_position0);
            ivMoveToPosition0.setImageAlpha(150);
            MyView.setImageViewSizeByDeviceSize(context, ivResetFilter, ivMoveToPosition0);
            setVisibility(View.GONE, R.id.items_back, R.id.items_more);
            hide();
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.items_reset_filter) {
                adapter.resetFiltering();
                SilverPreviousItemsView.this.refresh();

            } else if (view.getId() == R.id.items_to_position0) {
                gridView.getThis().post(new Runnable() {
                    @Override
                    public void run() {
                        gridView.getThis().smoothScrollToPosition(0);
                    }
                });
                ivMoveToPosition0.setVisibility(View.GONE);
            }
        }

        public void refresh() {
            if (adapter.isCategoryFiltered() || adapter.isStateTypeFiltered())
                ivResetFilter.setVisibility(View.VISIBLE);
            else
                ivResetFilter.setVisibility(View.GONE);

            if (gridView.getThis().getFirstVisiblePosition() == 0)
                ivMoveToPosition0.setVisibility(View.GONE);
            else
                ivMoveToPosition0.setVisibility(View.VISIBLE);
        }
    }

}
