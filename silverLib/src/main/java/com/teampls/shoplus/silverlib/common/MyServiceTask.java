package com.teampls.shoplus.silverlib.common;

import android.content.Context;

import com.teampls.shoplus.lib.common.BaseServiceTask;
import com.teampls.shoplus.lib.services.chatbot.ProjectChatbotServiceManager;
import com.teampls.shoplus.lib.services.chatbot.ProjectChatbotServiceProtocol;
import com.teampls.shoplus.silverlib.ProjectSilvermoonServiceManager;
import com.teampls.shoplus.silverlib.ProjectSilvermoonServiceProtocol;

/**
 * Created by Medivh on 2017-01-27.
 */

abstract public class MyServiceTask extends BaseServiceTask {
    public ProjectSilvermoonServiceProtocol service;

    public MyServiceTask(Context context) {
        this(context, "");
    }

    public MyServiceTask(Context context, String location) {
        super(context, location);
    }

    public MyServiceTask(Context context, String location, String progressMessage) {
        super(context, location, progressMessage);
    }

    public void setService() {
        service = ProjectSilvermoonServiceManager.defaultService(context);
    };
}
