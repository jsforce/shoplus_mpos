package com.teampls.shoplus.silverlib.view_silver;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.composition.NewItemComposition;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_global.CustomerPriceDB;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MainProgressBar;
import com.teampls.shoplus.lib.viewSetting.ColorSettingView;
import com.teampls.shoplus.lib.viewSetting.ReceiptSettingView;
import com.teampls.shoplus.lib.view_base.BaseMainViewPager;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.database.MallOrderDB;
import com.teampls.shoplus.silverlib.dialog.IntroDialog;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotOrderDataProtocol;
import com.teampls.shoplus.silverlib.view.ItemSelectionView;
import com.teampls.shoplus.silverlib.view.NamecardsView;

import java.util.List;

/**
 * Created by Medivh on 2018-04-12.
 */

abstract public class SilverMainViewPager extends BaseMainViewPager {
    protected ItemDB itemDB;
    protected NewItemCreator newItemCreator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        BaseAppWatcher.onActivityCreate(this);
        loginChecker = new LoginChecker(500);
        tabViewPager.setOffscreenPageLimit(adapter.getCount());
        itemDB = ItemDB.getInstance(context);
        newItemCreator = new NewItemCreator(context, mainProgressBar);
        createAwsTaskScheduler();

        if (BaseAppWatcher.isNetworkConnected()) {
            if (savedInstanceState == null)
                loginChecker.init().start();
        }

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE) && BasePreference.landScapeOrientationEnabled.get(context))
            myActionBar.hideLauncher(); // 탭과 버튼 사이에 낌
    }

    abstract public void createAwsTaskScheduler();

    @Override
    protected void onResume() {
        super.onResume();
        if (BaseAppWatcher.checkAppPermission(context) == false) return;
        onMyMenuCreate();
        if (awsTaskScheduler.finished)
            checkUserSettingData(null);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void doFinish() {
        itemDB.clearDuplications(); // 0번 cell에 의한 중복 삭제
        itemDB.images.finish(); // 전체 개수 기준으로 삭제
        itemDB.thumbImages.finish(); // 전체 개수 기준으로 삭제
        CustomerPriceDB.getInstance(context).finish(); // 최종 입력 6개월 이상된 자료 삭제
        if (loginChecker != null)
            loginChecker.cancel();
        deleteTempFiles(true); // finish
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
    }

    public void onMyMenuCreateDisconnected() {
        MyUI.toastSHORT(context, String.format("기존 상품 정보만 확인할 수 있습니다"));
        myActionBar.setTitle("오프라인 모드");
        myActionBar.setSubTitle("이전 자료");
        myActionBar.clear();
    }

    protected void downloadRecentOrders(final MyOnTask onTask) {
        new MyServiceTask(context, "downloadRecentOrders") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<ChatbotOrderDataProtocol> protocols = chatbotService.getRecentOrders(userSettingData.getUserShopInfo());
                MallOrderDB.getInstance(context).set(protocols);
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    protected void deleteItems() {
        ItemSelectionView.startActivity(context, new MyOnClick<List<ItemRecord>>() {
            @Override
            public void onMyClick(View view, final List<ItemRecord> records) {
                new MyServiceTask(context) {
                    @Override
                    public void doInBackground() throws MyServiceFailureException {
                        mainProgressBar.start("deleteItems", String.format("아이템 삭제 (%d)", records.size()), records.size());
                        for (ItemRecord record : records) {
                            itemService.deleteItem(userSettingData.getUserShopInfo(), record.itemId);
                            itemDB.deleteBy(record.itemId);
                            mainProgressBar.next("deleteItems");
                        }
                    }

                    @Override
                    public void onPostExecutionUI() {
                        MyUI.toastSHORT(context, String.format("상품을 삭제했습니다"));
                        adapter.refreshFragments();
                        mainProgressBar.stop("deleteItems");
                    }

                    @Override
                    public void catchException(MyServiceFailureException e) {
                        super.catchException(e);
                        mainProgressBar.stop("deleteItems");
                    }
                };
            }
        });
    }

    protected class LoginChecker extends BaseLoginChecker {

        public LoginChecker(long interval_ms) {
            super(interval_ms);
        }

        @Override
        public void onTick(long millsLeft) {
            super.onTick(millsLeft);
        }
    }

    public class NewItemCreator extends NewItemComposition {

        public NewItemCreator(Context context, MainProgressBar mainProgressBar) {
            super(context, mainProgressBar);
        }

        @Override
        public void onNoImageCreate(int newItemId) {
            startFullViewPager(newItemId);
        }
    }

    abstract public void startFullViewPager(int newItemId);

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        if (BaseAppWatcher.isLoggedIn() == false) {
            MyUI.toastSHORT(context, String.format("현재 로그인 중입니다. 잠시 후 사용해 주세요"));
            return;
        }
        super.onMyMenuSelected(myMenu);
        switch (myMenu) {
            case gridReceiptSetting:
                ReceiptSettingView.startActivity(context);
                break;
            case gridPrintNameCards:
                NamecardsView.startActivity(context);
                break;
            case gridUserGuide:
                new IntroDialog(context).show();
                break;
            case gridCustomColorNames:
                ColorSettingView.startActivity(context, null);
                break;
            case refresh:
                if (loginChecker.onTasking) {
                    MyUI.toastSHORT(context, String.format("데이터 업데이트가 진행중입니다. 잠시후 사용해주세요"));
                } else {
                    itemDB.clearCache();
                    loginChecker.init().start();
                }
                break;
        }
    }

}
