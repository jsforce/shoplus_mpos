package com.teampls.shoplus.silverlib;

import android.content.Context;

import com.teampls.shoplus.silverlib.awsservice.ProjectSilvermoonService;

/**
 * @author lucidite
 */
public class ProjectSilvermoonServiceManager {
    public static ProjectSilvermoonServiceProtocol defaultService(Context context) {
        return new ProjectSilvermoonService();
    }
}
