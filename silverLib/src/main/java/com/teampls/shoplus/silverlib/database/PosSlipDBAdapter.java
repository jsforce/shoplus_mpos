package com.teampls.shoplus.silverlib.database;

import android.content.Context;

import com.teampls.shoplus.lib.adapter.BaseSlipDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;

/**
 * Created by Medivh on 2017-04-24.
 */

public class PosSlipDBAdapter extends BaseSlipDBAdapter {
    public static final int SlipListView = 1;

    public PosSlipDBAdapter(Context context) {
        super(context, PosSlipDB.getInstance(context));
    }

    @Override
    public BaseViewHolder getViewHolder() {
        switch (viewType) {
            default:
                return new ViewHolder();
            case SlipListView:
                return new SilverSlipListViewHolder();
        }
    }

    class SilverSlipListViewHolder extends SlipListViewHolder {
        @Override
        public void update(int position) {
            super.update(position);
            SlipSummaryRecord summary = slipDB.items.getSummary(record.getSlipKey().toSID());
            tvAmount.setText(BaseUtils.toCurrencyOnlyStr(summary.amount + record.bill)); // 잔금 합친 금액을 보여줌
        }
    }

}
