package com.teampls.shoplus.silverlib.database;

import android.content.Context;
import android.graphics.Bitmap;

import com.teampls.shoplus.lib.adapter.BaseItemDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database_memory.MItemDB;
import com.teampls.shoplus.lib.event.MyOnTask;

/**
 * Created by Medivh on 2016-10-28.
 */
public class ItemDBAdapter extends BaseItemDBAdapter {
    public static final int ItemsFrag = 0, ItemAndOptionSearch = 1, ItemSearchByName = 2,
            ItemSelection = 3, ItemStateSettingView = 4, PreviousItemsView = 5, MallSelectionView = 6;

    public ItemDBAdapter(Context context) {
        super(context);
    }

    public ItemDBAdapter(Context context, ItemDB itemDB) {
        super(context, itemDB);
    }

    public ItemDBAdapter(Context context, MItemDB mItemDB) {
        super(context, mItemDB);
    }

    @Override
    public BaseViewHolder getViewHolder() {
        switch (viewType) {
            default:
            case ItemsFrag:
                return new DefaultViewHolder();
            case ItemAndOptionSearch:
                return new NameViewHolder();
            case ItemSearchByName:
                return new ItemSearchByNameViewHolder();
            case ItemStateSettingView:
                setImageViewRatio(1.0);
                return new DefaultViewHolder();
            case PreviousItemsView:
                setImageViewRatio(1.0);
                return new PreviousItemsViewHolder();
        }
    }

    // 별도 ImageDB로 처리한다
    class PreviousItemsViewHolder extends DefaultViewHolder {

        @Override
        protected void loadImage(int position) {
            if (position == 0) {
                imageLoader.retrieveThumb(record, PreviousThumbImageDB.getInstance(context), new MyOnTask<Bitmap>() {
                    @Override
                    public void onTaskDone(Bitmap bitmap) {
                        ivImage.setImageBitmap(bitmap);
                    }
                });
            } else {
                imageLoader.retrieveThumbAsync(record, PreviousThumbImageDB.getInstance(context), new MyOnTask<Bitmap>() {
                    @Override
                    public void onTaskDone(Bitmap bitmap) {
                        if (ivImage != null)
                            ivImage.setImageBitmap(bitmap);
                    }
                });
            }
        }
    }

}
