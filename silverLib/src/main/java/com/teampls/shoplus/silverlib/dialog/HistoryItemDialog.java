package com.teampls.shoplus.silverlib.dialog;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.MyStringAdapter;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.silverlib.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Medivh on 2019-07-04.
 */

abstract public class HistoryItemDialog<T> extends BaseDialog implements View.OnClickListener, AdapterView.OnItemClickListener {
    private ListView listView;
    private MyStringAdapter adapter;
    private List<T> records;

    public HistoryItemDialog(Context context, List<T> items) {
        super(context);

        adapter = new MyStringAdapter(context);
        adapter.enableBoundaryLine();

        records = items;
        Collections.reverse(records); // 최신순
        List<String> uiStrRecords = new ArrayList<>();
        for (T record : records)
            uiStrRecords.add(getUiString(record));
        adapter.setRecords(uiStrRecords);
        adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_SINGLE);

        listView = findViewById(R.id.dialog_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        listView.setDivider(null);
        listView.setDividerHeight(15);

        findTextViewById(R.id.dialog_list_title, "최근 선택 상품");
        if (items.size() == 0) {
            findTextViewById(R.id.dialog_list_message, "최근 선택한 상품이 없습니다\n\n\n");
        } else {
            findTextViewById(R.id.dialog_list_message, "최근 선택한 상품 중에서 다시 선택할 수 있습니다. ");
        }
        setOnClick(R.id.dialog_list_close, R.id.dialog_list_button_apply, R.id.dialog_list_button_cancel);
        setVisibility(View.VISIBLE, R.id.dialog_list_button_container);
        setVisibility(View.GONE,  R.id.dialog_list_button_apply);
        refresh();
        show();
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_list;
    }

    protected abstract String getUiString(T record);

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_list_close || view.getId() == R.id.dialog_list_button_cancel) {
            dismiss();

        } else if (view.getId() == R.id.dialog_list_button_apply) {
            if (adapter.getSortedClickedRecords().size() == 1) {
                onItemClicked(records.get(adapter.getClickedPositions().get(0)));
                dismiss();
            } else {
                MyUI.toastSHORT(context, String.format("설정에 오류가 있습니다"));
                dismiss();
            }
        }
    }

    abstract public void onItemClicked(@Nullable T selectedRecord);

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onItemClicked(records.get(position));
        dismiss(); // 선택이 곧 종료
    }
}