package com.teampls.shoplus.silverlib.database;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_memory.BaseListDB;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Medivh on 2018-07-16.
 */

public class MStockHistoryDB extends BaseListDB<OptionStockRecord> {
    private static MStockHistoryDB instance;
    private StockHistoryDB files;
    private Set<DateTime> createdDateTimes = new HashSet<>();
    private MyMap<String, Integer> stockSumsByItemId = new MyMap<>(0);
    private Set<Integer> serverItemIds = new HashSet<>(); // 서버에서 보내준 itemId 수집용
    private static String shopPhoneNumber = ""; // 현재 shop 정보만 다루자

    public static MStockHistoryDB getInstance(Context context, String shopPhoneNumber) {
        if (instance == null)
            instance = new MStockHistoryDB(context);
        MStockHistoryDB.shopPhoneNumber = shopPhoneNumber;
        return instance;
    }

    public MStockHistoryDB(Context context) {
        super(context, "MStockHistoryDB");
        files = StockHistoryDB.getInstance(context);
    }

    public void deleteAll(DateTime dateTime) {
        List<OptionStockRecord> records = new ArrayList<>();
        for (OptionStockRecord record : getRecords()) {
            if (record.createdDateTime.isEqual(dateTime) == false)
                continue;
            records.add(record);
        }
        deleteAll(records);
        deleteFromFile(records);
        createdDateTimes.remove(dateTime);
    }

    public void refreshFromFileDB() {
        database.clear();
        createdDateTimes.clear();
        stockSumsByItemId.clear();
        // 현재 샵 정보만 복원
        for (OptionStockRecord record : files.getRecords(StockHistoryDB.Column.shopPhoneNumber, shopPhoneNumber)) {
            insert(record);
            createdDateTimes.add(record.createdDateTime);
            serverItemIds.add(record.itemId);
            stockSumsByItemId.addInteger(record.getStockSumKey(), record.getPositiveStock());
        }
    }

    public void cleanUpByDays(int days) {
        files.cleanUpByDays(days, null);
    }

    public void toLogCatStockSum(String location) {
        for (String stockSumStr : stockSumsByItemId.keySet()) {
            Log.i("DEBUG_JS", String.format("[%s] %s, %d개", location, stockSumStr, stockSumsByItemId.get(stockSumStr)));
        }
    }

    public List<OptionStockRecord> insertStockMap(Map<ItemOptionRecord, Integer> stockMap) {
        List<OptionStockRecord> results = new ArrayList<>();
        DateTime createdDateTime = DateTime.now();
        for (ItemOptionRecord optionRecord : stockMap.keySet()) {
            OptionStockRecord record = new OptionStockRecord(shopPhoneNumber, createdDateTime, optionRecord, stockMap.get(optionRecord));
            insert(record);
            createdDateTimes.add(record.createdDateTime);
            stockSumsByItemId.addInteger(record.getStockSumKey(), record.getPositiveStock());
            serverItemIds.add(record.itemId);
            results.add(record);
        }
        return results;
    }

    synchronized public void saveToFileAsync(final List<OptionStockRecord> records) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                files.beginTransaction();
                for (OptionStockRecord record : records)
                    files.updateOrInsert(record);
                files.endTransaction();
            }
        };
    }

    synchronized private void deleteFromFile(final List<OptionStockRecord> records) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                List<OptionStockRecord> _records = new ArrayList<>();
                for (OptionStockRecord record : records) {
                    if (record.id <= 0) {
                        OptionStockRecord dbRecord = files.getRecordBy(record);
                        record.id = dbRecord.id;
                    }
                    _records.add(record);
                }
                files.deleteAll(_records);
            }
        };
    }

    @Override
    protected OptionStockRecord getEmptyRecord() {
        return new OptionStockRecord();
    }

    public List<DateTime> getSortedDateTimes() {
        List<DateTime> results = new ArrayList(createdDateTimes);
        Collections.sort(results);
        Collections.reverse(results);
        return results;
    }

    public MyMap<DateTime, Integer> getStockSums(int itemId) {
        MyMap<DateTime, Integer> results = new MyMap<>(0);
        for (DateTime dateTime : getSortedDateTimes())
            results.put(dateTime, stockSumsByItemId.get(new OptionStockRecord(shopPhoneNumber, dateTime, itemId).getStockSumKey()));
        return results;
    }

    public int getStockSum(DateTime dateTime, int itemId) {
        return stockSumsByItemId.get(new OptionStockRecord(shopPhoneNumber, dateTime, itemId).getStockSumKey());
    }

    public int getStockSum(DateTime dateTime, List<Integer> itemIds) {
        int result = 0;
        for (int itemId : itemIds) {
            result = result + stockSumsByItemId.get(new OptionStockRecord(shopPhoneNumber, dateTime, itemId).getStockSumKey());
            //Log.i("DEBUG_JS", String.format("[MStockHistoryDB.getPositiveStockSum] sum %d : %d, %s, %d", result, stockSumsByItemId.get(new OptionStockRecord(shopPhoneNumber, dateTime, itemId).getStockSumKey()), dateTime, itemId));
        }
        return result;
    }

    public int getLatestStockSum(int itemId) {
        List<DateTime> dateTimes = getSortedDateTimes();
        if (dateTimes.size() >= 1)
            return stockSumsByItemId.get(new OptionStockRecord(shopPhoneNumber, dateTimes.get(0), itemId).getStockSumKey());
        else
            return 0;
    }

    public MyMap<ItemOptionRecord, MyMap<DateTime, Integer>> getOptionDateTimeMap() {
        MyMap<DateTime, Integer> defaultMap = new MyMap<>(0);
        MyMap<ItemOptionRecord, MyMap<DateTime, Integer>> results = new MyMap<>(defaultMap); // option - time1, time2 ,....
        for (OptionStockRecord record : getRecords()) {
            MyMap<DateTime, Integer> dateStockMap = results.get(record.toOptionRecord(), new MyMap<DateTime, Integer>(0)); //
            dateStockMap.put(record.createdDateTime, record.stock);
            results.put(record.toOptionRecord(), dateStockMap);
        }
        return results;
    }

    public List<Integer> getServerItemIds() {
        return new ArrayList(serverItemIds);
    }

    public OptionStockRecord getRecord(ItemOptionRecord optionRecord, DateTime dateTime) {
        List<OptionStockRecord> results = new ArrayList<>();
        for (OptionStockRecord record : getRecords()) {
            if (record.itemId != optionRecord.itemId) continue;
            if (record.size != optionRecord.size) continue;
            if (record.color != optionRecord.color) continue;
            if (record.createdDateTime.isEqual(dateTime) == false) continue;
            results.add(record);
        }
        if (results.size() == 1) {
            return results.get(0);
        } else {
            Log.w("DEBUG_JS", String.format("[MStockHistoryDB.getRecord] %s, count %d", optionRecord.toStringWithDefault(","), results.size()));
            return getEmptyRecord();
        }
    }


}
