package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyImageLoader;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyNumberPlusMinus;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.SilverAppWatcher;

/**
 * Created by Medivh on 2019-02-14.
 */

public class MallOrderItemReview extends BaseActivity {
    private static SlipItemRecord slipItemRecord;
    protected MyRadioGroup<SlipItemType> slipItemTypes;
    protected MyNumberPlusMinus etQuantity;
    protected MyCurrencyEditText etUnitprice;
    protected boolean isManualItem = false;
    private static MyOnTask<Pair<DbActionType, SlipItemRecord>> onTask;

    public static void startActivity(Context context,  SlipItemRecord slipItemRecord, MyOnTask<Pair<DbActionType, SlipItemRecord>> onTask) {
        MallOrderItemReview.slipItemRecord = slipItemRecord;
        MallOrderItemReview.onTask = onTask;
        context.startActivity(new Intent(context, MallOrderItemReview.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SilverAppWatcher.checkInstance(this) == false) return;
        myActionBar.hide();
        isManualItem = (slipItemRecord.itemId == 0); // 0이면 수동추가 상품, 특별하게 다뤄져야 함
        final ItemRecord itemRecord = ItemDB.getInstance(context).getRecordBy(slipItemRecord.itemId); // 수동추가의 경우 itemId = 0

        findTextViewById(R.id.order_review_serial, itemRecord.serialNum);
        findTextViewById(R.id.order_review_name, slipItemRecord.name);
        findTextViewById(R.id.order_review_option, slipItemRecord.toOptionString(" / "));
        findTextViewById(R.id.order_review_price, itemRecord.getPriceStringWithRetail(context));
        BaseUtils.setBackgroundColorWithBox((ImageView) findViewById(R.id.order_review_option_color), slipItemRecord.color);

        etQuantity = new MyNumberPlusMinus(getView(), 0);
        etQuantity.setEditView(R.id.order_review_quantity_edittext);
        etQuantity.setPlusMinusButtons(R.id.order_review_quantity_plus, R.id.order_review_quantity_minus);
        etQuantity.setTextValue(slipItemRecord.quantity);

        etUnitprice = new MyCurrencyEditText(context, getView(), R.id.order_review_unitprice_edittext, 0);
        etUnitprice.setPlusMinusButton(R.id.order_review_unitprice_plus, R.id.order_review_unitprice_minus);
        etUnitprice.setTextViewAndBasicUnitCheckBox(R.id.order_review_unitprice_basicUnit, R.id.order_review_isOneDigit, userSettingData.isOneDigitBasicUnit());
        etUnitprice.setOriginalValue(slipItemRecord.unitPrice);
        etUnitprice.setOnCheckBoxChanged(new MyOnTask<Boolean>() {
            @Override
            public void onTaskDone(Boolean checked) {
                userSettingData.setBasicUnit(checked, null);
                etUnitprice.getEditText().getLayoutParams().width = MyDevice.toPixel(context, checked? 110 : 70);
            }
        });

        slipItemTypes = new MyRadioGroup<>(context, getView(), 1.0, this);
        setVisibility(View.VISIBLE, R.id.order_review_simpleTypeContainer);
        slipItemTypes.add(R.id.order_review_simple_sales, SlipItemType.SALES)
                .add(R.id.order_review_simple_preorder, SlipItemType.PREORDER)
                .add(R.id.order_review_simple_advance, SlipItemType.ADVANCE)
                .add(R.id.order_review_simple_preorder_outofstock, SlipItemType.PREORDER_OUT_OF_STOCK);
        slipItemTypes.setChecked(slipItemRecord.slipItemType);
        setVisibility(View.GONE, R.id.order_review_typeContainer);

        MyView.setTextViewByDeviceSize(context, getView(), R.id.order_review_quantity_title, R.id.order_review_unitprice_title);
        setOnClick(R.id.order_review_close, R.id.order_review_apply, R.id.order_review_split, R.id.order_review_delete,
                R.id.order_review_close_top);

        MyImageLoader.getInstance(context).retrieveThumb(itemRecord, (ImageView) findViewById(R.id.order_review_image));

        if (isManualItem) {
            setVisibility(View.GONE, R.id.order_review_image_frame, R.id.order_review_option_container,
                    R.id.order_review_price, R.id.order_review_simpleTypeContainer, R.id.order_review_simpleTypeContainer_bar);
            setVisibility(View.GONE, R.id.order_review_split);
        } else {
            setVisibility(View.GONE, R.id.order_review_delete); // 등록 상품은 삭제 금지
        }
    }

    @Override
    public int getThisView() {
        return R.layout.activity_order_review;
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUserSettingData(null);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.order_review_close || view.getId() == R.id.order_review_close_top) {
            finish();

        } else if (view.getId() == R.id.order_review_apply) {
            slipItemRecord.quantity = etQuantity.getValue();
            slipItemRecord.unitPrice = etUnitprice.getValue();
            slipItemRecord.slipItemType = slipItemTypes.getCheckedItem();
            if (onTask != null)
                onTask.onTaskDone(Pair.create(DbActionType.UPDATE, slipItemRecord));
            finish();

        } else if (view.getId() == R.id.order_review_split) {
            if (slipItemRecord.quantity <= 1) {
                MyUI.toastSHORT(context, String.format("개수가 2개 이상에서만 나누기가 됩니다"));
                return;
            }

            new UpdateValueDialog(context, "2개로 나누기", String.format("수량을 지정하시면 주문건을 2개로 나눠 따로 처리할 수 있습니다.\n\n현재 : %d개\n", slipItemRecord.quantity), 0) {
                @Override
                public void onApplyClick(String newStr) {
                    final int newQuantity = BaseUtils.toInt(newStr);
                    if (newQuantity >= slipItemRecord.quantity) {
                        doDismiss = false;
                        MyUI.toastSHORT(context, String.format("기존 개수보다 적어야 나눌 수 있습니다"));
                        return;
                    }

                    doDismiss = true;
                    slipItemRecord.unitPrice = etUnitprice.getValue(); // 단가만 반영
                    if (onTask != null) {
                        DbActionType dbActionType = DbActionType.SPLIT; // 분할 및 개수 지정
                        dbActionType.count = newQuantity;
                        onTask.onTaskDone(Pair.create(dbActionType, slipItemRecord));
                    }
                    finish();
                }
            };

        } else if (view.getId() == R.id.order_review_delete) {
            new MyAlertDialog(context, "항목 삭제", "이 항목을 삭제하시겠습니까?") {
                @Override
                public void yes() {
                    if (onTask != null)
                        onTask.onTaskDone(Pair.create(DbActionType.DELETE, slipItemRecord));
                    finish();
                }
            };
        }
    }

    @Override
    public void onMyMenuCreate() {
        // N.A
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        // N.A
    }
}
