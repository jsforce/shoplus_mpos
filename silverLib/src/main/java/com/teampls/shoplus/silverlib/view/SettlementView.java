package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.AccountLogRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.BaseListDB;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.viewSetting.PrinterSettingView;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyExcel;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.common.SilverAppWatcher;
import com.teampls.shoplus.silverlib.common.SilverPosPrinter;
import com.teampls.shoplus.silverlib.database.EntrustLogRecord;
import com.teampls.shoplus.silverlib.database.SettlementRecord;
import com.teampls.shoplus.silverlib.datatypes.SettlementAuxilityData;
import com.teampls.shoplus.silverlib.enums.SettlementAuxilityDataType;

import org.joda.time.DateTime;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Medivh on 2019-06-28.
 */

public class SettlementView extends BaseActivity {
    private static SettlementRecord record;
    private TextView tvPeriod, tvAmount, tvAmountCash, tvAmountOnline, tvAmountEntrust, tvIncome,
            tvSettlement, tvSettlementCash, tvSettlementIncome, tvSettlementSpending, tvSettlementVault, tvVault, tvSpending;
    private LinearLayout printView;
    private ExpandableHeightListView lvOutgoDetails, lvIncomeDetails, lvEntrusts;
    private SettlementAuxDBAdapter incomeAdapter, outgoAdapter, entrustAdapter;
    private SettlementAuxDB settlementAuxDB;
    protected SilverPosPrinter myPosPrinter;
    private EntrustLogDB entrustsDB;
    private DateTime fromDateTime, toDateTime;

    public static void startActivity(Context context, SettlementRecord record) {
        SettlementView.record = record;
        context.startActivity(new Intent(context, SettlementView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SilverAppWatcher.checkInstanceAndRestart(this) == false) return;
        myPosPrinter = SilverPosPrinter.getInstance(context);
        myActionBar.hideLauncher();
        settlementAuxDB = new SettlementAuxDB(context);
        entrustsDB = new EntrustLogDB(context);
        fromDateTime = record.getPeriod().first;
        toDateTime = record.getPeriod().second;

        printView = findViewById(R.id.account_settlement_container);
        String periodStr = String.format("%s ~ %s", fromDateTime.toString(BaseUtils.MDHm_Kor_Format), toDateTime.toString(BaseUtils.MDHm_Kor_Format));
        tvPeriod = findTextViewById(R.id.account_settlement_period_textview, periodStr);

        tvAmount = findTextViewById(R.id.account_settlement_amount, String.format("매출총액 : %s", BaseUtils.toCurrencyStr(record.amount)));
        tvAmountCash = findTextViewById(R.id.account_settlement_amount_cash, String.format("현장결제 : %s", BaseUtils.toCurrencyOnlyStr(record.cash)));
        tvAmountOnline = findTextViewById(R.id.account_settlement_amount_online, String.format("온라인청구 : %s", BaseUtils.toCurrencyOnlyStr(record.online)));
        tvAmountEntrust = findTextViewById(R.id.account_settlement_amount_entrust, String.format("대납청구 : %s", BaseUtils.toCurrencyOnlyStr(record.entrust)));

        tvSettlement = findTextViewById(R.id.account_settlement_settlement, "");
        tvSettlementCash = findTextViewById(R.id.account_settlement_settlement_cash, String.format("현장결제 : %s", BaseUtils.toCurrencyOnlyStr(record.cash)));
        tvSettlementIncome = findTextViewById(R.id.account_settlement_settlement_entrust, "");
        tvSettlementSpending = findTextViewById(R.id.account_settlement_settlement_cost, "");
        tvSettlementVault = findTextViewById(R.id.account_settlement_settlement_vault, "");

        tvVault = findTextViewById(R.id.account_settlement_vault, String.format("시재 : (+ 클릭)"));
        tvSpending = findTextViewById(R.id.account_settlement_spending, "");

        outgoAdapter = new SettlementAuxDBAdapter(context);
        lvOutgoDetails = findViewById(R.id.account_settlement_outgo_listview);
        lvOutgoDetails.setExpanded(true);
        lvOutgoDetails.setAdapter(outgoAdapter);
        lvOutgoDetails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new SettlementAuxDialog(context, outgoAdapter.getRecord(position)) {
                    @Override
                    public void onApplyClick(SettlementAuxilityData record) {
                        uploadSettlementData(record);
                    }

                    @Override
                    public void onDeleteClick(SettlementAuxilityData record) {
                        deleteSettlementData(record);
                    }
                };
            }
        });

        incomeAdapter = new SettlementAuxDBAdapter(context);
        tvIncome = findTextViewById(R.id.account_settlement_income, "");
        lvIncomeDetails = findViewById(R.id.account_settlement_income_listview);
        lvIncomeDetails.setExpanded(true);
        lvIncomeDetails.setAdapter(incomeAdapter);
        lvIncomeDetails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new SettlementAuxDialog(context, incomeAdapter.getRecord(position)) {
                    @Override
                    public void onApplyClick(SettlementAuxilityData record) {
                        uploadSettlementData(record);
                    }

                    @Override
                    public void onDeleteClick(SettlementAuxilityData record) {
                        deleteSettlementData(record);
                    }
                };
            }
        });

        entrustAdapter = new SettlementAuxDBAdapter(context);
        entrustAdapter.showDateTime();
        lvEntrusts = findViewById(R.id.account_settlement_entrust_listview);
        lvEntrusts.setExpanded(true);
        lvEntrusts.setAdapter(entrustAdapter);
        lvEntrusts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MyUI.toastSHORT(context, String.format("대납건은 수정할 수 없습니다"));
            }
        });

        setOnClick(R.id.account_settlement_spending_add, R.id.account_settlement_income_add, R.id.account_settlement_vault,
                R.id.account_settlement_print, R.id.account_settlement_close);

        MyView.setFontSizeByDeviceSize(context, tvPeriod, tvAmount, tvAmountCash, tvAmountOnline, tvAmountEntrust, tvIncome,
                tvSettlement, tvSettlementCash, tvSettlementIncome, tvSettlementSpending, tvSettlementVault, tvVault, tvSpending);

        refreshView();
        downloadSettlementAndEntrusts(fromDateTime, toDateTime);
    }

    @Override
    public int getThisView() {
        return R.layout.activity_account_settlement;
    }

    @Override
    protected void onDestroy() {
        if (myPosPrinter != null)
            myPosPrinter.disconnectPrinter();
        super.onDestroy();
    }

    private void refresh() {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                outgoAdapter.setRecords(settlementAuxDB.getOutgos());
                incomeAdapter.setRecords(settlementAuxDB.getIncomes());
                entrustAdapter.setRecordsFrom(entrustsDB.getRecords());
            }

            @Override
            public void onPostExecutionUI() {
                outgoAdapter.notifyDataSetChanged();
                incomeAdapter.notifyDataSetChanged();
                entrustAdapter.notifyDataSetChanged();
                refreshView();
            }
        };

    }

    private void refreshView() {
        Pair<SettlementAuxilityData, SettlementAuxilityData> vaults = settlementAuxDB.getVault();

        int outgoSum = 0;
        for (SettlementAuxilityData amountRecord : outgoAdapter.getRecords())
            outgoSum = outgoSum + amountRecord.getUnsignedAmount();

        int incomeSum = 0;
        for (SettlementAuxilityData incomeRecord : incomeAdapter.getRecords())
            incomeSum = incomeSum + incomeRecord.getUnsignedAmount(); // 추가수입은 음수로 처리하기로 약속함

        for (SettlementAuxilityData entrustRecord : entrustAdapter.getRecords())
            incomeSum = incomeSum + entrustRecord.getUnsignedAmount();

        tvSettlement.setText(String.format("현금결산 : %s", BaseUtils.toCurrencyStr(record.cash + incomeSum - outgoSum + vaults.first.amount - vaults.second.amount)));
        tvSettlementSpending.setText(String.format("차감액 : %s", BaseUtils.toCurrencySign(-outgoSum)));
        tvSettlementVault.setText(String.format("시재차액 : %s", BaseUtils.toCurrencySign(vaults.first.amount - vaults.second.amount)));
        tvSettlementIncome.setText(String.format("추가액 : %s", BaseUtils.toCurrencySign(incomeSum)));

        tvSpending.setText(String.format("차감액%s", outgoSum == 0 ? "" : String.format(" : %s", BaseUtils.toCurrencyStr(outgoSum))));
        tvIncome.setText(String.format("추가액%s", incomeSum == 0 ? "" : String.format(" : %s", BaseUtils.toCurrencyStr(incomeSum))));

        tvVault = findTextViewById(R.id.account_settlement_vault, (vaults.first.amount == 0 && vaults.second.amount == 0) ? "시재 (클릭)" : String.format("시재 : %s (시작) %s (종료)",
                BaseUtils.toCurrencyOnlyStr(vaults.first.amount), BaseUtils.toCurrencyOnlyStr(vaults.second.amount)));
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        if (view.getId() == R.id.account_settlement_close) {
            finish();

        } else if (view.getId() == R.id.account_settlement_print) {
            if (MyDevice.isBluetoothEnabled() == false) {
                MyUI.toast(context, String.format("블루투스가 꺼져있습니다. [프린터설정] 에서 켜실 수 있습니다"));
                return;
            }

            if (myPosPrinter.hasPairedDevice() == false) {
                MyUI.toast(context, String.format("연결된 장치가 없습니다. [프린터설정] 에서 선택해 주세요"));
                return;
            }

            new MyAlertDialog(context, "마감 출력", "마감을 출력하시겠습니까?") {
                @Override
                public void yes() {
                    myPosPrinter.printSettlement(record, settlementAuxDB.getVault(), outgoAdapter.getRecords(), incomeAdapter.getRecords(), entrustAdapter.getRecords());
                }
            };

            // 추가 수입
        } else if (view.getId() == R.id.account_settlement_income_add) {
            new SettlementAuxDialog(context, true) {
                @Override
                public void onApplyClick(SettlementAuxilityData record) {
                    uploadSettlementData(record);
                }

                @Override
                public void onDeleteClick(SettlementAuxilityData record) {
                    deleteSettlementData(record);
                }
            };

            // 지출
        } else if (view.getId() == R.id.account_settlement_spending_add) {
            new SettlementAuxDialog(context, false) {
                @Override
                public void onApplyClick(SettlementAuxilityData record) {
                    uploadSettlementData(record);
                }

                @Override
                public void onDeleteClick(SettlementAuxilityData record) {
                    deleteSettlementData(record);
                }
            };

        } else if (view.getId() == R.id.account_settlement_vault) {
            final Pair<SettlementAuxilityData, SettlementAuxilityData> vaults = settlementAuxDB.getVault();
            new VaultDialog(context, vaults) {
                @Override
                void onApplyClick(int start, int end) {
                    uploadVaultData(vaults.first.amount != start, start, fromDateTime.plusMinutes(1),  // 시작 1분후 입력한 것으로
                            vaults.second.amount != end, end, toDateTime.minusMinutes(1)); // 종료 1분전 입력한 것으로
                }

                @Override
                void onDeleteClick() {
                    uploadVaultData(true, 0, vaults.first.getCreatedDateTime(), true, 0, vaults.second.getCreatedDateTime());
                }
            };
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.add(MyMenu.sendKakao)
                .add(MyMenu.shareFile)
                .add(MyMenu.sendAsExcel)
                .add(MyMenu.printSetting)
                .add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case printSetting:
                PrinterSettingView.startActivity(context, new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        myPosPrinter.refreshCurrentDevice(); // 서로 다른 객체라 프린터를 선택했을경우 이를 전달해야 함
                    }
                });
                break;
            case sendAsExcel:
                new MyAsyncTask(context, "엑셀 파일 생성중...") {
                    @Override
                    public void doInBackground() {
                        MyExcel myExcel = new MyExcel(context);
                        String filePath = "";//myExcel.createEmployReport(record);
                        if (filePath.isEmpty() == false) {
                            MyUI.toastSHORT(context, String.format("엑셀 파일을 전달할 앱을 선택해 주세요"));
                            MyDevice.shareFile(context, filePath);
                        }
                    }
                };
                break;
            case shareFile:
                String filePath = MyDevice.createTempImageFilePath();
                boolean successful = MyGraphics.toFile(MyGraphics.toBitmap(printView), filePath);
                if (successful) {
                    MyDevice.refresh(context, new File(filePath));
                    MyDevice.shareImageFile(context, filePath);
                }
                break;
            case sendKakao:
                MyDevice.sendKakao(context, MyGraphics.toBitmap(printView));
                break;
            case refresh:
                downloadSettlementAndEntrusts(fromDateTime, toDateTime);
                break;
            case close:
                finish();
                break;
        }
    }

    public void downloadSettlementAndEntrusts(final DateTime fromDate, final DateTime toDate) {
        new MyServiceTask(context, "downloadSettlementAndEntrusts", "지출내역 다운중...") {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                // 지출건
                List<SettlementAuxilityData> data = service.getSettlementAuxilityData(userSettingData.getUserShopInfo(), fromDate, toDate);
                settlementAuxDB.set(data, fromDate, toDate);

                // 대납건
                List<AccountChangeLogDataProtocol> protocols = accountService.downloadEntrustedChangeHistory(userSettingData.getUserShopInfo(), userSettingData.getUserConfigType(), fromDate, toDate);
                entrustsDB.set(protocols);
                entrustsDB.clearCancelledPairs();
            }

            @Override
            public void onPostExecutionUI() {
                refresh();
            }
        };
    }

    public void uploadSettlementData(final SettlementAuxilityData record) {
        new MyServiceTask(context, "uploadSettlementData", "업로드중...") {
            SettlementAuxilityData result;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                result = service.postSettlementExpenseData(userSettingData.getUserShopInfo(), record.title, record.amount, record.getCreatedDateTime());
            }

            @Override
            public void onPostExecutionUI() {
                settlementAuxDB.insert(result);
                refresh();
            }
        };
    }

    public void deleteSettlementData(final SettlementAuxilityData record) {
        new MyServiceTask(context, "deleteSettlementData", "삭제중...") {
            SettlementAuxilityData result;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                result = service.postSettlementExpenseData(userSettingData.getUserShopInfo(), record.title, 0, record.getCreatedDateTime());
            }

            @Override
            public void onPostExecutionUI() {
                settlementAuxDB.delete(result);
                refresh();
            }
        };
    }


    public void uploadVaultData(final boolean isValid1, final int amount1, final DateTime dateTime1, final boolean isValid2, final int amount2, final DateTime dateTime2) {
        new MyServiceTask(context, "uploadVaultData", "시재 업로드중...") {
            SettlementAuxilityData result;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                if (isValid1) {
                    result = service.postSettlementAmountOnHandData(userSettingData.getUserShopInfo(), amount1, dateTime1);
                    settlementAuxDB.updateOrInsert(result);
                }

                if (isValid2) {
                    result = service.postSettlementAmountOnHandData(userSettingData.getUserShopInfo(), amount2, dateTime2);
                    settlementAuxDB.updateOrInsert(result);
                }
            }

            @Override
            public void onPostExecutionUI() {
                refresh();
            }
        };
    }

    class SettlementAuxDB extends BaseListDB<SettlementAuxilityData> {
        private DateTime fromDateTime = Empty.dateTime, toDateTime = Empty.dateTime;

        public SettlementAuxDB(Context context) {
            super(context, "SettlementAuxDB");
        }

        @Override
        protected SettlementAuxilityData getEmptyRecord() {
            return new SettlementAuxilityData();
        }

        public void set(List<SettlementAuxilityData> data, DateTime fromDateTime, DateTime toDateTime) {
            clear();
            for (SettlementAuxilityData record : data) {
                if (record.getType() == SettlementAuxilityDataType.EXPENSE && record.amount == 0) // 비용이 0인것은 제외
                    continue;
                database.add(record);
            }
            this.fromDateTime = fromDateTime;
            this.toDateTime = toDateTime;
        }

        @Override
        public void updateOrInsert(SettlementAuxilityData record) {
            SettlementAuxilityData dbRecord = getRecordById(record.getId());
            if (dbRecord.getId().isEmpty()) {
                insert(record);
            } else {
                dbRecord.update(record);
                update(dbRecord);
            }
        }

        @Override
        public boolean delete(SettlementAuxilityData record) {
            SettlementAuxilityData dbRecord = getRecordById(record.getId());
            if (dbRecord.getId().isEmpty() == false)
                return super.delete(dbRecord);
            else
                return false;
        }

        public SettlementAuxilityData getRecordById(String id) {
            for (SettlementAuxilityData record : getRecords()) {
                if (record.getId().equals(id))
                    return record;
            }
            return new SettlementAuxilityData();
        }

        public DateTime getNextDateTime() {
            List<SettlementAuxilityData> records = new ArrayList<>();
            for (SettlementAuxilityData record : getRecords()) {
                if (record.getType() == SettlementAuxilityDataType.EXPENSE) {
                    records.add(record);
                }
            }
            if (records.size() == 0) {
                return fromDateTime.plusMinutes(1);
            } else {
                Collections.sort(records); // 시간순 정렬
                return BaseUtils.min(records.get(records.size() - 1).getCreatedDateTime().plusMinutes(1), toDateTime);
            }
        }

        public Pair<SettlementAuxilityData, SettlementAuxilityData> getVault() {
            List<SettlementAuxilityData> vaults = new ArrayList<>();
            for (SettlementAuxilityData record : getRecords()) {
                if (record.getType() == SettlementAuxilityDataType.AMOUNT_ON_HAND)
                    vaults.add(record);
            }
            Collections.sort(vaults);
            if (vaults.size() == 0)
                return Pair.create(new SettlementAuxilityData(), new SettlementAuxilityData());
            else if (vaults.size() == 1)
                return Pair.create(vaults.get(0), vaults.get(0));
            else
                return Pair.create(vaults.get(0), vaults.get(vaults.size() - 1));
        }

        public List<SettlementAuxilityData> getOutgos() {
            List<SettlementAuxilityData> results = new ArrayList<>();
            for (SettlementAuxilityData record : getRecords()) {
                if (record.getType() == SettlementAuxilityDataType.EXPENSE && record.amount >= 0) {
                    results.add(record);
                }
            }
            return results;
        }

        public List<SettlementAuxilityData> getOutgosBy(double fromHour, double toHour, boolean isInRange) {
            return filterRecords(getOutgos(), fromHour, toHour, isInRange);
        }

        public List<SettlementAuxilityData> getIncomesBy(double fromHour, double toHour, boolean isInRange) {
            return filterRecords(getIncomes(), fromHour, toHour, isInRange);
        }

        private List<SettlementAuxilityData> filterRecords( List<SettlementAuxilityData> records, double fromHour, double toHour, boolean isInRange) {
            List<SettlementAuxilityData> results = new ArrayList<>();
            for (SettlementAuxilityData record :records) {
                if (isInRange) {
                    if (BaseUtils.isBetweenByHm(record.getCreatedDateTime(), fromHour, toHour))
                        results.add(record);
                } else {
                    if (BaseUtils.isBetweenByHm(record.getCreatedDateTime(), fromHour, toHour) == false)
                        results.add(record);
                }
            }
            return results;
        }

        public List<SettlementAuxilityData> getIncomes() {
            List<SettlementAuxilityData> results = new ArrayList<>();
            for (SettlementAuxilityData record : getRecords()) {
                if (record.getType() == SettlementAuxilityDataType.EXPENSE && record.amount < 0) { // 수입은 (-)로 처리하기로 약속
                    results.add(record);
                }
            }
            return results;
        }
    }

    class SettlementAuxDBAdapter extends BaseDBAdapter<SettlementAuxilityData> {
        private boolean doShowDateTime = false;

        public SettlementAuxDBAdapter(Context context) {
            super(context);
        }

        public void showDateTime() {
            doShowDateTime = true;
        }

        public void setRecordsFrom(List<EntrustLogRecord> entrustLogRecords) {
            clear();
            for (EntrustLogRecord record : entrustLogRecords)
                addRecord(record.toSettlementAuxilityData());
        }

        @Override
        public void generate() {

        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new BaseViewHolder() {
                private TextView tvString;

                @Override
                public int getThisView() {
                    return R.layout.row_string;
                }

                @Override
                public void init(View view) {
                    tvString = findTextViewById(context, view, R.id.row_string_textview);
                }

                @Override
                public void update(int position) {
                    SettlementAuxilityData record = getRecord(position);
                    if (doShowDateTime) {
                        tvString.setText(String.format("%s .. %s (%s)", record.title, BaseUtils.toCurrencyOnlyStr(record.getUnsignedAmount()), record.getCreatedDateTime().toString(BaseUtils.Hm_Format)));
                    } else {
                        tvString.setText(String.format("%s .. %s", record.title, BaseUtils.toCurrencyOnlyStr(record.getUnsignedAmount())));
                    }
                }
            };
        }
    }

    abstract class SettlementAuxDialog extends BaseDialog {
        private EditText etName;
        private MyCurrencyEditText  etCost;
        private SettlementAuxilityData settlementAuxilityData = new SettlementAuxilityData();
        private DbActionType dbActionType = DbActionType.INSERT;
        private boolean isIncome = false;

        public SettlementAuxDialog(Context context, boolean isIncome) {
            super(context);
            this.isIncome = isIncome;
            dbActionType = DbActionType.INSERT;
            setVisibility(View.GONE, R.id.dialog_settlement_aux_delete);
            init();
        }

        public SettlementAuxDialog(Context context, SettlementAuxilityData settlementAuxilityData) {
            super(context);
            dbActionType = DbActionType.UPDATE;
            this.settlementAuxilityData = settlementAuxilityData;
            if (isIncome == false)
                isIncome = settlementAuxilityData.isIncome();
            init();
        }

        private void init() {
            setDialogWidth(0.9, 0.7);
            if (isIncome)
                findTextViewById(R.id.dialog_settlement_aux_title, "수입 등록");

            etName = (EditText) findTextViewById(R.id.dialog_settlement_aux_name, settlementAuxilityData.title);
            etCost = new MyCurrencyEditText(context, getView(), R.id.dialog_settlement_aux_cost, R.id.dialog_settlement_aux_cost_clear);
            etCost.setOriginalValue(settlementAuxilityData.getUnsignedAmount());

            LinearLayout container = findViewById(R.id.dialog_settlement_aux_preset_container);
            MyView.setTextViewByDeviceSize(context, container);
            if (isIncome == false) {
                container.setVisibility(View.VISIBLE);
                for (View view : MyView.getChildViews(container)) {
                    if (view instanceof TextView) {
                        final TextView textView = (TextView) view;
                        MyView.setTextViewByDeviceSize(context, textView);
                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                etName.setText(textView.getText().toString());
                                etCost.getEditText().requestFocus();
                            }
                        });
                    }
                }
            } else {
                container.setVisibility(View.GONE);
            }

            setOnClick(R.id.dialog_settlement_aux_name_clear,
                    R.id.dialog_settlement_aux_apply, R.id.dialog_settlement_aux_cancel, R.id.dialog_settlement_aux_delete);
            MyDevice.showKeyboard(context, etName);
            show();
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_settlement_aux;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_settlement_aux_name_clear) {
                etName.setText("");

            } else if (view.getId() == R.id.dialog_settlement_aux_delete) {
                new MyAlertDialog(context, "항목 삭제", "이 항목을 삭제하시겠습니까?") {
                    @Override
                    public void yes() {
                        onDeleteClick(settlementAuxilityData);
                        dismiss();
                        MyDevice.hideKeyboard(context, etName);
                    }
                };

            } else if (view.getId() == R.id.dialog_settlement_aux_cancel) {
                MyDevice.hideKeyboard(context, etName);
                dismiss();

            } else if (view.getId() == R.id.dialog_settlement_aux_apply) {
                if (Empty.isEmpty(context, etName, "항목이 입력되지 않았습니다"))
                    return;
                if (Empty.isEmpty(context, etCost.getEditText(), "비용이 입력되지 않았습니다"))
                    return;

                settlementAuxilityData.title = etName.getText().toString();
                settlementAuxilityData.amount = etCost.getValue();
                if (isIncome)
                    settlementAuxilityData.amount = settlementAuxilityData.amount * (-1); // -는 수입을 의미함
                switch (dbActionType) {
                    case INSERT:
                        settlementAuxilityData.setDateTime(settlementAuxDB.getNextDateTime());
                        break;
                    case UPDATE:
                        break;
                }
                onApplyClick(settlementAuxilityData);
                dismiss();
                MyDevice.hideKeyboard(context, etName);
            }
        }

        abstract public void onApplyClick(SettlementAuxilityData record);

        abstract public void  onDeleteClick(SettlementAuxilityData record);

    }

    abstract class VaultDialog extends BaseDialog {
        private MyCurrencyEditText etStart, etEnd;
        private Pair<SettlementAuxilityData, SettlementAuxilityData> vaults;

        public VaultDialog(Context context, Pair<SettlementAuxilityData, SettlementAuxilityData> vaults) {
            super(context);
            setDialogWidth(0.9, 0.7);
            this.vaults = vaults;
            etStart = new MyCurrencyEditText(context, getView(), R.id.dialog_vault_start, R.id.dialog_vault_start_clear);
            etStart.setOriginalValue(vaults.first.amount);
            etEnd = new MyCurrencyEditText(context, getView(), R.id.dialog_vault_end, R.id.dialog_vault_end_clear);
            etEnd.setOriginalValue(vaults.second.amount);
            setOnClick(R.id.dialog_vault_apply, R.id.dialog_vault_cancel, R.id.dialog_vault_delete);
            MyDevice.showKeyboard(context, etStart.getEditText());
            MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_vault_title, R.id.dialog_vault_start_title, R.id.dialog_vault_end_title);
            if (vaults.first.amount == 0 && vaults.second.amount == 0)
                setVisibility(View.GONE, R.id.dialog_vault_delete);
            show();
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_vault_input;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_vault_apply) {
                onApplyClick(etStart.getValue(), etEnd.getValue());
                dismiss();
                MyDevice.hideKeyboard(context, etStart.getEditText());

            } else if (view.getId() == R.id.dialog_vault_cancel) {
                dismiss();
                MyDevice.hideKeyboard(context, etStart.getEditText());

            } else if (view.getId() == R.id.dialog_vault_delete) {
                new MyAlertDialog(context, "삭제", "입력한 시재를 삭제하시겠습니까?") {
                    @Override
                    public void yes() {
                        dismiss();
                        onDeleteClick();
                        MyDevice.hideKeyboard(context, etStart.getEditText());
                    }
                };
            }
        }

        abstract void onApplyClick(int vaultStart, int vaultEnd);

        abstract void onDeleteClick();
    }

    class EntrustLogDB extends BaseListDB<EntrustLogRecord> {

        public EntrustLogDB(Context context) {
            super(context, "EntrustLogDB");
        }

        @Override
        protected EntrustLogRecord getEmptyRecord() {
            return new EntrustLogRecord();
        }

        public void set( List<AccountChangeLogDataProtocol> protocols) {
            clear();
            for (AccountChangeLogDataProtocol protocol : protocols) {
                AccountLogRecord record = new AccountLogRecord(protocol, AccountType.EntrustedAccount);
                if (protocol.isCancelled())
                    continue;

                UserRecord counterpart = userDB.getRecord(record.getCounterpart(context));
                insert(new EntrustLogRecord(record.createdDateTime, counterpart.getName(), -record.variation)); // (- 임에 주의)
            }
        }

        public List<EntrustLogRecord> getRecordsBy(double fromHour, double toHour, boolean isInRange) {
            List<EntrustLogRecord> results = new ArrayList<>();
            for (EntrustLogRecord record : getRecords()) {
                if (isInRange) {
                    if (BaseUtils.isBetweenByHm(record.createdDate, fromHour, toHour))
                        results.add(record);
                } else {
                    if (BaseUtils.isBetweenByHm(record.createdDate, fromHour, toHour) == false)
                        results.add(record);
                }
            }
            return results;
        }

        public void clearCancelledPairs() {
            // 대납을 체크했다가 해제한 경우 2개 로그가 모두 남는 것을 제거
            List<EntrustLogRecord> pairedEntrusts = new ArrayList<>();
            for (EntrustLogRecord record : getRecords()) {
                if (record.amount < 0) { // 취소한 경우, 즉 금액이 마이너스만 조사해보면 된다
                    for (EntrustLogRecord targetRecord : getRecords()) {
                        if (record.equals(targetRecord) || pairedEntrusts.contains(targetRecord)) // 이미 쌍으로 제거된 항목
                            continue;
                        if (record.counterpartName.equals(targetRecord.counterpartName) && (record.amount + targetRecord.amount == 0)) {
                            pairedEntrusts.add(record);
                            pairedEntrusts.add(targetRecord);
                            break;
                        }
                    }
                }
            }

            for (EntrustLogRecord record : pairedEntrusts)
                record.toLogCat("clearCancelledPairs");

            database.removeAll(pairedEntrusts);
        }
    }
}
