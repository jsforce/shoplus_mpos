package com.teampls.shoplus.silverlib.dialog;

import android.content.Context;
import android.view.View;

import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;

/**
 * Created by Medivh on 2018-02-23.
 */

public class IntroDialog extends MyButtonsDialog {

    public IntroDialog(Context context) {
        this(context, "앱 사용법 안내", "앱 설치에 감사드립니다. 궁금하신 내용을 눌러보세요.");
    }

    private IntroDialog(final Context context, String title, String message) {
        super(context, title, message);
        setDialogWidth(0.85, 0.6);

        addButton("상품 등록 방법", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDevice.openWeb(context, "http://shopl-us.com/2017/09/20/android%ec%83%81%ed%92%88-%eb%93%b1%eb%a1%9d%ed%95%98%ea%b8%b0/");
            }
        });

        addButton("영수증 발행 방법", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDevice.openWeb(context, "https://shopl-us.com/2017/09/22/android%EC%98%81%EC%88%98%EC%A6%9D-%EC%9E%91%EC%84%B1%ED%95%98%EA%B8%B0/");
            }
        });

        addButton("홈페이지 방문", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDevice.openWeb(context, BaseGlobal.SHOPL_US);
            }
        });

        addButton("카톡으로 문의", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
            }
        });

        addButton("우리매장 방문요청", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                MyUI.toastSHORT(context, String.format("준비 중입니다. 우선 카톡으로 남겨주세요"));
            }
        });
    }

}