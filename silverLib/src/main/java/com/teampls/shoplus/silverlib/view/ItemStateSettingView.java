package com.teampls.shoplus.silverlib.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseItemDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseItemHeader;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.enums.ItemSortingKey;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseItemsActivity;
import com.teampls.shoplus.lib.view_base.MyGridView;
import com.teampls.shoplus.lib.view_module.BaseFloatingButtons;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.database.ItemDBAdapter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2018-08-04.
 */

public class ItemStateSettingView extends BaseItemsActivity implements AdapterView.OnItemClickListener {
    private ItemHeader itemHeader;
    private ItemDBAdapter adapter;
    public MyGridView gridView;
    private TextView tvNotice;
    private ItemsFloatingButtons floatingButtons;

    public static void startActivity(Context context) {
        ((Activity) context).startActivityForResult(new Intent(context, ItemStateSettingView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.hide();
        setAdaptor();
        itemHeader = new ItemHeader(this, getView(), adapter);
        itemHeader.setSortingKeys(ItemSortingKey.ItemId,
                ItemSortingKey.Name, ItemSortingKey.StockCount);

        floatingButtons = new ItemsFloatingButtons(getView());

        gridView = new MyGridView(context, getView(), R.id.item_state_setting_gridview);
        gridView.setAdapter(adapter);
        gridView.getThis().setOnItemClickListener(this);
        gridView.setOnScroll(new MyOnTask<Boolean>() {
            @Override
            public void onTaskDone(Boolean isFirstItemVisible) {
                if (floatingButtons != null)
                    floatingButtons.ivMoveToPosition0.setVisibility(isFirstItemVisible ? View.GONE : View.VISIBLE);
            }
        });

        tvNotice = findTextViewById(R.id.item_state_setting_notice, "");

        setOnClick(R.id.item_state_setting_close, R.id.item_state_setting_clear,
                R.id.item_state_setting_apply);

        itemHeader.refresh();
        refresh();
    }

    protected void setAdaptor() {
        adapter = new ItemDBAdapter(context);
        adapter.showName();
        adapter.setSelectable().setChoiceMode(BaseDBAdapter.CHOICE_MODE_MULTIPLE);
        adapter.setViewType(ItemDBAdapter.ItemStateSettingView);
    }

    public void refresh() {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                adapter.generate();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
                itemHeader.refresh();
                floatingButtons.refresh();
                refreshHeader();
            }
        };
    }

    private void refreshHeader() {
        tvNotice.setText(String.format("%d/%d", adapter.clickedPositions.size(), adapter.getCount()));
    }

    @Override
    public int getThisView() {
        return R.layout.activity_item_state_setting;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.item_state_setting_close) {
            doFinishForResult();

        } else if (view.getId() == R.id.item_state_setting_clear) {
            adapter.clearClicked();
            refresh();

        } else if (view.getId() == R.id.item_state_setting_apply) {
            new ChangeStateDialog(context, adapter.getSortedClickedRecords()) {
                @Override
                protected void onChangeFinished() {
                    adapter.clearClicked();
                    refresh();
                }
            };
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.add(MyMenu.close);

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case close:
                doFinishForResult();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        adapter.onItemClick(view, position);
        refreshHeader();
    }


    class ItemHeader extends BaseItemHeader {

        public ItemHeader(BaseItemsActivity activity, View view, BaseItemDBAdapter adapter) {
            super(activity, view, adapter);
        }

        @Override
        public void refreshAdapter() {
            ItemStateSettingView.this.refresh();
        }

        @Override
        public void onFilterNameClick(final int position) {
            gridView.getThis().post(new Runnable() {
                @Override
                public void run() {
                    gridView.getThis().smoothScrollToPosition(position);
                }
            });
        }
    }

    class ItemsFloatingButtons extends BaseFloatingButtons {
        protected ImageView ivResetFilter, ivMoveToPosition0;

        public ItemsFloatingButtons(View view) {
            super(view);
            add(R.id.items_reset_filter, R.id.items_to_position0);
            ivResetFilter = (ImageView) view.findViewById(R.id.items_reset_filter);
            ivMoveToPosition0 = (ImageView) view.findViewById(R.id.items_to_position0);
            ivMoveToPosition0.setImageAlpha(150);
            MyView.setImageViewSizeByDeviceSize(context, ivResetFilter, ivMoveToPosition0);
            setVisibility(View.GONE, R.id.items_back, R.id.items_more);
            hide();
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.items_reset_filter) {
                adapter.resetFiltering();
                ItemStateSettingView.this.refresh();

            } else if (view.getId() == R.id.items_to_position0) {
                gridView.getThis().post(new Runnable() {
                    @Override
                    public void run() {
                        gridView.getThis().smoothScrollToPosition(0);
                    }
                });
                ivMoveToPosition0.setVisibility(View.GONE);
            }
        }

        public void refresh() {
            if (adapter.isCategoryFiltered() || adapter.isStateTypeFiltered())
                ivResetFilter.setVisibility(View.VISIBLE);
            else
                ivResetFilter.setVisibility(View.GONE);

            if (gridView.getThis().getFirstVisiblePosition() == 0)
                ivMoveToPosition0.setVisibility(View.GONE);
            else
                ivMoveToPosition0.setVisibility(View.VISIBLE);
        }
    }

   abstract class ChangeStateDialog extends MyButtonsDialog {

        public ChangeStateDialog(final Context context, final List<ItemRecord> itemRecords) {
            super(context, "상태 변경", "선택하신 상품의 상태를 모두 변경합니다");
            for (final ItemStateType stateType : ItemStateType.values()) {
                addButton(String.format("%s로 변경", stateType.toUIStr()), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // mPOS의 경우에는 판매완료 = 삭제이므로
                        if (MyApp.get(context) == MyApp.Warspear && stateType == ItemStateType.SOLDOUT) {
                            new MyAlertDialog(context, "판매완료로 전환", "판매완료로 변경하면 목록에서 사라집니다. 전환하시겠습니까?") {
                                @Override
                                public void yes() {
                                    dismiss();
                                    changeStates(itemRecords, stateType);
                                }
                            };
                        } else {
                            dismiss();
                            changeStates(itemRecords, stateType);
                        }
                    }
                });
            }
            show();
        }

        private void changeStates(final List<ItemRecord> itemRecords, final ItemStateType itemState) {
            final ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("변경 중...");
            progressDialog.show();

            new MyServiceTask(context) {

                @Override
                public void doInBackground() throws MyServiceFailureException {
                    Set<Integer> itemIds = new HashSet<>();
                    for (ItemRecord itemRecord : itemRecords)
                        itemIds.add(itemRecord.itemId);
                    service.updateItemState(userSettingData.getUserShopInfo(), itemIds, itemState);
                    for (ItemRecord itemRecord : itemRecords) {
                        itemRecord.state = itemState;
                        ItemDB.getInstance(context).update(itemRecord.id, itemRecord);
                    }
                }

                @Override
                public void onPostExecutionUI() {
                    progressDialog.dismiss();
                    onChangeFinished();
                }

                @Override
                public void catchException(MyServiceFailureException e) {
                    super.catchException(e);
                    progressDialog.dismiss();
                    MyUI.toastSHORT(context, String.format("변경 중 오류가 발생했습니다"));
                }
            };
        }

        abstract protected void onChangeFinished();
    }

}
