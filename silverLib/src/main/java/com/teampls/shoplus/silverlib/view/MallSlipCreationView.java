package com.teampls.shoplus.silverlib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;

import com.teampls.shoplus.lib.adapter.BaseSlipItemDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.composition.BaseCreationComposition;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MAccountDB;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.MyMultilinesDialog;
import com.teampls.shoplus.lib.enums.AutoMessageRequestTimeConfigurationType;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.enums.PaymentMethod;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotOrderData;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderDeliveryType;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderState;
import com.teampls.shoplus.lib.services.chatbot.enums.OrderRejectReason;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotOrderDataProtocol;
import com.teampls.shoplus.lib.view.CounterpartTransView;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.viewSetting.ReceiptSettingView;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.common.SilverAppWatcher;
import com.teampls.shoplus.silverlib.common.SilverAwsService;
import com.teampls.shoplus.silverlib.database.MallOrderDB;
import com.teampls.shoplus.silverlib.view_silver.SilverMainViewPager;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import static com.teampls.shoplus.lib.database_global.GlobalDB.myDeliveryCost;

/**
 * Created by Medivh on 2019-02-12.
 */

public class MallSlipCreationView extends BaseActivity {
    private MallSlipCreationComposition slipCreationComposition;
    private static ChatbotOrderData orderData;
    private static boolean doShowOriginal = false;

    public static void startActivity(Context context, ChatbotOrderData orderData) {
        doShowOriginal = false;
        MallSlipCreationView.orderData = orderData;
        ((Activity) context).startActivityForResult(new Intent(context, MallSlipCreationView.class), BaseGlobal.RequestRefresh);
    }

    public static void startActivityOriginal(Context context, ChatbotOrderData orderData) {
        doShowOriginal = true;
        MallSlipCreationView.orderData = orderData;
        ((Activity) context).startActivityForResult(new Intent(context, MallSlipCreationView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        doFinishForResult();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SilverAppWatcher.checkInstanceAndRestart(this) == false) return;
        slipCreationComposition = new MallSlipCreationComposition(this, orderData, MallOrderDB.getInstance(context));
    }

    @Override
    public int getThisView() {
        return BaseCreationComposition.thisView;
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (slipCreationComposition != null)
            slipCreationComposition.refresh(); // 수정 내용 반영
    }

    class MallSlipCreationComposition extends BaseCreationComposition {
        private ChatbotOrderData orderData;
        private MallOrderDB orderDB;

        public MallSlipCreationComposition(BaseActivity activity, ChatbotOrderData orderData, MallOrderDB orderDB) {
            super(activity);
            myActionBar.hide();
            this.orderData = orderData;
            this.orderDB = orderDB;
            refresh();
            refreshBySettings();

            counterpart = userDB.getRecord(orderData.getBuyerPhoneNumber());
            refreshCounterpart(counterpart);
            downloadAccount(counterpart);

            // manual 버튼만 사용 (택배비.. 등등 추가)
            // setting 버튼은 바로 영수증 셋팅으로 이동
            // 동작하는 버튼 : 취소, 거절, 적용, 수동추가, 거래처이름 (이것도 제한), 요약
            setVisibility(View.GONE, R.id.slip_creation_function, R.id.slip_creation_date_container,
                    R.id.slip_creation_addByName, R.id.slip_creation_addByList,
                    R.id.slip_creation_addBySerial, R.id.slip_creation_addByQR,
                    R.id.slip_creation_addFromDalaran, R.id.slip_creation_print,
                    R.id.slip_creation_setting, R.id.slip_creation_memo,
                    R.id.slip_creation_addDelivery
            );
            setVisibility(View.VISIBLE, R.id.slip_creation_changeAll, R.id.slip_creation_refresh);

            // 취소, 거절, 완료건은 조회만 가능해야
            switch (orderData.getState()) {
                case CANCELLED:
                case REJECTED:
                    BaseUtils.setTextCancelled(true, tvSummary);
                    setVisibility(View.GONE, R.id.slip_creation_apply, R.id.slip_creation_delete, R.id.slip_creation_function,
                            R.id.slip_creation_addSlipitem, R.id.slip_creation_addSlipitem_bar, R.id.slip_creation_addContainer);
                    break;
                case FINISHED:
                    setVisibility(View.GONE, R.id.slip_creation_apply, R.id.slip_creation_delete, R.id.slip_creation_function,
                            R.id.slip_creation_addSlipitem, R.id.slip_creation_addSlipitem_bar, R.id.slip_creation_addContainer);
                    break;
                default:
                    setVisibility(View.GONE, R.id.slip_creation_function);
                    ((Button) findViewById(R.id.slip_creation_delete)).setText("거  절"); // 삭제는 없고 거절만 가능
                    break;
            }
            showMemo();
        }

        private List<String> getMemoStrings() {
            List<String> memoStrings = new ArrayList<>();
            if (orderData.getState().isCancelled())
                memoStrings.add(String.format("%s (%s)", orderData.getState().getUiStr(), orderData.getRejectionReason().getUiStr())); // 취소 및 취소 사유

            if (orderData.getDeliveryType() == ChatbotOrderDeliveryType.POST) {
                memoStrings.add(String.format("배송방법 : %s (%s)", orderData.getDeliveryType().getUiStr(), orderData.getAddress()));
            } else {
                memoStrings.add(String.format("배송방법 : %s", orderData.getDeliveryType().getUiStr()));
            }

            if (orderData.getMemo().isEmpty() == false)
                memoStrings.add(orderData.getMemo());
            return memoStrings;
        }

        private void showMemo() {
            List<String> memoStrings = getMemoStrings();
            int memoStringSize = memoStrings.size();
            for (String memoString : memoStrings) {
                String[] lines = memoString.split("\r\n|\r|\n");
                if (lines.length >= 2)
                    memoStringSize = lines.length - 1;
            }
            if (memoStringSize > tvMemo.getMaxLines())
                tvMemoMore.setVisibility(View.VISIBLE);

            if (memoStrings.size() >= 1) {
                tvMemo.setVisibility(View.VISIBLE);
                tvMemo.setText(TextUtils.join("\n", memoStrings));
            } else {
                tvMemo.setVisibility(View.GONE);
            }
        }

        private List<SlipItemRecord> getSlipItemRecords() {
            if (doShowOriginal)
                return orderData.getEditingItems(new ArrayList<SlipItemRecord>());
            else
                return orderDB.getItems(orderData.getOrderId());
        }

        @Override
        protected BaseSlipItemDBAdapter setAdapter() {
            adapter = new MallSlipItemDBAdapter(context);
            return adapter;
        }

        @Override
        protected void refresh() {
            refreshTitles(getSlipSummaryRecord(), "");
            // Adapter
            adapter.setRecords(getSlipItemRecords());
            adapter.notifyDataSetChanged();
            myEmptyGuide.refresh();
        }

        @Override
        protected void refreshBySettings() {
            super.refreshBySettings();
            setVisibility(View.VISIBLE, R.id.slip_creation_addPercentDC);
        }

        protected SlipItemRecord createNewSlipItem(String name, int quantity, int unitPrice, SlipItemType type) {
            SlipItemRecord slipItemRecord = new SlipItemRecord();
            slipItemRecord.slipKey = orderData.getOrderId();
            slipItemRecord.salesDateTime = orderData.getCreatedDateTime(); // serial은 추가할때 알아서
            slipItemRecord.name = name;
            slipItemRecord.quantity = quantity;
            slipItemRecord.unitPrice = unitPrice;
            slipItemRecord.slipItemType = type;
            return slipItemRecord;
        }

        @Override
        protected void addNewSlip(SlipItemRecord newSlipItemRecord) {
            orderDB.addItem(orderData.getOrderId(), newSlipItemRecord);
            refresh();
            scrollListView(MyView.BOTTOM);
        }

        @Override
        protected SlipSummaryRecord getSlipSummaryRecord() {
            return new SlipSummaryRecord(getSlipItemRecords(), globalDB.getNoQuantityKeywords());
        }


        // Header
        @Override
        protected void onCounterpartClick() {
            CounterpartDialog counterpartDialog = new CounterpartDialog(context, counterpart);
            counterpartDialog.hideFunction();
            counterpartDialog.show();
        }

        @Override
        protected void onChangeBuyerClick(UserRecord newBuyer) {
            // N.A
        }

//        @Override
//        protected void startTransView(UserRecord userRecord, MyOnTask onCopyClick) {
//            CounterpartTransView.startActivity(context, userRecord, onCopyClick);
//        }

        @Override
        protected void addTransFromPrevs(SlipFullRecord prevFullRecord) {
            MyUI.toastSHORT(context, String.format("이 기능은 지원하지 않습니다"));
        }

        @Override
        protected void onSummaryClick() {
            new MyMultilinesDialog(context, "금액기준 요약", getSlipSummaryRecord().toStringListSignSum()).show();
        }

        @Override
        protected void onSettingClick() {
            new MyMultilinesDialog(context, "고객 전달사항", getMemoStrings()).show();
        }

        // Body
        @Override
        protected void onTabTypeClick() {
            // N.A
        }

        @Override
        protected void onTabNameClick() {
            // N.A
        }

        // Scroll menus
        @Override
        protected void onAddByNameClick(ItemSearchType itemSearchType) {
            onAddByManualClick();
        }

        @Override
        protected void onAddByListClick() {
            // N.A
        }

        @Override
        protected void onAddByManualClick() {
            new AddInvoiceDialog(context, new MyOnTask<SlipItemRecord>() {
                @Override
                public void onTaskDone(SlipItemRecord slipItemRecord) {
                    addNewSlip(slipItemRecord);
                }
            });
        }

        @Override
        protected void onAddByQRClick() {
            // N.A
        }

        @Override
        protected void onWithdrawDepositClick() {
            SlipSummaryRecord slipSummaryRecord = getSlipSummaryRecord();
            AccountRecord accountRecord = MAccountDB.getInstance(context).getRecordByKey(counterpart.phoneNumber);

            new WithdrawDepositDialog(context, slipSummaryRecord.amount, accountRecord.deposit, slipSummaryRecord.depositSum) {
                @Override
                public void onApplyClick(int withdrawValue) {
                    SlipItemRecord slipItemRecord = createNewSlipItem("현매입금에서 빼기", 1, withdrawValue, SlipItemType.WITHDRAWAL);
                    addNewSlip(slipItemRecord);
                }
            };
        }

        @Override
        protected void onAddDCClick() {
            new DiscountDialog(context) {
                @Override
                public void onApplyClick(int unitPrice, int quantity) {
                    SlipItemRecord slipItemRecord = createNewSlipItem("단가 DC", quantity, unitPrice, SlipItemType.DISCOUNT);
                    addNewSlip(slipItemRecord);
                }
            };
        }

        @Override
        protected void onAddPercentDCClick() {
            SlipSummaryRecord slipSummaryRecord = getSlipSummaryRecord();
            new PercentDiscountDialog(context, slipSummaryRecord.amount) {
                @Override
                public void onApplyClick(final int percent, int amount, boolean isUnitPriceMode, final int basicUnit) {
                    if (isUnitPriceMode == false) {
                        SlipItemRecord slipItemRecord = createNewSlipItem(String.format("%d%% 할인", percent), 1, amount, SlipItemType.DISCOUNT);
                        addNewSlip(slipItemRecord);
                    } else {
                        String message = String.format("모든 단가를 %d%% 할인하시겠습니까?\n\n단가 단위 : %s", percent, BaseUtils.toCurrencyStr(basicUnit));
                        new MyAlertDialog(context, "판매 단가 할인", message) {
                            @Override
                            public void yes() {
                                List<SlipItemRecord> newRecords = new ArrayList<>();
                                for (SlipItemRecord slipItemRecord : orderDB.getItems(orderData.getOrderId())) {
                                    switch (slipItemRecord.slipItemType) {
                                        default:
                                            slipItemRecord.unitPrice = BaseUtils.round(slipItemRecord.unitPrice * (100 - percent) / 100, basicUnit);
                                            newRecords.add(slipItemRecord);
                                            break;
                                    }
                                }
                                orderDB.updateItems(orderData.getOrderId(), newRecords);
                                refresh();
                                scrollListView(MyView.BOTTOM);
                                MyUI.toastSHORT(context, String.format("단가 할인"));
                            }
                        };
                    }
                }
            };
        }

        @Override
        protected void onAddFromDalaranClick() {
            // N.A
        }

        protected void setSlipItemTypeAs(SlipItemType slipItemType) {
            List<SlipItemRecord> newRecords = new ArrayList<>();
            int position = -1;
            for (SlipItemRecord record : adapter.getRecords()) {
                boolean doChangeType = true;
                position++;
                if (record.slipItemType.familyType == SlipItemType.FamilyType.None)
                    doChangeType = false;

                switch (choiceMode) {
                    default:
                        break;
                    case Multiple:
                        if (adapter.getClickedPositions().contains(position) == false)
                            doChangeType = false;
                        break;
                }
                if (doChangeType)
                    record.slipItemType = slipItemType;
                newRecords.add(record);
            }
            orderDB.updateItems(orderData.getOrderId(), newRecords); // full set이 필요
            refresh();
        }

        @Override
        protected void onRefreshClick() {
            new MyAlertDialog(context, "최초 주문 상태로", "수정한 내용을 모두 버리고 고객이 주문했던 처음 상태로 돌아갑니다") {
                @Override
                public void yes() {
                    orderDB.deleteItems(orderData.getOrderId());
                    refresh();
                }
            };
        }

        @Override
        protected void onPrintClick() {
            // N.A
        }

        @Override
        protected void onScrollSettingClick() {
            ReceiptSettingView.startActivity(context);
        }


        // Buttons
        @Override
        protected void onApplyClick() {
            final SlipSummaryRecord slipSummaryRecord = getSlipSummaryRecord();
            new MallPaymentDialog(context, slipSummaryRecord) {
                @Override
                protected void onApplyClick(PaymentMethod paymentMethod) {
                    int onlinePayment = 0;
                    int entrustPayment = 0;
                    switch (paymentMethod) {
                        case Cash: // 오류임
                            break;
                        case Entrust:
                            entrustPayment = slipSummaryRecord.amount;
                            break;
                        case Online:
                            onlinePayment = slipSummaryRecord.amount;
                            break;
                    }

                    int billValue = 0;
                    if (BaseUtils.isValidPhoneNumber(orderData.getBuyerPhoneNumber())) {
                        new AutoMessageDialog(context, onlinePayment, entrustPayment, billValue).show();
                    } else {
                        postTransaction(onlinePayment, entrustPayment, billValue, AutoMessageRequestTimeConfigurationType.DO_NOT_SEND);
                        MyUI.toastSHORT(context, String.format("알림톡 생략 (임시번호 고객)"));
                    }
                    dismiss();
                }
            };
        }

        @Override
        protected void onDeleteClick() { // 여기서는 변경으로 표시됨
            final MyButtonsDialog onModifyClickDialog = new MyButtonsDialog(context, "주문서 거절", "");

            onModifyClickDialog.addButton("주문 거절 (재고부족)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rejectOrder(onModifyClickDialog, OrderRejectReason.OUT_OF_STOCK);
                }
            });
            onModifyClickDialog.addButton("주문 거절 (물류문제)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rejectOrder(onModifyClickDialog, OrderRejectReason.DELIVERY_PROBLEM);
                }
            });
            onModifyClickDialog.addButton("주문 거절 (매장사정)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rejectOrder(onModifyClickDialog, OrderRejectReason.INTERNAL_PROBLEM);
                }
            });
            onModifyClickDialog.addButton("주문 거절 (기타사유)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rejectOrder(onModifyClickDialog, OrderRejectReason.MISC);
                }
            });

            onModifyClickDialog.show();
        }

        private void rejectOrder(final MyButtonsDialog onModifyClickDialog, final OrderRejectReason orderRejectReason) {
            new MyAlertDialog(context, "주문 거절", "고객님께 주문을 거절하고 그 이유를 보내드리겠습니까?") {
                @Override
                public void yes() {
                    new MyServiceTask(context, "rejectOrder") {
                        private Pair<Boolean, ChatbotOrderDataProtocol> pair;

                        @Override
                        public void doInBackground() throws MyServiceFailureException {
                            pair = chatbotService.rejectOrder(userSettingData.getUserShopInfo(), orderData.getOrderId(), orderRejectReason);
                        }

                        @Override
                        public void onPostExecutionUI() {
                            onModifyClickDialog.dismiss();
                            if (pair.first == false)  //고객이 주문을 취소했거나 이미 완료/취소
                                MyUI.toastSHORT(context, String.format("고객이 취소했거나 완료된 주문입니다"));
                            orderDB.update(new ChatbotOrderData(pair.second));
                            finish(); // 종료
                        }
                    };
                }
            };
        }

        @Override
        protected void onFunctionClick() {
            // N.A 공유 버튼
        }

        @Override
        protected void postTransaction(int onlinePayment, int entrustPayment, int billValue, AutoMessageRequestTimeConfigurationType autoMessage) {
            if (onUploading.isOnTasking()) {
                MyUI.toastSHORT(context, String.format("서버에 전송중입니다. 잠시만 기다려주세요"));
                if (retryCount >= 3)
                    onUploading = TaskState.beforeTasking; //  무한 루프 방지
                return;
            }
            onUploading = TaskState.onTasking;
            retryCount = 0;

            // 시간을 재지정 해야
            SilverAwsService.uploadSlipAndStocksFromOrder(context, orderData, onlinePayment, entrustPayment, billValue,
                    autoMessage, new MyOnAsync<SlipFullRecord>() {
                        @Override
                        public void onSuccess(SlipFullRecord newFullRecord) {
                            onUploading = TaskState.afterTasking;

                            // 7일 이전 기록을 만들면 리스트에서 안 보이므로 경고를 해줘야 한다
                            if (globalDB.getCreatedDateTime(userSettingData).isBefore(DateTime.now().minusDays(7)))
                                MyUI.toastSHORT(context, String.format("일주일 이전 기록이므로 거래내역에서만 확인하실 수 있습니다 "));

                            MallSlipSharingView.startActivityNewRecord(context, newFullRecord, orderData);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();
                        }

                        @Override
                        public void onError(Exception e) {
                            onUploading = TaskState.afterTasking;
                        }
                    }, new MyOnAsync() {
                        @Override
                        public void onSuccess(Object result) {
                            refreshAfterCreation();
                            MyUI.toastSHORT(context, String.format("재고에 반영"));
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });

        }

        protected void refreshAfterCreation() {
            if (SilverMainViewPager.adapter != null)
                SilverMainViewPager.adapter.refreshFragments();
        }

        @Override
        protected void setCounterpart(String phoneNumber) {
            // N.A
        }


        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            if (orderData.getState().isCancelled()) {
                MyUI.toastSHORT(context, String.format("취소건은 수정할 수 없습니다"));
                return;
            }
            if (orderData.getState() == ChatbotOrderState.FINISHED) {
                MyUI.toastSHORT(context, String.format("완료된 건은 수정할 수 없습니다"));
                return;
            }

            switch (choiceMode) {
                case Multiple:
                    adapter.onItemClick(view, position);
                    return;
            }

            final SlipItemRecord slipItemRecord = adapter.getRecord(position);
            switch (slipItemRecord.slipItemType) {
                default:
                    MallOrderItemReview.startActivity(context, slipItemRecord, new MyOnTask<Pair<DbActionType, SlipItemRecord>>() {
                        @Override
                        public void onTaskDone(Pair<DbActionType, SlipItemRecord> result) {
                            switch (result.first) {
                                default:
                                    break;
                                case SPLIT:
                                    SlipItemRecord currentRecord = result.second;
                                    SlipItemRecord newRecord = currentRecord.clone();
                                    newRecord.quantity = currentRecord.quantity - result.first.count;
                                    currentRecord.quantity = result.first.count;

                                    List<SlipItemRecord> itemRecords = adapter.getRecords();
                                    itemRecords.remove(position);
                                    itemRecords.add(position, newRecord);
                                    itemRecords.add(position, currentRecord);

                                    orderDB.updateItems(orderData.getOrderId(), itemRecords);
                                    break;
                                case UPDATE:
                                    orderDB.updateItem(orderData.getOrderId(), adapter.getRecords(), position, result.second);
                                    break;
                                case DELETE:
                                    orderDB.deleteItem(orderData.getOrderId(), adapter.getRecords(), position);
                                    break;
                            }
                            refresh();
                        }
                    });
                    break;
                case DISCOUNT:
                case DEPOSIT:
                    deleteItem(slipItemRecord, position);
                    break;
            }
        }

        private void deleteItem(final SlipItemRecord slipItemRecord, final int position) {
            new MyAlertDialog(context, "항목삭제", String.format("%s를 삭제하시겠습니까?", slipItemRecord.name)) {
                @Override
                public void yes() {
                    orderDB.deleteItem(orderData.getOrderId(), adapter.getRecords(), position);
                    refresh();
                }
            };
        }

        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            return false;
        }

        class AddInvoiceDialog extends BaseDialog {
            private RadioButton rbDelivery, rbTaxInvoice;
            private MyCurrencyEditText etInvoice;
            private MyOnTask<SlipItemRecord> onTask;

            public AddInvoiceDialog(Context context, MyOnTask<SlipItemRecord> onTask) {
                super(context);
                setDialogWidth(0.8, 0.6);
                this.onTask = onTask;
                rbDelivery = findViewById(R.id.dialog_add_invoice_delivery);
                rbTaxInvoice = findViewById(R.id.dialog_add_invoice_taxinvoice);
                rbDelivery.setChecked(true);
                etInvoice = new MyCurrencyEditText(context, getView(), R.id.dialog_add_invoice_edittext, R.id.dialog_add_invoice_edittext_clear);
                etInvoice.setOriginalValue(myDeliveryCost.get(context));
                etInvoice.showKeyboard();
                MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_add_invoice_edittext_title, R.id.dialog_add_invoice_title);
                setOnClick(R.id.dialog_add_invoice_close, R.id.dialog_add_invoice_cancel, R.id.dialog_add_invoice_apply,
                        R.id.dialog_add_invoice_delivery, R.id.dialog_add_invoice_taxinvoice);
                show();
            }

            @Override
            public int getThisView() {
                return R.layout.dialog_add_invoice;
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.dialog_add_invoice_close || view.getId() == R.id.dialog_add_invoice_cancel) {
                    dismiss();
                } else if (view.getId() == R.id.dialog_add_invoice_apply) {
                    // PosSlipItemAddition.startAcitiviyInsert 역할
                    String name = "";
                    if (rbDelivery.isChecked()) {
                        myDeliveryCost.put(context, etInvoice.getValue());
                        name = rbDelivery.getText().toString();
                    } else {
                        name = rbTaxInvoice.getText().toString();
                    }
                    SlipItemRecord slipItemRecord = createNewSlipItem(name, 1, etInvoice.getValue(), SlipItemType.SALES);
                    if (onTask != null)
                        onTask.onTaskDone(slipItemRecord);
                    dismiss();


                } else if (view.getId() == R.id.dialog_add_invoice_taxinvoice) {
                    etInvoice.setOriginalValue(0);

                } else if (view.getId() == R.id.dialog_add_invoice_delivery) {
                    etInvoice.setOriginalValue(myDeliveryCost.get(context));
                }

            }
        }

        protected List<SlipItemType> getSlipItemTypes() {
            SlipItemType[] typeArray = new SlipItemType[]{SlipItemType.SALES,
                    SlipItemType.ADVANCE, SlipItemType.PREORDER, SlipItemType.PREORDER_OUT_OF_STOCK, SlipItemType.SAMPLE};
            return BaseUtils.toList(typeArray);
        }

        class MallSlipItemDBAdapter extends BaseSlipItemDBAdapter {

            public MallSlipItemDBAdapter(Context context) {
                super(context);
            }

            @Override
            public void generate() {
            }

            @Override
            public BaseViewHolder getViewHolder() {
                return new SlipCreationViewHolder();
            }
        }

        abstract class MallPaymentDialog extends MyButtonsDialog {

            public MallPaymentDialog(final Context context, final SlipSummaryRecord slipSummaryRecord) {
                super(context, "결제 방법", String.format("영수증 대금 %s은 어떻게 받기로 하셨습니까?", BaseUtils.toCurrencyStr(slipSummaryRecord.amount)));
                addButton("온라인 입금", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onApplyClick(PaymentMethod.Online);
                    }
                });
                addButton("삼촌 대납", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onApplyClick(PaymentMethod.Entrust);
                    }
                });
                addButton("나중에 매장와서", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onApplyClick(PaymentMethod.Entrust);
                    }
                });
                show();
            }

            abstract protected void onApplyClick(PaymentMethod paymentMethod);

        }

    }

}
