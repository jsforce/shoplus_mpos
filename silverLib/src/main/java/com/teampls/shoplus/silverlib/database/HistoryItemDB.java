package com.teampls.shoplus.silverlib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database.BaseDB;
/**
 * Created by Medivh on 2019-07-03.
 */

public class HistoryItemDB extends BaseDB<HistoryItemRecord> {
    private static HistoryItemDB instance;

    public static HistoryItemDB getInstance(Context context) {
        if (instance == null)
            instance = new HistoryItemDB(context);
        return instance;
    }

    public enum Column {
        _id, itemId, name;

        public static String[] toStrs() {
            return BaseUtils.toStrs(Column.values());
        }
    }

    private HistoryItemDB(Context context) {
        super(context, "ItemSearchHistoryDB", "History", 3, Column.toStrs());
        cleanUpBySize(10);
    }

    @Override
    protected int getId(HistoryItemRecord record) {
        return record.id;
    }

    @Override
    protected HistoryItemRecord getEmptyRecord() {
        return new HistoryItemRecord();
    }

    @Override
    protected HistoryItemRecord createRecord(Cursor cursor) {
        return new HistoryItemRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getInt(Column.itemId.ordinal()),
                cursor.getString(Column.name.ordinal())
        );
    }

    @Override
    protected ContentValues toContentvalues(HistoryItemRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.itemId.name(), record.itemId);
        contentvalues.put(Column.name.name(), record.name);
        return contentvalues;
    }

    public void toLogCat(String callLocation) {
        for (HistoryItemRecord record : getRecords())
            record.toLogCat(callLocation);
    }

    public boolean has(int itemId) {
        for (HistoryItemRecord record : getRecords()) {
            if (record.itemId == itemId)
                return true;
        }
        return false;
    }

    public void deleteBy(int itemId) {
        for (HistoryItemRecord record : getRecords()) {
            if (record.itemId == itemId)
                delete(record.id);
        }
    }
}