package com.teampls.shoplus.silverlib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.view.SimplePosSlipSharingView;
import com.teampls.shoplus.lib.view_base.BasePosSlipSharingView;
import com.teampls.shoplus.silverlib.R;

/**
 * Created by Medivh on 2017-10-19.
 */

public class PosSlipPreview extends BasePosSlipSharingView {

    public static void startActivity(Context context, SlipFullRecord slipFullRecord) {
        PosSlipPreview.slipFullRecord = slipFullRecord;
        BasePosSlipSharingView.itemOptionRecord = new ItemOptionRecord();
        actionType = DbActionType.READ;
        ((Activity) context).startActivityForResult(new Intent(context, PosSlipPreview.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        setVisibility(View.GONE, R.id.slip_sharing_createNewSlip, R.id.slip_sharing_to_exodar, R.id.slip_sharing_to_dalaran);
    }

    @Override
    protected void refreshView() {
        super.refreshView();
        tvDate.setText("== 작성중인 영수증 입니다. 거래 정보만 출력합니다 ==");
        setVisibility(View.GONE, tvBill, tvPayment, tvPhone1, tvPhone2, tvLocation,
                tvAddress, tvAccount, tvMemo, tvKey, tvDeposit, tvCounterAddress);
    }

    @Override
    public void onCreateNewSlipClick() {
    }

    @Override
    protected void cancelSlip(boolean doCopy) {
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.clear();
        myActionBar
                .add(MyMenu.basicInfo)
                .add(MyMenu.sendKakao)
                .add(MyMenu.shareFile)
                .add(MyMenu.printSetting)
                .add(MyMenu.close)
        ;
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        super.onMyMenuSelected(myMenu);
    }

    protected void printSlip() {
        new MyAlertDialog(context, "작성중 영수증 출력", "상호 확인을 위해 작성 중인 영수증을 출력하시겠습니까?") {
            @Override
            public void yes() {
                myPosPrinter.printPreview(slipFullRecord);
            }
        };
    }

}
