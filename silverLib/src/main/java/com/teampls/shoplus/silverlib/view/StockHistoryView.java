package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.ItemDataBundle;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.CategoryDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.MyDateTimeNavigator;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyExcel;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.common.SilverPreference;
import com.teampls.shoplus.silverlib.database.ItemOptionDBAdapter;
import com.teampls.shoplus.silverlib.database.ItemStockRecord;
import com.teampls.shoplus.silverlib.database.MStockHistoryDB;
import com.teampls.shoplus.silverlib.database.OptionStockRecord;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2018-07-15.
 */

public class StockHistoryView extends BaseActivity implements AdapterView.OnItemClickListener {
    private ListView listView;
    private ItemStockAdapter adapter;
    private ItemDB itemDB;
    private MStockHistoryDB stockHistoryDB;
    private List<TextView> tvSumsTitles = new ArrayList<>();
    private TextView tvSortingTitle, tvFilterState, tvFilterCategory, tvFilterName, tvSorting;

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, StockHistoryView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;

        itemDB = ItemDB.getInstance(context);
        stockHistoryDB = MStockHistoryDB.getInstance(context, userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber());
        stockHistoryDB.cleanUpByDays(7);

        adapter = new ItemStockAdapter(context);
        adapter.setSortingKey(MSortingKey.ID);
        listView = (ListView) findViewById(R.id.stock_history_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);

        tvFilterState = findTextViewById(R.id.stock_history_filter_state);
        tvFilterCategory = findTextViewById(R.id.stock_history_filter_category);
        tvFilterName = findTextViewById(R.id.stock_history_filter_name);
        tvSorting = findTextViewById(R.id.stock_history_filter_sorting);

        tvSortingTitle = findTextViewById(R.id.stock_history_sorting);
        tvSumsTitles.add(findTextViewById(R.id.stock_history_latest0, "-"));
        tvSumsTitles.add(findTextViewById(R.id.stock_history_latest1, "-"));
        tvSumsTitles.add(findTextViewById(R.id.stock_history_latest2, "-"));
        tvSumsTitles.add(findTextViewById(R.id.stock_history_latest3, "-"));
        tvSumsTitles.add(findTextViewById(R.id.stock_history_latest4, "-"));

        setOnClick(R.id.stock_history_sorting, R.id.stock_history_filter_state,
                R.id.stock_history_filter_category, R.id.stock_history_filter_name,
                R.id.stock_history_filter_sorting);

        onCreateAsync();

        for (final TextView tvSortingTitle : tvSumsTitles)
            tvSortingTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tvSortingTitle.getTag() instanceof DateTime == false) {
                        MyUI.toastSHORT(context, String.format("오류가 있습니다"));
                        return;
                    }

                    final DateTime dateTime = (DateTime) tvSortingTitle.getTag();
                    new MyAlertDialog(context, "기록삭제", String.format("%s 기록을 삭제하시겠습니까?", dateTime.toString(BaseUtils.MDHm_Format))) {
                        @Override
                        public void yes() {
                            stockHistoryDB.deleteAll(dateTime);
                            refresh();
                        }
                    };
                }
            });
    }

    private void onCreateAsync() {
        new MyAsyncTask(context, "", "저장 정보 확인중...") {

            @Override
            public void doInBackground() {
                stockHistoryDB.refreshFromFileDB();
            }

            @Override
            public void onPostExecutionUI() {
                List<DateTime> dateTimes = stockHistoryDB.getSortedDateTimes();
                if (dateTimes.size() >= 1)
                    refresh();
                else
                    snapshotItemStock();
            }
        };
    }

    @Override
    public int getThisView() {
        return R.layout.activity_stock_history;
    }

    public void refresh() {
        if (adapter == null) return;
        new MyAsyncTask(context, "StockHistoryView") {
            @Override
            public void doInBackground() {
                adapter.generate();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
                refreshTitles();
            }
        };
    }

    public void refreshTitles() {
        List<DateTime> dateTimes = stockHistoryDB.getSortedDateTimes();
        if (dateTimes.size() >= 1) {
            List<Integer> itemIds = new ArrayList<>();
            for (ItemStockRecord record : adapter.getRecords())
                itemIds.add(record.itemId);
            myActionBar.setTitle(String.format("%d종 %d개", adapter.getCount(), stockHistoryDB.getStockSum(dateTimes.get(0), itemIds)));
            myActionBar.setSubTitle("보존기간 : 1주일");
        } else {
            myActionBar.setTitle(String.format(""));
            myActionBar.setSubTitle("");
        }

        for (TextView tvSumsTitle : tvSumsTitles)
            tvSumsTitle.setText("-");

        for (int index = 0; index < Math.min(dateTimes.size(), tvSumsTitles.size()); index++) {
            DateTime dateTime = dateTimes.get(index);
            tvSumsTitles.get(index).setText(String.format("%s\n%s", dateTime.toString(BaseUtils.MD_FixedSize_Format), dateTime.toString(BaseUtils.HS_Fixed_Format)));
            tvSumsTitles.get(index).setTag(dateTime);
        }

        tvSortingTitle.setTextColor(ColorType.Black.colorInt);
        switch (adapter.sortingKey) {
            case Name:
                tvSortingTitle.setText("이 름 순");
                tvSorting.setText("이름순");
                break;
            case ID:
                tvSortingTitle.setText("최 신 순");
                tvSorting.setText("최신순");
                break;
            case Count:
                tvSortingTitle.setText("재 고 순");
                tvSorting.setText("재고순");
                break;
        }


        switch (adapter.getFilterCategory()) {
            case ALL:
                tvFilterCategory.setText("분 류");
                tvSorting.setTextColor(ColorType.Black.colorInt);
                break;
            default:
                tvFilterCategory.setText(adapter.getFilterCategory().toUIStr());
                tvSorting.setTextColor(ColorType.Blue.colorInt);
                break;
        }
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.stock_history_filter_state) {
            MyUI.toastSHORT(context, String.format("판매완료를 제외한 상품만 조사합니다"));

        } else if (view.getId() == R.id.stock_history_filter_category) {
            new CategoryDialog(context, "분류 선택", itemDB.getRecords(), false, false, true) {
                @Override
                public void onItemClicked(ItemCategoryType category) {
                    StockHistoryView.this.adapter.filterCategory(category);
                    StockHistoryView.this.refresh();
                }
            };

        } else if (view.getId() == R.id.stock_history_filter_name) {
            MyUI.toastSHORT(context, String.format("지원 예정입니다"));

        } else if (view.getId() == R.id.stock_history_sorting || view.getId() == R.id.stock_history_filter_sorting) {
            // set next sorting key
            switch (adapter.sortingKey) {
                case ID:
                    adapter.setSortingKey(MSortingKey.Name);
                    refresh();
                    break;
                case Name:
                    adapter.setSortingKey(MSortingKey.Count);
                    refresh();
                    break;
                case Count:
                    adapter.setSortingKey(MSortingKey.ID);
                    refresh();
                    break;
            }
        }
    }

    private void refreshStocks() {
        new MyServiceTask(context, "snapshotItemStock", "재고 조사중...") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                ItemDataBundle itemDataBundle = itemService.getItemsWithStock(userSettingData.getUserShopInfo(), false, false);
                itemDB.refresh(itemDataBundle);
            }

            @Override
            public void onPostExecutionUI() {
                snapshotItemStock();
            }
        };
    }

    private void snapshotItemStock() {
        new MyServiceTask(context, "snapshotItemStock", "재고 기록중...") {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                //   Map<ItemStockKey, Integer> stockMap = itemService.getItemStock(userSettingData.getUserShopInfo()); // 판매중, 재고가 양수만 가져옴
                Map<ItemOptionRecord, Integer> optionStockMap = itemDB.options.getStockMap();
                List<OptionStockRecord> records = stockHistoryDB.insertStockMap(optionStockMap); // 그냥 현재 재고를 저장
                stockHistoryDB.saveToFileAsync(records); // 파일부분은 async로 저장중
            }

            @Override
            public void onPostExecutionUI() {
                refresh();
            }

        };
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.clear();
        myActionBar.add(MyMenu.checkStocks)
                .add(MyMenu.sendAsExcel)
                .add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    @Override

    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case refresh:
                refreshStocks();
                break;
            case checkStocks:
                snapshotItemStock();
                break;
            case sendAsExcel:
                new MyAsyncTask(context, "StockHistoryView", "데이터 정리중..") {
                    private String filePath = "";

                    @Override
                    public void doInBackground() {
                        List<ItemStockRecord> records = new ArrayList<>();
                        for (ItemStockRecord record : adapter.getRecords()) {
                            record.stockSums = stockHistoryDB.getStockSums(record.itemId);
                            records.add(record);
                        }
                        setProgressMessage("엑셀파일 생성중");
                        MyExcel myExcel = new MyExcel(context);
                        filePath = myExcel.createStockHistory(stockHistoryDB.getSortedDateTimes(), records, stockHistoryDB.getOptionDateTimeMap());
                    }

                    @Override
                    public void onPostExecutionUI() {
                        if (filePath.isEmpty() == false) {
                            MyUI.toastSHORT(context, String.format("엑셀 파일을 전달할 앱을 선택해 주세요"));
                            MyDevice.shareFile(context, filePath);
                        }
                    }
                };
                break;
            case close:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        new StockDetailDialog(context, adapter.getRecord(position)).show();
    }

    class ItemStockAdapter extends BaseDBAdapter<ItemStockRecord> {
        private boolean doFilterCategory = false;
        private ItemCategoryType category = ItemCategoryType.ALL;

        public ItemStockAdapter(Context context) {
            super(context);
        }

        public void filterCategory(ItemCategoryType category) {
            this.category = category;
            doFilterCategory = (category != ItemCategoryType.ALL);
        }

        public ItemCategoryType getFilterCategory() {
            return category;
        }

        @Override
        public void generate() {
            List<ItemStockRecord> _records = new ArrayList<>();
            for (ItemRecord itemRecord : itemDB.getRecords()) {
                if (doFilterCategory)
                    if (itemRecord.category != category)
                        continue;
                ItemStockRecord _record = new ItemStockRecord(itemRecord);
                _record.sortingKey = sortingKey;
                _records.add(_record);
                _record.stockSum = stockHistoryDB.getLatestStockSum(itemRecord.itemId);
            }
            records = _records;
            Collections.sort(records);
        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new StockHistoryViewHolder();
        }


        class StockHistoryViewHolder extends BaseViewHolder {
            private TextView tvNum, tvName;
            private List<TextView> tvStockSums = new ArrayList<>();

            @Override
            public int getThisView() {
                return R.layout.row_stock_history;
            }

            @Override
            public void init(View view) {
                tvNum = findTextViewById(context, view, R.id.row_stock_history_num);
                tvName = findTextViewById(context, view, R.id.row_stock_history_name);
                tvStockSums.add(findTextViewById(context, view, R.id.row_stock_history_latest0));
                tvStockSums.add(findTextViewById(context, view, R.id.row_stock_history_latest1));
                tvStockSums.add(findTextViewById(context, view, R.id.row_stock_history_latest2));
                tvStockSums.add(findTextViewById(context, view, R.id.row_stock_history_latest3));
                tvStockSums.add(findTextViewById(context, view, R.id.row_stock_history_latest4));
            }

            @Override
            public void update(int position) {
                ItemStockRecord record = getRecord(position);
                tvNum.setText(Integer.toString(position + 1));
                tvName.setText(record.itemName);
                MyMap<DateTime, Integer> stockMap = stockHistoryDB.getStockSums(record.itemId);
                for (TextView textView : tvStockSums)
                    textView.setText("-");
                List<DateTime> dateTimes = stockHistoryDB.getSortedDateTimes();
                for (int index = 0; index < Math.min(dateTimes.size(), tvStockSums.size()); index++) {
                    int stockSum = stockMap.get(dateTimes.get(index));
                    int nextStockSum = (index == dateTimes.size() - 1) ? stockSum : stockMap.get(dateTimes.get(index + 1));
                    tvStockSums.get(index).setText(Integer.toString(stockSum));
                    tvStockSums.get(index).setTextColor(stockSum == nextStockSum ? ColorType.Black.colorInt : ColorType.Blue.colorInt);
                }
            }
        }
    }

    class StockDetailDialog extends BaseDialog {
        private ImageView ivItemImage;
        public ItemOptionDBAdapter optionStockAdapter;
        private String shopPhoneNumber = "";
        private MyDateTimeNavigator myDateTimeNavigator;
        private ListView lvOptions;
        private TextView tvStockSum;
        private ItemStockRecord itemStockRecord = new ItemStockRecord();

        public StockDetailDialog(Context context, ItemStockRecord itemStockRecord) {
            super(context);
            this.itemStockRecord = itemStockRecord;
            findTextViewById(R.id.dialog_stock_detail_title, String.format("%s 재고 기록", itemStockRecord.itemName));
            shopPhoneNumber = userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber();

            tvStockSum = findTextViewById(R.id.dialog_stock_detail_table_stock);
            findViewById(R.id.dialog_stock_detail_table_colorSize);
            ivItemImage = (ImageView) findViewById(R.id.dialog_stock_detail_imageView);
            lvOptions = (ListView) findViewById(R.id.dialog_stock_detail_listview);

            itemStockRecord.stockSums = stockHistoryDB.getStockSums(itemStockRecord.itemId);

            List<DateTime> dateTimes = new ArrayList(itemStockRecord.stockSums.toTreeMap(true).keySet());
            final DateTime dateTime = dateTimes.size() >= 1 ? dateTimes.get(0) : Empty.dateTime;

            optionStockAdapter = new ItemOptionDBAdapter(context);
            optionStockAdapter.setViewType(ItemOptionDBAdapter.StockHistory);
            optionStockAdapter.filterItemRecord(new ItemRecord(itemStockRecord.itemId, itemStockRecord.itemName));
            lvOptions.setAdapter(optionStockAdapter);

            myDateTimeNavigator = new MyDateTimeNavigator(context, dateTime, getView(), R.id.dialog_stock_detail_prev,
                    R.id.dialog_stock_detail_next);
            myDateTimeNavigator.setAsCustomMode(dateTimes, getView(), R.id.dialog_stock_detail_date);
            myDateTimeNavigator.setOnApplyClick(new MyOnClick<DateTime>() {
                @Override
                public void onMyClick(View view, final DateTime dateTime1) {
                    setOnStockHistoryCountShow(dateTime1);
                }
            });

            imageLoader.retrieveThumbAsync(shopPhoneNumber, itemStockRecord.itemId, new MyOnTask<Bitmap>() {
                @Override
                public void onTaskDone(Bitmap bitmap) {
                    if (Empty.isEmpty(bitmap)) {
                        setVisibility(View.GONE, R.id.dialog_stock_detail_imageView);
                    } else {
                        ivItemImage.setImageBitmap(bitmap);
                    }
                }
            });

            setOnClick(R.id.dialog_stock_detail_dismiss, R.id.dialog_stock_detail_close);
            setOnStockHistoryCountShow(dateTime);
        }

        private void setOnStockHistoryCountShow(final DateTime dateTime) {
            optionStockAdapter.setOnStockHistoryCountShow(new MyOnTask<Pair<ItemOptionRecord, TextView>>() {
                @Override
                public void onTaskDone(Pair<ItemOptionRecord, TextView> pair) {
                    OptionStockRecord record = stockHistoryDB.getRecord(pair.first, dateTime);
                    pair.second.setText(Integer.toString(record.stock));
                }
            });
            refresh();
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_stock_detail;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_stock_detail_dismiss ||
                    view.getId() == R.id.dialog_stock_detail_close) {
                dismiss();
            }
        }

        private void refresh() {
            optionStockAdapter.generate();
            optionStockAdapter.notifyDataSetChanged();
            int stockSum = stockHistoryDB.getStockSum(myDateTimeNavigator.getDateTime(), itemStockRecord.itemId);
            tvStockSum.setText(String.format("재고 (%d)", stockSum));
        }
    }

}
