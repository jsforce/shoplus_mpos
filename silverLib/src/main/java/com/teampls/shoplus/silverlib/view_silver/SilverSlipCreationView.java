package com.teampls.shoplus.silverlib.view_silver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseSlipItemDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseAwsService;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyImageLoader;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.composition.BaseCreationComposition;
import com.teampls.shoplus.lib.composition.ItemGridComposition;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database.PosAndSourceLinkRecord;
import com.teampls.shoplus.lib.database_app.DalaranPosSlipDB;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_map.KeyValueInt;
import com.teampls.shoplus.lib.database_memory.MAccountDB;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.MyEditTextDialog;
import com.teampls.shoplus.lib.dialog.MyMultilinesDialog;
import com.teampls.shoplus.lib.dialog.TransConfirmDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.enums.AutoMessageRequestTimeConfigurationType;
import com.teampls.shoplus.lib.enums.DBResultType;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.CounterpartTransView;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_base.BaseModuleView;
import com.teampls.shoplus.silverlib.dialog.HistoryItemDialog;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.lib.common.SelectedItemRecord;
import com.teampls.shoplus.silverlib.common.SilverPreference;
import com.teampls.shoplus.silverlib.database.HistoryItemDB;
import com.teampls.shoplus.silverlib.database.HistoryItemRecord;
import com.teampls.shoplus.silverlib.database.ItemDBAdapter;
import com.teampls.shoplus.silverlib.database.ItemOptionDBAdapter;
import com.teampls.shoplus.silverlib.database.PosSlipItemDBAdapter;
import com.teampls.shoplus.silverlib.dialog.OptionQuantityDialog;
import com.teampls.shoplus.silverlib.view.PosSlipItemAddition;
import com.teampls.shoplus.silverlib.view.PosSlipPreview;
import com.teampls.shoplus.silverlib.view.QrItemScanView;
import com.teampls.shoplus.silverlib.view.QrPosSlipCreationView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-04-15.
 */
// dalaran PosSlipCreationView와 유사하다
abstract public class SilverSlipCreationView extends BaseActivity {
    enum CurrentViewType {Slip, ItemList}

    protected MainView mainView;
    protected ItemsView itemsView;
    protected RetrievedItemView retrievedItemView;
    public static SlipRecord slipRecord = new SlipRecord();
    protected CurrentViewType currentViewType = CurrentViewType.Slip;
    protected PosSlipDB posSlipDB;
    protected ItemDB itemDB;
    protected boolean doBlockRefreshMainLayout = false;
    protected TaskState onUploading = TaskState.beforeTasking;
    protected int retryCount = 0;
    private String KEY_Make_byQR = "slipCreation.make.byQR";
    public KeyValueInt lastSelectedItemId = new KeyValueInt("silver.slip.lastSelected", 0);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.hide();
        posSlipDB = PosSlipDB.getInstance(context);
        itemDB = ItemDB.getInstance(context);

        mainView = new MainView(context, findViewById(R.id.slip_making_slipsContainer));
        itemsView = new ItemsView(context, findViewById(R.id.slip_making_itemsContainer));
        retrievedItemView = new RetrievedItemView(context, findViewById(R.id.slip_making_retrievedItemContainer), getView());

        onCreate();

    }

    @Override
    public int getThisView() {
        return R.layout.activity_slip_creation;
    }

    abstract public void onCreate();

    abstract public void startItemAndOptionSearch(ItemSearchType itemSearchType, MyOnClick<SelectedItemRecord> onTask);

    abstract public void startPosSlipSharingView(SlipFullRecord newFullRecord);

    abstract public void refreshAfterCreation();

    abstract public void startItemFullViewPager(ItemRecord itemRecord, MyOnTask onTask);

    // dalaran PosSlipCreationView와 유사하다
    class MainView extends BaseModuleView {
        private SlipCreationComposition slipCreationComposition;
        private int listViewDirectionOnResume = MyView.NO_DIRECTION;

        public MainView(Context context, View container) {
            super(context, getView(), container);
            slipCreationComposition = new SlipCreationComposition(SilverSlipCreationView.this);
        }

        @Override
        public void onClick(View view) {

        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (slipCreationComposition != null)
                slipCreationComposition.onItemClick(parent, view, position, id); // 사실상 동작은 안함
        }

        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            switch (requestCode) {
                case BaseGlobal.RequestRefresh:
                    refresh();
                    if (slipCreationComposition != null)
                        slipCreationComposition.scrollListView(listViewDirectionOnResume);
                    listViewDirectionOnResume = MyView.NO_DIRECTION;
                    break;
            }
        }

        @Override
        public void refresh() {
            if (slipCreationComposition != null)
                slipCreationComposition.refresh();
        }
    }

    class SlipCreationComposition extends BaseCreationComposition {

        public SlipCreationComposition(Activity activity) {
            super(activity);
            posSlipDB.items.refreshRecordsBySerial(slipRecord.getSlipKey().toSID());
            refresh();
            refreshBySettings();

            counterpart = userDB.getRecord(slipRecord.counterpartPhoneNumber);
            refreshCounterpart(counterpart);
            downloadAccount(counterpart); // 매입금은 알아야
        }

        @Override
        protected BaseSlipItemDBAdapter setAdapter() {
            adapter = new PosSlipItemDBAdapter(context);
            adapter.setViewType(PosSlipItemDBAdapter.SlipCreationView);
            if (SilverPreference.priceByBuyer.getValue(context))
                adapter.highlightUnitPriceByBuyer(true);
            return adapter;
        }

        @Override
        protected void refresh() {

            // 필요 자료
            List<SlipItemRecord> slipItemRecords = posSlipDB.items.getRecordsOrderBySerial(slipRecord.getSlipKey().toSID());
            SlipSummaryRecord slipSummaryRecord = new SlipSummaryRecord(slipItemRecords, globalDB.getNoQuantityKeywords());

            refreshTitles(slipSummaryRecord, "");

            // Adapter
            adapter.setRecords(slipItemRecords);
            adapter.notifyDataSetChanged();
            myEmptyGuide.refresh();
        }

        @Override
        protected SlipItemRecord createNewSlipItem(String name, int quantity, int unitPrice, SlipItemType type) {
            SlipItemRecord slipItemRecord = new SlipItemRecord(slipRecord);
            slipItemRecord.name = name;
            slipItemRecord.quantity = quantity;
            slipItemRecord.unitPrice = unitPrice;
            slipItemRecord.slipItemType = type;
            slipItemRecord.serial = posSlipDB.items.getNextSerial(slipRecord.getSlipKey().toSID());
            return slipItemRecord;
        }

        @Override
        protected void addNewSlip(SlipItemRecord newSlipItemRecord) {
            posSlipDB.items.insert(newSlipItemRecord);
            refresh();
            scrollListView(MyView.BOTTOM);
        }

        @Override
        protected void onCounterpartClick() {
            new CounterpartDialog(context, counterpart).enableAddTax().show();
        }

        @Override
        protected void addTransFromPrevs(SlipFullRecord prevFullRecord) {
            int serial = posSlipDB.items.getNextSerial(slipRecord.getSlipKey().toSID());
            for (SlipItemRecord slipItemRecord : prevFullRecord.items) {
                slipItemRecord.slipKey = slipRecord.getSlipKey().toSID(); // 같은 키로
                slipItemRecord.serial = serial;
                posSlipDB.items.insert(slipItemRecord);
                serial++;
            }
            refresh();
        }

        @Override
        protected void onSummaryClick() {
            new MyMultilinesDialog(context, "금액기준 요약", getSlipSummaryRecord().toStringListSignSum()).show();
        }

        @Override
        protected void onTabTypeClick() {
            posSlipDB.items.sortByTypes(slipRecord.getSlipKey().toSID());
            refresh();
        }

        @Override
        protected void onTabNameClick() {
            posSlipDB.items.sortByName(slipRecord.getSlipKey().toSID());
            refresh();
        }

        @Override
        protected void onAddByNameClick(ItemSearchType itemSearchType) {
            addByNameOrSerialNumClick(ItemSearchType.ByName);
        }

        private void addByNameOrSerialNumClick(ItemSearchType itemSearchType) {
            startItemAndOptionSearch(itemSearchType, new MyOnClick<SelectedItemRecord> () {
                @Override
                public void onMyClick(View view, SelectedItemRecord selectedItemAndOptions) {
                    if (selectedItemAndOptions.itemRecord.itemId >= 1) {
                        // item, option, start item update UI
                        addSlipItemsWithSALES(selectedItemAndOptions, false);
                    } else {
                        // 수동입력
                        SlipItemRecord slipItemRecord = new SlipItemRecord(slipRecord);
                        slipItemRecord.name = selectedItemAndOptions.itemRecord.name;
                        PosSlipItemAddition.startActivityInsert(context, BaseGlobal.RequestRefresh, slipRecord, slipItemRecord, false);
                        // 종료시 onActivityResults 시행
                    }
                }
            });
        }

        @Override
        protected void onAddByListClick() {
            currentViewType = CurrentViewType.ItemList;
            refreshMainLayout();
        }

        @Override
        protected void onAddByManualClick() {
            // 종료시 onActivityResults 시행
            PosSlipItemAddition.startActivityInsert(context, BaseGlobal.RequestRefresh, slipRecord, false);
            listViewDirectionOnResume = MyView.BOTTOM;
        }

        @Override
        protected void onAddByQRClick() {
            if (keyValueDB.getBool(KEY_Make_byQR)) {
                QrPosSlipCreationView.startActivity(context, slipRecord, new MyOnTask<List<SlipItemRecord>>() {
                    @Override
                    public void onTaskDone(List<SlipItemRecord> slipItemRecords) {
                        int serial = posSlipDB.items.getNextSerial(slipRecord.getSlipKey().toSID());
                        for (SlipItemRecord slipItemRecord : slipItemRecords) {
                            slipItemRecord.serial = serial;
                            DBResult dbResult = posSlipDB.items.insert(slipItemRecord);
                            slipItemRecord.id = dbResult.id;
                            serial++;
                        }
                        MyUI.toastSHORT(context, "추가");
                        mainView.listViewDirectionOnResume = MyView.BOTTOM;
                    }
                });
            } else {
                QrItemScanView.startActivity(context, new MyOnTask<ItemRecord>() {
                    @Override
                    public void onTaskDone(ItemRecord itemRecord) {
                        doBlockRefreshMainLayout = true;
                        retrievedItemView.showResult(itemRecord.itemId);
                    }
                });
            }
        }

        protected List<SlipItemType> getSlipItemTypes() {
            SlipItemType[] typeArray = new SlipItemType[]{SlipItemType.SALES, SlipItemType.RETURN,
                    SlipItemType.ADVANCE, SlipItemType.PREORDER, SlipItemType.CONSIGN, SlipItemType.SAMPLE,
                    SlipItemType.EXCHANGE_SALES, SlipItemType.EXCHANGE_RETURN, SlipItemType.EXCHANGE_SALES_PENDING,
                    SlipItemType.EXCHANGE_RETURN_PENDING};
            return BaseUtils.toList(typeArray);
        }

        protected void setSlipItemTypeAs(SlipItemType slipItemType) {
            List<SlipItemRecord> newRecords = new ArrayList<>();
            int position = -1;
            for (SlipItemRecord record : adapter.getRecords()) {
                position++;
                if (record.slipItemType.familyType == SlipItemType.FamilyType.None)
                    continue; // 특수 항목은 제외
                switch (choiceMode) {
                    default:
                        break;
                    case Multiple:
                        if (adapter.getClickedPositions().contains(position) == false)
                            continue; // 미선택 제외
                        break;
                }
                record.slipItemType = slipItemType;
                newRecords.add(record);
            }
            posSlipDB.items.updateOrInsertBySerial(newRecords);
            refresh();
        }

        @Override
        protected void onWithdrawDepositClick() {
            SlipSummaryRecord slipSummaryRecord = getSlipSummaryRecord();
            AccountRecord accountRecord = MAccountDB.getInstance(context).getRecordByKey(counterpart.phoneNumber);

            new WithdrawDepositDialog(context, slipSummaryRecord.amount, accountRecord.deposit, slipSummaryRecord.depositSum) {
                @Override
                public void onApplyClick(int withdrawValue) {
                    SlipItemRecord slipItemRecord = createNewSlipItem("현매입금에서 빼기", 1, withdrawValue, SlipItemType.WITHDRAWAL);
                    addNewSlip(slipItemRecord);
                }
            };
        }

        @Override
        protected void onAddDCClick() {
            new DiscountDialog(context) {
                @Override
                public void onApplyClick(int unitPrice, int quantity) {
                    SlipItemRecord slipItemRecord = createNewSlipItem("단가 DC", quantity, unitPrice, SlipItemType.DISCOUNT);
                    addNewSlip(slipItemRecord);
                }
            };
        }

        protected SlipSummaryRecord getSlipSummaryRecord() {
            final List<SlipItemRecord> items = posSlipDB.items.getRecordsBy(slipRecord.getSlipKey().toSID());
            final SlipSummaryRecord slipSummaryRecord = new SlipSummaryRecord(items, globalDB.getNoQuantityKeywords());
            return slipSummaryRecord;
        }

        @Override
        protected void onAddPercentDCClick() {
            new PercentDiscountDialog(context, getSlipSummaryRecord().amount) {
                @Override
                public void onApplyClick(final int percent, int amount, boolean isUnitPriceMode, final int basicUnit) {
                    if (isUnitPriceMode == false) {
                        SlipItemRecord slipItemRecord = createNewSlipItem(String.format("%d%% 할인", percent), 1, amount, SlipItemType.DISCOUNT);
                        addNewSlip(slipItemRecord);
                    } else {
                        String message = String.format("모든 단가를 %d%% 할인하시겠습니까?\n\n단가 단위 : %s", percent, BaseUtils.toCurrencyStr(basicUnit));

                        new MyAlertDialog(context, "판매 단가 할인", message) {
                            @Override
                            public void yes() {
                                for (SlipItemRecord slipItemRecord : posSlipDB.items.getRecordsBy(slipRecord.getSlipKey().toSID())) {
                                    switch (slipItemRecord.slipItemType) {
                                        default:
                                            slipItemRecord.unitPrice = BaseUtils.round(slipItemRecord.unitPrice * (100 - percent) / 100, basicUnit);
                                            posSlipDB.items.update(slipItemRecord.id, slipItemRecord);
                                            break;
                                    }
                                }
                                MyUI.toastSHORT(context, String.format("단가를 할인했습니다"));
                                refresh();
                            }
                        };
                    }
                }
            };
        }

        @Override
        protected void onAddFromDalaranClick() {
            if (MyDevice.hasPackage(context, MyApp.Dalaran.getPackageName()) == false) {
                new MyAlertDialog(context, "미송잔금 미설치", "미송잔금앱이 설치되어 있지 않습니다. 설치 하시겠습니까?") {
                    @Override
                    public void yes() {
                        MyDevice.openPlayStore(context, MyApp.Dalaran.getPackageName());
                    }
                };
            } else {

                DalaranPosSlipDB dalaranPosSlipDB = DalaranPosSlipDB.getInstance(context);
                if (dalaranPosSlipDB.isValid == false)
                    return;
                if (dalaranPosSlipDB.hasUnconfirmedRecord(slipRecord.counterpartPhoneNumber) == false) {
                    MyUI.toastSHORT(context, String.format("작성중인 %s 기록이 없습니다", counterpart.name));
                    return;
                }

                SlipRecord dalaranSlipRecord = dalaranPosSlipDB.getUnconfirmedRecord(slipRecord.counterpartPhoneNumber);
                List<SlipItemRecord> dalaranSlipItemRecords = dalaranPosSlipDB.items.getRefreshedRecordsBy(dalaranSlipRecord.getSlipKey().toSID());

                if (dalaranSlipItemRecords.size() == 0) {
                    MyUI.toastSHORT(context, String.format("[%s] 작성 중이나 세부 항목이 없습니다", counterpart.name));
                    return;
                }

                int insertCount = 0;
                for (SlipItemRecord dalaranSlipItemRecord : dalaranSlipItemRecords) {
                    DBResult result = posSlipDB.updateOrInsertItemFromSource(slipRecord, dalaranSlipItemRecord);
                    if (result.resultType == DBResultType.INSERTED)
                        insertCount++;
                }

                MyUI.toastSHORT(context, String.format("미송앱 자료 %d건을 추가했습니다 (%d건 중복)", dalaranSlipItemRecords.size(), dalaranSlipItemRecords.size() - insertCount));
                refresh();
                scrollListView(MyView.BOTTOM);

            }
        }

        @Override
        protected void onPrintClick() {
            PosSlipPreview.startActivity(context, posSlipDB.getFullRecord(slipRecord));
        }

        @Override
        protected void onScrollSettingClick() {
            new ScrollBarSettingDialog(context).show();
        }

        class ChangeAllDialog extends MyButtonsDialog {
            public ChangeAllDialog(Context context, final MyOnTask<SlipItemType> onTask) {
                super(context, "일괄 변경", "모든 항목을 다음 상태로 변경합니다");

                List<Pair<String, SlipItemType>> items = new ArrayList<>();
                items.add(Pair.create(SlipItemType.SALES.uiName, SlipItemType.SALES));
                items.add(Pair.create(SlipItemType.ADVANCE.uiName, SlipItemType.ADVANCE));
                items.add(Pair.create(SlipItemType.PREORDER.uiName, SlipItemType.PREORDER));
                items.add(Pair.create(SlipItemType.PREORDER_OUT_OF_STOCK.uiName, SlipItemType.PREORDER_OUT_OF_STOCK));
                addRadioButtons(items, SlipItemType.SALES, new MyOnTask<SlipItemType>() {
                    @Override
                    public void onTaskDone(SlipItemType slipItemType) {
                        dismiss();
                        if (onTask != null)
                            onTask.onTaskDone(slipItemType);
                    }
                });
                show();
            }
        }

        @Override
        protected void onApplyClick() {
            final List<SlipItemRecord> items = posSlipDB.items.getRecordsBy(slipRecord.getSlipKey().toSID());
            SlipSummaryRecord slipSummaryRecord = getSlipSummaryRecord();
            if (items.size() <= 0) {
                MyUI.toastSHORT(context, String.format("입력 자료가 없습니다"));
                return;
            }

            if (UserRecord.isTemp(slipRecord.counterpartPhoneNumber)) {
                new TempUserDialog(context).show();
                return;
            }

            new TransConfirmDialog(context, slipSummaryRecord, MAccountDB.getInstance(context).getRecordByKey(counterpart.phoneNumber)) {
                @Override
                public void onApplyClick(int onlinePayment, int entrustPayment, int billValue) { // 2회 연속 눌리는 경우 문제가 됨

                    if (userSettingData.isActivated(ShoplusServiceType.AutoMessaging)) {
                        if (BaseUtils.isValidPhoneNumber(slipRecord.counterpartPhoneNumber)) {
                            dismiss();
                            new AutoMessageDialog(context, onlinePayment, entrustPayment, billValue).show();
                        } else {
                            postTransaction(onlinePayment, entrustPayment, billValue, AutoMessageRequestTimeConfigurationType.DO_NOT_SEND);
                            MyUI.toastSHORT(context, String.format("알림톡 생략 (임시번호 고객)"));
                        }
                    } else {
                        postTransaction(onlinePayment, entrustPayment, billValue, AutoMessageRequestTimeConfigurationType.DO_NOT_SEND);
                    }
                }
            };
        }

        @Override
        protected void onDeleteClick() {
            new onDeleteClickDialog(context).show();
        }

        class onDeleteClickDialog extends MyButtonsDialog {

            public onDeleteClickDialog(final Context context) {
                super(context, "영수증 삭제", "작성중인 영수증을 삭제 하시겠습니까?");
                addButton("영수증 삭제", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        posSlipDB.deleteFromDBs(slipRecord.getSlipKey());
                        dismiss();
                        doFinishForResult();
                    }
                });
                addButton("내용만 삭제", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        posSlipDB.deleteLink(slipRecord.getSlipKey());
                        posSlipDB.items.deleteBy(slipRecord.getSlipKey().toSID());
                        dismiss();
                        refresh();
                    }
                });
                show();
            }
        }

        @Override
        protected void onFunctionClick() { // 공유버튼
            final List<SlipItemRecord> items = posSlipDB.items.getRecordsBy(slipRecord.getSlipKey().toSID());
            if (items.size() <= 0) {
                MyUI.toastSHORT(context, String.format("입력 자료가 없습니다"));
                return;
            }

            new MyEditTextDialog(context, "공유기록으로 전환", "다른 사람도 수정할 수 있도록 공유 기록으로 전환 하시겠습니까?") {
                @Override
                public void yes(final String comment) {
                    new MyServiceTask(context, "PosSlipCreationView") {
                        @Override
                        public void doInBackground() throws MyServiceFailureException {
                            transactionService.postDraft(userSettingData.getUserShopInfo(), slipRecord.counterpartPhoneNumber,
                                    SlipItemRecord.toProtocolList(items), comment);
                            posSlipDB.deleteFromDBs(slipRecord.getSlipKey());
                            MyUI.toastSHORT(context, String.format("공유 기록으로 전환 되었습니다. '공유기록'을 눌러주세요"));
                            dismiss();
                            finish();
                        }
                    };
                }
            };

        }

        @Override
        protected void onChangeBuyerClick(UserRecord newBuyer) {
            setCounterpart(newBuyer.phoneNumber);
            // 거래처 변경 후속 조치
            refreshCounterpart(counterpart);
            downloadAccount(counterpart);
            MyUI.toastSHORT(context, String.format("%s로 변경했습니다", counterpart.name));
        }

        @Override
        protected void postTransaction(int onlinePayment, int entrustPayment, int billValue, AutoMessageRequestTimeConfigurationType autoMessage) {
            if (onUploading.isOnTasking()) {
                MyUI.toastSHORT(context, String.format("서버에 전송중입니다. 잠시만 기다려주세요"));
                if (retryCount >= 3)
                    onUploading = TaskState.beforeTasking; //  무한 루프 방지
                return;
            }
            onUploading = TaskState.onTasking;
            retryCount = 0;

            slipRecord.onlinePayment = onlinePayment;
            slipRecord.entrustPayment = entrustPayment;
            slipRecord.bill = billValue;

            final List<PosAndSourceLinkRecord> dalaranSlipItemRecords = posSlipDB.posAndSourceLink.getRecordsBy(posSlipDB.items.getRecordsBy(slipRecord.getSlipKey().toSID()));
            // 시간을 재지정 해야
            BaseAwsService.uploadSlipAndStocks(context, "Silver.SlipCreation", slipRecord,
                    globalDB.getCreatedDateTime(userSettingData), true, autoMessage, new MyOnAsync<SlipFullRecord>() {
                        @Override
                        public void onSuccess(SlipFullRecord newFullRecord) {
                            onUploading = TaskState.afterTasking;

                            // 미송앱에서 가져온 데이터도 전표로 생성되면 미송앱에서도 삭제한다
                            if (MyDevice.hasPackage(context, MyApp.Dalaran.getPackageName())) {
                                for (PosAndSourceLinkRecord dalaranSlipItemRecord : dalaranSlipItemRecords) {
                                    DalaranPosSlipDB.getInstance(context).deleteItem(dalaranSlipItemRecord.sourceSlipItemDbId);
                                }
                            }

                            // 7일 이전 기록을 만들면 리스트에서 안 보이므로 경고를 해줘야 한다
                            if (globalDB.getCreatedDateTime(userSettingData).isBefore(DateTime.now().minusDays(7)))
                                MyUI.toastSHORT(context, String.format("일주일 이전 기록이므로 거래내역에서만 확인하실 수 있습니다 "));

                            startPosSlipSharingView(newFullRecord);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();
                        }

                        @Override
                        public void onError(Exception e) {
                            onUploading = TaskState.afterTasking;
                        }
                    }, new MyOnAsync() {
                        @Override
                        public void onSuccess(Object result) {
                            refreshAfterCreation();
                            MyUI.toastSHORT(context, String.format("재고에 반영"));
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
        }

        @Override
        protected void setCounterpart(String phoneNumber) {
            counterpart = userDB.getRecord(phoneNumber);
            slipRecord.counterpartPhoneNumber = phoneNumber;
            PosSlipDB.getInstance(context).update(slipRecord.id, slipRecord);  // items는 변동사항이 없음 (key : owner + date)
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (choiceMode) {
                case Multiple:
                    adapter.onItemClick(view, position);
                    return;
            }

            final SlipItemRecord slipItemRecord = adapter.getRecord(position);
            if (posSlipDB.posAndSourceLink.has(slipItemRecord.id)) { // 현재 아이템이 history에 있다는건 dalaran에서 가져왔다는 뜻
                new MyAlertDialog(context, "미송앱 자료 삭제", "미송앱에서 가져온 자료는 삭제만 할 수 있습니다. (다시 추가 가능)\n\n삭제 하시겠습니까?") {
                    @Override
                    public void yes() {
                        posSlipDB.deleteItem(slipItemRecord.id); // serial 재할당, history내 serial 조정 자동으로
                        posSlipDB.items.refreshRecordsBySerial(slipItemRecord.slipKey);
                        refresh();
                    }
                };
                return;
            }

            if (slipItemRecord.isItemTraceAttached()) {
                PosSlipItemAddition.startActivityUpdate(context, slipRecord, slipItemRecord, true);
            } else {
                if (slipItemRecord.slipItemType.isNonEditableType()) { // 할인, 부가세
                    new MyAlertDialog(context, slipItemRecord.slipItemType.uiName, String.format("%s은 삭제만 할 수 있습니다. 삭제 하시겠습니까?"
                            , slipItemRecord.slipItemType.uiName)) {
                        @Override
                        public void yes() {
                            posSlipDB.deleteItem(slipItemRecord.id); // serial 재할당, history내 serial 조정 자동으로
                            posSlipDB.items.refreshRecordsBySerial(slipItemRecord.slipKey);
                            refresh();
                        }
                    };
                } else if (slipItemRecord.slipItemType.isPriceOnlyType()) { // 기타 비용 (택배비 같은)
                    new UpdateValueDialog(context, "택배비", "", slipItemRecord.unitPrice) {

                        @Override
                        public void onCreate() {
                            super.onCreate();
                            enableFunctionButton("삭 제");
                        }

                        @Override
                        public void onApplyClick(String newValue) {
                            GlobalDB.myDeliveryCost.put(context, BaseUtils.toInt(newValue));
                            slipItemRecord.unitPrice = BaseUtils.toInt(newValue);
                            posSlipDB.items.updateOrInsertBySerial(slipItemRecord);
                            refresh();
                        }

                        @Override
                        protected void onFunctionClick() {
                            doDismiss();
                            posSlipDB.deleteItem(slipItemRecord.id); // serial 재할당, history내 serial 조정 자동으로
                            posSlipDB.items.refreshRecordsBySerial(slipItemRecord.slipKey);
                            refresh();
                        }
                    };
                } else {
                    PosSlipItemAddition.startActivityUpdate(context, slipRecord, slipItemRecord, true);
                }
            }
            // 종료시 onActivityResults 시행
        }

        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            final SlipItemRecord slipItemRecord = adapter.getRecord(position);
            new ItemLongClickDialog(context, slipItemRecord).show();
            return true; // false면 onItemClick도 호출
        }

        class ItemLongClickDialog extends MyButtonsDialog {

            public ItemLongClickDialog(final Context context, final SlipItemRecord slipItemRecord) {
                super(context, "항목 추가 동작", slipItemRecord.name);
                setDialogSize(0.8, -1);

                if (slipItemRecord.isItemTraceAttached() == false) {
                    addButton("복사해서 추가", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SlipItemRecord newSlipItemRecord = slipItemRecord.clone();
                            newSlipItemRecord.serial = posSlipDB.items.getNextSerial(slipItemRecord.slipKey);
                            posSlipDB.items.insert(newSlipItemRecord);
                            mainView.refresh();
                            lvSlipItems.setSelection(adapter.getCount() - 1);
                            dismiss();
                        }
                    });
                }

                addButton("삭   제", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        posSlipDB.deleteItem(slipItemRecord.id);
                        posSlipDB.items.refreshRecordsBySerial(slipItemRecord.slipKey);
                        mainView.refresh();
                        dismiss();
                    }
                });

                addButton("맨 위로", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        posSlipDB.items.pullUp(slipItemRecord.slipKey, slipItemRecord.serial);
                        mainView.refresh();
                        scrollListView(MyView.TOP);
                        dismiss();
                    }
                });

                addButton("맨 아래로", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        posSlipDB.items.pullDown(slipItemRecord.slipKey, slipItemRecord.serial);
                        mainView.refresh();
                        scrollListView(MyView.BOTTOM);
                        dismiss();
                    }
                });

                addButton("상품정보 수정", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ItemRecord itemRecord = itemDB.getRecordBy(slipItemRecord.itemId);
                        if (itemRecord.id <= 0) {
                            MyUI.toastSHORT(context, String.format("수동으로 추가된 상품입니다"));
                            return;
                        }
                        startItemFullViewPager(itemRecord, null);
                        dismiss();
                    }
                });
            }
        }

    }

    // 기본 형태는 BaseItemsFrag를 따른다
    public class ItemsView extends BaseModuleView {
        public ItemGridComposition itemGridView;
        private HistoryItemDB historyItemDB;

        public ItemsView(final Context context, View container) {
            super(context, getView(), container);
            historyItemDB = HistoryItemDB.getInstance(context);
            itemGridView = new ItemGridComposition(context, this);
            setAdaptor();
            itemGridView.onCreateView(getView());
            itemGridView.myEmptyGuide.setMessage("내 상품들을 이미지로 보여줍니다" +
                    "\n\n이미지가 없는 경우 첫화면에서\n '상품추가' 버튼을 눌러 추가해주세요~");
            itemGridView.setOnBackButtonClick(new MyOnClick() {
                @Override
                public void onMyClick(View view, Object record) {
                    onBackPressed();
                }
            });
            itemGridView.setOnShowHistoryClick(new MyOnClick() {
                @Override
                public void onMyClick(View view, Object record) {

                    new HistoryItemDialog<HistoryItemRecord>(context, historyItemDB.getRecords()) {
                        @Override
                        protected String getUiString(HistoryItemRecord record) {
                            return record.name;
                        }

                        @Override
                        public void onItemClicked(@Nullable HistoryItemRecord selectedRecord) {
                            if (selectedRecord == null)
                                return;
                           itemGridView.selectItem(itemDB.getRecordBy(selectedRecord.itemId));
                        }
                    };
                }
            });

            refresh();
        }

        protected void setAdaptor() {
            if (itemGridView.adapter == null)
                itemGridView.adapter = new ItemDBAdapter(context);
            itemGridView.adapter.resetFiltering().setViewType(ItemDBAdapter.ItemsFrag);
            itemGridView.setSortingKey("pos.slip.creation.view.sorting").setFinishItemSearchByName(true);
            itemGridView.setItemStateKey("pos.slip.creation.itemstate");
            itemGridView.adapter.filterStates(itemGridView.getItemStates());
            itemGridView.setFilterNameKey("pos.slip.creation.filtername");
        }

        public void refresh() {
            if (itemGridView == null) return;
            if (itemGridView.adapter == null) return;

            new MyAsyncTask(context, "PosSlipCreationView") {
                @Override
                public void doInBackground() {
                    itemGridView.adapter.generate();
                }

                @Override
                public void onPostExecutionUI() {
                    itemGridView.adapter.notifyDataSetChanged();
                    itemGridView.onRefresh();
                //    itemGridView.setLastSelectedItem(itemDB.getRecordBy(lastSelectedItemId.get(context)));
                }
            };
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            doBlockRefreshMainLayout = true;
            retrievedItemView.showResult(itemGridView.adapter.getRecord(position).itemId);
        }

        @Override
        public void onClick(View view) {
            itemGridView.onClick(view);
        }

    }

    class RetrievedItemView extends BaseModuleView implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
        private int thisView = R.layout.module_item;
        private ImageView ivItemImage;
        private TextView tvItemName, tvItemPrice, tvItemCategory, tvTableAmount;
        private ItemDB itemDB;
        public ItemOptionDBAdapter optionAdapter;
        private ListView listView;
        private ItemRecord itemRecord = new ItemRecord();
        private SilverSlipItemSelection slipItemSelection;

        public RetrievedItemView(final Context context, View container, View view) {
            super(context, getView(), container);
            itemDB = ItemDB.getInstance(context);
            slipItemSelection = new SilverSlipItemSelection(context, itemDB);
            ivItemImage = findViewById(R.id.module_item_image);
            tvItemName = findViewById(R.id.module_item_name);
            tvItemPrice = findViewById(R.id.module_item_price);
            tvItemCategory = findViewById(R.id.module_item_category);
            tvTableAmount = findTextViewById(R.id.module_item_amount);
            listView = view.findViewById(R.id.module_item_options);
            MyView.setTextViewByDeviceSize(context, getView(), R.id.module_item_color,
                    R.id.module_item_size, R.id.module_item_stock);

            optionAdapter = new ItemOptionDBAdapter(context);
            optionAdapter.setViewType(ItemOptionDBAdapter.SlipCreationView);
            listView.setAdapter(optionAdapter);
            listView.setOnItemClickListener(this);
            listView.setOnItemLongClickListener(this);

            setOnClick(R.id.module_item_close, R.id.module_item_apply, R.id.module_item_cancel,
                    R.id.module_item_addFast, R.id.module_item_openFullView, R.id.module_item_category,
                    R.id.module_item_name, R.id.module_item_price, R.id.module_item_selectAll,
                    R.id.module_item_image);

            MyView.setBlueColorFilter(findViewById(R.id.module_item_addFast));
        }

        @Override
        public void show() {
            super.show();
        }

        @Override
        public void hide() {
            super.hide();
        }

        @Override
        public void refresh() {

        }

        public boolean showResult(final int itemId) {
            if (itemDB.has(itemId) == false) {
                hide();
                MyUI.toastSHORT(context, String.format("목록에서 찾을 수 없습니다 [%d]", itemId));
                return false;
            }

            show();

            boolean changed = (itemRecord.itemId != itemId); // 이전과 다른 아이템인지
            itemRecord = itemDB.getRecordBy(itemId);
            List<ItemOptionRecord> options = itemDB.options.getSortedRecords(itemRecord.itemId);  // updateOrInsert
            if (options.size() == 0)
                new NoOptionDialog(context, itemRecord, null);

            tvItemName.setText(String.format("이름 : %s", itemRecord.name));
            tvItemPrice.setText(String.format("가격 : %s", itemRecord.getPriceStringWithRetail(context)));
            tvItemCategory.setText(String.format("분류 : %s", itemRecord.category.toUIStr()));

            List<ItemOptionRecord> selectedOptions = getSelectedOptions();
            for (ItemOptionRecord option : options) {
                option.revStock = 0;  // 최초 수량을 0으로 초기화
                if (changed == false) { // 이전 것을 계속 보고 있다면
                    for (ItemOptionRecord selectedOption : selectedOptions) {
                        if (option.color == selectedOption.color && option.size == selectedOption.size) {
                            option.revStock = selectedOption.revStock; // 이전에 저장한게 있으면 그 값을 복원시켜줌
                            break;
                        }
                    }
                }
            }

            optionAdapter.setRecords(options);
            optionAdapter.notifyDataSetChanged();

            MyImageLoader.getInstance(context).retrieveThumb(itemRecord, ivItemImage);
            refreshSelected();
            return true;
        }

        class NoOptionDialog extends MyButtonsDialog {
            private final static String NoOptionTitle = "컬러-사이즈 추가", NoOptionMessage = "입력된 컬러-사이즈가 없습니다";

            public NoOptionDialog(final Context context, final ItemRecord itemRecord, final MyOnTask onTask) {
                super(context, NoOptionTitle, NoOptionMessage);

                addButton("기본 컬러-사이즈 추가", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        slipItemSelection.addDefaultOption(itemRecord, new MyOnTask<ItemOptionRecord>() {
                            @Override
                            public void onTaskDone(ItemOptionRecord result) {
                                optionAdapter.clear();
                                optionAdapter.addRecord(result);
                                optionAdapter.notifyDataSetChanged();
                                refreshSelected();
                                MyUI.toastSHORT(context, String.format("기본 옵션을 추가했습니다"));
                                dismiss();
                                if (onTask != null)
                                    onTask.onTaskDone("");
                            }
                        });
                    }
                });

                addButton("입력 페이지로 이동", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doBlockRefreshMainLayout = true;
                        startItemFullViewPager(itemRecord, new MyOnTask() {
                            @Override
                            public void onTaskDone(Object result) {
                                itemsView.refresh();
                                showResult(itemRecord.itemId);
                            }
                        });
                        MyUI.toastSHORT(context, String.format("[컬러-사이즈 추가]를 눌러주세요"));
                        dismiss();
                    }
                });
                show();
            }
        }

        // SilverItemAndOptionSearch.onApplyClick 참조
        private void onApplyClick(final ItemRecord selectedItem) {
            final List<ItemOptionRecord> options = getSelectedOptions();
            if (options.size() > 0) {
                lastSelectedItemId.put(context, selectedItem.itemId);
                doBlockRefreshMainLayout = false;
                selectedItem.getUnitPriceByBuyer(context, slipRecord.counterpartPhoneNumber, new MyOnTask<Integer>() {
                            @Override
                            public void onTaskDone(Integer result) {
                                addSlipItemsWithSALES(new SelectedItemRecord(itemRecord, result, getSelectedOptions(), true), true);
                                new MyDelayTask(context, 500) {
                                    @Override
                                    public void onFinish() {
                                        optionAdapter.clear(); // 저장된 revStock 삭제
                                        if (currentViewType != CurrentViewType.Slip)
                                            onBackPressed(); // 영수증 화면으로 돌아가기
                                        mainView.onActivityResult(BaseGlobal.RequestRefresh, 0, null); // SlipItemAddition과 동일하게
                                    }
                                };
                            }
                        });

            } else {
                if (optionAdapter.getCount() == 0) {
                    new NoOptionDialog(context, selectedItem, null);
                } else if (optionAdapter.getCount() == 1) { // 옵션이 1개면서 클릭하지 않았을때는 자동으로 선택해준다
                    onMyOptionClick(optionAdapter.getRecord(0));
                } else {
                    MyUI.toastSHORT(context, String.format("선택된 옵션이 없습니다"));
                    return;
                }
            }
        }

        private void onAddFastClick(final ItemRecord selectedItem) {
            lastSelectedItemId.put(context, selectedItem.itemId);

            slipItemSelection.onAddFastClick(selectedItem, optionAdapter, getSelectedOptions(), new MyOnTask<List<ItemOptionRecord>>() {
                @Override
                public void onTaskDone(final List<ItemOptionRecord> options) {
                    doBlockRefreshMainLayout = false;

                    selectedItem.getUnitPriceByBuyer(context, slipRecord.counterpartPhoneNumber, new MyOnTask<Integer>() {
                        @Override
                        public void onTaskDone(Integer unitPriceByBuyer) {
                            addSlipItemsWithSALES(new SelectedItemRecord(selectedItem, unitPriceByBuyer, options, false), false);
                            new MyDelayTask(context, 500) {
                                @Override
                                public void onFinish() {
                                    optionAdapter.clear();
                                    onBackPressed();
                                    mainView.onActivityResult(BaseGlobal.RequestRefresh, 0, null); // SlipItemAddition과 동일하게
                                }
                            };
                        }
                    });
                }
            });
        }


        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.module_item_openFullView || view.getId() == R.id.module_item_name ||
                    view.getId() == R.id.module_item_price || view.getId() == R.id.module_item_category ||
                    view.getId() == R.id.module_item_image) {
                doBlockRefreshMainLayout = true;
                startItemFullViewPager(itemRecord, new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        showResult(itemRecord.itemId);
                        itemsView.refresh();
                    }
                });

            } else if (view.getId() == R.id.module_item_close || view.getId() == R.id.module_item_cancel) {
                doBlockRefreshMainLayout = false;
                retrievedItemView.hide();
                mainView.refresh();
                switch (currentViewType) {
                    case Slip:
                        break;
                    case ItemList:
                        break;
                }
            } else if (view.getId() == R.id.module_item_apply) {
                onApplyClick(itemRecord);

            } else if (view.getId() == R.id.module_item_addFast) { // 이미지로 추가에서 빠른판매
                onAddFastClick(itemRecord);

            } else if (view.getId() == R.id.module_item_selectAll) {
                onSelectAllClick(itemRecord);
            }
        }

        private void onSelectAllClick(ItemRecord itemRecord) {
            slipItemSelection.selectAllOptions(optionAdapter, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    refreshSelected();
                }
            });
        }

        private List<ItemOptionRecord> getSelectedOptions() {
            List<ItemOptionRecord> results = new ArrayList<>();
            for (ItemOptionRecord record : optionAdapter.getRecords())
                if (record.revStock >= 1)
                    results.add(record);
            return results;
        }

        private void refreshSelected() {
            List<ItemOptionRecord> selectedOptions = getSelectedOptions();
            int selectedAmount = 0;
            if (selectedOptions.size() >= 1) {
                for (ItemOptionRecord optionRecord : selectedOptions)
                    selectedAmount = selectedAmount + optionRecord.revStock;
            }
            tvTableAmount.setText(String.format("수량 (%d)", selectedAmount));
        }

        @Override
        // ItemAndOptionSearch에도 동일 기능 있음
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final ItemOptionRecord optionRecord = optionAdapter.getRecord(position);
            onMyOptionClick(optionRecord);
        }

        private void onMyOptionClick(final ItemOptionRecord optionRecord) {
            new OptionQuantityDialog(context, optionRecord, SilverPreference.stockCheckingEnabled.getValue(context)) {
                @Override
                public void onApplyClick(int newRevStock) {
                    optionRecord.revStock = newRevStock;
                    optionAdapter.notifyDataSetChanged();
                    refreshSelected();
                }
            };
        }

        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            ItemOptionRecord optionRecord = optionAdapter.getRecord(position);
            if (optionRecord.revStock == 0) {
                optionRecord.revStock = 1;
            } else {
                optionRecord.revStock = 0;
            }
            optionAdapter.notifyDataSetChanged();
            refreshSelected();
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case BaseGlobal.RequestRefresh:
                mainView.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void refreshMainLayout() {
        if (doBlockRefreshMainLayout)
            return;

        mainView.hide();
        itemsView.hide();
        retrievedItemView.hide();

        switch (currentViewType) {
            case Slip:
                mainView.show();
                break;
            case ItemList:
                itemsView.show();
                itemsView.refresh();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkUserSettingData(new MyOnTask() {
            @Override
            public void onTaskDone(Object result) {
                refreshMainLayout();
            }
        });
    }

    @Override
    public void onBackPressed() {
        doBlockRefreshMainLayout = false;
        switch (currentViewType) {
            case Slip:
                super.onBackPressed();
                break;
            default:
                currentViewType = CurrentViewType.Slip;
                refreshMainLayout();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
        }
    }

    @Override
    public void onMyMenuCreate() {
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
    }

    // 선택된 아이템을 임시 DB에 저장
    // 기존에는 quantity가 무조건 1이었는데 이제는 revStock에 quantity를 담아서 보내줘야 한다 (이름으로, 이미지로)
    private void addSlipItemsWithSALES(final SelectedItemRecord selectedItemRecord, final boolean doToastResult) {
        // 거래처별 단가를 확인
        List<SlipItemRecord> newSlipItemRecords = new ArrayList<>();
        int serial = posSlipDB.items.getNextSerial(slipRecord.getSlipKey().toSID());
        for (ItemOptionRecord itemOptionRecord : selectedItemRecord.items) {
            SlipItemRecord slipItemRecord = new SlipItemRecord(selectedItemRecord.itemRecord, itemOptionRecord, slipRecord);
            if (slipItemRecord.name.isEmpty())
                slipItemRecord.name = selectedItemRecord.itemRecord.getUiName();
            slipItemRecord.slipItemType = SlipItemType.SALES; // 일단 판매로
            slipItemRecord.quantity = itemOptionRecord.revStock; // 이 부분이 변경됨
            slipItemRecord.serial = serial;
            slipItemRecord.unitPrice = selectedItemRecord.unitPriceByBuyer; // 거래처별 단가
            DBResult dbResult = posSlipDB.items.insert(slipItemRecord);
            slipItemRecord.id = dbResult.id;
            newSlipItemRecords.add(slipItemRecord);
            serial++;
        }

        MyUI.toastSHORT(context, "추가");
        mainView.listViewDirectionOnResume = MyView.BOTTOM;
        if (selectedItemRecord.doShowUpdateActivity)
            PosSlipItemAddition.startActivityUpdate(context, slipRecord, newSlipItemRecords, doToastResult, true);
    }

}
