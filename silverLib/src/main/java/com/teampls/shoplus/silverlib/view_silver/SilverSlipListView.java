package com.teampls.shoplus.silverlib.view_silver;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BasePosSlipListView;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.common.SilverPreference;
import com.teampls.shoplus.silverlib.database.PosSlipDBAdapter;
import com.teampls.shoplus.silverlib.datatypes.SettlementAuxilityData;
import com.teampls.shoplus.silverlib.view.SettlementSettingView;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2019-06-26.
 */

abstract public class SilverSlipListView extends BasePosSlipListView {

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    public void setAdapters() {
        adapter = new PosSlipDBAdapter(context);
        adapter.doSplitConfirmed(true).blockCancelled(!SilverPreference.isShowCancelled(context));
        adapter.setOrderRecord(PosSlipDB.Column.salesDateTime, true).setViewType(PosSlipDBAdapter.SlipListView);
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.settleAccount)
                .add(MyMenu.cancelSlip)
                .add(MyMenu.openDrafts)
                .add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        super.onMyMenuSelected(myMenu);
        switch (myMenu) {
            case settleAccount:
                new SettleAccountClickDialog(context).show();
                break;
        }
    }

    class SettleAccountClickDialog extends MyButtonsDialog {

        public SettleAccountClickDialog(final Context context) {
            super(context, "마감관련 업무 선택", "");

            addButton("금일 마감 시작", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    SettlementSettingView.startActivity(context);
                }
            });

            addButton("지출내역 빠른추가", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    new InputSettlementAuxDialog(context).show();
                }
            });
        }
    }

    class InputSettlementAuxDialog extends BaseDialog {
        private EditText etName, etCost;
        private SettlementAuxilityData settlementAuxilityData = new SettlementAuxilityData();

        public InputSettlementAuxDialog(Context context) {
            super(context);
            setVisibility(View.GONE, R.id.dialog_settlement_aux_delete);
            init();

            LinearLayout container = findViewById(R.id.dialog_settlement_aux_preset_container);
            MyView.setTextViewByDeviceSize(context, container);
            container.setVisibility(View.VISIBLE);
            for (View view : MyView.getChildViews(container)) {
                if (view instanceof TextView) {
                    final TextView textView = (TextView) view;
                    MyView.setTextViewByDeviceSize(context, textView);
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            etName.setText(textView.getText().toString());
                            etCost.requestFocus();
                        }
                    });
                }
            }
        }

        private void init() {
            setDialogSize(0.9, -1);
            etName = (EditText) findTextViewById(R.id.dialog_settlement_aux_name, settlementAuxilityData.title);
            etCost = (EditText) findTextViewById(R.id.dialog_settlement_aux_cost, settlementAuxilityData.amount == 0 ? "" : Integer.toString(settlementAuxilityData.amount));
            setOnClick(R.id.dialog_settlement_aux_name_clear, R.id.dialog_settlement_aux_cost_clear,
                    R.id.dialog_settlement_aux_apply, R.id.dialog_settlement_aux_cancel, R.id.dialog_settlement_aux_delete);
            MyDevice.showKeyboard(context, etName);
            show();
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_settlement_aux;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_settlement_aux_name_clear) {
                etName.setText("");

            } else if (view.getId() == R.id.dialog_settlement_aux_cost_clear) {
                etCost.setText("");

            } else if (view.getId() == R.id.dialog_settlement_aux_cancel) {
                MyDevice.hideKeyboard(context, etName);
                dismiss();

            } else if (view.getId() == R.id.dialog_settlement_aux_apply) {
                if (Empty.isEmpty(context, etName, "항목이 입력되지 않았습니다"))
                    return;
                if (Empty.isEmpty(context, etCost, "비용이 입력되지 않았습니다"))
                    return;

                settlementAuxilityData.title = etName.getText().toString();
                settlementAuxilityData.amount = BaseUtils.toInt(etCost.getText().toString());
                settlementAuxilityData.setDateTime(DateTime.now()); // 현재시간
                onApplyClick(settlementAuxilityData);
            }
        }

        public void onApplyClick(final SettlementAuxilityData record) {
            new MyServiceTask(context, "uploadSettlementData") {
                SettlementAuxilityData result;

                @Override
                public void doInBackground() throws MyServiceFailureException {
                    result = service.postSettlementExpenseData(userSettingData.getUserShopInfo(), record.title, record.amount, record.getCreatedDateTime());
                }

                @Override
                public void onPostExecutionUI() {
                    dismiss();
                    MyDevice.hideKeyboard(context, etName);
                    MyUI.toastSHORT(context, String.format("지출내역 추가 완료"));
                }
            };
        }

    }

}
