package com.teampls.shoplus.silverlib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.datatypes.ItemTraceData;
import com.teampls.shoplus.lib.datatypes.ItemTraceRecord;
import com.teampls.shoplus.lib.enums.ItemTraceState;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-10-16.
 */

public class ItemTraceOptionView extends BaseActivity {
    private static ItemOptionRecord optionRecord;
    private ItemTraceGroupAdapter foundAdapter;
    private TaskState onDownloading = TaskState.beforeTasking;
    private ExpandableHeightListView lvFound;

    public static void startActivity(Context context, ItemOptionRecord optionRecord) {
        ItemTraceOptionView.optionRecord = optionRecord;
        ((Activity) context).startActivityForResult(new Intent(context, ItemTraceOptionView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;

        foundAdapter = new ItemTraceGroupAdapter(context, ItemTraceState.FOUND, userSettingData.getLocations());
        lvFound = (ExpandableHeightListView) findViewById(R.id.option_trace_found);
        lvFound.setAdapter(foundAdapter);
        lvFound.setExpanded(true);

        setVisibility(View.GONE, R.id.option_trace_notFound_container,
                R.id.option_trace_sold_container, R.id.option_trace_note);

        findTextViewById(R.id.option_trace_name, ItemDB.getInstance(context).getRecordBy(optionRecord.itemId).getUiName());
        findTextViewById(R.id.option_trace_option, optionRecord.toStringWithDefault(" : "));

        MyView.setTextViewByDeviceSize(context, getView(), R.id.option_trace_found_title, R.id.option_trace_note, R.id.option_trace_saas
                , R.id.option_trace_notFound_title, R.id.option_trace_sold_title);
        findViewById(R.id.option_trace_imageView).setBackgroundColor(optionRecord.color.colorInt);
        downloadTraces();

    }

    @Override
    public int getThisView() {
        return R.layout.activity_option_trace;
    }

    private void refresh() {
        if (foundAdapter == null) return;

        new MyAsyncTask(context, "ItemTraceOptionView") {
            @Override
            public void doInBackground() {
            }

            @Override
            public void onPostExecutionUI() {
                foundAdapter.generate(); // ExpandableHeightListView는 이렇게 처리해야
                foundAdapter.notifyDataSetChanged();
                findTextViewById(R.id.option_trace_saas_count, Integer.toString(foundAdapter.saasCount));
            }
        };
    }

    private void downloadTraces() {
        if (onDownloading.isOnTasking()) {
            MyUI.toastSHORT(context, String.format("다운로드 중"));
            return;
        }
        onDownloading = TaskState.onTasking;
        new MyServiceTask(context, "ItemTraceOptionView") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<ItemTraceData> data = service.getTraces(userSettingData.getUserShopInfo(), optionRecord.toStockKey());
                foundAdapter.setDataAndFilter(data);
            }

            @Override
            public void onPostExecutionUI() {
                refresh();
                MyUI.toastSHORT(context, String.format("최신 자료"));
                onDownloading = TaskState.afterTasking;
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                onDownloading = TaskState.afterTasking;
            }
        };
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case refresh:
                downloadTraces();
                break;
            case close:
                finish();
                break;
        }
    }

    class ItemTraceGroupRecord {
        public String location = "";
        public List<ItemTraceRecord> itemTraceRecords;

        public ItemTraceGroupRecord(String location, List<ItemTraceRecord> itemTraceRecords) {
            this.location = location;
            this.itemTraceRecords = itemTraceRecords;
        }

        public List<String> toStringList() {
            List<String> results = new ArrayList<>();
            int num = 1;
            for (ItemTraceRecord record : itemTraceRecords) {
                results.add(String.format("%d : %s 생성", num, record.getDateTime().toString(BaseUtils.MDHm_Kor_Format)));
                num++;
            }
            return results;
        }
    }

    class ItemTraceGroupAdapter extends BaseDBAdapter<ItemTraceGroupRecord> {
        private List<String> locations = new ArrayList<>();
        private ItemTraceState state;
        private List<ItemTraceData> dataList = new ArrayList<>();
        public int saasCount = 0;

        public ItemTraceGroupAdapter(Context context, ItemTraceState state, List<String> locations) {
            super(context);
            this.state = state;
            this.locations = locations;
        }

        public ItemTraceGroupAdapter setDataAndFilter(List<ItemTraceData> dataList) {
            this.dataList = dataList;
            return this;
        }

        @Override
        public void generate() {
            MyListMap<String, ItemTraceRecord> listMap = new MyListMap<>();
            for (String location : locations)
                listMap.put(location, new ArrayList<ItemTraceRecord>()); // 모든 위치 입력

            saasCount = 0;
            for (ItemTraceData data : dataList) {
                ItemTraceRecord traceRecord = new ItemTraceRecord(data);
                if (traceRecord.state == state)
                    listMap.add(traceRecord.location, traceRecord);
                if (traceRecord.state == ItemTraceState.WAREHOUSE)
                    saasCount = saasCount + data.getBundleCount();
            }

            List<ItemTraceGroupRecord> _records = new ArrayList<>();
            for (String location : listMap.keySet())
                _records.add(new ItemTraceGroupRecord(location, listMap.getList(location)));
            records = _records;
        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new ItemTraceViewHolder();
        }

        class ItemTraceViewHolder extends BaseViewHolder {
            private ImageView ivIcon;
            private TextView tvLocation, tvDate, tvCount;

            @Override
            public int getThisView() {
                return R.layout.row_option_trace_location;
            }

            @Override
            public void init(View view) {
                ivIcon = (ImageView) view.findViewById(R.id.row_option_trace_location_icon);
                tvLocation = (TextView) view.findViewById(R.id.row_option_trace_location_name);
                tvCount = (TextView) view.findViewById(R.id.row_option_trace_location_count);
                tvDate = (TextView) view.findViewById(R.id.row_option_trace_location_date);
                MyView.setTextViewByDeviceSize(context, tvLocation, tvCount, tvDate);
                MyView.setImageViewSizeByDeviceSize(context, ivIcon);
            }

            @Override
            public void update(int position) {
                if (position >= records.size() || position < 0) return;
                ItemTraceGroupRecord record = records.get(position);
                tvCount.setText(Integer.toString(record.itemTraceRecords.size()));
                tvDate.setVisibility(View.GONE);

                if (userDB.has(record.location)) {
                    tvLocation.setText(userDB.getRecord(record.location).getName());
                } else {
                    tvLocation.setText(record.location);
                }
            }
        }

    }

}
