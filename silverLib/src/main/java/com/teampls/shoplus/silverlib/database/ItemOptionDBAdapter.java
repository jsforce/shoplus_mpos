package com.teampls.shoplus.silverlib.database;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseItemOptionDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.view.ItemTraceOptionView;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2017-03-10.
 */

public class ItemOptionDBAdapter extends BaseItemOptionDBAdapter {
    public static final int ItemFullViewStock = 1, ItemFullViewSlip = 2, SlipCreationView = 3,
            ItemAndOptionSearch = 4, StockHistory = 5;
    private DateTime stockDateTime = Empty.dateTime;
    private MyOnTask onStockHistoryCountShow;

    public ItemOptionDBAdapter(Context context) {
        super(context, ItemDB.getInstance(context));
    }

    public ItemOptionDBAdapter(Context context, ItemDB itemDB) {
        super(context, itemDB);
    }

    public void setOnStockHistoryCountShow(MyOnTask onStockHistoryCountShow) {
        this.onStockHistoryCountShow = onStockHistoryCountShow;
    }



    @Override
    public BaseViewHolder getViewHolder() {
        switch (viewType) {
            default:
            case ItemFullViewStock:
                SilverItemFullViewHolder viewHolder1 = new SilverItemFullViewHolder();
                viewHolder1.showInBoundButton(true);
                return viewHolder1;
            case ItemFullViewSlip:
                SlipCreationViewHolder viewHolder = new SlipCreationViewHolder();
                viewHolder.setColorSet(ColorType.Khaki);
                return viewHolder;

            case SlipCreationView:
                viewHolder = new SlipCreationViewHolder();
                viewHolder.setColorSet(ColorType.Khaki);
                return viewHolder;
            case ItemAndOptionSearch:
                return new OptionAndQuantityViewHolder();
            case StockHistory:
                return new StockHistoryViewHolder();
        }
    }



   public class SilverItemFullViewHolder extends ItemFullViewHolder {

        protected void startItemTraceOptionView(ItemOptionRecord itemOptionRecord) {
                ItemTraceOptionView.startActivity(context, itemOptionRecord);
        }
    }

    class StockHistoryViewHolder extends BaseViewHolder {
        private ImageView ivColor;
        private TextView tvColor, tvSize, tvStock;

        @Override
        public int getThisView() {
            return R.layout.row_stock_detail;
        }

        @Override
        public void init(View view) {
            ivColor = (ImageView) view.findViewById(R.id.row_stock_detail_color_imageview);
            tvColor = findTextViewById(context, view, R.id.row_stock_detail_color_name);
            tvSize = findTextViewById(context, view, R.id.row_stock_detail_size);
            tvStock = findTextViewById(context, view, R.id.row_stock_detail_stock);

        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0) return;
            ItemOptionRecord optionRecord = getRecord(position);
            ivColor.setBackgroundColor(optionRecord.color.colorInt);
            tvColor.setText(UserSettingData.getColorName(optionRecord.color));
            tvSize.setText(optionRecord.size.uiName);
            if (onStockHistoryCountShow != null)
                onStockHistoryCountShow.onTaskDone(Pair.create(optionRecord, tvStock));
        }
    }

    class SlipCreationViewHolder extends BaseViewHolder {
        private ImageView imageView;
        private TextView tvColorName, tvSize, tvStock, tvQuantity;
        private LinearLayout container;

        @Override
        public int getThisView() {
            return R.layout.row_slip_creation_list;
        }

        @Override
        public void init(View view) {
            container = (LinearLayout) view.findViewById(R.id.row_slip_creation_list_container);
            imageView = (ImageView) view.findViewById(R.id.row_slip_creation_list_color_imageview);
            tvColorName = (TextView) view.findViewById(R.id.row_slip_creation_list_color_name);
            tvSize = (TextView) view.findViewById(R.id.row_slip_creation_list_size);
            tvStock = (TextView) view.findViewById(R.id.row_slip_creation_list_stock);
            tvQuantity = (TextView) view.findViewById(R.id.row_slip_creation_list_quantity);
            MyView.setTextViewByDeviceSize(context, tvColorName, tvSize, tvStock, tvQuantity);
        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0) return;
            ItemOptionRecord optionRecord = getRecord(position);
            imageView.setBackgroundColor(optionRecord.color.colorInt);
            tvColorName.setText(UserSettingData.getColorName(optionRecord.color));
            tvSize.setText(optionRecord.size.uiName);
            tvStock.setText(Integer.toString(optionRecord.stock));
            tvQuantity.setText(Integer.toString(optionRecord.revStock));
            if (optionRecord.revStock >= 1) {
                container.setBackgroundColor(selectedColor.colorInt);
                tvQuantity.setVisibility(View.VISIBLE);
            } else {
                container.setBackgroundColor(ColorType.Trans.colorInt);
                tvQuantity.setVisibility(View.INVISIBLE);
            }
        }
    }

}
