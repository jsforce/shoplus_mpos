package com.teampls.shoplus.silverlib.common;

import android.os.Bundle;

import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.PreferenceBoolean;
import com.teampls.shoplus.lib.database_global.GlobalDB;

/**
 * Created by Medivh on 2018-10-03.
 */

abstract public class SilverPreference extends BasePreference {
    public static PreferenceBoolean mallMenuEnabled = new PreferenceBoolean("show.mall.topmenu", false);
    public static PreferenceBoolean stockCheckingEnabled = new PreferenceBoolean("check.current.stock", false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        mallMenuEnabled.updatePrevValue(context);
    }

    protected void addSilverPreference() {
        addCheckBox("1원 단위 사용","가격을 1원 단위까지 사용합니다", isOneDigitBasicUnit);
        addCheckBox("[시험기능] 거래처별 단가 적용", "거래처별로 단가를 지정하고 그 값을 계속 사용합니다 (도매가만 적용)", priceByBuyer);
        addCheckBox("도소매 가격 분리", "도소매 가격을 다르게 관리합니다. 거래처를 소매로 지정하면 소매 가격이 적용됩니다.",
                retailEnabled);
        addCheckBox("시간표시", "거래 시간을 표시합니다", doShowHm);
        addCheckBox("카톡쇼핑몰 아이콘 노출", "첫화면 상단 메뉴에 카톡 쇼핑몰 아이콘을 표시합니다", mallMenuEnabled);
        addCheckBox("재고 부족 경고", "영수증 작성시 재고가 부족하면 경고를 띄웁니다", stockCheckingEnabled);
        addCheckBox("화면 회전 허용", "가로 방향으로 돌리면 화면도 회전합니다", landScapeOrientationEnabled);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mallMenuEnabled.isChanged(context))
            doRefreshMenu = true;
    }
}
