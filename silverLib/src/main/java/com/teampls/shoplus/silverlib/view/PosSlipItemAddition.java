package com.teampls.shoplus.silverlib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.Gravity;
import android.view.View;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.CustomerPriceDB;
import com.teampls.shoplus.lib.database_global.PricingRecord;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;
import com.teampls.shoplus.lib.view_base.BaseSlipItemAddition;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.common.SilverPreference;
import com.teampls.shoplus.lib.database_old.TempUnitPriceByBuyer;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-01-18.
 */
// dalaran에도 동일 activity 있음
public class PosSlipItemAddition extends BaseSlipItemAddition {
    private static boolean doToastResult = false, doSkipQuantity = false;
    protected PosSlipDB posSlipDB;

    // serial를 가지고 여기를 들어와야 한다
    public static void startActivityInsert(Context context, int requestCode, SlipRecord slipRecord, boolean doToastResult) {
        PosSlipItemAddition.slipRecord = slipRecord;
        PosSlipItemAddition.slipItemRecord = new SlipItemRecord(slipRecord);
        slipItemRecord.serial = PosSlipDB.getInstance(context).items.getNextSerial(slipRecord.getSlipKey().toSID());
        PosSlipItemAddition.dbActionType = DbActionType.INSERT;
        PosSlipItemAddition.doToastResult = doToastResult;
        ((Activity) context).startActivityForResult(new Intent(context, PosSlipItemAddition.class), requestCode);
    }

    public static void startActivityInsert(Context context, int requestCode, SlipRecord slipRecord, SlipItemRecord slipItemRecord, boolean doToastResult) {
        PosSlipItemAddition.slipRecord = slipRecord;
        PosSlipItemAddition.slipItemRecord = slipItemRecord;
        PosSlipItemAddition.slipItemRecords.clear();
        PosSlipItemAddition.slipItemRecords.add(slipItemRecord);
        slipItemRecord.serial = PosSlipDB.getInstance(context).items.getNextSerial(slipRecord.getSlipKey().toSID());
        PosSlipItemAddition.dbActionType = DbActionType.INSERT;
        PosSlipItemAddition.doToastResult = doToastResult;
        PosSlipItemAddition.doSkipQuantity = false;
        ((Activity) context).startActivityForResult(new Intent(context, PosSlipItemAddition.class), requestCode);
    }

    public static void startActivityUpdate(Context context, SlipRecord slipRecord, SlipItemRecord slipItemRecord, boolean doToastResult) {
        PosSlipItemAddition.slipRecord = slipRecord;
        PosSlipItemAddition.slipItemRecord = slipItemRecord;
        PosSlipItemAddition.slipItemRecords.clear();
        PosSlipItemAddition.slipItemRecords.add(slipItemRecord);
        PosSlipItemAddition.dbActionType = DbActionType.UPDATE;
        PosSlipItemAddition.doToastResult = doToastResult;
        PosSlipItemAddition.doSkipQuantity = slipItemRecord.isItemTraceAttached(); // QR이 있으면 수량은 Skip 해야 함
        ((Activity) context).startActivityForResult(new Intent(context, PosSlipItemAddition.class), BaseGlobal.RequestRefresh);
    }

    public static void startActivityUpdate(Context context, SlipRecord slipRecord, List<SlipItemRecord> slipItemRecords,  boolean doToastResult, boolean doSkipQuantity) {
        if (slipItemRecords.size() <= 0) {
            MyUI.toastSHORT(context, String.format("입력 데이터에 오류가 있습니다 (거래 기록 항목)"));
            return;
        }
        PosSlipItemAddition.slipRecord = slipRecord;
        PosSlipItemAddition.slipItemRecords = slipItemRecords;
        PosSlipItemAddition.slipItemRecord = slipItemRecords.get(0); // 대표
        if (doSkipQuantity == false)
            PosSlipItemAddition.slipItemRecord.quantity = 0; // Quantity 입력을 쉽게 해주기 위해
        if (slipItemRecords.size() == 1) {
            PosSlipItemAddition.dbActionType = DbActionType.UPDATE;
        } else {
            PosSlipItemAddition.dbActionType = DbActionType.MULTIPLE_UPDATE;
        }
        PosSlipItemAddition.doToastResult = doToastResult;
        PosSlipItemAddition.doSkipQuantity = doSkipQuantity;
        ((Activity) context).startActivityForResult(new Intent(context, PosSlipItemAddition.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    protected void setSlipDB() {
        posSlipDB = PosSlipDB.getInstance(context);
    }

    protected void initializeSubContainers() {
        if (slipItemRecord.itemId > 0) {
            subContainers.add(Module.name, R.id.module_new_slip_nameContainer, false);
            subContainers.add(Module.quantity, R.id.module_new_slip_quantityContainer, !doSkipQuantity);
            subContainers.add(Module.unitPrice, R.id.module_new_slip_unitPriceContainer, true);
            subContainers.add(Module.slipType, R.id.module_new_slip_slipTypeContainer, true);
        } else {
            subContainers.add(Module.name, R.id.module_new_slip_nameContainer, true);
            subContainers.add(Module.quantity, R.id.module_new_slip_quantityContainer, true);
            subContainers.add(Module.unitPrice, R.id.module_new_slip_unitPriceContainer, true);
            subContainers.add(Module.slipType, R.id.module_new_slip_slipTypeContainer, true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        switch (dbActionType) {
            case UPDATE:
                // 재고개수 확인
                if (SilverPreference.stockCheckingEnabled.getValue(context)) {
                    final ItemOptionRecord optionRecord = ItemDB.getInstance(context).options.getRecordBy(slipItemRecord.toOption().key);
                    etQuantity.addTextChangedListener(new BaseTextWatcher() {
                        @Override
                        public void afterTextChanged(Editable s) {
                            if (enabled == false)
                                return;
                            enabled = false;
                            if (optionRecord.itemId > 0) {
                                int newStockValue = BaseUtils.toInt(s.toString(), Integer.MIN_VALUE); // 공란일 경우는 체크하지 않음
                                if (optionRecord.stock < newStockValue)
                                    MyUI.toastSHORT(context, String.format("재고부족 (현재 %d개)", optionRecord.stock), Gravity.CENTER_VERTICAL);
                            }
                            enabled = true;
                        }
                    });
                }
            default:
                break;
        }
        setVisibility(View.GONE, R.id.module_new_slip_advance_outofstock, R.id.module_new_slip_preorder_outofstock);
        //R.id.module_new_slip_preorder_cancel 도 사용되지는 않지만 미적 측면에서 잔류
    }

    @Override
    public void onDeleteClick(SlipItemRecord slipItemRecord) {
        ((PosSlipDB) posSlipDB).deleteItem(slipItemRecord.id);
        onFinish();
    }

//    @Override
//    protected void setPricingContainer() {
//        pricingContainer.setVisibility(BasePreference.isPricingEnabled(context) && slipItemRecord.itemId > 0 ? View.VISIBLE : View.GONE);
//    }

    private void updateItem(final ItemRecord beforeRecord, final ItemRecord afterRecord, final MyOnTask onTask) {
        new MyServiceTask(context, "updateItem") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                ItemDataProtocol result = itemService.updateItem(userSettingData.getUserShopInfo(), beforeRecord, afterRecord); // 서버
                ItemDB.getInstance(context).update(beforeRecord.id, afterRecord); // DB
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    @Override
    public void onApplyClick(DbActionType actionType, final SlipItemRecord slipItemRecord) {
        // 거래처별 단가 저장 (사용으로 설정되어 있고 단가가 변경되었을때) ... 같은 단가로 계속 사용하면?
        if (rgPriceMode.getCheckedItem() == PriceMode.ByCustomer && slipItemRecord.itemId > 0
                && originalSlipItemRecord.unitPrice != slipItemRecord.unitPrice) {
            PricingRecord record = new PricingRecord(0, userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber(),
                    slipRecord.counterpartPhoneNumber, slipItemRecord.itemId, slipItemRecord.unitPrice, DateTime.now());
            CustomerPriceDB.getInstance(context).updateOrInsert(record);
            slipItemRecord.setPriceDelta(slipItemRecord.unitPrice - originalSlipItemRecord.unitPrice); // 서버 업로드용
            TempUnitPriceByBuyer.getInstance(context).put(slipItemRecord.itemId, slipItemRecord.unitPrice - originalSlipItemRecord.unitPrice); // 마지막이 적용됨
        }

        // 단가가 0인 상품에 단가를 입력한 경우 자동 저장
        ItemRecord beforeRecord = ItemDB.getInstance(context).getRecordBy(slipItemRecord.itemId);
        if (beforeRecord.itemId > 0 && beforeRecord.unitPrice == 0 && slipItemRecord.unitPrice != 0) { // 상품 가격은 0인데 가격이 입력된 경우
            ItemRecord afterRecord = beforeRecord.clone();
            afterRecord.unitPrice = slipItemRecord.unitPrice;
            updateItem(beforeRecord, afterRecord, null); // 같은 값이 들어가면 동작하지 않음
        }

        // SlipItemType - 일반 / 교환 직접 지정
        if (cbShowSingleExchanges.isChecked() == false) {
            switch (dbActionType) {
                case INSERT:
                    switch (slipItemRecord.slipItemType) {
                        default:
                            posSlipDB.items.insert(slipItemRecord);
                            if (doToastResult)
                                MyUI.toastSHORT(context, String.format("[%s, %s, %s] 추가", slipItemRecord.name, slipItemRecord.getColorName(), slipItemRecord.size.uiName));
                            onFinish();
                            break;
                        case RETURN:
                        case ADVANCE_SUBTRACTION:
                        case WITHDRAWAL:
                            splitSlipItemRecord(actionType, posSlipDB.items, doToastResult, new MyOnTask() {
                                @Override
                                public void onTaskDone(Object result) {
                                    onFinish();
                                }
                            });
                            break;
                        case EXCHANGE_SALES: // 교환
                            slipItemRecord.quantity = 1;
                            SlipItemRecord newSlipItemRecord = slipItemRecord.clone();
                            slipItemRecord.slipItemType = SlipItemType.EXCHANGE_RETURN;
                            newSlipItemRecord.slipItemType = SlipItemType.EXCHANGE_SALES;
                            posSlipDB.items.insert(slipItemRecord);
                            posSlipDB.items.insert(newSlipItemRecord);
                            onFinish();
                            break;
                        case EXCHANGE_RETURN_PENDING: // 선교환
                            slipItemRecord.quantity = 1;
                            newSlipItemRecord = slipItemRecord.clone();
                            slipItemRecord.slipItemType = SlipItemType.EXCHANGE_RETURN_PENDING;
                            newSlipItemRecord.slipItemType = SlipItemType.EXCHANGE_SALES;
                            posSlipDB.items.insert(slipItemRecord);
                            posSlipDB.items.insert(newSlipItemRecord);
                            onFinish();
                            break;
                        case EXCHANGE_SALES_PENDING: // 선반품
                            slipItemRecord.quantity = 1;
                            newSlipItemRecord = slipItemRecord.clone();
                            slipItemRecord.slipItemType = SlipItemType.EXCHANGE_RETURN;
                            newSlipItemRecord.slipItemType = SlipItemType.EXCHANGE_SALES_PENDING;
                            posSlipDB.items.insert(slipItemRecord);
                            posSlipDB.items.insert(newSlipItemRecord);
                            onFinish();
                            break;
                    }
                    break;

                case UPDATE:
                    switch (slipItemRecord.slipItemType) {
                        default:
                            posSlipDB.items.update(slipItemRecord.id, slipItemRecord);
                            if (doToastResult)
                                MyUI.toastSHORT(context, String.format("[%s, %s, %s] 수정", slipItemRecord.name, slipItemRecord.getColorName(), slipItemRecord.size.uiName));
                            onFinish();
                            break;
                        case RETURN:
                        case ADVANCE_SUBTRACTION:
                        case WITHDRAWAL:
                            splitSlipItemRecord(actionType, posSlipDB.items, doToastResult, new MyOnTask() {
                                @Override
                                public void onTaskDone(Object result) {
                                    onFinish();
                                }
                            });
                            break;
                        case EXCHANGE_SALES: // 교환
                            slipItemRecord.quantity = 1;
                            SlipItemRecord newSlipItemRecord = slipItemRecord.clone();
                            slipItemRecord.slipItemType = SlipItemType.EXCHANGE_RETURN;
                            newSlipItemRecord.slipItemType = SlipItemType.EXCHANGE_SALES;
                            posSlipDB.items.update(slipItemRecord.id, slipItemRecord);
                            posSlipDB.items.insert(newSlipItemRecord);
                            onFinish();
                            break;
                        case EXCHANGE_RETURN_PENDING: // 선교환
                            slipItemRecord.quantity = 1;
                            newSlipItemRecord = slipItemRecord.clone();
                            slipItemRecord.slipItemType = SlipItemType.EXCHANGE_RETURN_PENDING;
                            newSlipItemRecord.slipItemType = SlipItemType.EXCHANGE_SALES;
                            posSlipDB.items.update(slipItemRecord.id, slipItemRecord);
                            posSlipDB.items.insert(newSlipItemRecord);
                            onFinish();
                            break;
                        case EXCHANGE_SALES_PENDING: // 선반품
                            slipItemRecord.quantity = 1;
                            newSlipItemRecord = slipItemRecord.clone();
                            slipItemRecord.slipItemType = SlipItemType.EXCHANGE_RETURN;
                            newSlipItemRecord.slipItemType = SlipItemType.EXCHANGE_SALES_PENDING;
                            posSlipDB.items.update(slipItemRecord.id, slipItemRecord);
                            posSlipDB.items.insert(newSlipItemRecord);
                            onFinish();
                            break;
                    }
                    break;

                case MULTIPLE_UPDATE: // slipItemRecord의 quantity, unitPrice, itemState를 전체에 적용
                    List<SlipItemRecord> updatedRecords = new ArrayList<>();
                    for (SlipItemRecord record : slipItemRecords) {
                        if (doSkipQuantity == false)
                            record.quantity = slipItemRecord.quantity; // update Quantity
                        record.unitPrice = slipItemRecord.unitPrice;
                        record.slipItemType = slipItemRecord.slipItemType;
                        updatedRecords.add(record);
                    }

                    switch (slipItemRecords.get(0).slipItemType) {
                        default:
                            for (SlipItemRecord record : updatedRecords)
                                posSlipDB.items.update(record.id, record);
                            if (doToastResult)
                                MyUI.toastSHORT(context, String.format("%d개 항목에 적용", updatedRecords.size()));
                            onFinish();
                            break;
                        case EXCHANGE_SALES:
                        case EXCHANGE_RETURN_PENDING:
                        case EXCHANGE_SALES_PENDING:
                            new ExchangeDialog(context, updatedRecords.get(0), updatedRecords.get(1)) {
                                @Override
                                public void onApplyClick(SlipItemRecord record1, SlipItemRecord record2) {
                                    posSlipDB.items.update(record1.id, record1);
                                    posSlipDB.items.update(record2.id, record2);
                                    MyUI.toastSHORT(context, String.format("교환 상품으로 설정되었습니다"));
                                    onFinish();
                                }
                            };
                            break;
                    }
                    break;
            }

        } else { // cbShowSingleExchanges.isChecked()
            switch (dbActionType) {
                case INSERT:
                    posSlipDB.items.insert(slipItemRecord);
                    if (doToastResult)
                        MyUI.toastSHORT(context, String.format("[%s, %s, %s] 추가", slipItemRecord.name, slipItemRecord.getColorName(), slipItemRecord.size.uiName));
                    break;
                case UPDATE:
                    posSlipDB.items.update(slipItemRecord.id, slipItemRecord);
                    if (doToastResult)
                        MyUI.toastSHORT(context, String.format("[%s, %s, %s] 수정", slipItemRecord.name, slipItemRecord.getColorName(), slipItemRecord.size.uiName));
                    break;
                case MULTIPLE_UPDATE:
                    for (SlipItemRecord record : slipItemRecords) {
                        if (doSkipQuantity == false)
                            record.quantity = slipItemRecord.quantity; // update Quantity
                        record.unitPrice = slipItemRecord.unitPrice;
                        record.slipItemType = slipItemRecord.slipItemType;
                        posSlipDB.items.update(record.id, record);
                    }
                    if (doToastResult)
                        MyUI.toastSHORT(context, String.format("%d개 항목에 적용", slipItemRecords.size()));
                    break;
            }
            onFinish();
        }
    }
}

