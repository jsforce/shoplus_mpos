package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.view_base.BaseMallBuyersView;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.SilverAppWatcher;

/**
 * Created by Medivh on 2019-02-25.
 */

public class MallBuyersView extends BaseMallBuyersView {

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, MallBuyersView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SilverAppWatcher.checkInstanceAndRestart(this) == false) return;
        btCancel.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.listview_apply) {
            finish();
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case refresh:
                if (cbShowAll.isChecked()) {
                    downloadAllBuyers();
                } else {
                    downloadRequestedBuyers();
                }
                break;
            case close:
                finish();
                break;
        }
    }


}
