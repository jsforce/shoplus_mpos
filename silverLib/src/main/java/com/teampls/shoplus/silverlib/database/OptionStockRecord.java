package com.teampls.shoplus.silverlib.database;

import android.support.annotation.NonNull;

import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.database.BaseRecord;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2018-07-15.
 */

public class OptionStockRecord implements Comparable<OptionStockRecord> {
    public int id = 0, itemId = 0, stock = 0;
    public DateTime createdDateTime = Empty.dateTime;
    public ColorType color = ColorType.Default;
    public SizeType size = SizeType.Default;
    public String shopPhoneNumber = "";
    public MSortingKey sortingKey = MSortingKey.MultiField;
    // key - shopPhoneNumber, dateTime, itemId, color, size
    // value - stock

    public ItemOptionRecord toOptionRecord() {
        ItemOptionRecord result = new ItemOptionRecord(itemId, color, size);
        result.stock = stock;
        return result;
    }

    public OptionStockRecord(){};

    public OptionStockRecord(int id, String shopPhoneNumber, DateTime createdDateTime, int itemId, ColorType color, SizeType size,
                             int stock) {
        this.shopPhoneNumber = shopPhoneNumber;
        this.createdDateTime = createdDateTime;
        this.id = id;
        this.itemId = itemId;
        this.color = color;
        this.size = size;
        this.stock = stock;
    }

    public OptionStockRecord(String shopPhoneNumber, DateTime createdDateTime, ItemOptionRecord optionRecord, int stock) {
        this.shopPhoneNumber = shopPhoneNumber;
        this.createdDateTime = createdDateTime;
        this.itemId = optionRecord.itemId;
        this.color = optionRecord.color;
        this.size = optionRecord.size;
        this.stock = stock;
    }

    public int getPositiveStock() {
        return Math.max(0, stock);
    }

    public OptionStockRecord(String shopPhoneNumber, DateTime createdDateTime, int itemId) {
        this.shopPhoneNumber = shopPhoneNumber;
        this.createdDateTime = createdDateTime;
        this.itemId = itemId;
    }

    public String getStockSumKey() {
        return String.format("%d.%d.%s", itemId, createdDateTime.getMillis(), shopPhoneNumber);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof OptionStockRecord)) return false;
        OptionStockRecord record = (OptionStockRecord) object;
        if (shopPhoneNumber.equals(record.shopPhoneNumber) == false) return false;
        if (createdDateTime.isEqual(record.createdDateTime) == false) return false;
        if (itemId != record.itemId) return false;
        if (size != record.size) return false;
        return color == record.color;
    }

    public boolean isSame(OptionStockRecord record) {
        return BaseRecord.isSame(this, record);
    }

    @Override
    public int hashCode() {
        int result = itemId;
        result = 31 * result + color.hashCode();
        result = 31 * result + size.hashCode();
        result =31*result + shopPhoneNumber.hashCode();
        result = 31*result + createdDateTime.hashCode();
        return result;
    }

    @Override
    public int compareTo(@NonNull OptionStockRecord record) {
        switch (sortingKey) {
            default:
            case ID:
                return -((Integer) itemId).compareTo(record.itemId);
            case MultiField: // item-color-size
                if (itemId != record.itemId) {
                    return -((Integer) itemId).compareTo(record.itemId);
                } else if (color.compareTo(record.color) != 0) {
                    return (color.compareTo(record.color));
                } else {
                    return size.compareTo(record.size);
                }
        }
    }
}
