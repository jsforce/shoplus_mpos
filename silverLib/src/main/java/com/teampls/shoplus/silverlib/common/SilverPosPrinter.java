package com.teampls.shoplus.silverlib.common;

import android.content.Context;
import android.util.Pair;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.printer.MyPosPrinter;
import com.teampls.shoplus.silverlib.database.SettlementRecord;
import com.teampls.shoplus.silverlib.datatypes.SettlementAuxilityData;

import org.joda.time.DateTime;

import java.util.List;

import jpos.JposException;

/**
 * Created by Medivh on 2019-05-07.
 */

public class SilverPosPrinter extends MyPosPrinter {
    private static SilverPosPrinter instance;

    private SilverPosPrinter(Context context) {
        super(context);
    }

    public static SilverPosPrinter getInstance(Context context) {
        if (instance == null)
            instance = new SilverPosPrinter(context);
        SilverPosPrinter.context = context;
        return instance;
    }

    public void printSettlement(SettlementRecord record, Pair<SettlementAuxilityData, SettlementAuxilityData> vaults, List<SettlementAuxilityData> outgoRecords, List<SettlementAuxilityData> incomeRecords, List<SettlementAuxilityData> entrustRecords ) {
        if (isAvailable() == false)
            return;

        try {
            open(currentProductName);
            claim(0);
            setDeviceEnabled(true);

            // data
            int outgoSum = 0;
            for (SettlementAuxilityData amountRecord : outgoRecords)
                outgoSum = outgoSum + amountRecord.getUnsignedAmount();

            int incomeSum = 0;
            for (SettlementAuxilityData incomeRecord : incomeRecords)
                incomeSum = incomeSum + incomeRecord.getUnsignedAmount(); // 추가수입은 음수로 처리하기로 약속함
            for (SettlementAuxilityData entrustRecord : entrustRecords)
                incomeSum = incomeSum + entrustRecord.getUnsignedAmount();

            printResetLine();
            printDateTime(DateTime.now());
            printLine(BaseUtils.toStringWithWeekDay(record.getPeriod().first, BaseUtils.MD_Kor_Format) + " 마감", EscSeq.Center, EscSeq.Double, EscSeq.Bold);
            printResetLine();
            printLine(String.format("기간: %s ~ %s", record.getPeriod().first.toString(BaseUtils.MDHm_Kor_Format), record.getPeriod().second.toString(BaseUtils.MDHm_Kor_Format)));
            printResetLine();

            printLine(String.format("매출총액: %s", BaseUtils.toCurrencyStr(record.amount)), EscSeq.DoubleWide);
            printLine(String.format("현장결제: %s", BaseUtils.toCurrencyOnlyStr(record.cash)), EscSeq.FontNormal);
            printLine(String.format("온라인청구: %s", BaseUtils.toCurrencyOnlyStr(record.online)), EscSeq.FontNormal);
            printLine(String.format("대납청구: %s", BaseUtils.toCurrencyOnlyStr(record.entrust)), EscSeq.FontNormal);
            printResetLine();

            printLine(String.format("현금결산: %s", BaseUtils.toCurrencyStr(record.cash + incomeSum - outgoSum + vaults.first.amount - vaults.second.amount)), EscSeq.DoubleWide);
            printLine(String.format("현장결제: %s", BaseUtils.toCurrencyOnlyStr(record.cash)), EscSeq.FontNormal);
            printLine(String.format("추가액: %s", BaseUtils.toCurrencySign(incomeSum)), EscSeq.FontNormal);
            printLine(String.format("차감액: %s", BaseUtils.toCurrencySign(-outgoSum)), EscSeq.FontNormal);
            printLine(String.format("시재차액: %s", BaseUtils.toCurrencySign(vaults.first.amount - vaults.second.amount)), EscSeq.FontNormal);
            printResetLine();

            printLine("시재", EscSeq.DoubleWide);
            printLine(String.format("시작: %s", BaseUtils.toCurrencyOnlyStr(vaults.first.amount)), EscSeq.FontNormal);
            printLine(String.format("종료: %s", BaseUtils.toCurrencyOnlyStr(vaults.second.amount)), EscSeq.FontNormal);
            printResetLine();

            printLine("차감액", EscSeq.DoubleWide);
            for (SettlementAuxilityData outgoRecord : outgoRecords)
                printLine(String.format("%s .. %s", outgoRecord.title, BaseUtils.toCurrencyOnlyStr(outgoRecord.amount)), EscSeq.FontNormal);
            printResetLine();

            printLine("추가액", EscSeq.DoubleWide);
            for (SettlementAuxilityData entrustRecord : entrustRecords)
                printLine(String.format("%s .. %s (%s)", entrustRecord.title, BaseUtils.toCurrencyOnlyStr(entrustRecord.getUnsignedAmount()), entrustRecord.getCreatedDateTime().toString(BaseUtils.Hm_Format)), EscSeq.FontNormal);
            for (SettlementAuxilityData amountRecord : incomeRecords)
                printLine(String.format("%s .. %s", amountRecord.title, BaseUtils.toCurrencyOnlyStr(amountRecord.getUnsignedAmount())), EscSeq.FontNormal);
            printResetLine();

            printTail();

        } catch (JposException e) {
            handleError(e);
        } finally {
            try {
                close();
            } catch (JposException e) {
                e.printStackTrace();
            }
        }
    }
}
