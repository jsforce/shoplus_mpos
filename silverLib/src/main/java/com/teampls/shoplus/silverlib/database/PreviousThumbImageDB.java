package com.teampls.shoplus.silverlib.database;

import android.content.Context;

import com.teampls.shoplus.lib.database_global.ImageDB;

public class PreviousThumbImageDB extends ImageDB {
    private static PreviousThumbImageDB instance = null;

    public static PreviousThumbImageDB getInstance(Context context) {
        if (instance == null)
            instance = new PreviousThumbImageDB(context);
        return instance;
    }

    private PreviousThumbImageDB(Context context) {
        super(context, "PreviousThumbImageDB", Ver, Column.toStrs());
        columnAttrs = Column.getAttributions();
    }

    public DBHelper getDBHelper() {
        return new DBHelper(context); // 로컬에 두자
    }

}
