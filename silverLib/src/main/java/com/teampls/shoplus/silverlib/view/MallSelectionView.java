package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.view_base.BaseItemSelectionView;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.common.SilverAppWatcher;
import com.teampls.shoplus.silverlib.database.ItemDBAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2019-02-09.
 */

public class MallSelectionView extends BaseItemSelectionView {

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, MallSelectionView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SilverAppWatcher.checkInstanceAndRestart(this) == false) return;
        rbButton3.setText("추천상품");
        downloadClickedItems();
    }

    protected void setFilterKey(int index) {
        // N.A
    }

    private void downloadClickedItems() {

        new MyServiceTask(context, "onMallSelection", "") {
            private List<Integer> clickedItemIds;
            @Override
            public void doInBackground() throws MyServiceFailureException {
                clickedItemIds = chatbotService.getNewArrivalsItemIds(userSettingData.getUserShopInfo());
            }

            @Override
            public void onPostExecutionUI() {
                adapter.setSortingKey(selectedSortingKey.mSortingKey);
                adapter.generate(false);
                adapter.applyGeneratedRecords();
                adapter.resetClickedPosition(clickedItemIds);  // 최초 generate는 해야 click이 동작하므로... 향후 개선하자
                adapter.notifyDataSetChanged();
                refreshHeader();
            }
        };
    }

    @Override
    protected void setUiComponents() {
        setOnClick(R.id.item_selection_showAll, R.id.item_selection_excludeSent,
                R.id.item_selection_showLast);
        ((RadioButton) findViewById(R.id.item_selection_showAll)).setChecked(true);
        setVisibility(View.GONE, R.id.item_selection_excludeSent);
    }

    @Override
    protected void setAdaptor() {
        if (adapter == null)
            adapter = new ItemDBAdapter(context);
        adapter.showName().showDate().setSelectable();
        adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_ORDERED_MULTIPLE);
        adapter.setViewType(ItemDBAdapter.MallSelectionView);
        adapter.setClickedPositionsLimit(BaseAppWatcher.MallItemCountLimit);
    }

    @Override
    protected void onShowAllClick() {
        adapter.resetFilterItemIds();
        refresh();
    }

    @Override
    public void onShowLast() { // clicked
        adapter.filterItemIds(adapter.getClickedItemIds());
        refresh();
    }

    public void refresh() {
        if (adapter == null) return;
        clickedItemIds = adapter.getClickedItemIds(); // filtering이나 sorting에 의해 clicked position 는 새로 지정해야 하므로
        new MyAsyncTask(context, "MallSelectionView") {
            @Override
            public void doInBackground() {
                adapter.setSortingKey(selectedSortingKey.mSortingKey);
                adapter.generate(false);
            }

            @Override
            public void onPostExecutionUI() {
                adapter.applyGeneratedRecords();
                adapter.resetClickedPosition(clickedItemIds);
                adapter.notifyDataSetChanged();
                refreshHeader();
            }
        };
    }

    @Override
    public void onApplyClick(View view) {
        new MyAlertDialog(context, "선택완료", String.format("%d개를 추천상품으로 올리시겠습니까?\n\n고객 폰에서 추천상품으로 나타납니다.", adapter.getSortedClickedRecords().size())) {
            @Override
            public void yes() {
                new MyServiceTask(context, "onMallSelection", "전송 중...") {

                    @Override
                    public void doInBackground() throws MyServiceFailureException {
                        List<Integer> clickedItemIds = new ArrayList<>();
                        for (ItemRecord record : adapter.getClickedRecords())
                            clickedItemIds.add(record.itemId);
                        chatbotService.updateNewArrivalItemIds(userSettingData.getUserShopInfo(), clickedItemIds);
                    }

                    @Override
                    public void onPostExecutionUI() {
                        MyUI.toastSHORT(context, String.format("추천상품 선택완료"));
                        finish();
                    }
                };

            }
        };
    }
}
