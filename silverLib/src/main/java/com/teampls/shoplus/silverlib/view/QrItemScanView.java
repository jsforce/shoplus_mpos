package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;

import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.datatypes.ItemTraceIdData;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyQrScanner;

import me.dm7.barcodescanner.zbar.Result;

/**
 * Created by Medivh on 2018-11-11.
 */

public class QrItemScanView extends BaseActivity implements MyQrScanner.ResultHandler   {
    private MyQrScanner scannerView;
    private ItemDB itemDB;
    private static MyOnTask<ItemRecord> onTask;

    public static void startActivity(Context context, MyOnTask<ItemRecord> onTask) {
        QrItemScanView.onTask = onTask;
        context.startActivity(new Intent(context, QrItemScanView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.hide();
        itemDB = ItemDB.getInstance(context);

        scannerView = new MyQrScanner(context, this);
        scannerView.addTo(getView(), R.id.empty_container);
    }

    @Override
    public int getThisView() {
        return R.layout.base_empty;
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stop();
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    final int delayTime = 200;
    @Override
    public void handleResult(Result result) {
        final String traceId = result.getContents().toString();
        final ItemTraceIdData scannedItemTraceIdData = new ItemTraceIdData(traceId);
        if (scannedItemTraceIdData.isValid() == false) {
            MyUI.toastSHORT(context, String.format("QR 오류 : %s", result.getContents().toString()));
            MyDevice.beep(70, 150);
            scannerView.resumePreview(delayTime); // 현재 QR에서 화면을 이동할 시간을 주자
            return;
        }

        ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        toneGenerator.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);

        // 상품정보 확인
        ItemRecord itemRecord = itemDB.getRecordBy(scannedItemTraceIdData.getItemOption().getItemId());
        if (itemRecord.id == 0) {
            itemDB.searchRecord(context, scannedItemTraceIdData.getItemOption().getItemId(), new MyOnTask<ItemRecord>() {
                @Override
                public void onTaskDone(ItemRecord result) {
                    if (result.itemId == 0) {
                        MyUI.toastSHORT(context, String.format("타 매장 상품 : %d", scannedItemTraceIdData.getItemOption().getItemId()));
                        scannerView.resumePreview(delayTime);
                        return;
                    } else {
                        onItemFound(result);
                    }
                }
            });
            return;
        } else {
            onItemFound(itemRecord);
        }
    }

    private void onItemFound(ItemRecord itemRecord) {
        if (onTask != null)
            onTask.onTaskDone(itemRecord);
        finish();
    }
}
