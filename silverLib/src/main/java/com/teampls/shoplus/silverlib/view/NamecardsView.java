package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.BaseRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.datatypes.MyShopInformationData;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.SearchStringDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.printer.MyPosPrinter;
import com.teampls.shoplus.lib.viewSetting.BaseSettingView;
import com.teampls.shoplus.lib.viewSetting.PrinterSettingView;
import com.teampls.shoplus.lib.view.ImagePickerView;
import com.teampls.shoplus.lib.view.MyInfoView;
import com.teampls.shoplus.lib.view_module.MyCheckBox;
import com.teampls.shoplus.lib.view_module.MyMessengerSetting;
import com.teampls.shoplus.silverlib.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-04-08.
 */

public class NamecardsView extends BaseSettingView {
    private TextView tvName, tvPhone1, tvPhone2, tvAccount1, tvAccount2, tvAccount3,
            tvAddress, tvLocation, tvMemo, tvImageFilePath;
    private List<String> buildingAddresses = new ArrayList<>();
    private MyShopInformationData shopData;
    private MyPosPrinter myPosPrinter;
    private MyMessengerSetting myMessengerSetting;
    private MyCheckBox cbLargePhoneNumber, cbMemoSpace;

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, NamecardsView.class));
    }

    @Override
    public int getThisView() {
        return R.layout.activity_namecards;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.setTitle("명함 출력");
        myActionBar.hideRefreshTime();
        shopData = userSettingData.cloneShopData();

        // 영수증 생성용 정보
        buildingAddresses = MyInfoView.getBuildingAddresses();
        tvName = findTextViewById(R.id.namecards_name, "");
        tvPhone1 = findTextViewById(R.id.namecards_phoneNumber1, "");
        tvPhone2 = findTextViewById(R.id.namecards_phoneNumber2, "");
        tvAccount1 = findTextViewById(R.id.namecards_account1, "");
        tvAccount2 = findTextViewById(R.id.namecards_account2, "");
        tvAccount3 = findTextViewById(R.id.namecards_account3, "");
        tvAddress = findTextViewById(R.id.namecards_address, "");
        tvLocation = findTextViewById(R.id.namecards_location, "");
        tvMemo = findTextViewById(R.id.namecards_memo, "");
        tvImageFilePath = findTextViewById(R.id.namecards_image_filepath,"");

        cbLargePhoneNumber = new MyCheckBox(context, findCheckBoxById(R.id.namecards_large_phoneNumber), GlobalDB.doEnlargePhoneNumberNameCard);
        cbMemoSpace = new MyCheckBox(context, findCheckBoxById(R.id.namecards_memoSpace), GlobalDB.doAddMemoSpaceNameCard);

        setOnClick(R.id.namecards_close, R.id.namecards_memo, R.id.namecards_name,
                R.id.namecards_phoneNumber1, R.id.namecards_phoneNumber2, R.id.namecards_account1,
                R.id.namecards_account2,  R.id.namecards_account3, R.id.namecards_address, R.id.namecards_location,
                R.id.namecards_print, R.id.namecards_close
        );

        myMessengerSetting = new MyMessengerSetting(context, getView());
        setMyPosPrinter(globalDB.getValue(GlobalDB.KEY_PRINTER_Selected));
        setOnClick(R.id.namecards_image_filepath);
        setCheckBox(R.id.namecards_image_checkbox, GlobalDB.KEY_PRINTER_QR_Image, false);
    }

    private void setMyPosPrinter(String deviceName) {
        myPosPrinter = MyPosPrinter.getInstance(context);
    }

    private void refresh() {
        BaseUtils.setText(tvName, "매장명 : ", shopData.getShopName());
        BaseUtils.setText(tvPhone1, "매장번호1 : ", shopData.getShopPhoneNumber(0));
        BaseUtils.setText(tvPhone2, "매장번호2 : ", shopData.getShopPhoneNumber(1));
        BaseUtils.setText(tvAccount1, "계좌1 : ", shopData.getAccountNumber(0));
        BaseUtils.setText(tvAccount2, "계좌2 : ", shopData.getAccountNumber(1));
        BaseUtils.setText(tvAccount3, "계좌3 : ", shopData.getAccountNumber(2));
        BaseUtils.setText(tvAddress, "건물주소 : ", shopData.getBuildingAddress());
        BaseUtils.setText(tvLocation, "매장위치 : ", shopData.getDetailedAddress());
        BaseUtils.setText(tvMemo, "메모 : ", shopData.getNotice());

        myMessengerSetting.refresh();

        String imageFilePath = globalDB.getValue(GlobalDB.KEY_PRINTER_QR_Image_Path);
        if (new File(imageFilePath).exists() == false) {
            globalDB.put(GlobalDB.KEY_PRINTER_QR_Image, false);
            globalDB.put(GlobalDB.KEY_PRINTER_QR_Image_Path, "");
            imageFilePath = "";
        }

        if (imageFilePath.isEmpty()) {
            tvImageFilePath.setText("(미입력)");
            tvImageFilePath.setTextColor(ColorType.Gray.colorInt);
        } else {
            tvImageFilePath.setText(MyDevice.getFileNameOnly(imageFilePath, true));
            tvImageFilePath.setTextColor(ColorType.Black.colorInt);
        }

    }

    @Override
    protected void onDestroy() {
        if (myPosPrinter != null)
            myPosPrinter.disconnectPrinter();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.printSetting)
                .add(MyMenu.contactUs)
                .add(MyMenu.close);
    }

    private void checkShopDataAndFinish() {
        if (BaseRecord.isSame(shopData, userSettingData.getWorkingShop()) == false) {
            new MyAlertDialog(context, "변경 내용 저장", "변경된 내용이 있습니다. 저장하시겠습니까?") {
                @Override
                public void yes() {
                    userSettingData.updateWorkingShopData(shopData, new MyOnTask() {
                        @Override
                        public void onTaskDone(Object result) {
                            MyUI.toastSHORT(context, String.format("설정을 적용했습니다"));
                            finish();
                        }
                    });
                }

                @Override
                public void no() {
                    finish();
                }
            };
        } else {
            finish();
        }
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case printSetting:
                PrinterSettingView.startActivity(context);
                break;
            case contactUs:
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                break;
            case close:
                checkShopDataAndFinish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        checkShopDataAndFinish();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view); // 이거 빠지면 메뉴 동작안함, 주의

        // shopData는 마지막에 저장
        if (view.getId() == R.id.namecards_name) {
            new UpdateValueDialog(context, "매장명 입력", "", shopData.getShopName()) {
                public void onApplyClick(String newStr) {
                    shopData.setShopName(newStr);
                    refresh();
                }
            };

        } else if (view.getId() == R.id.namecards_close) {
            checkShopDataAndFinish();

        } else if (view.getId() == R.id.namecards_phoneNumber1) {
            new UpdateValueDialog(context, "전화번호 입력", "", shopData.getShopPhoneNumber(0)) {
                public void onApplyClick(String newStr) {
                    shopData.updatePhoneNumber(0, newStr);
                    refresh();
                }
            };

        } else if (view.getId() == R.id.namecards_phoneNumber2) {
            new UpdateValueDialog(context, "전화번호 입력", "", shopData.getShopPhoneNumber(1)) {
                public void onApplyClick(String newStr) {
                    shopData.updatePhoneNumber(1, newStr);
                    refresh();
                }
            };

        } else if (view.getId() == R.id.namecards_account1) {
            new UpdateValueDialog(context, "계좌 입력", "", shopData.getAccountNumber(0)) {
                public void onApplyClick(String newStr) {
                    shopData.updateAccountNumber(0, newStr);
                    refresh();
                }
            };

        } else if (view.getId() == R.id.namecards_account2) {
            new UpdateValueDialog(context, "계좌 입력", "", shopData.getAccountNumber(1)) {
                public void onApplyClick(String newStr) {
                    shopData.updateAccountNumber(1, newStr);
                    refresh();
                }
            };

        } else if (view.getId() == R.id.namecards_account3) {
            new UpdateValueDialog(context, "계좌 입력", "", shopData.getAccountNumber(2)) {
                public void onApplyClick(String newStr) {
                    shopData.updateAccountNumber(2, newStr);
                    refresh();
                }
            };

        } else if (view.getId() == R.id.namecards_address) {
            new SearchStringDialog(context, "주소 입력", buildingAddresses) {
                @Override
                public void onItemClick(String string) {
                    String buildingAddress = string;
                    if (string.contains("]")) {
                        String buildingName = string.split("]")[0].replace("[", "");
                        buildingAddress = string.split("]")[1].trim();
                        if (shopData.getDetailedAddress().isEmpty()) // 건물이름 자동으로 등록
                            shopData.setDetailedAddress(buildingName);
                    }
                    shopData.setBuildingAddress(buildingAddress);
                    refresh();
                }
            };

        } else if (view.getId() == R.id.namecards_location) {
            new UpdateValueDialog(context, "매장 위치 입력", "", shopData.getDetailedAddress()) {
                public void onApplyClick(String newStr) {
                    shopData.setDetailedAddress(newStr);
                    refresh();
                }
                SlipItemType a;
            };

        } else if (view.getId() == R.id.namecards_memo) {
            new UpdateValueDialog(context, "메모 입력", "", shopData.getNotice()) {
                public void onApplyClick(String newStr) {
                    shopData.setNotice(newStr);
                    refresh();
                }
            };

        } else if (view.getId() == R.id.namecards_image_filepath) {
            MyUI.toast(context, String.format("500x500 이하의 작은 흑백 이미지로 선택해주세요"));
            ImagePickerView.start(context, 1, true, new MyOnClick<List<String>>() {
                @Override
                public void onMyClick(View view, List<String> imagePaths) {
                    if (imagePaths.size() == 0) // 시작 옵션에 의해 발생하지는 않는다
                        return;
                    String filePath = imagePaths.get(0);
                    globalDB.put(GlobalDB.KEY_PRINTER_QR_Image_Path, filePath);
                    refresh();
                }
            });

        } else if (view.getId() == R.id.namecards_print) {
            myPosPrinter.printNameCards();
            
        } else {

        }
    }

}
