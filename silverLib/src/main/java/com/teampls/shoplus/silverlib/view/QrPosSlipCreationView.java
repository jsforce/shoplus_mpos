package com.teampls.shoplus.silverlib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.dialog.MyCheckableListDialog;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.PosSlipItemDB;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_memory.MSlipDB;
import com.teampls.shoplus.lib.datatypes.ItemTraceData;
import com.teampls.shoplus.lib.datatypes.ItemTraceIdData;
import com.teampls.shoplus.lib.datatypes.ItemTraceRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.enums.ItemTraceState;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.MyCheckBox;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyQrScanner;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.database.PosSlipItemDBAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import me.dm7.barcodescanner.zbar.Result;

/**
 * Created by Medivh on 2018-08-08.
 */

public class QrPosSlipCreationView extends BaseActivity implements AdapterView.OnItemClickListener, MyQrScanner.ResultHandler {
    private ListView lvSlipItems;
    private MyQrScanner scannerView;
    private ItemDB itemDB;
    private TextView tvSummary, tvInfo, tvScan;
    private PosSlipItemDBAdapter adapter;
    private static SlipRecord slipRecord;
    private static MyOnTask onApplyClickTask;
    private Set<String> othersTraceIds = new HashSet<>(), myTraceIds = new HashSet<>();
    private HashMap<String, String> traceIdSlipKeyMap = new HashMap<>();
    private MyCheckBox cbFastSale;

    enum Mode {New, Pendings}

    private MyRadioGroup<Mode> rgMode;
    private List<SlipItemRecord> newRecords = new ArrayList<>(), pendingRecords = new ArrayList<>();

    public static void startActivity(Context context, SlipRecord slipRecord, MyOnTask<List<SlipItemRecord>> onApplyClickTask) {
        QrPosSlipCreationView.slipRecord = slipRecord;
        QrPosSlipCreationView.onApplyClickTask = onApplyClickTask;
        ((Activity) context).startActivityForResult(new Intent(context, QrPosSlipCreationView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.hide();
        itemDB = ItemDB.getInstance(context);

        List<SlipRecord> slipRecords = PosSlipDB.getInstance(context).getRecords(PosSlipDB.Column.confirmed, false);
        for (SlipItemRecord slipItemRecord : PosSlipDB.getInstance(context).items.getRecordsBy(slipRecords)) {
            if (slipItemRecord.getSlipKey().equals(slipRecord.getSlipKey())) {
                myTraceIds.addAll(slipItemRecord.getItemTraces()); // 중복등록 방지용
                if (SlipItemType.getQrPendings().contains(slipItemRecord.slipItemType))
                    pendingRecords.add(slipItemRecord);
            } else {
                othersTraceIds.addAll(slipItemRecord.getItemTraces()); // 중복등록 방지용
                for (String traceId : slipItemRecord.getItemTraces())
                    traceIdSlipKeyMap.put(traceId, slipRecord.getSlipKey().toSID()); // 중복했을때 정보 제공용
            }
        }

        adapter = new PosSlipItemDBAdapter(context, MSlipDB.getInstance(context));

        scannerView = new MyQrScanner(context, this);
        scannerView.addTo(getView(), R.id.qr_slip_creation_scanContainer);
        lvSlipItems = (ListView) findViewById(R.id.qr_slip_creation_listview);
        lvSlipItems.getLayoutParams().height = MyDevice.getHeight(context) / 3;
        lvSlipItems.setOnItemClickListener(this);
        lvSlipItems.setAdapter(adapter);

        cbFastSale = new MyCheckBox(keyValueDB, findCheckBoxById(R.id.qr_slip_creation_fastSale), "qrpos.slip.creation.fastsale", true);
        UserRecord counterpart = userDB.getRecord(slipRecord.counterpartPhoneNumber);
        findTextViewById(R.id.qr_slip_creation_counterpart, String.format("%s", counterpart.getName()));
        tvSummary = findTextViewById(R.id.qr_slip_creation_summary, "");
        tvInfo = findTextViewById(R.id.qr_slip_creation_info, "");
        tvScan = findTextViewById(R.id.qr_slip_creation_scan);

        rgMode = new MyRadioGroup<>(context, getView());
        rgMode.add(R.id.qr_slip_creation_newMode, Mode.New)
                .add(R.id.qr_slip_creation_pendingMode, Mode.Pendings);
        rgMode.setChecked(Mode.New);
        rgMode.setOnClickListener(false, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });

        setOnClick(R.id.qr_slip_creation_cancel, R.id.qr_slip_creation_apply,
                R.id.qr_slip_creation_clear, R.id.qr_slip_creation_summary,
                R.id.qr_slip_creation_scan);

        refresh();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        new OnItemClickDialog(context, adapter.getRecord(position)).show();
    }

    class OnItemClickDialog extends MyButtonsDialog {

        public OnItemClickDialog(Context context, final SlipItemRecord record) {
            super(context, "QR 스캔 기록", record.toNameOptionQuantityString(" / "));
            scannerView.stopCameraPreview();
            switch (rgMode.getCheckedItem()) {
                case New:
                    addButton("삭  제", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            newRecords.remove(record);
                            myTraceIds.removeAll(record.getItemTraces());
                            QrPosSlipCreationView.this.refresh();
                            dismiss();
                            scannerView.resumePreview(0);
                        }
                    });
                case Pendings:
                    addButton("스캔 지우기", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            myTraceIds.removeAll(record.getItemTraces());
                            record.traceIds.clear();
                            QrPosSlipCreationView.this.refresh();
                            dismiss();
                            scannerView.resumePreview(0);
                        }
                    });
                    break;
            }

        }

        @Override
        public void onCloseClick() {
            scannerView.resumePreview(0);
        }
    }

    @Override
    public int getThisView() {
        return R.layout.activity_qr_slip_creation;
    }

    public void refresh() {
        if (adapter == null) return;
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                switch (rgMode.getCheckedItem()) {
                    case New:
                        adapter.setViewType(PosSlipItemDBAdapter.QrCreationViewNew);
                        adapter.setRecords(newRecords);
                        break;
                    case Pendings:
                        adapter.setViewType(PosSlipItemDBAdapter.QrCreationViewPendings);
                        adapter.setRecords(pendingRecords);
                        break;
                }
            }

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
                refreshTextViews();
            }
        };
    }

    private void refreshTextViews() {
        SlipSummaryRecord newSummary = new SlipSummaryRecord(newRecords);
        SlipSummaryRecord pendingSummary = new SlipSummaryRecord(pendingRecords);
        tvSummary.setText(String.format("%s건 %s", newSummary.count + pendingSummary.count,
                BaseUtils.toCurrencyStr(newSummary.amount + pendingSummary.amount)));
        rgMode.setText(Mode.New, String.format("추가(%d)", newSummary.traceIdCount));
        rgMode.setText(Mode.Pendings, String.format("확인(%d/%d)", pendingSummary.traceIdCount, pendingSummary.quantity));

        switch (rgMode.getCheckedItem()) {
            case New:
                adapter.setViewType(PosSlipItemDBAdapter.QrCreationViewNew);
                adapter.setRecords(newRecords);
                break;
            case Pendings:
                adapter.setViewType(PosSlipItemDBAdapter.QrCreationViewPendings);
                adapter.setRecords(pendingRecords);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stop();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.qr_slip_creation_cancel) {
            scannerView.stopCameraPreview();
            new MyAlertDialog(context, "입력 취소", "입력을 취소하시겠습니까?") {
                @Override
                public void yes() {
                    doFinishForResult();
                }

                public void no() {
                    scannerView.resumePreview(0);
                }
            };

        } else if (view.getId() == R.id.qr_slip_creation_apply) {
            scannerView.stopCameraPreview();
            new MyAlertDialog(context, "QR 스캔 완료", "현재 스캔한 내용을 영수증에 추가하시겠습니까?") {
                @Override
                public void yes() {
                    for (SlipItemRecord record : pendingRecords)
                        PosSlipItemDB.getInstance(context).update(record.id, record);
                    if (onApplyClickTask != null)
                        onApplyClickTask.onTaskDone(newRecords);
                    doFinishForResult();
                }

                @Override
                public void no() {
                    scannerView.resumePreview(0);
                }
            };

        } else if (view.getId() == R.id.qr_slip_creation_summary) {

        } else if (view.getId() == R.id.qr_slip_creation_clear) {
            new MyAlertDialog(context, "스캔항목 전체 삭제", "스캔 항목을 모두 지우시겠습니까?") {
                @Override
                public void yes() {
                    // refreshCurrentDevice myTraceId
                    for (SlipItemRecord slipItemRecord : PosSlipDB.getInstance(context).items.getRecordsBy(slipRecord.getSlipKey().toSID()))
                        myTraceIds.addAll(slipItemRecord.getItemTraces()); // 중복등록 방지용
                    newRecords.clear();
                    for (SlipItemRecord record : pendingRecords)
                        record.traceIds.clear();
                    refresh();
                }
            };

        } else if (view.getId() == R.id.qr_slip_creation_scan) {
            if (tvScan.getText().toString().equals("스캔멈춤")) {
                tvScan.setText("스캔시작");
                scannerView.stopCameraPreview();
            } else {
                tvScan.setText("스캔멈춤");
                scannerView.resumePreview(0);
            }
        }
    }

    @Override
    public void onMyMenuCreate() {
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
    }


    final int delayTime = 200;

    @Override
    public void handleResult(Result result) {
        final String traceId = result.getContents().toString();
        final ItemTraceIdData scannedItemTraceIdData = new ItemTraceIdData(traceId);

        if (scannedItemTraceIdData.isValid() == false) {
            MyUI.toastSHORT(context, String.format("QR 오류 : %s", result.getContents().toString()));
            MyDevice.beep(70, 150);
            scannerView.resumePreview(delayTime); // 현재 QR에서 화면을 이동할 시간을 주자
            return;
        }

        if (myTraceIds.contains(traceId)) {
            MyUI.toastSHORT(context, String.format("이미 추가된 상품입니다"));
            MyDevice.beep(70, 150);
            scannerView.resumePreview(delayTime); // 현재 QR에서 화면을 이동할 시간을 주자
            return;
        }

        if (othersTraceIds.contains(traceId)) {
            SlipRecord slipRecord = PosSlipDB.getInstance(context).getRecordByKey(traceIdSlipKeyMap.get(traceId));
            UserRecord counterpart = UserDB.getInstance(context).getRecord(slipRecord.counterpartPhoneNumber);
            if (counterpart.isEmpty())
                MyUI.toastSHORT(context, String.format("다른 영수증에 추가된 상품입니다"));
            else
                MyUI.toastSHORT(context, String.format("%s 영수증에 추가된 상품입니다", counterpart.getName()));
            MyDevice.beep(70, 150);
            scannerView.resumePreview(delayTime); // 현재 QR에서 화면을 이동할 시간을 주자
            return;
        }

        ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        toneGenerator.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);

        // 상품정보 확인
        ItemRecord itemRecord = itemDB.getRecordBy(scannedItemTraceIdData.getItemOption().getItemId());
        if (itemRecord.id == 0) {
            itemDB.searchRecord(context, scannedItemTraceIdData.getItemOption().getItemId(), new MyOnTask<ItemRecord>() {
                @Override
                public void onTaskDone(ItemRecord result) {
                    if (result.itemId == 0) {
                        MyUI.toastSHORT(context, String.format("타 매장 상품 : %d", scannedItemTraceIdData.getItemOption().getItemId()));
                        scannerView.resumePreview(delayTime);
                        return;
                    } else {
                        switch (rgMode.getCheckedItem()) {
                            case New:
                                checkItemTraceData(traceId, result);
                                break;
                            case Pendings:
                                matchSlipItem(traceId, result);
                                break;
                        }

                    }
                }
            });
            return;
        } else {
            switch (rgMode.getCheckedItem()) {
                case New:
                    checkItemTraceData(traceId, itemRecord);
                    break;
                case Pendings:
                    matchSlipItem(traceId, itemRecord);
                    break;
            }
        }
    }

    final int scanDisplayDelay = 200;

    private void matchSlipItem(final String traceId, final ItemRecord itemRecord) {
        ItemOptionRecord optionRecord = new ItemTraceIdData(traceId).getItemOption().toOptionRecord();
        tvInfo.setText(String.format("%s/%s/%s", itemRecord.getUiName(), optionRecord.color.getUiName(), optionRecord.size.uiName));
        boolean found = false;
        for (SlipItemRecord record : pendingRecords) {
            if (record.toOption().equals(optionRecord)) {
                found = true;
                if (record.traceIds.contains(traceId)) {
                    MyUI.toastSHORT(context, String.format("이미 추가된 상품입니다"));
                    break;
                }

                if (record.traceIds.size() >= record.quantity) {
                    MyUI.toastSHORT(context, String.format("최대 개수입니다"));
                    break;
                }

                record.attachItemTrace(traceId);
                lvSlipItems.setSelection(pendingRecords.indexOf(record));
                break;
            }
        }
        if (found == false)
            MyUI.toastSHORT(context, String.format("목록에 없는 상품입니다"));
        refresh();
        scannerView.resumePreview(scanDisplayDelay);
    }

    // 이미 판매된 제품인지 검토
    private void checkItemTraceData(final String traceId, final ItemRecord itemRecord) {

        new MyServiceTask(context) {
            ItemTraceData traceData;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                traceData = service.getTrace(userSettingData.getUserShopInfo(), traceId);
            }

            @Override
            public void onPostExecutionUI() {
                ItemOptionRecord optionRecord = traceData.getTraceIdData().getItemOption().toOptionRecord();
                final ItemTraceRecord traceRecord = new ItemTraceRecord(traceData);
                tvInfo.setText(String.format("%s/%s/%s/%s(%s)",
                        itemRecord.getUiName(), optionRecord.color.getUiName(), optionRecord.size.uiName,
                        traceData.getState().uiStr, traceRecord.getLocation(context)));

                String alertTitle = "", alertMessage = "";
                boolean doShowAlertDialog = false;
                switch (traceData.getState()) {
                    case SOLD:
                        alertTitle = "판매된 상품";
                        alertMessage = String.format("[%s]에 판매된 상품입니다. 무시하고 계속 판매를 진행하시겠습니까?", traceRecord.getLocation(context));
                        doShowAlertDialog = true;
                        break;
                    case NOT_FOUND:
                        alertTitle = "못찾은 상품";
                        alertMessage = "이 상품은 이전에 못찾은 상품입니다. 판매를 진행하시겠습니까?";
                        doShowAlertDialog = true;
                        break;
                    case HIDDEN:
                        alertTitle = "삭제된 상품";
                        alertMessage = "이 상품은 이전에 삭제된 상품입니다. 판매를 진행하시겠습니까?";
                        doShowAlertDialog = true;
                        break;
                    default:
                        break;
                }

                if (doShowAlertDialog) {
                    new MyAlertDialog(context, alertTitle, alertMessage) {
                        @Override
                        public void yes() {
                            addTraceRecord(itemRecord, traceRecord);
                        }

                        @Override
                        public void no() {
                            scannerView.resumePreview(0);
                        }
                    };
                } else {
                    addTraceRecord(itemRecord, traceRecord);
                }
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                if (e.getErrorCode().contains("NotRegistered")) {
                    final ItemTraceRecord traceRecord = new ItemTraceRecord(traceId, ItemTraceState.DEFAULT, ""); // 기존 Location 없음
                    tvInfo.setText(String.format("%s/%s/%s/미등록",
                            itemRecord.getUiName(), traceRecord.color.getUiName(), traceRecord.size.uiName));

                    new MyAlertDialog(context, "미등록 상품", "미등록 상품입니다. 판매를 진행하시겠습니까?") {
                        @Override
                        public void yes() {
                            addTraceRecord(itemRecord, traceRecord);
                        }

                        @Override
                        public void no() {
                            scannerView.resumePreview(0);
                        }
                    };
                } else {
                    super.catchException(e);
                    scannerView.resumePreview(scanDisplayDelay); // 스캔이 중단되어 있으면 안됨
                }
            }
        };
    }

    private void addTraceRecord(final ItemRecord itemRecord, final ItemTraceRecord traceRecord) {
        final SlipItemRecord slipItemRecord = new SlipItemRecord(itemRecord, traceRecord, slipRecord);
        slipItemRecord.quantity = 1;
        if (cbFastSale.getCheckBox().isChecked()) {
            slipItemRecord.slipItemType = SlipItemType.SALES;
            int position = addRecord(slipItemRecord);
            lvSlipItems.setSelection(position);
            myTraceIds.add(traceRecord.traceId);
            refresh();
            scannerView.resumePreview(scanDisplayDelay);
        } else {
            new MyCheckableListDialog<SlipItemType>(context, "속성 선택", SlipItemType.getQrRelated(), SlipItemType.SALES, "") {
                @Override
                public void onItemClicked(@Nullable SlipItemType selectedRecord) {
                    slipItemRecord.slipItemType = selectedRecord;
                    int position = addRecord(slipItemRecord);
                    lvSlipItems.setSelection(position);
                    myTraceIds.add(traceRecord.traceId);
                    refresh();
                    scannerView.resumePreview(scanDisplayDelay);
                }

                @Override
                protected String getUiString(SlipItemType record) {
                    return record.uiName;
                }
            };
        }
    }

    private int addRecord(SlipItemRecord slipItemRecord) {
        // 동일 아이템이 있는지 검사
        int position = 0;
        boolean found = false;

        // toSummaryRecords
        for (SlipItemRecord prevRecord : newRecords) {
            // found
            if (prevRecord.equals(slipItemRecord) && prevRecord.slipItemType == slipItemRecord.slipItemType) { // 이전 목록에서 동일 아이템 발견
                if (slipItemRecord.getItemTraces().size() == 1) {
                    prevRecord.traceIds.addAll(slipItemRecord.getItemTraces());
                    prevRecord.quantity = prevRecord.getItemTraces().size();
                    position = newRecords.indexOf(prevRecord);
                    found = true;
                } else {
                    MyUI.toastSHORT(context, String.format("QR 정보에 오류가 있습니다"));
                }
                break;
            }
        }
        // not found
        if (found == false) {
            slipItemRecord.quantity = 1;
            newRecords.add(slipItemRecord);
            position = newRecords.size() - 1;
        }
        return position;
    }
}
