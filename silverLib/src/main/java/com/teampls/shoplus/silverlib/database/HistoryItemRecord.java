package com.teampls.shoplus.silverlib.database;

import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by Medivh on 2019-07-03.
 */

public class HistoryItemRecord  {
    public int id = 0, itemId = 0;
    public String name = "";

    public HistoryItemRecord() {}

    public HistoryItemRecord(int id, int itemId, String name) {
        this.id = id;
        this.itemId = itemId;
        this.name = name;
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %d, itemid %d, %s", callLocation, id, itemId, name));
    }

}