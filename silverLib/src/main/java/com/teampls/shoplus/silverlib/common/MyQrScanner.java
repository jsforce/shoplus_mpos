package com.teampls.shoplus.silverlib.common;

import android.content.Context;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.LinearLayout;

import com.teampls.shoplus.lib.common.MyDelayTask;

import java.util.Arrays;

import me.dm7.barcodescanner.zbar.BarcodeFormat;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by Medivh on 2017-10-13.
 */

public class MyQrScanner extends ZBarScannerView {
    private Context context;
    private boolean activated = false;
    private ZBarScannerView.ResultHandler resultHandler;

    public MyQrScanner(Context context, ZBarScannerView.ResultHandler resultHandler) {
        super(context);
        this.context = context;
        this.resultHandler = resultHandler;
    }

    public void addTo(View view, @IdRes int containerResId) {
        ((LinearLayout) view.findViewById(containerResId)).addView(this);
    }

    public void start() {
        if (activated == false) {
            setResultHandler(resultHandler);
            setFormats(Arrays.asList(new BarcodeFormat[]{BarcodeFormat.QRCODE}));
            setAutoFocus(true);
            startCamera();
            activated = true;
        }
    }

    public void stop() {
        stopCamera();
        activated = false;
    }

    public void resumePreview(int delayMills) {
        resumeCameraPreview(null);
        new MyDelayTask(context, delayMills) {
            @Override
            public void onFinish() {
                resumeCameraPreview(resultHandler);
            }
        };
    }
}
