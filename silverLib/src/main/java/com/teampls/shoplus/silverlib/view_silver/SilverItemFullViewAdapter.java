package com.teampls.shoplus.silverlib.view_silver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.LocalItemRecord;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_global.ImageRecord;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.BaseItemOptionDialog;
import com.teampls.shoplus.lib.dialog.CategoryDialog;
import com.teampls.shoplus.lib.dialog.ItemNameDialog;
import com.teampls.shoplus.lib.dialog.UpdatePriceDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnClickTriple;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.view.ImageZoomView;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseFullViewAdapter;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import com.teampls.shoplus.lib.view_module.MyCheckBox;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;
import com.teampls.shoplus.lib.view_module.MyKeyValueEditText;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.common.SilverPreference;
import com.teampls.shoplus.silverlib.database.ItemOptionDBAdapter;
import com.teampls.shoplus.silverlib.dialog.OptionQuantityDialog;
import com.teampls.shoplus.silverlib.view.ItemCartView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-03-27.
 */

abstract public class SilverItemFullViewAdapter extends BaseFullViewAdapter<ItemRecord> {
    protected ItemDB itemDB;
    protected boolean doEnableSlip = true;

    protected enum OptionMode {Stock, Slip}

    protected OptionMode optionMode = OptionMode.Stock;

    public SilverItemFullViewAdapter(Context context, List<ItemRecord> records, boolean doEnableSlip) {
        super(context, records);
        itemDB = ItemDB.getInstance(context);
        this.doEnableSlip = doEnableSlip;
    }

    @Override
    public ItemRecord getEmptyRecord() {
        return new ItemRecord();
    }

    @Override
    protected int getThisView() {
        return R.layout.activity_itemfullview;
    }

    public abstract void startPreference();

    //  ViewHolder가 실질적인 ItemFullView 역할을 수행 (메뉴 기능은 FullViewPager에 있음)
    protected class SilverFullViewHolder extends BaseViewHolder {
        public ItemOptionDBAdapter optionStockAdapter, optionSlipAdapter;
        public ExpandableHeightListView lvOptionStock, lvOptionSlip;
        public ScrollView scrollView;
        public Bitmap bitmap = Empty.bitmap;
        public TextView tvItemName, tvNote, tvPrice, tvSerial, tvCategory, tvMode, tvNotice,
                tvSaveOption, tvClearOption, tvCost ;
        public MyRadioGroup<ItemStateType> stateTypes;
        public ImageView imageView;
        public View view;
        private int itemId = 0;
        private CheckBox cbOptionMode, cbBookmark, cbDiscount;

        @Override
        public void init(final View view) {
            this.view = view;
            stateTypes = new MyRadioGroup<>(context, view, 1.0, this);

            // 재고 listView
            optionStockAdapter = new ItemOptionDBAdapter(context);
            optionStockAdapter.setViewType(ItemOptionDBAdapter.ItemFullViewStock);
            optionStockAdapter.splitUnconfirmed();
            optionStockAdapter.setOnRowClick(new MyOnClick() {
                @Override
                public void onMyClick(View view, Object record) {
                    refreshAdapter();
                }
            });
            lvOptionStock = (ExpandableHeightListView) view.findViewById(R.id.itemfullview_options_stock);
            lvOptionStock.setAdapter(optionStockAdapter);
            lvOptionStock.setExpanded(true);
            lvOptionStock.setFocusable(false);

            // 장바구니 listView
            optionSlipAdapter = new ItemOptionDBAdapter(context);
            optionSlipAdapter.setViewType(ItemOptionDBAdapter.ItemFullViewSlip);
            lvOptionSlip = (ExpandableHeightListView) view.findViewById(R.id.itemfullview_options_slip);
            lvOptionSlip.setAdapter(optionSlipAdapter);
            lvOptionSlip.setExpanded(true);

            scrollView = (ScrollView) view.findViewById(R.id.itemfullview_scrollview);
            imageView = (ImageView) view.findViewById(R.id.itemfullview_image);
            tvItemName = setText(R.id.itemfullview_name, "");
            tvPrice = setText(R.id.itemfullview_price, "");
            tvNote = setText(R.id.itemfullview_note, "");
            tvSerial = setText(R.id.itemfullview_serial, "");
            tvCategory = setText(R.id.itemfullview_category, "");
            tvMode = setText(R.id.itemfullview_table_stock, "");
            tvSaveOption = _findViewById(R.id.itemfullview_saveOption);
            tvClearOption = _findViewById(R.id.itemfullview_clearOption);
            tvNotice = setText(R.id.itemfullview_notice, "");
            tvCost = (TextView) view.findViewById(R.id.itemfullview_cost);
            cbOptionMode = (CheckBox) view.findViewById(R.id.itemfullview_optionMode);
            cbOptionMode.setChecked(optionMode == OptionMode.Slip);
            cbBookmark = view.findViewById(R.id.itemfullview_bookmark);
            cbDiscount = view.findViewById(R.id.itemfullview_discount);

            MyView.setTextViewByDeviceSizeScale(context, 0.5, tvItemName, tvPrice, tvCategory, tvNote, tvSaveOption, tvClearOption,
                    tvNotice, tvCost, tvSerial, cbBookmark);
            MyView.setTextViewByDeviceSizeScale(context, view, 0.5, R.id.itemfullview_table_color, R.id.itemfullview_table_size,
                    R.id.itemfullview_table_stock, R.id.itemfullview_optionMode);
            MyView.setColorFilter(view.findViewById(R.id.itemfullview_addCart), ColorType.SkyBlue);

            stateTypes.add(R.id.itemfullview_state_preparing, ItemStateType.PREPARING)
                    .add(R.id.itemfullview_state_displayed, ItemStateType.DISPLAYED)
                    .add(R.id.itemfullview_state_stored, ItemStateType.STORED)
                    .add(R.id.itemfullview_state_soldout, ItemStateType.SOLDOUT);
            stateTypes.setAsNormalFont(context);

            setOnClick(view, R.id.itemfullview_image, R.id.itemfullview_add_option, R.id.itemfullview_saveOption, R.id.itemfullview_note,
                    R.id.itemfullview_name, R.id.itemfullview_price, R.id.itemfullview_category, R.id.itemfullview_close,
                    R.id.itemfullview_addCart, R.id.itemfullview_shareMain, R.id.itemfullview_clearOption,
                    R.id.itemfullview_optionMode, R.id.itemfullview_cost, R.id.itemfullview_bookmark, R.id.itemfullview_discount
                    );

            if (doEnableSlip == false) {
                optionMode = OptionMode.Stock;
                setVisibility(view, View.GONE, R.id.itemfullview_addCart, R.id.itemfullview_optionMode);
                tvNotice.setText("장바구니 기능은 비활성화 됐습니다");
            } else {
                tvNotice.setText("컬러이름 클릭 : 재고변동보기, 삭제");
            }

            if (BaseAppWatcher.isNetworkConnected() == false) {
                setEnabled(view, false, R.id.itemfullview_state_preparing, R.id.itemfullview_state_displayed,
                        R.id.itemfullview_state_stored, R.id.itemfullview_state_soldout, R.id.itemfullview_name,
                        R.id.itemfullview_price, R.id.itemfullview_cost, R.id.itemfullview_category, R.id.itemfullview_note
                        );
                setVisibility(view, View.GONE, R.id.itemfullview_addCart, R.id.itemfullview_add_option,
                        R.id.itemfullview_saveOption, R.id.itemfullview_optionMode);
            }
        }

        @Override
        public int getThisView() {
            return R.layout.activity_itemfullview;
        }

        @Override
        public void setYPos(int y) {
            scrollView.scrollTo(0, y);
        }

        @Override
        public void update(int position) {
            this.position = position;
            final ItemRecord itemRecord = getRecord(position);
            itemId = itemRecord.itemId;
            tvSerial.setText(itemRecord.serialNum);
            cbBookmark.setChecked(itemDB.getBookmarks().contains(itemRecord.itemId));
            cbDiscount.setChecked(itemRecord.name.startsWith(BaseGlobal.DiscountHeader));

            // 재고입력 모드
            optionStockAdapter.filterItemRecord(itemRecord).generate();
            optionStockAdapter.setOnStockUpdated(new MyOnClick<ItemOptionRecord>() {
                @Override
                public void onMyClick(View view, ItemOptionRecord record) {
                    refreshSaveStockButtons();
                }
            });

            // 영수증 작성 모드
            resetSlipAdapter();
            lvOptionSlip.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final ItemOptionRecord optionRecord = optionSlipAdapter.getRecord(position);
                    new OptionQuantityDialog(context, optionRecord, SilverPreference.stockCheckingEnabled.getValue(context)) {
                        @Override
                        public void onApplyClick(int newRevStock) {
                            optionRecord.revStock = newRevStock;
                            optionSlipAdapter.notifyDataSetChanged();
                            refresh();
                        }
                    };
                }
            });

            lvOptionSlip.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    ItemOptionRecord optionRecord = optionSlipAdapter.getRecord(position);
                    if (optionRecord.revStock == 0) {
                        optionRecord.revStock = 1;
                    } else {
                        optionRecord.revStock = 0;
                    }
                    optionSlipAdapter.notifyDataSetChanged();
                    refresh();
                    return true;
                }
            });

            showImage(itemRecord);

            if (userSettingData.hasMasterAndNotMaster()) {
                tvCost.setVisibility(View.GONE);
            } else {
                tvCost.setVisibility(View.VISIBLE);
            }
        }

        protected void showImage(final ItemRecord itemRecord) {
            imageLoader.retrieve(itemRecord, new MyOnTask<Bitmap>() {
                @Override
                public void onTaskDone(Bitmap result) {
                    bitmap = result;
                    imageView.setImageBitmap(bitmap);
                    if (Empty.isEmptyImage(itemRecord.mainImagePath))
                        imageView.getLayoutParams().height = imageView.getLayoutParams().height / 3;
                    else
                        imageView.getLayoutParams().height = MyGraphics.getHeightPx(context, 0, bitmap);
                    setYPos(0);
                }
            });
        }

        private void resetSlipAdapter() {
            List<ItemOptionRecord> options = itemDB.options.getSortedRecords(itemId);
            for (ItemOptionRecord optionRecord : options)
                optionRecord.revStock = 0;
            optionSlipAdapter.setRecords(options);
        }

        private void refreshSaveStockButtons() {
            if (itemDB.options.hasOnRevision(itemId)) {
                tvSaveOption.setVisibility(View.VISIBLE);
                tvClearOption.setVisibility(View.VISIBLE);
            } else {
                tvSaveOption.setVisibility(View.GONE);
                tvClearOption.setVisibility(View.GONE);
            }
        }

        public void refresh() {
            ItemRecord record = itemDB.getRecordBy(itemId);
            int cost = itemDB.locals.getRecordBy(itemId).cost;
            stateTypes.setChecked(record.state);
            tvItemName.setText(record.name.isEmpty() ? "" : String.format("이름 : %s", record.name));
            tvCategory.setText(record.category == ItemCategoryType.NONE ? "" : String.format("분류 : %s", record.category.toUIStr()));
            tvNote.setText(record.privateNote);
            tvPrice.setText(record.unitPrice == 0 ? "" : String.format("가격 : %s", record.getPriceStringWithRetail(context)));

            if (cost == 0) {
                tvCost.setText("");
            } else {
                if (record.unitPrice == 0) {
                    tvCost.setText(String.format("원가 : %s", BaseUtils.toCurrencyStr(cost)));
                } else {
                    tvCost.setText(String.format("원가 : %s (마진 : %d%%)", BaseUtils.toCurrencyStr(cost), BaseUtils.getMarginPercent(record.unitPrice, cost)));
                }
            }

            switch (optionMode) {
                // 재고 모드
                case Stock:
                    tvNotice.setText("컬러이름 클릭 : 재고변동보기, 삭제");
                    if (doEnableSlip == false)
                        tvNotice.setText("장바구니 기능은 비활성화 됐습니다");
                    tvMode.setText(String.format("재고 (%d)", ItemDB.getInstance(context).options.getPositiveStockSum(record.itemId)));
                    setVisibility(view, View.VISIBLE, R.id.itemfullview_stock_container);
                    setVisibility(view, View.GONE, R.id.itemfullview_options_slip, R.id.itemfullview_addCart);
                    refreshSaveStockButtons();
                    break;

                // 장바구니 모드
                case Slip:
                    tvNotice.setText("클릭해서 구매 수량을 입력할 수 있습니다. 길게 클릭하시면 1개로 지정됩니다.");
                    List<ItemOptionRecord> selectedOptions = getSelectedOptions();
                    int selectedAmount = 0;
                    if (selectedOptions.size() >= 1) {
                        for (ItemOptionRecord optionRecord : selectedOptions)
                            selectedAmount = selectedAmount + optionRecord.revStock;
                    }
                    tvMode.setText(String.format("수량 (%d)", selectedAmount));
                    setVisibility(view, View.GONE, R.id.itemfullview_stock_container);
                    setVisibility(view, View.VISIBLE, R.id.itemfullview_options_slip, R.id.itemfullview_addCart);
                    break;
            }

        }

        private List<ItemOptionRecord> getSelectedOptions() {
            List<ItemOptionRecord> results = new ArrayList<>();
            for (ItemOptionRecord record : optionSlipAdapter.getRecords())
                if (record.revStock >= 1)
                    results.add(record);
            return results;
        }

        public void refreshAdapter() {
            switch (optionMode) {
                case Stock:
                    if (optionStockAdapter == null) {
                        Log.e("DEBUG_JS", String.format("[SilverFullViewHolder.refreshAdapter] optionAdapter == null"));
                        return;
                    }
                    optionStockAdapter.generate(); // ExpandableHeightListView 특징 반영
                    optionStockAdapter.notifyDataSetChanged();
                    break;

                case Slip:
                    if (optionSlipAdapter == null) return;
                    optionSlipAdapter.notifyDataSetChanged();
                    break;
            }
        }

        @Override
        public void onClick(View view) {
            super.onClick(view);
            final ItemRecord record = itemDB.getRecordBy(itemId);

            if (view.getId() == R.id.itemfullview_optionMode) {
                optionMode = cbOptionMode.isChecked() ? OptionMode.Slip : OptionMode.Stock;
                if (optionMode == OptionMode.Slip) {
                    List<String> requirements = new ArrayList<>();
                    if (record.name.isEmpty())
                        requirements.add("이름");
                    if (itemDB.options.getRecordsBy(record.itemId).size() == 0)
                        requirements.add("컬러-사이즈");
                    if (requirements.size() >= 1) {
                        MyUI.toastSHORT(context, String.format("%s 입력 필요", TextUtils.join(", ", requirements)));
                        cbOptionMode.setChecked(false);
                        optionMode = OptionMode.Stock;
                        return;
                    }
                    resetSlipAdapter();
                }
                refresh();
                refreshAdapter();

            } else if (view.getId() == R.id.itemfullview_bookmark) {
                if (cbBookmark.isChecked() && itemDB.getBookmarks().size() >= userSettingData.getMaxBookmarkCount()) {
                    MyUI.toastSHORT(context, String.format("최대 %d개까지 가능합니다", userSettingData.getMaxBookmarkCount()));
                    cbBookmark.setChecked(false);
                    return;
                }
                updateBookmark(record, cbBookmark.isChecked(), null);

            } else if (view.getId() == R.id.itemfullview_discount) {
                if (cbDiscount.isChecked()) {
                    new UpdatePriceDialog(context, "할인가 입력", String.format("현재 가격 : %s", record.getPriceStringWithRetail(context)), record.unitPrice, record.retailPrice, userSettingData.getBasicUnit()) {
                        @Override
                        public void onApplyClick(final int price, final int retailPrice) {
                            new MyServiceTask(context, "unitPrice") {
                                @Override
                                public void doInBackground() throws MyServiceFailureException {
                                    ItemRecord updatedRecord = itemDB.getRecordBy(itemId);
                                    updatedRecord.unitPrice = price;
                                    updatedRecord.retailPrice = retailPrice;
                                    if (updatedRecord.name.startsWith(BaseGlobal.DiscountHeader) == false) // 이름에 "세일_" 추가
                                        updatedRecord.name = BaseGlobal.DiscountHeader + updatedRecord.name;
                                    updateItem(record, updatedRecord, new MyOnTask() {
                                        @Override
                                        public void onTaskDone(Object result) {
                                            MyUI.toastSHORT(context, String.format("세일표시 추가"));
                                            refresh();
                                        }
                                    });
                                }
                            };
                        }

                        @Override
                        protected void onCancelClick() {
                            super.onCancelClick();
                            cbDiscount.setChecked(false);
                        }
                    };
                } else {
                    ItemRecord updatedRecord = itemDB.getRecordBy(itemId);
                    if (updatedRecord.name.startsWith(BaseGlobal.DiscountHeader)) // 이름에 "세일_" 추가
                        updatedRecord.name = updatedRecord.name.replace(BaseGlobal.DiscountHeader,"");
                    updateItem(record, updatedRecord, new MyOnTask() {
                        @Override
                        public void onTaskDone(Object result) {
                            MyUI.toastSHORT(context, String.format("세일표시 삭제"));
                            refresh();
                        }
                    });
                }

            } else if (view.getId() == R.id.itemfullview_shareMain) {
                ImageRecord imageRecord = itemDB.images.getRecordBy(itemId);
                String filePath = MyDevice.createTempImageFilePath();
                if (MyGraphics.toFile(imageRecord.getByteArray(), filePath) == false)
                    return;
                MyDevice.shareImageFile(context, filePath);

            } else if (view.getId() == R.id.itemfullview_image) {
                ImageZoomView.start(context, bitmap);

            } else if (view.getId() == R.id.itemfullview_close) {
                onCancelClick.onMyClick(view, "");

            } else if (view.getId() == R.id.itemfullview_clearOption) {
                clearItemOptions();

            } else if (view.getId() == R.id.itemfullview_saveOption) {
                saveItemOptions();

            } else if (view.getId() == R.id.itemfullview_addCart) {
                addCart();

                // TextViews
            } else if (view.getId() == R.id.itemfullview_name) {
                new ItemNameDialog(context, itemId, new MyOnTask<Pair<ItemRecord, ItemRecord>>() {
                    @Override
                    public void onTaskDone(Pair<ItemRecord, ItemRecord> result) {
                        updateItem(result.first, result.second, null);
                    }
                });

            } else if (view.getId() == R.id.itemfullview_note) {
                new ItemNoteDialog(context).show();

            } else if (view.getId() == R.id.itemfullview_price) {
                new UpdatePriceDialog(context, "판매가 입력", "", record.unitPrice, record.retailPrice, userSettingData.getBasicUnit()) {
                    @Override
                    public void onApplyClick(final int price, final int retailPrice) {
                        new MyServiceTask(context, "unitPrice") {
                            @Override
                            public void doInBackground() throws MyServiceFailureException {
                                ItemRecord updatedRecord = itemDB.getRecordBy(itemId);
                                updatedRecord.unitPrice = price;
                                updatedRecord.retailPrice = retailPrice;
                                updateItem(record, updatedRecord, new MyOnTask() {
                                    @Override
                                    public void onTaskDone(Object result) {
                                        refresh();
                                    }
                                });
                            }
                        };
                    }
                };

            } else if (view.getId() == R.id.itemfullview_cost) { // master only
                final LocalItemRecord localItemRecord = itemDB.locals.getRecordWithItemId(itemId); // 없을수도 있다
                new ItemCostDialog(context, localItemRecord.cost, userSettingData.getBasicUnit()) {
                    @Override
                    public void onApplyClick(final int value, final boolean doUpdateUnitPrice, final int marginPercent) {
                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("반영중...");
                        progressDialog.show();

                        new MyServiceTask(context, "uploadCost") {
                            Integer unitPriceNullable = 0;

                            @Override
                            public void doInBackground() throws MyServiceFailureException {
                                if (doUpdateUnitPrice) {
                                    unitPriceNullable = itemService.updateCostPrice(userSettingData.getUserShopInfo(), itemId, value, marginPercent);
                                } else {
                                    itemService.updateCostPrice(userSettingData.getUserShopInfo(), itemId, value, null);
                                }
                                localItemRecord.cost = value;
                                itemDB.locals.updateOrInsert(localItemRecord); // 원가
                            }

                            @Override
                            public void onPostExecutionUI() {
                                if (doUpdateUnitPrice && (unitPriceNullable != null)) {
                                    ItemRecord updatedRecord = itemDB.getRecordBy(itemId);
                                    updatedRecord.unitPrice = unitPriceNullable; // 선택시 판매가
                                    updateItem(record, updatedRecord, new MyOnTask() {
                                        @Override
                                        public void onTaskDone(Object result) {
                                            refresh();
                                            progressDialog.dismiss();
                                        }
                                    });
                                } else {
                                    refresh();
                                    progressDialog.dismiss();
                                }
                            }

                            @Override
                            public void catchException(MyServiceFailureException e) {
                                super.catchException(e);
                                progressDialog.dismiss();
                            }
                        };
                    }

                    @Override
                    public void onChangeBasicUnit() {
                        startPreference();
                    }
                };

            } else if (view.getId() == R.id.itemfullview_category) {
                new SelectCategoryDialog(context).show();

            } else if (view.getId() == R.id.itemfullview_add_option) {
                new AddItemOptionDialog(context, itemId).show();

                // State buttons
            } else if (view.getId() == R.id.itemfullview_state_preparing ||
                    view.getId() == R.id.itemfullview_state_displayed ||
                    view.getId() == R.id.itemfullview_state_stored ||
                    view.getId() == R.id.itemfullview_state_soldout) {
                final ItemStateType itemStateType = (ItemStateType) view.getTag();
                if (record.state != itemStateType)
                    changeItemStateType(record, itemStateType);
            }
        }

        private void saveItemOptions() {
            if (itemDB.options.hasOnRevision(itemId) == false) {
                MyUI.toastSHORT(context, String.format("수정된 내용이 없습니다"));
                return;
            }

            new MyServiceTask(context, "saveItemOptions", "컬러-사이즈 저장중") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    itemService.updateItemStock(userSettingData.getUserShopInfo(), itemDB.options.getOnRevisionBundle(itemId));
                    itemDB.options.confirmRevision(itemId);
                }

                @Override
                public void onPostExecutionUI() {
                    refresh();
                    refreshAdapter();
                }

            };
        }

        private void clearItemOptions() {
            if (itemDB.options.hasOnRevision(itemId) == false) {
                MyUI.toastSHORT(context, String.format("수정된 내용이 없습니다"));
                return;
            }
            itemDB.options.clearOnRevisions(itemId);
            refresh();
            refreshAdapter();
        }

        private void addCart() {
            final List<ItemOptionRecord> itemOptionRecords = getSelectedOptions();
            if (itemOptionRecords.size() <= 0) {
                MyUI.toastSHORT(context, String.format("선택된 옵션이 없습니다. 원하는 [컬러-크기] 를 선택해주세요"));
                return;
            }

            final ItemRecord itemRecord = itemDB.getRecordBy(itemId);
            final String header = String.format("%s%s", itemOptionRecords.get(0).toString("-"), itemOptionRecords.size() == 1 ? "" : String.format(" 외 %d건", itemOptionRecords.size() - 1));
            ItemCartView.startActivity(context, header, itemRecord, new MyOnClickTriple<UserRecord, SlipItemType, Integer>() {
                @Override
                public void onMyClick(View view, UserRecord counterpart, SlipItemType slipItemType, Integer unitPrice) {
                    PosSlipDB posSlipDB = PosSlipDB.getInstance(context);
                    SlipRecord slipRecord;
                    if (posSlipDB.getPhoneNumbers(false).contains(counterpart.phoneNumber)) {
                        slipRecord = posSlipDB.getUnconfirmedRecord(counterpart.phoneNumber); // 기존 거래에 추가
                    } else {
                        slipRecord = new SlipRecord(userSettingData.getProviderRecord().phoneNumber, counterpart.phoneNumber); // 신규 거래 생성
                        slipRecord.confirmed = false;
                        posSlipDB.insert(slipRecord);
                    }

                    int serial = posSlipDB.items.getNextSerial(slipRecord.getSlipKey().toSID());
                    for (ItemOptionRecord itemOptionRecord : itemOptionRecords) {
                        SlipItemRecord slipItemRecord = new SlipItemRecord(itemRecord, itemOptionRecord, slipRecord);
                        slipItemRecord.slipItemType = slipItemType;
                        slipItemRecord.unitPrice = unitPrice;
                        slipItemRecord.quantity = itemOptionRecord.revStock; // 지정된 수량 입력
                        slipItemRecord.serial = serial;
                        posSlipDB.items.insert(slipItemRecord);
                        serial++;
                    }
                    MyUI.toastSHORT(context, String.format("[%s] %s, %s 추가", counterpart.name, BaseUtils.toCurrencyStr(unitPrice), slipItemType.uiName));
                    ((Activity) context).finish();
                }
            });
        }

        private void changeItemStateType(final ItemRecord record, final ItemStateType itemStateType) {
            new MyServiceTask(context, "SilverItemFullViewAdapter.changeItemStateType") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    service.updateItemState(userSettingData.getUserShopInfo(), record.itemId, itemStateType);
                    record.state = itemStateType;
                    itemDB.updateOrInsert(record);
                }

                @Override
                public void onPostExecutionUI() {
                    refresh();
                    onItemStateTypeChanged(record.state);
                }
            };
        }

        protected void onItemStateTypeChanged(ItemStateType state) {
            MyUI.toastSHORT(context, String.format("상품 상태를 변경했습니다"));
        }

        class ItemNoteDialog extends BaseDialog {
            private EditText etItemNote;
            private ItemRecord itemRecord;

            public ItemNoteDialog(Context context) {
                super(context);
                setDialogWidth(0.95, 0.7);
                itemRecord = itemDB.getRecordBy(itemId);
                etItemNote = (EditText) findTextViewById(R.id.dialog_item_message_edittext, itemRecord.privateNote);
                findTextViewById(R.id.dialog_item_message_title, "내부 직원용 노트");
                MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_item_message_title, R.id.dialog_item_message_edittext);
                setOnClick(R.id.dialog_item_message_cancel, R.id.dialog_item_message_apply, R.id.dialog_item_message_clear);
                MyDevice.showKeyboard(context, etItemNote);
            }

            @Override
            public int getThisView() {
                return R.layout.dialog_item_message;
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.dialog_item_message_clear) {
                    etItemNote.setText("");
                } else if (view.getId() == R.id.dialog_item_message_apply) {
                    ItemRecord updatedRecord = itemDB.getRecordBy(itemId);
                    updatedRecord.privateNote = etItemNote.getText().toString().replace(";", "-").replace(":", "-");
                    updateItem(itemRecord, updatedRecord, new MyOnTask() {
                        @Override
                        public void onTaskDone(Object result) {
                            MyDevice.hideKeyboard(context, etItemNote);
                            dismiss();
                        }
                    });
                } else if (view.getId() == R.id.dialog_item_message_cancel) {
                    MyDevice.hideKeyboard(context, etItemNote);
                    dismiss();
                }
            }
        }

        private void updateItem(final ItemRecord record, final ItemRecord updatedRecord, final MyOnTask onTask) {
            new MyServiceTask(context, "updateItem") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    itemService.updateItem(userSettingData.getUserShopInfo(), record, updatedRecord); // 서버
                    itemDB.update(record.id, updatedRecord); // DB
                    records.set(position, updatedRecord); // View
                }

                @Override
                public void onPostExecutionUI() {
                    refresh();
                    if (onTask != null)
                        onTask.onTaskDone("");
                }
            };
        }

        private void updateBookmark(final ItemRecord record, final boolean isChecked, final MyOnTask onTask) {
            new MyServiceTask(context, "updateBookmark") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    List<Integer> bookmarks = new ArrayList<>();
                    if (isChecked) {
                        bookmarks = itemService.postBookmark(userSettingData.getUserShopInfo(), record.itemId);
                        MyUI.toastSHORT(context, String.format("맨 위로 올라갑니다"));
                    } else {
                        bookmarks = itemService.deleteBookmark(userSettingData.getUserShopInfo(), record.itemId);
                        MyUI.toastSHORT(context, String.format("원래 자리로 돌아옵니다"));
                    }
                    itemDB.setBookmarks(bookmarks);
                }

                @Override
                public void onPostExecutionUI() {
                    refresh();
                    if (onTask != null)
                        onTask.onTaskDone("");
                }
            };
        }

        class SelectCategoryDialog extends CategoryDialog {
            public SelectCategoryDialog(Context context) {
                super(context, "분류 선택", ItemDB.getInstance(context).getRecords(), false, false, false);
            }

            @Override
            public void onItemClicked(ItemCategoryType categoryType) {
                ItemRecord itemRecord = itemDB.getRecordBy(itemId);
                ItemRecord updatedRecord = itemDB.getRecordBy(itemId);
                updatedRecord.category = categoryType;
                updateItem(itemRecord, updatedRecord, new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        dismiss();
                    }
                });
            }
        }

        class AddItemOptionDialog extends BaseItemOptionDialog {

            public AddItemOptionDialog(Context context, int itemId) {
                super(context, itemId);
            }

            @Override
            public void onApplyClick() {
                super.onApplyClick();
                saveItemOptions(); // 자동 put
            }

            // 색상명을 변경하기 위해서 연 경우는 반영이 돼야 한다
            @Override
            public void onCancelClick() {
                super.onCancelClick();
                refresh();
                refreshAdapter();
            }
        }

        abstract public class ItemCostDialog extends BaseDialog {
            private MyCurrencyEditText etValue;
            private MyKeyValueEditText etPercent;
            private TextView tvUnitPrice, tvMargin;
            private MyCheckBox cbUnitPrice;
            private MyTextWatcher myTextWatcher;

            public ItemCostDialog(Context context, int value, int basicUnit) {
                super(context);
                setDialogWidth(0.9, 0.8);
                myTextWatcher = new MyTextWatcher();

                etValue = new MyCurrencyEditText(context, getView(), R.id.dialog_cost_edittext, R.id.dialog_cost_edittext_clear);
                etValue.setTextViewAndBasicUnit(R.id.dialog_cost_basicUnit, basicUnit);
                etValue.setOriginalValue(value);
                etValue.getEditText().addTextChangedListener(myTextWatcher);

                etPercent = new MyKeyValueEditText(context, getView(), R.id.dialog_cost_revenuePercent,
                        "itemfullview.cost.percent", Integer.toString(userSettingData.getDefaultCostMarginPercent()));
                etPercent.setOnClearClick(R.id.dialog_cost_clear_percent);
                etPercent.setAfterTextChanged(false, new MyOnTask<String>() {
                    @Override
                    public void onTaskDone(String result) {
                        refreshViews();
                    }
                });

                tvUnitPrice = findTextViewById(R.id.dialog_cost_unitPrice);
                tvMargin = findTextViewById(R.id.dialog_cost_margin);

                setOnClick(R.id.dialog_cost_cancel, R.id.dialog_cost_apply, R.id.dialog_cost_change_basicUnit);
                MyDevice.showKeyboard(context, etValue.getEditText());
                show();

                MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_cost_change_title,
                       R.id.dialog_cost_change_guide, R.id.dialog_cost_update_unitPrice,
                        R.id.dialog_cost_revenueTitle, R.id.dialog_cost_revenueTail, R.id.dialog_cost_margin_guide);

                cbUnitPrice = new MyCheckBox(keyValueDB, getView(), R.id.dialog_cost_update_unitPrice, "itemfullview.update.unitprice", false);
                cbUnitPrice.setOnClick(true, new MyOnClick<Boolean>() {
                    @Override
                    public void onMyClick(View view, Boolean checked) {
                        setVisibility(checked ? View.VISIBLE : View.GONE, R.id.dialog_cost_unitPrice_container);
                        if (checked)
                            refreshViews();
                    }
                });

            }

            private void refreshViews() {
                int cost = etValue.getValue();
                int percent = BaseUtils.toInt(etPercent.getValue());
                int unitPrice = BaseUtils.ceil(cost * (1.0 + percent / 100.0), 1000); // 단가는 올림으로 처리
                tvUnitPrice.setText(String.format("판매가 : %s", BaseUtils.toCurrencyStr(unitPrice)));
                tvMargin.setText(String.format("(마진율 : %d%%)", BaseUtils.getMarginPercent(unitPrice, cost)));
            }


            @Override
            public int getThisView() {
                return R.layout.dialog_cost;
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.dialog_cost_cancel) {
                    MyDevice.hideKeyboard(context, etValue.getEditText());
                    dismiss();

                } else if (view.getId() == R.id.dialog_cost_apply) {
                    cbUnitPrice.saveValue();
                    etPercent.saveValue();
                    onApplyClick(etValue.getValue(), cbUnitPrice.isChecked(), BaseUtils.toInt(etPercent.getValue()));

                    if (userSettingData.getDefaultCostMarginPercent() != BaseUtils.toInt(etPercent.getValue()))
                        updateDefaultCostMargin(BaseUtils.toInt(etPercent.getValue()));

                    MyDevice.hideKeyboard(context, etValue.getEditText());
                    dismiss();

                } else if (view.getId() == R.id.dialog_cost_change_basicUnit) {
                    onChangeBasicUnit();
                    dismiss();

                } else {

                }
            }

            private void updateDefaultCostMargin(final int percent) {
                new MyServiceTask(context, "updateDefaultCostMargin") {

                    @Override
                    public void doInBackground() throws MyServiceFailureException {
                        UserSettingDataProtocol protocol = mainShopService.updateDefaultMarginAdditionRate(userSettingData.getUserShopInfo(), percent);
                        userSettingData.set(protocol);
                    }

                    @Override
                    public void onPostExecutionUI() {

                    }
                };
            }

            abstract public void onApplyClick(int value, boolean doUpdateUnitPrice, int marginPercent);

            abstract public void onChangeBasicUnit();

            class MyTextWatcher extends BaseTextWatcher {

                @Override
                public void afterTextChanged(Editable s) {
                    refreshViews();
                }
            }
        }

    }


}
