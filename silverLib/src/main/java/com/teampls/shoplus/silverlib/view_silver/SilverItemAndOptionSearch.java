package com.teampls.shoplus.silverlib.view_silver;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyImageLoader;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.SelectedItemRecord;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_map.KeyValueInt;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseItemAndOptionSearch;
import com.teampls.shoplus.silverlib.dialog.HistoryItemDialog;
import com.teampls.shoplus.silverlib.common.SilverPreference;
import com.teampls.shoplus.silverlib.database.HistoryItemDB;
import com.teampls.shoplus.silverlib.database.HistoryItemRecord;
import com.teampls.shoplus.silverlib.database.ItemDBAdapter;
import com.teampls.shoplus.silverlib.database.ItemOptionDBAdapter;
import com.teampls.shoplus.silverlib.dialog.OptionQuantityDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-04-15.
 */

abstract public class SilverItemAndOptionSearch extends BaseItemAndOptionSearch {
    protected SilverSlipItemSelection slipItemSelection;
    public KeyValueInt lastSelectedItemId = new KeyValueInt("silver.slip.lastSelected", 0);
    protected HistoryItemDB historyItemDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        slipItemSelection = new SilverSlipItemSelection(context, itemDB);
        lastSelectedItem = itemDB.getRecordBy(lastSelectedItemId.get(context));
        if (lastSelectedItem.itemId > 0) {
            tvLastSelectedItem.setVisibility(View.VISIBLE);
            tvLastSelectedItem.setText(lastSelectedItem.getUiName());
        }
        historyItemDB = HistoryItemDB.getInstance(context);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    protected void setItemDB() {
        itemDB = ItemDB.getInstance(context);
    }

    @Override
    protected void setItemAdapter() {
        itemAdapter = new ItemDBAdapter(context);
        itemAdapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_SINGLE);
        itemAdapter.setViewType(ItemDBAdapter.ItemAndOptionSearch);

        switch (itemSearchType) {
            case ByName:
                itemAdapter.setOrderRecord(ItemDB.Column.name, false);
                break;
            case BySerialNum:
                itemAdapter.setOrderRecord(ItemDB.Column.serialNum, true);
                break;
        }

        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                itemAdapter.generate();
            }
        };
    }

    @Override
    protected void setOptionAdapter() {
        optionAdapter = new ItemOptionDBAdapter(context);
        optionAdapter.setViewType(ItemOptionDBAdapter.ItemAndOptionSearch);
    }

    protected class NoOptionDialog extends MyButtonsDialog {

        public NoOptionDialog(final Context context, final ItemRecord itemRecord, final MyOnTask onTask) {
            super(context, "컬러-사이즈 추가", "입력된 컬러-사이즈가 없습니다");
            addButton("기본 컬러-사이즈 추가", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    slipItemSelection.addDefaultOption(itemRecord, new MyOnTask<ItemOptionRecord>() {
                        @Override
                        public void onTaskDone(ItemOptionRecord result) {
                            selectedOption = result;
                            optionAdapter.clear();
                            optionAdapter.addRecord(selectedOption);
                            optionAdapter.notifyDataSetChanged();
                            refreshSelected();
                            dismiss();
                            if (onTask != null)
                                onTask.onTaskDone("");
                        }
                    });
                }
            });

            addButton("입력 페이지로 이동", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startItemFullViewPager(itemRecord);
                    MyUI.toastSHORT(context, String.format("[컬러-사이즈 추가]를 눌러주세요"));
                    dismiss();
                }
            });

            show();
        }
    }

    @Override
    protected void onMyItemClick(final ItemRecord itemRecord) {
        selectedItem = itemRecord;
        MyImageLoader.getInstance(context).retrieveThumbAsync(itemRecord, new MyOnTask<Bitmap>() {
                    @Override
                    public void onTaskDone(Bitmap result) {
                        imageView.setImageBitmap(result);
                    }
                }
        );

        List<ItemOptionRecord> options = itemDB.options.getSortedRecords(selectedItem.itemId);  // updateOrInsert
        optionAdapter.setRecords(options);
        optionAdapter.notifyDataSetChanged();

        List<ItemOptionRecord> selectedOptions = getSelectedOptions();
        for (ItemOptionRecord option : options) {
            option.revStock = 0;  // 최초 수량을 0으로 초기화
            for (ItemOptionRecord selectedOption : selectedOptions) {
                if (option.color == selectedOption.color && option.size == selectedOption.size) {
                    option.revStock = selectedOption.revStock; // 이전에 저장한게 있으면 그 값을 복원시켜줌
                    break;
                }
            }
        }
        selectedOption = new ItemOptionRecord();
        refreshSelected();
        setBottomButtons(); // selectedItem 유무에 따라

        if (options.size() == 0)
            new NoOptionDialog(context, itemRecord, null);
    }

    protected List<ItemOptionRecord> getSelectedOptions() {
        List<ItemOptionRecord> results = new ArrayList<>();
        for (ItemOptionRecord record : optionAdapter.getRecords())
            if (record.revStock >= 1)
                results.add(record);
        return results;
    }

    // PosSlipCreationView.RetrievedItemView에도 동일 기능 있음
    @Override
    protected void onMyOptionClick(final ItemOptionRecord optionRecord) {
        selectedOption = optionRecord;

        new OptionQuantityDialog(context, optionRecord, SilverPreference.stockCheckingEnabled.getValue(context)) {
            @Override
            public void onApplyClick(int newRevStock) {
                optionRecord.revStock = newRevStock;
                optionAdapter.notifyDataSetChanged();
                refreshSelected();
                etShadowNumber.setText("0"); // 숫자입력후는 키보드를 숫자로 유지
                etShadowNumber.requestFocus();
            }
        };
    }

    protected void onMyOptionLongClick(ItemOptionRecord optionRecord) {
        selectedOption = optionRecord;
        if (optionRecord.revStock == 0) {
            optionRecord.revStock = 1;
        } else {
            optionRecord.revStock = 0;
        }
        optionAdapter.notifyDataSetChanged();
        refreshSelected();
    }

    private void saveSelectedItem(ItemRecord selectedItem) {
        lastSelectedItemId.put(context, selectedItem.itemId);
        if (historyItemDB.has(selectedItem.itemId))
            historyItemDB.deleteBy(selectedItem.itemId);
        historyItemDB.insert(new HistoryItemRecord(0, selectedItem.itemId, selectedItem.getUiName()));
    }

    protected void onAddFastClick(final ItemRecord selectedItem) {
        final List<ItemOptionRecord> options = getSelectedOptions();
        if (selectedItem.itemId <= 0) {
            MyUI.toastSHORT(context, String.format("상품이 선택되지 않았습니다. 확인해주세요"));
            return;
        }

        saveSelectedItem(selectedItem);

        slipItemSelection.onAddFastClick(selectedItem, optionAdapter, options, new MyOnTask() {
            @Override
            public void onTaskDone(Object result) {
                if (onApplyClicked != null) {
                    selectedItem.getUnitPriceByBuyer(context, buyerPhoneNumber, new MyOnTask<Integer>() {
                        @Override
                        public void onTaskDone(Integer result) {
                            onApplyClicked.onMyClick(null, new SelectedItemRecord(selectedItem, result, options, false)); // 정보 업데이트 창을 띄우지 않는다
                            doFinish();
                        }
                    });

                } else {
                    doFinish();
                }
            }
        });
    }

    @Override
    protected void onApplyClick(final ItemRecord selectedItem) {
        final List<ItemOptionRecord> options = getSelectedOptions();

        if (btApply.getText().toString().contains("수동")) {
            new MyAlertDialog(context, "수동추가로 이동", "상품이 선택되지 않았습니다. 선택하지 않고 수동 추가로 이동하시겠습니까?") {
                @Override
                public void yes() {
                    doFinish();
                    selectedItem.itemId = 0;
                    selectedItem.name = etName.getText().toString();
                    if (onApplyClicked != null)
                        onApplyClicked.onMyClick(null, new SelectedItemRecord(selectedItem, 0, options, true));
                    // 정보 업데이트 창을 자동으로 띄운다 (가격, 상태)
                }
            };
            return;
        }

        if (selectedItem.itemId <= 0) {
            MyUI.toastSHORT(context, String.format("상품이 선택되지 않았습니다. 확인해주세요"));
            return;
        }

        if (options.size() > 0) {

            // "다음" 클릭시
            saveSelectedItem(selectedItem);

            selectedItem.getUnitPriceByBuyer(context, buyerPhoneNumber, new MyOnTask<Integer>() {
                @Override
                public void onTaskDone(Integer result) {
                    doFinish();
                    if (onApplyClicked != null)
                        onApplyClicked.onMyClick(null, new SelectedItemRecord(selectedItem, result, options, true));
                }
            });

        } else {
            if (optionAdapter.getCount() == 0) {
                new NoOptionDialog(context, selectedItem, null);

            } else if (optionAdapter.getCount() == 1) { // 옵션이 1개면서 클릭하지 않았을때는 자동으로 선택해준다
                onMyOptionClick(optionAdapter.getRecord(0));

            } else {
                MyUI.toastSHORT(context, String.format("컬러-사이즈가 선택되지 않았습니다"));
            }
        }
    }

    public void setBottomButtons() {
        if (selectedItem.itemId > 0) {
            btOpenFullView.setVisibility(View.VISIBLE);
            btAddFast.setVisibility(View.VISIBLE);
            btApply.setVisibility(View.VISIBLE);
            btShowHistory.setVisibility(View.GONE);
            btApply.setText("다  음");
            MyView.removeColorFilter(btApply);
            floatingButtons.hide();
        } else {
            btOpenFullView.setVisibility(View.GONE);
            btAddFast.setVisibility(View.GONE);
            btShowHistory.setVisibility(View.VISIBLE);
            if (etName.getText().toString().trim().isEmpty()) { // 완전히 빈 상태
                btApply.setVisibility(View.GONE);
                floatingButtons.hide();
            } else { // 뭔가 타이핑한 상태
                btApply.setVisibility(View.VISIBLE);
                btApply.setText("수동추가로");
                MyView.setColorFilter(btApply, ColorType.SkyBlue);
                floatingButtons.show();
            }
        }
    }

    @Override
    protected void onModifyClick(final ItemRecord selectedItem) {
        if (selectedItem.itemId <= 0) {
            MyUI.toastSHORT(context, String.format(" 선택된 상품이 없습니다"));
            return;
        }
        startItemFullViewPager(selectedItem);
    }

    protected void refreshSelected() {
        List<String> summaries = new ArrayList<>();
        summaries.add(BaseUtils.toLimitString(selectedItem.getUiName(), 10));
        List<ItemOptionRecord> selectedOptions = getSelectedOptions();
        if (selectedOptions.size() >= 1) {
            int selectedAmount = 0;
            for (ItemOptionRecord optionRecord : selectedOptions)
                selectedAmount = selectedAmount + optionRecord.revStock;
            summaries.add(String.format("%d개", selectedAmount));
        }
        tvSelectedItem.setText(TextUtils.join("/", summaries));
    }

    abstract public void startItemFullViewPager(ItemRecord itemRecord);

    @Override
    protected void onAddAllOptionsClick(final ItemRecord selectedItem) {
        if (selectedItem.itemId == 0) {
            MyUI.toastSHORT(context, String.format("먼저 상품을 선택해주세요"));
            return;
        }
        slipItemSelection.selectAllOptions(optionAdapter, null);
        refreshSelected();
    }

    @Override
    protected void onShowHistoryClick() {
        new HistoryItemDialog<HistoryItemRecord>(context, historyItemDB.getRecords()) {

            @Override
            protected String getUiString(HistoryItemRecord record) {
                return record.name;
            }

            @Override
            public void onItemClicked(@Nullable HistoryItemRecord selectedRecord) {
                if (selectedRecord == null)
                    return;
                selectItem(itemDB.getRecordBy(selectedRecord.itemId));
            }
        };
    }


}
