package com.teampls.shoplus.silverlib.view_silver;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseAwsService;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MSlipDB;
import com.teampls.shoplus.lib.database_memory.MTransDB;
import com.teampls.shoplus.lib.dialog.MyMultilinesDialog;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.view_base.BasePosSlipSharingView;
import com.teampls.shoplus.silverlib.R;

import java.util.ArrayList;

/**
 * Created by Medivh on 2018-04-15.
 */

abstract public class SilverSlipSharingView extends BasePosSlipSharingView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        setVisibility(View.VISIBLE, R.id.slip_sharing_createNewSlip);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    public void onCreateNewSlipClick() {
        if (posSlipDB.hasUnconfirmedRecord(counterpart.phoneNumber)) {
            MyUI.toastSHORT(context, String.format("작성 중인 기록으로 이동합니다"));
            SlipRecord slipRecord = posSlipDB.getUnconfirmedRecord(counterpart.phoneNumber);
            startPosSlipCreationView(slipRecord);
        } else {
            MyUI.toastSHORT(context, String.format("%s 과 새 거래를 생성합니다", counterpart.name));
            startPosSlipCreationView(counterpart);
        }
        doFinishForResult();
    }

    abstract public void startPosSlipCreationView(SlipRecord slipRecord);

    abstract public void startPosSlipCreationView(UserRecord counterpart);

    @Override
    protected void cancelSlip(final boolean doCopy) {
        boolean doUpdateItemDB = true;
        BaseAwsService.cancelSlip(context, slipFullRecord.getKey(), doUpdateItemDB, new MyOnAsync<SlipFullRecord>() {
            // onCancelSlip
            @Override
            public void onSuccess(final SlipFullRecord copiedFullRecord) {
                SlipRecord slipRecord = posSlipDB.getRecordByKey(slipFullRecord.getKey());
                if (slipRecord.id != 0) {
                    slipRecord.cancelled = true;
                    posSlipDB.update(slipRecord.id, slipRecord);
                    // mPOS 용
                    MTransDB.getInstance(context).updateOrInsert(slipRecord);
                    MSlipDB.getInstance(context).updateOrInsert(slipRecord);
                }
                if (doCopy) {
                    posSlipDB.insert(copiedFullRecord, new ArrayList<String>(), new ArrayList<String>());
                    MyUI.toastSHORT(context, String.format("재작성을 위해 복사하고 해당 거래를 취소했습니다"));
                } else {
                    MyUI.toastSHORT(context, String.format("해당 거래를 취소했습니다"));
                }
                doFinish();
            }

            @Override
            public void onError(Exception e) {
                Log.e("DEBUG_JS", String.format("[SilverSlipSharingView.onError] %s", e));
                checkCancelled();
            }

        }, new MyOnAsync() {
            // onUpdateStock
            @Override
            public void onSuccess(Object result) {
                refreshAfterCancelled();
            }

            @Override
            public void onError(Exception e) {

            }
        });
    }

    abstract public void refreshAfterCancelled();

    @Override
    public void onMyMenuCreate() {
        myActionBar.clear();
        if (userSettingData.isProvider()) {
            myActionBar
                    .add(MyMenu.basicInfo)
                    .add(MyMenu.sendKakao)
                    .add(MyMenu.shareFile)
                    .add(MyMenu.printSetting)
                    .add(slipFullRecord.record.isMine(context), MyMenu.cancelSlip)
                    .add(MyMenu.addReceivableReceipt);
        } else if (userSettingData.isBuyer()) {
            myActionBar.add(MyMenu.sendKakao)
                    .add(MyMenu.shareFile);
        }
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        super.onMyMenuSelected(myMenu);
        switch (myMenu) {
            case addReceivableReceipt:
                switch (actionType) {
                    default:
                        new MyMultilinesDialog(context, "받은 잔금 입력", "영수증이 이미 확정되었습니다. 잔금 입력처리는 취소후 다시 작성하시거나 미송잔금앱을 이용해주세요").show();
                        break;
                    case INSERT:
                        new AddReceivableReceiptDialog(context).show();
                        break;
                }
                break;
        }
    }
}
