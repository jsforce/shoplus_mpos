package com.teampls.shoplus.silverlib.datatypes;

import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.silverlib.enums.SettlementAuxilityDataType;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 결산용 보조 데이터. 시재 및 비용 등을 포함한다.
 *
 * @author lucidite
 */
public class SettlementAuxilityData implements Comparable<SettlementAuxilityData> {
    private String id;
    private SettlementAuxilityDataType type;
    public String title;
    public int amount;

    public SettlementAuxilityData(JSONObject dataObj) throws JSONException  {
        this.id = dataObj.getString("created");
        String typeStr = dataObj.getString("aux_type");
        this.type = SettlementAuxilityDataType.fromRemoteStr(typeStr);
        this.title = dataObj.optString("title", "");
        this.amount = dataObj.optInt("amount", 0);
    }

    public String getId() {
        return id;
    }

    public DateTime getCreatedDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.id);
    }

    public SettlementAuxilityDataType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public int compareTo(@NonNull SettlementAuxilityData o) {
        return getCreatedDateTime().compareTo(o.getCreatedDateTime()); // 시간순
    }

    public SettlementAuxilityData() {
        id = "";
        type = SettlementAuxilityDataType.EXPENSE;
        title = "";
        amount = 0;
    }

    public void setDateTime(DateTime dateTime) {
        id = APIGatewayHelper.getRemoteDateTimeString(dateTime);
    }

    public SettlementAuxilityData(DateTime dateTime, String title, int amount) {
        setDateTime(dateTime);
        type = SettlementAuxilityDataType.EXPENSE;
        this.title = title;
        this.amount = amount;
    }

    public int getUnsignedAmount() {
        return Math.abs(amount); // 수입을 - 부호로 처리했는데 이 영향을 제거
    }

    public void update(SettlementAuxilityData newData) {
        this.type = newData.type;
        this.title = newData.title;
        this.amount = newData.amount;
    }

    public boolean isIncome() {
        return (type == SettlementAuxilityDataType.EXPENSE && amount < 0);
    }


    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] id %s, type %s, title %s, amount %d", location, id, type.toUiStr(), title, amount));
    }
}
