package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseSlipDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_memory.MemorySlipDB;
import com.teampls.shoplus.lib.database_memory.MemorySlipItemDB;
import com.teampls.shoplus.lib.datatypes.MyTriple;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.PickDateDialog;
import com.teampls.shoplus.lib.dialog.SetPeriodDialog;
import com.teampls.shoplus.lib.dialog.WorkingTimeDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.WorkingTime;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.view.SimplePosSlipSharingView;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.common.SilverPreference;
import com.teampls.shoplus.silverlib.database.SettlementRecord;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Medivh on 2019-06-26.
 */

public class SettlementSettingView extends BaseActivity implements AdapterView.OnItemClickListener {
    private TextView tvPeriod, tvAmount;
    public DateTime fromDate = Empty.dateTime, toDate = Empty.dateTime;
    private SettleTransDB transDB;
    private SettlementRecord settlementRecord = new SettlementRecord();
    private MyRadioGroup<WorkingTime> workingTime;
    private TextView tvToday, tvYesterday, tvUserDefined;
    private DayPeriodType dayPeriodType = DayPeriodType.NotDefined;
    private RadioGroup workingTimeContainer;
    protected ListView listView;
    public SettleTransDBAdapter adapter;
    private LinearLayout periodTextviewContainer;
    private MyEmptyGuide emptyGuide;

    private enum DayPeriodType {NotDefined, Today, Yesterday, UserDefied}

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, SettlementSettingView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myActionBar.hide();
        transDB = new SettleTransDB(context);
        adapter = new SettleTransDBAdapter(context, transDB);
        listView = findViewById(R.id.settlement_setting_listview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        tvPeriod = findTextViewById(R.id.settlement_setting_period_textview);
        tvAmount = findTextViewById(R.id.settlement_setting_amount);
        tvToday = findTextViewById(R.id.settlement_setting_period_today);
        tvYesterday = findTextViewById(R.id.settlement_setting_period_yesterday);
        tvUserDefined = findTextViewById(R.id.settlement_setting_period_userDefined);
        workingTimeContainer = findViewById(R.id.settlement_setting_workingTime_container);
        periodTextviewContainer = findViewById(R.id.settlement_setting_period_textview_container);

        int backgroundColor = ColorType.toInt(MyApp.getThemeColors(context).first);
        findViewById(R.id.settlement_setting_period_container).setBackgroundColor(backgroundColor);
        workingTimeContainer.setBackgroundColor(backgroundColor);
        periodTextviewContainer.setBackgroundColor(backgroundColor);

        emptyGuide = new MyEmptyGuide(context, getView(), adapter, listView);
        emptyGuide.setContactUsButton(View.GONE);
        emptyGuide.setMessage("거래 내역이 없습니다");

        setOnClick(R.id.settlement_setting_period_today, R.id.settlement_setting_period_yesterday, R.id.settlement_setting_period_userDefined,
                R.id.settlement_setting_period_timeSetting, R.id.settlement_setting_close, R.id.settlement_setting_settle);

        restoreWorkingTime();
        setWorkingTime(getView());

        dayPeriodType = DayPeriodType.NotDefined;
        fromDate = Empty.dateTime;
        toDate = Empty.dateTime;

        refresh();
    }

    @Override
    public int getThisView() {
        return R.layout.activity_settlement_setting;
    }

    private void restoreWorkingTime() {
        if (GlobalDB.DayStartTime.isExist(context) == false) {
            double hourLine = keyValueDB.getDouble("statistics.hourline", 0.0);
            Pair<Double, Double> pair = SilverPreference.getShiftTimes(context);
            if (hourLine > 0) {
                GlobalDB.DayStartTime.put(context, hourLine);
            } else {
                if (pair.first >= 0)
                    GlobalDB.DayStartTime.put(context, pair.first);
                if (pair.second >= 0)
                    GlobalDB.NightStartTime.put(context, pair.second);
                if (pair.first >= 0 || pair.second >= 0)
                    Log.i("DEBUG_JS", String.format("[WorkingTimeDialog] copy preference (%f, %f) --> global DB", pair.first, pair.second));
            }
        }
    }

    private void setWorkingTime(View view) {
        workingTime = new MyRadioGroup<>(context, view, 1.0, this);
        workingTime.add(R.id.settlement_setting_allDay, WorkingTime.AllDay);
        workingTime.add(R.id.settlement_setting_day, WorkingTime.Day);
        workingTime.add(R.id.settlement_setting_night, WorkingTime.Night);
        workingTime.setChecked(WorkingTime.AllDay);
        workingTime.setOnClickListener(false, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshSettlementRecord();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SimplePosSlipSharingView.startActivity(context, transDB.getFullRecord(adapter.getRecord(position).getSlipKey().toSID()));
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        final MyTriple<Double, Double, Boolean> timeSetting = BaseUtils.getTimeSetting(context);
        if (view.getId() == R.id.settlement_setting_period_today) {
            dayPeriodType = DayPeriodType.Today;
            refreshPeriodButtons(dayPeriodType);
            Pair<DateTime, DateTime> period = BaseUtils.getDayPeriod(timeSetting.first, 0);
            fromDate = period.first;
            toDate = period.second;
            downloadTrans(fromDate, toDate); // 무조건 하루 단위로 다운로드

        } else if (view.getId() == R.id.settlement_setting_period_yesterday) {
            dayPeriodType = DayPeriodType.Yesterday;
            refreshPeriodButtons(dayPeriodType);
            Pair<DateTime, DateTime> period = BaseUtils.getDayPeriod(timeSetting.first, 1);
            fromDate = period.first;
            toDate = period.second;
            downloadTrans(fromDate, toDate);

        } else if (view.getId() == R.id.settlement_setting_period_userDefined) {
            dayPeriodType = DayPeriodType.UserDefied;
            refreshPeriodButtons(dayPeriodType);
            new OnUserDefinedClick(context).show();

        } else if (view.getId() == R.id.settlement_setting_period_timeSetting) {
            new WorkingTimeDialog(context, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    switch (dayPeriodType) {
                        case Today:
                            onClick(tvToday);
                            break;
                        case Yesterday:
                            onClick(tvYesterday);
                            break;
                        case UserDefied:
                            MyUI.toastSHORT(context, String.format("직접입력 상태입니다"));
                            break;
                        default:
                            break;
                    }
                }
            });

        } else if (view.getId() == R.id.settlement_setting_close) {
            finish();

        } else if (view.getId() == R.id.settlement_setting_settle) {
            if (fromDate.equals(Empty.dateTime) || toDate.equals(Empty.dateTime)) {
                MyUI.toastSHORT(context, String.format("대상 날짜를 선택해주세요"));
                return;
            }

            settlementRecord.setPeriod(getPeriod(workingTime.getCheckedItem()));
            SettlementView.startActivity(context, settlementRecord);
        }
    }

    class OnUserDefinedClick extends MyButtonsDialog {

        public OnUserDefinedClick(final Context context) {
            super(context, "날짜 선택", "");
            final int rangeDayLimit = userSettingData.isMasterOrNoMaster()? 365 : 30;
            final int periodDayLimit = 1; // 마감은 하루내에서만 선택하도록 하자

            addButton("달력에서 선택", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    new PickDateDialog(context, "날짜 선택", false, rangeDayLimit) {
                        @Override
                        public void onApplyClick(DateTime finalDateTime) {
                            Pair<DateTime, DateTime> period = BaseUtils.toPeriod(finalDateTime, BaseUtils.getTimeSetting(context), WorkingTime.AllDay);
                            fromDate = period.first;
                            toDate = period.second;
                            downloadTrans(fromDate, toDate); // 무조건 하루 단위로 다운로드
                        }
                    };
                }
            });

            addButton("직접 입력", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();

                    new SetPeriodDialog(context, fromDate, toDate, rangeDayLimit, periodDayLimit) {

                        @Override
                        public void onApplyClick(DateTime startDateTime, DateTime endDateTime) {
                            fromDate = startDateTime;
                            toDate = endDateTime;
                            downloadTrans(fromDate, toDate);
                        }
                    };

                }
            });
        }
    }

    private void refreshPeriodButtons(DayPeriodType dayPeriodType) {
        tvToday.setTextColor(ColorType.White.colorInt);
        tvToday.setTypeface(Typeface.DEFAULT);
        tvYesterday.setTextColor(ColorType.White.colorInt);
        tvYesterday.setTypeface(Typeface.DEFAULT);
        tvUserDefined.setTextColor(ColorType.White.colorInt);
        tvUserDefined.setTypeface(Typeface.DEFAULT);

        switch (dayPeriodType) {
            case Today:
                tvToday.setTextColor(ColorType.Blue.colorInt);
                tvToday.setTypeface(Typeface.DEFAULT_BOLD);
                break;
            case Yesterday:
                tvYesterday.setTextColor(ColorType.Blue.colorInt);
                tvYesterday.setTypeface(Typeface.DEFAULT_BOLD);
                break;
            case UserDefied:
                tvUserDefined.setTextColor(ColorType.Blue.colorInt);
                tvUserDefined.setTypeface(Typeface.DEFAULT_BOLD);
                break;
        }
    }

    private void refreshTextViews() {
        tvPeriod.setText(String.format("기간: %s ~ %s", BaseUtils.toStringWithWeekDay(fromDate, BaseUtils.MDHm_Format),
                BaseUtils.toStringWithWeekDay(toDate, BaseUtils.MDHm_Format)));
        MyTriple<Double, Double, Boolean> timeSetting = BaseUtils.getTimeSetting(context);
        switch (dayPeriodType) {
            case NotDefined:
                periodTextviewContainer.setVisibility(View.GONE);
                workingTimeContainer.setVisibility(View.GONE);
                break;
            case UserDefied:
            case Today:
            case Yesterday:
                periodTextviewContainer.setVisibility(View.VISIBLE);
                workingTimeContainer.setVisibility(timeSetting.third ? View.VISIBLE : View.GONE); // 주야간 교대?
                break;
        }
        tvAmount.setText(String.format("합계: %s",BaseUtils.toCurrencyStr(settlementRecord.summaryRecord.amount)));
    }

    public void refreshSettlementRecord() {
        MyTriple<Double, Double, Boolean> timeSetting = BaseUtils.getTimeSetting(context);
        switch (workingTime.getCheckedItem()) {
            case AllDay:
                settlementRecord.setTransAndSummarize(transDB.getRecords(), transDB.items);
                break;
            case Day:
                settlementRecord.setTransAndSummarize(transDB.getRecordsByHour(timeSetting.first, timeSetting.second, true), transDB.items);
                break;
            case Night:
                settlementRecord.setTransAndSummarize(transDB.getRecordsByHour(timeSetting.first, timeSetting.second, false), transDB.items);
                break;
        }
        refresh();
    }

    public Pair<DateTime, DateTime> getPeriod(WorkingTime workingTime) {
        MyTriple<Double, Double, Boolean> timeSetting = BaseUtils.getTimeSetting(context);
        switch (workingTime) {
            default:
            case AllDay:
                return Pair.create(fromDate, toDate);
            case Day:
                return BaseUtils.splitPeriod(fromDate, toDate, timeSetting.second, 0);
            case Night:
                return BaseUtils.splitPeriod(fromDate, toDate, timeSetting.second, 1);
        }
    }

    public void refresh() {
        if (adapter == null) return;

        new MyAsyncTask(context, "SettlementSettingView") {

            @Override
            public void doInBackground() {
                adapter.setRecords(settlementRecord.getTrans());
            }

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
                emptyGuide.refresh();
                refreshTextViews();
            }
        };

    }

    public void downloadTrans(final DateTime fromDateTime, final DateTime toDateTime) {

        new MyServiceTask(context, "downloadTrans", "최신 정보 업데이트중...") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                // 거래내역건
                List<SlipDataProtocol> transProtocols = transactionService.getTransactions(userSettingData.getUserShopInfo(), fromDateTime, toDateTime, userSettingData.isBuyer());
                transDB.set(transProtocols, false);
            }

            @Override
            public void onPostExecutionUI() {
                refreshSettlementRecord();
            }

        };
    }


    @Override
    public void onMyMenuCreate() {
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
    }



    class SettleTransDB extends MemorySlipDB {

        protected SettleTransDB(Context context) {
            super(context, "SettleTransDB");
            items = new SettleTransItemDB(context);
        }

        public List<SlipRecord> getRecords() {
            List<SlipRecord> results = super.getRecords();
            Collections.sort(results);
            return results;
        }

        public List<SlipRecord> getRecordsByHour(double fromHour, double toHour, boolean isInRange) {
            List<SlipRecord> results = new ArrayList<>();
            for (SlipRecord record : getRecords()) {
                if (isInRange) {
                    if (BaseUtils.isBetweenByHm(record.createdDateTime, fromHour, toHour))
                        results.add(record);
                } else {
                    if (BaseUtils.isBetweenByHm(record.createdDateTime, fromHour, toHour) == false)
                        results.add(record);
                }
            }
            return results;
        }
    }

    class SettleTransItemDB extends MemorySlipItemDB {

        protected SettleTransItemDB(Context context) {
            super(context, "SettleTransItemDB");
        }

    }

    class SettleTransDBAdapter extends BaseSlipDBAdapter {

        public SettleTransDBAdapter(Context context, MemorySlipDB transDB) {
            super(context, transDB);
        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new ViewHolder();
        }

        @Override
        public void generate() {
        }

    }
}
