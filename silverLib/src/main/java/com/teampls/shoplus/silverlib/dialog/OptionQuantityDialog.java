package com.teampls.shoplus.silverlib.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;

/**
 * Created by Medivh on 2017-11-10.
 */

abstract public class OptionQuantityDialog extends UpdateValueDialog {
    private boolean hasQuantity = false, doCheckStock = false;
    private ItemOptionRecord optionRecord;

    public OptionQuantityDialog(Context context, ItemOptionRecord optionRecord, boolean doCheckStock) {
        super(context, "수량 입력", String.format("[옵션] %s ... 재고 : %d", optionRecord.toStringWithDefault(" : "), optionRecord.stock), optionRecord.revStock, optionRecord.revStock >= 1? optionRecord.revStock : 1 );
        setDialogWidth(0.9, 0.6);
        this.optionRecord = optionRecord;
        this.doCheckStock = doCheckStock;
        hasQuantity = optionRecord.revStock >= 1;

        switch (MyDevice.getDeviceSize(context)) {
            default:
                Window window = this.getWindow();
                WindowManager.LayoutParams params = window.getAttributes();
                params.y = MyDevice.toPixel(context, 20);
                params.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
                params.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(params);
                break;
            case W600:
            case W720:
            case W800:
                break;
        }
        show();
    }

    @Override
    public void onApplyClick(String newValue) {
        int newRevStock = BaseUtils.toInt(newValue);
        if (TextUtils.isEmpty(newValue)) {
            if (hasQuantity == false)
                newRevStock = 1;
        }

        if (doCheckStock && optionRecord.stock < newRevStock) {
            doDismiss = false;
            final int finalNewRevStock = newRevStock;
            new MyAlertDialog(context, "재고부족", String.format("현재 : %d개, 입력 : %d개\n\n계속 입력하시겠습니까?", optionRecord.stock, newRevStock)) {
                @Override
                public void yes() {
                    MyDevice.hideKeyboard(context, etString);
                    dismiss();
                    onApplyClick(finalNewRevStock);
                }
            };
        } else {
            onApplyClick(newRevStock);
        }
    }

    abstract public void onApplyClick(int newRevStock);
}
