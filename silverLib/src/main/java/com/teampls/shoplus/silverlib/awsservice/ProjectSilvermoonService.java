package com.teampls.shoplus.silverlib.awsservice;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.SlipJSONData;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.datatypes.ItemTraceData;
import com.teampls.shoplus.lib.datatypes.ItemTraceIdData;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;
import com.teampls.shoplus.silverlib.ProjectSilvermoonServiceProtocol;
import com.teampls.shoplus.silverlib.datatypes.SettlementAuxilityData;
import com.teampls.shoplus.silverlib.enums.SettlementAuxilityDataType;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * ProjectSilvermmonServiceProtocol에 대한 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectSilvermoonService implements ProjectSilvermoonServiceProtocol {

   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // mPOS에서 챙길물건 정보를 가져오기 위해 복사해 옴
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private List<SlipDataProtocol> getSlipsFromRemote(String resource, Boolean followingMode, Boolean isBuyer) throws MyServiceFailureException {
        try {
            Map<String, String> queryParams = new HashMap<>();
            if (followingMode) {
                queryParams.put("follow", "true");
            }
            queryParams.put("buyer", isBuyer.toString());
            String response = APIGatewayClient.requestGET(resource, queryParams, UserHelper.getIdentityToken());
            return SlipJSONData.build(response);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    private String getPhoneNumberForDalaran(UserShopInfoProtocol userShop) {
        return userShop.getMyShopOrUserPhoneNumber();     // Dalaran에서는 메인 샵을 사용하지 않는다.
    }

    private String getPeriodSpecificSlipsResourcePath(String phoneNumber, DateTime fromDate, DateTime toDate) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "sales-slips", "period",
                APIGatewayHelper.getRemoteDateString(fromDate) + "-" + APIGatewayHelper.getRemoteDateString(toDate));
    }

    @Override
    public List<SlipDataProtocol> getPeriodSpecificSlips(UserShopInfoProtocol userShop, DateTime fromDate, DateTime toDate, Boolean isBuyer) throws MyServiceFailureException {
        String resource = this.getPeriodSpecificSlipsResourcePath(getPhoneNumberForDalaran(userShop), fromDate, toDate);
        return this.getSlipsFromRemote(resource, false, isBuyer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private String getMyItemStatesResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "state");
    }

    private String getMainShopPhoneNumberForSilvermoon(UserShopInfoProtocol userShop) {
        return userShop.getMainShopOrMyShopOrUserPhoneNumber();     // Silvermoon에서는 메인 샵 정보를 사용한다
    }


    @Override
    public void updateItemState(UserShopInfoProtocol userShop, int itemid, ItemStateType newState) throws MyServiceFailureException {
        Set<Integer> itemids = new HashSet<>();
        itemids.add(itemid);
        this.updateItemState(userShop, itemids, newState);
    }

    @Override
    public void updateItemState(UserShopInfoProtocol userShop, Set<Integer> itemids, ItemStateType newState) throws MyServiceFailureException {
        try {
            String resource = this.getMyItemStatesResourcePath(getMainShopPhoneNumberForSilvermoon(userShop));
            JSONArray itemIdArr = new JSONArray(itemids);
            JSONObject requestBody = new JSONObject()
                    .put("items", itemIdArr)
                    .put("item_state", newState.toRemoteStr());
            APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public ItemTraceData getTrace(UserShopInfoProtocol userShop, String itemTraceId) throws MyServiceFailureException {
        ItemTraceIdData data = new ItemTraceIdData(itemTraceId);
        if (data.isValid()) {
            String resource = this.getItemTraceDataForAnItemTraceIdResourcePath(getMainShopPhoneNumberForSilvermoon(userShop), data);
            List<ItemTraceData> result = this.getTraces(resource);
            if (!result.isEmpty()) {
                return result.get(0);
            } else {
                throw new MyServiceFailureException("NotRegistered", "The code is not registered");
            }
        } else {
            throw new MyServiceFailureException("InvalidCode", "Invalid item trace code");
        }

    }

    private String getItemTraceDataForAnItemTraceIdResourcePath(String phoneNumber, ItemTraceIdData data) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "item-trace", "items",
                String.valueOf(data.getItemOption().getItemId()),
                data.getItemOption().getColorOption().toRemoteStr() + "-"
                        + data.getItemOption().getSizeOption().toRemoteStr() + "-"
                        + data.getSerialNumber()
        );
    }

    private List<ItemTraceData> getTraces(String resource) throws MyServiceFailureException {
        try {
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            JSONArray traceDataArr = new JSONArray(response);
            List<ItemTraceData> result = new ArrayList<>();
            for (int i = 0; i < traceDataArr.length(); ++i) {
                result.add(new ItemTraceData(traceDataArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<ItemTraceData> getTraces(UserShopInfoProtocol userShop, ItemStockKey itemOption) throws MyServiceFailureException {
        String resource = this.getItemTraceDataForAnItemOptionResourcePath(getMainShopPhoneNumberForSilvermoon(userShop), itemOption);
        return this.getTraces(resource);
    }

    private String getItemTraceDataForAnItemOptionResourcePath(String phoneNumber, ItemStockKey itemOption) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "item-trace", "items",
                String.valueOf(itemOption.getItemId()),
                itemOption.getColorOption().toRemoteStr() + "-" + itemOption.getSizeOption().toRemoteStr() + "-");
    }

    ////////////////////////////////////////////////////////////////////////////////
    // [STORM-192] 상품 링크 기능

    private String getPhoneNumberForTransaction(UserShopInfoProtocol userShop) {
        return userShop.getMyShopOrUserPhoneNumber();
    }

    private String getLinkOfItemsInATransactionResourcePath(String phoneNumber, String transactionId) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", transactionId, "links", "items");
    }

    @Override
    public String getItemsLinkOfTransaction(UserShopInfoProtocol userShop, String transactionId) throws MyServiceFailureException {
        try {
            String resource = this.getLinkOfItemsInATransactionResourcePath(this.getPhoneNumberForTransaction(userShop), transactionId);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            return new JSONObject(response).getString("tr-item-link");
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // [SILVER-435] 결산 관련 보조기능

    private String getPhoneNumberForSettlement(UserShopInfoProtocol userShop) {
        return userShop.getMyShopOrUserPhoneNumber();       // 결산 용도로는 메인 샵을 사용하지 않는다.
    }

    private String getSettlementAuxDataResource(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "settlements", "aux-data");
    }

    private String getSettlementAuxDataOfPeriodResourcePath(String phoneNumber, DateTime startDate, DateTime endDate) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "settlements", "aux-data",
                APIGatewayHelper.getRemoteDateTimeString(startDate) + "-" + APIGatewayHelper.getRemoteDateTimeString(endDate));
    }

    private JSONObject getBaseRequestDataObjForSettlementAuxData(int amount, DateTime dateTime) throws JSONException {
        return new JSONObject()
                .put("amount", amount)
                .put("created", APIGatewayHelper.getRemoteDateTimeString(dateTime));
    }

    @Override
    public List<SettlementAuxilityData> getSettlementAuxilityData(UserShopInfoProtocol userShop, DateTime startDate, DateTime endDate) throws MyServiceFailureException {
        try {
            String resource = this.getSettlementAuxDataOfPeriodResourcePath(this.getPhoneNumberForSettlement(userShop), startDate, endDate);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            JSONArray settlementAuxDataArr = new JSONArray(response);
            List<SettlementAuxilityData> result = new ArrayList<>();
            for (int i = 0; i < settlementAuxDataArr.length(); ++i) {
                result.add(new SettlementAuxilityData(settlementAuxDataArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public SettlementAuxilityData postSettlementExpenseData(UserShopInfoProtocol userShop, String title, int amount, DateTime dateTime) throws MyServiceFailureException {
        try {
            JSONObject requestBody = getBaseRequestDataObjForSettlementAuxData(amount, dateTime)
                    .put("aux_type", SettlementAuxilityDataType.EXPENSE.toRemoteStr());
            if (title != null && !title.isEmpty()) {
                requestBody.put("title", title);
            }
            return this.postSettlementAuthData(userShop, requestBody);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public SettlementAuxilityData postSettlementAmountOnHandData(UserShopInfoProtocol userShop, int amount, DateTime dateTime) throws MyServiceFailureException {
        try {
            JSONObject requestBody = getBaseRequestDataObjForSettlementAuxData(amount, dateTime)
                    .put("aux_type", SettlementAuxilityDataType.AMOUNT_ON_HAND.toRemoteStr());
            return this.postSettlementAuthData(userShop, requestBody);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    private SettlementAuxilityData postSettlementAuthData(UserShopInfoProtocol userShop, JSONObject requestDataObj) throws MyServiceFailureException {
        try {
            String resource = this.getSettlementAuxDataResource(this.getPhoneNumberForSettlement(userShop));
            JSONObject requestBody = new JSONObject().put("data", requestDataObj);
            String response = APIGatewayClient.requestPOST(resource, requestBody.toString(), UserHelper.getIdentityToken());
            JSONObject result = new JSONObject(response);
            return new SettlementAuxilityData(result);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }
}
