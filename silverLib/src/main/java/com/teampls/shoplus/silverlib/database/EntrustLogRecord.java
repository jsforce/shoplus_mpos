package com.teampls.shoplus.silverlib.database;

import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.silverlib.datatypes.SettlementAuxilityData;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2019-06-26.
 */

public class EntrustLogRecord {
    public String counterpartName = "";
    public int id = 0, amount = 0;
    public DateTime createdDate = Empty.dateTime;

    public EntrustLogRecord(DateTime createdDate, String counterpartName, int amount) {
        this.createdDate = createdDate;
        this.counterpartName = counterpartName;
        this.amount = amount;
    }

    public EntrustLogRecord() {

    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] <%d> %s, %s, %d", location, id, createdDate.toString(BaseUtils.fullFormat), counterpartName, amount));
    }

    public SettlementAuxilityData toSettlementAuxilityData() {
        return new SettlementAuxilityData(createdDate, counterpartName, amount);
    }
}
