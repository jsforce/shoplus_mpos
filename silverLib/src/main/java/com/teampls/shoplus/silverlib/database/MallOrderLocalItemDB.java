package com.teampls.shoplus.silverlib.database;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.database.PosSlipItemDB;

import java.util.List;

/**
 * Created by Medivh on 2019-02-09.
 */

public class MallOrderLocalItemDB extends PosSlipItemDB {
    private static MallOrderLocalItemDB instance;

    public static MallOrderLocalItemDB getInstance(Context context) {
        if (instance == null)
            instance = new MallOrderLocalItemDB(context);
        return instance;
    }

    private MallOrderLocalItemDB(Context context) {
        super(context, "MallOrderLocalItemDB", Ver, Column.toStrs());
    }

    public void deleteInvalids(List<String> validOrderIds) {
        List<String> allOrderIds = getStrings(Column.slipKey);
        allOrderIds.removeAll(validOrderIds);
        deleteAll(Column.slipKey, allOrderIds);
    }
}
