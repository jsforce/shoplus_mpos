package com.teampls.shoplus.silverlib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Pair;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.QueryAndRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.event.MyOnTask;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Medivh on 2018-07-15.
 */
// 백업용도로만 사용한다. 직접 자료를 가져오지 않는다 (Async로 저장중이기 때문)
public class StockHistoryDB extends BaseDB<OptionStockRecord> {
    private static StockHistoryDB instance = null;

    public enum Column {
        _id, shopPhoneNumber, createdDateTime, itemId, color, size, sizeOrdinal, stock;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    private StockHistoryDB(Context context) {
        super(context, "StockHistoryDB", 1, Column.toStrs());
    }

    public static StockHistoryDB getInstance(Context context) {
        if (instance == null)
            instance = new StockHistoryDB(context);
        return instance;
    }

    @Override
    protected ContentValues toContentvalues(OptionStockRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.createdDateTime.toString(), record.createdDateTime.toString());
        contentvalues.put(Column.shopPhoneNumber.toString(), record.shopPhoneNumber);
        contentvalues.put(Column.itemId.toString(), record.itemId);
        contentvalues.put(Column.color.toString(), record.color.toString());
        contentvalues.put(Column.size.toString(), record.size.toString());
        contentvalues.put(Column.sizeOrdinal.toString(), record.size.ordinal());
        contentvalues.put(Column.stock.toString(), record.stock);
        return contentvalues;
    }

    @Override
    protected int getId(OptionStockRecord record) {
        return record.id;
    }

    @Override
    protected OptionStockRecord getEmptyRecord() {
        return new OptionStockRecord();
    }

    @Override
    protected OptionStockRecord createRecord(Cursor cursor) {
        return new OptionStockRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.shopPhoneNumber.ordinal()),
                BaseUtils.toDateTime(cursor.getString(Column.createdDateTime.ordinal())),
                cursor.getInt(Column.itemId.ordinal()),
                ColorType.valueOfDefault(cursor.getString(Column.color.ordinal())),
                SizeType.valueOfDefault(cursor.getString(Column.size.ordinal())),
                cursor.getInt(Column.stock.ordinal())
        );
    }

    @Override
    protected OptionStockRecord getRecordByKey(OptionStockRecord record) {
        List<QueryAndRecord> queryAndRecords = new QueryAndRecord(Column.shopPhoneNumber, record.shopPhoneNumber).toList();
        queryAndRecords.add(new QueryAndRecord(Column.createdDateTime, record.createdDateTime.toString()));
        queryAndRecords.add(new QueryAndRecord(Column.itemId, record.itemId));
        queryAndRecords.add(new QueryAndRecord(Column.color, record.color.toString()));
        queryAndRecords.add(new QueryAndRecord(Column.size, record.size.toString()));
        return getRecordByQuery(queryAndRecords);
    }

    public List<DateTime> getCreatedDateTimes() {
        List<String> dates = getStrings(Column.createdDateTime);
        dates = new ArrayList<>(new HashSet<>(dates));
        Collections.sort(dates);
        Collections.reverse(dates);
        List<DateTime> results = new ArrayList<>();
        for (String dateStr : dates)
            results.add(BaseUtils.toDateTime(dateStr));
        return results;
    }

    public MyMap<DateTime, Integer> getStockSums(String shopPhoneNumber, int itemId) {
        List<QueryAndRecord> queryAndRecords = new QueryAndRecord(Column.shopPhoneNumber, shopPhoneNumber).toList();
        queryAndRecords.add(new QueryAndRecord(Column.itemId, itemId));
        MyMap<DateTime, Integer> results = new MyMap<>(0);
        for (OptionStockRecord record : getRecords(queryAndRecords)) {
            int stockSum = results.get(record.createdDateTime);
            stockSum = stockSum + record.stock;
            results.put(record.createdDateTime, stockSum);
        }
        return results;
    }

    public OptionStockRecord getRecordBy(OptionStockRecord record) {
        List<QueryAndRecord> queryAndRecords = new QueryAndRecord(Column.shopPhoneNumber, record.shopPhoneNumber).toList();
        queryAndRecords.add(new QueryAndRecord(Column.createdDateTime, record.createdDateTime.toString()));
        queryAndRecords.add(new QueryAndRecord(Column.itemId, record.itemId));
        queryAndRecords.add(new QueryAndRecord(Column.color, record.color.toString()));
        queryAndRecords.add(new QueryAndRecord(Column.size, record.size.toString()));
        return getRecordByQuery(queryAndRecords);
    }

    public List<OptionStockRecord> getRecordsBy(int itemId) {
        List<OptionStockRecord> results = getRecords(Column.itemId, Integer.toString(itemId));
        Collections.sort(results);
        return results;
    }

    public List<ItemOptionRecord> getOptions(int itemId) {
        Set<ItemOptionRecord> results = new HashSet<>();
        for (OptionStockRecord record : getRecords(Column.itemId, Integer.toString(itemId))) {
            results.add(record.toOptionRecord());
        }
        List<ItemOptionRecord> listResult = new ArrayList<>(results);
        Collections.sort(listResult);
        return listResult;
    }

    public void cleanUpByDays(final int daysLimit, final MyOnTask onTask) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                DateTime fromDateTime = Empty.dateTime; // 1900-1-1
                DateTime toDateTime = BaseUtils.createDay().minusDays(daysLimit).minusMillis(1);
                List<Integer> ids = new ArrayList<>();
                for (OptionStockRecord record : getRecordsBetween(Column.createdDateTime, fromDateTime.toString(), toDateTime.toString(), null))
                    ids.add(record.id);
                deleteAll(Column._id, ids);
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }
}
