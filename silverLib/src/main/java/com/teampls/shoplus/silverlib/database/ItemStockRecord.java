package com.teampls.shoplus.silverlib.database;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_memory.MSortingKey;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-07-19.
 */

public  class ItemStockRecord implements Comparable<ItemStockRecord> {
    public int itemId = 0, stockSum = 0;
    public String itemName = "";
    public MSortingKey sortingKey = MSortingKey.ID;
    public MyMap<DateTime, Integer> stockSums = new MyMap<DateTime, Integer>(0);

    public ItemStockRecord() {};

    public ItemStockRecord(ItemRecord itemRecord) {
        this.itemId = itemRecord.itemId;
        this.itemName = itemRecord.getUiName();
    }

    @Override
    public int compareTo(@NonNull ItemStockRecord record) {
        switch (sortingKey) {
            default:
            case ID:
                return -((Integer) itemId).compareTo(record.itemId);
            case Name:
                if (itemName.isEmpty() && record.itemName.isEmpty()) {
                    return 0;
                } else if (itemName.isEmpty()) {
                    return 1;
                } else if (record.itemName.isEmpty()) {
                    return -1;
                } else {
                    return itemName.compareTo(record.itemName);
                }
            case Count:
                return -((Integer) stockSum).compareTo(record.stockSum);
        }
    }
}