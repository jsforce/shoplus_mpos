package com.teampls.shoplus.silverlib;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.datatypes.ItemTraceData;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;
import com.teampls.shoplus.silverlib.datatypes.SettlementAuxilityData;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Set;

/**
 * @author lucidite
 */

public interface ProjectSilvermoonServiceProtocol {

    List<SlipDataProtocol> getPeriodSpecificSlips(UserShopInfoProtocol userShop, DateTime fromDate, DateTime toDate, Boolean isBuyer) throws MyServiceFailureException;


    /**
     * 아이템 상태를 변경한다.
     *
     * @param userShop
     * @param itemid
     * @param newState
     * @throws MyServiceFailureException
     */
    void updateItemState(UserShopInfoProtocol userShop, int itemid, ItemStateType newState) throws MyServiceFailureException;

    /**
     * 여러 아이템 상태를 변경한다. (변경할 상태는 한 상태로 고정)
     *
     * @param userShop
     * @param itemids
     * @param newState
     * @throws MyServiceFailureException
     */
    void updateItemState(UserShopInfoProtocol userShop, Set<Integer> itemids, ItemStateType newState) throws MyServiceFailureException;

    /**
     * 특정 라벨의 현재 상태를 조회한다.
     *
     * @param userShop
     * @param itemTraceId
     * @return
     * @throws MyServiceFailureException
     */
    ItemTraceData getTrace(UserShopInfoProtocol userShop, String itemTraceId) throws MyServiceFailureException;

    /**
     * 특정 아이템 옵션의 전체 추적 재고 현황을 조회한다.
     *
     * @param userShop
     * @param itemOption
     * @return 해당 아이템의 전체 추적 재고 현황 목록
     * @throws MyServiceFailureException
     */
    List<ItemTraceData> getTraces(UserShopInfoProtocol userShop, ItemStockKey itemOption) throws MyServiceFailureException;

    /**
     * [STORM-192] 특정 거래내역에 포함된 상품의 링크 주소를 조회한다.
     *             주의! 신상알림 사용자 전용으로, 현재는 서버에서 사용 권한을 확인하지 않으므로 앱에서 사용 권한 확인 후 제공해야 한다.
     *             (차후 서버에서 사용 권한을 확인하는 방안 검토)
     *
     * @param userShop
     * @param transactionId
     * @return
     * @throws MyServiceFailureException
     */
    String getItemsLinkOfTransaction(UserShopInfoProtocol userShop, String transactionId) throws MyServiceFailureException;

    ////////////////////////////////////////////////////////////////////////////////
    // [SILVER-435] 결산 관련 보조기능

    /**
     * 특정 기간 내의 결산 보조 데이터를 조회한다.
     * @param userShop
     * @param startDate
     * @param endDate
     * @return
     * @throws MyServiceFailureException
     */
    List<SettlementAuxilityData> getSettlementAuxilityData(UserShopInfoProtocol userShop, DateTime startDate, DateTime endDate) throws MyServiceFailureException;

    /**
     * 특정 시점에 (현금) 비용 데이터를 추가한다.
     *
     * @param userShop
     * @param title
     * @param amount
     * @param dateTime
     * @return
     * @throws MyServiceFailureException
     */
    SettlementAuxilityData postSettlementExpenseData(UserShopInfoProtocol userShop, String title, int amount, DateTime dateTime) throws MyServiceFailureException;

    /**
     * 특정 시점에 (현금) 시재 데이터를 추가한다.
     *
     * @param userShop
     * @param amount
     * @param dateTime
     * @return
     * @throws MyServiceFailureException
     */
    SettlementAuxilityData postSettlementAmountOnHandData(UserShopInfoProtocol userShop, int amount, DateTime dateTime) throws MyServiceFailureException;
}
