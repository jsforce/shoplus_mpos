package com.teampls.shoplus.silverlib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotSettingData;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotAuthModeType;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOpenModeType;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.MyCheckBox;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.common.SilverAppWatcher;
import com.teampls.shoplus.silverlib.database.MallOrderDB;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotOrderData;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotOrderDataProtocol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Medivh on 2019-02-09.
 */

public class MallOrdersView extends BaseActivity implements AdapterView.OnItemClickListener {
    private MallOrderDBAdapter adapter;
    private ListView lvOrders;
    private MyEmptyGuide myEmptyGuide;
    private MallOrderDB orderDB;
    private boolean onDownloading = false;
    private MyCheckBox cbShowCancelled;
    private static MyOnTask onTask;

    public static void startActivity(Context context, MyOnTask onTask) {
        MallOrdersView.onTask = onTask;
        context.startActivity(new Intent(context, MallOrdersView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SilverAppWatcher.checkInstanceAndRestart(this) == false) return;
        orderDB = MallOrderDB.getInstance(context);
        adapter = new MallOrderDBAdapter(context);

        lvOrders = findViewById(R.id.mall_orders_listview);
        lvOrders.setAdapter(adapter);
        lvOrders.setOnItemClickListener(this);
        cbShowCancelled = new MyCheckBox(keyValueDB, getView(), R.id.mall_orders_showCancelled, "mallorder.show.cancelled", true);
        cbShowCancelled.setOnClick(false, new MyOnClick<Boolean>() {
            @Override
            public void onMyClick(View view, Boolean record) {
                refresh();
            }
        });

        myEmptyGuide = new MyEmptyGuide(context, getView(), adapter, lvOrders);
        myEmptyGuide.setMessage("주문 내역이 없습니다");
        myEmptyGuide.setContactUsButton(View.GONE);

        setOnClick(R.id.mall_orders_close, R.id.mall_orders_items);
        downloadRecentOrders();
    }

    @Override
    public int getThisView() {
        return R.layout.activity_mall_orders;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (onDownloading == false)
            refresh(); // 항상 최신화
    }

    public void refresh() {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                adapter.showCancelled(cbShowCancelled.isChecked());
                adapter.generate(false);
            }

            @Override
            public void onPostExecutionUI() {
                adapter.applyGeneratedRecords();
                adapter.notifyDataSetChanged();
                myEmptyGuide.refresh();
            }
        };
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.mall_orders_close) {
            if (onTask != null)
                onTask.onTaskDone("");
            finish();
        } else if (view.getId() == R.id.mall_orders_items) {
            MallSelectionView.startActivity(context);
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.counterpart)
                .add(MyMenu.refresh)
                .add(MyMenu.setting)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case setting:
                new MyServiceTask(context, "MallOrdersView", "설정 확인중...") {
                    ChatbotSettingData settingData;

                    @Override
                    public void doInBackground() throws MyServiceFailureException {
                        settingData = chatbotService.getChatbotSetting(userSettingData.getUserShopInfo());
                    }

                    @Override
                    public void onPostExecutionUI() {
                        new MallSettingDialog(context, settingData).show();
                    }
                };
                break;
            case counterpart:
                MallBuyersView.startActivity(context);
                break;
            case refresh:
                downloadRecentOrders();
                break;
            case close:
                if (onTask != null)
                    onTask.onTaskDone("");
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final ChatbotOrderData orderData = adapter.getRecord(position);
        switch (orderData.getState()) {
            case FINISHED:
                // 해당 영수증을 가져온다
                SlipRecord slipRecord = PosSlipDB.getInstance(context).getRecordById(orderData.getLinkedTransactionId());
                if (slipRecord.id > 0) {
                    SlipFullRecord slipFullRecord = PosSlipDB.getInstance(context).getFullRecord(slipRecord);
                    MallSlipSharingView.startActivity(context, slipFullRecord, orderData);
                } else {
                    downloadTransaction(orderData.getLinkedTransactionId(), new MyOnTask<SlipFullRecord>() {
                        @Override
                        public void onTaskDone(SlipFullRecord slipFullRecord) {
                            MallSlipSharingView.startActivity(context, slipFullRecord, orderData);
                        }
                    });
                }
                break;
            case CANCELLED:
            case REJECTED:
            case IN_PROGRESS:
                // 작성중 화면으로 이동한다
                MallSlipCreationView.startActivity(context, orderData);
                break;
            case REQUESTED:
                // Progress로 만들고 작성중 화면으로 이동한다.
                setOrderInProgress(orderData.getOrderId(), new MyOnTask<ChatbotOrderData>() {
                    @Override
                    public void onTaskDone(ChatbotOrderData orderData) {
                        MallSlipCreationView.startActivity(context, orderData);
                    }
                });
                break;
        }
    }

    private void setOrderInProgress(final String orderId, final MyOnTask<ChatbotOrderData> onTask) {
        new MyServiceTask(context, "setOrderInProgress", "") {
            Pair<Boolean, ChatbotOrderDataProtocol> pair;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                pair = chatbotService.setOrderInProgress(userSettingData.getUserShopInfo(), orderId);
                orderDB.update(new ChatbotOrderData(pair.second));
            }

            @Override
            public void onPostExecutionUI() {
                if (pair.first)
                    MyUI.toastSHORT(context, String.format("%s 상태로 변경", pair.second.getState().getUiStr()));
                else
                    MyUI.toastSHORT(context, String.format("고객이 주문을 취소했습니다."));

                if (onTask != null)
                    onTask.onTaskDone(new ChatbotOrderData(pair.second));
            }
        };
    }

    private void downloadTransaction(final String transactionId, final MyOnTask<SlipFullRecord> onTask) {
        new MyServiceTask(context, "downloadTransaction") {
            private SlipFullRecord slipFullRecord;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                SlipDataProtocol protocol = transactionService.getATransaction(userSettingData.getUserShopInfo(), transactionId, userSettingData.isBuyer());
                slipFullRecord = new SlipFullRecord(protocol);
                PosSlipDB.getInstance(context).updateOrInsertFast(slipFullRecord);
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone(slipFullRecord);
            }
        };
    }

    protected void downloadRecentOrders() {
        if (onDownloading)
            return;
        onDownloading = true;
        new MyServiceTask(context, "downloadRecentOrders", "카톡주문 다운로드 중...") {
            boolean isNewBuyerFound = false;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<ChatbotOrderDataProtocol> protocols = chatbotService.getRecentOrders(userSettingData.getUserShopInfo());
                orderDB.set(protocols);

                // check if new buyer is found
                for (ChatbotOrderDataProtocol protocol : protocols) {
                    if (protocol.getBuyerPhoneNumber().isEmpty())
                        continue;
                    if (userDB.has(protocol.getBuyerPhoneNumber()) == false) {
                        isNewBuyerFound = true;
                        break;
                    }
                }
            }

            @Override
            public void onPostExecutionUI() {
                onDownloading = false;
                if (isNewBuyerFound) {
                    MyUI.toastSHORT(context, String.format("새 거래처 발견"));
                    downloadBuyers();
                } else {
                    refresh();
                }
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                onDownloading = false;
            }
        };
    }

    protected void downloadBuyers() {
        new CommonServiceTask(context, "downloadRequestedBuyers", "거래처 정보 다운로드 중...") {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                userDB.update(workingShopService.getContactUpdates(userSettingData.getUserShopInfo(), userDB.getLatestTimeStamp()));
            }

            @Override
            public void onPostExecutionUI() {
                refresh();
            }

        };

    }

    class MallOrderDBAdapter extends BaseDBAdapter<ChatbotOrderData> {
        private MallOrderDB orderDB;
        private MyListMap<String, SlipItemRecord> localItemMap;
        private boolean doShowCancelled = true;

        public MallOrderDBAdapter(Context context) {
            super(context);
            orderDB = MallOrderDB.getInstance(context);
        }

        public void showCancelled(boolean value) {
            this.doShowCancelled = value;
        }

        @Override
        public void generate() {
            localItemMap = orderDB.localItems.getListMap();
            generatedRecords.clear();
            List<ChatbotOrderData> requested = new ArrayList<>();
            List<ChatbotOrderData> inProgress = new ArrayList<>();

            List<ChatbotOrderData> orders = orderDB.getRecords();
            Collections.sort(orders); // 날짜순

            for (ChatbotOrderData data : orders) {
                switch (data.getState()) {
                    case REQUESTED:
                        requested.add(data);
                        break;
                    case IN_PROGRESS:
                        inProgress.add(data);
                        break;
                    case CANCELLED:
                    case REJECTED:
                        if (doShowCancelled)
                            generatedRecords.add(data);
                        break;
                    default:
                        generatedRecords.add(data);
                }
            }

            generatedRecords.addAll(0, inProgress);
            generatedRecords.addAll(0, requested);

            if (doSetRecordWhenGenerated)
                applyGeneratedRecords();
        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new ViewHolder();
        }

        public class ViewHolder extends BaseViewHolder {
            protected TextView tvDate, tvCounterpart, tvAmount, tvStatus;
            protected ChatbotOrderData record;
            protected LinearLayout container;

            @Override
            public int getThisView() {
                return R.layout.row_slip_state;
            }

            @Override
            public void init(View view) {
                container = view.findViewById(R.id.row_slip_state_container);
                tvDate = view.findViewById(R.id.row_slip_state_date);
                tvCounterpart = view.findViewById(R.id.row_slip_state_counterpart);
                tvAmount = view.findViewById(R.id.row_slip_state_amount);
                tvStatus = view.findViewById(R.id.row_slip_state_status);
                MyView.setTextViewByDeviceSize(context, tvDate, tvCounterpart, tvAmount, tvStatus);
            }

            @Override
            public void update(int position) {
                record = getRecord(position);
                UserRecord counterpart = UserDB.getInstance(context).getRecordWithNumber(record.getBuyerPhoneNumber());
                SlipSummaryRecord summary = new SlipSummaryRecord(record.getEditingItems(localItemMap.getList(record.getOrderId())));

                if (BasePreference.isShowWeekday(context)) {
                    tvDate.setText(BaseUtils.toStringWithWeekDay(record.getCreatedDateTime(), BaseUtils.MD_FixedSize_Format));
                } else {
                    tvDate.setText(record.getCreatedDateTime().toString(BaseUtils.MD_FixedSize_Format));
                }

                tvCounterpart.setText(counterpart.getNameWithRetail());
                tvAmount.setText(BaseUtils.toCurrencyOnlyStr(summary.amount));
                tvStatus.setText(record.getState().getUiStr());
                tvStatus.setTextColor(record.getState().color.colorInt);
                BaseUtils.setTextCancelled(record.getState().isCancelled(), tvDate, tvCounterpart, tvAmount, tvStatus);
            }
        }
    }

    class MallSettingDialog extends MyButtonsDialog {
        private CheckBox cbPhoneNumberAuth;
        private List<RadioButton> rbOpenModes;
        private ChatbotSettingData currentSetting;

        public MallSettingDialog(final Context context, final ChatbotSettingData settingData) {
            super(context, "카톡몰 설정", "카톡몰의 공개여부와 보안을 설정할 수 있습니다");
            this.currentSetting = settingData;

            List<Pair<String, ChatbotOpenModeType>> openMode = new ArrayList<>();
            openMode.add(Pair.create("카톡몰 비공개", ChatbotOpenModeType.CLOSED));
            if (userSettingData.getProtocol().isRetailOn()) {
                openMode.add(Pair.create("카톡몰 공개 (도매가로)", ChatbotOpenModeType.OPEN_WHOLESALE));
                openMode.add(Pair.create("카톡몰 공개 (소매가로)", ChatbotOpenModeType.OPEN_RETAIL));
                rbOpenModes = addRadioButtons(openMode, settingData.getOpenMode(), null);
            } else {
                openMode.add(Pair.create("카톡몰 공개", ChatbotOpenModeType.OPEN_WHOLESALE));
                if (settingData.getOpenMode() == ChatbotOpenModeType.OPEN_RETAIL) // 도소매 모드도 아닌데 소매가로 되어 있는 경우
                    rbOpenModes = addRadioButtons(openMode, ChatbotOpenModeType.OPEN_WHOLESALE, null);
                else
                    rbOpenModes = addRadioButtons(openMode, settingData.getOpenMode(), null);
            }
            addLine(0);
            cbPhoneNumberAuth = addCheckBox("전화번호 문자 인증", settingData.getAuthMode() == ChatbotAuthModeType.AUTH_REQUIRED, null);
            cbPhoneNumberAuth.setTextSize(18);
        }

        @Override
        public void onCloseClick() {
            ChatbotOpenModeType openMode = ChatbotOpenModeType.CLOSED;
            for (RadioButton radioButton : rbOpenModes) {
                if (radioButton.isChecked()) {
                    openMode = (ChatbotOpenModeType) radioButton.getTag();
                    break;
                }
            }
            final ChatbotAuthModeType authMode = cbPhoneNumberAuth.isChecked() ? ChatbotAuthModeType.AUTH_REQUIRED : ChatbotAuthModeType.AUTH_FREE;
            if (currentSetting.getOpenMode() == openMode && currentSetting.getAuthMode() == authMode) {
                return; // no change;
            }

            if (userSettingData.isMasterOrNoMaster() == false) {
                MyUI.toastSHORT(context, String.format("사장님만 변경할 수 있습니다"));
                return;
            }

            final ChatbotOpenModeType finalOpenMode = openMode;
            new MyServiceTask(context, "MallSettingDialog", "설정 저장중...") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    chatbotService.updateChatbotSetting(userSettingData.getUserShopInfo(), new ChatbotSettingData(authMode, finalOpenMode));
                }
            };
        }
    }

}
