package com.teampls.shoplus.silverlib.common;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAwsService;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MAccountDB;
import com.teampls.shoplus.lib.database_memory.MSlipDB;
import com.teampls.shoplus.lib.database_memory.MTransDB;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.datatypes.MyTriple;
import com.teampls.shoplus.lib.enums.AutoMessageRequestTimeConfigurationType;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.silverlib.database.MallOrderDB;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotOrderData;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderState;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2019-02-19.
 */

public class SilverAwsService extends BaseAwsService {

    public static void uploadSlipAndStocksFromOrder(final Context context, final ChatbotOrderData orderData, final int onlinePayment,
                                                    final int entrustPayment, final int billValue, final AutoMessageRequestTimeConfigurationType autoMessage,
                                           final MyOnAsync<SlipFullRecord> onPostSlip, final MyOnAsync onUpdateStock) {
        final UserRecord counterpart = UserDB.getInstance(context).getRecord(orderData.getBuyerPhoneNumber());

        new CommonServiceTask(context, "SilverAwsService", "거래 기록 생성중...") {
            private SlipFullRecord newFullRecord = new SlipFullRecord();

            @Override
            public void doInBackground() throws MyServiceFailureException {
                newFullRecord = new SlipFullRecord();
                List<SlipItemDataProtocol> items = new ArrayList<>();
                Set<Integer> itemIds = new HashSet<>();
                MallOrderDB orderDB = MallOrderDB.getInstance(context);

                for (SlipItemRecord record : orderDB.getItems(orderData.getOrderId())) {
                    items.add(record);
                    if (record.itemId > 0)
                        itemIds.add(record.itemId); // 0 이하는 재고에 반영이 안되므로 오류만 일으킨다
                }

                // upload to server : slip items, stocks, contact info
                // 특이점 : 항상 현재시간, orderId 있음, 잔금청구는 없음?
                MyTriple<AccountsData, SlipDataProtocol, String> triple = transactionService.postTransaction(
                        UserSettingData.getInstance(context).getUserShopInfo(), orderData.getBuyerPhoneNumber(),
                        DateTime.now(), items, onlinePayment, entrustPayment, billValue, autoMessage, orderData.getOrderId()); // 생성 시간 = 현재 시간

                orderData.setState(ChatbotOrderState.FINISHED);
                orderDB.update(orderData);

                newFullRecord = new SlipFullRecord(triple.second); //
                newFullRecord.record.confirmed = true;
                newFullRecord.serviceLink = triple.third;

                // mPOS의 거래기록, 챙길물건 정보를 위해
                PosSlipDB.getInstance(context).insert(newFullRecord, new ArrayList<String>(), new ArrayList<String>());
                MTransDB.getInstance(context).insert(newFullRecord);
                MSlipDB.getInstance(context).insertPendings(newFullRecord);

                adjustStockAsync(context, callLocation, false, triple.second, BaseUtils.getMin(itemIds), true, onUpdateStock);
                MAccountDB.getInstance(context).updateOrInsert(new AccountRecord(newFullRecord.record.counterpartPhoneNumber,
                        triple.first, counterpart.name));
            }

            @Override
            public void onPostExecutionUI() {
                if (onPostSlip != null)
                    onPostSlip.onSuccess(newFullRecord);
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                if (onPostSlip != null)
                    onPostSlip.onError(e);
                MyUI.toastSHORT(context, String.format("거래기록 생성 중 오류가 발생했습니다. 다시 시도를 부탁드립니다"));
            }
        };
    }
}
