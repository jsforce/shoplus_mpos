package com.teampls.shoplus.silverlib.database;

import android.content.Context;

import com.teampls.shoplus.lib.adapter.BaseSlipItemDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database_memory.MSlipDB;

/**
 * Created by Medivh on 2017-04-11.
 */

public class PosSlipItemDBAdapter extends BaseSlipItemDBAdapter {
    public static final int SlipCreationView = 0, SlipSharingView = 1, QrCreationViewNew =2, QrCreationViewPendings = 3;

    public PosSlipItemDBAdapter(Context context) {
        super(context, PosSlipDB.getInstance(context));
    }

    public PosSlipItemDBAdapter(Context context, MSlipDB mSlipDB) {
        super(context, mSlipDB);
    }

    @Override
    public BaseViewHolder getViewHolder() {
        switch (viewType) {
            default:
            case SlipCreationView:
                return new SlipCreationViewHolder();
            case SlipSharingView:
                return new SlipSharingViewHolder();
            case QrCreationViewNew:
                return new QrPosSlipCreationNewViewHolder();
            case QrCreationViewPendings:
                return new QrPosSlipCreationPendingsViewHolder();
        }
    }



}