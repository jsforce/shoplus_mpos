package com.teampls.shoplus.silverlib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.view_base.BasePosSlipSharingView;
import com.teampls.shoplus.silverlib.R;
import com.teampls.shoplus.silverlib.common.SilverAppWatcher;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotOrderData;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderDeliveryType;
import com.teampls.shoplus.silverlib.view_silver.SilverSlipSharingView;

/**
 * Created by Medivh on 2019-02-17.
 */

public class MallSlipSharingView extends SilverSlipSharingView {
    private static ChatbotOrderData orderData = new ChatbotOrderData();

    public static void startActivity(Context context, SlipFullRecord slipFullRecord, ChatbotOrderData orderData) {
        MallSlipSharingView.slipFullRecord = slipFullRecord;
        BasePosSlipSharingView.itemOptionRecord = new ItemOptionRecord();
        MallSlipSharingView.orderData = orderData;
        actionType = DbActionType.READ;
        ((Activity) context).startActivityForResult(new Intent(context, MallSlipSharingView.class), BaseGlobal.RequestRefresh);
    }

    public static void startActivityNewRecord(Context context, SlipFullRecord slipFullRecord, ChatbotOrderData orderData) { // 생성하면서 호출시 (잔금 자동 합산때문에 구별함)
        MallSlipSharingView.slipFullRecord = slipFullRecord;
        MallSlipSharingView.orderData = orderData;
        actionType = DbActionType.INSERT;
        ((Activity) context).startActivityForResult(new Intent(context, MallSlipSharingView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SilverAppWatcher.checkInstanceAndRestart(this) == false) return;
        findTextViewById(R.id.slip_sharing_createNewSlip, "주문원본");

        // 배송 정보, 메모
       String counterAddressStr = String.format("배송 : %s", orderData.getDeliveryType().getUiStr());
        if (orderData.getDeliveryType() == ChatbotOrderDeliveryType.POST)
            counterAddressStr = String.format("%s (%s)", counterAddressStr, orderData.getAddress());
        if (orderData.getMemo().isEmpty() == false)
            counterAddressStr = counterAddressStr + "\n" + orderData.getMemo();
        tvCounterAddress.setText(counterAddressStr);
        tvCounterAddress.setVisibility(counterAddressStr.isEmpty()? View.GONE : View.VISIBLE);
    }

    public void onCreateNewSlipClick() {
        MallSlipCreationView.startActivityOriginal(context, orderData);
    }

    @Override
    public void startPosSlipCreationView(SlipRecord slipRecord) {
        // NA
    }

    @Override
    public void startPosSlipCreationView(UserRecord counterpart) {
        // NA
    }

    @Override
    public void refreshAfterCancelled() {
        // NA
    }
}
