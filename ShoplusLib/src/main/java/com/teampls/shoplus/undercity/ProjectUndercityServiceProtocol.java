package com.teampls.shoplus.undercity;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;
import com.teampls.shoplus.undercity.datatypes.ItemStockSnapshotBundleData;
import com.teampls.shoplus.undercity.datatypes.PurchaseOrderRemainsDataBundle;
import com.teampls.shoplus.undercity.datatypes.SupplierData;
import com.teampls.shoplus.undercity.enums.ItemStockUpdateType;
import com.teampls.shoplus.undercity.enums.WarehousingDepartureType;
import com.teampls.shoplus.undercity.protocol.ItemStockUpdateDataProtocol;
import com.teampls.shoplus.undercity.protocol.LogisticsWarehousingDataProtocol;
import com.teampls.shoplus.undercity.protocol.LogisticsWarehousingSummaryDataProtocol;

import org.joda.time.DateTime;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 창고입고요청 관련 서비스
 * @author lucidite
 */

public interface ProjectUndercityServiceProtocol {
    /**
     * 최근 창고입고요청 목록을 조회한다.
     *
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    List<LogisticsWarehousingSummaryDataProtocol> getRecentWarehousingList(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 특정 창고입고요청 상세정보를 조회한다.
     *
     * @param userShop
     * @param warehousingId
     * @return
     * @throws MyServiceFailureException
     */
    LogisticsWarehousingDataProtocol getSpecificWarehousing(UserShopInfoProtocol userShop, String warehousingId) throws MyServiceFailureException;

    /**
     * 새 창고입고요청 정보를 생성한다.
     *
     * @param userShop
     * @param departureType
     * @param deliveryDate 배송예정일 (optional, 날짜만 사용)
     * @param departureCounts 배송예정수량 (mandatory)
     * @param comment
     * @return 생성된 정보 반환
     * @throws MyServiceFailureException
     */
    LogisticsWarehousingDataProtocol postNewWarehousing(UserShopInfoProtocol userShop, WarehousingDepartureType departureType, DateTime deliveryDate, Map<ItemStockKey, Integer> departureCounts, String comment) throws MyServiceFailureException;

    /**
     * 특정 창고입고요청 배송예정일자를 변경한다.
     *
     * @param userShop
     * @param warehousingId
     * @param deliveryDate 날짜 정보만 사용한다. null을 입력하면 기존 정보를 삭제한다.
     * @return 변경 후 정보 반환
     * @throws MyServiceFailureException
     */
    LogisticsWarehousingDataProtocol updateDeliveryDate(UserShopInfoProtocol userShop, String warehousingId, DateTime deliveryDate) throws MyServiceFailureException;

    /**
     * 특정 창고입고요청의 메모를 변경한다.
     *
     * @param userShop
     * @param warehousingId
     * @param comment
     * @return 변경 후 정보 반환
     * @throws MyServiceFailureException
     */
    LogisticsWarehousingDataProtocol updateComment(UserShopInfoProtocol userShop, String warehousingId, String comment) throws MyServiceFailureException;

    //////////////////////////////////////////////////////////////////////
    // Purchase Order (Item Stock Update)

    /**
     * 새로운 공급자를 추가한다.
     * [NOTE] 공급자는 Main Shop 수준에서 관리해야 한다.
     *
     * @param userShop
     * @param supplier
     * @return
     * @throws MyServiceFailureException
     */
    void postSupplier(UserShopInfoProtocol userShop, SupplierData supplier) throws MyServiceFailureException;

    /**
     * 내 공급자 목록을 조회한다.
     * [NOTE] 공급자는 Main Shop 수준에서 관리해야 한다.
     *
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    List<SupplierData> getSuppliers(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 월별 Item Update 중 발주 관련 목록을 조회한다. (발주, 도착)
     *
     * @param userShop
     * @param month
     * @return
     * @throws MyServiceFailureException
     */
    List<ItemStockUpdateDataProtocol> getMonthlyPurchaseOrderUpdates(UserShopInfoProtocol userShop, DateTime month) throws MyServiceFailureException;

    /**
     * 월별 Item Update 중 매장 재고수량 수정 목록을 조회한다. (판매결산, 일괄수정)
     *
     * @param userShop
     * @param month
     * @return
     * @throws MyServiceFailureException
     */
    List<ItemStockUpdateDataProtocol> getMonthlyItemStockBatchUpdates(UserShopInfoProtocol userShop, DateTime month) throws MyServiceFailureException;

    /**
     * 새로운 Item Update 건을 생성한다. (발주, 도착, 판매결산, 일괄수정)
     *
     * @param userShop
     * @param type
     * @param supplier
     * @param counts
     * @param comment
     * @return
     * @throws MyServiceFailureException
     */
    ItemStockUpdateDataProtocol postItemStockUpdate(UserShopInfoProtocol userShop, ItemStockUpdateType type, String supplier, Map<ItemStockKey, Integer> counts, String comment) throws MyServiceFailureException;

    /**
     * 상품 옵션별 잔여 주문 수량을 조회한다.
     *
     * @param userShop
     * @return 잔여 주문량 및 유효재고(0 이상)
     */
    PurchaseOrderRemainsDataBundle getOrderRemains(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 특정 색상/사이즈 목록의 잔여 주문 수량을 0으로 초기화한다.
     * [NOTE] updateOrderRemains의 특별 케이스 (잔여 주문을 날리기 위한 용도)
     *
     * @param userShop
     * @param options
     * @throws MyServiceFailureException
     */
    void clearOrderRemains(UserShopInfoProtocol userShop, Collection<ItemStockKey> options) throws MyServiceFailureException;

    /**
     * [IRON-193] 특정 색상/사이즈 목록의 잔여 주문 수량을 수정한다.
     * [NOTE] 현재 기준으로 변경 결과값을 따로 받아오지는 않는다. (로컬에서 필요한 경우 선택적으로 재동기화할 것)
     *
     * @param userShop
     * @param newRemains    색상/사이즈와 잔여 주문 수량 mapping 데이터
     * @throws MyServiceFailureException
     */
    void updateOrderRemains(UserShopInfoProtocol userShop, Map<ItemStockKey, Integer> newRemains) throws MyServiceFailureException;


    //////////////////////////////////////////////////////////////////////
    // Item Stock Snapshot

    /**
     * [SILVER-537] 두 날짜의 재고 스냅샷 및 그 사이의 입고내역을 조회한다.
     * [NOTE] 각 날짜의 재고 스냅샷은 새벽 6시를 기준으로 한다. 입고내역은 두 스냅샷 생성 시간 사이의 내역을 반환한다.
     *
     * @param userShop
     * @param startingDay
     * @param endingDay
     * @return
     * @throws MyServiceFailureException
     */
    ItemStockSnapshotBundleData getSnapshotData(UserShopInfoProtocol userShop, DateTime startingDay, DateTime endingDay) throws MyServiceFailureException;
}
