package com.teampls.shoplus.undercity.protocol;

import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.undercity.enums.ItemStockUpdateType;

import org.joda.time.DateTime;

import java.util.Map;

/**
 * 상품 재고/주문 관련 수량 업데이트 데이터 프로토콜
 *
 * @author lucidite
 */
public interface ItemStockUpdateDataProtocol {
    String getUpdateId();
    DateTime getCreatedDateTime();
    ItemStockUpdateType getUpdateType();
    Map<ItemStockKey, Integer> getUpdateCounts();
    String getSupplierId();
    String getComment();
}
