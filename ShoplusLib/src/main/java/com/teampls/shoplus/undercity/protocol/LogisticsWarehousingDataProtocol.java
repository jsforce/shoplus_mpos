package com.teampls.shoplus.undercity.protocol;

import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.undercity.enums.WarehousingDepartureType;

import java.util.Map;

/**
 * 창고입고 상세정보 데이터 프로토콜
 *
 * @author lucidite
 */
public interface LogisticsWarehousingDataProtocol extends LogisticsWarehousingSummaryDataProtocol {
    /**
     * 창고입고 요약 데이터를 반환한다.
     * @return
     */
    LogisticsWarehousingSummaryDataProtocol getSummary();

    /**
     * 창고입고 예정/요청 수량을 반환한다.
     * @return
     */
    Map<ItemStockKey, Integer> getDepartureCounts();

    /**
     * 물류이동 도착 수량을 반환한다(도착 설정 시).
     * @return
     */
    Map<ItemStockKey, Integer> getReceiptCounts();

    String getComment();
}
