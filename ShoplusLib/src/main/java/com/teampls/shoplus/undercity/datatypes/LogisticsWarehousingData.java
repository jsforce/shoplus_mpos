package com.teampls.shoplus.undercity.datatypes;

import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.undercity.enums.WarehousingDepartureType;
import com.teampls.shoplus.undercity.protocol.LogisticsWarehousingDataProtocol;
import com.teampls.shoplus.undercity.protocol.LogisticsWarehousingSummaryDataProtocol;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author lucidite
 */
public class LogisticsWarehousingData implements LogisticsWarehousingDataProtocol {
    private LogisticsWarehousingSummaryDataProtocol summary;
    private Map<ItemStockKey, Integer> departureCounts;
    private Map<ItemStockKey, Integer> receiptCounts;
    private String comment;

    public LogisticsWarehousingData(JSONObject json) throws JSONException {
        this.summary = new LogisticsWarehousingSummaryData(json);
        this.departureCounts = LogisticsWarehousingData.parseCounts(json.optJSONObject("dep_counts"));
        this.receiptCounts = LogisticsWarehousingData.parseCounts(json.optJSONObject("receipt_counts"));
        this.comment = json.optString("comment", "");
    }

    private static Map<ItemStockKey, Integer> parseCounts(JSONObject countsObj) throws JSONException {
        Map<ItemStockKey, Integer> result = new HashMap<>();
        if (countsObj == null) {
            return result;
        }

        Iterator<String> iter = countsObj.keys();
        while (iter.hasNext()) {
            String itemOptionApiStr = iter.next();
            ItemStockKey itemOption = new ItemStockKey(itemOptionApiStr);
            int count = countsObj.getInt(itemOptionApiStr);
            result.put(itemOption, count);
        }
        return result;
    }

    @Override
    public String getUserId() {
        return this.summary.getUserId();
    }

    @Override
    public WarehousingDepartureType getType() {
        return this.summary.getType();
    }

    @Override
    public String getWarehousingId() {
        return this.summary.getWarehousingId();
    }

    @Override
    public DateTime getCreatedDateTime() {
        return this.summary.getCreatedDateTime();
    }

    @Override
    public DateTime getDeliveryDateTime() {
        return this.summary.getDeliveryDateTime();
    }

    @Override
    public Boolean isReceived() {
        return this.summary.isReceived();
    }

    @Override
    public DateTime getReceiptDateTime() {
        return this.summary.getReceiptDateTime();
    }

    @Override
    public LogisticsWarehousingSummaryDataProtocol getSummary() {
        return this.summary;
    }

    @Override
    public Map<ItemStockKey, Integer> getDepartureCounts() {
        return this.departureCounts;
    }

    @Override
    public Map<ItemStockKey, Integer> getReceiptCounts() {
        return this.receiptCounts;
    }

    @Override
    public String getComment() {
        return this.comment;
    }
}
