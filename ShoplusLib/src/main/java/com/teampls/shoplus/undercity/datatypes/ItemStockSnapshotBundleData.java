package com.teampls.shoplus.undercity.datatypes;

import android.text.TextUtils;
import android.util.Log;

import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.undercity.protocol.ItemStockUpdateDataProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 두 날짜의 재고 스냅샷 및 그 사이의 입고내역 데이터
 *
 * @author lucidite
 */
public class ItemStockSnapshotBundleData {
    private ItemStockSnapshotData startingSnapshot;
    private ItemStockSnapshotData endingSnapshot;
    private List<ItemStockUpdateDataProtocol> warehousings;

    public ItemStockSnapshotBundleData(JSONObject dataObj) throws JSONException {
        JSONArray snapshotArr = dataObj.getJSONArray("snapshots");
        JSONObject startSnapshotObj = snapshotArr.getJSONObject(0);
        JSONObject endSnapshotObj = snapshotArr.getJSONObject(1);
        this.startingSnapshot = new ItemStockSnapshotData(startSnapshotObj);
        this.endingSnapshot = new ItemStockSnapshotData(endSnapshotObj);

        this.warehousings = new ArrayList<>();
        JSONArray warehousingArr = dataObj.optJSONArray("warehousings");
        for (int i=0; i < warehousingArr.length(); ++i) {
            this.warehousings.add(new ItemStockUpdateData(warehousingArr.getJSONObject(i)));
        }
    }

    public ItemStockSnapshotData getStartingSnapshot() {
        return startingSnapshot;
    }

    public ItemStockSnapshotData getEndingSnapshot() {
        return endingSnapshot;
    }

    public List<ItemStockUpdateDataProtocol> getWarehousings() {
        return warehousings;
    }

    public void toLogCat(String location) {
        List<String> startingStrings = new ArrayList<>();
        for (ItemStockKey stockKey : startingSnapshot.keySet())
            startingStrings.add(String.format("%s (%d)", stockKey.toString(" / "), startingSnapshot.get(stockKey)));
        Log.i("DEBUG_JS", String.format("[%s] starting %s", location, TextUtils.join(", ", startingStrings)));

        List<String> endingStrings = new ArrayList<>();
        for (ItemStockKey stockKey : endingSnapshot.keySet())
            endingStrings.add(String.format("%s (%d)", stockKey.toString(" / "), endingSnapshot.get(stockKey)));
        Log.i("DEBUG_JS", String.format("[%s] ending %s", location, TextUtils.join(", ", endingStrings)));
    }
}
