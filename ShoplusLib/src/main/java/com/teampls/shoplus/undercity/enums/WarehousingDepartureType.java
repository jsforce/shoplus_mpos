package com.teampls.shoplus.undercity.enums;

import android.support.annotation.NonNull;

/**
 * @author lucidite
 */
public enum WarehousingDepartureType {
    SHOP("W", "창고보냄", "매장→창고"),
    FACTORY("D", "창고직배송", "공장→창고");

    @NonNull
    private String remoteStr;
    private String nameStr;
    private String uiStr;

    WarehousingDepartureType(String remoteStr, String nameStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.nameStr = nameStr;
        this.uiStr = uiStr;
    }

    public static WarehousingDepartureType fromRemoteStr(String remoteStr) {
        for (WarehousingDepartureType type: WarehousingDepartureType.values()) {
            if (type.remoteStr.equals(remoteStr)) {
                return type;
            }
        }
        return SHOP;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }

    public String getName() {
        return this.nameStr;
    }

    public String toUiStr() {
        return this.uiStr;
    }
}
