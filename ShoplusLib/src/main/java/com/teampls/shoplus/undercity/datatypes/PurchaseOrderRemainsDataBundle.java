package com.teampls.shoplus.undercity.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayDataParser;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * 발주 잔여 수량 및 유효재고(0 이상) 정보의 번들 데이터 클래스
 *
 * @author lucidite
 */
public class PurchaseOrderRemainsDataBundle {
    private Map<ItemStockKey, Integer> orderRemainsCounts;
    private Map<ItemStockKey, Integer> effectiveStockCounts;

    public PurchaseOrderRemainsDataBundle(JSONObject json) throws JSONException {
        this.orderRemainsCounts = APIGatewayDataParser.parseItemStock(json.getJSONObject("purchase_orders"));
        this.effectiveStockCounts = APIGatewayDataParser.parseItemStock(json.getJSONObject("item_stock"));
    }

    public Map<ItemStockKey, Integer> getOrderRemainsCounts() {
        return orderRemainsCounts;
    }

    public Map<ItemStockKey, Integer> getEffectiveStockCounts() {
        return effectiveStockCounts;
    }
}
