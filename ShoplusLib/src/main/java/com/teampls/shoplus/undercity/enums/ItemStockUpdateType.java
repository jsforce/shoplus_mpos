package com.teampls.shoplus.undercity.enums;

import com.teampls.shoplus.lib.enums.ColorType;

import static com.teampls.shoplus.lib.enums.ColorType.FlatBlueDark;
import static com.teampls.shoplus.lib.enums.ColorType.FlatForestGreen;
import static com.teampls.shoplus.lib.enums.ColorType.FlatPowerBlueDark;
import static com.teampls.shoplus.lib.enums.ColorType.FlatRedDark;

/**
 * @author lucidite
 */
// 참고 WarehousingDepartureType
public enum ItemStockUpdateType {
    ORDER("order", "발주", 0, FlatRedDark),
    ARRIVAL("arrival", "입고", 1,  FlatPowerBlueDark),
    SALES("sales", "판매입력", -1, FlatForestGreen),
    BATCH("batch", "수동수정", 1, FlatBlueDark);

    private String remoteStr;
    private String uiStr;
    public int sign = 0;
    public ColorType backgroundColor = ColorType.Trans;

    ItemStockUpdateType(String remoteStr, String uiStr, int sign, ColorType backgroundColor) {
        this.remoteStr = remoteStr;
        this.backgroundColor = backgroundColor;
        this.uiStr = uiStr;
        this.sign = sign;
    }

    public String getRemoteStr() {
        return remoteStr;
    }

    public String getUiStr() {
        return uiStr;
    }

    public static ItemStockUpdateType fromRemoteStr(String remoteStr) {
        if (remoteStr == null) {
            return ORDER;           // fallback
        }
        for (ItemStockUpdateType type: ItemStockUpdateType.values()) {
            if (type.remoteStr.equals(remoteStr)) {
                return type;
            }
        }
        return ORDER;               // fallback
    }
}
