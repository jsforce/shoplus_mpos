package com.teampls.shoplus.undercity.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 * 재고 스냅샷 데이터에 대한 wrapper class
 *
 * @author lucidite
 */
public class ItemStockSnapshotData extends HashMap<ItemStockKey, Integer> {
    private DateTime createdDateTime;

    /**
     * 서버로부터 수신한 스냅샷 JSON 데이터로부터 옵션-재고수량 매핑 데이터를 얻는다.
     *
     * @param snapshotObj
     * @throws JSONException
     */
    public ItemStockSnapshotData(JSONObject snapshotObj) throws JSONException {
        super();
        this.createdDateTime = APIGatewayHelper.getDateTimeFromRemoteString(snapshotObj.getString("created"));

        JSONObject snapshotStockObj = snapshotObj.getJSONObject("stock");
        Iterator<String> itemIter = snapshotStockObj.keys();
        while (itemIter.hasNext()) {
            String itemIdStr = itemIter.next();
            int itemid = Integer.valueOf(itemIdStr);
            JSONObject itemStockDataOfAnItem = snapshotStockObj.getJSONObject(itemIdStr);
            Iterator<String> itemOptionIter = itemStockDataOfAnItem.keys();
            while (itemOptionIter.hasNext()) {
                String itemOptionParamStr = itemOptionIter.next();
                ItemStockKey itemOption = ItemStockKey.build(itemid, itemOptionParamStr);
                if (itemOption == null) {
                    // [DEBUG] Item Option 문자열 관련 문제
                    continue;
                }
                int stockValue = itemStockDataOfAnItem.getInt(itemOptionParamStr);
                this.put(itemOption, stockValue);
            }
        }
    }

    public DateTime getCreatedDateTime() {
        return createdDateTime;
    }
}
