package com.teampls.shoplus.undercity.awsservice;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayDataParser;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;
import com.teampls.shoplus.undercity.ProjectUndercityServiceProtocol;
import com.teampls.shoplus.undercity.datatypes.ItemStockSnapshotBundleData;
import com.teampls.shoplus.undercity.datatypes.ItemStockUpdateData;
import com.teampls.shoplus.undercity.datatypes.LogisticsWarehousingData;
import com.teampls.shoplus.undercity.datatypes.LogisticsWarehousingSummaryData;
import com.teampls.shoplus.undercity.datatypes.PurchaseOrderRemainsDataBundle;
import com.teampls.shoplus.undercity.datatypes.SupplierData;
import com.teampls.shoplus.undercity.enums.ItemStockUpdateType;
import com.teampls.shoplus.undercity.enums.WarehousingDepartureType;
import com.teampls.shoplus.undercity.protocol.ItemStockUpdateDataProtocol;
import com.teampls.shoplus.undercity.protocol.LogisticsWarehousingDataProtocol;
import com.teampls.shoplus.undercity.protocol.LogisticsWarehousingSummaryDataProtocol;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ProjectUndercityServiceProtocol 대한 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectUndercityService implements ProjectUndercityServiceProtocol {
    private String getPhoneNumberForUndercity(UserShopInfoProtocol userShop) {
        return userShop.getMyShopOrUserPhoneNumber();                           // Main Shop을 사용하지 않음 (필요시 재검토)
    }

    private String getMyWarehousingsResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "logistics", "warehousings");
    }

    private String getMyRecentWarehousingListResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "logistics", "warehousings", "recent");
    }

    private String getAnWarehousingResourcePath(String phoneNumber, String warehousingId) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "logistics", "warehousings", warehousingId);
    }

    private String getAFieldOfASpecificWarehousingResourcePath(String phoneNumber, String warehousingId, String fieldname) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "logistics", "warehousings", warehousingId, fieldname);
    }

    @Override
    public List<LogisticsWarehousingSummaryDataProtocol> getRecentWarehousingList(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String response = APIGatewayClient.requestGET(
                    this.getMyRecentWarehousingListResourcePath(getPhoneNumberForUndercity(userShop)), null, UserHelper.getIdentityToken());
            JSONArray orderSummaryArr = new JSONArray(response);
            return LogisticsWarehousingSummaryData.fromJSONArray(orderSummaryArr);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public LogisticsWarehousingDataProtocol getSpecificWarehousing(UserShopInfoProtocol userShop, String warehousingId) throws MyServiceFailureException {
        try {
            String response = APIGatewayClient.requestGET(
                    this.getAnWarehousingResourcePath(getPhoneNumberForUndercity(userShop), warehousingId), null, UserHelper.getIdentityToken());
            return new LogisticsWarehousingData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public LogisticsWarehousingDataProtocol postNewWarehousing(UserShopInfoProtocol userShop, WarehousingDepartureType departureType, DateTime deliveryDate, Map<ItemStockKey, Integer> departureCounts, String comment) throws MyServiceFailureException {
        try {

            if (departureCounts == null || departureCounts.isEmpty()) {
                throw new MyServiceFailureException("EmptyCounts", "Order counts is empty");
            }
            JSONObject countObj = new JSONObject();
            for (Map.Entry<ItemStockKey, Integer> countSet: departureCounts.entrySet()) {
                countObj.put(countSet.getKey().toAPIParamStr(), countSet.getValue().intValue());
            }

            JSONObject dataObj = new JSONObject()
                    .put("dep_type", departureType.toRemoteStr())
                    .put("dep_counts", countObj);
            if (deliveryDate != null) {
                dataObj.put("delivery", APIGatewayHelper.getRDSDateOnlyString(deliveryDate));
            }
            if (comment != null && !comment.isEmpty()) {
                dataObj.put("comment", comment);
            }
            JSONObject bodyObj = new JSONObject().put("data", dataObj);

            String response = APIGatewayClient.requestPOST(
                    this.getMyWarehousingsResourcePath(getPhoneNumberForUndercity(userShop)),
                    bodyObj.toString(), UserHelper.getIdentityToken());


            return new LogisticsWarehousingData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public LogisticsWarehousingDataProtocol updateDeliveryDate(UserShopInfoProtocol userShop, String warehousingId, DateTime deliveryDate) throws MyServiceFailureException {
        try {
            JSONObject bodyObj;
            if (deliveryDate == null) {
                bodyObj = new JSONObject().put("data", false);
            } else {
                bodyObj = new JSONObject().put("data", new JSONObject().put("delivery", APIGatewayHelper.getRDSDateOnlyString(deliveryDate)));
            }

            String response = APIGatewayClient.requestPUT(
                    this.getAFieldOfASpecificWarehousingResourcePath(getPhoneNumberForUndercity(userShop), warehousingId, "delivery"),
                    bodyObj.toString(), UserHelper.getIdentityToken());
            return new LogisticsWarehousingData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public LogisticsWarehousingDataProtocol updateComment(UserShopInfoProtocol userShop, String warehousingId, String comment) throws MyServiceFailureException {
        try {
            JSONObject bodyObj;
            if (comment == null || comment.isEmpty()) {
                bodyObj = new JSONObject().put("data", false);
            } else {
                bodyObj = new JSONObject().put("data", new JSONObject().put("comment", comment));
            }

            Log.i("DEBUG_JS", String.format("[ProjectUndercityService.updateComment] %s", bodyObj.toString()));

            String response = APIGatewayClient.requestPUT(
                    this.getAFieldOfASpecificWarehousingResourcePath(getPhoneNumberForUndercity(userShop), warehousingId, "comment"),
                    bodyObj.toString(), UserHelper.getIdentityToken());
            return new LogisticsWarehousingData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    //////////////////////////////////////////////////////////////////////
    // Purchase Order

    /**
     * [NOTE] 일반 거래처(buyer)와 경로를 공유한다 (서버에서 구별하여 처리)
     * @param phoneNumber
     * @return
     */
    private String getMyContactResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "contact");
    }

    private String getMyItemStockUpdateResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock", "updates");
    }

    private String getMonthlyPurchaseOrderUpdateResourcePath(String phoneNumber, DateTime month) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock", "updates", "O" + APIGatewayHelper.getRemoteMonthString(month));
    }

    private String getMonthlyItemStockBatchUpdateResourcePath(String phoneNumber, DateTime month) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock", "updates", "S" + APIGatewayHelper.getRemoteMonthString(month));
    }

    private String getMyItemStockResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock");
    }

    private String getMyPurchaseOrderRemainsResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock", "order-remains");
    }

    /**
     * [NOTE] Purchase Order는 메인 샵 수준으로 관리
     * @param userShop
     * @return
     */
    private String getPhoneNumberForUndercityPurchaseOrder(UserShopInfoProtocol userShop) {
        return userShop.getMainShopOrMyShopOrUserPhoneNumber();
    }

    @Override
    public void postSupplier(UserShopInfoProtocol userShop, SupplierData supplier) throws MyServiceFailureException {
        try {
            JSONObject requestBodyObj = new JSONObject().put("suppliers", supplier.getObject());
            String resource = this.getMyContactResourcePath(getPhoneNumberForUndercityPurchaseOrder(userShop));
            APIGatewayClient.requestPOST(resource, requestBodyObj.toString(), UserHelper.getIdentityToken());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<SupplierData> getSuppliers(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            Map<String, String> queryParams = new HashMap<>();
            queryParams.put("type", "supplier");
            String resource = this.getMyContactResourcePath(getPhoneNumberForUndercityPurchaseOrder(userShop));
            String response = APIGatewayClient.requestGET(resource, queryParams, UserHelper.getIdentityToken());
            JSONArray supplierArr = new JSONArray(response);
            List<SupplierData> result = new ArrayList<>();
            for (int i = 0; i < supplierArr.length(); ++i) {
                result.add(new SupplierData(supplierArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<ItemStockUpdateDataProtocol> getMonthlyPurchaseOrderUpdates(UserShopInfoProtocol userShop, DateTime month) throws MyServiceFailureException {
        String resource = this.getMonthlyPurchaseOrderUpdateResourcePath(getPhoneNumberForUndercityPurchaseOrder(userShop), month);
        return this.getItemStockUpdates(userShop, resource);
    }

    @Override
    public List<ItemStockUpdateDataProtocol> getMonthlyItemStockBatchUpdates(UserShopInfoProtocol userShop, DateTime month) throws MyServiceFailureException {
        String resource = this.getMonthlyItemStockBatchUpdateResourcePath(getPhoneNumberForUndercityPurchaseOrder(userShop), month);
        return this.getItemStockUpdates(userShop, resource);
    }

    private List<ItemStockUpdateDataProtocol> getItemStockUpdates(UserShopInfoProtocol userShop, String resource) throws MyServiceFailureException {
        try {
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            List<ItemStockUpdateDataProtocol> result = new ArrayList<>();
            JSONArray itemUpdateArr = new JSONArray(response);
            for (int i = 0; i < itemUpdateArr.length(); ++i) {
                result.add(new ItemStockUpdateData(itemUpdateArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public ItemStockUpdateDataProtocol postItemStockUpdate(UserShopInfoProtocol userShop, ItemStockUpdateType type, String supplier, Map<ItemStockKey, Integer> counts, String comment) throws MyServiceFailureException {
        try {
            String resource = this.getMyItemStockUpdateResourcePath(getPhoneNumberForUndercityPurchaseOrder(userShop));
            JSONObject requestDataObj = new JSONObject().put("update_type", type.getRemoteStr());
            if (supplier != null && !supplier.isEmpty()) {
                requestDataObj.put("supplier", supplier);
            }
            if (comment != null && !comment.isEmpty()) {
                requestDataObj.put("comment", comment);
            }
            JSONObject countObj = APIGatewayDataParser.encodeItemStock(counts);
            if (countObj.length() > 0) {
                requestDataObj.put("counts", countObj);
            }
            JSONObject requestBodyObj = new JSONObject().put("data", requestDataObj);
            String response = APIGatewayClient.requestPOST(resource, requestBodyObj.toString(), UserHelper.getIdentityToken());
            return new ItemStockUpdateData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public PurchaseOrderRemainsDataBundle getOrderRemains(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            Map<String, String> queryParams = new HashMap<>();
            queryParams.put("type", "all");
            String resource = this.getMyItemStockResourcePath(getPhoneNumberForUndercityPurchaseOrder(userShop));

            String response = APIGatewayClient.requestGET(resource, queryParams, UserHelper.getIdentityToken());
            return new PurchaseOrderRemainsDataBundle(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public void clearOrderRemains(UserShopInfoProtocol userShop, Collection<ItemStockKey> options) throws MyServiceFailureException {
        Map<ItemStockKey, Integer> countData = new HashMap<>();
        for (ItemStockKey itemOption: options) {
            countData.put(itemOption, 0);           // 잔여수량을 0으로 설정
        }
        this.updateOrderRemains(userShop, countData);
    }

    @Override
    public void updateOrderRemains(UserShopInfoProtocol userShop, Map<ItemStockKey, Integer> newRemains) throws MyServiceFailureException {
        try {
            JSONObject countObj = APIGatewayDataParser.encodeItemStock(newRemains);
            JSONObject requestDataObj = new JSONObject().put("remains", countObj);

            String resource = this.getMyPurchaseOrderRemainsResourcePath(getPhoneNumberForUndercityPurchaseOrder(userShop));
            APIGatewayClient.requestPOST(resource, requestDataObj.toString(), UserHelper.getIdentityToken());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    //////////////////////////////////////////////////////////////////////
    // Item Stock Snapshot

    private String getSnapshotDataOfTwoSpecificDays(String phoneNumber, DateTime staringDay, DateTime endingDay) {
        return MyAWSConfigs.getResourcePath(
                phoneNumber, "items", "stock", "snapshots",
                APIGatewayHelper.getRemoteDateString(staringDay) + "-" + APIGatewayHelper.getRemoteDateString(endingDay));
    }

    /**
     * [NOTE] Item Stock 데이터는 메인 샵 수준으로 관리
     * @param userShop
     * @return
     */
    private String getPhoneNumberForItemStock(UserShopInfoProtocol userShop) {
        return userShop.getMainShopOrMyShopOrUserPhoneNumber();
    }

    @Override
    public ItemStockSnapshotBundleData getSnapshotData(UserShopInfoProtocol userShop, DateTime startingDay, DateTime endingDay) throws MyServiceFailureException {
        try {
            String resource = this.getSnapshotDataOfTwoSpecificDays(this.getPhoneNumberForItemStock(userShop), startingDay, endingDay);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            return new ItemStockSnapshotBundleData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }
}
