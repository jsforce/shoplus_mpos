package com.teampls.shoplus.undercity.datatypes;

import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayDataParser;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.undercity.enums.ItemStockUpdateType;
import com.teampls.shoplus.undercity.protocol.ItemStockUpdateDataProtocol;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * @author lucidite
 */
public class ItemStockUpdateData implements ItemStockUpdateDataProtocol, Comparable<ItemStockUpdateData> {
    private String updateId;
    private ItemStockUpdateType updateType;
    private Map<ItemStockKey, Integer> updateCounts;
    private String supplierId;
    private String comment;
    public MSortingKey sortingKey = MSortingKey.Date;

    public ItemStockUpdateData() {};

    public ItemStockUpdateData(JSONObject json) throws JSONException {
        this.updateId = json.getString("created").substring(1);     // eliminate prefix
        String updateTypeStr = json.getString("update_type");
        this.updateType = ItemStockUpdateType.fromRemoteStr(updateTypeStr);
        this.updateCounts = APIGatewayDataParser.parseItemStock(json.optJSONObject("counts"));
        this.supplierId = json.optString("supplier", "");
        this.comment = json.optString("comment", "");
    }

    public void toLogcat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] %s, %s, %d,  %s, %s",
                location, updateId, updateType, updateCounts.size(), supplierId, comment));
    }

    @Override
    public String getUpdateId() {
        return this.updateId;
    }

    @Override
    public DateTime getCreatedDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.updateId);
    }

    @Override
    public ItemStockUpdateType getUpdateType() {
        return this.updateType;
    }

    @Override
    public Map<ItemStockKey, Integer> getUpdateCounts() {
        return this.updateCounts;
    }

    @Override
    public String getSupplierId() {
        return this.supplierId;
    }

    @Override
    public String getComment() {
        return this.comment;
    }

    @Override
    public int compareTo(@NonNull ItemStockUpdateData o) {
        switch (sortingKey) {
            case Date:
            default:
                return -getCreatedDateTime().compareTo(o.getCreatedDateTime());
        }
    }
}

//    public ItemStockUpdateData(ItemStockUpdateDataProtocol protocol) {
//        this.updateId = protocol.getUpdateId();
//        this.updateType = protocol.getUpdateType();
//        this.updateCounts = protocol.getUpdateCounts();
//        this.supplier = protocol.getSupplierId();
//        this.comment = protocol.getComment();
//    }