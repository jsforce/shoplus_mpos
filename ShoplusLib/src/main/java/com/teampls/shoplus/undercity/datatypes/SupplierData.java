package com.teampls.shoplus.undercity.datatypes;

import android.support.annotation.NonNull;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.joda.time.DateTime;

/**
 * 발주(Purchase Order) 관련 공급자 데이터
 *
 * @author lucidite
 */
public class SupplierData implements JSONDataWrapper, Comparable<SupplierData> {
    public String supplierId;
    public String name;
    public String memo;

    /**
     * 전화번호 없이 공급자를 생성한다.
     *
     * @param name
     * @param memo
     */
    public SupplierData(String name, String memo) {
        this.supplierId = APIGatewayHelper.getRemoteDateTimeString(DateTime.now());
        this.name = name;
        this.memo = memo;
    }

    /**
     * 전화번호로 공급자를 생성한다.
     *
     * @param suppierId
     * @param name
     * @param memo
     */
    public SupplierData(String suppierId, String name, String memo) {
        this.supplierId = suppierId;
        this.name = name;
        this.memo = memo;
    }

    public SupplierData(JSONObject json) throws JSONException {
        this.supplierId = json.getString("contact");
        this.name = json.optString("name", "");
        this.memo = json.optString("memo", "");
    }

    public String getSupplierId() {
        return this.supplierId == null ? "" : this.supplierId;
    }

    public String getName() {
        return this.name == null ? "" : this.name;
    }

    public String getMemo() {
        return this.memo == null ? "" : this.memo;
    }

    @Override
    public JSONObject getObject() throws JSONException {
        if (this.supplierId == null || this.supplierId.isEmpty()) {
            throw new JSONException("Empty Supplier ID");
        }
        JSONObject data = new JSONObject();
        if (this.name != null && !this.name.isEmpty()) {
            data.put("name", this.name);
        }
        if (this.memo != null && !this.memo.isEmpty()) {
            data.put("memo", this.memo);
        }
        return new JSONObject().put(this.supplierId, data);
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("SupplierData instance does not have a JSON array");
    }

    @Override
    public int compareTo(@NonNull SupplierData o) {
        return name.compareTo(o.name);
    }

    public SupplierData() {
        this.name = "";
        this.supplierId = "";
        this.memo = "";
    }
}
