package com.teampls.shoplus.undercity.protocol;

import com.teampls.shoplus.undercity.enums.WarehousingDepartureType;

import org.joda.time.DateTime;

/**
 * 물류이동 목록을 표시하기 위한 각 발주/창고이동 요약 데이터 프로토콜
 *
 * @author lucidite
 */
public interface LogisticsWarehousingSummaryDataProtocol {
    String getUserId();
    String getWarehousingId();
    DateTime getCreatedDateTime();

    /**
     * 창고입고 유형을 반환한다.
     * @return
     */
    WarehousingDepartureType getType();

    DateTime getDeliveryDateTime();

    /**
     * 도착 여부를 반환한다.
     * [NOTE] 발주(ORDER) 타입의 경우 고객이 직접 도착 설정하며,
     *        창고이동(WAREHOUSING) 타입의 경우 창고관리자가 도착 설정한다.
     *
     * @return
     */
    Boolean isReceived();

    /**
     * 도착으로 처리한 시간 문자열을 반환한다.
     * 도착으로 설정되지 않은 경우 빈 문자열을 반환한다.
     * @return
     */
    DateTime getReceiptDateTime();
}
