package com.teampls.shoplus.undercity.datatypes;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.undercity.enums.WarehousingDepartureType;
import com.teampls.shoplus.undercity.protocol.LogisticsWarehousingSummaryDataProtocol;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucidite
 */
public class LogisticsWarehousingSummaryData implements LogisticsWarehousingSummaryDataProtocol {
    private String userid;
    private String warehousingId;
    private WarehousingDepartureType departureType;
    private DateTime deliveryTime;
    private DateTime receiptDateTime;
    private boolean isReceived;

    public LogisticsWarehousingSummaryData(JSONObject json) throws JSONException {
        this.userid = json.getString("userid");
        this.warehousingId = json.getString("log_id").replaceFirst("W#", "");
        this.departureType = WarehousingDepartureType.fromRemoteStr(json.optString("dep_type", ""));
        this.deliveryTime = APIGatewayHelper.getDateFromRDSDateOnlyString(json.optString("delivery", ""));
        String receiptDateTimeStr = json.optString("receipt", "");
        this.receiptDateTime = APIGatewayHelper.getDateTimeFromRemoteString(receiptDateTimeStr);
        this.isReceived = (receiptDateTimeStr != null && !receiptDateTimeStr.isEmpty());
    }

    public static List<LogisticsWarehousingSummaryDataProtocol> fromJSONArray(JSONArray json) throws JSONException {
        List<LogisticsWarehousingSummaryDataProtocol> result = new ArrayList<>();
        for (int i = 0; i < json.length(); ++i) {
            result.add(new LogisticsWarehousingSummaryData(json.getJSONObject(i)));
        }
        return result;
    }

    @Override
    public String getUserId() {
        return this.userid;
    }

    @Override
    public String getWarehousingId() {
        return this.warehousingId;
    }

    @Override
    public WarehousingDepartureType getType() {
        return this.departureType;
    }

    public DateTime getDeliveryDateTime() {
        return deliveryTime;
    }

    @Override
    public DateTime getCreatedDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.warehousingId);
    }

    @Override
    public Boolean isReceived() {
        return this.isReceived;
    }

    @Override
    public DateTime getReceiptDateTime() {
        return receiptDateTime;
    }

}
