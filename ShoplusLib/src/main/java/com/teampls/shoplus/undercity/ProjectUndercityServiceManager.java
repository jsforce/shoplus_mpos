package com.teampls.shoplus.undercity;

import android.content.Context;

import com.teampls.shoplus.undercity.awsservice.ProjectUndercityService;

/**
 * @author lucidite
 */
public class ProjectUndercityServiceManager {
    public static ProjectUndercityServiceProtocol defaultService(Context context) {
        return new ProjectUndercityService();
    }
}
