package com.teampls.shoplus.lib.services.chatbot;

import android.content.Context;

import com.teampls.shoplus.lib.services.chatbot.awsservices.ProjectChatbotService;

/**
 * @author lucidite
 */
public class ProjectChatbotServiceManager {
    public static ProjectChatbotServiceProtocol defaultService(Context context) {
        return new ProjectChatbotService();
    }
}
