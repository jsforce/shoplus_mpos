package com.teampls.shoplus.lib.datatypes;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.enums.ItemTraceState;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.LocalUserRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;

import org.joda.time.DateTime;

public class ItemTraceRecord {
    public int id = 0, itemId = 0;
    public ColorType color = ColorType.White;
    public SizeType size = SizeType.Default;
    public ItemTraceState state = ItemTraceState.DEFAULT; // 등록 이전 상태
    public String location = "", traceId = ""; // bundle 정보는 traceId에 저장되어 있음
    // DB에 저장하지 않는 임시 정보
    public boolean _found = false;

    public ItemTraceRecord(){};

    public ItemTraceRecord(int id,  String traceId, int itemId, ColorType color, SizeType size,
            ItemTraceState state, String location) {
        this.id = id;
        this.traceId = traceId;
        this.itemId = itemId;
        this.color = color;
        this.size = size;
        this.state = state;
        this.location = location;
    }

    public ItemOptionRecord toOptionRecord() {
        return new ItemOptionRecord(itemId, color, size);
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %d, %s, %s, %s, %s, %s, %s, found? %s", callLocation, itemId, color.getUiName(), size.uiName,
                getDateTime().toString(BaseUtils.fullFormat), state.toString(), location, traceId, _found));
        ItemTraceIdData data = getIdData();
        Log.i("DEBUG_JS", String.format("   %s, %d", data.getBundleVersion(), data.getBundleCount()));
    }

    public ItemTraceRecord(ItemTraceData data) {
        this.traceId = data.getTraceIdData().encode();
        this.itemId = data.getTraceIdData().getItemOption().getItemId();
        this.color = data.getTraceIdData().getItemOption().getColorOption();
        this.size = data.getTraceIdData().getItemOption().getSizeOption();
        this.state = data.getState();
        this.location = data.getLocationOrBuyer();
    }

    public ItemTraceRecord(ItemTraceIdData data, ItemTraceState state, String location) {
        this.traceId = data.encode();
        this.itemId = data.getItemOption().getItemId();
        this.color = data.getItemOption().getColorOption();
        this.size = data.getItemOption().getSizeOption();
        this.state = state;
        this.location = location;
    }

    public ItemTraceRecord(String traceId, ItemTraceState state, String location) {
        this.traceId = traceId;
        ItemTraceIdData itemTraceIdData = new ItemTraceIdData(traceId);
        this.itemId = itemTraceIdData.getItemOption().getItemId();
        this.color = itemTraceIdData.getItemOption().getColorOption();
        this.size = itemTraceIdData.getItemOption().getSizeOption();
        this.state = state;
        this.location = location;
    }

    public ItemTraceIdData getIdData() {
        return new ItemTraceIdData(traceId);
    }

    public boolean isSame(ItemTraceRecord record) {
        if (itemId != record.itemId) return false;
        if (color != record.color) return false;
        if (size != record.size) return false;
        if (state != record.state) return false;
        if (location.equals(record.location) == false) return false;
        if (traceId.equals(record.traceId) == false) return false;
        return false;
    }

    public String getSerialStr() {
        ItemTraceIdData data = new ItemTraceIdData(traceId);
        String serialNumber = data.getSerialNumber();
        if (serialNumber.length() >= 8) {
            return serialNumber.substring(8);
        } else {
            Log.e("DEBUG_JS", String.format("[ItemTraceRecord.getSerialStr] invalid traceId : %s, serial %s", traceId, serialNumber));
            return "";
        }
    }

    public int getSerialNum() {
        try {
            return Integer.valueOf(getSerialStr(), 16);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            return 0;
        }
    }

    public DateTime getDateTime() {
        ItemTraceIdData data = new ItemTraceIdData(traceId);
        // data.getCreatedDateTime(); -- 이건 x 1000이 빠져있어 사용하면 날짜 오류가 발생한다
        String serialNumber = data.getSerialNumber();
        if (serialNumber.length() >= 8) {
            return BaseUtils.toDateTime(Long.valueOf(serialNumber.substring(0, 8), 16)*1000);
        } else {
            Log.e("DEBUG_JS", String.format("[ItemTraceRecord.getSerialStr] invalid traceId : %s, serial %s", traceId, serialNumber));
            return Empty.dateTime;
        }
    }

    public String getLocation(Context context) {
        UserRecord userRecord = UserDB.getInstance(context).getRecord(location);
        if (userRecord.id > 0) {
            return userRecord.getName();
        } else {
            return location;
        }
    }
}
