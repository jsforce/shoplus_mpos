package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.view.MyView;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Medivh on 2019-01-31.
 */

abstract public class BaseModule implements View.OnClickListener {
    protected Context context;
    protected View myView;
    private Set<View> resizedTexts = new HashSet<>();
    protected UserSettingData userSettingData;
    protected GlobalDB globalDB;

    public BaseModule(Context context, View view) {
        this.context = context;
        this.myView = view;
        userSettingData = UserSettingData.getInstance(context);
        globalDB = GlobalDB.getInstance(context);
    }

    public TextView findTextViewById(int viewId) {
        TextView result = (TextView) myView.findViewById(viewId);
        resizeTextView(result);
        return result;
    }

    public TextView findTextViewById(int viewId, String text) {
        TextView result = (TextView) myView.findViewById(viewId);
        result.setText(text);
        resizeTextView(result);
        return result;
    }

    private void resizeTextView(TextView textView) {
        if (resizedTexts.contains(textView) == false) {
            MyView.setTextViewByDeviceSize(context, textView);
            resizedTexts.add(textView);
        }
    }

    public EditText findEditTextById(int viewId, String text) {
        EditText result = myView.findViewById(viewId);
        result.setText(text);
        result.setSelection(text.length());
        resizeTextView(result);
        return result;
    }

    public CheckBox findCheckBoxById(int viewId) {
        CheckBox result = (CheckBox) myView.findViewById(viewId);
        setOnClick(viewId);
        resizeTextView(result);
        return result;
    }

    public View setOnClick(int viewId) {
        View view = myView.findViewById(viewId);
        view.setOnClickListener(this);
        if (view instanceof TextView && resizedTexts.contains(view) == false) {
            MyView.setTextViewByDeviceSize(context, (TextView) view);
            resizedTexts.add(view);
        }
        return view;
    }

    public void setOnClick(int... resIds) {
        for (int resId : resIds) {
            setOnClick(resId);
        }
    }
}
