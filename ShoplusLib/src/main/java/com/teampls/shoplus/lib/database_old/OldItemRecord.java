package com.teampls.shoplus.lib.database_old;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.NewArrivalMessageKey;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;
import com.teampls.shoplus.lib.protocol.NewArrivalsItemDataProtocol;
import com.teampls.shoplus.lib.protocol.SimpleItemDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class OldItemRecord implements ItemDataProtocol, Comparable<OldItemRecord> {
    public int id = 0, itemId = 0, unitPrice = 0, retailPrice = 0;
    public String providerPhoneNumber = "", name = "", description = "", mainImagePath = "", privateNote = "", serialNum = "";
    public DateTime createdDateTime = new DateTime(), modifiedDateTime = new DateTime(), pickedDateTime = new DateTime();
    public ItemCategoryType category = ItemCategoryType.NONE;
    public ItemStateType state = ItemStateType.DISPLAYED;

    public MSortingKey sortingKey = MSortingKey.Date;
    public int _count = 0; // QR, POS 재고 숫자

    public OldItemRecord() {
    }

    public List<OldItemRecord> toList() {
        List<OldItemRecord> results = new ArrayList<>();
        results.add(this);
        return results;
    }

    public OldItemRecord(int id, int itemId, String providerPhoneNumber, String name, String description,
                         DateTime createdDateTime, DateTime modifiedDateTime, DateTime pickedDateTime,
                         String mainImagePath, int unitPrice, int retailPrice, String privateNote, String serialNum,
                         ItemCategoryType category, ItemStateType state) {
        this.id = id;
        this.itemId = itemId;
        this.providerPhoneNumber = providerPhoneNumber;
        this.createdDateTime = createdDateTime;
        this.modifiedDateTime = modifiedDateTime;
        this.name = name;
        this.description = description;
        this.pickedDateTime = pickedDateTime;
        this.mainImagePath = mainImagePath;
        this.unitPrice = unitPrice;
        this.retailPrice = retailPrice;
        this.privateNote = privateNote;
        this.serialNum = serialNum;
        this.category = category;
        this.state = state;
    }

    public OldItemRecord(int itemId, String name) {
        this.itemId = itemId;
        this.name = name;
    }

    public OldItemRecord clone() {
        OldItemRecord result = new OldItemRecord(id, itemId, providerPhoneNumber, name, description, createdDateTime,
                modifiedDateTime, pickedDateTime, mainImagePath, unitPrice, retailPrice, privateNote,
                serialNum, category, state);
        return result;
    }

    public OldItemRecord(ItemDataProtocol itemDataProtocol) {
        this.itemId = itemDataProtocol.getItemId();
        this.providerPhoneNumber = getString(itemDataProtocol.getProviderPhoneNumber());
        this.name = getString(itemDataProtocol.getItemName());
        this.description = getString(itemDataProtocol.getItemDescription());
        this.mainImagePath = getString(itemDataProtocol.getMainImagePath());
        this.createdDateTime = itemDataProtocol.getCreatedDatetime();
        this.modifiedDateTime = itemDataProtocol.getModifiedDatetime();
        this.pickedDateTime = itemDataProtocol.getPickedDatetime(); // 초단위까지만
        this.unitPrice = itemDataProtocol.getPrice();
        this.retailPrice = itemDataProtocol.getRetailPrice();
        this.privateNote = getString(itemDataProtocol.getPrivateNote());
        this.serialNum = itemDataProtocol.getSerialNumber();
        this.category = (itemDataProtocol.getCategory() == null ? ItemCategoryType.NONE : itemDataProtocol.getCategory());
        this.state = itemDataProtocol.getItemState();
    }

    /**
     * [NOTE] 2019.03.22 @sunstrider
     * SimpleItemDataProtocol Chinese Name이 포함되어 있지 않음 @sunstider
     */
    public OldItemRecord(SimpleItemDataProtocol protocol, DateTime createdDateTime) {
        this.itemId = protocol.getItemId();
        this.serialNum = protocol.getSerialNumber();
        this.providerPhoneNumber = protocol.getProviderPhoneNumber();
        this.state = protocol.getItemState();
        this.name = protocol.getItemName();
        this.mainImagePath = protocol.getMainImagePath();
        this.unitPrice = protocol.getPrice();
        this.createdDateTime = createdDateTime;
        this.modifiedDateTime = createdDateTime;
        this.pickedDateTime = createdDateTime;
    }

    /**
     * [NOTE] 2019.03.22 @sunstrider
     * NewArrivalsItemDataProtocol에는 Chinese Name이 포함되어 있지 않음 @sunstider
     */
    public OldItemRecord(NewArrivalMessageKey messageKey, NewArrivalsItemDataProtocol protocol) {
        this.itemId = protocol.getItemId();
        this.providerPhoneNumber = messageKey.getSender();
        this.name = Empty.isEmpty(protocol.getItemName()) ? "" : protocol.getItemName();
        this.description = protocol.getItemDescription();
        if (protocol.getImagePaths() != null && !protocol.getImagePaths().equals("null") && protocol.getImagePaths().size() >= 1)
            this.mainImagePath = protocol.getImagePaths().get(0);
        this.createdDateTime = messageKey.getCreatedDateTime(); // 아이템 생성일자가 아니라 받은 날짜임!
        this.modifiedDateTime = messageKey.getCreatedDateTime();
        this.pickedDateTime = messageKey.getCreatedDateTime();
        this.category = protocol.getCategory();
    }

    /**
     * [NOTE] 2019.03.22 @sunstrider
     * NewArrivalsItemDataProtocol에는 Chinese Name이 포함되어 있지 않음 @sunstider
     */
    public OldItemRecord(NewArrivalsItemDataProtocol protocol) {
        this.itemId = protocol.getItemId();
        this.providerPhoneNumber = protocol.getSender();
        this.name = Empty.isEmpty(protocol.getItemName()) ? "" : protocol.getItemName();
        // [NOTE|STORM-158] chinese name 미포함
        this.description = protocol.getItemDescription();
        if (protocol.getImagePaths() != null && !protocol.getImagePaths().equals("null") && protocol.getImagePaths().size() >= 1)
            this.mainImagePath = protocol.getImagePaths().get(0);
        this.createdDateTime = protocol.getDateTime(); // 아이템 생성일자가 아니라 받은 날짜임!
        this.modifiedDateTime = protocol.getDateTime();
        this.pickedDateTime = protocol.getDateTime();
        this.category = protocol.getCategory();
    }

    public String getPriceStringWithRetail(Context context) {
        if (unitPrice == 0)
            return "";
     else {
        if (UserSettingData.getInstance(context).getProtocol("withRetail").isRetailOn() && retailPrice > 0) {
            return String.format("%s (%s)", BaseUtils.toCurrencyOnlyStr(unitPrice), BaseUtils.toCurrencyOnlyStr(retailPrice));
        } else {
            return BaseUtils.toCurrencyStr(unitPrice);
        }
    }
    }


    private String getString(String serverStr) {
        if (serverStr == null)
            return "";
        if (serverStr.toLowerCase().equals("null"))
            return "";
        return serverStr;
    }

    public String getUiName() {
        if (name.isEmpty())
            return serialNum;
        else
            return name;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] %d, id %d, %s, %s, %d원 (%d), %d개, %s, %s, %s, %s, %s, %s",
                location, id, itemId, providerPhoneNumber, name, unitPrice, retailPrice, _count, createdDateTime.toString(BaseUtils.fullFormat),
                modifiedDateTime.toString(BaseUtils.fullFormat), pickedDateTime.toString(BaseUtils.serverFormat),
                category.toString(), state.toUIStr(), mainImagePath
        ));
    }

    public void toLogCatSimple(String location) {
        Log.i("DEBUG_JS", String.format("[%s] %d, %s, %s",
                location, itemId, name, serialNum));
    }

    public boolean isSame(OldItemRecord record) {
        //    return BaseRecord.isSame(this, record);
        if (itemId != record.itemId) return false;
        if (providerPhoneNumber.equals(record.providerPhoneNumber) == false) return false;
        if (name.equals(record.name) == false) return false;
        if (description.equals(record.description) == false) return false;
        if (createdDateTime.isEqual(record.createdDateTime) == false) return false;
        if (modifiedDateTime.isEqual(record.modifiedDateTime) == false) return false;
        if (pickedDateTime.isEqual(record.pickedDateTime) == false) return false;
        if (mainImagePath.equals(record.mainImagePath) == false) return false;
        if (unitPrice != record.unitPrice) return false;
        if (retailPrice != record.retailPrice) return false;
        if (privateNote.equals(record.privateNote) == false) return false;
        if (serialNum.equals(record.serialNum) == false) return false;
        if (category != record.category) return false;
        if (state != record.state) return false;
        return true;
    }

    @Override
    public int getItemId() {
        return itemId;
    }

    @Override
    public String getSerialNumber() {
        return serialNum;
    }

    @Override
    public String getProviderPhoneNumber() {
        return providerPhoneNumber;
    }

    @Override
    public ItemStateType getItemState() {
        return state;
    }

    @Override
    public ItemCategoryType getCategory() {
        return category;
    }

    @Override
    public String getItemName() {
        return name;
    }

    @Override
    public String getChineseName() {
        return "";
    }

    @Override
    public String getItemDescription() {
        return description;
    }

    @Override
    public String getMainImagePath() {
        return mainImagePath;
    }

    @Override
    public int getPrice() {
        return unitPrice;
    }

    @Override
    public int getRetailPrice() {
        return retailPrice;
    }

    public String getPrivateNote() {
        return privateNote;
    }

    @Override
    public DateTime getCreatedDatetime() {
        return createdDateTime;
    }

    @Override
    public DateTime getModifiedDatetime() {
        return modifiedDateTime;
    }

    @Override
    public DateTime getPickedDatetime() {
        return pickedDateTime;
    }

    @Override
    public int compareTo(@NonNull OldItemRecord record) {
        switch (sortingKey) {
            case ID:
                if (itemId == record.itemId) {
                    return -pickedDateTime.compareTo(record.pickedDateTime);
                } else {
                    return -((Integer) itemId).compareTo(record.itemId);
                }
            case Name:
                if (name.isEmpty() && record.name.isEmpty()) {
                    return 0;
                } else if (name.isEmpty()) {
                    return 1;
                } else if (record.name.isEmpty()) {
                    return -1;
                } else {
                    return name.compareTo(record.name);
                }
            case Count: // currentStock
            case Value:
                return -((Integer) _count).compareTo(record._count);
            default:
            case Date:
                if (pickedDateTime.isEqual(record.pickedDateTime))
                    return -((Integer) itemId).compareTo(record.itemId);
                else
                    return -pickedDateTime.compareTo(record.pickedDateTime);
        }
    }
}