package com.teampls.shoplus.lib.common;

import android.content.Context;

/**
 * Created by Medivh on 2018-11-19.
 */

public class PreferenceBoolean {
    public String key = "";
    public boolean defaultValue = false;
    private boolean prevValue = false;

    public PreferenceBoolean(String key, boolean defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
        this.prevValue = defaultValue;
    }

    public void updatePrevValue(Context context) {
        prevValue = getValue(context);
    }

    public boolean getValue(Context context) {
        return BasePreference.getBoolean(context, key, defaultValue);
    }

    public void setValue(Context context, boolean value) {
        BasePreference.setBoolean(context, key, value);
    }

    public boolean isChanged(Context context) {
        return prevValue != BasePreference.getBoolean(context, key, defaultValue);
    }

}
