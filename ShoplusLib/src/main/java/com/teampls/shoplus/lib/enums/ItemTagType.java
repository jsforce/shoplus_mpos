package com.teampls.shoplus.lib.enums;

/**
 * 아이템 태그 타입. 서버에 저장되는 문자열의 일관성을 위해 별도 ENUM 타입으로 지정.
 *
 * @author lucidite
 */
public enum ItemTagType {
    NO_TAG(""), DISPLAY("DP"), DISCOUNT("DC"), NOTICE("NT"), CODI("CD");

    private String tagStr;

    ItemTagType(String tagStr) {
        this.tagStr = tagStr;
    }

    /**
     * 서버로 전송하기 위한 (HTTP Request의 파라미터) 문자열로 변환한다.
     *
     * @return
     */
    public String toParamString() {
        return this.tagStr;
    }

    /**
     * 서버로부터 받은 (파라미터) 문자열을 ItemTagType으로 변환한다.
     *
     * @param paramStr 서버로부터 받은 문자열 (EnumType 내부에 저장된 문자열과 비교한다)
     * @return  일치하는 ItemTagType. paramStr이 null이거나 일치하는 대상이 없으면 NO_TAG를 반환한다.
     */
    public static ItemTagType toTagType(String paramStr) {
        if (paramStr == null) {
            return ItemTagType.NO_TAG;
        }
        for (ItemTagType tag : ItemTagType.values()) {
            if (tag.tagStr.equalsIgnoreCase(paramStr)) {
                return tag;
            }
        }
        return ItemTagType.NO_TAG;
    }
}
