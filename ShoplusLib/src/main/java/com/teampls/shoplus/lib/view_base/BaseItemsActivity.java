package com.teampls.shoplus.lib.view_base;

import android.widget.AdapterView;

/**
 * Created by Medivh on 2017-12-01.
 */

abstract public class BaseItemsActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    abstract public void refresh();

}
