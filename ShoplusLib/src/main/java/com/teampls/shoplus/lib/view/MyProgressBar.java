package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.widget.ProgressBar;

public class MyProgressBar {
	private ProgressBar progressBar;
	private volatile Thread progressBarThread;
	private int currentPos = 0;
	private int totalNum = 0;
	private int count;
	private Activity activity;
	
	public MyProgressBar(ProgressBar progressBar, int totalNum, Activity activity) {
		this.progressBar = progressBar;
		this.totalNum = totalNum;
		progressBar.setMax(totalNum);
		this.activity = activity;
	}
	
	public void start() {
		if (progressBarThread == null) {
			progressBarThread = new Thread(null,backgroundThread,"startProgressBarThread");
			currentPos = 0;
			progressBarThread.start();
		}	
	}
	
	public synchronized void stop() {
		if (progressBarThread != null) {
			Thread tmpThread = progressBarThread;
			progressBarThread = null;
			tmpThread.interrupt();
		}
		//progressBar.setVisibility(ProgressBar.GONE);		
	}

	private Runnable backgroundThread = new Runnable() {
		@Override
		public void run() {
			if (Thread.currentThread() == progressBarThread) {
				currentPos = 0;
				while (currentPos < totalNum) {
					try {
						progressBarHandle.sendMessage(progressBarHandle.obtainMessage());
						Thread.sleep(100);
					} catch (final InterruptedException e) {
						return;
					} catch (final Exception e) {
						return;
					}
				}
			}
		}

		Handler progressBarHandle = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				currentPos = currentPos + 100;
				progressBar.setProgress(currentPos);
				if (currentPos == totalNum) {
					stop();
				}
			}
		};
	};
}