package com.teampls.shoplus.lib.datatypes;

import org.json.JSONObject;

/**
 * 파파고 번역 지원을 위한 API 정보
 *
 * @author lucidite
 */
public class PapagoTranslationAPIData {
    private String apiUrl;
    private String clientId;
    private String clientSecret;

    public PapagoTranslationAPIData(JSONObject dataObj) {
        if (dataObj == null) {
            this.apiUrl = "";
            this.clientId = "";
            this.clientSecret = "";
        } else {
            this.apiUrl = dataObj.optString("url", "");
            this.clientId = dataObj.optString("id", "");
            this.clientSecret = dataObj.optString("key", "");
        }
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public boolean isAvailable() {
        return (!this.apiUrl.isEmpty()) && (!this.clientId.isEmpty()) && (!this.clientSecret.isEmpty());
    }
}
