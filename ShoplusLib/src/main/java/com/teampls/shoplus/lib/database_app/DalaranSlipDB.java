package com.teampls.shoplus.lib.database_app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.database.SlipDB;

/**
 * Created by Medivh on 2017-11-03.
 */

public class DalaranSlipDB extends SlipDB {

    private static DalaranSlipDB instance = null;

    public static DalaranSlipDB getInstance(Context context) {
        try {
            Context dalaranContext = context.createPackageContext(MyApp.Dalaran.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            if (instance == null)
                instance = new DalaranSlipDB(dalaranContext);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[DalaranItemDB.getInstance] not found"));
        }
        return instance;
    }

    public static boolean exists(Context context) {
        try {
            Context dalaranContext = context.createPackageContext(MyApp.Dalaran.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private DalaranSlipDB(Context dalaranContext) {
        super(dalaranContext);
        items = DalaranSlipItemDB.getInstance(context);
    }

}
