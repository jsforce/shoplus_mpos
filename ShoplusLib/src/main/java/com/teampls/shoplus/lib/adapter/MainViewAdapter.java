package com.teampls.shoplus.lib.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.view_base.BaseFragment;

public class MainViewAdapter extends FragmentPagerAdapter {
    private final List<BaseFragment> fragments = new ArrayList<>();
    private final List<String> fragmentTitles = new ArrayList<>();
    public boolean doRefresh = false;

    public MainViewAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    public List<BaseFragment> getFragments() {
        return fragments;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public void addFragment(BaseFragment fragment) {
        if (fragment.isAdded()) {
            Log.w("DEBUG_JS", String.format("[MainViewAdapter.addFragment] %s is already added", fragment.getTitle()));
            return;
        }
        //Log.i("DEBUG_JS", String.format("[MainViewAdapter.addFragment] %s, tab %d", fragment.getTitle(), getCount()));
        fragments.add(fragment);
        fragmentTitles.add(fragment.getTitle());
        notifyDataSetChanged();
    }

    public void clearFragments() {
        fragments.clear();
        fragmentTitles.clear();
        notifyDataSetChanged();
        //   Log.i("DEBUG_JS", String.format("[MainViewAdapter.clearFragments] ..."));
    }

    public void refreshFragments() {
        for (BaseFragment fragment : getFragments())
            fragment.refresh();
    }

    public boolean isFragementsCreated() {
        for (BaseFragment fragment : getFragments())
            if (fragment.isViewCreated == false)
                return false;
        return true;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitles.get(position);
    }

    public void setPageTitle(int position, String title) {
        fragmentTitles.set(position, title);
    }

    public int getIndex(String title) {
        int index = 0;
        for (String fragmentTitle : fragmentTitles) {
            if (fragmentTitle.equals(title)) {
                return index;
            } else {
                index = index + 1;
            }
        }
        return 0;
    }

    public boolean hasFragment(String className) {
        for (BaseFragment fragment : getFragments()) {
            if (fragment.getClass().getSimpleName().toLowerCase().equals(className.toLowerCase()))
                return true;
        }
        return false;
    }

    public BaseFragment getFragment(String className) {
        for (BaseFragment fragment : getFragments()) {
            if (fragment.getClass().getSimpleName().toLowerCase().equals(className.toLowerCase()))
                return fragment;
        }
        return new Empty.EmptyFragment();
    }

    public BaseFragment getFragment(int position) {
        if (position < fragments.size()) {
            return fragments.get(position);
        } else {
            return Empty.getFragment();
        }
    }

    @Override
    public int getItemPosition(Object object) {
        if (doRefresh) {
            return POSITION_NONE;
        } else {
            return super.getItemPosition(object);
        }
    }

}


