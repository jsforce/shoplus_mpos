package com.teampls.shoplus.lib.awsservice.apigateway.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author lucidite
 */
public interface JSONDataWrapper {
    JSONObject getObject() throws JSONException;
    JSONArray getArray() throws JSONException;
}
