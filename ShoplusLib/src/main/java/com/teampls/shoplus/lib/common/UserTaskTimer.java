package com.teampls.shoplus.lib.common;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.event.MyOnTick;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Medivh on 2016-04-14.
 */

public class UserTaskTimer  extends Timer {
    private UserTimerTask task = new UserTimerTask();
    private MyOnTick onTickEvent;
    private long delay, period = 0;

    public UserTaskTimer (String id, long delay, long period, MyOnTick I) {
        super();
       task.id = id;
        this.delay = delay;
        this.period = period;
        setOnTickEvent(I);
    }

    public void start(Context context) {
        task.context = context;
        this.schedule(task, delay, period);
    }

    public String getId() {
        return task.id;
    }

    public void setOnTickEvent(MyOnTick I) {
        task.onTickEvent = I;
    }

}

class UserTimerTask extends TimerTask {
    public String id = "";
    public MyOnTick onTickEvent;
    public Context context;
    public boolean stop = false;

    @Override
    public void run() {
        if (context == null) {
            Log.i("DEBUG_JS", String.format("[%s] context == null", getClass().getSimpleName()));
            stop = true;
            return;
        }

        if (MyUI.isActivity(context, "MyAsyncTask") == false)
            return;

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                onTickEvent.onTick(id);
            }
        });
    }
}
