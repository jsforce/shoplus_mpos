package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.teampls.shoplus.lib.enums.DBResultType;

import java.util.Map;

/**
 * Created by Medivh on 2017-10-18.
 */

public class LocalItemDB extends BaseDB<LocalItemRecord> {
    private static LocalItemDB instance = null;
    private static int Ver = 2;

    public enum Column {
        _id, itemId, cost;
        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    public static LocalItemDB getInstance(Context context) {
        if (instance == null)
            instance = new LocalItemDB(context);
        return instance;
    }

    private LocalItemDB(Context context) {
        super(context, "LocalItemDB", Ver, Column.toStrs());
    }

    @Override
    protected int getId(LocalItemRecord record) {
        return record.id;
    }

    @Override
    protected LocalItemRecord getEmptyRecord() {
        return new LocalItemRecord();
    }

    @Override
    protected LocalItemRecord createRecord(Cursor cursor) {
        return new LocalItemRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getInt(Column.itemId.ordinal()),
                cursor.getInt(Column.cost.ordinal())
        );
    }

    public void toLogCat(String callLocation) {
        for (LocalItemRecord record : getRecords())
            record.toLogCat(callLocation);
    }

    @Override
    protected ContentValues toContentvalues(LocalItemRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.itemId.toString(), record.itemId);
        contentvalues.put(Column.cost.toString(), record.cost);
        return contentvalues;
    }

    public LocalItemRecord getRecordBy(int itemId) {
        return getRecord(Column.itemId, Integer.toString(itemId));
    }

    public LocalItemRecord getRecordWithItemId(int itemId) {
        LocalItemRecord result = getRecord(Column.itemId, Integer.toString(itemId));
        if (result.id == 0)
            result.itemId = itemId;
        return result;
    }

    public DBResult updateOrInsert(LocalItemRecord record) {
        LocalItemRecord dbRecord = getRecordBy(record.itemId);
        if (dbRecord.id > 0) {
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public void updateOrInsert(Map<Integer, Integer> costMap) { // itemId - cost
        for (int itemId : costMap.keySet())
            updateOrInsert(new LocalItemRecord(0, itemId, costMap.get(itemId)));
    }

    public void deleteBy(int itemId) {
        for (int id : getIds(Column.itemId, itemId))
            delete(id);
    }

}
