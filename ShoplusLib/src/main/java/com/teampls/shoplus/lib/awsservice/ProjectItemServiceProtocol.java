package com.teampls.shoplus.lib.awsservice;

import android.support.annotation.Nullable;
import android.util.Pair;

import com.teampls.shoplus.lib.datatypes.ItemCostPriceBundle;
import com.teampls.shoplus.lib.datatypes.ItemDataBundle;
import com.teampls.shoplus.lib.datatypes.ItemOptionData;
import com.teampls.shoplus.lib.datatypes.ItemSearchKey;
import com.teampls.shoplus.lib.datatypes.ItemStockChangeLogBundleData;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.datatypes.MerchandiseData;
import com.teampls.shoplus.lib.datatypes.SpecificItemDataBundle;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.protocol.ImageDataProtocol;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;
import com.teampls.shoplus.lib.protocol.SimpleItemDataProtocol;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

/**
 * 아이템 관련 공통 서비스 API 관리자 프로토콜
 * NOTE. 현재 ProjectCommonService 등에 아이템 관련 API가 분산되어 있으며, 차차 리팩토링할 것(이 클래스로 이전)
 * (보조 계정으로 주 계정의 아이템 정보에 접근하는 기능을 일괄적으로 지원하는 경우를 고려)
 *
 * @author lucidite
 */

public interface ProjectItemServiceProtocol {

    List<ItemDataProtocol> makeItems(UserShopInfoProtocol userShop, List<String> mainImageLocalPaths) throws MyServiceFailureException;

    ItemDataProtocol makeItemWithoutImage(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    ItemDataProtocol updateItem(UserShopInfoProtocol userShop, ItemDataProtocol original, ItemDataProtocol updated) throws MyServiceFailureException;

    void deleteItem(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException;

    /**
     * 아이템 재고 데이터를 조회한다.
     *
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    Map<ItemStockKey, Integer> getItemStock(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 아이템 재고 데이터를 업데이트한다.
     * 기존에 없는 재고항목(item-color-size)은 새로 작성하며, 있는 항목은 재고값을 덮어쓴다.
     *
     * @param userShop
     * @param remains   재고값. 재고항목(item-color-size)과 재고항목별 재고수량의 map 데이터
     * @throws MyServiceFailureException
     */
    void updateItemStock(UserShopInfoProtocol userShop, Map<ItemStockKey, Integer> remains) throws MyServiceFailureException;

    /**
     * 특정 아이템 옵션(및 연동재고) 데이터를 삭제한다.
     *
     * @param userShop
     * @param optionToBeDeleted
     * @throws MyServiceFailureException
     */
    void deleteItemStock(UserShopInfoProtocol userShop, ItemStockKey optionToBeDeleted) throws MyServiceFailureException;

    /**
     * [STORM-156] 개별 옵션을 판매중 또는 판매완료로 상태를 변경한다.
     *
     * @param userShop
     * @param itemOption 상태를 변경할 상품 옵션
     * @param isSoldOut 판매완료일 경우 true, 판매중일 경우 false
     * @throws MyServiceFailureException
     */
    void updateItemOptionState(UserShopInfoProtocol userShop, ItemStockKey itemOption, boolean isSoldOut) throws MyServiceFailureException;

    Map<ItemStockKey, ItemOptionData> adjustStock(UserShopInfoProtocol userShop, List<SlipItemDataProtocol> transactionItems, Boolean isCancellation, int firstItemId) throws MyServiceFailureException;

    /**
     * 기본 상품 데이터를 조회한다.
     *
     * @param userShop
     * @param isMPOS mPOS App인 경우 true, 이외의 경우 false (API 분화 전까지는 수량 조건이 달라짐)
     * @param isExcludingSoldout Sold-out 상품을 제외하려면 true, Sold-out을 포함한 전체 상품을 조회하려면 false
     * @return
     * @throws MyServiceFailureException
     */
    ItemDataBundle getItemsWithStock(UserShopInfoProtocol userShop, Boolean isMPOS, Boolean isExcludingSoldout) throws MyServiceFailureException;

    /**
     * 특정 범위의 아이템(및 옵션/재고) 정보를 조회한다.
     *
     * @param userShop
     * @param startItemId 조회하려는 범위의 시작 ID (inclusive)
     * @param endItemId 조회하려는 범위의 종료 ID (inclusive)
     * @param isExcludingSoldout Sold-out 상품을 제외하려면 true, Sold-out을 포함한 전체 상품을 조회하려면 false
     * @return
     * @throws MyServiceFailureException
     */
    ItemDataBundle getRangedItemsWithStock(UserShopInfoProtocol userShop, int startItemId, int endItemId, Boolean isExcludingSoldout) throws MyServiceFailureException;

    /**
     * 특정 월의 상품 목록을 조회한다.
     *
     * @param userShop
     * @param month
     * @return
     * @throws MyServiceFailureException
     */
    List<SimpleItemDataProtocol> getMontlyItemList(UserShopInfoProtocol userShop, DateTime month) throws MyServiceFailureException;

    /**
     * 특정 월의 판매완료 상품 목록을 조회한다.
     * [NOTE] mPOS 사용자 지원, SILVER-369
     *
     * @param userShop
     * @param month
     * @return
     * @throws MyServiceFailureException
     */
    List<SimpleItemDataProtocol> getMontlySoldoutItemList(UserShopInfoProtocol userShop, DateTime month) throws MyServiceFailureException;

    /**
     * 특정 아이템의 정보 및 옵션 정보를 조회한다.
     *
     * @param userShop
     * @param itemid
     * @return
     * @throws MyServiceFailureException
     */
    SpecificItemDataBundle getSpecificItem(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException;

    /**
     * 상품번호로 특정 아이템의 정보 및 옵션 정보를 조회한다.
     * 여기서의 상품번호는 상품 카탈로그 수준에서 할당한 일련번호로, 개별 상품(현물)의 일련번호가 아니다.
     *
     * @param userShop
     * @param itemSerialNumber  yy-#### 포맷, ('-'를 제외한) 각 자리는 모두 10진수 숫자
     * @return
     * @throws MyServiceFailureException
     */
    SpecificItemDataBundle getSpecificItem(UserShopInfoProtocol userShop, String itemSerialNumber) throws MyServiceFailureException;

    ItemDataBundle searchItems(UserShopInfoProtocol userShop, String itemNameSearchWord) throws MyServiceFailureException;

    Pair<ItemDataBundle, ItemSearchKey> getItemsOfCategory(UserShopInfoProtocol userShop, ItemCategoryType category, ItemSearchKey searchKey) throws MyServiceFailureException;

    ImageDataProtocol downloadItemImage(String remoteImagePath) throws MyServiceFailureException;

    ImageDataProtocol downloadItemThumbnail(String remoteImagePath) throws MyServiceFailureException;

    ImageDataProtocol downloadMainImage(ItemDataProtocol item) throws MyServiceFailureException;

    ImageDataProtocol downloadMainThumbnail(ItemDataProtocol item) throws MyServiceFailureException;

    /**
     * 판매자의 상품 정보를 조회한다. 아이템 이름과 섬네일 정보를 받아온다.
     *
     * @param userShop
     * @param provider 판매자 (전화번호)
     * @param itemid 상품의 아이템 ID
     * @return
     * @throws MyServiceFailureException
     */
    MerchandiseData getMerchandiseData(UserShopInfoProtocol userShop, String provider, int itemid) throws MyServiceFailureException;

    ImageDataProtocol downloadSlipImage(String remoteImagePath) throws MyServiceFailureException;

    /**
     * 특정 아이템의 매입원가를 수정한다.
     * marginAdditionRateInPercent을 설정하여 가격 정보도 같이 업데이트할 수 있다.
     *
     * @param userShop
     * @param itemid
     * @param costPrice 매입원가 (0 이상, 원 단위 금액)
     * @param marginAdditionRateInPercent 마진 가산 비율(%), 가격 자동 업데이트를 적용하지 않을 경우 null을 전달할 것
     * @return 가격 정보를 업데이트한 경우 서버에서 업데이트한 가격(sales price)을, 업데이트하지 않은 경우 null을 반환
     * @throws MyServiceFailureException
     */
    @Nullable
    Integer updateCostPrice(UserShopInfoProtocol userShop, int itemid, int costPrice, @Nullable Integer marginAdditionRateInPercent) throws MyServiceFailureException;

    /**
     * 특정 아이템의 매입원가를 조회한다.
     *
     * @param userShop
     * @param itemid
     * @return 매입원가 값, 설정되어 있지 않은 경우 0을 반환한다.
     * @throws MyServiceFailureException
     */
    int getCostPriceOfAnItem(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException;

    /**
     * 아이템들의 매입원가를 조회한다. 아이템 ID 범위를 지정한다.
     *
     * e.g. 거래기록 목록의 매입원가 목록을 조회하는 경우, 거래기록의 아이템 ID 시작과 끝을 전달한다.
     *
     * @param userShop
     * @param fromItemId
     * @param toItemId
     * @return
     * @throws MyServiceFailureException
     */
    ItemCostPriceBundle getCostPriceOfItems(UserShopInfoProtocol userShop, int fromItemId, int toItemId) throws MyServiceFailureException;

    /**
     * 허용된 사용자로부터 아이템 카탈로그 정보를 복제하여 아이템을 생성한다.
     * [NOTE] 마지막으로 복제한 이후에 생성된 아이템만 복제한다. 정상적인 경우 중복 복사가 발생하지 않아야 한다.
     *
     * @param userShop
     * @param sourcePhoneNumber 복제하려는 카탈로그 사용자. 메인 샵이 아님에 유의한다.
     * @return 새로 복제된 아이템 개수를 반환한다.
     * @throws MyServiceFailureException
     */
    int cloneItems(UserShopInfoProtocol userShop, String sourcePhoneNumber) throws MyServiceFailureException;

    // [SILVER-231/SILVER-403] Item Bookmark 기능

    /**
     * 상품 즐겨찾기 목록에 특정 상품을 추가하고 해당 상품의 정보를 받아온다.
     * [NOTE] 현재 기기에 없는 상품을 과거 목록(월별)에서 즐겨찾기했을 때 즉시 상품정보를 가져오기 위한 용도의 기능이다.
     *
     * @param userShop
     * @param itemid
     * @return
     * @throws MyServiceFailureException
     */
    Pair<List<Integer>, SpecificItemDataBundle> postBookmarkAndGetItemData(
            UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException;

    /**
     * 상품 즐겨찾기 목록에 특정 상품을 추가한다.
     * [NOTE] 현재 기기에 있는 상품을 즐겨찾기했을 때 상품정보 조회 없이 즐겨찾기에만 추가하기 위한 용도의 기능이다.
     *
     * @param userShop
     * @param itemid
     * @return
     * @throws MyServiceFailureException
     */
    List<Integer> postBookmark(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException;

    /**
     * 상품 즐겨찾기 목록을 업데이트한다.
     * [NOTE] 즐겨찾기 상세편집 - 순서 변경, 여러 상품의 삭제 및 전체삭제 등 - 에 활용하기 위한 용도의 기능이다.
     *
     * @param userShop
     * @param bookmarks
     * @throws MyServiceFailureException
     */
    void updateBookmarks(UserShopInfoProtocol userShop, List<Integer> bookmarks) throws MyServiceFailureException;

    /**
     * 특정 상품을 즐겨찾기 목록에서 삭제한다.
     *
     * @param userShop
     * @param itemid
     * @return
     * @throws MyServiceFailureException
     */
    List<Integer> deleteBookmark(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException;

    /**
     * 아이템의 메인 이미지를 교체한다. 변경된 메인 이미지의 remote 경로를 반환한다.
     *
     * @param userShop
     * @param itemid
     * @param mainImageLocalPath
     * @return 메인 이미지의 변경된 remote 경로
     * @throws MyServiceFailureException
     */
    String changeMainImage(UserShopInfoProtocol userShop, int itemid, String mainImageLocalPath) throws MyServiceFailureException;

    /**
     * [SILVER-530] 특정 옵션의 일별 재고수량 변동 로그를 조회한다.
     * [NOTE] 카탈로그 사용자용, 날짜별 navigation 지원
     *
     * @param userShop
     * @param itemOption
     * @param specificDay
     * @return
     * @throws MyServiceFailureException
     */
    ItemStockChangeLogBundleData getItemStockChangeLogData(UserShopInfoProtocol userShop, ItemStockKey itemOption, DateTime specificDay) throws MyServiceFailureException;

    /**
     * [SILVER-530] 특정 옵션의 최근재고수량 변동 로그를 조회한다.
     * [NOTE] mPOS 사용자용, 최근 24시간 조회 (서버에서 설정)
     *
     * @param userShop
     * @param itemOption
     * @return
     * @throws MyServiceFailureException
     */
    ItemStockChangeLogBundleData getRecentItemStockChangeLogData(UserShopInfoProtocol userShop, ItemStockKey itemOption) throws MyServiceFailureException;
}
