package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * App Version 정보
 *
 * @author lucidite
 */

public class AppVersion {
    private String latestVersion;
    private String requiredVersion;

    public AppVersion() {
        // invalid information: isValid 메서드로 다운로드받은 유효성을 확인할 수 있다.
        this.latestVersion = "";
        this.requiredVersion = "";
    }

    public AppVersion(String jsonString) throws JSONException {
        JSONObject versionObj = new JSONObject(jsonString);
        this.latestVersion = versionObj.getString("l");
        this.requiredVersion = versionObj.getString("r");
    }

    public String getLatestVersion() {
        return latestVersion;
    }

    public String getRequiredVersion() {
        return requiredVersion;
    }

    public Boolean isValid() {
        return !this.latestVersion.isEmpty() && !this.requiredVersion.isEmpty();
    }
}
