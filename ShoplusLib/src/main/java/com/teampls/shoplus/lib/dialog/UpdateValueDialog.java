package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.text.InputType;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2017-04-07.
 */

abstract public class UpdateValueDialog extends BaseDialog implements View.OnKeyListener {
    public boolean doDismiss = true;
    protected EditText etString;

    public UpdateValueDialog(Context context, String title, String message, int number) {
        this(context, title, message, number, 0);
    }

    public UpdateValueDialog(Context context, String title, String message, float number) {
        this(context, title, message, "", 0.8);
        if (number == 0)
            etString.setText("");
        else
            etString.setText(Float.toString(number));
        etString.setHint("0");
        etString.setGravity(Gravity.CENTER_VERTICAL);
        etString.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL); //|InputType.TYPE_NUMBER_FLAG_SIGNED);
        etString.setOnKeyListener(this); // number에서만 엔터 = 닫기 기능 사용
    }

    public UpdateValueDialog(Context context, String title, String message, int number, int hint) {
        this(context, title, message, "", 0.8);
        if (number == 0)
            etString.setText("");
        else
            etString.setText(Integer.toString(number));
        etString.setHint(Integer.toString(hint));
        etString.setGravity(Gravity.CENTER_VERTICAL);
        etString.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL); //|InputType.TYPE_NUMBER_FLAG_SIGNED);
        etString.setOnKeyListener(this); // number에서만 엔터 = 닫기 기능 사용
    }

    public UpdateValueDialog(Context context, String title, String message, String value) {
        this(context, title, message, value, 0.95);
    }

    public UpdateValueDialog(Context context, String title, String value, boolean isLongMemo) {
        this(context, title, "", value, 0.95);
        setVisibility(View.GONE, R.id.dialog_update_value_edittext_clear);
        Button button = (Button) findViewById(R.id.dialog_update_value_function);
        button.setVisibility(View.VISIBLE);
        button.setText("지우기");
        setVisibility(View.VISIBLE, button);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                etString.setText("");
            }
        });
    }

    private UpdateValueDialog(Context context, String title, String message, String value, double widthRatio) {
        super(context);
        findTextViewById(R.id.dialog_update_value_title, title);
        setDialogWidth(widthRatio, 0.7);
        etString = (EditText) findViewById(R.id.dialog_update_value_edittext);
        etString.setText(value);
        etString.setHint(value);
        etString.requestFocus();

        if (message.isEmpty()) {
            setVisibility(View.GONE, R.id.dialog_update_value_message);
        } else {
            ((TextView) findViewById(R.id.dialog_update_value_message)).setText(message);
        }
        setOnClick(R.id.dialog_update_value_cancel, R.id.dialog_update_value_apply, R.id.dialog_update_value_function,
                R.id.dialog_update_value_edittext_clear);

        MyDevice.showKeyboard(context, etString);
        show();

        MyView.setTextViewByDeviceSize(context, etString);
        MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_update_value_title, R.id.dialog_update_value_message);

        new MyDelayTask(context, 100) {
            @Override
            public void onFinish() {
                if (etString.getText().toString().length() > 0)
                    etString.setSelection(etString.getText().toString().length()); // 주의, etString에는 아직 글씨가 쓰여져있지 않다
            }
        };
    }

    protected void enableFunctionButton(String title) {
        setVisibility(View.VISIBLE, R.id.dialog_update_value_function);
        findTextViewById(R.id.dialog_update_value_function).setText(title);
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_update_value;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_ENTER:
                    onApplyClick(etString.getText().toString().trim());
                    if (doDismiss) {
                        MyDevice.hideKeyboard(context, etString);
                        dismiss();
                    }
                    return true;
                default:
                    break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_update_value_cancel) {
            MyDevice.hideKeyboard(context, etString);
            dismiss();
        } else if (view.getId() == R.id.dialog_update_value_apply) {
            onApplyClick(etString.getText().toString().trim());
            if (doDismiss) {
                MyDevice.hideKeyboard(context, etString);
                dismiss();
            }
        } else if (view.getId() == R.id.dialog_update_value_edittext_clear) {
            etString.setText("");
            etString.setHint("");

        } else if (view.getId() == R.id.dialog_update_value_function) {
            onFunctionClick();
        }
    }

    protected void onFunctionClick() {
    }

    abstract public void onApplyClick(String newValue);
}
