package com.teampls.shoplus.lib.enums;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum SizeType {
    Default(null, "기본"),
    XSmall("xs", "XS"), Small("s", "S"),  Medium("m", "M"),  Large("l", "L"),  Xlarge("xl", "XL"),  xXLarge("xxl", "xxL"), Free("free", "Free"),
    s44, s55, s66, s77, s88, s99, // 44-99
    s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, // 0-12
    s14 ,s16, s18, s20, s22, s24, s25, s26, s27, s28, s29, s30, // 14-30
    s32, s34, s35, s36, s37, s38, s39, s40, s41, s42, // 32-42
    s80, s85, s90, s95, s100, s105, s110, // 80-110
    s220, s225, s230, s235, s240, s245, s250, s255,
    s260, s265, s270, s275, s280, s285, s290, s295, s300
    ;

    private String remoteStr;
    public String uiName = "";

    SizeType(String remoteStr, String uiName) {
        this.remoteStr = remoteStr;
        this.uiName = uiName;
    }

    SizeType() {
        this.remoteStr = this.toString().replace("s", "");
        this.uiName = remoteStr;
    }

    public static SizeType valueOfDefault(String name) {
        try {
            return valueOf(name);
        } catch (Exception e) {
            SizeType result = SizeType.Default;
            return result;
        }
    }

    public String toRemoteStr() {
        if (TextUtils.isEmpty(remoteStr)) {
            return "";
        } else {
            return this.remoteStr;
        }
    }

    public List<SizeType> toList() {
        List<SizeType> results = new ArrayList<>();
        results.add(this);
        return results;
    }

    public static SizeType ofRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty()) {
            return Default;
        }
        for (SizeType size : SizeType.values()) {
            if (size.remoteStr == null) {
                continue;
            }
            if (size.remoteStr.toLowerCase().equals(remoteStr.toLowerCase())) {
                return size;
            }
        }
        return Default;
    }

}
