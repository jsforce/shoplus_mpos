package com.teampls.shoplus.lib.gcm;

/**
 * Created by Medivh on 2016-04-08.
 */
public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}
