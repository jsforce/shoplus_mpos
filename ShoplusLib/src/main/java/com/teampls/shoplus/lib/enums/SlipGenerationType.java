package com.teampls.shoplus.lib.enums;

/**
 * Slip 생성과 관련된 분류 타입
 *
 * @author lucidite
 */

public enum SlipGenerationType {
    NONE(null),
    MANUAL("m"),
    AUTO_GENERATED("a"),
    AUTO_GENERATED_CARBON_COPY("c");

    private String remoteStr;

    SlipGenerationType(String remoteStr) {
        this.remoteStr = remoteStr;
    }

    public String toRemoteStr() {
        return remoteStr;
    }

    /**
     * 서버로부터 수신한 문자열로부터 Slip 타입을 반환한다.
     * fallback은 backward compatibility를 위해 제공한다(필드값을 갖지 않는 과거 데이터를 수동 입력 등으로 처리).
     *
     * @param remoteStr
     * @param fallback 일치하는 문자열이 없는 경우 반환할 Slip 타입
     * @return
     */
    public static SlipGenerationType fromRemoteStr(String remoteStr, SlipGenerationType fallback) {
        if (remoteStr == null) {
            return fallback;            // 하방호환성 용도: 과거 입력된 slips을 처리: MAYQUEEN
        }
        for (SlipGenerationType type: SlipGenerationType.values()) {
            if (remoteStr.equals(type.remoteStr)) {
                return type;
            }
        }
        return fallback;
    }
}
