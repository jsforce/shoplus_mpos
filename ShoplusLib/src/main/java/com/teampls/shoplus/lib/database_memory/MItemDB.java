package com.teampls.shoplus.lib.database_memory;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.ProjectItemServiceManager;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ProjectItemServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ServiceErrorHelper;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.datatypes.SpecificItemDataBundle;
import com.teampls.shoplus.lib.enums.ItemCategoryType;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Medivh on 2018-06-30.
 */

public class MItemDB extends BaseMapDB<String, ItemRecord> {
    private static MItemDB instance;
    public MItemOptionDB options;
    private int refreshedItemIdMin = 0;
    private ItemDB files;
    private ProjectItemServiceProtocol itemService;
    private UserSettingData userSettingData;
    private List<Integer> bookmarks = new ArrayList<>();

    public static MItemDB getInstance(Context context) {
        if (instance == null)
            instance = new MItemDB(context);
        return instance;
    }

    private MItemDB(Context context) {
        super(context, "MItemDB");
        files = ItemDB.getInstance(context);
        options = MItemOptionDB.getInstance(context);
        itemService = ProjectItemServiceManager.defaultService(context);
        userSettingData = UserSettingData.getInstance(context);
    }

    @Override
    public void clear() {
        super.clear();
        options.clear();
    }

    @Override
    protected ItemRecord getEmptyRecord() {
        return new ItemRecord();
    }

    @Override
    public String getKeyValue(ItemRecord record) {
        return Integer.toString(record.itemId);
    }

    public List<Integer> getBookmarks() {
        return bookmarks;
    }

    public int getRefreshedItemIdMin() {
        if (refreshedItemIdMin > 0)
            return refreshedItemIdMin;
        else {
            return BaseUtils.getMin(getItemIds());
        }
    }

    private List<Integer> getItemIds() {
        Set<Integer> results = new HashSet<>();
        for (String itemIdStr : getKeys())
            results.add(BaseUtils.toInt(itemIdStr));
        return new ArrayList(results);
    }

    public List<ItemRecord> getRecordsBy(List<Integer> itemIds) {
        ArrayList<ItemRecord> records = new ArrayList<ItemRecord>(0);
        if (itemIds.size() <= 0) return records;
        for (int itemId : itemIds) {
            ItemRecord record = getRecordBy(itemId);
            if (record.itemId > 0) // found
                records.add(record);
        }
        return records;
    }

    public boolean has(int itemId) {
        return databaseMap.containsKey(Integer.toString(itemId));
    }

    public ItemRecord getRecordBy(int itemId) {
        return getRecordByKey(Integer.toString(itemId));
    }


    public void copyFrom(ItemDB itemDB) {
        clear();
        insertAll(itemDB.getRecords());
        options.clear();
        options.insertAll(itemDB.options.getRecords());
    }

    public void addRecordsFromFileDBExcept(List<Integer> itemIds) {
        for (ItemRecord record : files.getRecords()) {
            if (itemIds.contains(record.itemId) == false) {
                updateOrInsert(record);
                options.set(record.itemId, files.options.getRecordsBy(record.itemId), false);
            }
        }
    }

    public void downloadItemsIfNotExists(List<Integer> itemIds, boolean doSaveToFile) throws MyServiceFailureException {
        List<Integer> existingItemIds = getItemIds();
        List<Integer> notExistingItemIds = BaseUtils.clone(itemIds);
        notExistingItemIds.removeAll(existingItemIds);  // itemIds.removeAll을 하게 되면 원함수에 자료도 영향을 받는다
        for (int itemId : notExistingItemIds) {
            Pair<ItemRecord, List<ItemOptionRecord>> pair = downloadItemRecord(itemId);
            insert(pair.first);
            options.deleteByItemId(itemId);
            options.insertAll(pair.second);
            if (doSaveToFile) {
                files.updateOrInsert(pair.first);
                files.options.updateOrInsertFast(pair.first.itemId, pair.second);
            }
        }
    }

    public void addRecordIfNotExists(Map<ItemStockKey, Integer> traceCounts) {
        for (ItemStockKey key : traceCounts.keySet()) {
            int itemId = key.getItemId();
            if (has(itemId) == false) {
                addRecordFromFileDB(itemId);
            }
        }
    }

    private void addRecordFromFileDB(int itemId) {
        ItemRecord record = files.getRecordBy(itemId); // from file
        if (record.itemId == 0) {
            try {
                Pair<ItemRecord, List<ItemOptionRecord>> itemAndOptions = downloadItemRecord(itemId);
                record = itemAndOptions.first;
                save(record.toList());
                options.updateOrInsertAll(itemAndOptions.second);
                insert(record);
            } catch (MyServiceFailureException e) {
                String message = e.getLocalizedMessage();
                if (message.contains("[404]Not Found")) {
                    Log.w("DEBUG_JS", String.format("[MItemDB.addRecordIfNotExists] %s Deleted", itemId));
                } else {
                    MyUI.toast(context, ServiceErrorHelper.getMyServiceFailureMessageForUser(e, true));
                }
            }
        } else {
            insert(record);
        }
    }

    private void save(final List<ItemRecord> records) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                for (ItemRecord record : records) {
                    files.updateOrInsert(record);
                }
            }
        };
    }

    public MyMap<Integer, Integer> getStockMap() {
        return options.getStockMap();
    }

    public MyMap<Integer, Integer> getTraceStockMap() {
        return options.getTraceStockMap();
    }

    private Pair<ItemRecord, List<ItemOptionRecord>> downloadItemRecord(int itemId) throws MyServiceFailureException {
        SpecificItemDataBundle bundle = itemService.getSpecificItem(userSettingData.getUserShopInfo(), itemId);
        List<ItemOptionRecord> options = new ArrayList<>();
        for (ItemStockKey stockKey : bundle.getStock().keySet())
            options.add(new ItemOptionRecord(stockKey, bundle.getStock().get(stockKey)));
        return Pair.create(new ItemRecord(bundle.getItem()), options);
    }

    public List<ItemRecord> getRecordsBy(ItemCategoryType category) {
        List<ItemRecord> results = new ArrayList<>();
        for (ItemRecord record : getRecords())
            if (record.category == category)
                results.add(record);
        return results;
    }

    public void deleteByMonth(DateTime month) {
        List<ItemRecord> itemRecordsForDelete = new ArrayList<>();
        for (ItemRecord record : getRecords()) {
            if (BaseUtils.toMonth(record.createdDateTime).equals(month))
                itemRecordsForDelete.add(record);
        }
        deleteAll(itemRecordsForDelete);
    }
}
