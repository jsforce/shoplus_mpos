package com.teampls.shoplus.lib.dialog;

import android.app.ProgressDialog;
import android.content.Context;

import com.teampls.shoplus.lib.common.MyUI;

/**
 * Created by Medivh on 2019-07-03.
 */

public class MyProgressDialog extends ProgressDialog {
    private Context context;

    public MyProgressDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public void dismiss() {
        if (MyUI.isActiveActivity(context, "MyProgressDialog") == false) // 이미 사라진 activity에 붙어 있는 dialog임
            return;
        super.dismiss();
    }
}
