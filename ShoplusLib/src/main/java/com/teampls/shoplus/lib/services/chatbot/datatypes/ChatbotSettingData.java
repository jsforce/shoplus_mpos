package com.teampls.shoplus.lib.services.chatbot.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotAuthModeType;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOpenModeType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 챗봇 동작 모드 설정 데이터
 *
 * @author lucidite
 */
public class ChatbotSettingData implements JSONDataWrapper {
    private ChatbotAuthModeType authMode;
    private ChatbotOpenModeType openMode;

    public ChatbotSettingData(JSONObject dataObj) {
        boolean authModeValue = dataObj.optBoolean("auth_mode", ChatbotAuthModeType.DEFAULT_MODE.toRemoteValue());
        String openModeStr = dataObj.optString("open_mode", "");
        this.authMode = ChatbotAuthModeType.fromRemoteStr(authModeValue);
        this.openMode = ChatbotOpenModeType.fromRemoteStr(openModeStr);
    }

    public ChatbotSettingData(ChatbotAuthModeType authMode, ChatbotOpenModeType openMode) {
        this.authMode = authMode;
        this.openMode = openMode;
    }

    public ChatbotAuthModeType getAuthMode() {
        return authMode;
    }

    public void setAuthMode(ChatbotAuthModeType authMode) {
        this.authMode = authMode;
    }

    public ChatbotOpenModeType getOpenMode() {
        return openMode;
    }

    public void setOpenMode(ChatbotOpenModeType openMode) {
        this.openMode = openMode;
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return new JSONObject()
                .put("auth_mode", this.authMode.toRemoteValue())
                .put("open_mode", this.openMode.toRemoteStr());
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("ChatbotSettingData instance does not have a JSON array");
    }
}
