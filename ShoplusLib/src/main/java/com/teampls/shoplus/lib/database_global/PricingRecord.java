package com.teampls.shoplus.lib.database_global;

import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;

import org.joda.time.DateTime;

public class PricingRecord {
    public int id = 0, itemId = 0, unitPrice = 0;
    public String buyerPhoneNumber = "", workingShopPhoneNumber;
    public DateTime updatedDateTime = Empty.dateTime;

    public PricingRecord(){};

    public PricingRecord(int id, String workingShopPhoneNumber, String buyerPhoneNumber, int itemId, int unitPrice,
                              DateTime updatedDateTime) {
        this.id = id;
        this.workingShopPhoneNumber = workingShopPhoneNumber;
        this.buyerPhoneNumber = buyerPhoneNumber;
        this.itemId = itemId;
        this.unitPrice = unitPrice;
        this.updatedDateTime = updatedDateTime;
    }

    public PricingRecord(String workingShopPhoneNumber, String buyerPhoneNumber, int itemId) {
        this.workingShopPhoneNumber = workingShopPhoneNumber;
        this.buyerPhoneNumber = buyerPhoneNumber;
        this.itemId = itemId;
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %d, working %s, buyer %s, itemId %d, price %d, %s",
                callLocation, id, workingShopPhoneNumber, buyerPhoneNumber, itemId, unitPrice, updatedDateTime.toString(BaseUtils.fullFormat)));
    }
}
