package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.support.annotation.IdRes;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_memory.MAccountDB;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.PaymentMethod;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import com.teampls.shoplus.lib.view_module.MyCheckBox;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-05-23.
 */

abstract public class TransConfirmDialog extends BaseDialog {
    private int totalAmount = 0, billValue = 0;
    private MyRadioGroup<PaymentMode> paymentModes;
    private List<PaymentUiSet> paymentUiSets = new ArrayList<>();
    private MyCheckBox cbAutoBill;

    private enum PaymentMode {Single, Multiple}

    private TextView tvAmount, tvPayment, tvBill, tvReceivable;
    private AccountRecord accountRecord;
    private SlipSummaryRecord summaryRecord;

    public TransConfirmDialog(final Context context, SlipSummaryRecord summaryRecord, final AccountRecord accountRecord) {
        super(context);
        setDialogWidth(0.9, 0.7);
        this.accountRecord = accountRecord;
        this.summaryRecord = summaryRecord;

        tvAmount = findTextViewById(R.id.dialog_trans_confirm_amount_value);
        tvPayment = findTextViewById(R.id.dialog_trans_confirm_payment);
        tvBill = findTextViewById(R.id.dialog_trans_confirm_bill);
        tvReceivable = findTextViewById(R.id.dialog_trans_confirm_receivable);

        totalAmount = summaryRecord.amount;
        paymentModes = new MyRadioGroup(context, getView(), 1.0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearPaymentUiSetsChecked();
                getPaymentUiSet(PaymentMethod.Cash).cbSelected.setChecked(true); // 현금을 기본으로
                switch (paymentModes.getCheckedItem()) {
                    case Single:
                        MyUI.toastSHORT(context, String.format("전액 버튼을 눌러주세요"));
                        break;
                    case Multiple:
                        MyUI.toastSHORT(context, String.format("결제 방법을 모두 체크해 주세요"));
                        break;
                }
                refreshPayment();
            }
        });

        paymentModes.add(R.id.dialog_trans_confirm_payment_single, PaymentMode.Single).add(R.id.dialog_trans_confirm_payment_multiple, PaymentMode.Multiple);
        paymentModes.setChecked(PaymentMode.Single);
        paymentUiSets.add(new PaymentUiSet(PaymentMethod.Cash, R.id.dialog_trans_confirm_payment_cash_checkbox,
                R.id.dialog_trans_confirm_payment_cash, R.id.dialog_trans_confirm_payment_cash_clear, R.id.dialog_trans_confirm_payment_cashOnly));
        paymentUiSets.add(new PaymentUiSet(PaymentMethod.Online, R.id.dialog_trans_confirm_payment_online_checkbox,
                R.id.dialog_trans_confirm_payment_online, R.id.dialog_trans_confirm_payment_online_clear, R.id.dialog_trans_confirm_payment_onlineOnly));
        paymentUiSets.add(new PaymentUiSet(PaymentMethod.Entrust, R.id.dialog_trans_confirm_payment_entrust_checkbox,
                R.id.dialog_trans_confirm_payment_entrust, R.id.dialog_trans_confirm_payment_entrust_clear, R.id.dialog_trans_confirm_payment_entrustOnly));

        cbAutoBill = new MyCheckBox(context, (CheckBox) getView().findViewById(R.id.dialog_trans_confirm_auto_bill), GlobalDB.doRequestBillAutomatically);
        cbAutoBill.setOnClick(true, new MyOnClick<Boolean>() {
            @Override
            public void onMyClick(View view, Boolean autoRequest) {
                if (autoRequest) {
                    if (accountRecord.getReceivable() > 0)
                        billValue = accountRecord.getReceivable();
                    refreshBill();
                }
            }
        });
        setOnClick(R.id.dialog_trans_confirm_payment_cancel, R.id.dialog_trans_confirm_payment_apply,
                R.id.dialog_trans_confirm_receivable_refresh, R.id.dialog_trans_confirm_bill_value_input,
                R.id.dialog_trans_confirm_bill_value_all, R.id.dialog_trans_confirm_bill);

        refreshPayment();
        refreshBill();
        show();

        MyView.setTextViewByDeviceSize(context, getView(),
                R.id.dialog_trans_confirm_amount_title, R.id.dialog_trans_confirm_amount_value,
                R.id.dialog_trans_confirm_payment_title, R.id.dialog_trans_confirm_payment,
                R.id.dialog_trans_confirm_payment_cash_title, R.id.dialog_trans_confirm_payment_online_title, R.id.dialog_trans_confirm_payment_entrust_title,
                R.id.dialog_trans_confirm_bill_title, R.id.dialog_trans_confirm_receivable_message
        );
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_trans_confirm;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_trans_confirm_payment_cancel) {
            dismiss();

        } else if (view.getId() == R.id.dialog_trans_confirm_bill_value_input || view.getId() == R.id.dialog_trans_confirm_bill) {
            new UpdateCurrencyDialog(context, "잔금청구", "청구할 금액을 입력해주세요", 0, accountRecord.getReceivable()) {
                @Override
                public void onApplyClick(int newValue) {
                    billValue = newValue;
                    refreshBill();
                }
            };

        } else if (view.getId() == R.id.dialog_trans_confirm_receivable_refresh) {
            new CommonServiceTask(context, "TransConfirmDialog", "잔금 확인중...") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    AccountsData data = accountService.getAccounts(userSettingData.getUserShopInfo(), userSettingData.getUserConfigType(),
                            accountRecord.counterpartPhoneNumber);
                    accountRecord = new AccountRecord(accountRecord.counterpartPhoneNumber, data, accountRecord.name);
                    MAccountDB.getInstance(context).updateOrInsert(accountRecord);
                }

                @Override
                public void onPostExecutionUI() {
                    if (cbAutoBill.isChecked() && accountRecord.getReceivable() > 0)
                        billValue = accountRecord.getReceivable(); // 현재값을 바로 반영해야 함
                    refreshBill();
                }
            };

        } else if (view.getId() == R.id.dialog_trans_confirm_bill_value_all) {
            billValue = accountRecord.getReceivable();
            refreshBill();

        } else if (view.getId() == R.id.dialog_trans_confirm_payment_apply) {
            final int cashPayment = getPaymentUiSet(PaymentMethod.Cash).getValue();
            final int onlinePayment = getPaymentUiSet(PaymentMethod.Online).getValue();
            final int entrustPayment = getPaymentUiSet(PaymentMethod.Entrust).getValue();
            if (totalAmount != (cashPayment + onlinePayment + entrustPayment)) {
                new MyAlertDialog(context, "금액 확인", String.format("합산 금액이 맞지 않습니다.\n\n - 총액 : %s\n - 합산액 : %s",
                        BaseUtils.toCurrencyStr(totalAmount), BaseUtils.toCurrencyStr(cashPayment + onlinePayment + entrustPayment)),
                        "다시 확인", null) {
                    @Override
                    public void yes() {
                    }
                };
            } else {
                dismiss();
                onApplyClick(onlinePayment, entrustPayment, billValue);
            }
        }
    }

    abstract public void onApplyClick(int onlinePayment, int entrustPayment, int billValue);

    private List<PaymentUiSet> getSelectedUiSets() {
        List<PaymentUiSet> results = new ArrayList<>();
        for (PaymentUiSet paymentUiSet : paymentUiSets)
            if (paymentUiSet.cbSelected.isChecked())
                results.add(paymentUiSet);
        return results;
    }

    private void clearPaymentUiSetsChecked() {
        for (PaymentUiSet paymentUiSet : paymentUiSets)
            paymentUiSet.cbSelected.setChecked(false);
    }

    private PaymentUiSet getPaymentUiSet(PaymentMethod paymentMethod) {
        for (PaymentUiSet paymentUiSet : paymentUiSets)
            if (paymentUiSet.paymentMethod == paymentMethod)
                return paymentUiSet;
        Log.e("DEBUG_JS", String.format("[TransConfirmDialog.getPaymentUiSet] not found : %s", paymentMethod.toString()));
        return new PaymentUiSet();
    }

    private void refreshBill() {
        tvReceivable.setText(String.format("잔금: %s", BaseUtils.toCurrencyOnlyStr(accountRecord.getReceivable())));
        tvBill.setText(BaseUtils.toCurrencyStr(billValue));
        if (billValue > accountRecord.getReceivable() && accountRecord.getReceivable() > 0)
            MyUI.toastSHORT(context, String.format("[주의] 청구액 확인"));
        refreshAmount();
    }

    private void refreshAmount() {
        tvAmount.setText(BaseUtils.toCurrencyStr(totalAmount + billValue));
        tvAmount.setTextColor(billValue == 0 ? ColorType.Black.colorInt : ColorType.Blue.colorInt);
    }

    private void refreshPayment() {
        tvPayment.setText(BaseUtils.toCurrencyStr(totalAmount));

        for (PaymentUiSet paymentUiSet : paymentUiSets)
            paymentUiSet.setTextWatcher(false);

        // set UI and default value
        List<PaymentUiSet> checkedUiSets = new ArrayList<>();
        for (PaymentUiSet paymentUiSet : paymentUiSets) {
            paymentUiSet.setUiByPaymentMode(paymentModes.getCheckedItem());
            if (paymentUiSet.cbSelected.isChecked())
                checkedUiSets.add(paymentUiSet);
            else
                paymentUiSet.etValue.setText(""); // 값을 0으로 변경하고 값 산정에서 제외
        }

        // set Value
        for (PaymentUiSet checkedUiSet : checkedUiSets) {
            int notZeroValueCount = 0;
            int otherValueSum = 0;
            for (PaymentUiSet _paymentUiSet : checkedUiSets) {
                if (_paymentUiSet.paymentMethod != checkedUiSet.paymentMethod)
                    otherValueSum = otherValueSum + _paymentUiSet.getValue();
                if (_paymentUiSet.getValue() != 0)
                    notZeroValueCount++;
            }
            switch (checkedUiSets.size()) {
                case 1:
                    checkedUiSet.etValue.setText(BaseUtils.toCurrencyOnlyStr(totalAmount));
                    break;
                case 2: // 배타적 모드
                    if (checkedUiSet.etValue.hasFocus() == false)
                        checkedUiSet.etValue.setText(BaseUtils.toCurrencyOnlyStr(totalAmount - otherValueSum));
                    break;
                case 3: // 배타적 모드를 만들 수 없다. 2개에 값이 있고 하나만 0인 상황을 임시 배타적이라고 보고 힌트를 주자
                    if (notZeroValueCount == 2 && checkedUiSet.getValue() == 0)
                        checkedUiSet.etValue.setHint(BaseUtils.toCurrencyOnlyStr(totalAmount - otherValueSum));
                    break;
            }
        }

        for (PaymentUiSet paymentUiSet : paymentUiSets)
            paymentUiSet.setTextWatcher(true);

        refreshAmount();
    }

    class PaymentUiSet {
        private CheckBox cbSelected;
        private EditText etValue;
        private TextView tvOnly;
        public PaymentMethod paymentMethod;
        private ImageView ivClear;
        private MyTextWatcher textWatcher = new MyTextWatcher();

        public PaymentUiSet() {
        }

        public PaymentUiSet(final PaymentMethod paymentMethod, @IdRes int checkBoxResId,
                            @IdRes int editTextResId, @IdRes int imageViewResId, @IdRes int textViewResId) {
            this.paymentMethod = paymentMethod;
            cbSelected = (CheckBox) findViewById(checkBoxResId);
            cbSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    refreshPayment();
                }
            });
            etValue = (EditText) findViewById(editTextResId);
            etValue.addTextChangedListener(textWatcher);
            etValue.setFocusable(false);
            tvOnly = (TextView) findViewById(textViewResId);
            tvOnly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clearPaymentUiSetsChecked();
                    cbSelected.setChecked(true); // 나만 체크된 것으로
                    refreshPayment();
                }
            });
            ivClear = (ImageView) findViewById(imageViewResId);
            ivClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<PaymentUiSet> selectedUiSets = getSelectedUiSets();
                    if (selectedUiSets.size() == 1 && selectedUiSets.get(0).paymentMethod == paymentMethod) {
                        MyUI.toastSHORT(context, String.format("1개 방법만 선택되어 있습니다"));
                        return; // 1개만 체크되어 있는데 그걸 clear 하면 동작할 필요가 없다
                    }
                    textWatcher.activated = false;
                    etValue.setText("");
                    etValue.requestFocus();
                    textWatcher.activated = true;
                    refreshPayment();
                }
            });
            MyView.setTextViewByDeviceSize(context, cbSelected, etValue, tvOnly);
        }

        public void setUiByPaymentMode(PaymentMode paymentMode) {
            switch (paymentMode) {
                case Single:
                    etValue.setFocusable(false);
                    cbSelected.setVisibility(View.GONE);
                    ivClear.setVisibility(View.GONE);
                    tvOnly.setVisibility(View.VISIBLE);
                    break;
                case Multiple:
                    etValue.setHint("0");
                    etValue.setFocusableInTouchMode(cbSelected.isChecked());
                    etValue.setFocusable(cbSelected.isChecked());
                    cbSelected.setVisibility(View.VISIBLE);
                    ivClear.setVisibility(View.VISIBLE);
                    tvOnly.setVisibility(View.GONE);
                    break;
            }
        }

        public int getValue() {
            return BaseUtils.toCurrencyInt(etValue.getText().toString());
        }

        public void setTextWatcher(boolean value) {
            textWatcher.activated = value;
        }

        class MyTextWatcher extends BaseTextWatcher {
            public boolean activated = true;

            @Override
            public void afterTextChanged(Editable s) {
                if (activated == false) return;
                refreshPayment();
            }
        }

    }
}
