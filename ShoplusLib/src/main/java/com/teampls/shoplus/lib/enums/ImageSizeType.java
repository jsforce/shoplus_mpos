package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2017-01-17.
 */

public enum ImageSizeType {
    ORIGINAL, THUMB;
}
