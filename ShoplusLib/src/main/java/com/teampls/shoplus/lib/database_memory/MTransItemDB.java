package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

/**
 * Created by Medivh on 2018-04-18.
 */

public class MTransItemDB extends MemorySlipItemDB {

    private static MTransItemDB instance;

    public static MTransItemDB getInstance(Context context) {
        if (instance == null)
            instance = new MTransItemDB(context);
        return instance;
    }

    private MTransItemDB(Context context) {
        super(context, "MTransItemDB");
    }
}
