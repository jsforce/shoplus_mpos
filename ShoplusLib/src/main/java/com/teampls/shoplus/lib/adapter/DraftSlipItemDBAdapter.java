package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.List;

import static com.teampls.shoplus.lib.adapter.BaseSlipItemDBAdapter.textViewRatio;

/**
 * Created by Medivh on 2017-08-23.
 */

public class DraftSlipItemDBAdapter extends BaseDBAdapter<SlipItemRecord> {
    protected List<String> noQuantityKeywords = new ArrayList<>();

    public DraftSlipItemDBAdapter(Context context) {
        super(context);
        noQuantityKeywords = globalDB.getNoQuantityKeywords();
    }

    @Override
    public void generate() {

    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new DraftSlipViewHolder();
    }

    public class DraftSlipViewHolder extends BaseViewHolder {
        private TextView tvType, tvName, tvUnitPrice, tvQuantity, tvSum, tvSerial;

        @Override
        public int getThisView() {
            return R.layout.row_slip_item_creation;
        }

        @Override
        public void init(View view) {
            tvType = (TextView) view.findViewById(R.id.row_slip_type);
            tvName = (TextView) view.findViewById(R.id.row_slip_name);
            tvUnitPrice = (TextView) view.findViewById(R.id.row_slip_unitPrice);
            tvQuantity = (TextView) view.findViewById(R.id.row_slip_quantity);
            tvSum = (TextView) view.findViewById(R.id.row_slip_sum);
            tvSerial = (TextView) view.findViewById(R.id.row_slip_serial);
            MyView.setTextViewByDeviceSizeScale(context, textViewRatio, tvType, tvName, tvUnitPrice, tvQuantity, tvSum);
        }


        @Override
        public void update(int position) {
            if (position >= getCount()) return;
            String delimiter = ":";
            SlipItemRecord record = records.get(position);
            tvType.setText(record.slipItemType.toMultiUiName());
            tvType.setTextColor(record.slipItemType.getColorInt());
            tvSerial.setText(String.format("%d", record.serial + 1));
            String nameString = (record.name.isEmpty() ? "(미입력)" : record.name);

            String colorString = (record.color == ColorType.Default) || (record.getColorName().isEmpty()) ? "" : String.format(" %s %s", delimiter, record.getColorName());
            String sizeString = (record.size == SizeType.Default) ? "" : String.format(" %s %s", delimiter, record.size.uiName);
            tvName.setText(nameString + colorString + sizeString);
            tvUnitPrice.setText(BaseUtils.toCurrencyStr(record.unitPrice).replace("원", ""));
            tvQuantity.setText(String.format("x %d", record.quantity));
            tvSum.setText(record.slipItemType.getSign() == 0 ? "" : record.slipItemType.getSignStr() + BaseUtils.toCurrencyStr(record.unitPrice * record.quantity).replace("원", ""));

            // 수량 표시 제거
            if (noQuantityKeywords.contains(record.name)) {
                tvType.setText("");
                tvUnitPrice.setText("");
                tvQuantity.setText("");
            }

            if (SlipItemType.getNoQuantityTypes().contains(record.slipItemType)) {
                tvUnitPrice.setText("");
                tvQuantity.setText("");
            }
        }
    }
}
