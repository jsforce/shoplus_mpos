package com.teampls.shoplus.lib.template;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-09-18.
 */
abstract public class TemplateDBAdapter extends BaseDBAdapter {
    protected TemplateDB templateDB;
    protected List<TemplateRecord> records = new ArrayList<>(0);
    protected Enum<?> sortingColumn = TemplateDB.Column.createdDateTime;
    protected boolean isDescending = false;
    protected ViewHolder viewHolder;
    public enum RowType {Default, Checkable}
    protected RowType rowType = RowType.Default;

    public TemplateDBAdapter(Context context) {
        super(context);
        templateDB = TemplateDB.getInstance(context);
    }

    public List<TemplateRecord> getRecords() {
        return records;
    }

    public TemplateRecord getRecord(int position) {
        if (position >= records.size() || position < 0)
            return new TemplateRecord();
        return records.get(position);
    }

    public TemplateDBAdapter setRowType(RowType rowType) {
        this.rowType = rowType;
        return this;
    }

    @Override
    public void generate() {

        ArrayList<TemplateRecord> _records = new ArrayList<>(0);
        for (int id : templateDB.getIdsOrderBy(sortingColumn, isDescending)) {
            TemplateRecord record = templateDB.getRecord(id);
            _records.add(record);
        }
        records = _records;
    }

    @Override
    public int getCount() {
        return records.size();
    }

    @Override
    public Object getItem(int position) {
        return getRecord(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            switch (rowType) {
                case Default:
                    convertView = inflater.inflate(DefaultViewHolder.thisView, null);
                    viewHolder = new DefaultViewHolder(convertView);
                    break;
                case Checkable:
                    convertView = inflater.inflate(CheckableViewHolder.thisView, null);
                    viewHolder = new CheckableViewHolder(convertView);
                    break;
            }
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.update(position);
        return convertView;
    }

   interface ViewHolder {
        void update(int position);
    }

    class DefaultViewHolder implements ViewHolder {
        public ImageView icon;
        public TextView name, phoneNumber;
        public static final int thisView = 0;

        public DefaultViewHolder(View view) {
            name = (TextView) view.findViewById(0);
            phoneNumber = (TextView) view.findViewById(0);
            icon = (ImageView) view.findViewById(0);
        }

        public void update(int position) {
            TemplateRecord record = records.get(position);
        }
    }

    class CheckableViewHolder implements ViewHolder {
        public CheckBox checkBox;
        public TextView name, phoneNumber;
        public static final int thisView = 0;

        public CheckableViewHolder(View view) {
            name = (TextView) view.findViewById(0);
            phoneNumber = (TextView) view.findViewById(0);
            checkBox = (CheckBox) view.findViewById(0);
        }

        public void update(int position) {
            TemplateRecord record = records.get(position);
        }
    }
}
