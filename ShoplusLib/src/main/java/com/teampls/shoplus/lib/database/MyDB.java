package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.Record;
import com.amazonaws.mobileconnectors.cognito.SyncConflict;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.teampls.shoplus.lib.awsservice.cognito.CognitoUserDataManager;
import com.teampls.shoplus.lib.awsservice.cognito.datasetenum.CognitoTransactionFields;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_map.KeyValueDB;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-09-03.
 */
public class MyDB extends BaseDB<MyRecord> {
    private static MyDB instance = null;
    private static int Ver = 1;
    private KeyValueDB keyValueDB;

    public enum Column {
        _id, username, phone_number, nickname, password, confirmed;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    private MyDB(Context context) {
        super(context, "MyDB", "MyListTable", Ver, Column.toStrs());
        keyValueDB = KeyValueDB.getInstance(context);
    }

    public static MyDB getInstance(Context context) {
        if (instance == null)
            instance = new MyDB(context);
        return instance;
    }

    public void resetPassword(String phoneNumber, String newPassword, boolean confirmed) {
        logOut();
        register(new MyRecord(phoneNumber, newPassword, true));
    }

    public void logOut() {
        for (MyApp myApp : MyApp.values()) {
            try {
                Context otherContext = context.createPackageContext(myApp.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
                DBHelper otherHelper = new DBHelper(otherContext);
                SQLiteDatabase otherDatabase = otherHelper.getWritableDatabase();
                // DELETE
                otherDatabase.execSQL(MyQuery.delete(DB_TABLE));
                otherHelper.onCreate(otherDatabase);
                Log.w("DEBUG_JS", String.format("[%s] delete DB : %s", DB_NAME, otherContext.getPackageName()));
                // TODO 삭제당한 앱에서는 이를 인지하고 앱을 종료해야 한다
            } catch (PackageManager.NameNotFoundException e) {
                // not found
            } catch (SQLException e) {
                // package는 있지만 myDB가 없는 경우
                Log.e("DEBUG_JS", String.format("[MyDB.resetPassword] %s, %s", myApp.getPackageName(), e.getLocalizedMessage()));
            }
        }
    }

    public void open() {
        try {
            helper = new DBHelper(context);
            database = helper.getWritableDatabase();
            if (confirmed())
                return;

            boolean confirmedFound = false;
            MyRecord otherRecord = new MyRecord();
            Context otherContext = context;
            for (MyApp myApp : MyApp.values()) {
                try {
                    otherContext = context.createPackageContext(myApp.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
                    DBHelper otherHelper = new DBHelper(otherContext);
                    SQLiteDatabase otherDatabase = otherHelper.getWritableDatabase();
                    Cursor cursor = otherDatabase.query(DB_TABLE, columns, null, null, null, null, null);
                    if (cursor != null && cursor.getCount() >= 1)
                        cursor.moveToFirst();
                    otherRecord = getRecord(cursor);
                    if (otherRecord.confirmed) {
                        confirmedFound = true;
                        break;
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    // packageName이 없으면 바로 여기로 이동
                } catch (SQLException e) {
                    // package는 있지만 myDB가 없는 경우
                    Log.e("DEBUG_JS", String.format("[MyDB.open] %s, %s", myApp.getPackageName(), e.getLocalizedMessage()));
                }
            }

            if (confirmedFound) {
                Log.i("DEBUG_JS", String.format("[MyDB.open] copy from %s", otherContext.getPackageName()));
                register(otherRecord); // copy from confirmed other MyDB
            }
        } catch (SQLException e) {
            Log.e("DEBUG_JS", String.format("[%s] fail to open %s, ver. %d", getClass().getSimpleName(), DB_TABLE, DB_VERSION));
        }
    }

    protected ContentValues toContentvalues(MyRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.username.toString(), record.username);
        contentvalues.put(Column.phone_number.toString(), record.phoneNumber);
        contentvalues.put(Column.nickname.toString(), record.nickname);
        contentvalues.put(Column.password.toString(), record.password);
        contentvalues.put(Column.confirmed.toString(), record.confirmed);
        return contentvalues;
    }

    @Override
    protected void toLogCat(int id, MyRecord record, int option, String operation) {
        Log.i("DEBUG_JS", String.format("[%s] %s, %s, %s, %s", getClass().getSimpleName(),
                record.username, record.phoneNumber, record.nickname, record.password));
    }

    @Override
    protected int getId(MyRecord record) {
        return record.id;
    }

    @Override
    protected MyRecord getEmptyRecord() {
        return new MyRecord();
    }

    @Override
    protected MyRecord createRecord(Cursor cursor) {
        return new MyRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.username.ordinal()),
                cursor.getString(Column.phone_number.ordinal()),
                cursor.getString(Column.nickname.ordinal()),
                cursor.getString(Column.password.ordinal()),
                BaseUtils.toBoolean(cursor.getInt(Column.confirmed.ordinal()))
        );
    }

    public void toLogCat(String callLocation) {
        for (MyRecord record : getRecords())
            record.toLogCat(callLocation);
        if (getRecords().isEmpty())
            Log.w("DEBUG_JS", String.format("[%s] empty", callLocation));
    }

    public void register(MyRecord record) {
        if (isOpen() == false) open();
        switch (this.getSize()) {
            default:
                new Exception(String.format("[%s.register] invalid size %d", getClass().getSimpleName(), this.getSize()));
                break;
            case 0:
                insert(record);
                break;
            case 1:
                update(1, record);
                break;
        }
    }

    public boolean registered() {
        if (isOpen() == false) open();
        return ((getSize() == 1) && (getPrivateNumber().isEmpty() == false));
    }

    public MyRecord getMyRecord() {
        if (registered() == false) {
            return new MyRecord();
        }
        return getRecord(1);
    }

    public String getUsername() {
        return this.getString(Column.username, 1);
    }

    public String getPrivateNumber() {
        if (isOpen() == false) open();
        return this.getString(Column.phone_number, 1);
    }

    public String getPrivateName() {
        if (isOpen() == false) open();
        return this.getString(Column.nickname, 1);
    }

    public String getPassword() {
        if (isOpen() == false) open();
        return this.getString(Column.password, 1);
    }

    public boolean confirmed() {
        if (isOpen() == false) open();
        if (registered()) {
            return BaseUtils.toBoolean(this.getInt(Column.confirmed, 1));
        } else {
            return false;
        }
    }

    public String getPasswordAsterisk() {
        String result = "";
        int startInx = 0;
        if (getPassword().length() >= 4) {
            result = getPassword().substring(0, 1);
            startInx = 1;
        }
        for (int i = startInx; i < getPassword().length(); i++) {
            result = result + "*";
        }
        return result;
    }

    public String getUserPoolNumber() {
        String COUNTRY_CODE_KR = "+82";
        if (getPrivateNumber().isEmpty())
            return COUNTRY_CODE_KR + "";
        else
            return COUNTRY_CODE_KR + getPrivateNumber().substring(1);
    }

    private List<String> getShopPhones() {
        List<String> results = new ArrayList<>();
        results.add(keyValueDB.getValue("shop.phone1"));
        results.add(keyValueDB.getValue("shop.phone2"));
        return results;
    }

    public List<String> getShopPhoneNumbers() {
        return getShopPhones();
    }

    @Deprecated
    private void updateShopPhones(List<String> phones) {
        if (phones.size() != 2) {
            Log.e("DEBUG_JS", String.format("[MyDB.updateShopPhones] size %d", phones.size()));
            return;
        }
        keyValueDB.put("shop.phone1", phones.get(0));
        keyValueDB.put("shop.phone2", phones.get(1));
    }

    private List<String> getShopAccounts() {
        List<String> results = new ArrayList<>();
        results.add(keyValueDB.getValue("shop.account1"));
        results.add(keyValueDB.getValue("shop.account2"));
        results.add(keyValueDB.getValue("shop.account3"));
        return results;
    }

    public List<String> getAccounts() {
        return getShopAccounts();
    }

    @Deprecated
    private void updateShopAccounts(List<String> accounts) {
        if (accounts.size() != 3) {
            Log.e("DEBUG_JS", String.format("[MyDB.updateShopAccounts] size %d", accounts.size()));
            return;
        }
        keyValueDB.put("shop.account1", accounts.get(0));
        keyValueDB.put("shop.account2", accounts.get(1));
        keyValueDB.put("shop.account3", accounts.get(2));
    }

    private List<String> getShopAddress() {
        List<String> results = new ArrayList<>();
        results.add(keyValueDB.getValue("shop.address"));
        results.add(keyValueDB.getValue("shop.location"));
        return results;
    }

    @Deprecated
    private void updateShopAddress(List<String> addresses) {
        if (addresses.size() != 2) {
            Log.e("DEBUG_JS", String.format("[MyDB.updateShopAddress] size %d", addresses.size()));
            return;
        }
        keyValueDB.put("shop.address", addresses.get(0));
        keyValueDB.put("shop.location", addresses.get(1));
    }

    public String getBuildingAdd() {
        return getShopAddress().get(0);
    }

    public String getDetailAdd() {
        return getShopAddress().get(1);
    }

    private String getShopMemo() {
        return keyValueDB.getValue("shop.memo");
    }

    public String getNotice() {
        return getShopMemo();
    }

    @Deprecated
    private void updateShopMemo(String memo) {
        keyValueDB.put("shop.memo", memo);
    }

    public void refresh(CognitoUserDataManager manager) {
        MyRecord record = getMyRecord();
        String shopName = manager.getTransactionBaseData(CognitoTransactionFields.SHOPNAME);
        if (shopName.isEmpty()) {
            // 가입시 UI에서 받은 이름을 서버에 올린다
            uploadShopDataToServer(CognitoTransactionFields.SHOPNAME, record.nickname);
        } else {
            record.nickname = shopName;
            register(record);
        }

        List<String> phones = new ArrayList<>();
        phones.add(manager.getTransactionBaseData(CognitoTransactionFields.PHONE_1));
        phones.add(manager.getTransactionBaseData(CognitoTransactionFields.PHONE_2));
        updateShopPhones(phones);

        List<String> addresses = new ArrayList<>();
        addresses.add(manager.getTransactionBaseData(CognitoTransactionFields.BUILDING_ADDR));
        addresses.add(manager.getTransactionBaseData(CognitoTransactionFields.SHOP_ADDR));
        updateShopAddress(addresses);

        List<String> accounts = new ArrayList<>();
        accounts.add(manager.getTransactionBaseData(CognitoTransactionFields.BANK_ACCOUNT_1));
        accounts.add(manager.getTransactionBaseData(CognitoTransactionFields.BANK_ACCOUNT_2));
        accounts.add(manager.getTransactionBaseData(CognitoTransactionFields.BANK_ACCOUNT_3));
        updateShopAccounts(accounts);

        updateShopMemo(manager.getTransactionBaseData(CognitoTransactionFields.COMMENTS));

    }

    @Deprecated
    private void uploadShopDataToServer(final CognitoTransactionFields field, final String value) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                CognitoUserDataManager.getInstance().setTransactionBaseData(field, value, callback);
            }
        };
    }

    Dataset.SyncCallback callback = new Dataset.SyncCallback() {
        @Override
        public void onSuccess(Dataset dataset, List<Record> updatedRecords) {
        }

        @Override
        public boolean onConflict(Dataset dataset, List<SyncConflict> conflicts) {
            return false;
        }

        @Override
        public boolean onDatasetDeleted(Dataset dataset, String datasetName) {
            return false;
        }

        @Override
        public boolean onDatasetsMerged(Dataset dataset, List<String> datasetNames) {
            return false;
        }

        @Override
        public void onFailure(DataStorageException dse) {
            MyUI.toastSHORT(context, String.format("서버에 저장하지 못했습니다\n%s", dse.getLocalizedMessage()));
        }
    };

}