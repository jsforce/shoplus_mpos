package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseUserDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyKeyboard;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-04-15.
 */

public class MyUserMergerView extends BaseActivity implements AdapterView.OnItemClickListener {
    private BaseUserDBAdapter adapter;
    private ListView listView;
    protected EditText etName;
    protected MyKeyboard myKeyboard;
    private List<UserRecord> fullRecords;
    private static String keyword = "";
    private List<UserRecord> selectedRecords = new ArrayList<>();
    private static MyOnTask<List<UserRecord>> onUserMerged;

    public static void startActivity(Context context, String keyword, MyOnTask<List<UserRecord>> onUserMerged) {
        MyUserMergerView.keyword = keyword;
        MyUserMergerView.onUserMerged = onUserMerged;
        context.startActivity(new Intent(context, MyUserMergerView.class));
    }

    @Override
    public int getThisView() {
        return R.layout.base_user_search;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        userDB = UserDB.getInstance(context);
        userDB.cleanUp();

        etName = (EditText) findViewById(R.id.user_search_name);
        if (MyDevice.getAndroidVersion() < 27)
            etName.setHint("이름 또는 전화번호");
        etName.addTextChangedListener(new MyTextWatcher());
        adapter = new BaseUserDBAdapter(context) {
            @Override
            public BaseViewHolder getViewHolder() {
                return new DefaultCheckableViewHolder() {
                    public void update(int position) {
                        super.update(position);
                        if (selectedRecords.contains(record)) {
                            rbChecked.setChecked(true);
                            container.setBackgroundColor(ColorType.Beige.colorInt);
                        } else {
                            rbChecked.setChecked(false);
                            container.setBackgroundColor(Color.TRANSPARENT);
                        }
                    }
                };
            }
        };
        adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_MULTIPLE);

        listView = (ListView) findViewById(R.id.user_search_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);

        myKeyboard = new MyKeyboard(context, findViewById(R.id.user_search_container), etName);
        setOnClick(R.id.user_search_clear);
        initFullRecords();

        etName.setText(keyword);
        if (keyword.isEmpty())
            myKeyboard.show();

        setVisibility(View.GONE, R.id.user_search_add);
        MyView.setTextViewByDeviceSize(context, getView(), R.id.user_search_name, R.id.user_search_add);
    }

    private void initFullRecords() {
        adapter.setOrderRecord(UserDB.Column.name, false);
        adapter.generate();
        fullRecords = adapter.getRecords();
    }

    public void refresh() {
        if (adapter == null) return;
        adapter.generate(); // 사실상 동작하지 않음
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.user_search_clear) {
            etName.setText("");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserRecord record = adapter.getRecord(position);
        if (record.isGuest(userSettingData)) {
            MyUI.toastSHORT(context, String.format("[고객]은 합치기를 하실 수 없습니다"));
            return;
        }
        if (record.isRetail(userSettingData)) {
            MyUI.toastSHORT(context, String.format("[소매]는 합치기를 하실 수 없습니다"));
            return;
        }
        if (selectedRecords.contains(record))
            selectedRecords.remove(record);
        else
            selectedRecords.add(record);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.mergeSelected)
                .add(MyMenu.keyboard)
                .add(MyMenu.close);
    }

    private void mergeUsers(final UserRecord validRecord, final List<UserRecord> invalidRecords) {
        new CommonServiceTask(context, "MyUserMergerView", "거래처 정보 통합중...") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                for (UserRecord invalidRecord : invalidRecords) {
                    setProgressMessage(String.format("%s 통합 중...", invalidRecord.name));
                    workingShopService.mergeContacts(userSettingData.getUserShopInfo(), validRecord.phoneNumber, invalidRecord.phoneNumber);
                    userDB.delete(invalidRecord.phoneNumber);
                    Log.w("DEBUG_JS", String.format("[MyUserMergerView] (%s, %s) <-- (%s, %s)", validRecord.name, validRecord.phoneNumber, invalidRecord.name, invalidRecord.phoneNumber));
                }
                fullRecords = userDB.getRecordsOrderBy(UserDB.Column.name, false);
                adapter.setRecords(fullRecords);
            }

            @Override
            public void onPostExecutionUI() {
                super.onPostExecutionUI();
                selectedRecords.clear();
                refresh();
                MyUI.toastSHORT(context, String.format("통합 결과는 재시작후 확인하실 수 있습니다"));
                if (onUserMerged != null)
                    onUserMerged.onTaskDone(invalidRecords);
            }

        };
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case mergeSelected:
                if (selectedRecords.size() < 2) {
                    MyUI.toastSHORT(context, String.format("최소 2명이상 선택하셔야 합니다"));
                    return;
                }
                new SelectTargetUserDialog(context, selectedRecords) {
                    @Override
                    public void onApplyClick(UserRecord selectedRecord, List<UserRecord> unselectedRecords) {
                        mergeUsers(selectedRecord, unselectedRecords);
                        dismiss();
                    }
                };
                break;
            case keyboard:
                myKeyboard.switchKeyboard();
                break;
            case close:
                myKeyboard.hide();
                finish();
                break;
        }
    }

    class MyTextWatcher extends BaseTextWatcher {
        @Override
        public void afterTextChanged(Editable s) {
            List<UserRecord> results = new ArrayList<>();
            results.addAll(selectedRecords);
            String keyword = s.toString().toLowerCase();
            for (UserRecord record : fullRecords) {
                if (selectedRecords.contains(record))
                    continue;
                if (record.name.replace("*", "").toLowerCase().contains(keyword) || record.phoneNumber.contains(keyword))
                    results.add(record);
            }
            adapter.setRecords(results);
            refresh();
        }
    }

    abstract class SelectTargetUserDialog extends BaseDialog implements AdapterView.OnItemClickListener {
        private ListView lvSelectedUsers;
        private BaseUserDBAdapter adapter;

        public SelectTargetUserDialog(Context context, List<UserRecord> selectedUsers) {
            super(context);
            setDialogWidth(0.9, 0.6);

            adapter = new BaseUserDBAdapter(context) {
                @Override
                public BaseViewHolder getViewHolder() {
                    return new DefaultCheckableViewHolder();
                }
            };
            adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_SINGLE);
            adapter.setRecords(selectedUsers);
            findTextViewById(R.id.dialog_select_user_title, "사용하실 거래처 선택");
            lvSelectedUsers = (ListView) findViewById(R.id.dialog_select_user_listview);
            lvSelectedUsers.setAdapter(adapter);
            lvSelectedUsers.setOnItemClickListener(this);
            setOnClick(R.id.dialog_select_user_close, R.id.dialog_select_user_apply);
            show();
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_select_user;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_select_user_close) {
                myKeyboard.hide();
                dismiss();
            } else if (view.getId() == R.id.dialog_select_user_apply) {
                final List<UserRecord> selectedRecords = adapter.getSortedClickedRecords();
                final List<UserRecord> unselectedRecords = adapter.getRecords();
                if (selectedRecords.size() <= 0) {
                    MyUI.toastSHORT(context, String.format("앞으로 사용하실 거래처를 선택해 주세요"));
                    return;
                }

                String notice = String.format("합치면 되돌릴 수 없습니다.\n\n선택하신 [%s]로 합치시겠습니까?", selectedRecords.get(0).name);
                new MyAlertDialog(context, String.format("%s 선택", selectedRecords.get(0).name), notice) {
                    @Override
                    public void yes() {
                        unselectedRecords.remove(selectedRecords.get(0));
                        onApplyClick(selectedRecords.get(0), unselectedRecords);
                    }

                };
            }
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            adapter.onItemClick(view, position);
        }

        abstract public void onApplyClick(UserRecord selectedRecord, List<UserRecord> unselectedRecords);
    }


}
