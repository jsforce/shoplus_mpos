package com.teampls.shoplus.lib.view;

import android.os.Handler;
import android.os.Message;
import android.widget.ProgressBar;

public class MyProgressBarTime {
	private ProgressBar progressBar;
	private volatile Thread progressBarThread;
	private int currentPos = 0;
	private int totalTime = 0;
	
	public MyProgressBarTime(ProgressBar progressBar, int totalTime) {
		this.progressBar = progressBar;
		this.totalTime = totalTime;
		progressBar.setMax(totalTime);
	}
	
	public void start() {
		if (progressBarThread == null) {
			progressBarThread = new Thread(null,backgroundThread,"startProgressBarThread");
			currentPos = 0;
			progressBarThread.start();
		}	
	}
	
	public synchronized void stop() {
		if (progressBarThread != null) {
			Thread tmpThread = progressBarThread;
			progressBarThread = null;
			tmpThread.interrupt();
		}
		//progressBar.setVisibility(ProgressBar.GONE);		
	}

	private Runnable backgroundThread = new Runnable() {
		@Override
		public void run() {
			if (Thread.currentThread() == progressBarThread) {
				currentPos = 0;
				while (currentPos < totalTime) {
					try {
						progressBarHandle.sendMessage(progressBarHandle.obtainMessage());
						Thread.sleep(100);
					} catch (final InterruptedException e) {
						return;
					} catch (final Exception e) {
						return;
					}
				}
			}
		}

		Handler progressBarHandle = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				currentPos = currentPos + 100;
				progressBar.setProgress(currentPos);
				if (currentPos == totalTime) {
					stop();
				}
			}
		};
	};
}