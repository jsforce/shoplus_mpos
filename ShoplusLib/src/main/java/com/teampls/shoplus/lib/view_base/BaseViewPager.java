package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Medivh on 2016-11-25.
 */

public class BaseViewPager extends ViewPager {

    private boolean enabled; //이 것이 스크롤을 막아주는 중요 변수!

    public BaseViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            if (this.enabled) {
                return super.onTouchEvent(event);
            } else {
            //    Log.w("DEBUG_JS", String.format("[BaseViewPager.onTouchEvent] disabled..."));
            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsStrting = sw.toString();
            Log.e("INFO", exceptionAsStrting);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        } else {
           // Log.w("DEBUG_JS", String.format("[BaseViewPager.onInterceptTouchEvent] disabled..."));
        }
        return false;
    }

    public void setPagingEnabled() { //이 메소드를 이용해서 스크롤을 풀어주고
        this.enabled = true;
      //  Log.w("DEBUG_JS", String.format("[BaseViewPager.setPagingEnabled] "));
    }

    public void setPagingDisabled() { //이 메소드를 이용해서 스크롤을 막아줍니다.
        this.enabled = false;
      // Log.w("DEBUG_JS", String.format("[BaseViewPager.setPagingDisabled] "));
    }

}