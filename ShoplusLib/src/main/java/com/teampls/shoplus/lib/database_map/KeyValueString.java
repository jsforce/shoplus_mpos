package com.teampls.shoplus.lib.database_map;

import android.content.Context;

/**
 * Created by Medivh on 2018-11-19.
 */

public class KeyValueString extends BaseDBValue<String> {

    public KeyValueString(String key, String defaultValue) {
        super(key, defaultValue);
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public KeyValueDB getDB(Context context) {
        return KeyValueDB.getInstance(context);
    }

    @Override
    public void put(Context context, String value) {
        getDB(context).put(key, value);
    }

    public String get(Context context) {
        return getDB(context).getValue(key, defaultValue);
    }

}
