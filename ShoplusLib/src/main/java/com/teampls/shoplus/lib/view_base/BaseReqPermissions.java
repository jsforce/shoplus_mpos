package com.teampls.shoplus.lib.view_base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;

/**
 * Created by Medivh on 2017-06-28.
 */

abstract public class BaseReqPermissions extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        myActionBar.hide();
        setOnClick(R.id.userPermission_openPermission, R.id.userPermission_request);
    }

    @Override
    public int getThisView() {
        return R.layout.base_user_permission;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.userPermission_openPermission) {
            MyDevice.openAppSetting(context);
        } else if (view.getId() == R.id.userPermission_request) {
            ActivityCompat.requestPermissions((Activity) context, MyDevice.getAllPermissionStr(), 1234);
        }
    }

    private void checkPermissions() {
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE)) {
            MyDevice.createFolders(context, BaseAppWatcher.appDir);
            Log.i("DEBUG_JS", String.format("[BaseReqPermissions.onRequestPermissionsResult] create folders"));
        }

        if (MyDevice.hasAllPermissions(context)) {
            finish();
            startActivity(BaseUtils.getIntent(context, getUserClassification()));
        } else {
            new MyAlertDialog(context, "일부 권한 미승인", "일부 권한이 승인되지 않았습니다. 다음 페이지로 그냥 넘어가시겠습니까?\n\n일부 기능 사용시 권한 문제로 오류가 발생할 수 있습니다") {
                @Override
                public void yes() {
                    finish();
                    startActivity(BaseUtils.getIntent(context, getUserClassification()));
                }
            };
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        checkPermissions();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        checkPermissions();
    }

    abstract public Class<?> getUserClassification();

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }
}
