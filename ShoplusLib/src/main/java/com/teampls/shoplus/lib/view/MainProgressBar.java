package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyUITask;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Medivh on 2016-12-22.
 */

public class MainProgressBar {
    private LinearLayout progressContainer;
    private TextView progressMessage, errorMessage;
    private ProgressBar progressBar;
    private Context context;
    private String currentTaskId = "";
    private Map<String, TaskRecord> taskRecords = new HashMap<>();

    public MainProgressBar(Context context) {
        this.context = context;
        progressContainer = (LinearLayout) ((Activity) context).findViewById(R.id.main_progressContainer);
        progressBar = (ProgressBar) ((Activity) context).findViewById(R.id.main_progressBar);
        progressMessage = (TextView) ((Activity) context).findViewById(R.id.main_progressMessage);
        errorMessage = (TextView) ((Activity) context).findViewById(R.id.main_errorMessage);

        if (context == null)
            Log.e("DEBUG_JS", String.format("[MainProgressBar.MainProgressBar] context == null"));
        if (progressContainer == null)
            Log.e("DEBUG_JS", String.format("[MainProgressBar.MainProgressBar] progressContainer == null, %d", R.id.main_progressContainer));
        init();
    }

    public void init() {
        new MyUITask(context, "MainProgressBar.initBase") {
            @Override
            public void doInBackground() {
                progressContainer.setVisibility(View.GONE);
                progressMessage.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                errorMessage.setVisibility(View.GONE);
            }
        };
    }

    public void onError(final String message) {
        new MyUITask(context, "MainProgressBar.onError") {
            @Override
            public void doInBackground() {
                progressContainer.setVisibility(View.VISIBLE);
                progressMessage.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                errorMessage.setVisibility(View.VISIBLE);
                errorMessage.setText(message);
            }
        };
    }

    public void start(String taskId, final String message, final int max) {
        taskRecords.put(taskId, new TaskRecord(taskId, message, max)); // Task 목록에 추가
        if (currentTaskId.isEmpty())
            currentTaskId = taskId;
        showProgress();
    }

    public void refresh(String taskId, final String message, final int max) {
        start(taskId, message, max);
    }

    private boolean onShowing = false;

    private void showProgress() {
        if (onShowing) return;
        onShowing = true;
        final TaskRecord taskRecord = taskRecords.get(currentTaskId);
        new MyUITask(context, "MainProgressBar.showProgress") {
            @Override
            public void doInBackground() {
                if (taskRecord == null) {
                    Log.e("DEBUG_JS", String.format("[MainProgressBar.doInBackground] %s not found", currentTaskId));
                    return;
                }
                progressContainer.setVisibility(View.VISIBLE);
                progressMessage.setText(taskRecord.message);
                progressBar.setProgress(taskRecord.progress);
                progressBar.setMax(taskRecord.max);
                onShowing = false;
            }

            @Override
            public void catchException(Exception e) {
                super.catchException(e);
                onShowing = false;
            }
        };
    }

    public void setProgress(String taskId, final int progress) {
        if (taskRecords.containsKey(taskId) == false) {
            Log.e("DEBUG_JS", String.format("[MainProgressBar.setProgress] %s not found", taskId));
            return;
        }

        TaskRecord taskRecord = taskRecords.get(taskId);
        taskRecord.progress = progress;
        taskRecords.put(taskId, taskRecord);

        if (currentTaskId.isEmpty() == false) {
            currentTaskId = taskId;
            showProgress();
        }

    }

    public void next(String taskId) {
        if (taskRecords.containsKey(taskId) == false) {
//            Log.e("DEBUG_JS", String.format("[MainProgressBar.next] %s not found", taskId));
//            for (String id : taskRecords.keySet())
//                Log.e("DEBUG_JS", String.format("[MainProgressBar.next] %s, %s, %d", id, taskRecords.get(id).message, taskRecords.get(id).max));
            return;
        }
        TaskRecord taskRecord = taskRecords.get(taskId);
        taskRecord.progress = taskRecord.progress + 1;
        taskRecords.put(taskId, taskRecord);

        if (currentTaskId.isEmpty())
            currentTaskId = taskId;
        showProgress();

    }

    public int getProgress(String taskId) {
        TaskRecord taskRecord = taskRecords.get(taskId);
        if (taskRecord == null)
            return 0;
        else
            return taskRecord.progress;
    }

    public void stop(String taskId) {
        taskRecords.remove(taskId); // Task 목록에서 제거
        if (taskId.equals(currentTaskId))
            currentTaskId = "";
        if (taskRecords.size() == 0) {
            new MyDelayTask(context, 1000) {
                @Override
                public void onFinish() {
                    progressContainer.setVisibility(View.GONE);
                }
            };
        }
    }

    public boolean isProgressFinished(String taskId) {
        if (taskRecords.containsKey(taskId) == false) {
            Log.e("DEBUG_JS", String.format("[MainProgressBar.isProgressFinished] %s not found", taskId));
            return true;
        }
        TaskRecord taskRecord = taskRecords.get(taskId);
        return (taskRecord.progress >= taskRecord.max);
    }

    class TaskRecord {
        String taskId = "", message = "";
        int max = 0, progress = 0;

        public TaskRecord(String taskId, String message, int max) {
            this.taskId = taskId;
            this.message = message;
            this.max = max;
            this.progress = 0;
        }

        public void toLogCat(String location) {
            Log.i("DEBUG_JS", String.format("[%s] %s, %d/%d, %s", location, taskId, progress, max, message));
        }
    }
}
