package com.teampls.shoplus.lib.awsservice.s3;

import com.amazonaws.SDKGlobalConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3Client;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;

/**
 * @author lucidite
 */
public class S3Helper {
    // TODO @Sunstrider 중복 생성에 대한 개선 고려 (singleton?)
    public static AmazonS3Client getClient(AWSCredentials credential) {
        // AWS S3 Client Configuration: Supporting AWS Signature Version 4
        System.setProperty(SDKGlobalConfiguration.ENFORCE_S3_SIGV4_SYSTEM_PROPERTY,"true");
        AmazonS3Client s3Client = new AmazonS3Client(credential);
        s3Client.setRegion(Region.getRegion(MyAWSConfigs.getCurrentConfig().getS3BucketRegion()));
        return s3Client;
    }
}
