package com.teampls.shoplus.lib.datatypes;

import org.json.JSONObject;

/**
 * 외부 API 지원을 위한 클라이언트 데이터
 *
 * @author lucidite
 */
public class ExternalAPIData {
    private PapagoTranslationAPIData papago;

    public ExternalAPIData(){
        this.papago = new PapagoTranslationAPIData(null);
    }

    public ExternalAPIData(JSONObject dataObj) {
        if (dataObj == null) {
            this.papago = new PapagoTranslationAPIData(null);
        } else {
            this.papago = new PapagoTranslationAPIData(dataObj.optJSONObject("papago"));
        }
    }

    public PapagoTranslationAPIData getPapago() {
        return papago;
    }
}
