package com.teampls.shoplus.lib;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.ProjectItemServiceProtocol;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectItemService;

/**
 * @author lucidite
 */
public class ProjectItemServiceManager {
    public static ProjectItemServiceProtocol defaultService(Context context) {
        return new ProjectItemService();
    }
}
