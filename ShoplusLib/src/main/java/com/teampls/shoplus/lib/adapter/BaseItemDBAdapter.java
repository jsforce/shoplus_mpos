package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Hangul;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyImageLoader;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.QueryAndRecord;
import com.teampls.shoplus.lib.database.QueryOrderRecord;
import com.teampls.shoplus.lib.database_memory.MItemDB;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


abstract public class BaseItemDBAdapter extends BaseDBAdapter<ItemRecord> {
    protected ItemDB itemDB;
    protected MItemDB mItemDB;
    protected int maxCount = 0;
    protected boolean doFilterProviders = false, doFilterDays = false, doShowSerial = false, doShowChar = false, doFilterMonths = false;
    protected boolean doFilterIds = false, doShowName = false, doShowPrice = false, doShowProvider = false, doShowState = true, doShowRetailPrice = false;
    protected boolean doShowDate = true, doFilterItemIds = false, doFilterItemCount = false, selectable = false;
    protected boolean doAddFirstRecord = false, doFilterStateTypes = false, bookmarkEnabled = true;
    protected boolean doFilterCategory = false, doBlockItemIds = false, doBlockEmptyName = false;
    protected List<DateTime> selectedDays = new ArrayList<>(0), selectedMonths = new ArrayList<>(); //
    protected List<Integer> selectedIds = new ArrayList<>(0); // selected initially
    protected List<Integer> selectedItemIds = new ArrayList<>(0), blockedItemIds = new ArrayList<>(); // selected initially
    protected List<String> providerPhoneNumbers = new ArrayList<>(0);
    protected List<ItemStateType> selectedStates = new ArrayList<>();
    protected ItemCategoryType selectedCategory = ItemCategoryType.NONE;
    protected MyImageLoader imageLoader;
    private ItemRecord firstRecord = new ItemRecord();
    // ViewHolder
    protected double imageViewRatio = 1.2;
    protected MyMap<Integer, Integer> itemIdCountMapForSorting = new MyMap<>(0);
    protected boolean countMapUpdated = false;

    public BaseItemDBAdapter(Context context) {
        this(context, ItemDB.getInstance(context));
    }

    public BaseItemDBAdapter(Context context, ItemDB itemDB) {
        super(context);
        this.itemDB = itemDB;
        mode = Mode.FileDB;
        imageLoader = MyImageLoader.getInstance(context);
        resetOrdering();
    }

    public BaseItemDBAdapter(Context context, MItemDB mItemDB) {
        super(context);
        this.mItemDB = mItemDB;
        mode = Mode.MemoryDB;
        imageLoader = MyImageLoader.getInstance(context);
        resetOrdering();
    }

    public void toLogCat(String callLocation) {
        for (ItemRecord record : getRecords())
            record.toLogCat(callLocation);
    }

    public BaseItemDBAdapter resetOrdering() {
        setSortingKey(MSortingKey.ID);
        return this;
    }

    public List<Integer> getBookmarks() {
        switch (mode) {
            default:
            case FileDB:
                return itemDB.getBookmarks();
            case MemoryDB:
                return mItemDB.getBookmarks();
        }
    }

    public BaseItemDBAdapter enableBookmark(boolean value) {
        bookmarkEnabled = value;
        return this;
    }

    public void setImageViewRatio(double ratio) {
        this.imageViewRatio = ratio;
    }

    public boolean isCategoryFiltered() {
        return doFilterCategory;
    }

    public boolean isStateTypeFiltered() {
        return doFilterStateTypes;
    }

    public boolean isItemIdsFiltered() {
        return doFilterItemIds;
    }

    public BaseItemDBAdapter setMaxCount(int maxCount) {
        this.maxCount = maxCount;
        return this;
    }

    public BaseItemDBAdapter resetFiltering() {
        doSkipGenerate = false;
        doFilterProviders = false;
        doFilterIds = false;
        doFilterDays = false;
        doFilterMonths = false;
        doFilterItemIds = false;
        doFilterCategory = false;
        doBlockItemIds = false;
        doBlockEmptyName = false;
        doFilterStateTypes = false;
        return this;
    }

    public void resetFilterCategory() {
        doFilterCategory = false;
    }

    public BaseItemDBAdapter resetShowing() {
        doShowName = false;
        doShowPrice = false;
        doShowRetailPrice = false;
        doShowSerial = false;
        doShowProvider = false;
        doShowDate = false;
        doShowState = false;
        doShowChar = false;
        return this;
    }

    public boolean isShowingDate() {
        return doShowDate;
    }

    public BaseItemDBAdapter filterCategory(ItemCategoryType itemCategoryType) {
        this.doFilterCategory = (itemCategoryType != ItemCategoryType.ALL);
        this.selectedCategory = itemCategoryType;
        return this;
    }

    public ItemCategoryType getFilterCategory() {
        return selectedCategory;
    }

    public List<ItemStateType> getFilterStates() {
        if (doFilterStateTypes == false)
            selectedStates.clear();
        return selectedStates;
    }

    public BaseItemDBAdapter filterProviders(List<String> providerPhoneNumbers) {
        this.doFilterProviders = true;
        this.providerPhoneNumbers = providerPhoneNumbers;
        return this;
    }

    public boolean isProvidersFiltered() {
        return doFilterProviders;
    }

    public List<String> getFilterProviders() {
        return providerPhoneNumbers;
    }

    public BaseItemDBAdapter filterStates(ItemStateType... itemStates) {
        selectedStates.clear();
        doFilterStateTypes = true;
        for (ItemStateType state : itemStates)
            selectedStates.add(state);

        if (new HashSet(selectedStates).size() == ItemStateType.values().length) {
            doFilterStateTypes = false;
            selectedStates.clear();
        }

        return this;
    }

    public BaseItemDBAdapter filterStates(List<ItemStateType> itemStates) {
        doFilterStateTypes = true;
        selectedStates = itemStates;
        return this;
    }

    public BaseItemDBAdapter filterDays(ArrayList<DateTime> selectedDays) {
        this.doFilterDays = true;
        this.selectedDays = selectedDays;
        return this;
    }

    public BaseItemDBAdapter filterMonths(DateTime... selectedMonth) {
        this.doFilterMonths = true;
        selectedMonths.clear();
        for (DateTime month : selectedMonth)
            selectedMonths.add(month);
        return this;
    }

    public BaseItemDBAdapter filterIds(ArrayList<Integer> dbIds) {
        this.doFilterIds = true;
        this.selectedIds = dbIds;
        orderRecords = new QueryOrderRecord(ItemDB.Column.itemId, true, true).toList();
        return this;
    }

    public BaseItemDBAdapter blockItemIds(List<Integer> itemIds) {
        this.doBlockItemIds = true;
        this.blockedItemIds = itemIds;
        return this;
    }

    public BaseItemDBAdapter blockEmptyName() {
        this.doBlockEmptyName = true;
        return this;
    }

    public BaseItemDBAdapter filterItemId(int itemId) {
        this.doFilterItemIds = true;
        this.selectedItemIds.clear();
        this.selectedItemIds.add(itemId);
        return this;
    }

    public BaseItemDBAdapter filterItemIds(List<Integer> itemIds) {
        this.doFilterItemIds = true;
        this.selectedItemIds.clear();
        for (Integer itemId : itemIds)
            selectedItemIds.add(itemId);
        // this.selectedItemIds = itemIds; Adapter에서 clear 실행시 문제의 소지가 있다
        orderRecords = new QueryOrderRecord(ItemDB.Column.itemId, true, true).toList();
        // Log.i("DEBUG_JS", String.format("[BaseItemDBAdapter.filterItemIds] %s", TextUtils.join(",", BaseUtils.toStringList(itemIds))));
        return this;
    }

    public void resetFilterItemIds() {
        this.doFilterItemIds = false;
        this.selectedItemIds.clear();
    }

    public List<Integer> getSelectedItemIds() {
        return selectedItemIds;
    }

    public BaseItemDBAdapter showName() {
        this.doShowName = true;
        return this;
    }

    public BaseItemDBAdapter showChar() {
        this.doShowChar = true;
        return this;
    }

    public BaseItemDBAdapter showSerial() {
        this.doShowSerial = true;
        return this;
    }

    public BaseItemDBAdapter showState() {
        this.doShowState = true;
        return this;
    }

    public BaseItemDBAdapter showPrice() {
        this.doShowPrice = true;
        return this;
    }

    public BaseItemDBAdapter showRetailPrice() {
        this.doShowRetailPrice = true;
        return this;
    }

    public BaseItemDBAdapter showProvider() {
        this.doShowProvider = true;
        return this;
    }

    public BaseItemDBAdapter showDate() {
        this.doShowDate = true;
        return this;
    }

    public BaseItemDBAdapter hideDate() {
        this.doShowDate = false;
        return this;
    }

    public BaseItemDBAdapter setSelectable() {
        this.selectable = true;
        return this;
    }

    public void setSortedItemIds(List<Integer> sortedItemIds) {
        records = itemDB.getRecordsBy(sortedItemIds);
        doSkipGenerate = true;
    }

    public MyMap<Integer, ItemRecord> getRecordMap() {
        MyMap<Integer, ItemRecord> results = new MyMap<>(new ItemRecord());
        for (ItemRecord record : getRecords())
            results.put(record.itemId, record);
        return results;
    }

    public MyMap<Integer, Integer> getPositionMap() {
        int position = 0;
        MyMap<Integer, Integer> results = new MyMap<>(0);
        for (ItemRecord record : getRecords()) {
            results.put(record.itemId, position);
            position++;
        }
        return results;
    }

    public void resetClickedPosition(List<Integer> clickedItemIds) {
        int position = 0;
        switch (choiceMode) {
            default:
                clickedPositions.clear();
                for (ItemRecord record : getRecords()) {
                    if (clickedItemIds.contains(record.itemId))
                        clickedPositions.add(position);
                    position++;
                }
                break;
            case CHOICE_MODE_ORDERED_MULTIPLE:
                MyMap<Integer, Integer> positionMap = getPositionMap(); // itemId - position
                clickedPositions.clear();
                for (int itemId : clickedItemIds)
                    clickedPositions.add(positionMap.get(itemId));
                break;
        }
    }

    public List<Integer> getClickedItemIds() {
        List<Integer> clickedItemIds = new ArrayList();
        switch (choiceMode) {
            default:
                for (ItemRecord record : getSortedClickedRecords())
                    clickedItemIds.add(record.itemId);
                break;
            case CHOICE_MODE_ORDERED_MULTIPLE:
                for (ItemRecord record : getClickedRecords())
                    clickedItemIds.add(record.itemId);
                break;
        }
        return clickedItemIds;
    }

    public void addFirstRecord(String name) {
        doAddFirstRecord = true;
        firstRecord.name = name;
    }

    public boolean isFirstRecordAdded() {
        return doAddFirstRecord;
    }

    public void setCountMap(MyMap<Integer, Integer> itemIdCountMap) {
        countMapUpdated = true;
        this.itemIdCountMapForSorting = itemIdCountMap;
    }

    public void autoSetCountMap() {
        countMapUpdated = true;
        switch (mode) {
            default:
                itemIdCountMapForSorting = itemDB.getStockMap();
                break;
            case MemoryDB:
                itemIdCountMapForSorting = mItemDB.getStockMap();
                break;
        }
    }

    public BaseItemDBAdapter enableCountMap(boolean value) {
        this.countMapUpdated = value;
        return this;
    }

    @Override
    public void generate() {
        if (doSkipGenerate) return;

        int count = 0;
        // itemRecords 추출방법이 다르다
        List<ItemRecord> itemRecords= new ArrayList<>();
        switch (mode) {
            default:
                if (doFilterIds) {
                    itemRecords = itemDB.getRecordsIN(ItemDB.Column._id, BaseUtils.toStringList(selectedIds), orderRecords);
                } else if (doFilterItemIds) {
                    itemRecords = itemDB.getRecordsIN(ItemDB.Column.itemId, BaseUtils.toStringList(selectedItemIds), orderRecords);
                } else {
                    if (doFilterCategory) {
                        List<QueryAndRecord> queryAndRecords = new ArrayList<>();
                        queryAndRecords.add(new QueryAndRecord(ItemDB.Column.category, selectedCategory.toString()));
                        itemRecords = itemDB.getRecords(queryAndRecords, orderRecords);
                    } else {
                        itemRecords = itemDB.getRecordsOrderBy(orderRecords);
                    }
                }
                break;
            case MemoryDB:
                if (doFilterIds) {
                    MyUI.toastSHORT(context, String.format("필터 오류 (id)"));
                }  else if (doFilterItemIds) {
                    for (ItemRecord record : mItemDB.getRecords())
                        if (selectedItemIds.contains(record.itemId))
                            itemRecords.add(record);
                } else {
                    itemRecords = mItemDB.getRecords();
                }
                break;
        }

        List<Integer> bookmarkIds = getBookmarks();
        Map<Integer, ItemRecord> bookmarkMap = new TreeMap<>();
        generatedRecords.clear();

        for (ItemRecord record : itemRecords) {

            if (doFilterCategory)
                if (selectedCategory != record.category)
                    continue;

            if (doBlockEmptyName)
                if (record.name.isEmpty())
                    continue;

            if (doBlockItemIds)
                if (blockedItemIds.contains(record.itemId))
                    continue;

            if (doFilterProviders)
                if (providerPhoneNumbers.contains(record.providerPhoneNumber) == false)
                    continue;

            if (doFilterDays)
                if (selectedDays.contains(BaseUtils.toDay(record.pickedDateTime)) == false)
                    continue;

            if (doFilterMonths)
                if (selectedMonths.contains(BaseUtils.toMonth(record.pickedDateTime)) == false)
                    continue;

            if (doFilterStateTypes)
                if (selectedStates.contains(record.state) == false)
                    continue;

            count = count + 1;
            if (doFilterItemCount)
                if (count > maxCount)
                    break;

            if (countMapUpdated)
                record._count = itemIdCountMapForSorting.get(record.itemId);
            else
                record._count = 0; // 혼선 방지

            record.sortingKey = sortingKey;

            // 북마크 : 북마크 선택, sorting이 ID 순일때만
            if (bookmarkEnabled && bookmarkIds.contains(record.itemId)) { //&& sortingKey == MSortingKey.ID
                bookmarkMap.put(bookmarkIds.indexOf(record.itemId), record);
            } else {
                generatedRecords.add(record);
            }
            //   Log.i("DEBUG_JS", String.format("[BasemItemDBAdapter.generate] (%d) added : item %s (%d) provider %s(%s)", _records.size(), record.name, record.itemId, record.providerEmail, record.providerName));
        }

        if (doSortByKey)
            Collections.sort(generatedRecords);

        if (bookmarkEnabled)
            generatedRecords.addAll(0, bookmarkMap.values());

        if (doAddFirstRecord)
            generatedRecords.add(0, firstRecord);

        if (doSetRecordWhenGenerated)
            applyGeneratedRecords();
    }

    // 특정 날짜의 아이템 모두 선택
    public void markAsClickedAt(DateTime date) {
        clickedPositions.clear();
        int position = 0;
        int clickedPositionCount = 0;
        for (ItemRecord record : records) {
            if (BaseUtils.toDay(record.pickedDateTime).isEqual(date)) {
                clickedPositions.add(position);
                clickedPositionCount++;
            }
            position++;
            if (doCheckClickedPositionLimit && clickedPositionCount >= clickedPositionsLimit) {
                MyUI.toastSHORT(context, String.format("최대 %d개", clickedPositionsLimit));
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void markAsClickedBetween(int fromPosition, int toPosition) {
        int clickedPositionCount = 0;
        clickedPositions.clear();
        for (int position = 0; position < getCount() - 1; position++) {
            if (BaseUtils.isBetween(position, fromPosition, toPosition)) {
                clickedPositions.add(position);
                clickedPositionCount++;
            }
            if (doCheckClickedPositionLimit && clickedPositionCount >= clickedPositionsLimit) {
                MyUI.toastSHORT(context, String.format("최대 %d개", clickedPositionsLimit));
                break;
            }
        }
        notifyDataSetChanged();
    }

    public boolean isShowingDateChanged(int position) {
        if (records.size() <= 0) return false;
        if (position == 0) return true;
        // position >= 1
        DateTime pickedDate = records.get(position).pickedDateTime;
        DateTime prevPikcedDate = records.get(position - 1).pickedDateTime;
        if (prevPikcedDate.getYear() != pickedDate.getYear())
            return true;  // new year
        if (prevPikcedDate.getDayOfYear() != pickedDate.getDayOfYear()) // different day
            return true;
        //
        return false;
    }

    public boolean isBookmarkChanged(int position) {
        if (records.size() <= 0) return false;
        if (position == 0) return true;
        // position >= 1
        boolean isBookmark = getBookmarks().contains(records.get(position).itemId);
        boolean isPrevBookmark = getBookmarks().contains(records.get(position - 1).itemId);
        return isBookmark != isPrevBookmark;
    }

    public boolean isShowingCharChanged(int position) {
        if (records.size() <= 0) return false;
        if (position == 0) return true;
        String initialName = Hangul.getInitial(records.get(position).name);
        String preInitialName = Hangul.getInitial(records.get(position - 1).name);
        return !initialName.equals(preInitialName);
    }

    //=================================================================================================
// ordered 경우 R.layout.row_image_picker 참고
    public class DefaultViewHolder extends BaseViewHolder {
        private int cell3View = R.layout.cell3_item;
        protected ImageView ivImage;
        protected TextView tvDate, tvItemState, tvName, tvPrice, tvStock, tvSubImageCount,
                tvRecommMessage, tvSerial, tvChar, tvProvider, tvOrder;
        protected View view;
        protected ItemRecord record;
        protected ImageView ivChecked, ivBookmark;
        protected FrameLayout container;
        protected LinearLayout textContainer, background;

        @Override
        public int getThisView() {
            return cell3View;
        }

        public DefaultViewHolder() {
        }

        @Override
        public void init(View view) {
            this.view = view;
            ivImage = (ImageView) view.findViewById(R.id.cell3_item_image);
            tvDate = (TextView) view.findViewById(R.id.cell3_item_date);
            tvItemState = (TextView) view.findViewById(R.id.cell3_item_state);
            ivChecked = (ImageView) view.findViewById(R.id.cell3_item_checked);
            ivBookmark = view.findViewById(R.id.cell3_item_bookmark);
            tvName = (TextView) view.findViewById(R.id.cell3_item_name);
            tvPrice = (TextView) view.findViewById(R.id.cell3_item_price);
            tvStock = (TextView) view.findViewById(R.id.cell3_item_stock);
            tvRecommMessage = (TextView) view.findViewById(R.id.cell3_item_message);
            tvSerial = (TextView) view.findViewById(R.id.cell3_item_serial);
            tvSubImageCount = (TextView) view.findViewById(R.id.cell3_item_subImageCount);
            tvOrder = findTextViewById(context, view, R.id.cell3_item_order);
            container = (FrameLayout) view.findViewById(R.id.cell3_item_container);
            textContainer = (LinearLayout) view.findViewById(R.id.cell3_item_textContainer);
            tvChar = (TextView) view.findViewById(R.id.cell3_item_char);
            tvProvider = (TextView) view.findViewById(R.id.cell3_item_provider);
            background = (LinearLayout) view.findViewById(R.id.cell3_item_background);

            setVisibility(View.GONE, tvRecommMessage, tvSubImageCount, tvProvider, tvOrder);

            // adjust cell to device size
            int columnNum = MyDevice.getColumnNum(context);
            container.getLayoutParams().width = MyDevice.getWidth(context) / columnNum;
            container.getLayoutParams().height = (int) (MyDevice.getWidth(context) / columnNum * imageViewRatio);

            MyView.setTextViewByDeviceSizeScale(context, 1.0, tvDate, tvItemState, tvName, tvPrice, tvStock, tvRecommMessage,
                    tvSerial, tvSubImageCount, tvChar, tvProvider);

            MyView.setImageViewSizeByDeviceSize(context, ivBookmark);
            if (MyDevice.isTablet(context)) {
                MyView.setImageSize(context, 75, ivChecked);
                MyView.setMargin(context, tvItemState, 0, 0, 0, 30);
            }
        }

        protected void loadImage(final int position) {
            if (position == 0) {
                imageLoader.retrieveThumb(record, new MyOnTask<Bitmap>() {
                    @Override
                    public void onTaskDone(Bitmap bitmap) {
                        ivImage.setImageBitmap(bitmap);
                    }
                });
            } else {
                imageLoader.retrieveThumbAsync(record, new MyOnTask<Bitmap>() {
                    @Override
                    public void onTaskDone(Bitmap bitmap) {
                        if (ivImage != null)
                            ivImage.setImageBitmap(bitmap);
                    }
                });
            }
        }

        // 개별 뷰에서는 항상 최신 데이터를 가져온다
        protected void updateStockCount() {
            if (countMapUpdated == false) {
                switch (mode) {
                    case FileDB:
                        record._count = itemDB.options.getPositiveStockSum(record.itemId);
                        break;
                    case MemoryDB:
                        record._count = mItemDB.options.getStockSum(record.itemId);
                        break;
                }
            }
            setTextTitle(tvStock, record._count > 0, Integer.toString(record._count));
        }

        @Override
        public void update(final int position) {
            if (position >= records.size() || position < 0) return;
            record = records.get(position);
            container.getLayoutParams().height = (int) (container.getLayoutParams().width * imageViewRatio);
            loadImage(position);

            boolean isBookmark = getBookmarks().contains(record.itemId);
            ivBookmark.setVisibility(isBookmark ? View.VISIBLE : View.GONE);

            ivChecked.setVisibility(View.GONE);
            tvOrder.setVisibility(View.GONE);

            if (selectable) {
                if (clickedPositions.contains(position)) {
                    switch (choiceMode) {
                        default:
                            ivChecked.setVisibility(View.VISIBLE);
                            break;
                        case CHOICE_MODE_ORDERED_MULTIPLE:
                            tvOrder.setVisibility(View.VISIBLE);
                            tvOrder.setText(String.format("%d", clickedPositions.indexOf(position) + 1));
                            break;
                    }
                    ivImage.setImageAlpha(255);
                } else {
                    ivImage.setImageAlpha(BaseGlobal.ImageAlpha);
                }
            }

            if (doShowDate && !isBookmark) { // 북마크는 날짜를 보여줄 필요가 없음
                tvDate.setVisibility(isShowingDateChanged(position) || isBookmarkChanged(position) ? View.VISIBLE : View.GONE);
                if (BasePreference.isShowWeekday(context)) {
                    tvDate.setText(record.pickedDateTime.toString(BaseUtils.MD_Format) + " " + BaseUtils.getDayOfWeekKor(record.pickedDateTime));
                } else {
                    tvDate.setText(record.pickedDateTime.toString(BaseUtils.MD_Kor_Format));
                }
            } else {
                tvDate.setVisibility(View.GONE);
            }

            setTextTitle(tvName, doShowName, record.name);
            setTextTitle(tvSerial, doShowSerial, record.serialNum.equals("00-0000") ? "" : record.serialNum);

            tvPrice.setTextColor(ColorType.Black.colorInt);
            if (doShowPrice) {
                tvPrice.setVisibility(View.VISIBLE);
                if (doShowRetailPrice) {
                    tvPrice.setTextColor(ColorType.RetailColor.colorInt);
                    tvPrice.setText(record.retailPrice == 0 ? "" : BaseUtils.toCurrencyStr(record.retailPrice));
                } else {
                    tvPrice.setText(record.unitPrice == 0 ? "" : BaseUtils.toCurrencyStr(record.unitPrice));
                }
            } else {
                tvPrice.setVisibility(View.GONE);
            }

            if (record.state == ItemStateType.DISPLAYED || doShowState == false) {
                tvItemState.setVisibility(View.GONE);
            } else {
                tvItemState.setText(record.state.toUIStr());
                tvItemState.setVisibility(View.VISIBLE);
            }

            updateStockCount();

            if (doShowChar && record.name.isEmpty() == false && !isBookmark) {
                tvChar.setVisibility(isShowingCharChanged(position) ? View.VISIBLE : View.GONE);
                tvChar.setText(Hangul.getInitial(record.name));
            } else {
                tvChar.setVisibility(View.GONE);
            }

        }
    }

    public class NameViewHolder extends BaseViewHolder {
        protected TextView tvName;
        protected LinearLayout container;

        @Override
        public int getThisView() {
            return R.layout.row_string;
        }

        @Override
        public void init(View view) {
            tvName = (TextView) view.findViewById(R.id.row_string_textview);
            container = (LinearLayout) view.findViewById(R.id.row_string_container);
            MyView.setTextViewByDeviceSize(context, tvName);
        }

        @Override
        public void update(int position) {
            ItemRecord record = getRecord(position);
            tvName.setText(record.getUiName());
            if (clickedPositions.contains(position)) {
                container.setBackgroundColor(ColorType.Khaki.colorInt);
            } else {
                container.setBackgroundColor(ColorType.Trans.colorInt);
            }
        }
    }


    public class ItemSearchByNameViewHolder extends BaseViewHolder {
        protected TextView tvName;
        protected ImageView ivImage;

        @Override
        public int getThisView() {
            return R.layout.row_string_image;
        }

        @Override
        public void init(View view) {
            tvName = (TextView) view.findViewById(R.id.row_string_image_textView);
            ivImage = (ImageView) view.findViewById(R.id.row_string_image_imageView);
            MyView.setTextViewByDeviceSize(context, tvName);
            MyView.setImageViewSizeByDeviceSizeScale(context, 0.5, ivImage);
        }

        @Override
        public void update(int position) {
            ItemRecord record = getRecord(position);
            tvName.setText(String.format("%s%s", record.name, record.unitPrice == 0 ? "" : " : " + BaseUtils.toCurrencyOnlyStr(record.unitPrice)));
            imageLoader.retrieveThumbAsync(record, new MyOnTask<Bitmap>() {
                @Override
                public void onTaskDone(Bitmap result) {
                    if (ivImage != null)
                        ivImage.setImageBitmap(result);
                }
            });
        }
    }
}


/*
    public class TwoColumnViewHolder extends BaseViewHolder {
        private int thisView = R.layout.cell2_item;
        protected ImageView ivImage, ivNotice, ivDiscount;
        protected TextView tvDate, tvItemState, tvName, tvPrice, tvSerial, tvViewCount;
        protected View view;
        protected ItemRecord record;

        @Override
        public int getThisView() {
            return thisView;
        }

        @Override
        public void init(View view) {
            ivImage = (ImageView) view.findViewById(R.id.cell2_item_image);
            tvDate = (TextView) view.findViewById(R.id.cell2_item_date);
            tvSerial = (TextView) view.findViewById(R.id.cell2_item_serial);
            tvItemState = (TextView) view.findViewById(R.id.cell2_item_state);
            tvName = (TextView) view.findViewById(R.id.cell2_item_name);
            tvPrice = (TextView) view.findViewById(R.id.cell2_item_price);
            tvItemState.setVisibility(View.GONE);
        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0) return;
            record = records.get(position);
            //LocalUserRecord userRecord = LocalUserDB.getInstance(context).getRecordByKey(record.providerPhoneNumber); // provider 표시용
            if ((position == 0) && (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING)) {
                //      Log.w("DEBUG_JS", String.format("[TwoColumnViewHolder.update] position == 0, FLING ... NO image"));
                return;
            }

            setTextTitle(tvName, doShowName, record.name);
            setTextTitle(tvSerial, doShowSerial, record.serialNum);

            if (doShowPrice) {
                tvPrice.setVisibility(View.VISIBLE);
                if (doShowRetailPrice) {
                    tvPrice.setText(record.retailPrice == 0? "" : BaseUtils.toCurrencyStr(record.retailPrice));
                } else {
                    tvPrice.setText(record.unitPrice == 0? "" : BaseUtils.toCurrencyStr(record.unitPrice));
                }
            } else {
                tvPrice.setVisibility(View.GONE);
            }

            if (doShowDate) {
                tvDate.setVisibility(isShowingDateChanged(position) ? View.VISIBLE : View.GONE);
                tvDate.setText(record.pickedDateTime.toString(BaseUtils.MD_Kor_Format));
            } else {
                tvDate.setVisibility(View.GONE);
            }

            imageLoader.retrieveThumbAsync(record, new MyOnTask<Bitmap>() {
                @Override
                public void onTaskDone(Bitmap bitmap) {
                    if (ivImage != null)
                        ivImage.setImageBitmap(bitmap);
                }
            });

        }
    }
 */