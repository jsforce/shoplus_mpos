package com.teampls.shoplus.lib.protocol;

import android.support.annotation.Nullable;

import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemTraceAttachErrorCode;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.enums.SlipItemType;

import java.util.Set;

/**
 * @author lucidite
 */
public interface SlipItemDataProtocol {
    /**
     * 거래(전표) 항목이 참조하는 판매아이템의 ID. 연결된 아이템이 없을 경우 -1의 값을 가진다.
     * @return
     */
    int getItemId();

    /**
     * 거래(전표) 항목의 판매아이템 이름
     * @return
     */
    String getItemName();

    /**
     * 거래(전표) 항목의 판매아이템 컬러 옵션. 사전에 약속된 컬러 code 문자열로부터 변환한 데이터.
     * @return
     */
    ColorType getColor();

    /**
     * 거래(전표) 항목의 판매아이템 사용자 지정 컬러 이름. 지정된 사용자 컬러 이름이 없을 경우 null을 반환한다.
     * @return 사용자 지정 컬러 이름, 없을 경우 null
     */
    @Nullable
    String getCustomColorName();

    /**
     * 거래(전표) 항목의 판매아이템 컬러 이름.
     * 사용자 지정 컬러 이름이 있는 경우 이를 반환하고, 없는 경우 기본 컬러셋의 이름을 반환한다.
     * @return
     */
    String getColorName();

    /**
     * 거래(전표) 항목의 판매아이템 사이즈 옵션. 사전에 약속된 타입 문자열로부터 SizeType으로 변환하여 반환한다.
     * @return
     */
    SizeType getSize();

    /**
     * 거래(전표) 항목의 판매아이템 단가.
     * @return
     */
    int getUnitPrice();

    /**
     * 거래(전표) 항목의 판매아이템 수량.
     * @return
     */
    int getQuantity();

    /**
     * 거래(전표) 항목의 종류(상태).
     * @return
     */
    SlipItemType getItemType();

    /**
     * 거래(전표) 항목의 최종 수정일. MM/dd 포맷을 가져야 한다.
     * 참고: 항목 업데이트 서비스를 호출하면 서비스 메서드 내에서 업데이트하는 방식으로 데이터를 갱신한다.
     * @return
     */
    String getUpdatedDate();

    /**
     * 연동된 재고추적 QR코드 목록
     * 서버에 전송하기 위한 용도로 사용하며, 서버로부터 받는 데이터에서는 일반적으로 포함되어 있지 않다.
     *
     * @return
     */
    Set<String> getItemTraces();

    /**
     * (작성 중인 거래항목에 대해) 재고추적 QR이 연동되어 있는 항목인지를 반환한다.
     * 연동되어 있는 항목인 경우, 거래유형을 변경할 수 없게 하는 것을 권장한다.
     * (또는 유형에 따라 잘 처리해 줄 수 있을 것이나, 사용자가 세부 ID를 볼 수 없으므로 지우고 다시 작성하는 것이 안전할 것임)
     * [NOTE] 서버에서 받은 값에는 QR코드 정보가 포함되어 있지 않으므로 적용해서는 안 된다.
     *
     * @return
     */
    Boolean isItemTraceAttached();

    /**
     * 새로운 재고추적 QR을 추가한다.
     * 중복 체크를 위한 용도로 사용해야 한다. 정상적으로 붙여진 경우 수량을 증가시켜야 할 것이다.
     * [NOTE] 서버에서 받은 값에는 QR코드 정보가 포함되어 있지 않으므로 적용해서는 안 된다.
     *
     * @param itemTrace
     * @return 에러 원인 코드
     */
    ItemTraceAttachErrorCode attachItemTrace(String itemTrace);

    /**
     * 사용자별 가격이 설정된 경우 (사용자별 가격 - 상품 기준가)를 반환한다.
     * [SILVER-371] 거래기록 작성 시에 가격의 변동이 있는 경우, 원래 가격과의 차이를 설정한 값이다.
     * 0도 의미를 가지므로(기존에 저장된 사용자별 가격 삭제), null을 반환할 수 있음에 유의한다.
     *
     * @return
     */
    @Nullable
    Integer getPriceDelta();

    /**
     * [SILVER-371] 사용자별 가격을 설정한다. 상품의 가격을 설정하면, 내부적으로 (거래항목의 가격 - 상품 기준가)를 반환한다.
     * 현재 거래항목의 단가가 itemReferencePrice와 동일하면 기존에 설정된 사용자별 가격을 삭제한다.
     * 불필요한 업데이트를 방지하기 위해, 사용자별 가격에 변동이 없는 경우 설정하지 않는 것을 권장한다.
     *
     * @param itemReferencePrice    상품의 (기준) 가격
     */
    void setPriceDelta(int itemReferencePrice);
}
