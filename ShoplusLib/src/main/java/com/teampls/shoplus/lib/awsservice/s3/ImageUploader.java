package com.teampls.shoplus.lib.awsservice.s3;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucidite
 */
public class ImageUploader {
    private AWSCredentials credential;
    private String userid;
    private String bucketName;

    public ImageUploader(AWSCredentials credential, String userid, String bucketName) {
        this.credential = credential;
        this.userid = userid;
        this.bucketName = bucketName;
    }

    /**
     * 단일 파일을 S3으로 업로드한다. 이미지 교체 용도로 사용한다(SILVER-200).
     *
     * @param imageType 상품 이미지 유형을 지정한다.
     * @param timestamp
     * @param localFilePath
     * @return
     * @throws AmazonClientException
     */
    public String upload(ImageUploadType imageType, DateTime timestamp, String localFilePath) throws AmazonClientException {
        AmazonS3Client s3Client = S3Helper.getClient(credential);
        return this.uploadImage(imageType, timestamp, localFilePath, s3Client);
    }

    /**
     * 여러 파일을 S3로 업로드한다. 오리지널 파일만 업로드한다.
     *
     * @param imageType 이미지 유형을 지정한다.
     * @param timestamp
     * @param localFilePaths
     * @return
     * @throws AmazonClientException
     */
    public List<String> upload(ImageUploadType imageType, DateTime timestamp, List<String> localFilePaths) throws AmazonClientException {
        AmazonS3Client s3Client = S3Helper.getClient(credential);
        List<String> remoteFilePaths = new ArrayList<>();
        for (String localFilePath : localFilePaths) {
            String remoteFilePath = this.uploadImage(imageType, timestamp, localFilePath, s3Client);
            remoteFilePaths.add(remoteFilePath);
        }
        return remoteFilePaths;
    }

    private String uploadImage(ImageUploadType imageType, DateTime timestamp, String localFilePath, AmazonS3Client s3Client) {
        String remoteFilePath = ImageFilePathHelper.genRemoteFilePath(imageType, timestamp, userid);
        PutObjectRequest por = new PutObjectRequest(this.bucketName, remoteFilePath, new java.io.File(localFilePath));
        s3Client.putObject(por);
        return remoteFilePath;
    }
}
