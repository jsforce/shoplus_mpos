package com.teampls.shoplus.lib.database_map;

/**
 * Created by Medivh on 2016-04-12.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.DBResult;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class KeyValueDB extends BaseDB<KeyValueRecord> {
    private static KeyValueDB instance = null;
    private static int Ver = 1;

    public enum Column {
        _id, key, value;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    private KeyValueDB(Context context) {
        super(context, "KeyValueDB", Ver, Column.toStrs());
    }

    public KeyValueDB(Context context, String DBname, int DBversion, String[] columns) {
        super(context, DBname, DBversion, columns);
    }

    public KeyValueDB(Context context, String DBname, int DBversion) {
        this(context, DBname, DBversion, Column.toStrs());
    }

    @Override
    protected int getId(KeyValueRecord record) {
        return record.id;
    }

    public static KeyValueDB getInstance(Context context) {
        if (instance == null)
            instance = new KeyValueDB(context);
        return instance;
    }

    protected ContentValues toContentvalues(KeyValueRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.key.toString(), record.key);
        contentvalues.put(Column.value.toString(), record.value);
        return contentvalues;
    }

    @Override
    protected void toLogCat(int id, KeyValueRecord record, int option, String operation) {
        Log.i("DEBUG_JS", String.format("[%s] [%d] key = %s, value = %s,", operation,
                id, record.key, record.value));
    }

    public void put(int key, int value) {
        put(Integer.toString(key), Integer.toString(value));
    }

    public boolean put(String key, String value) {
        DBResult result;
        if (hasKey(key.toLowerCase())) {
            KeyValueRecord record = getRecord(getId(Column.key, key.toLowerCase()));
            record.value = value;
            result = update(record.id, record);
        } else {
            result = insert(new KeyValueRecord(0, key.toLowerCase(), value)); // key = lowercase
        }
        return result.resultType.updatedOrInserted();
    }

    public boolean put(int key, boolean value) {
        return put(Integer.toString(key), value);
    }

    public boolean put(String key, int value) {
        return put(key, String.valueOf(value));
    }

    public boolean put(String key, long value) {
        return put(key, String.valueOf(value));
    }

    public boolean put(String key, boolean value) {
        if (key.isEmpty())
            return false;
        return put(key, String.valueOf(value));
    }

    public boolean put(String key, double value) {
        return put(key, String.valueOf(value));
    }

    public <T> boolean put(String key, List<T> integers) {
        return put(key, TextUtils.join(multiItemDelimiter, BaseUtils.toStringList(integers)));
    }

    public List<String> getStrings(String key, String defaultString) {
        String value = getValue(key, defaultString);
        return BaseUtils.toList(value.split(multiItemDelimiter));
    }

    public boolean hasKey(String key) {
        if (key.isEmpty())
            return false;
        return hasValue(Column.key, key.toLowerCase());
    }

    public String getValue(String key) {
        return getValue(key, "");
    }

    public String getValue(String key, String defaultValue) {
        if (hasKey(key)) {
            int id = getId(Column.key, key.toLowerCase());
            return this.getRecord(id).value;
        } else {
            return defaultValue;
        }
    }

    public KeyValueRecord getRecordByKey(int key) {
        return getRecord(Column.key, Integer.toString(key));
    }

    public KeyValueRecord getRecordByKey(String key) {
        return getRecord(Column.key, key);
    }

    public int getInt(String key, int defaultValue) {
        if (hasKey(key)) {
            if (BaseUtils.isInteger(getValue(key))) {
                try {
                    return Integer.valueOf(getValue(key));
                } catch (NumberFormatException e) {
                    return defaultValue;
                }
            } else {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }


    public float getFloat(String key) {
        return getFloat(key, 0);
    }

    public float getFloat(String key, float defaultValue) {
        if (hasKey(key)) {
            if (BaseUtils.isFloat(getValue(key))) {
                return Float.valueOf(getValue(key));
            } else {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public double getDouble(String key, double defaultValue) {
        if (hasKey(key)) {
            if (BaseUtils.isFloat(getValue(key))) {
                return Double.valueOf(getValue(key));
            } else {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public int getInt(String key) {
      return getInt(key, 0);
    }

    public int getInt(int key) {
        return getInt(Integer.toString(key), 0);
    }

    public long getLong(String key) {
        return (hasKey(key)? Long.valueOf(getValue(key)) : 0);
    }

    public long getLong(String key, long defaultValue) {
        return (hasKey(key)? Long.valueOf(getValue(key)) : defaultValue);
    }

    public boolean getBool(int key, boolean defaultValue) {
        return getBool(Integer.toString(key), defaultValue);
    }

    public boolean getBool(String key, boolean defaultValue) {
        if (hasKey(key)) {
            return Boolean.valueOf(getValue(key));
        } else {
            return defaultValue;
        }
    }

    public boolean getBool(String key) {
        return getBool(key, false);
    }

    public DateTime getDateTime(String key) {
        if (hasKey(key)) {
            return BaseUtils.toDateTime(getValue(key));
        } else {
            return Empty.dateTime;
        }
    }

    @Override
    protected KeyValueRecord getEmptyRecord() {
        return new KeyValueRecord();
    }

    @Override
    protected KeyValueRecord createRecord(Cursor cursor) {
        return new KeyValueRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.key.ordinal()),
                cursor.getString(Column.value.ordinal())
        );
    }

    public void toLogCat() {
        for (KeyValueRecord record : getRecords()) {
            toLogCat(record.id, record, 0, "KeyValueDB.show");
        }
    }

    public void setIntegersAsHashSet(String key) {
        Set<String> items = new HashSet<>();
        for (String item : getValue(key).split(multiItemDelimiter))
            items.add(item);
        String result = TextUtils.join(multiItemDelimiter, items);
        //Log.i("DEBUG_JS", String.format("[KeyValueDB.addIntegers] %s, %s", key, result));
        put(key, result);
    }

    public void addIntegers(String key, List<Integer> values, int limit) {
        if (values.isEmpty()) return;
        String integersStr = getValue(key, "0");
        List<Integer> integers = new ArrayList<>();
        for (String integerStr : integersStr.split(multiItemDelimiter)) {
            int integer = BaseUtils.toInt(integerStr, -999);
            if (integer != -999)
                integers.add(integer);
        }

        for (int value : values) {
            // 중복으로 추가되는 것 방지
            if (integers.size() > 0) {
                if (value != integers.get(integers.size() - 1))
                    integers.add(value); // 추가
            } else {
                integers.add(value); // 추가
            }
        }

        if (integers.size() >= 1000)
            limit = 1000;

        if (limit > 0) {
            if (integers.size() > limit)
                integers = integers.subList(integers.size()-limit, integers.size());  // 최대 개수 지정
        }

        String result = TextUtils.join(multiItemDelimiter, integers);
//        Log.i("DEBUG_JS", String.format("[KeyValueDB.addIntegers] %s, %s", key, result));
        put(key, result);
    }

    public List<Integer> getIntegers(String key) {
        String integersStr = getValue(key, "0"); // 오래된 순서로
   //     Log.i("DEBUG_JS", String.format("[KeyValueDB.getIntegers] %s, %s", key, integersStr));
        List<Integer> integers = new ArrayList<>();
        for (String integerStr : integersStr.split(multiItemDelimiter)) {
            int integer = BaseUtils.toInt(integerStr, -999);
            if (integer != -999)
                integers.add(integer);
        }
        return integers;
    }

}
