package com.teampls.shoplus.lib.datatypes;

/**
 * Created by Medivh on 2017-09-01.
 */

public class MyTriple<F, S, T> {
    public F first;
    public S second;
    public T third;

    public static <F, S, T> MyTriple<F, S, T> create(F first, S second, T third) {
        MyTriple result = new MyTriple();
        result.first = first;
        result.second = second;
        result.third = third;
        return result;
    }

}
