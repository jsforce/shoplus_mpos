package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.widget.CompoundButtonCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.enums.ColorType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-09-05.
 */
public class MyView {
    public static final int CONTENT = ViewGroup.LayoutParams.WRAP_CONTENT;
    public static final int PARENT = ViewGroup.LayoutParams.MATCH_PARENT;
    public static final int HORIZONTAL = LinearLayout.HORIZONTAL;
    public static final int VERTICAL = LinearLayout.VERTICAL;
    public static final int NO_DIRECTION = -1;
    public static final int TOP = 0;
    public static final int BOTTOM = 1;
    public static final int LEFT = Gravity.LEFT; // 3
    // usage : container.addView(MyView.createImageView(...), MyView.getParams(context, 60, 80));

    public static View createLine(Context context, int heightDp) {
        View result = new View(context);
        result.setLayoutParams(getParams(context, PARENT, heightDp));
        result.setBackgroundColor(ColorType.LightGrey.colorInt);
        return result;
    }

    public static ListView createListView(Context context) {
        ListView result = new ListView(context);
        result.setLayoutParams(getParamsParent());
        return result;
    }

    public static List<View> getChildViews(ViewGroup view) {
        List<View> results = new ArrayList<>();
        for (int index = 0; index < view.getChildCount(); index++)
            results.add(view.getChildAt(index));
        return results;
    }

    public static <T> T getView(Context context, View view, String idStr) {
        int id = context.getResources().getIdentifier(idStr, "id", context.getPackageName());
        if (id == 0) {
            Log.e("DEBUG_JS", String.format("[MyView.getView] %s not found", idStr));
            return null;
        } else {
            T result = (T) view.findViewById(id);
            ((View) result).setId(id);
            return result;
        }
    }

    public static void setEnabled(ImageView imageView, boolean enabled) {
        if (enabled) {
            imageView.setColorFilter(null);
            imageView.setImageAlpha(255);
        } else {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);  //0 means grayscale
            ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
            imageView.setColorFilter(cf);
            imageView.setImageAlpha(128);   // 128 = 0.5
        }
    }

    public static void setColorFilter(View view, ColorType colorType) {
        view.getBackground().setColorFilter(colorType.colorInt, PorterDuff.Mode.MULTIPLY);
    }

    public static void setBlueColorFilter(View view) {
        view.getBackground().setColorFilter(ColorType.SkyBlue.colorInt, PorterDuff.Mode.MULTIPLY);
    }

    public static void removeColorFilter(View view) {
        view.getBackground().setColorFilter(null);
    }

    public static void setAsNormalFont(Context context, TextView... textviews) {
        float normalFontScale = (float) 1.0;
        float largeFontScale = (float) 1.3;
        if (MyDevice.getFontScale(context) >= largeFontScale) {
            for (TextView textview : textviews) {
                float originalSize = MyDevice.toOriginalSP(context, textview.getTextSize()); //  font sp  . 18sp가 23.3sp로 높아짐
                textview.setTextSize((float) (originalSize - 5));
            }
        }
    }

    public static LinearLayout createContainer(Context context, int orientation, int widthDp, int heightDp) {
        LinearLayout result = createContainer(context, orientation, 0, -1, -1);
        result.setLayoutParams(getParams(context, widthDp, heightDp));
        return result;
    }

    public static LinearLayout createContainer(Context context, int orientation, int padding, int color, int gravity) {
        LinearLayout result = new LinearLayout(context);
        result.setOrientation(orientation); // LinearLayout.VERTICAL
        if (color > 0)
            result.setBackgroundColor(color);
        if (gravity > 0)
            result.setGravity(gravity); // Gravity.CENTER | Gravity.BOTTOM
        int px = MyDevice.toPixel(context, padding);
        result.setPadding(px, px, px, px);
        result.setLayoutParams(getParams());
        return result;
    }

    public static ProgressDialog getProgressDialog(Context context, String message) {
        if (context instanceof Activity == false)
            Log.e("DEBUG_JS", String.format("[MyView.getProgressDialog] not Activity %s", context.getClass().getSimpleName()));
        ProgressDialog result = new ProgressDialog(context);
        result.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        result.setMessage(message);
        result.show();
        return result;
    }

    public static RadioButton createRadioButton(Context context, String text, int textSize) {
        return createRadioButton(context, text, textSize, false);
    }

    public static RadioButton createRadioButton(Context context, String text, int textSize, boolean checked) {
        RadioButton radioButton = new RadioButton(context);
        radioButton.setText(text);
        if (textSize >= 1)
            radioButton.setTextSize(textSize);
        radioButton.setChecked(checked);
        radioButton.setLayoutParams(getParams());
        return radioButton;
    }

    public static CheckBox createCheckBox(Context context, String text, int textSize) {
        CheckBox checkBox = new CheckBox(context);
        checkBox.setText(text);
        if (textSize > 0)
            checkBox.setTextSize(textSize);
        checkBox.setTextColor(context.getResources().getColor(R.color.Black));
        if (MyDevice.getAndroidVersion() <= 21)
            CompoundButtonCompat.setButtonTintList(checkBox, ColorStateList.valueOf(ColorType.CheckBoxColor.colorInt));
        checkBox.setLayoutParams(getParams());
        return checkBox;
    }

    public static Button createButton(Context context, String text, int textSize, int padding) {
        Button button = new Button(context);
        button.setText(text);
        button.setTextSize(textSize);
        int paddingPixel = MyDevice.toPixel(context, padding);
        button.setPadding(paddingPixel, paddingPixel, paddingPixel, paddingPixel);
        button.setLayoutParams(getParams());
        return button;
    }

    public static Button getGridColorButton(Context context, ColorType color, int widthDp, int heightDp, int rowInx, int columnInx) {
        Button button = new Button(context);
        button.setBackgroundColor(color.colorInt);
        button.setText(UserSettingData.getColorName(color));
        button.setTextSize(12);
        button.setTextColor(color.textColor);

        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
        params.width = MyDevice.toPixel(context, widthDp);
        params.height = MyDevice.toPixel(context, heightDp);
        params.setGravity(Gravity.CENTER);
        params.columnSpec = GridLayout.spec(columnInx);
        params.rowSpec = GridLayout.spec(rowInx);
        button.setLayoutParams(params);
        return button;
    }

    public static GridView setGridView(Context context, GridView view, int columnCount, int spacing) {
        view.setNumColumns(columnCount);
        view.setVerticalSpacing((int) MyDevice.toPixel(context, spacing));
        view.setHorizontalSpacing((int) MyDevice.toPixel(context, spacing));
        return view;
    }

    public static ImageView createImageView(Context context, Bitmap bitmap) {
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(bitmap);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(getParams());
        return imageView;
    }

    public static ImageView getImageViewPadding(Context context, int resource, int paddingDP) {
        ImageView imageView = new ImageView(context);
        imageView.setImageResource(resource);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        int paddingPixel = MyDevice.toPixel(context, paddingDP);
        imageView.setPadding(paddingPixel, paddingPixel, paddingPixel, paddingPixel);
        return imageView;
    }

    public static ImageView createImageView(Context context, int resource, int background) {
        ImageView imageView = new ImageView(context);
        imageView.setImageResource(resource);
        imageView.setBackgroundResource(background);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(getParams());
        return imageView;
    }

    public static TableRow getTableRow(Context context, int padding) {
        TableRow row = new TableRow(context);
        int paddingPixel = MyDevice.toPixel(context, padding);
        row.setPadding(paddingPixel, paddingPixel, paddingPixel, paddingPixel);
        return row;
    }

    public static RadioGroup createRadioGroup(Context context, int width, int height, int orientation, int gravity) {
        RadioGroup result = new RadioGroup(context);
        result.setOrientation(orientation);
        result.setGravity(gravity);
        result.setLayoutParams(new RadioGroup.LayoutParams(width, height));
        return result;
    }

    public static TextView createTextView(Context context, String text, int size, int padding, int color) {
        return createTextView(context, text, size, padding, false, color);
    }

    public static TextView createTextView(Context context, String text, int size, int padding, boolean bold) {
        return createTextView(context, text, size, padding, bold, ColorType.Black.colorInt);
    }

    public static TextView createTextView(Context context, String text, int size, int padding, boolean bold, int color) {
        TextView textView = new TextView(context);
        textView.setText(text);
        int paddingPixel = MyDevice.toPixel(context, padding);
        textView.setPadding(paddingPixel, paddingPixel, paddingPixel, paddingPixel);
        textView.setTextSize(size);
        textView.setTextColor(color);
        textView.setLineSpacing(0, 1.2f);
        textView.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        if (bold)
            textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setLayoutParams(getParams());
        return textView;
    }

    public static void setPadding(Context context, View view, int left, int top, int right, int bottom) {
        view.setPadding(MyDevice.toPixel(context, left), MyDevice.toPixel(context, top),
                MyDevice.toPixel(context, right), MyDevice.toPixel(context, bottom));
    }

    public static void adjustWeight(TextView textView1, int weight1, TextView textView2, int weight2, int dWeight) {
        textView1.setLayoutParams(new TableRow.LayoutParams(0,ViewGroup.LayoutParams.WRAP_CONTENT, weight1+dWeight));
        textView2.setLayoutParams(new TableRow.LayoutParams(0,ViewGroup.LayoutParams.WRAP_CONTENT, weight2-dWeight));
    }

    public static View setOnClick(View view, View.OnClickListener onClickListener) {
        view.setClickable(true);
        view.setOnClickListener(onClickListener);
        return view;
    }

    public static TextView setColor(TextView textView, ColorType colorType, int backgroundColorResId) {
        textView.setTextColor(colorType.colorInt);
        if (backgroundColorResId > 0)
            textView.setBackgroundResource(backgroundColorResId);
        return textView;
    }

    public static LinearLayout.LayoutParams getParams() {
        return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    public static LinearLayout.LayoutParams getParamsWithMargin(Context context, int leftDp, int topDp, int rightDp, int bottomDp) {
        LinearLayout.LayoutParams params = getParams();
        return setMarginLinear(context, params, leftDp, topDp, rightDp, bottomDp);
    }

    public static GridLayout.LayoutParams getGridParamsWithMargin(Context context, int widthDp, int heightDp, int leftDp, int topDp, int rightDp, int bottomDp) {
        return new GridLayout.LayoutParams(getParamsWithMargin(context, widthDp, heightDp, leftDp, topDp, rightDp, bottomDp));
    }

    public static LinearLayout.LayoutParams getParamsWithMargin(Context context, int widthDp, int heightDp, int leftDp, int topDp, int rightDp, int bottomDp) {
        LinearLayout.LayoutParams params = getParams(context, widthDp, heightDp);
        return setMarginLinear(context, params, leftDp, topDp, rightDp, bottomDp);
    }

    public static LinearLayout.LayoutParams getParamsParent() {
        return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
    }

    public static LinearLayout.LayoutParams getParamsParent(Context context, int leftDp, int topDp, int rightDp, int bottomDp) {
        LinearLayout.LayoutParams params = getParamsParent();
        return setMarginLinear(context, params, leftDp, topDp, rightDp, bottomDp);
    }

    public static LinearLayout.LayoutParams getParams(Context context, int widthDp, int heightDp) {
        return new LinearLayout.LayoutParams(MyDevice.toPixel(context, widthDp), MyDevice.toPixel(context, heightDp));
    }

    public static LinearLayout.LayoutParams getParams(Context context, int widthDp, int heightDp, int weight) {
        return new LinearLayout.LayoutParams(MyDevice.toPixel(context, widthDp), MyDevice.toPixel(context, heightDp), weight);
    }

    public static LinearLayout.LayoutParams getImageParams(Context context, Bitmap bitmap, int containerWidthMarginDP, int topMarginDP) {
        int imageViewWidth = MyDevice.getWidth(context) - MyDevice.toPixel(context, containerWidthMarginDP * 2);
        int imageViewHeight = (imageViewWidth * bitmap.getHeight()) / bitmap.getWidth();
        return getParamsWithMargin(context, MyDevice.toDP(context, imageViewWidth), MyDevice.toDP(context, imageViewHeight), 0, topMarginDP, 0, 0);
    }

    public static LinearLayout.LayoutParams setMarginLinear(Context context, LinearLayout.LayoutParams params, int leftDp, int topDp, int rightDp, int bottomDp) {
        params.setMargins(MyDevice.toPixel(context, leftDp), MyDevice.toPixel(context, topDp), MyDevice.toPixel(context, rightDp), MyDevice.toPixel(context, bottomDp));
        return params;
    }

    public static <T> T setMargin(Context context, T view, int leftDp, int topDp, int rightDp, int bottomDp) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) ((View) view).getLayoutParams();
        params.setMargins(MyDevice.toPixel(context, leftDp), MyDevice.toPixel(context, topDp), MyDevice.toPixel(context, rightDp), MyDevice.toPixel(context, bottomDp));
        ((View) view).setLayoutParams(params);
        return view;
    }


    public static ImageView getIcon(Context context, int resource, int sizeDp, int paddingDp, int alpha, View.OnClickListener onClickListener) {
        int paddingPixel = MyDevice.toPixel(context, paddingDp);
        ImageView imageView = new ImageView(context);
        imageView.setImageResource(resource);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setLayoutParams(getParams(context, sizeDp, sizeDp));
        imageView.setPadding(paddingPixel, paddingPixel, paddingPixel, paddingPixel);
        imageView.setImageAlpha(alpha);
        imageView.setOnClickListener(onClickListener);
        return imageView;
    }

    public static View addMargin(Context context, View view, int leftDp, int topDp, int rightDp, int bottomDp) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.setMargins(MyDevice.toPixel(context, leftDp), MyDevice.toPixel(context, topDp),
                MyDevice.toPixel(context, rightDp), MyDevice.toPixel(context, bottomDp));
        view.setLayoutParams(params);
        return view;
    }

    public static View createVerEmptySpace(Context context, int heightDp) {
        View view = new View(context);
        LinearLayout.LayoutParams params = getParams(context, MyView.PARENT, MyDevice.toPixel(context, heightDp));
        view.setLayoutParams(params);
        return view;
    }

    public static View createHorEmptySpace(Context context, int widthDp) {
        View view = new View(context);
        LinearLayout.LayoutParams params = getParams(context, MyDevice.toPixel(context, widthDp), MyView.PARENT);
        view.setLayoutParams(params);
        return view;
    }

    public static void setIconSizeByDeviceSize(Context context, List<ImageView> imageViews) {
        setIconSizeByDeviceSize(context, imageViews.toArray(new ImageView[imageViews.size()]));
    }

    public static void setIconSizeByDeviceSize(Context context, ImageView... imageViews) {
        if (imageViews.length <= 0) return;
        for (ImageView imageView : imageViews) {
            int imageSize = imageView.getLayoutParams().width;
            switch (MyDevice.getDeviceSize(context)) {
                default:
                    continue;
                case W600:
                case W720:
                case W800:
                    imageSize = imageSize + MyDevice.toPixel(context, 5); // +5dp
                    imageView.getLayoutParams().width = imageSize;
                    imageView.getLayoutParams().height = imageSize;
                    break;
            }
        }
    }

    public static void setImageViewSizeByDeviceSize(Context context, ImageView... imageViews) {
        setImageViewSizeByDeviceSizeScale(context, 1, imageViews);
    }


    public static void setImageViewSizeByDeviceSizeScale(Context context, View view, double ratio, Integer... resIds) {
        List<ImageView> imageViewList = new ArrayList<>();
        for (int resId : resIds) {
            ImageView imageView = (ImageView) view.findViewById(resId);
            if (imageView != null)
                imageViewList.add(imageView);
        }
        setImageViewSizeByDeviceSizeScale(context, ratio, imageViewList.toArray(new ImageView[imageViewList.size()]));
    }

    public static void setImageViewSizeByDeviceSizeScale(Context context, double ratio, ImageView... imageViews) {
        if (imageViews.length <= 0) return;
        for (ImageView imageView : imageViews) {
            double scale = 1.0;
            MyDevice.DeviceSize myDeviceSize = MyDevice.getDeviceSize(context);
            if (myDeviceSize.size <= 360) {

            } else if (myDeviceSize.size < 600) {
                scale = scale + 0.1 * ratio;
            } else if (myDeviceSize.size < 720) {
                scale = scale + 0.2 * ratio;
            } else if (myDeviceSize.size < 800) {
                scale = scale + 0.3 * ratio;
            } else if (myDeviceSize.size < 5000) {
                scale = scale + 0.4 * ratio;
            }
            imageView.getLayoutParams().width = (int) (imageView.getLayoutParams().width * scale);
            imageView.getLayoutParams().height = (int) (imageView.getLayoutParams().height * scale);
        }
    }

    // 최초 1회만 해야 하는 것이 중요, 여러번 반복하면 계속 나누기가 중복 적용되는 문제가 있음
    private static void ignoreFontScale(Context context, TextView... textViews) {
        float fontScale = MyDevice.getFontScale(context);
        if (fontScale == 1 || fontScale <= 0) return;

        for (TextView textView : textViews) {
            float fontSize = MyDevice.toOriginalSP(context, textView.getTextSize());
            textView.setTextSize(fontSize / fontScale); // setTextSize 하는 순간 Device의 scale이 다시 적용되기 때문
            Log.i("DEBUG_JS", String.format("[MyView.ignoreFontScale] %f  / %f = %f", fontSize, fontScale, fontSize / fontScale));
        }
    }

    public static void setFontSize(float fontSize, TextView... textViews) {
        for (TextView textView : textViews)
            textView.setTextSize(fontSize);
    }

    public static void increaseFontSize(Context context, int plusSize, TextView... textViews) {
        if (textViews.length <= 0) return;
        for (TextView textView : textViews)
            setFontSize(MyDevice.toOriginalSP(context, textView.getTextSize()) + plusSize, textView);
    }

    public static void setTextViewByDeviceSize(Context context, LinearLayout linearLayout) {
        List<TextView> textViews = new ArrayList<>();
        for (View view : MyView.getChildViews(linearLayout))
            if (view instanceof TextView)
                textViews.add((TextView) view);
        MyView.setTextViewByDeviceSize(context, textViews.toArray(new TextView[textViews.size()]));
    }

    public static void setTextViewByDeviceSize(Context context, TextView... textViews) {
        setTextViewByDeviceSizeScale(context, 1.0, textViews);
    }

    public static void setTextViewByDeviceSize(Context context, TextView textView) {
        setTextViewByDeviceSizeScale(context, 1.0, textView);
    }

//    public static void setTextViewByDeviceSize(Context context, TextView textView, int fontSize) {
//        setFontSizeByDeviceSizeScale(context, 1.0, textViews);
//        setMarginByDeviceSizeScale(context, 1.0, textView);
//        setPaddingByDeviceSizeScale(context, 1.0, textView);
//    }

    public static void setTextViewByDeviceSizeScale(Context context, double scale, TextView... textViews) {
        setFontSizeByDeviceSizeScale(context, scale, textViews);
        setMarginByDeviceSizeScale(context, scale, textViews);
        setPaddingByDeviceSizeScale(context, scale, textViews);
    }

    public static void setFontSizeByDeviceSize(Context context, TextView textView) {
        setFontSizeByDeviceSizeScale(context, 1.0, textView);
    }

    public static void setFontSizeByDeviceSize(Context context, TextView... textViews) {
        setFontSizeByDeviceSizeScale(context, 1.0, textViews);
    }

    // 주의! (... double scale, int...resIds)가 연달아 나오지 않게.. 던지는 쪽에 resIds만 던져도 구분못한다
    public static void setFontSizeByDeviceSize(Context context, View view, int... resIds) {
        setFontSizeByDeviceSizeScale(context, view, 1.0, resIds);
    }

    public static void setFontSizeByDeviceSizeScale(Context context, View view, double scale, int... resIds) {
        List<TextView> textViews = new ArrayList<>();
        for (int resId : resIds)
            textViews.add((TextView) view.findViewById(resId));
        setFontSizeByDeviceSizeScale(context, scale, textViews.toArray(new TextView[textViews.size()]));
    }

    private static void setFontSizeByDeviceSizeScale(Context context, double scale, TextView... textViews) {
        if (textViews.length <= 0) {
            Log.w("DEBUG_JS", String.format("[MyView.setFontSizeByDeviceSizeScale] textViews size == 0, %s", context.getClass().getSimpleName()));
            return;
        }

        for (TextView textView : textViews) {
            float fontSize = MyDevice.toOriginalSP(context, textView.getTextSize());
            switch (MyDevice.getDeviceSize(context)) {
                default:
                    break;
                case W600:
                    fontSize = fontSize + (float) (2 * scale);
                    break;
                case W720:
                    fontSize = fontSize + (float) (3 * scale);
                    break;
                case W800:
                    fontSize = fontSize + (float) (4 * scale);
                    break;
            }
            setFontSize(fontSize, textView);
        }
    }

    public static void setTextViewByDeviceSize(Context context, View view, int... resIds) {
        setTextViewByDeviceSizeScale(context, view, 1.0, resIds);
    }

    public static void setTextViewByDeviceSizeScale(Context context, View view, double scale, int... resIds) {
        setFontSizeByDeviceSizeScale(context, view, scale, resIds);
        setMarginByDeviceSizeScale(context, view, scale, resIds);
    }


    public static void setMarginByDeviceSize(Context context, View view, int... resIds) {
        setMarginByDeviceSizeScale(context, view, 1.0, resIds);
    }

    public static void setMarginByDeviceSizeScale(Context context, View view, double scale, int... resIds) {
        List<TextView> textViews = new ArrayList<>();
        for (int resId : resIds)
            textViews.add((TextView) view.findViewById(resId));
        setMarginByDeviceSizeScale(context, scale, textViews.toArray(new TextView[resIds.length]));
    }

    public static void setMarginByDeviceSize(Context context, TextView... textViews) {
        setMarginByDeviceSizeScale(context, 1.0, textViews);
    }

    public static void setMarginByDeviceSizeScale(Context context, double scale, TextView... textViews) {
        if (textViews.length <= 0) return;
        if (MyDevice.isLessThan(context, MyDevice.DeviceSize.W411))
            return;
        double compensateScale = 0.5; // 1.0이면 너무 크게
        double ratio = MyDevice.getDeviceSize(context).size / 360.0;
        double incremental = (ratio - 1.0) * scale * compensateScale;
        ratio = 1.0 + incremental;
        // ex) 1.7 = 1.0 + 0.7 * scale

        for (TextView textView : textViews) {
            try {
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) textView.getLayoutParams();
                params.leftMargin = (int) (params.leftMargin * ratio);
                params.topMargin = (int) (params.topMargin * ratio);
                params.bottomMargin = (int) (params.bottomMargin * ratio);
                params.rightMargin = (int) (params.rightMargin * ratio);
                textView.setLayoutParams(params);
            } catch (ClassCastException e) {
                //
            }
        }
    }

    public static void setPaddingByDeviceSizeScale(Context context, View... views) {
        if (MyDevice.isLessThan(context, MyDevice.DeviceSize.W411))
            return;
        double compensateScale = 0.5; // 1.0이면 너무 크게 패딩이 잡혀서
        double ratio = MyDevice.getDeviceSize(context).size / 360.0;
        double incremental = (ratio - 1.0) * compensateScale;
        ratio = 1.0 + incremental;

        for (View view : views) {
            int paddingLeft = (int) (view.getPaddingLeft() * ratio);
            int paddingTop = (int) (view.getPaddingTop() * ratio);
            int paddingBottom = (int) (view.getPaddingBottom() * ratio);
            int paddingRight = (int) (view.getPaddingRight() * ratio);
            view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        }
    }

    public static void setPaddingByDeviceSizeScale(Context context, double scale, TextView... textViews) {
        if (textViews.length <= 0) return;
        if (MyDevice.isLessThan(context, MyDevice.DeviceSize.W411))
            return;
        double compensateScale = 0.5; // 1.0이면 너무 크게 패딩이 잡혀서
        double ratio = MyDevice.getDeviceSize(context).size / 360.0;
        double incremental = (ratio - 1.0) * scale * compensateScale;
        ratio = 1.0 + incremental;

        for (TextView textView : textViews) {
            int paddingLeft = (int) (textView.getPaddingLeft() * ratio);
            int paddingTop = (int) (textView.getPaddingTop() * ratio);
            int paddingBottom = (int) (textView.getPaddingBottom() * ratio);
            int paddingRight = (int) (textView.getPaddingRight() * ratio);
            textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        }
    }

    public static void setImageSize(Context context, int iconSizeDp, ImageView imageView) {
        imageView.getLayoutParams().width = MyDevice.toPixel(context, iconSizeDp);
        imageView.getLayoutParams().height = MyDevice.toPixel(context, iconSizeDp);
    }

    public static FrameLayout getImageFrame(Context context, ImageView imageView, LinearLayout iconContainer) {
        final FrameLayout frameLayout = new FrameLayout(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //params.setMargins(MyDevice.toPixel(context, leftDp), MyDevice.toPixel(context, topDp), MyDevice.toPixel(context, rightDp), MyDevice.toPixel(context, bottomDp));
        // not working
        params.gravity = Gravity.TOP;
        frameLayout.setForegroundGravity(Gravity.TOP);
        frameLayout.setLayoutParams(params);

        if (imageView.getLayoutParams() == null)
            imageView.setLayoutParams(MyView.getParamsParent());
        frameLayout.addView(imageView);

        iconContainer.setGravity(Gravity.RIGHT);
        frameLayout.addView(iconContainer);
        return frameLayout;
    }

    public static LinearLayout getIconContainer(Context context) {
        LinearLayout iconContainer = MyView.createContainer(context, MyView.HORIZONTAL, MyView.PARENT, MyView.CONTENT);
        iconContainer.setGravity(Gravity.RIGHT);
        iconContainer = (LinearLayout) addMargin(context, iconContainer, 0, 5, 10, 0); // not working
        int paddingDp = MyDevice.toPixel(context, 5);
        iconContainer.setPadding(0, paddingDp, paddingDp, 0);
        return iconContainer;
    }

}
