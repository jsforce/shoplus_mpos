package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.enums.DBResultType;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class MonthlyCounterpartDB extends BaseDB<MonthlyCounterpartRecord> {

    private static MonthlyCounterpartDB instance = null;
    private static int Ver = 1;

    public enum Column {
        _id, phoneNumber, name, amount, quantity, monthDateTime, isSumup;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    private MonthlyCounterpartDB(Context context) {
        super(context, "MonthlyCounterpartDB", Ver, Column.toStrs());
    }

    public static MonthlyCounterpartDB getInstance(Context context) {
        if (instance == null)
            instance = new MonthlyCounterpartDB(context);
        return instance;
    }

    protected ContentValues toContentvalues(MonthlyCounterpartRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.phoneNumber.name(), record.phoneNumber);
        contentvalues.put(Column.name.name(), record.name);
        contentvalues.put(Column.amount.name(), record.amount);
        contentvalues.put(Column.quantity.name(), record.quantity);
        contentvalues.put(Column.monthDateTime.name(), record.monthDateTime.toString());
        contentvalues.put(Column.isSumup.name(), record.isSumup);
        return contentvalues;
    }


    public DBResult updateOrInsert(MonthlyCounterpartRecord record) {
        // key : phoneNumber, monthDate, isSumup
        List<QueryAndRecord> queryAndRecords = new QueryAndRecord(Column.phoneNumber, record.phoneNumber).toList();
        queryAndRecords.add(new QueryAndRecord(Column.monthDateTime, record.monthDateTime.toString()));
        queryAndRecords.add(new QueryAndRecord(Column.isSumup, record.isSumup));
        MonthlyCounterpartRecord dbRecord = getRecordByQuery(queryAndRecords);
        if (dbRecord.id > 0) {
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    @Override
    protected int getId(MonthlyCounterpartRecord record) {
        return record.id;
    }

    @Override
    protected MonthlyCounterpartRecord getEmptyRecord() {
        return new MonthlyCounterpartRecord();
    }

    @Override
    protected MonthlyCounterpartRecord createRecord(Cursor cursor) {
        return new MonthlyCounterpartRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.phoneNumber.ordinal()),
                cursor.getString(Column.name.ordinal()),
                cursor.getInt(Column.amount.ordinal()),
                cursor.getInt(Column.quantity.ordinal()),
                BaseUtils.toDateTime(cursor.getString(Column.monthDateTime.ordinal())),
                BaseUtils.toBoolean(cursor.getInt(Column.isSumup.ordinal()))
        );
    }

    public void deleteInvalids(int year, int month, List<String> validPhoneNumbers) {
        for (MonthlyCounterpartRecord record : getRecordsBy(Column.monthDateTime, year, month)) {
            if (validPhoneNumbers.contains(record.phoneNumber) == false)
                delete(record.id);
        }
    }

    public void refresh(int year, int month, List<MonthlyCounterpartRecord> records) {
        List<String> validPhoneNumbers = new ArrayList<>();
        for (MonthlyCounterpartRecord record : records) {
            updateOrInsert(record);
            validPhoneNumbers.add(record.phoneNumber);
        }
        deleteInvalids(year, month, validPhoneNumbers);
    }

}