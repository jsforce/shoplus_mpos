package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseUserDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_global.AddressbookDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.composition.UserComposition;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-02-22.
 */

public class MyAddressbookView extends BaseActivity implements AdapterView.OnItemClickListener {
    private AddressbookAdapter adapter;
    private ListView listView;
    private EditText etName;
    private List<UserRecord> fullRecords;
    private AddressbookDB addressbookDB;
    private UserComposition userComposition;
    private static MyOnTask onFinish;

    public static void startActivity(Context context, MyOnTask onFinish) {
        MyAddressbookView.onFinish = onFinish;
        context.startActivity(new Intent(context, MyAddressbookView.class));
    }

    @Override
    public int getThisView() {
        return R.layout.base_user_search;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        userComposition = new UserComposition(context);
        userComposition.cleanUp();
        addressbookDB = AddressbookDB.getInstance(context);

        adapter = new AddressbookAdapter(context);
        adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_MULTIPLE);

        etName = (EditText) findViewById(R.id.user_search_name);
        etName.addTextChangedListener(new MyTextWatcher());
        listView = (ListView) findViewById(R.id.user_search_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        setOnClick(R.id.user_search_clear, R.id.user_search_cancel, R.id.user_search_apply);
        setVisibility(View.GONE, R.id.user_search_add);
        setVisibility(View.VISIBLE, R.id.user_search_buttonContainer);

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.READ_CONTACTS) == false) {
            MyUI.toast(context, String.format("주소록 권한이 없습니다. 설정 > 내정보 > 권한 에서 등록해주세요"));
            return;
        }

        if (addressbookDB.isEmpty())
            userComposition.syncAddressBook(new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    setFullRecords();
                }
            });
        else {
            setFullRecords();
            MyUI.toastSHORT(context, String.format("최근 추가한 주소가 있으면 새로고침을 해주세요"));
        }
    }



    private void setFullRecords() {
        fullRecords = addressbookDB.getRecordsOrderBy(UserDB.Column.name, false);
        adapter.setRecords(fullRecords);
        refresh();
    }

    public void refresh() {
        if (adapter == null) return;
        new MyAsyncTask(context, "refreshDay") {
            @Override
            public void doInBackground() {
                adapter.generate();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
                myActionBar.setTitle(String.format("%d/%d", adapter.clickedPositions.size(), fullRecords.size()), true);
            }
        };
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case refresh:
                userComposition.syncAddressBook(new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        setFullRecords();
                    }
                });
                break;
            case close:
                finish();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.user_search_clear) {
            etName.setText("");
        } else if (view.getId() == R.id.user_search_cancel) {
            finish();
        } else if (view.getId() == R.id.user_search_apply) {
            final List<UserRecord> clickedRecords = adapter.getSortedClickedRecords();
            if (clickedRecords.size() <= 0) {
                MyUI.toastSHORT(context, String.format("선택된 거래처가 없습니다"));
                return;
            }

            new MyAlertDialog(context, "신규 거래처 등록", String.format("%d 거래처를 신규로 등록 하시겠습니까?", clickedRecords.size())) {
                @Override
                public void yes() {
                    userComposition.updateUsers("MyAddressbookView", clickedRecords, new MyOnTask() {
                        @Override
                        public void onTaskDone(Object result) {
                            MyUI.toastSHORT(context, "거래처를 등록했습니다");
                            if (onFinish != null)
                                onFinish.onTaskDone("");
                            finish();
                        }
                    });
                }
            };
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
        adapter.onItemClick(view, position);
        myActionBar.setTitle(String.format("%d/%d", adapter.getSortedClickedRecords().size(), adapter.getCount()), true);
    }

    class MyTextWatcher extends BaseTextWatcher {
        @Override
        public void afterTextChanged(Editable s) {
            List<UserRecord> results = new ArrayList<>();
            List<UserRecord> clickedRecords = adapter.getSortedClickedRecords();
            String keyword = s.toString().toLowerCase();

            for (UserRecord record : fullRecords) {
                if (clickedRecords.contains(record)) {
                    results.add(record);
                } else if (record.name.toLowerCase().contains(keyword) || record.phoneNumber.contains(s)) {
                    results.add(record);
                }
            }
            adapter.setRecords(results);

            // position이 모두 달라졌으므로 clicked item 위치도 새로 파악해야 한다
            adapter.clearClicked();
            for (int position = 0; position < adapter.getCount(); position++) {
                if (clickedRecords.contains(adapter.getRecord(position))) {
                    adapter.setClickedPosition(position);
                }
            }
            refresh();
        }
    }

    class AddressbookAdapter extends BaseUserDBAdapter {

        public AddressbookAdapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new DefaultCheckableViewHolder() {
                @Override
                public void update(int position) {
                    super.update(position);
                    if (clickedPositions.contains(position)) {
                        container.setBackgroundColor(ColorType.Khaki.colorInt);
                    } else {
                        container.setBackgroundColor(Color.TRANSPARENT);
                    }
                }
            };
        }
    }

}
