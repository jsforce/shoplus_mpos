package com.teampls.shoplus.lib.awsservice.s3;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author lucidite
 */

public class ImageFilePathHelper {
    private static DateTimeFormatter remoteFilePathDateFormatter = DateTimeFormat.forPattern("yy/MM/dd");
    private static DateTimeFormatter remoteFilePathTimeFormatter = DateTimeFormat.forPattern("HHmmssSSS");
    private final static String ext = ".jpg";
    private final static int divider = 100000;

    public static String genRemoteFilePath(ImageUploadType imageType, DateTime timestamp, String userid) {
        String useridStr;
        try {
            int useridValue = Integer.valueOf(userid);
            int idPart1Value = useridValue % divider;
            int idPart2Value = useridValue / divider;
            useridStr = String.format("%04x%05x", idPart1Value, idPart2Value);
        } catch (NumberFormatException e) {
            useridStr = userid;
        }
        String rand = Long.toHexString(Double.doubleToLongBits(Math.random())).substring(0, 6);
        String basepath = imageType.getBasePath() + remoteFilePathDateFormatter.print(timestamp) + "/" + useridStr + "/";
        String filename = remoteFilePathTimeFormatter.print(timestamp) + "_" + rand;
        return basepath + filename + ext;
    }
}
