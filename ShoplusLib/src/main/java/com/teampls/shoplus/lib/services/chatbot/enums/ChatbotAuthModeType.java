package com.teampls.shoplus.lib.services.chatbot.enums;

/**
 * Chatbot Auth Mode 설정 타입
 *
 * @author lucidite
 */
public enum ChatbotAuthModeType {
    AUTH_REQUIRED(true, "인증모드"),
    AUTH_FREE(false, "해제모드");

    private boolean remoteValue;
    private String uiStr;

    public static ChatbotAuthModeType DEFAULT_MODE = ChatbotAuthModeType.AUTH_FREE;

    ChatbotAuthModeType(boolean remoteValue, String uiStr) {
        this.remoteValue = remoteValue;
        this.uiStr = uiStr;
    }

    public static ChatbotAuthModeType fromRemoteStr(boolean remoteValue) {
        for (ChatbotAuthModeType mode: ChatbotAuthModeType.values()) {
            if (mode.remoteValue == remoteValue) {
                return mode;
            }
        }
        return DEFAULT_MODE;
    }

    public boolean toRemoteValue() {
        return remoteValue;
    }

    public String toUiStr() {
        return uiStr;
    }
}
