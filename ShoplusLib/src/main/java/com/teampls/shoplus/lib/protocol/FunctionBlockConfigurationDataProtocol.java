package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.enums.FunctionBlockConfigurationType;

/**
 * 특정 기능을 블록하는 설정 데이터 프로토콜
 *
 * @author lucidite
 */
public interface FunctionBlockConfigurationDataProtocol {
    /**
     * 특정 기능이 차단 설정되었는지를 반환한다.
     *
     * @param function
     * @return  전역 차단 설정된 경우 true, 허용된 경우 false
     */
    boolean isBlocked(FunctionBlockConfigurationType function);
}
