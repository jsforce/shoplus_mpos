package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.enums.FunctionBlockConfigurationType;
import com.teampls.shoplus.lib.protocol.FunctionBlockConfigurationDataProtocol;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author lucidite
 */
public class FunctionBlockConfigurationData implements FunctionBlockConfigurationDataProtocol {
    private Map<FunctionBlockConfigurationType, Boolean> blockConfigurations;

    public FunctionBlockConfigurationData(JSONObject dataObj) {
        this.blockConfigurations = new HashMap<>();

        if (dataObj == null) {
            return;
        }

        Iterator<String> functionNames = dataObj.keys();
        while(functionNames.hasNext()) {
            String functionName = functionNames.next();
            FunctionBlockConfigurationType function = FunctionBlockConfigurationType.fromRemoteStr(functionName);
            if (function != FunctionBlockConfigurationType.NONE) {
                boolean isBlocked = dataObj.optBoolean(functionName, false);
                this.blockConfigurations.put(function, isBlocked);
            }
        }
    }

    @Override
    public boolean isBlocked(FunctionBlockConfigurationType function) {
        Boolean result;
        if (this.blockConfigurations.containsKey(function)) {
            result = this.blockConfigurations.get(function);
        } else {
            result = false;
        }
        return (result == null) ? false : result;
    }
}
