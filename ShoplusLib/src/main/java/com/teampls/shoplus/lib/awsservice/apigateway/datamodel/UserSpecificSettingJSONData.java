package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import com.teampls.shoplus.lib.awsservice.apigateway.protocol.APIInputModelInterface;
import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.protocol.UserSpecificSettingProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 사용자별 설정 데이터. 로그인한 사용자의 설정을 반환한다.
 *
 * @author lucidite
 */

public class UserSpecificSettingJSONData implements UserSpecificSettingProtocol, JSONDataWrapper, APIInputModelInterface {
    private JSONObject data;

    public UserSpecificSettingJSONData(JSONObject dataObj) {
        if (dataObj != null) {
            this.data = dataObj;
        } else {
            this.data = new JSONObject();
        }
    }

    public UserSpecificSettingJSONData(UserSpecificSettingProtocol otherSetting) {
        this.data = new JSONObject();
        this.setShop(otherSetting.getShop());
    }

    @Override
    public String getShop() {
        return data.optString("shop", "");
    }

    @Override
    public void setShop(String shop) {
        try {
            if (shop.isEmpty()) {
                data.remove("shop");
            } else {
                data.put("shop", shop);
            }
        } catch (JSONException e) {
            // do nothing
        }
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return this.data;
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("UserSpecificSettingJSONData instance does not have a JSON array");
    }

    @Override
    public String toInputModelString() {
        return data.toString();
    }
}
