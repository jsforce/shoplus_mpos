package com.teampls.shoplus.lib.enums;

import android.support.annotation.Nullable;

/**
 * 고객 거래처의 도매/소매 타입 (기본값은 도매)
 *
 * @author lucidite
 */
public enum ContactBuyerType {
    WHOLESALE("w"),
    RETAIL("r");

    private String remoteConfigStr;

    ContactBuyerType(String remoteConfigStr) {
        this.remoteConfigStr = remoteConfigStr;
    }

    public String toUiStr() {
        if (this == WHOLESALE) {
            return "도매가";
        } else {
            return "소매가";
        }
    }

    public ColorType getColor() {
        if (this == WHOLESALE) {
            return ColorType.FlatRedDark;
        } else {
            return ColorType.FlatBlueDark;
        }
    }

    public String toRemoteConfigStr() {
        return this.remoteConfigStr;
    }

    public static ContactBuyerType fromRemoteRetailFlagValue(boolean isRetail) {
        return isRetail ? RETAIL : WHOLESALE;
    }

    /**
     * 서버에 설정된 설정 문자열(w, r)로부터 고객 유형 설정값을 반환한다.
     * [NOTE] 올바르지 않은 문자열인 경우 null을 반환한다. (Nullable임에 유의)
     *
     * @param remoteConfigStr
     * @return
     */
    @Nullable
    public static ContactBuyerType fromRemoteConfigStr(String remoteConfigStr) {
        if (remoteConfigStr == null || remoteConfigStr.isEmpty()) {
            return null;
        }
        for (ContactBuyerType type: ContactBuyerType.values()) {
            if (type.remoteConfigStr.equals(remoteConfigStr)) {
                return type;
            }
        }
        return null;            // nullable
    }

    public static ContactBuyerType valueOfWithDefault(String name) {
        try {
            return valueOf(name);
        } catch (IllegalArgumentException e) {
            return ContactBuyerType.WHOLESALE;
        }
    }

    public static ContactBuyerType DEFAULT_TYPE() {
        return WHOLESALE;
    }


}
