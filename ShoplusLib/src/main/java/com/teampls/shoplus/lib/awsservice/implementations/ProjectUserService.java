package com.teampls.shoplus.lib.awsservice.implementations;

import android.os.Messenger;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ProjectUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.ContactJSONData;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.UserSettingJSONData;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.datatypes.MessengerLinkData;
import com.teampls.shoplus.lib.datatypes.MyShopInformationData;
import com.teampls.shoplus.lib.datatypes.ServiceActivationResult;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.MessengerType;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 사용자 체크 관련 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectUserService implements ProjectUserServiceProtocol {

    private String getMyServiceActivationResourcePath(String phoneNumber, ShoplusServiceType service) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "activation", service.toRemoteStr());
    }

    private String getTempServiceActivationResourcePath(String phoneNumber, ShoplusServiceType service) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "activation", service.toRemoteStr(), "temp");
    }

    private String getMyContactInfoResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "contact");
    }

    private String getContactUpdatesResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "contact", "updates");
    }

    private String getContactInfoSearchResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "contact", "search");
    }

    private String getAContactInfoResourecPath(String phoneNumber, String contact) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "contact", contact);
    }

    public String getMyShopInformationResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "user-setting", "shop-info");
    }

    public String getMyMessengerLinksResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "user-setting", "messenger-links");
    }

    private String getPhoneNumberForUserService(UserShopInfoProtocol userShop) {
        return userShop.getMyShopOrUserPhoneNumber();     // User Service에서는 메인 샵 정보를 사용하지 않는다.
    }

    @Override
    public Boolean checkServiceActivation(UserShopInfoProtocol userShop, ShoplusServiceType service) throws MyServiceFailureException {
        try {
            String resource = this.getMyServiceActivationResourcePath(getPhoneNumberForUserService(userShop), service);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            JSONObject result = new JSONObject(response);
            if (result.equals(JSONObject.NULL)) {
                return null;
            } else {
                return Boolean.valueOf(result.optBoolean("activated", false));
            }
        } catch (JSONException e) {
            //throw new MyServiceFailureException(e);
            return null;
        }
    }

    private ServiceActivationResult requestActivation(UserShopInfoProtocol userShop, ShoplusServiceType service, JSONObject params) throws JSONException, MyServiceFailureException {
        String resource = this.getTempServiceActivationResourcePath(getPhoneNumberForUserService(userShop), service);
        String response = APIGatewayClient.requestPOST(resource, params.toString(), UserHelper.getIdentityToken());
        return new ServiceActivationResult(new JSONObject(response));
    }


    @Override
    public ServiceActivationResult requestTempActivation(UserShopInfoProtocol userShop, ShoplusServiceType service, Boolean allowContact, Boolean demoRequested) throws MyServiceFailureException {
        try {
            JSONObject requestBody = new JSONObject()
                    .put("allow_contact", allowContact)
                    .put("req_demo", demoRequested);
            return this.requestActivation(userShop, service, requestBody);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public ServiceActivationResult activateWithRedeemCode(UserShopInfoProtocol userShop, ShoplusServiceType service, String redeemCode, String shopName) throws MyServiceFailureException {
        try {
            JSONObject requestBody = new JSONObject()
                    .put("redeem", redeemCode);
            if (shopName != null && !shopName.isEmpty()) {
                requestBody.put("shop_name", shopName);
            }
            return this.requestActivation(userShop, service, requestBody);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public ServiceActivationResult activateMPOSService(UserShopInfoProtocol userShop, String shopName, String shopInfo, String introducer) throws MyServiceFailureException {
        try {
            JSONObject requestBody = new JSONObject()
                    .put("shop_name", shopName)
                    .put("shop_info", shopInfo)
                    .put("introducer", (introducer.isEmpty() ? "NONE" : introducer));
            return this.requestActivation(userShop, ShoplusServiceType.MPOS, requestBody);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public Pair<Long, List<ContactDataProtocol>> getContactUpdates(UserShopInfoProtocol userShop, long lastUpdatedTimestamp) throws MyServiceFailureException {
        try {
            String resource = this.getContactUpdatesResourcePath(getPhoneNumberForUserService(userShop));
            Map<String, String> queryParams = new HashMap<>();
            if (lastUpdatedTimestamp > 0) {
                queryParams.put("lut", String.valueOf(lastUpdatedTimestamp));
            }

            String response = APIGatewayClient.requestGET(resource, queryParams, UserHelper.getIdentityToken());
            JSONObject result = new JSONObject(response);
            long newUpdatedTimestamp = result.getLong("timestamp");
            JSONArray updatedContactArr = result.getJSONArray("contacts");
            List<ContactDataProtocol> updatedContacts = new ArrayList<>();
            for (int i = 0; i < updatedContactArr.length(); ++i) {
                updatedContacts.add(new ContactJSONData(updatedContactArr.getJSONObject(i)));
            }
            return new Pair<>(Long.valueOf(newUpdatedTimestamp), updatedContacts);
        } catch (JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public ContactDataProtocol getSpecificContact(UserShopInfoProtocol userShop, String contactNumber) throws MyServiceFailureException {
        try {
            String resource = this.getAContactInfoResourecPath(getPhoneNumberForUserService(userShop), contactNumber);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            return new ContactJSONData(new JSONObject(response));
        } catch (JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public Map<String, ContactDataProtocol> searchContactInfo(UserShopInfoProtocol userShop, Set<String> searchingPhoneNumber) throws MyServiceFailureException {
        try {
            String resource = this.getContactInfoSearchResourcePath(getPhoneNumberForUserService(userShop));
            JSONArray requestBody = new JSONArray(searchingPhoneNumber);
            String response = APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
            return ContactJSONData.fromJSONObject(new JSONObject(response));
        } catch (JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public void updateContactInfo(UserShopInfoProtocol userShop, Map<String, ContactDataProtocol> contactInfo) throws MyServiceFailureException {
        try {
            String resource = this.getMyContactInfoResourcePath(getPhoneNumberForUserService(userShop));
            JSONObject requestBody = new JSONObject();
            for (Map.Entry<String, ContactDataProtocol> c: contactInfo.entrySet()) {
                requestBody.put(c.getKey(), new ContactJSONData(c.getValue()).getObject());
            }
            APIGatewayClient.requestPOST(resource, requestBody.toString(), UserHelper.getIdentityToken());
        } catch (JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public void mergeContacts(UserShopInfoProtocol userShop, String acquirer, String predecessor) throws MyServiceFailureException {
        try {
            String resource = this.getAContactInfoResourecPath(getPhoneNumberForUserService(userShop), acquirer);

            JSONObject requestBody = new JSONObject();
            requestBody.put("operation", "merge");

            JSONObject dataObj = new JSONObject();
            dataObj.put("predecessor", predecessor);
            requestBody.put("data", dataObj);

            APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
            //return ContactJSONData.fromJSONObject(new JSONObject(response));
        } catch (JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public UserSettingDataProtocol updateMyShopInformation(UserShopInfoProtocol userShop, MyShopInformationData shopInformation) throws MyServiceFailureException {
        try {
            String resource = this.getMyShopInformationResourcePath(getPhoneNumberForUserService(userShop));
            JSONObject requestBodyObj = new JSONObject()
                    .put("value", shopInformation.getObject());
            String response = APIGatewayClient.requestPUT(resource, requestBodyObj.toString(), UserHelper.getIdentityToken());
            return new UserSettingJSONData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public void updateMessengerLink(UserShopInfoProtocol userShop, MessengerType messengerType, String url) throws MyServiceFailureException {
        try {
            String resource = this.getMyMessengerLinksResourcePath(getPhoneNumberForUserService(userShop));
            JSONObject requestBodyObj = new JSONObject().put("type", messengerType.toRemoteStr());
            Log.i("DEBUG_JS", String.format("[ProjectUserService.updateMessengerLink] type %s, url %s, %s", messengerType, url, requestBodyObj));
            if (url != null && !url.isEmpty()) {
                requestBodyObj.put("url", url);
            }
           APIGatewayClient.requestPUT(resource, requestBodyObj.toString(), UserHelper.getIdentityToken());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }
}
