package com.teampls.shoplus.lib.template;

import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.database.MemoRecord;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class TemplateRecord {
    public int id = 0;
    public long draftId = 0, versionId = 0;
    public DateTime createdDateTime = Empty.dateTime, updatedDateTime = Empty.dateTime;
    public String recipientPhoneNumber = "";
    public List<MemoRecord> memos = new ArrayList();

    public boolean isSame(TemplateRecord record) {
        return false;
    }

    public TemplateRecord(){};

    public TemplateRecord(int id, long draftId, long versionId, DateTime createdDateTime, DateTime updatedDateTime,
                          String recipientPhoneNumber, List<MemoRecord> memos) {
        this.id = id;
        this.draftId = draftId;
        this.versionId = versionId;
        this.createdDateTime = createdDateTime;
        this.updatedDateTime = updatedDateTime;
        this.recipientPhoneNumber = recipientPhoneNumber;
        this.memos = memos;
    }
}
