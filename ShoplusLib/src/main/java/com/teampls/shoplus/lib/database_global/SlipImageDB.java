package com.teampls.shoplus.lib.database_global;

import android.content.Context;
import android.database.SQLException;
import android.util.Log;

/**
 * Created by Medivh on 2016-09-03.
 */
public class SlipImageDB extends ImageDB {
    private static SlipImageDB instance = null;

    public static SlipImageDB getInstance(Context context) {
        if (instance == null)
            instance = new SlipImageDB(context);
        return instance;
    }

    private SlipImageDB(Context context) {
        super(context, "SlipImageDB", Ver, Column.toStrs());
        columnAttrs = Column.getAttributions();
    }

    public void open() {
        try {
            helper = new DBHelper(context);
            database = helper.getWritableDatabase();
        } catch (SQLException e) {
            Log.e("DEBUG_JS", String.format("[%s] fail to open %s, %d", getClass().getSimpleName(), DB_TABLE, DB_VERSION));
        }

//        if (isColumnsValid() == false) {
//            Log.e("DEBUG_JS", String.format("[BaseDB.BaseDB] columns are invalid..."));
//            deleteDB();
//        }
    }


}
