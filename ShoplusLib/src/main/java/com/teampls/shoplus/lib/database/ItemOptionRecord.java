package com.teampls.shoplus.lib.database;


import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.ItemOptionData;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemOptionRecord implements Comparable<ItemOptionRecord> {
    public int id = 0, itemId = 0, stock = 0, revStock = 0; // stock 서버에 저장된 값, revisionStock 입력중인 값 (수정중인 경우에 사용)
    public ColorType color = ColorType.Default;
    public SizeType size = SizeType.Default;
    public String key = ""; // 서버에서 받아오는 값은 아니지만 중복 옵션을 방지하기 위해 사용
    public boolean confirmed = true, isSoldOut = false;
    //
    public boolean _deleted = false;
    public MSortingKey _sortingKey = MSortingKey.MultiField;

    public ItemOptionRecord clone() {
        ItemOptionRecord result = new ItemOptionRecord(id, key, itemId, color, size, confirmed, stock, revStock, isSoldOut);
        result._deleted = _deleted;
        result._sortingKey = _sortingKey;
        return result;
    }

    public ItemOptionRecord() {
    }

    public boolean isEmpty() {
        return ((itemId == 0) && (color == ColorType.Default) && (size == SizeType.Default));
    }

    public List<ItemOptionRecord> toList() {
        List<ItemOptionRecord> results = new ArrayList<>();
        results.add(this);
        return results;
    }

    public ItemOptionRecord(int id, String key, int itemId, ColorType color, SizeType size, boolean confirmed,
                            int stock, int revStock, boolean isSoldOut) {
        this.key = key;
        this.id = id;
        this.itemId = itemId;
        this.color = color;
        this.size = size;
        this.confirmed = confirmed;
        this.stock = stock;
        this.revStock = revStock;
        this.isSoldOut = isSoldOut;
    }

    public ItemOptionRecord(int itemId) {
        this.itemId = itemId;
        setKey();
    }

    public ItemOptionRecord(int itemId, ColorType color, SizeType size) {
        this.itemId = itemId;
        this.color = color;
        this.size = size;
        setKey();
    }

    public ItemOptionRecord(int itemId, ColorType color, SizeType size, boolean confirmed) {
        this.itemId = itemId;
        this.color = color;
        this.size = size;
        this.confirmed = confirmed;
        setKey();
    }

    public ItemOptionRecord(ItemStockKey key, ItemOptionData optionData) {
        this.itemId = key.getItemId();
        this.color = (key.getColorOption() == null ? ColorType.Default : key.getColorOption());
        this.size = (key.getSizeOption() == null ? SizeType.Default : key.getSizeOption());
        setKey();
        this.stock = optionData.getStock();
        this.isSoldOut = optionData.isSoldOut();
        this.revStock = stock;
    }

    private void setKey() {
        key = String.format("%d,%s,%s", itemId, color.toString(), size.toString());
    }

    public boolean isSame(ItemOptionRecord record) {
        return BaseRecord.isSame(this, record);
//        if (itemId != record.itemId) return false;
//        if (color != record.color) return false;
//        if (size != record.size) return false;
//        if (confirmed != record.confirmed) return false;
//        if (stock != record.stock) return false;
//        if (revStock != record.revStock) return false;
//        return true;
    }

    public boolean isOnRevision() {
        return ((confirmed == false) || (stock != revStock));
    }

    public ItemStockKey toStockKey() {
        return new ItemStockKey(itemId, color, size);
    }

    public void toLogCat(String location) {
        if (location.startsWith("e."))
            Log.e("DEBUG_JS", String.format("[%s] itemId %d, color %s, size %s : confirmed? %s, stock %d, revStock %d, soldOut? %s",
                    location, itemId, UserSettingData.getColorName(color), size.uiName, confirmed, stock, revStock, isSoldOut));
        else
            Log.i("DEBUG_JS", String.format("[%s] itemId %d, color %s, size %s : confirmed? %s, stock %d, revStock %d, soldOut? %s",
                    location, itemId, UserSettingData.getColorName(color), size.uiName, confirmed, stock, revStock, isSoldOut));
    }

    public void toLogCat(String location, ItemDB itemDB) {
        Log.i("DEBUG_JS", String.format("[%s] itemId %d (%s), color %s, size %s, stock %d, revStock %d",
                location, itemId, itemDB.getRecordBy(itemId).name, UserSettingData.getColorName(color), size.uiName, stock, revStock));
    }

    public String toStringWithName(String name, String delimiter) {
        List<String> results = new ArrayList<>();
        results.add(name.isEmpty() ? "(미입력)" : name);
        if (color != ColorType.Default)
            results.add(color.getUiName());
        if (size != SizeType.Default)
            results.add(size.uiName);
        return TextUtils.join(delimiter, results);
    }

    public String toString(String delimiter) {
        List<String> results = new ArrayList<>();
        if (color != ColorType.Default)
            results.add(color.getUiName());
        if (size != SizeType.Default)
            results.add(size.uiName);
        return TextUtils.join(delimiter, results);
    }

    public String toStringWithDefault(String delimiter) {
        List<String> results = new ArrayList<>();
        results.add(UserSettingData.getColorName(color));
        results.add(size.uiName);
        return TextUtils.join(delimiter, results);
    }

    public String toStringWithDefaultAndStock(String delimiter) {
        List<String> results = new ArrayList<>();
        results.add(UserSettingData.getColorName(color));
        results.add(size.uiName);
        results.add(Integer.toString(stock));
        return TextUtils.join(delimiter, results);
    }

    public Map<ItemStockKey, Integer> toMap() {
        Map<ItemStockKey, Integer> results = new HashMap<>();
        results.put(toStockKey(), stock);
        return results;
    }

    public int getPositiveStock() {
        return Math.max(0, stock); // 합산시 음수로 인해 실제 재고 개수가 떨어지지 않게
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof ItemOptionRecord)) return false;
        ItemOptionRecord record = (ItemOptionRecord) object;

        if (itemId != record.itemId) return false;
        if (size != record.size) return false;
        return color == record.color;
    }

    @Override
    public int hashCode() {
        int result = itemId;
        result = 31 * result + color.hashCode();
        result = 31 * result + size.hashCode();
        return result;
    }

    @Override
    public int compareTo(@NonNull ItemOptionRecord record) {
        switch (_sortingKey) {
            default:
            case ID:
                return -((Integer) itemId).compareTo(record.itemId);
            case MultiField: // item-color-size
                if (itemId != record.itemId) {
                    return -((Integer) itemId).compareTo(record.itemId);
                } else if (color.compareTo(record.color) != 0) {
                    return (color.compareTo(record.color));
                } else {
                    return size.compareTo(record.size);
                }
        }
    }
}
