package com.teampls.shoplus.lib.awsservice;

import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.protocol.UserSpecificSettingProtocol;

/**
 * @author lucidite
 */

public interface ProjectSpecificUserServiceProtocol {
    /**
     * 개별 사용자별(로그인한 사용자) 설정 및 공용설정을 조회한다.
     *
     * @param loginUserPhoneNumber
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol getUserSetting(String loginUserPhoneNumber) throws MyServiceFailureException;

    /**
     * 개별 사용자별(로그인한 사용자) 설정을 업데이트한다.
     *
     * @param loginUserPhoneNumber
     * @param setting
     * @return 개별 사용자의 설정 및 공영 설정의 쌍
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateUserSpecificSetting(String loginUserPhoneNumber, UserSpecificSettingProtocol setting) throws MyServiceFailureException;
}
