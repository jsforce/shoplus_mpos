package com.teampls.shoplus.lib.database_global;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.QueryAndRecord;
import com.teampls.shoplus.lib.datatypes.UserShopInfo;

import org.joda.time.DateTime;

import java.util.List;

public class CustomerPriceDB extends BaseDB<PricingRecord> {

    private static CustomerPriceDB instance = null;
    private static int Ver = 1;

    public enum Column {
        _id, workingShopPhoneNumber, buyerPhoneNumber, itemId, unitPrice, updatedDateTime;
        // workingShopNumber : 근무샵을 옮겨다녀도 저장해놓게
        // updateDateTime : 마지막 업뎃 날짜가 6개월이 지나면 자동 삭제하게 (투데이에서 찾아보면 되니까)

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    protected CustomerPriceDB(Context context) {
        super(context, "PricingDB", Ver, Column.toStrs());
    }

    public static CustomerPriceDB getInstance(Context context) {
        if (instance == null)
            instance = new CustomerPriceDB(context);
        return instance;
    }

    public DBHelper getDBHelper() {
        return new DBHelper(context, String.format("%s/%s.db", MyDevice.teamplDir, DB_NAME));
    }

    protected ContentValues toContentvalues(PricingRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.workingShopPhoneNumber.name(), record.workingShopPhoneNumber);
        contentvalues.put(Column.buyerPhoneNumber.name(), record.buyerPhoneNumber);
        contentvalues.put(Column.itemId.name(), record.itemId);
        contentvalues.put(Column.unitPrice.name(), record.unitPrice);
        contentvalues.put(Column.updatedDateTime.name(), record.updatedDateTime.toString());
        return contentvalues;
    }

    @Override
    public PricingRecord getRecordByKey(PricingRecord record) {
        List<QueryAndRecord> queryAndRecords = new QueryAndRecord(Column.buyerPhoneNumber, record.buyerPhoneNumber).toList();
        queryAndRecords.add(new QueryAndRecord(Column.itemId, record.itemId));
        queryAndRecords.add(new QueryAndRecord(Column.workingShopPhoneNumber, record.workingShopPhoneNumber));
        return getRecordByQuery(queryAndRecords);
    }

    public int getPrice(UserShopInfo userShopInfo, String buyerPhoneNumber, int itemId, int defaultUnitPrice) {
        PricingRecord record = getRecordByKey(new PricingRecord(userShopInfo.getMyShopOrUserPhoneNumber(), buyerPhoneNumber, itemId));
        if (record.id > 0)
            return record.unitPrice;
        else
            return defaultUnitPrice;
    }

    @Override
    protected int getId(PricingRecord record) {
        return record.id;
    }

    public void toLogCat(String callLocation) {
        for (PricingRecord record : getRecords())
            record.toLogCat(callLocation);
    }

    @Override
    protected PricingRecord getEmptyRecord() {
        return new PricingRecord();
    }

    @Override
    protected PricingRecord createRecord(Cursor cursor) {
        return new PricingRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.workingShopPhoneNumber.ordinal()),
                cursor.getString(Column.buyerPhoneNumber.ordinal()),
                cursor.getInt(Column.itemId.ordinal()),
                cursor.getInt(Column.unitPrice.ordinal()),
                BaseUtils.toDateTime(cursor.getString(Column.updatedDateTime.ordinal()))
                );
    }

    public void finish() {
        DateTime deadline = DateTime.now().minusMonths(6);
        List<PricingRecord> records = getRecordsBetween(Column.updatedDateTime, Empty.dateTime.toString(), deadline.toString(), null);
        deleteAll(Column._id, getIds(records));
    }

}