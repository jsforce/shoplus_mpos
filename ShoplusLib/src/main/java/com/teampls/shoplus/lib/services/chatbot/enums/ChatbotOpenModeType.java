package com.teampls.shoplus.lib.services.chatbot.enums;

/**
 * Chatbot Open Mode 설정 타입
 *
 * @author lucidite
 */
public enum ChatbotOpenModeType {
    CLOSED("c", "비공개"),
    OPEN_WHOLESALE("w", "공개(기본)"),
    OPEN_RETAIL("r", "공개(소매)");

    private String remoteStr;
    private String uiStr;

    private static ChatbotOpenModeType DEFAULT_MODE = ChatbotOpenModeType.CLOSED;

    ChatbotOpenModeType(String remoteStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
    }

    public static ChatbotOpenModeType fromRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty()) {
            return DEFAULT_MODE;
        }
        for (ChatbotOpenModeType mode: ChatbotOpenModeType.values()) {
            if (mode.remoteStr.equals(remoteStr)) {
                return mode;
            }
        }
        return DEFAULT_MODE;
    }

    public String toRemoteStr() {
        return remoteStr;
    }

    public String toUiStr() {
        return uiStr;
    }
}
