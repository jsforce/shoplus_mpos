package com.teampls.shoplus.lib.database;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.enums.AccountChangeOperationType;
import com.teampls.shoplus.lib.enums.AccountLogType;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;

import org.joda.time.DateTime;

import static com.teampls.shoplus.lib.database_memory.MSortingKey.Date;

/**
 * Created by Medivh on 2017-08-26.
 */

public class AccountLogRecord implements AccountChangeLogDataProtocol, Comparable<AccountLogRecord> {
    public int id = 0, account = 0, variation = 0, sortKey = 0; // account는 전체합산, variation은 현재것
    private String logId = "";
    public String provider = "", buyer = "";
    public DateTime createdDateTime = Empty.dateTime, updatedDateTime = Empty.dateTime;
    public boolean cleared = false, cancelled = false, relatedToCashFlow = false, isCrossing = false;
    public AccountChangeOperationType operation = AccountChangeOperationType.Transaction; // 어떤 일에 대한 로그인가? (추가 설명)
    public AccountType accountType = AccountType.OnlineAccount; // online? entrust?
    public int receivable = 0;  // 온라인, 대납 합산용 임시 변수, 앱에서 계산하는 값
    public MSortingKey sortingKey = MSortingKey.ID;  // sortKey값

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] id %s, %s --> %s, %s ->%s, account %d, variation %d, cleared? %s, oper %s, type %s, " +
                        "cancelled? %s, related to cash? %s, sort %d, crossing? %s",
                location, logId, provider, buyer, createdDateTime.toString(BaseUtils.fullFormat), updatedDateTime.toString(BaseUtils.fullFormat),
                account, variation, cleared, operation.toString(), accountType.toString(), cancelled, relatedToCashFlow, sortKey, isCrossing));
    }

    public void toLogCatNumber(String location) {
        Log.i("DEBUG_JS", String.format("[%s] %s, (%s), 변경 %d, 현재 %d, 누적 %d, 작업 %s",
                location, createdDateTime.toString(BaseUtils.MDHm_Format), accountType.toString(), variation, account, receivable, operation.toString()));
    }

    public AccountLogRecord() {
    }

    ;

    public AccountLogRecord(int id, String logId, String provider, String buyer, DateTime createdDateTime,
                            DateTime updatedDateTime, int account, int variation, boolean cleared,
                            AccountChangeOperationType operation, AccountType accountType, boolean cancelled,
                            boolean relatedToCashFlow, int sortKey, boolean isCrossing) {
        this.id = id;
        this.logId = logId;
        this.provider = provider;
        this.buyer = buyer;
        this.createdDateTime = createdDateTime;
        this.updatedDateTime = updatedDateTime;
        this.account = account;
        this.variation = variation;
        this.cleared = cleared;
        this.operation = operation;
        this.accountType = accountType;
        this.cancelled = cancelled;
        this.relatedToCashFlow = relatedToCashFlow;
        this.sortKey = sortKey;
        this.isCrossing = isCrossing;
    }

    public AccountLogRecord clone() {
        AccountLogRecord result = new AccountLogRecord();
        result.logId = logId;
        result.provider = provider;
        result.buyer = buyer;
        result.createdDateTime = createdDateTime;
        result.updatedDateTime = updatedDateTime;
        result.account = account;
        result.variation = variation;
        result.cleared = cleared;
        result.operation = operation;
        result.accountType = accountType;
        result.cancelled = cancelled;
        result.relatedToCashFlow = relatedToCashFlow;
        result.sortKey = sortKey;
        result.isCrossing = isCrossing;
        return result;
    }

    public AccountLogRecord(AccountChangeLogDataProtocol protocol, AccountType accountType) {
        this.logId = protocol.getLogId();
        this.provider = protocol.getProvider();
        this.buyer = protocol.getBuyer();
        this.createdDateTime = protocol.getCreatedDateTime();
        this.updatedDateTime = protocol.getUpdatedDateTime();
        this.account = protocol.getCurrentAmount();
        this.variation = protocol.getChangeAmount();
        this.cleared = protocol.isCleared();
        this.operation = protocol.getOperationType();
        this.accountType = accountType;
        this.cancelled = protocol.isCancelled();
        this.relatedToCashFlow = protocol.isRelatedToCashFlow();
        this.sortKey = protocol.getSortKey();
        this.isCrossing = protocol.isCrossingAccounts();
    }

    public boolean isSame(AccountLogRecord record) {
        if (logId.equals(record.logId) == false) return false;
        if (provider.equals(record.provider) == false) return false;
        if (buyer.equals(record.buyer) == false) return false;
        if (createdDateTime.isEqual(record.createdDateTime) == false) return false;
        if (updatedDateTime.isEqual(record.updatedDateTime) == false) return false;
        if (account != record.account) return false;
        if (variation != record.variation) return false;
        if (cleared != record.cleared) return false;
        if (operation != record.operation) return false;
        if (accountType != record.accountType) return false;
        if (cancelled != record.cancelled) return false;
        if (relatedToCashFlow != record.relatedToCashFlow) return false;
        if (sortKey != record.sortKey) return false;
        if (isCrossing != record.isCrossing) return false;
        return true;
    }

    public String getKey() {
        return accountType.toString() + logId;
    }

    public String getCounterpart(Context context) {
        //Log.i("DEBUG_JS", String.format("[AccountLogRecord.getCounterpart] provider record %s, provider %s",  UserSettingData.getInstance(context).getProviderRecord().phoneNumber, provider));
        if (UserSettingData.getInstance(context).getProviderRecord().phoneNumber.equals(provider)) // 공급자가 내 번호면
            return buyer;
        else
            return provider;
    }

    public AccountLogType getAccountLogSummaryType() {
        switch (accountType) {
            default:
                return AccountLogType.CashPaid;
            case DepositAccount:
                if (cleared)
                    return AccountLogType.DepositPaid;
                else
                    return AccountLogType.Deposit;
            case EntrustedAccount:
                if (cleared)
                    if (isCrossingAccounts())
                        return AccountLogType.OnlinePaid;
                    else
                        return AccountLogType.EntrustPaid;
                else
                    return AccountLogType.Entrust;
            case OnlineAccount:
                if (cleared)
                    if (isCrossingAccounts())
                        return AccountLogType.EntrustPaid;
                    else
                        return AccountLogType.OnlinePaid;
                else
                    return AccountLogType.Online;
        }
    }

    public SlipDataKey getSlipKey() {
        return new SlipDataKey(provider, createdDateTime);
    }

    @Override
    public String getLogId() {
        return logId;
    }

    @Override
    public DateTime getCreatedDateTime() {
        return createdDateTime;
    }

    @Override
    public int getSortKey() {
        return sortKey;
    }

    @Override
    public String getProvider() {
        return provider;
    }

    @Override
    public String getBuyer() {
        return buyer;
    }

    @Override
    public int getChangeAmount() {
        return variation;
    }

    @Override
    public int getCurrentAmount() {
        return account;
    }

    @Override
    public Boolean isCleared() {
        return cleared;
    }

    @Override
    public Boolean isCancelled() {
        return cancelled;
    }

    @Override
    public Boolean isCrossingAccounts() {
        return isCrossing;
    }

    @Override
    public Boolean isRelatedToCashFlow() {
        return relatedToCashFlow;
    }

    @Override
    public DateTime getUpdatedDateTime() {
        return updatedDateTime;
    }

    @Override
    public AccountChangeOperationType getOperationType() {
        return operation;
    }

    @Override
    public int compareTo(@NonNull AccountLogRecord record) {
        // AccountLogRecord는 잔금적산 계산때문에 시간순대로 계산하고 맨 마지막에 reverse를 해서 모두 내림차순으로 정리한다
        switch (sortingKey) {
            default:
                return ((Integer) sortKey).compareTo(record.sortKey); // 최근것이 앞으로
            case Date:
                return createdDateTime.compareTo(record.createdDateTime);
        }
    }
}
