package com.teampls.shoplus.lib.viewSetting;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_map.GlobalBoolean;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseActivity;

/**
 * Created by Medivh on 2018-10-06.
 */

abstract public class BaseSettingView extends BaseActivity {

    protected void setCheckBox(int resId, final String globalDBKey, boolean defaultValue) {
        final CheckBox checkBox = ((CheckBox) findViewById(resId));
        MyView.setTextViewByDeviceSize(context, checkBox);
        checkBox.setChecked(globalDB.getBool(globalDBKey, defaultValue));
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalDB.put(globalDBKey, checkBox.isChecked());
            }
        });
    }

    protected void setCheckBox(int resId, final GlobalBoolean globalBoolean) {
        setCheckBox(resId, globalBoolean.key, globalBoolean.defaultValue);
    }

    protected void addCheckBox(LinearLayout container, String text, final GlobalBoolean globalBoolean) {
        addCheckBox(container, text, globalBoolean.key, globalBoolean.defaultValue);
    }

    protected void addCheckBox(LinearLayout container, String text, final String globalDBKey, boolean defaultValue) {
        LinearLayout.LayoutParams params = MyView.getParams(context, MyView.PARENT, MyView.CONTENT);
        params.setMargins(0, 5, 0, 0);
        final CheckBox checkBox = new CheckBox(context);
        checkBox.setText(text);
        checkBox.setTextSize(18);
        checkBox.setLayoutParams(params);
        checkBox.setChecked(globalDB.getBool(globalDBKey, defaultValue));
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalDB.put(globalDBKey, checkBox.isChecked());
            }
        });
        MyView.setTextViewByDeviceSize(context, checkBox);
        container.addView(checkBox);
    }

    protected void addLine(LinearLayout container) {
        addLine(container, 5);
    }

    protected void addLine(LinearLayout container, int heightDp) {
        container.addView(MyView.createLine(context, heightDp));
    }

    public void addCheckBoxs(LinearLayout container, boolean doAddLine) {
        addCheckBox(container, "잔금 표시", GlobalDB.doShowReceivables);
        addCheckBox(container, "잔금 표시 (작성중에)", GlobalDB.doShowReceivablesToMemo);
        addCheckBox(container, "잔금 자동 청구", GlobalDB.doRequestBillAutomatically);
        addCheckBox(container, "매입금 표시", GlobalDB.KEY_RECEIPT_ShowDeposit, false);
        addLine(container, 1);
        addCheckBox(container, "상품수량 총합 표시", GlobalDB.KEY_RECEIPT_ShowQuantitySum, false);
        addCheckBox(container, "택배비, 부가세는 수량 합산에서 제외", GlobalDB.doExcludeKeywordsFromQuantity);
        addCheckBox(container, "x1 대신 '1개'로 수량 표시", GlobalDB.KEY_RECEIPT_UseHangulQuantity, false);
        addLine(container, 1);
        addCheckBox(container, "주문/샘플/위탁 금액도 표시", GlobalDB.KEY_RECEIPT_ShowPreorderSums, false);
        addCheckBox(container, "판매합산, 빼기합산 표시", GlobalDB.doShowPlusMinusSum);
        addCheckBox(container, "거래처이름 크게 표시", GlobalDB.KEY_RECEIPT_EnlargeCounterpart, false);
        addCheckBox(container, "영수증 완료시 알림톡 전송화면 표시", GlobalDB.KEY_RECEIPT_ShowNotifTalk, true);
        if (doAddLine)
            addLine(container);
    }


}
