package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.database_map.KeyValueDB;

/**
 * Created by Medivh on 2017-02-14.
 */

abstract public class MyNoticeDialog extends BaseDialog {
    private CheckBox checkBox ;
    private String key;
    private ImageView imageView;

    public MyNoticeDialog(Context context, String title, String message, Bitmap bitmap, String notified) {
        super(context);
        this.key = notified;
        findTextViewById(R.id.dialog_notice_title, title);
        findTextViewById(R.id.dialog_notice_message, message);
        setOnClick(R.id.dialog_notice_apply);
        checkBox = (CheckBox) findViewById(R.id.dialog_notice_checkbox);
        imageView = (ImageView) findViewById(R.id.dialog_notice_image);
        if (bitmap == null || bitmap.getWidth() <= 0 || bitmap.getHeight() <= 0) {
            imageView.setVisibility(View.GONE);
        } else {
            imageView.getLayoutParams().height = MyGraphics.getHeight(imageView.getLayoutParams().width, bitmap.getWidth(), bitmap.getHeight());
            imageView.setImageBitmap(bitmap);
        }
        if (KeyValueDB.getInstance(context).getBool(notified)) {
            onDismiss();
        } else {
            show();
        }
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_notice;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_notice_apply) {
            KeyValueDB.getInstance(context).put(key, checkBox.isChecked());
            dismiss();
            onDismiss();
        }
    }

    public abstract void onDismiss();
}
