package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.CheckableStringAdapter;
import com.teampls.shoplus.lib.common.MyUI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-08-08.
 */

abstract public class MyCheckableListDialog<T> extends BaseDialog implements AdapterView.OnItemClickListener {
    private ListView listView;
    private CheckableStringAdapter adapter;
    private List<T> records;
    private boolean doAllRecord = false, applyButtonEnabled = false;

    public MyCheckableListDialog(Context context, String title, List<T> records, T recommendedRecord) {
        super(context);
        setDialogWidth(0.9, 0.6);
        applyButtonEnabled = true;
        this.records = records;
        List<String> uiStrRecords = new ArrayList<>();
        for (T record : records)
            uiStrRecords.add(getUiString(record));
        setVisibility(View.GONE, R.id.dialog_list_message);
        setVisibility(View.VISIBLE,  R.id.dialog_list_button_container);
        init(title, uiStrRecords, recommendedRecord);
    }

    public MyCheckableListDialog(Context context, String title, List<T> records, @Nullable T selectedRecord, String allRecordName) {
        super(context);
        setDialogWidth(0.9, 0.6);
        this.records = records;
        List<String> uiStrRecords = new ArrayList<>();
        for (T record : records)
            uiStrRecords.add(getUiString(record));

        doAllRecord = !allRecordName.isEmpty();
        if (doAllRecord) {
            records.add(0, null);
            uiStrRecords.add(0, allRecordName);
        }
        setVisibility(View.GONE, R.id.dialog_list_message, R.id.dialog_list_button_container);
        init(title, uiStrRecords, selectedRecord);
    }

    protected void init(String title, List<String> uiStrRecords, T selectedRecord) {
        int selectedIndex = 0;
        if (selectedRecord != null) {
            int index = 0;
            for (T record : records) {
                if (record == selectedRecord)
                    selectedIndex = index;
                index++;
            }
        }

        adapter = new CheckableStringAdapter(context);
        adapter.setRecords(uiStrRecords);
        adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_SINGLE);
        listView = (ListView) findViewById(R.id.dialog_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        if (selectedRecord != null)
            adapter.setClickedPosition(selectedIndex);
        findTextViewById(R.id.dialog_list_title, title);
        setOnClick(R.id.dialog_list_close, R.id.dialog_list_button_apply, R.id.dialog_list_button_cancel);
        refresh();
        show();
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_list;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_list_close || view.getId() == R.id.dialog_list_button_cancel) {
            dismiss();
        } else if (view.getId() == R.id.dialog_list_button_apply) {
            if (adapter.getSortedClickedRecords().size() == 1) {
                onItemClicked(records.get(adapter.getClickedPositions().get(0)));
                dismiss();
            } else {
                MyUI.toastSHORT(context, String.format("설정에 오류가 있습니다"));
                dismiss();
            }
        }
    }

    abstract public void onItemClicked(@Nullable T selectedRecord);

    protected abstract String getUiString(T record);

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (applyButtonEnabled) {
            adapter.onItemClick(view, position); // 클릭만으로는 종료하지 않는다
        } else {
            onItemClicked(records.get(position));
            dismiss(); // 선택이 곧 종료
        }
    }

}
