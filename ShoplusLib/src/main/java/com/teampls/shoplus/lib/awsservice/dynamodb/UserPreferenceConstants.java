package com.teampls.shoplus.lib.awsservice.dynamodb;

/**
 * @author lucidite
 */

public class UserPreferenceConstants {
    public static final String SLIP_SYNC_PROVIDER = "provider";
    public static final String SLIP_SYNC_BUYER_FOLLOWING = "buyer-following";
    public static final String SLIP_SYNC_BUYER_SEPARATED = "buyer-separated";
}
