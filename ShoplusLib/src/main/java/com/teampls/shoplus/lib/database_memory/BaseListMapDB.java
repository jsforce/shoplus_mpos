package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

import com.teampls.shoplus.lib.common.MyListMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-07-08.
 */

public class BaseListMapDB<K, V> extends BaseMemoryDB<List<V>> {
    protected MyListMap<K, V> database = new MyListMap<>();

    public BaseListMapDB(Context context, String dbName) {
        this.context = context;
        this.DB_NAME = dbName;
    }

    public void add(K key, V record) {
        database.add(key, record);
    }

    public void updateOrAdd(K key, V record) {
        database.updateOrAdd(key, record);
    }

    @Override
    protected List<V> getEmptyRecord() {
        return new ArrayList<>();
    }

    public void clear() {
        database.clear();
    }
}
