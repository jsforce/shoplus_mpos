package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseSlipItemDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyImageLoader;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.composition.BaseCreationComposition;
import com.teampls.shoplus.lib.database.AccountLogRecord;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MAccountDB;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.dialog.BaseAccountLogDialog;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.DeleteSafelyDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.MyImageDialog;
import com.teampls.shoplus.lib.dialog.MyMultilinesDialog;
import com.teampls.shoplus.lib.dialog.UserDetailsDialog;
import com.teampls.shoplus.lib.enums.AccountChangeUpdateOperationType;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.enums.BalanceChangeMethodType;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.enums.MemberPermission;
import com.teampls.shoplus.lib.enums.TransType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.printer.MyPosPrinter;
import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view.PosSlipInfoView;
import com.teampls.shoplus.lib.view.TransNoteView;
import com.teampls.shoplus.lib.viewSetting.PrinterSettingView;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.teampls.shoplus.lib.database_global.GlobalDB.KEY_RECEIPT_ShowDeposit;

/**
 * Created by Medivh on 2017-04-29.
 */

abstract public class BasePosSlipSharingView extends BaseActivity implements AdapterView.OnItemClickListener {
    protected static SlipFullRecord slipFullRecord = new SlipFullRecord();
    protected static ItemOptionRecord itemOptionRecord = new ItemOptionRecord();
    protected BaseSlipItemDBAdapter adapter;
    protected TextView tvProvider, tvCounterpart, tvPhone1, tvPhone2, tvLocation, tvAccount, tvMemo, tvAddress,
            tvKey, tvCounterAddress, tvDeposit, tvSummary, tvBill, tvPayment, tvDate, tvPreorderSum, tvMemoFromPrivate,
            tvBeforeReceivable, tvThisReceivable, tvAfterReceivable, tvReceiptReceivable, tvReceivableRead;
    protected LinearLayout printView;
    protected ExpandableHeightListView lvSlipItems;
    protected MyPosPrinter myPosPrinter;
    protected PosSlipDB posSlipDB;
    protected ScrollView scrollView;
    protected boolean accountUpdated = false;
    protected UserRecord counterpart = new UserRecord();
    protected float tvCounterpartSizeSP = 16; // sp
    protected int receiptValue = 0;
    protected MAccountDB accountDB;
    protected SlipSummaryRecord slipSummaryRecord = new SlipSummaryRecord();
    protected DateTime accountDownloadTime = Empty.dateTime;
    protected static DbActionType actionType = DbActionType.READ;
    private LinearLayout plusMinusSumContainer, plusContainer, minusContainer, receivableContainer, receivableReadContainer,
    receivableBeforeContainer, receivableThisContainer, receivableReceiptContainer, memoFromPrivateContainer;
    private TextView tvPlusQuantitySum, tvPlusAmountSum, tvMinusQuantitySum, tvMinusAmountSum;
    private TransType transType = TransType.bill;


    @Override
    public int getThisView() {
        return R.layout.base_slip_sharing;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        posSlipDB = PosSlipDB.getInstance(context);
        accountDB = MAccountDB.getInstance(context);
        setAdapter();

        printView = findViewById(R.id.slip_sharing_container);
        if (slipFullRecord.record.isMine(context)) {
            // 사실상 여기다
            counterpart = userDB.getRecord(slipFullRecord.record.counterpartPhoneNumber);
        } else {
            // 소매가 영수증을 보려고 할 때 발생한다
            counterpart = userSettingData.getMyRecord(); // 발행자가 도매면 받는 그 대상은 나 자신이다
        }
        slipSummaryRecord = new SlipSummaryRecord(slipFullRecord.items, globalDB.getNoQuantityKeywords());
        tvDate = findTextViewById(R.id.slip_sharing_date);
        tvSummary = findTextViewById(R.id.slip_sharing_summary, "");
        tvPreorderSum = findTextViewById(R.id.slip_sharing_preorderSum, "");
        tvBill = findTextViewById(R.id.slip_sharing_bill, "");
        tvPayment = findTextViewById(R.id.slip_sharing_payment, "");
        transType = getTransType(slipSummaryRecord); // 기본은 청구, 전액현금+잔금청구 없을때만 영수로 표시
        tvProvider = findTextViewById(R.id.slip_sharing_provider, "");
        tvCounterpart = findTextViewById(R.id.slip_sharing_counterpart, counterpart.getName() + " 귀하");
        tvPhone1 = findTextViewById(R.id.slip_sharing_phone1, "");
        tvPhone2 = findTextViewById(R.id.slip_sharing_phone2, "");
        tvLocation = findTextViewById(R.id.slip_sharing_location, "");
        tvAddress = findTextViewById(R.id.slip_sharing_address, "");
        tvAccount = findTextViewById(R.id.slip_sharing_account, "");
        tvMemo = findTextViewById(R.id.slip_sharing_memo, "");
        tvMemoFromPrivate = findTextViewById(R.id.slip_sharing_memo_from_private, "");
        tvDeposit = findTextViewById(R.id.slip_sharing_deposit, "");

        receivableContainer = findViewById(R.id.slip_sharing_receivable_container);
        tvBeforeReceivable = findTextViewById(R.id.slip_sharing_receivable_before);
        tvThisReceivable = findTextViewById(R.id.slip_sharing_receivable_this);
        tvReceiptReceivable = findTextViewById(R.id.slip_sharing_receivable_receipt);
        tvAfterReceivable = findTextViewById(R.id.slip_sharing_receivable_after);
        receivableReadContainer = findViewById(R.id.slip_sharing_receivable_read_container);
        tvReceivableRead = findTextViewById(R.id.slip_sharing_receivable_current);
        receivableBeforeContainer = findViewById(R.id.slip_sharing_receivable_before_container);
        receivableThisContainer = findViewById(R.id.slip_sharing_receivable_this_container);
        receivableReceiptContainer = findViewById(R.id.slip_sharing_receivable_receipt_container);
        memoFromPrivateContainer = findViewById(R.id.slip_sharing_memo_from_private_container);
        memoFromPrivateContainer.setVisibility(View.GONE);

        tvKey = findTextViewById(R.id.slip_sharing_key, String.format("[증빙번호] %s", slipFullRecord.getKey().createdDateTime.toString(BaseUtils.KEY_Format)));
        tvCounterAddress = findTextViewById(R.id.slip_sharing_delivery_address, String.format("택배주소 : %s", counterpart.location));
        if (counterpart.location.isEmpty())
            tvCounterAddress.setVisibility(View.GONE);
        tvCounterpartSizeSP = MyDevice.toOriginalSP(context, tvCounterpart.getTextSize());

        lvSlipItems = findViewById(R.id.slip_sharing_listview);
        lvSlipItems.setExpanded(true);
        lvSlipItems.setOnItemClickListener(this);
        lvSlipItems.setAdapter(adapter);

        setOnClick(R.id.slip_sharing_provider, R.id.slip_sharing_cancel, R.id.slip_sharing_print, R.id.slip_sharing_counterpart,
                R.id.slip_sharing_createNewSlip, R.id.slip_sharing_to_exodar, R.id.slip_sharing_to_dalaran,
                R.id.slip_sharing_float_print, R.id.slip_sharing_transNote, R.id.slip_sharing_date, R.id.slip_sharing_memo_from_private_container);

        MyView.setImageViewSizeByDeviceSize(context, (ImageView) findViewById(R.id.slip_sharing_float_print));
        MyView.setTextViewByDeviceSize(context, getView(), R.id.slip_sharing_table_unitprice,
                R.id.slip_sharing_table_name, R.id.slip_sharing_table_quantity, R.id.slip_sharing_table_sum, R.id.slip_sharing_table_type,
                R.id.slip_sharing_receivable_before_title, R.id.slip_sharing_receivable_this_title, R.id.slip_sharing_receivable_receipt_title,
                R.id.slip_sharing_receivable_after_title, R.id.slip_sharing_memo_from_private_title);

        plusMinusSumContainer = findViewById(R.id.slip_sharing_plusminus_container);
        plusMinusSumContainer.setVisibility(View.GONE);
        plusContainer = findViewById(R.id.slip_sharing_plus_container);
        minusContainer = findViewById(R.id.slip_sharing_minus_container);
        MyView.setTextViewByDeviceSize(context, plusContainer);
        MyView.setTextViewByDeviceSize(context, minusContainer);
        tvPlusQuantitySum = findViewById(R.id.slip_sharing_plus_quantity_sum);
        tvPlusAmountSum = findViewById(R.id.slip_sharing_plus_amount_sum);
        tvMinusQuantitySum = findViewById(R.id.slip_sharing_minus_quantity_sum);
        tvMinusAmountSum = findViewById(R.id.slip_sharing_minus_amount_sum);

        setVisibility(View.GONE, R.id.slip_sharing_createNewSlip);
        refresh();
        // onResume에서   downloadAccount(counterpart) 실행
    }


    private TransType getTransType(SlipSummaryRecord slipSummaryRecord) {
        // 온라인/대납 청구 없고 잔금 청구가 없는 경우에 한해서만 영수로 표시
        SlipRecord slipRecord = slipFullRecord.record;
        if ( slipRecord.onlinePayment == 0 &&  slipRecord.entrustPayment == 0
                && slipSummaryRecord.amount !=0  &&  slipRecord.bill == 0)
            return TransType.receipt;
        else
            return TransType.bill;
    }

    protected void downloadAccount(final UserRecord counterpart) {
        new CommonServiceTask(context, "BasePosSlipSharingView") {
            AccountRecord accountRecord;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                AccountsData data = accountService.getAccounts(userSettingData.getUserShopInfo(),
                        userSettingData.getUserConfigType(), counterpart.phoneNumber);
                accountRecord = new AccountRecord(counterpart.phoneNumber, data, counterpart.name);
                MAccountDB.getInstance(context).updateOrInsert(accountRecord);
                accountUpdated = true;
                accountDownloadTime = DateTime.now();
            }

            @Override
            public void onPostExecutionUI() {
                // 자동 잔금 추가는 4가지 조건이 만족돼야 함
                // 1. 생성시 호출했을때
                // 2. 자동생성을 True로 했을때
                // 3. 청구 영수증 일때 (합산금 = 온라인 + 대납)
                // 4. 미수금이 양수일때
//                if (actionType == DbActionType.INSERT) {
//                    if (globalDB.getBool(KEY_RECEIPT_AutoAddReceivable)) {
//                        if (slipSummaryRecord.amount == (slipFullRecord.record.onlinePayment + slipFullRecord.record.entrustPayment)) {// 청구서
//                            if (accountRecord.getReceivable() > 0)
//                                receiptValue = accountRecord.getReceivable() - slipFullRecord.record.onlinePayment - slipFullRecord.record.entrustPayment;
//                            if (receiptValue < 0)
//                                receiptValue = 0; // 어떤 이유에서든 청구 금액이 줄어들면 자동청구는 막자, 뭔가 비정상적인 경우다
//                        }
//                    }
//                }
                refreshAmounts();
            }
        };
    }

    protected void refreshReceivables(AccountRecord accountRecord) {
        if (globalDB.doShowReceivables.get(context)) {
            switch (actionType) {
                case INSERT:
                    receivableContainer.setVisibility(View.VISIBLE);
                    receivableReadContainer.setVisibility(View.GONE);
                    int thisReceivable = slipFullRecord.record.onlinePayment + slipFullRecord.record.entrustPayment;

                    tvBeforeReceivable.setText(BaseUtils.toCurrencyOnlyStr(accountRecord.getReceivable() + receiptValue - thisReceivable));
                    receivableThisContainer.setVisibility(thisReceivable != 0 ? View.VISIBLE : View.GONE);
                    tvThisReceivable.setText(BaseUtils.toCurrencySign(thisReceivable));

                    receivableReceiptContainer.setVisibility(receiptValue != 0 ? View.VISIBLE : View.GONE);
                    tvReceiptReceivable.setText(BaseUtils.toCurrencySign(-receiptValue));

                    tvAfterReceivable.setText(BaseUtils.toCurrencyOnlyStr(accountRecord.getReceivable()));
                    break;
                default:
                    receivableContainer.setVisibility(View.GONE);
                    receivableReadContainer.setVisibility(View.VISIBLE);
                    tvReceivableRead.setText(String.format("현재 잔금 : %s\n(%s 기준)",
                            BaseUtils.toCurrencyOnlyStr(accountRecord.getReceivable()), accountDownloadTime.toString(BaseUtils.YMD_HS_Kor_Format)));
                    break;
            }
        } else {
            receivableContainer.setVisibility(View.GONE);
            receivableReadContainer.setVisibility(View.GONE);
        }
    }

    protected void refreshAmounts() {
        AccountRecord accountRecord = accountDB.getRecordByKey(counterpart.phoneNumber);
        if (globalDB.getBool(GlobalDB.KEY_RECEIPT_ShowQuantitySum))
            tvSummary.setText(String.format("%d개 %s", slipSummaryRecord.quantity, BaseUtils.toCurrencyStr(slipSummaryRecord.amount + slipFullRecord.record.bill))); // 영수증 총액 + 잔금 영수액
        else
            tvSummary.setText(String.format("%s", BaseUtils.toCurrencyStr(slipSummaryRecord.amount + slipFullRecord.record.bill))); // 영수증 총액 + 잔금 영수액
        if (globalDB.getBool(GlobalDB.KEY_RECEIPT_ShowPreorderSums)) {
            List<String> summaryStrings = new ArrayList<>();
            if (slipSummaryRecord.preorderSum != 0)
                summaryStrings.add(String.format("주문 : %s", BaseUtils.toCurrencyStr(slipSummaryRecord.preorderSum)));
            if (slipSummaryRecord.sampleSum != 0 || slipSummaryRecord.consignSum != 0)
                summaryStrings.add(String.format("샘플/위탁 : %s", BaseUtils.toCurrencyStr(slipSummaryRecord.sampleSum + slipSummaryRecord.consignSum)));
            tvPreorderSum.setText("(" + TextUtils.join(", ", summaryStrings) + ")");
            tvPreorderSum.setVisibility(summaryStrings.isEmpty() ? View.GONE : View.VISIBLE);
        } else {
            tvPreorderSum.setVisibility(View.GONE);
        }

        tvDeposit.setText(String.format("매입금 : %s", BaseUtils.toCurrencyStr(accountRecord.deposit)));
        tvDeposit.setVisibility((globalDB.getBool(KEY_RECEIPT_ShowDeposit) && accountRecord.deposit > 0) ? View.VISIBLE : View.GONE);
        int cashPayment = slipSummaryRecord.amount - slipFullRecord.record.onlinePayment - slipFullRecord.record.entrustPayment;
        List<String> paymentStrs = new ArrayList<>();
        if (cashPayment != 0)
            paymentStrs.add(String.format("현장결제: %s", BaseUtils.toCurrencyOnlyStr(cashPayment)));
        if (slipFullRecord.record.onlinePayment != 0)
            paymentStrs.add(String.format("온라인청구: %s", BaseUtils.toCurrencyOnlyStr(slipFullRecord.record.onlinePayment)));
        if (slipFullRecord.record.entrustPayment != 0)
            paymentStrs.add(String.format("대납청구: %s", BaseUtils.toCurrencyOnlyStr(slipFullRecord.record.entrustPayment)));
        if (paymentStrs.size() == 0) {
            tvPayment.setVisibility(View.GONE);
        } else {
            tvPayment.setText(TextUtils.join(", ", paymentStrs));
        }
        tvBill.setText(String.format("잔금청구 : %s", BaseUtils.toCurrencyOnlyStr(slipFullRecord.record.bill)));
        tvBill.setVisibility(slipFullRecord.record.bill !=0 ? View.VISIBLE : View.GONE);
        refreshReceivables(accountRecord);
    }

    protected void setAdapter() {
        adapter = new BaseSlipItemDBAdapter(context, PosSlipDB.getInstance(context)) {
            @Override
            public BaseViewHolder getViewHolder() {
                return new SlipSharingViewHolder();
            }

        };
        adapter.setRecords(slipFullRecord.items);
        if (itemOptionRecord.isEmpty() == false)
            adapter.highlightOptionRecord(itemOptionRecord);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final SlipItemRecord record = adapter.getRecord(position);
        MyImageLoader.getInstance(context).retrieveThumbAsync(record.getSlipKey().ownerPhoneNumber, record.itemId, new MyOnTask<Bitmap>() {
            @Override
            public void onTaskDone(Bitmap bitmap) {
                if (Empty.isEmpty(bitmap)) {
                    MyUI.toastSHORT(context, String.format("이미지를 찾지 못했습니다"));
                } else {
                    new MyImageDialog(context, record.name, null, bitmap, null);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (myPosPrinter != null)
            myPosPrinter.disconnectPrinter();
        super.onDestroy();
    }

    private void refresh() {
        if (adapter == null) return;
        adapter.generate();
        adapter.notifyDataSetChanged(); // 반드시 이 형태 유지, ExpandableHeightListView 특징 고려
    }

    protected void refreshView() {
        DateTime salesDate = slipFullRecord.record.salesDateTime;
        tvDate.setText(String.format("%s (%s) %s / %s", salesDate.toString(BaseUtils.YMD_Kor_Format),
                BaseUtils.getDayOfWeekKor(salesDate), salesDate.toString(BaseUtils.HS_Kor_Format), transType.uiStr));

        String providerName = "";
        if (slipFullRecord.record.isMine(context)) {
            providerName = userSettingData.getProviderRecord().getName();
        } else {
            providerName = userDB.getRecord(slipFullRecord.record.ownerPhoneNumber).getName();
        }
        BaseUtils.setText(tvProvider, providerName);
        if (globalDB.getBool(GlobalDB.KEY_RECEIPT_EnlargeCounterpart)) {
            tvCounterpart.setTextSize(tvCounterpartSizeSP + 4);
        } else {
            tvCounterpart.setTextSize(tvCounterpartSizeSP);
        }
        BaseUtils.setTextCancelled(tvDate, slipFullRecord.record.cancelled);

        if (userSettingData.isProvider()) {
            setVisibility(View.VISIBLE, R.id.slip_sharing_attach_container);
            BaseUtils.setTextWithVisbility(tvPhone1, "T.", userSettingData.getWorkingShop().getShopPhoneNumber(0));
            BaseUtils.setTextWithVisbility(tvPhone2, "T.", userSettingData.getWorkingShop().getShopPhoneNumber(1));
            BaseUtils.setTextWithVisbility(tvLocation, "", userSettingData.getWorkingShop().getDetailedAddress());
            BaseUtils.setTextWithVisbility(tvAddress, "", String.format("%s %s",
                    userSettingData.getWorkingShop().getBuildingAddress(), userSettingData.getWorkingShop().getDetailedAddress()));
            BaseUtils.setTextWithVisbility(tvAccount, "", TextUtils.join("\n", userSettingData.getWorkingShop().get3Accounts()).trim());
            BaseUtils.setTextWithVisbility(tvMemo, "", userSettingData.getWorkingShop().getNotice());
        } else {
            setVisibility(View.GONE, R.id.slip_sharing_attach_container);
        }

        BaseCreationComposition.setPlusMinusSum(context, slipSummaryRecord, plusMinusSumContainer, plusContainer, tvPlusQuantitySum, tvPlusAmountSum,
                minusContainer, tvMinusQuantitySum, tvMinusAmountSum);

        scrollView = findViewById(R.id.slip_sharing_scrollview);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_UP);
            }
        });
    }

    public void onCreateNewSlipClick() {

    }

    private void refreshMemoFromPrivate(int visibility, List<String> memo) {
        memoFromPrivateContainer.setVisibility(visibility);
        tvMemoFromPrivate.setText(TextUtils.join("\n", memo));
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.slip_sharing_cancel) {
            doFinishForResult();

        } else if (view.getId() == R.id.slip_sharing_memo_from_private_container) {
            new MyAlertDialog(context, "메모 삭제", "표시한 메모를 삭제하시겠습니까? 개인 메모에서 다시 불러올 수 있습니다") {
                @Override
                public void yes() {
                    refreshMemoFromPrivate(View.GONE, Empty.stringList);
                }
            };

        } else if (view.getId() == R.id.slip_sharing_date) {
            new SetTransTypeDialog(context, transType, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    transType = (TransType) result;
                    MyUI.toastSHORT(context, String.format("%s함", transType.uiStr));
                    refreshView();
                }
            });

        } else if (view.getId() == R.id.slip_sharing_transNote) {
            TransNoteView.startActivity(context, slipFullRecord.record.getTransactionId(), new MyOnTask<List<String>>() {
                @Override
                public void onTaskDone(List<String> result) {
                    refreshMemoFromPrivate(View.VISIBLE, result);
                    MyUI.toastSHORT(context, String.format("메모가 추가되었습니다"));
                }
            });

        } else if (view.getId() == R.id.slip_sharing_counterpart) {

            new UserDetailsDialog(context, counterpart) {
                @Override
                public void onInfoUpdated(UserRecord userRecord) {
                    counterpart = userRecord;
                    tvCounterpart.setText(counterpart.getName() + " 귀하");
                }

                @Override
                public void onAccountUpdated() {
                    refreshAmounts();
                }
            };

        } else if (view.getId() == R.id.slip_sharing_to_exodar) {
            MyApp.Exodar.open(context);

        } else if (view.getId() == R.id.slip_sharing_to_dalaran) {
            MyApp.Dalaran.open(context);

        } else if (view.getId() == R.id.slip_sharing_createNewSlip) {
            onCreateNewSlipClick();

        } else if (view.getId() == R.id.slip_sharing_print || view.getId() == R.id.slip_sharing_float_print) {
            if (MyDevice.isBluetoothEnabled() == false) {
                MyUI.toast(context, String.format("블루투스가 꺼져있습니다. [프린터설정] 에서 켜실 수 있습니다"));
                return;
            }

            if (myPosPrinter.hasPairedDevice() == false) {
                MyUI.toast(context, String.format("연결된 장치가 없습니다. [프린터설정] 에서 선택해 주세요"));
                return;
            }

            if (accountUpdated == false) {
                MyUI.toastSHORT(context, String.format("매입금, 잔금 정보를 확인하지 못했습니다"));
                // return; 없어도 그냥 출력은 되게
            }

            printSlip();
        }
    }

    protected void printSlip() {
        new MyAlertDialog(context, "거래 기록 출력", "거래 기록을 출력하시겠습니까?") {
            @Override
            public void yes() {
                AccountRecord accountRecord = MAccountDB.getInstance(context).getRecordByKey(slipFullRecord.getCounterpartPhoneNumber());
                myPosPrinter.printSlip(slipFullRecord, accountRecord, accountDownloadTime, receiptValue, transType, actionType, tvMemoFromPrivate.getText().toString());
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUserSettingData(new MyOnTask() {
            @Override
            public void onTaskDone(Object result) {
                myPosPrinter = MyPosPrinter.getInstance(context);
                refreshView();
                refreshAmounts(); // 다운로드 중에 표시를 해야
                downloadAccount(counterpart);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BaseGlobal.RequestRefresh:
                refresh();
                break;
        }
    }

    public void doFinish() {
        doFinishForResult();
    }

    protected abstract void cancelSlip(boolean doCopy);

    protected void checkCancelled() {
        new CommonServiceTask(context, "SlipSharingView", "") {
            SlipDataProtocol protocol;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                protocol = transactionService.getATransaction(userSettingData.getUserShopInfo(), slipFullRecord.record.getTransactionId(), userSettingData.isBuyer());
            }

            @Override
            public void onPostExecutionUI() {
                if (protocol.isCancelled()) {
                    slipFullRecord.record.cancelled = true;
                    posSlipDB.updateOrInsertFast(new SlipFullRecord(protocol));
                    MyUI.toastSHORT(context, String.format("이미 취소된 영수증입니다"));
                    refreshView();
                }
            }
        };
    }

    public void onCancelSlipClick() {
        String myPhoneNumber = MyDevice.getMyPhoneNumber(context);
        String message = "";
        String confirmKey = "";
        if (myPhoneNumber.isEmpty()) {
            confirmKey = BaseUtils.getRandomNumber(6);
            message = String.format("취소자를 기록하기 위해 [%s] 를 입력해주세요", confirmKey);
        } else {
            if (myPhoneNumber.length() > 4) {
                confirmKey = myPhoneNumber.substring(myPhoneNumber.length() - 4, myPhoneNumber.length());
                message = String.format("취소자를 기록하기 위해 핸드폰 뒤 4자리[%s]를 입력해주세요", confirmKey);
            } else {
                confirmKey = BaseUtils.getRandomNumber(6);
                message = String.format("취소자를 기록하기 위해 [%s] 를 입력해주세요", confirmKey);
            }
        }

        message = message + "\n\n[복사후취소] = 영수증을 수정할 경우";
        new DeleteSafelyDialog(context, "거래 취소", message, confirmKey, "거래취소", "복사후취소", "닫  기") {
            @Override
            public void onDeleteClick() {
                cancelSlip(false);
            }

            public boolean onFuncClick() {
                for (SlipRecord slipRecord : posSlipDB.getRecords()) {
                    if (slipRecord.counterpartPhoneNumber.equals(slipFullRecord.record.counterpartPhoneNumber)) {
                        if (slipRecord.confirmed == false) {
                            UserRecord counterpart = userDB.getRecord(slipFullRecord.getCounterpartPhoneNumber());
                            MyUI.toastSHORT(context, String.format("%s님의 작성중인 자료가 있어 복사를 할 수 없습니다.", counterpart.name));
                            return false;
                        }
                    }
                }
                cancelSlip(true);
                return true;
            }
        };
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case close:
                doFinishForResult();
                break;

            case help:
                new SlipSharingUserGuide(context).show();
                break;

            case cancelSlip:
                // 권한 체크
                if (MemberPermission.canCancelTransaction(context, slipFullRecord.getKey().createdDateTime) == false) {
                    MemberPermission.CancelTransactionAfter1Day.toast(context);
                    return;
                }

                if (slipFullRecord.record.cancelled) {
                    MyUI.toastSHORT(context, String.format("이미 취소된 영수증입니다"));
                    return;
                }

                // 24시간 이상시 경고 메세지
                Duration duration = new Duration(slipFullRecord.record.createdDateTime, DateTime.now());
                if (duration.getStandardHours() >= 24) {
                    new MyAlertDialog(context, "[경고] 확정된 영수증 취소",
                            String.format("발행 후 %d시간이 지난 영수증입니다.\n\n관련 기록도 (거래, 주문, 미송, 잔금) 취소되는 위험한 작업입니다. 취소 하시겠습니까?", duration.getStandardHours())) {
                        @Override
                        public void yes() {
                            onCancelSlipClick();
                        }
                    };
                } else {
                    onCancelSlipClick();
                }
                break;

            case delete:
                new MyAlertDialog(context, "거래 기록 삭제", "이 기록을 목록에서 삭제하시겠습니까?") {
                    @Override
                    public void yes() {
                        posSlipDB.delete(slipFullRecord.record.id);
                        posSlipDB.items.deleteBy(slipFullRecord.getKey().toSID());
                        MyUI.toastSHORT(context, String.format("기록을 삭제했습니다"));
                        doFinishForResult();
                    }
                };
                break;

            case printSetting:
                PrinterSettingView.startActivity(context);
                break;

            case sendKakao:
                if (MyDevice.hasKakao(context) == false) {
                    MyUI.toastSHORT(context, String.format("카카오톡이 설치되어 있지 않습니다"));
                    return;
                }

                if (userSettingData.isActivated(ShoplusServiceType.NewArrivals)) {
                    new SendKakaoClick(context).show();
                } else {
                    MyUI.toastSHORT(context, String.format("카카오톡을 실행합니다"));
                    MyDevice.sendKakaoSlip(context, slipFullRecord, MyGraphics.toBitmap(printView), BaseGlobal.SHOPL_INSTALL_GUIDE);
                }

                break;

            case shareFile:
                if (userSettingData.isActivated(ShoplusServiceType.NewArrivals)) {
                    new ShareFileClick(context).show();
                } else {
                    String filePath = MyDevice.createTempImageFilePath();
                    boolean successful = MyGraphics.toFile(MyGraphics.toBitmap(printView), filePath);
                    if (successful) {
                        MyDevice.refresh(context, new File(filePath));
                        MyDevice.shareImageFile(context, filePath);
                    }
                }
                break;

            case basicInfo:
                PosSlipInfoView.startActivity(context);
                break;
        }
    }

    class SendKakaoClick extends MyButtonsDialog {

        public SendKakaoClick(final Context context) {
            super(context, "카톡 전송","");
            addButton("영수증만 전송", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    MyUI.toastSHORT(context, String.format("카카오톡을 실행합니다"));
                    MyDevice.sendKakaoSlip(context, slipFullRecord, MyGraphics.toBitmap(printView), BaseGlobal.SHOPL_INSTALL_GUIDE);
                }
            });

            addButton("영수증 + 이미지 전송", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    MyUI.toastSHORT(context, String.format("카카오톡을 실행합니다"));
                    MyDevice.sendKakaoSlip(context, slipFullRecord, MyGraphics.toBitmap(printView), slipFullRecord.serviceLink);
                }
            });

            addTitleButton("자동전송문의", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                }
            });

        }
    }

    class ShareFileClick extends MyButtonsDialog {

        public ShareFileClick(final Context context) {
            super(context, "영수증 공유","");
            addButton("영수증 공유", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    String filePath = MyDevice.createTempImageFilePath();
                    boolean successful = MyGraphics.toFile(MyGraphics.toBitmap(printView), filePath);
                    if (successful) {
                        MyDevice.refresh(context, new File(filePath));
                        MyDevice.shareImageFile(context, filePath);
                    }
                }
            });

            addButton("상품 이미지 공유", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    MyDevice.shareText(context, "구매하신 상품 이미지", slipFullRecord.serviceLink);
                }
            });

            addTitleButton("자동전송문의", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                }
            });

        }
    }

    class SlipSharingUserGuide extends BaseDialog {

        public SlipSharingUserGuide(Context context) {
            super(context);
            setDialogWidth(0.95, 0.6);
            setOnClick(R.id.dialog_order_sharing_close);
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_image_quality_guide;
        }

        @Override
        public void onClick(View v) {
            dismiss();
        }
    }


    public class AddReceivableReceiptDialog extends MyButtonsDialog {

        public AddReceivableReceiptDialog(final Context context) {
            super(context, "잔금 받음", "받은 잔금을 영수증에 추가합니다.");

            addButton("잔금 받음 (버튼식)", (userSettingData.getReceivableManagementType() != BalanceChangeMethodType.DELTA), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MemberPermission.ChangeEntrusted.hasPermission(userSettingData) == false) {
                        MemberPermission.ChangeEntrusted.toast(context);
                        return;
                    }

                    if (MemberPermission.ChangeOnline.hasPermission(userSettingData) == false) {
                        MemberPermission.ChangeOnline.toast(context);
                        return;
                    }

                    new AddReceivableFromList(context, counterpart).show(); // 목록을 띄워줘야 함
                    dismiss();
                }
            });

            addButton("잔금 받음 (은행식)", (userSettingData.getReceivableManagementType() != BalanceChangeMethodType.TRANSACTION_CASE), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MemberPermission.ChangeEntrusted.hasPermission(userSettingData) == false) {
                        MemberPermission.ChangeEntrusted.toast(context);
                        return;
                    }

                    if (MemberPermission.ChangeOnline.hasPermission(userSettingData) == false) {
                        MemberPermission.ChangeOnline.toast(context);
                        return;
                    }

                    new AddReceivableByManual(context).show();
                    dismiss();
                }
            });

            addButton("잔금 청구 (이전함)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new MyMultilinesDialog(context, "기능 이전", "잔금 청구는 영수증을 완료할 때 입력 해주세요.").show();
                }
            });
        }
    }

    public class AddReceivableByManual extends BaseDialog {
        private int receivable = 0;
        private TextView tvRemainder;
        private MyCurrencyEditText etValue;

        public AddReceivableByManual(Context context) {
            super(context);
            setDialogWidth(0.9, 0.6);
            etValue = new MyCurrencyEditText(context, getView(), R.id.dialog_request_receivable_value, R.id.dialog_request_receivable_clear_value);
            etValue.setTextViewAndBasicUnit(R.id.dialog_request_receivable_basicUnit, 1);
            etValue.setOnValueChanged(new MyOnClick<Integer>() {
                @Override
                public void onMyClick(View view, Integer value) {
                    tvRemainder.setText(BaseUtils.toCurrencyOnlyStr(receivable - value));
                }
            });
            tvRemainder = (TextView) findViewById(R.id.dialog_request_receivable_remains);
            setOnClick(R.id.dialog_request_receivable_help,
                    R.id.dialog_request_receivable_cancel, R.id.dialog_request_receivable_apply);
            MyDevice.showKeyboard(context, etValue.getEditText());
            downloadAccount(counterpart);
            MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_request_receivable_title,
                    R.id.dialog_request_receivable_current, R.id.dialog_request_receivable_current_title,
                    R.id.dialog_request_receivable_value, R.id.dialog_request_receivable_value_title, R.id.dialog_request_receivable_basicUnit,
                    R.id.dialog_request_receivable_remains_title, R.id.dialog_request_receivable_remains,
                    R.id.dialog_request_receivable_guide);
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_request_receivable;
        }

        private void downloadAccount(final UserRecord counterpart) {
            new CommonServiceTask(context, "PosSlipSharingView") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    AccountsData data = accountService.getAccounts(userSettingData.getUserShopInfo(), userSettingData.getUserConfigType(), counterpart.phoneNumber);
                    AccountRecord accountRecord = new AccountRecord(counterpart.phoneNumber, data, counterpart.name);
                    accountDB.updateOrInsert(accountRecord);
                    receivable = accountRecord.getReceivable(); // 현재 잔금
                    //  value = receivable; // 최초 값은 잔금 총액으로
                }

                @Override
                public void onPostExecutionUI() {
                    findTextViewById(R.id.dialog_request_receivable_current, BaseUtils.toCurrencyOnlyStr(receivable));
                    etValue.setOriginalValue(0);
                }
            };
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_request_receivable_help) {
                MyDevice.openWeb(context, "http://wp.me/p8qpsT-ax");
            } else if (view.getId() == R.id.dialog_request_receivable_cancel) {
                MyDevice.hideKeyboard(context, etValue.getEditText());
                dismiss();
            } else if (view.getId() == R.id.dialog_request_receivable_apply) {
                onApplyClick(etValue.getValue());
            }
        }

        public void onApplyClick(final int value) {
            // 무조건 대납(현금) 처리
            new CommonServiceTask(context, "PosSlipSharingView") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    DateTime dateTime = globalDB.getCreatedDateTime(userSettingData);
                    AccountsData data = accountService.changeAccountAmount(userSettingData.getUserShopInfo(), counterpart.phoneNumber,
                            AccountType.EntrustedAccount, -value, dateTime); // * 부호가 바뀌는 것에 주의 (뺄값을 입력받았으므로)
                    MAccountDB.getInstance(context).updateOrInsert(new AccountRecord(counterpart.phoneNumber, data, counterpart.name));
                }

                @Override
                public void onPostExecutionUI() {
                    dismiss();
                    MyDevice.hideKeyboard(context, etValue.getEditText());
                    MyUI.toastSHORT(context, String.format("잔금에서 %s을 뺐습니다.", BaseUtils.toCurrencyStr(value)));
                    receiptValue = receiptValue + value; // 청구하는 미수금은 증가하므로 +다
                    refreshAmounts();
                }
            };
        }

    }

    public class AddReceivableFromList extends BaseAccountLogDialog {

        public AddReceivableFromList(Context context, UserRecord counterpart) {
            super(context, counterpart);
        }

        @Override
        public void onItemClick(final AccountLogRecord record) {
            final AccountChangeUpdateOperationType operationType;
            String title, message;
            if (record.cleared) {
                if ((record.accountType == AccountType.OnlineAccount) && (record.isCrossing == false)) {
                    MyUI.toastSHORT(context, String.format("이 온라인은 취소해서 영수중의 미수금을 차감할 수 없습니다"));
                    return;
                }
                title = "잔금 차감";
                message = String.format("%s 완료건을 취소하시고 영수증에서 차감 하시겠습니까?", BaseUtils.toCurrencyStr(record.variation));
                operationType = AccountChangeUpdateOperationType.Restoration;
            } else {
                title = "잔금 합산";
                message = String.format("%s 미수금을 완료 처리하고 영수증에 추가 하시겠습니까?\n\n오늘 현금 입금으로 처리되고 결산에 반영 됩니다.", BaseUtils.toCurrencyStr(record.variation));
                operationType = AccountChangeUpdateOperationType.Clearance;
            }
            new MyAlertDialog(context, title, message) {
                @Override
                public void yes() {
                    switch (record.accountType) {
                        case DepositAccount:
                            break;
                        case EntrustedAccount:
                            updateAccountAndLog(record, operationType, false);
                            break;
                        case OnlineAccount:
                            updateAccountAndLog(record, operationType, true);
                            break;
                    }
                }
            };
        }

        // 온라인을 클릭해서 실제로는 대납에서 돈을 빼야 한다. 그래야 결산이 맞는다.
        private void updateAccountAndLog(final AccountLogRecord record, final AccountChangeUpdateOperationType operationType, final boolean isCrossing) {

            new CommonServiceTask(context, "PosSlipSharingView") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    AccountChangeLogDataProtocol protocol = accountService.updateAccountChange(userSettingData.getUserShopInfo(),
                            record.accountType, record.getLogId(), operationType, isCrossing);
                    AccountRecord accountRecord = accountDB.getRecordByKey(record.getCounterpart(context));
                    accountRecord.update(record.accountType, protocol, isCrossing); // 변경된 부분만 반영해야 한다
                    accountDB.updateOrInsert(accountRecord);

                    AccountLogRecord accountLogRecord = new AccountLogRecord(protocol, record.accountType);
                    accountLogDB.updateAndInsert(record.getKey(), accountLogRecord, isCrossing);
                }

                @Override
                public void onPostExecutionUI() {
                    switch (operationType) {
                        case Clearance:
                            receiptValue = receiptValue + record.variation;
                            break;
                        case Restoration:
                            receiptValue = receiptValue - record.variation;
                            break;
                    }
                    refreshAmounts();
                    MyUI.toastSHORT(context, String.format("총 추가된 잔금 금액은 %s 입니다", BaseUtils.toCurrencyStr(receiptValue)));
                    refresh();
                }
            };
        }
    }

    class SetTransTypeDialog extends MyButtonsDialog {

        public SetTransTypeDialog(Context context, TransType transType, final MyOnTask onTask) {
            super(context, "거래 설정", "");
            addRadioButtons(TransType.receipt.uiStr, TransType.receipt, TransType.bill.uiStr, TransType.bill, transType, new MyOnTask<TransType>() {
                @Override
                public void onTaskDone(TransType result) {
                    if (onTask != null)
                        onTask.onTaskDone(result);
                }
            });
            show();
        }
    }
}