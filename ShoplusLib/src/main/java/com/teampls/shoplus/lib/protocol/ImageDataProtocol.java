package com.teampls.shoplus.lib.protocol;

import android.graphics.Bitmap;

import java.io.File;

/**
 * 이미지 데이터를 교환하기 위한 인터페이스. 이미지 데이터를 특정 형식으로 반환하는 메서드로 구성한다.
 *
 * @author lucidite
 */
public interface ImageDataProtocol {
    /**
     * 이미지 데이터를 byte 배열 형식으로 반환한다.
     *
     * @return 이미지 데이터의 byte 배열
     */
    byte[] getByteArray();

    /**
     * 이미지 데이터를 Bitmap 인스턴스 형식으로 반환한다.
     *
     * @return 이미지 데이터를 Bitmap으로 변환한 데이터
     */
    Bitmap getBitmap();

    /**
     * 이미지 데이터를 파일로 저장한다.
     *
     * @param localTargetDir    파일로 저장할 경로
     * @param filename  저장할 파일 이름
     * @param compressFormat    압축 포맷 (e.g. Bitmap.CompressFormat.JPEG)
     * @return  로컬의 저장 경로를 반환한다.
     */
    String saveTo(File localTargetDir, String filename, Bitmap.CompressFormat compressFormat);
}
