package com.teampls.shoplus.lib.database;

import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2017-10-18.
 */

public class LocalItemRecord {
    public int id = 0, itemId = 0, cost = 0;

    public LocalItemRecord() {};

    public LocalItemRecord(int id, int itemId, int cost) {
        this.id = id;
        this.itemId = itemId;
        this.cost = cost;
    }

    public boolean isSame(LocalItemRecord record) {
        return BaseRecord.isSame(this, record);
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] itemId %d, cost %d", callLocation, itemId, cost));
    }
}
