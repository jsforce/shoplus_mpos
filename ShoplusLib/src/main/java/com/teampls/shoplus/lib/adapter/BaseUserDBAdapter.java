package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.SlipDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2016-09-18.
 */
abstract public class BaseUserDBAdapter extends BaseDBAdapter<UserRecord> {
    protected UserDB userDB;
    private boolean doFilterSlipCounterpart = false;
    protected boolean retailUserEnabled = false;
    private UserRecord firstRecord;
    private boolean doAddFirstRecord = false;
    protected List<String> retailPhoneNumbers = new ArrayList<>();

    public BaseUserDBAdapter(Context context) {
        super(context);
        userDB = UserDB.getInstance(context);
        setDefaultOrdering();
        setRetailMode(UserSettingData.getInstance(context).getProtocol("BaseUserDBAdapter").isRetailOn());
    }

    public void setDefaultOrdering() {
        setOrderRecord(UserDB.Column.name, false);
    }

    public BaseUserDBAdapter setUserDB(UserDB userDB) {
        this.userDB = userDB;
        return this;
    }


    public BaseUserDBAdapter resetFilters() {
        doSkipGenerate = false;
        doFilterSlipCounterpart = false;
        return this;
    }

    public void addFirstRecord(String name) {
        doAddFirstRecord = true;
        firstRecord = new UserRecord();
        firstRecord.name = name;
    }

    public BaseUserDBAdapter filterSlipCounterparts() {
        doFilterSlipCounterpart = true;
        return this;
    }

    public BaseUserDBAdapter setRetailMode(boolean value) {
        retailUserEnabled = value;
        return this;
    }

    public void refreshRetailUsers() {
        if (retailUserEnabled)
            retailPhoneNumbers = UserDB.getInstance(context).getStrings(UserDB.Column.phoneNumber, UserDB.Column.priceType, ContactBuyerType.RETAIL.toString());
    }

    @Override
    public void generate() {
        if (doSkipGenerate) return;

        refreshRetailUsers();

        Set<String> slipCounterpartPhoneNumbers = new HashSet<>();
        if (doFilterSlipCounterpart) {
            slipCounterpartPhoneNumbers.addAll(SlipDB.getInstance(context).getStrings(SlipDB.Column.counterpartPhoneNumber));
            slipCounterpartPhoneNumbers.addAll(SlipDB.getInstance(context).getStrings(SlipDB.Column.ownerPhoneNumber));
        }

        List<UserRecord> _starRecords = new ArrayList<>(0);
        generatedRecords.clear();
        for (UserRecord record : userDB.getRecordsOrderBy(orderRecords)) {
            if (doFilterSlipCounterpart)
                if (slipCounterpartPhoneNumbers.contains(record.phoneNumber) == false)
                    continue;
            if (record.name.startsWith("*")) {
                _starRecords.add(record);
            } else {
                generatedRecords.add(record);
            }
        }

        if (doAddFirstRecord)
            generatedRecords.add(0, firstRecord);
        generatedRecords.addAll(_starRecords);

        if (doSetRecordWhenGenerated)
            applyGeneratedRecords();
    }

    public class DefaultViewHolder extends BaseViewHolder {
        public ImageView ivIcon;
        public TextView name, phoneNumber;
        protected UserRecord record;
        protected LinearLayout container;

        @Override
        public int getThisView() {
            return R.layout.row_user;
        }

        @Override
        public void init(View view) {
            name = (TextView) view.findViewById(R.id.row_user_name);
            phoneNumber = (TextView) view.findViewById(R.id.row_user_phoneNumber);
            ivIcon = (ImageView) view.findViewById(R.id.row_user_icon);
            container = view.findViewById(R.id.row_user_container);
            MyView.setTextViewByDeviceSize(context, name, phoneNumber);
        }

        public void update(int position) {
            record = records.get(position);
            if (record.name.isEmpty() || record.name.equals(record.phoneNumber)) {
                name.setText("(미입력)");
            } else {
                name.setText(record.name);
            }
            phoneNumber.setText(BaseUtils.toPhoneFormat(record.phoneNumber));

            if (retailUserEnabled) {
                if (retailPhoneNumbers.contains(record.phoneNumber)) {
                    record.priceType = ContactBuyerType.RETAIL;
                    name.setText(record.getNameWithRetail());
                    name.setTextColor(ColorType.RetailColor.colorInt); // RetailColor
                    phoneNumber.setTextColor(ColorType.RetailColor.colorInt);
                    ivIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.user_navy));
                } else {
                    name.setTextColor(ColorType.Black.colorInt);
                    phoneNumber.setTextColor(ColorType.Black.colorInt);
                    ivIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.user_black));
                }
            }
        }
    }

    public class DefaultCheckableViewHolder extends BaseViewHolder {
        protected RadioButton rbChecked;
        protected TextView tvName, tvPhoneNumber;
        protected UserRecord record;
        protected LinearLayout container;

        @Override
        public int getThisView() {
            return R.layout.row_user_checkable;
        }

        @Override
        public void init(View view) {
            container = (LinearLayout) view.findViewById(R.id.row_user_checkable_container);
            tvName = (TextView) view.findViewById(R.id.row_user_checkable_name);
            tvPhoneNumber = (TextView) view.findViewById(R.id.row_user_checkable_phoneNumber);
            rbChecked = (RadioButton) view.findViewById(R.id.row_user_checkable_checked);
            rbChecked.setFocusable(false);
            rbChecked.setClickable(false);
            MyView.setTextViewByDeviceSize(context, tvName, tvPhoneNumber, rbChecked);
        }

        @Override
        public void update(int position) {
            record = records.get(position);
            this.position = position;
            tvName.setText(record.name.isEmpty() ? "(미입력)" : record.name);
            tvPhoneNumber.setText(record.phoneNumber.isEmpty() ? "(미입력)" : BaseUtils.toPhoneFormat(record.phoneNumber));
            if (clickedPositions.contains(position)) {
                rbChecked.setChecked(true);
                container.setBackgroundColor(ColorType.Beige.colorInt);
            } else {
                rbChecked.setChecked(false);
                container.setBackgroundColor(Color.TRANSPARENT);
            }
        }
    }

    public class ItemCartViewHolder extends BaseViewHolder {
        private TextView tvName, tvPhone;
        private CheckBox checked;

        @Override
        public int getThisView() {
            return R.layout.row_counterpart_checkable;
        }

        @Override
        public void init(View view) {
            tvName = (TextView) view.findViewById(R.id.row_counterpart_name);
            tvPhone = (TextView) view.findViewById(R.id.row_counterpart_phone);
            checked = (CheckBox) view.findViewById(R.id.row_counterpart_checked);
        }

        @Override
        public void update(int position) {
            UserRecord record = getRecord(position);
            BaseUtils.setText(tvName, record.name);
            BaseUtils.setText(tvPhone, BaseUtils.toPhoneFormat(record.phoneNumber));
            checked.setChecked(clickedPositions.contains(position));
        }
    }

}
