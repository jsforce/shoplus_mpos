package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseUserDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.database_global.AddressbookDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.composition.UserComposition;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_global.OldAddressbookDB;
import com.teampls.shoplus.lib.event.MyOnAsync;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2016-10-29.
 */
abstract public class AllUsersDialog extends BaseDialog implements ListView.OnItemClickListener {
    protected UserComposition userComposition;
    protected BaseUserDBAdapter adapter;
    protected boolean onLoading = false;
    protected TextView tvTitle, tvProgressMessage;
    protected LinearLayout progressContainer, container;
    protected ImageView ivOpenGoogleDialog;
    protected EditText etName;
    protected MyTextWatcher textWatcher;
    private int count = 0;
    protected List<UserRecord> fullRecords = new ArrayList<>();

    @Override
    public int getThisView() {
        return R.layout.dialog_all_user;
    }

    public AllUsersDialog(Context context) {
        super(context);
        userComposition = new UserComposition(context);
        fullRecords = userComposition.getAllSortedRecords();
        adapter = new UserDBAdapter(context);

        setDialogSize(R.id.dialog_all_user_container);
        tvTitle = (TextView) findViewById(R.id.dialog_all_user_title);
        ivOpenGoogleDialog = (ImageView) findViewById(R.id.dialog_all_user_add);
        tvProgressMessage = (TextView) findViewById(R.id.dialog_all_user_progressMessage);
        progressContainer = (LinearLayout) findViewById(R.id.dialog_all_user_progressContainer);
        progressContainer.setVisibility(View.GONE);
        etName = (EditText) findViewById(R.id.dialog_all_user_edittext);

        adapter.setRecords(fullRecords);
        textWatcher = new MyTextWatcher();
        etName.addTextChangedListener(textWatcher);
        ListView listView = (ListView) findViewById(R.id.dialog_all_user_list);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        setOnClick(R.id.dialog_all_user_refresh, R.id.dialog_all_user_add,
                R.id.dialog_all_user_close, R.id.dialog_all_user_clear);
        refresh();
        if (OldAddressbookDB.getInstance(context).isEmpty())
            reloadAync();
    }

    private void refresh() {
        if (adapter == null) return;
        adapter.generate();
        adapter.notifyDataSetChanged();
    }

    private void reloadAync() {
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.READ_CONTACTS) == false) {
            MyUI.toastSHORT(context, String.format("주소록 조회 권한이 없습니다"));
            return;
        }

        if (onLoading) return;
        onLoading = true;
        count = 0;

        new MyAsyncTask(context, "AllUsersDialog") {
            Map<String, String> addressBook = new HashMap<>();

            @Override
            public void doInBackground() {
                MyUI.setVisibility(context, progressContainer, View.VISIBLE);

                addressBook = MyDevice.getMyAddressBook(context, new MyOnAsync<UserRecord>() {
                    @Override
                    public void onSuccess(UserRecord userRecord) {
                        AddressbookDB.getInstance(context).updateOrInsert(userRecord);
                        count++;
                        MyUI.setText(context, tvProgressMessage, String.format("주소록 로딩중 ... %d", count));
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
                //
            }

            @Override
            public void onPostExecutionUI() {
                progressContainer.setVisibility(View.GONE);
                onLoading = false;
                onUpdated();
                onSyncFinished(addressBook);
            }

            @Override
            public void catchException(Exception e) {
                super.catchException(e);
                onLoading = false;
            }
        };

    }

    private void onUpdated() {
        fullRecords = userComposition.getAllSortedRecords();
        adapter.setRecords(fullRecords);
        refresh();
    }

    protected void onSyncFinished(Map<String, String> addressBook) {
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_all_user_close) {
            onCancelClick();

        } else if (view.getId() == R.id.dialog_all_user_add) {
            new UpdateUserDialog(context, new UserRecord(), DbActionType.INSERT) {
                @Override
                public void onUserInputFinish(UserRecord userRecord) {
                    fullRecords = userComposition.getAllSortedRecords();
                    etName.setText(userRecord.name);
                    etName.setSelection(userRecord.name.length());
                }
            };

        } else if (view.getId() == R.id.dialog_all_user_refresh) {
            reloadAync();

        } else if (view.getId() == R.id.dialog_all_user_clear) {
            etName.setText("");
        }
    }


    public void onCancelClick() {
        dismiss();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onItemClick(adapter.getRecord(position));
    }

    public abstract void onItemClick(UserRecord record);

    private class UserDBAdapter extends BaseUserDBAdapter {

        public UserDBAdapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new DefaultViewHolder();
        }
    }


    public class MyTextWatcher extends BaseTextWatcher {

        public void afterTextChanged(Editable s) {
            List<UserRecord> results = new ArrayList<>();
            String keyword = s.toString().toLowerCase();
            for (UserRecord record : fullRecords)
                if (record.name.toLowerCase().contains(keyword) && !record.name.isEmpty())
                    results.add(record);
            adapter.setRecords(results);
            adapter.notifyDataSetChanged();
        }
    }
}

