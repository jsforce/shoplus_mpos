package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Created by Medivh on 2017-01-19.
 */

public class MyKeyboard implements ViewTreeObserver.OnGlobalLayoutListener {
    private View containerView, focusView;
    private Context context;

    public MyKeyboard(Context context, View containerView, View focusView) {
        this.context = context;
        this.containerView = containerView;
        this.containerView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        this.focusView = focusView;
    }

    public MyKeyboard(Context context, View view, int containerResId, int focusViewResId) {
        this.context = context;
        this.containerView = view.findViewById(containerResId);
        this.containerView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        this.focusView = view.findViewById(focusViewResId);
    }

    @Override
    public void onGlobalLayout() {
        isShown();
    }

    public boolean isShown() {
    //    Log.i("DEBUG_JS", String.format("[MyKeyboard.isShown] %d - %d > %d ? %s",containerView.getRootView().getHeight(), containerView.getHeight(), MyDevice.toPixel(context, 200), ((containerView.getRootView().getHeight() - containerView.getHeight()) > MyDevice.toPixel(context, 200))));
        return ((containerView.getRootView().getHeight() - containerView.getHeight()) > MyDevice.toPixel(context, 200));
    }

    public void switchKeyboard() {
        if (focusView == null) {
            focusView = containerView;
            Log.e("DEBUG_JS", String.format("[MyKeyboard.switchKeyboard] focusView is not set"));
        }

        if (isShown()) {
            MyDevice.hideKeyboard(context, focusView);
        } else {
            MyDevice.showKeyboard(context, focusView);
        }
    }

    public void show() {
        if (isShown() == false) {
            new MyDelayTask(context, 100) {
                @Override
                public void onFinish() {
                    MyDevice.showKeyboard(context, focusView);
                }
            };
        }
    }

    public void hide() {
        if (isShown())
            MyDevice.hideKeyboard(context, focusView);
    }

    public MyKeyboard setFocusView(View focusView) {
        this.focusView = focusView;
        return this;
    }
}
