package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_global.ImageRecord;
import com.teampls.shoplus.lib.database_global.SlipImageDB;
import com.teampls.shoplus.lib.database_global.SlipImagePathDB;
import com.teampls.shoplus.lib.database_global.SlipImagePathRecord;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.enums.DBResultType;
import com.teampls.shoplus.lib.enums.SlipGenerationType;
import com.teampls.shoplus.lib.event.MyOnTask;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Medivh on 2016-09-07.
 */
public class SlipDB extends BaseDB<SlipRecord> {
    private static SlipDB instance = null;
    protected static int Ver = 1;
    public SlipImageDB images = null;
    public SlipItemDB items = null;
    public SlipImagePathDB imagePaths = null;
    public ServiceLinkDB serviceLinks = null;

    // PosSlipDB 사용 때문에 더 이상 업그레이드 하지 않는다. 확장이 필요하면 extends를 이용하자.

    public enum Column {
        _id, ownerPhoneNumber, counterpartPhoneNumber, createdDateTime, salesDateTime, confirmed,
        onlinePayment, onlinePaid, onlinePaidDateStr, entrustPayment, entrustPaid, entrustPaidDateStr, generation,
        completed, cancelled, cancelledDateTime, claimedReceivable;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    protected SlipDB(Context context) {
        super(context, "SlipDB7", "SlipDBTable", Ver, Column.toStrs()); // PosSlipDB 챙겨야 함
        imagePaths = SlipImagePathDB.getInstance(context);
        items = SlipItemDB.getInstance(context);
        images = SlipImageDB.getInstance(context);
        serviceLinks = ServiceLinkDB.getInstance(context);
    }

    protected SlipDB(Context context, String DBName, int DBversion, String[] columns) {
        super(context, DBName, DBversion, columns);
        imagePaths = SlipImagePathDB.getInstance(context);
        items = SlipItemDB.getInstance(context);
        images = SlipImageDB.getInstance(context);
        serviceLinks = ServiceLinkDB.getInstance(context);
    }

    public static SlipDB getInstance(Context context) {
        if (instance == null)
            instance = new SlipDB(context);
        return instance;
    }

    @Override
    public void deleteDB() {
        if (isOpen() == false)
            open();
        if (images != null) images.deleteDB();
        if (items != null) items.deleteDB();
        if (imagePaths != null) imagePaths.deleteDB();
        if (serviceLinks != null) serviceLinks.deleteDB();
        super.deleteDB();
    }

    @Override
    public void clear() {
        if (images != null) images.clear();
        if (items != null) items.clear();
        if (imagePaths != null) imagePaths.clear();
        if (serviceLinks != null) serviceLinks.clear();
        super.clear();
    }

    @Override
    public int delete(int id) {
        SlipRecord record = getRecord(id);
        this.deleteFromDBs(record.getSlipKey());
        return 0;
    }

    public void deleteFromDBs(SlipDataKey slipKey) {
        if (has(slipKey) == false) {
            Log.w("DEBUG_JS", String.format("[SlipDB.deleteFromDBs] %s not found", slipKey.toSID()));
            return;
        }
        SlipRecord record = getRecordByKey(slipKey);
        String slipKeyStr = slipKey.toSID();
        images.deleteAll(SlipImageDB.Column.remotePath, imagePaths.getPaths(slipKeyStr));
        imagePaths.deleteRecords(slipKeyStr);
        items.deleteBy(slipKeyStr);
        serviceLinks.deleteBy(slipKeyStr);
        super.delete(record.id);
    }

    protected ContentValues toContentvalues(SlipRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.ownerPhoneNumber.toString(), record.ownerPhoneNumber);
        contentvalues.put(Column.counterpartPhoneNumber.toString(), record.counterpartPhoneNumber);
        contentvalues.put(Column.createdDateTime.toString(), record.createdDateTime.toString());
        contentvalues.put(Column.salesDateTime.toString(), record.salesDateTime.toString());
        contentvalues.put(Column.confirmed.toString(), record.confirmed);
        contentvalues.put(Column.onlinePayment.toString(), record.onlinePayment);
        contentvalues.put(Column.onlinePaid.toString(), record.onlinePaid);
        contentvalues.put(Column.onlinePaidDateStr.toString(), record.onlinePaidDateStr);
        contentvalues.put(Column.entrustPayment.toString(), record.entrustPayment);
        contentvalues.put(Column.entrustPaid.toString(), record.entrustPaid);
        contentvalues.put(Column.entrustPaidDateStr.toString(), record.entrustPaidDateStr);
        contentvalues.put(Column.generation.toString(), record.generation.toString());
        contentvalues.put(Column.completed.toString(), record.completed);
        contentvalues.put(Column.cancelled.toString(), record.cancelled);
        contentvalues.put(Column.cancelledDateTime.toString(), record.cancelledDateTime.toString());
        contentvalues.put(Column.claimedReceivable.toString(), record.bill);
        return contentvalues;
    }

    @Override
    protected void toLogCat(int id, SlipRecord record, int option, String operation) {
        if (option == 0 || option == 1) {
            Log.i("DEBUG_JS", String.format("[%s] <%d> own %s, counter %s, created %s, sales %s",
                    operation, id,
                    record.ownerPhoneNumber,
                    record.counterpartPhoneNumber,
                    record.createdDateTime.toString(BaseUtils.fullFormat),
                    record.salesDateTime.toString(BaseUtils.fullFormat)
            ));
        }
    }

    public DBResult updateOrInsert(SlipRecord record) {
        SlipRecord dbRecord = getRecordByKey(record.getSlipKey());
        if (dbRecord.id > 0) {
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public List<SlipRecord> getRecordsByKey(List<SlipDataKey> keys) {
        List<SlipRecord> results = new ArrayList<>();
        for (SlipRecord record : getRecords()) {
            if (keys.contains(record.getSlipKey()))
                results.add(record);
        }
        return results;
    }

    public SlipRecord getRecordByKey(SlipDataKey key) {
        int id = getId(Column.ownerPhoneNumber, key.ownerPhoneNumber, Column.createdDateTime, key.createdDateTime.toString());
        if (id == 0)
            return new SlipRecord();
        return getRecord(id);
    }

    public SlipRecord getRecordByKey(String slipKey) {
        return getRecordByKey(new SlipDataKey(slipKey));
    }

    public DBResult insert(SlipFullRecord slipFullRecord) {
        List<String> emptyPaths = new ArrayList<>(0);
        return insert(slipFullRecord, emptyPaths, emptyPaths);
    }

    public DBResult insert(SlipFullRecord slipFullRecord, List<String> originalImagePaths, List<String> localImagePaths) {
        //SlipRecord record, List<String> imageRemotePaths, List<String> imageLocalPaths, List<String> imageOriginalPaths) {
        DBResult dbResult = insert(slipFullRecord.record);
        for (SlipImagePathRecord imagePath : slipFullRecord.imagePaths)
            imagePaths.insert(imagePath);
        for (SlipItemRecord slipItemRecord : slipFullRecord.items)
            items.insert(slipItemRecord);
        for (int index = 0; index < slipFullRecord.imagePaths.size(); index++) {
            String remoteImagePath = slipFullRecord.imagePaths.get(index).imagePath;
            this.images.insert(new ImageRecord(originalImagePaths.get(index), remoteImagePath, MyGraphics.toByte(localImagePaths.get(index))));
        }
        serviceLinks.put(slipFullRecord.getKey().toSID(), slipFullRecord.serviceLink);
        return dbResult;
    }

    public boolean has(SlipDataKey key) {
        // Log.i("DEBUG_JS", String.format("[SlipDB.has] own %s, time %s", key.ownerPhoneNumber, key.createdDateTime.toString()));
        return hasValue(Column.ownerPhoneNumber, key.ownerPhoneNumber, Column.createdDateTime, key.createdDateTime.toString());
    }

    public List<String> getKeys() {
        List<String> results = new ArrayList<>();
        for (SlipRecord record : getRecords())
            results.add(record.getSlipKey().toSID());
        return results;
    }

    public boolean has(String phoneNumber) {
        return (hasValue(Column.ownerPhoneNumber, phoneNumber)
                || hasValue(Column.counterpartPhoneNumber, phoneNumber));
    }

    public SlipRecord getRecordById(String transactionId) {
        if (transactionId.isEmpty())
            return new SlipRecord();
        DateTime salesDateTime = APIGatewayHelper.getDateTimeFromRemoteString(transactionId);
        return getRecord(Column.salesDateTime, salesDateTime.toString());
    }

    public DBResult updateOrInsertBySerial(SlipFullRecord slipFullRecord) {
        DBResult result = updateOrInsert(slipFullRecord.record);
        //if (result.resultType.updatedOrInserted()) 논리상으로는 이것만 해당되겠지만 .... 시간에 큰 차이가 없으므로
        for (SlipImagePathRecord imagePath : slipFullRecord.imagePaths)
            imagePaths.updateOrInsert(imagePath);
        for (SlipItemRecord slipItemRecord : slipFullRecord.items)
            items.updateOrInsertBySerial(slipItemRecord);
        serviceLinks.put(slipFullRecord.getKey().toSID(), slipFullRecord.serviceLink);
        return result;
    }

    public void updateOrInsertFast(SlipFullRecord slipFullRecord) {
        updateOrInsert(slipFullRecord.record);
        for (SlipImagePathRecord imagePath : slipFullRecord.imagePaths)
            imagePaths.updateOrInsert(imagePath);

        // 주의! items의 serial이 정리되어 있어야 한다 (여기서 정리한번 해주고 싶지만 side effect를 고려해 일단 전달하는 쪽에 책임을 넘긴다)
        items.updateOrInsertWithSlipKey(slipFullRecord.record.getSlipKey().toSID(), slipFullRecord.items);
        if (slipFullRecord.serviceLink.isEmpty() == false)
            serviceLinks.put(slipFullRecord.getKey().toSID(), slipFullRecord.serviceLink);
    }

    public SlipRecord getRecord(int id) {
        Cursor cursor = getCursor(id);
        SlipRecord record = getRecord(cursor);
        cursor.close();
        return record;
    }

    public void toLogCat(String location) {
        for (SlipRecord record : getRecords())
            record.toLogCat(location);
    }

    public void toLogCatAll(String location) {
        for (SlipRecord record : getRecords()) {
            record.toLogCat(location);
            items.toLogCat(location, record.getSlipKey().toSID());
        }
    }

    public void toLogCat(SlipRecord record, int option, String operation) {
        toLogCat(record.id, record, option, operation);
    }

    public void toLogCat(List<SlipFullRecord> fullRecords, String operation) {
        for (SlipFullRecord fullRecord : fullRecords)
            toLogCat(fullRecord.record.id, fullRecord.record, 0, operation);
    }

    public List<SlipRecord> getRecords() {
        Cursor cursor = getCursorOrderBy(Column.salesDateTime, true);
        List<SlipRecord> results = getRecords(cursor);
        cursor.close();
        return results;
    }

    @Override
    protected int getId(SlipRecord record) {
        return record.id;
    }

    @Override
    protected SlipRecord getEmptyRecord() {
        return new SlipRecord();
    }

    @Override
    protected SlipRecord createRecord(Cursor cursor) {
        return new SlipRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.ownerPhoneNumber.ordinal()),
                cursor.getString(Column.counterpartPhoneNumber.ordinal()),
                BaseUtils.toDateTime(cursor.getString(Column.createdDateTime.ordinal())),
                BaseUtils.toDateTime(cursor.getString(Column.salesDateTime.ordinal())),
                BaseUtils.toBoolean(cursor.getInt(Column.confirmed.ordinal())),
                cursor.getInt(Column.onlinePayment.ordinal()),
                BaseUtils.toBoolean(cursor.getInt(Column.onlinePaid.ordinal())),
                cursor.getString(Column.onlinePaidDateStr.ordinal()),
                cursor.getInt(Column.entrustPayment.ordinal()),
                BaseUtils.toBoolean(cursor.getInt(Column.entrustPaid.ordinal())),
                cursor.getString(Column.entrustPaidDateStr.ordinal()),
                SlipGenerationType.valueOf(cursor.getString(Column.generation.ordinal())),
                BaseUtils.toBoolean(cursor.getInt(Column.completed.ordinal())),
                BaseUtils.toBoolean(cursor.getInt(Column.cancelled.ordinal())),
                BaseUtils.toDateTime(cursor.getString(Column.cancelledDateTime.ordinal())),
                false,
                cursor.getInt(Column.claimedReceivable.ordinal())
        );
    }

    public MyMap<String, SlipRecord> getMap() {
        MyMap<String, SlipRecord> results = new MyMap<>(getEmptyRecord());
        for (SlipRecord record : getRecords())
            results.put(record.getSlipKey().toSID(), record);
        return results;
    }

    public List<ImageRecord> getImageRecords(String slipKey) {
        ArrayList<ImageRecord> results = new ArrayList<>();
        for (String remotePaths : imagePaths.getPaths(slipKey)) {
            ImageRecord record = images.getRecordBy(remotePaths);
            results.add(record);
        }
        return results;
    }

    public ImageRecord getImageRecord(String slipKey, int consecutiveNum) {
        List<ImageRecord> results = getImageRecords(slipKey);
        if (results.size() <= consecutiveNum) {
            Log.w("DEBUG_JS", String.format("[SlipDB.getImageRecord] slipKey <%d> images size %d, requests %d", slipKey, results.size(), consecutiveNum));
            return new ImageRecord();
        }
        return results.get(consecutiveNum);
    }

    public List<String> getImagePathsNoBitmap() {
        HashSet<String> allPaths = new HashSet(imagePaths.getStrings(SlipImagePathDB.Column.imagePath));
        HashSet<String> existingPaths = new HashSet(images.getStrings(SlipImageDB.Column.remotePath));
        allPaths.removeAll(existingPaths);
        return new ArrayList(allPaths);
    }

    public HashMap<String, Integer> getSalesCount() {
        HashMap<String, Integer> results = new HashMap<>(0);
        for (SlipRecord record : getRecords()) {
            String clientPhoneNumber = record.getCounterpart(context);
            if (results.containsKey(clientPhoneNumber) == false)
                results.put(clientPhoneNumber, 0);
            results.put(clientPhoneNumber, results.get(clientPhoneNumber) + 1);
        }
        return results;
    }

    public HashMap<String, DateTime> getLatestSalesDateTime() {
        HashMap<String, DateTime> results = new HashMap<>(0);
        for (SlipRecord record : getRecords()) {
            String clientPhoneNumber = record.getCounterpart(context);
            if (results.containsKey(clientPhoneNumber) == false)
                results.put(clientPhoneNumber, Empty.dateTime);
            if (record.salesDateTime.isAfter(results.get(clientPhoneNumber)))
                results.put(clientPhoneNumber, record.salesDateTime);
        }
        return results;
    }

    public static String getMax(List<String> stringList) {
        String result = "";
        for (String str : stringList) {
            if (str.compareTo(result) > 0)
                result = str;
        }
        return result;
    }

    public List<String> getUserPhoneNumbers() {
        HashSet<String> results = new HashSet(getStrings(Column.counterpartPhoneNumber));
        results.remove(UserSettingData.getInstance(context).getProviderRecord().phoneNumber);
        return new ArrayList(results);
    }

    public List<SlipFullRecord> getFullRecords() {
        List<SlipFullRecord> results = new ArrayList<>();
        for (SlipRecord slipRecord : getRecords())
            results.add(getFullRecord(slipRecord));
        return results;
    }

    public SlipFullRecord getFullRecord(int id) {
        return getFullRecord(getRecord(id));
    }

    public SlipFullRecord getFullRecord(SlipRecord record) {
        SlipFullRecord result = new SlipFullRecord();
        result.record = record;
        result.items = items.getRecordsBy(record.getSlipKey().toSID());
        result.imagePaths = imagePaths.getRecordsBy(record.getSlipKey().toSID());
        result.serviceLink = serviceLinks.getValue(record.getSlipKey().toSID());
        return result;
    }

    public List<SlipFullRecord> getFullRecords(List<SlipRecord> records) {
        List<SlipFullRecord> results = new ArrayList<>();
        for (SlipRecord record : records)
            results.add(getFullRecord(record));
        return results;
    }

    public void deleteInvalid(List<String> validKeys) {
        for (SlipRecord record : getRecords()) {
            if (validKeys.contains(record.getSlipKey().toSID()) == false) {
                delete(record.id);
            }
        }
    }

    public void deleteInvalidAsync(final int year, final int month, final List<String> validKeys) {
        new MyAsyncTask(context, "deleteInvalids") {
            @Override
            public void doInBackground() {
                for (SlipRecord record : getRecordsBy(Column.createdDateTime, year, month, null)) {
                    if (validKeys.contains(record.getSlipKey().toSID()) == false)
                        delete(record.id);
                }
            }
        };
    }

    public void deleteInvalidAsync(final DateTime fromDate, final DateTime toDateTime, final List<String> validKeys) {
        new MyAsyncTask(context, "deleteInvalids") {
            @Override
            public void doInBackground() {
                for (SlipRecord record : getRecordsBetween(Column.createdDateTime, fromDate.toString(), toDateTime.toString(), null)) {
                    if (validKeys.size() == 0 || validKeys.contains(record.getSlipKey().toSID()) == false) {
                        delete(record.id);
                    }
                }
            }
        };
    }

    public void deleteInvalid(final DateTime fromDate, final DateTime toDateTime, final List<String> validKeys) {
        List<Integer> idsForDeletion = new ArrayList<>();
        for (SlipRecord record : getRecordsBetween(Column.createdDateTime, fromDate.toString(), toDateTime.toString(), null)) {
            if (validKeys.size() == 0 || validKeys.contains(record.getSlipKey().toSID()) == false) {
                delete(record.id);
            }
        }


    }

    public List<SlipRecord> getRecordsBefore(int day) {
        DateTime dateTime = DateTime.now().minusDays(day);
        List<SlipRecord> results = new ArrayList<>();
        for (SlipRecord record : getRecords())
            if (record.salesDateTime.isBefore(dateTime))
                results.add(record);
        return results;
    }

    public List<String> getPhoneNumbers(boolean confirmed) {
        List<String> result = getStrings(Column.counterpartPhoneNumber, Column.confirmed, confirmed);
        return new ArrayList<>(new HashSet(result));
    }


    public List<SlipRecord> getRecordsBetween(DateTime fromDateTime, DateTime toDateTime, boolean doExcludeCancelled) {
        List<SlipRecord> results = new ArrayList<>();
        for (SlipRecord record : getRecordsBetween(Column.createdDateTime, fromDateTime.toString(),
                toDateTime.toString(), new QueryOrderRecord(Column.createdDateTime, false, false).toList())) {
            if (doExcludeCancelled && record.cancelled) continue;
            results.add(record);
        }
        return results;
    }

    public boolean hasUnconfirmedRecord(String counterpartPhoneNumber) {
        return hasValue(Column.confirmed, false, Column.counterpartPhoneNumber, counterpartPhoneNumber);
    }

    // Data 구조상 복수개가 가능하지만 UI에서 막았다고 가정한다. 오류가 나면 맨 마지막 것만 살린다
    public SlipRecord getUnconfirmedRecord(String counterpartPhoneNumber) {
        return getRecord(Column.confirmed, BaseUtils.toDbStr(false), Column.counterpartPhoneNumber, counterpartPhoneNumber);
    }

    public List<String> getCancelledSlipKeys() {
        List<String> results = new ArrayList<>();
        for (SlipRecord record : getRecords(Column.cancelled, true)) {
            results.add(record.getSlipKey().toSID());
        }
        return results;
    }

    public void cleanUpByMonthAsync(final int monthLimit, final MyOnTask onTask) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                DateTime fromDateTime = Empty.dateTime; // 1900-1-1
                DateTime toDateTime = BaseUtils.createMonthDateTime().minusMonths(monthLimit).minusMillis(1);
                for (SlipRecord posSlipRecord : getRecordsBetween(Column.createdDateTime, fromDateTime.toString(), toDateTime.toString(), null))
                    deleteFromDBs(posSlipRecord.getSlipKey());
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    public void cleanUpByDays(final int daysLimit, final MyOnTask onTask) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                DateTime fromDateTime = Empty.dateTime; // 1900-1-1
                DateTime toDateTime = BaseUtils.createDay().minusDays(daysLimit).minusMillis(1);
                List<SlipRecord> oldRecords = getRecordsBetween(Column.createdDateTime, fromDateTime.toString(), toDateTime.toString(), null);
                List<String> oldSlipKeys = new ArrayList<>();
                for (SlipRecord slipRecord : oldRecords)
                    oldSlipKeys.add(slipRecord.getSlipKey().toSID());
                List<String> oldPaths = imagePaths.getStringsIN(SlipImagePathDB.Column.imagePath, SlipImagePathDB.Column.slipKey, oldSlipKeys);

                imagePaths.deleteAll(SlipImagePathDB.Column.slipKey, oldSlipKeys);
                images.deleteAll(SlipImageDB.Column.remotePath, oldPaths);
                items.deleteAll(SlipItemDB.Column.slipKey, oldSlipKeys);
                serviceLinks.deleteAll(ServiceLinkDB.Column.key, oldSlipKeys);
                deleteAll(oldRecords);
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    public void cleanUpBySize(int sizeLimit) {
        if (isOpen() == false)
            open();
        int size = getSize();
        if (size <= sizeLimit) return;
        int count = 0;
        int limit = size - sizeLimit;

        List<String> invalidSlipKeys = new ArrayList<>();
        List<Integer> invalidIds = new ArrayList<>();
        for (SlipRecord slipRecord : getRecordsOrderBy(Column._id, false)) {
            invalidIds.add(slipRecord.id);
            invalidSlipKeys.add(slipRecord.getSlipKey().toSID());
            count++;
            if (count >= limit)
                break;
        }
        deleteAll(Column._id, invalidIds);
        items.deleteAll(SlipItemDB.Column.slipKey, invalidSlipKeys);
        serviceLinks.deleteAll(ServiceLinkDB.Column.key, invalidSlipKeys);
    }

    public void deleteAllBy(List<String> validSlipKeys) {
        List<String> invalidSlipKeys = new ArrayList<>();
        List<Integer> invalidIds = new ArrayList<>();
        List<String> invalidRemotePaths = new ArrayList<>();
        for (SlipRecord record : getRecords()) {
            String slipKey = record.getSlipKey().toSID();
            if (validSlipKeys.contains(slipKey) == false) {
                invalidIds.add(record.id);
                invalidSlipKeys.add(slipKey);
                invalidRemotePaths.addAll(SlipImagePathDB.getInstance(context).getPaths(slipKey));
            }
        }

        deleteAll(Column._id, invalidIds);
        images.deleteAll(SlipImageDB.Column.remotePath, invalidRemotePaths);
        imagePaths.deleteAll(SlipImagePathDB.Column.slipKey, invalidSlipKeys);
        items.deleteAll(SlipItemDB.Column.slipKey, invalidSlipKeys);
        serviceLinks.deleteAll(ServiceLinkDB.Column.key, invalidSlipKeys);
    }

}
