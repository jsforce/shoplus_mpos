package com.teampls.shoplus.lib.awsservice.dynamodb.types;

/**
 * Failure Types
 *
 * @author lucidite
 */

public enum FailureIssueType {
    /**
     * 장비별로 발생할 수 있는 이슈에 대한 Failure Log 수집 (device and os 데이터)
     */
    DEVICE_SPECIFIC("device-specific");

    private String remoteStr;

    FailureIssueType(String remoteStr) {
        this.remoteStr = remoteStr;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }
}
