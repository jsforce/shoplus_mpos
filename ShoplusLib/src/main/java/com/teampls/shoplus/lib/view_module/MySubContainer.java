package com.teampls.shoplus.lib.view_module;

import android.support.annotation.IdRes;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by Medivh on 2018-01-26.
 */

public class MySubContainer <T> {
    public T module;
    public int resId = 0;
    public boolean valid = true;
    private LinearLayout container;

    public MySubContainer() {};

    public MySubContainer(T module, View view, @IdRes int resId, boolean valid) {
        this.module = module;
        this.resId = resId;
        this.valid = valid;
        container = (LinearLayout) view.findViewById(resId);
    }

    public void setVisible(boolean visible) {
        container.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public T getModule() {
        return module;
    }

    public void setValid(boolean value) {
        this.valid = value;
    }
}
