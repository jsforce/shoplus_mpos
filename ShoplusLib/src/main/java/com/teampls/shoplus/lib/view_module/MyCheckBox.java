package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_map.GlobalBoolean;
import com.teampls.shoplus.lib.database_map.KeyValueBoolean;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.event.MyOnClick;

/**
 * Created by Medivh on 2018-01-23.
 */

public class MyCheckBox {
    private CheckBox checkBox;
    private KeyValueDB keyValueDB;
    private String key = "";
    private boolean autoSave = true;
    private Context context;
    private boolean defaultValue = false;

    public MyCheckBox(final Context context, View view, int resId, final KeyValueBoolean keyValueBoolean) {
        this(context, (CheckBox) view.findViewById(resId), keyValueBoolean);
    }

    public MyCheckBox(final Context context, final CheckBox checkBox, final KeyValueBoolean keyValueBoolean) {
        this.context = context;
        this.checkBox = checkBox;
        checkBox.setChecked(keyValueBoolean.get(context));
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoSave)
                    keyValueBoolean.put(context, checkBox.isChecked());
            }
        });
        // 호환용 코드
        keyValueDB = KeyValueDB.getInstance(context);
        key = keyValueBoolean.key;
        defaultValue = keyValueBoolean.defaultValue;
    }

    public MyCheckBox(final Context context, final CheckBox checkBox, final GlobalBoolean globalBoolean) {
        this.context = context;
        this.checkBox = checkBox;
        checkBox.setChecked(globalBoolean.get(context));
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoSave)
                    globalBoolean.put(context, checkBox.isChecked());
            }
        });
        // 호환용 코드
        keyValueDB = GlobalDB.getInstance(context);
        key = globalBoolean.key;
        defaultValue = globalBoolean.defaultValue;
    }

    public MyCheckBox(KeyValueDB keyValueDB, View view, int resId, final String key, boolean defaultValue) {
        this.keyValueDB = keyValueDB;
        this.key = key;
        this.defaultValue = defaultValue;
        checkBox = (CheckBox) view.findViewById(resId);
        checkBox.setChecked(keyValueDB.getBool(key, defaultValue));
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoSave)
                    saveValue();
            }
        });
    }

    public MyCheckBox(KeyValueDB keyValueDB, CheckBox checkBox, final String key, boolean defaultValue) {
        this.keyValueDB = keyValueDB;
        this.key = key;
        this.defaultValue = defaultValue;
        this.checkBox = checkBox;
        checkBox.setChecked(keyValueDB.getBool(key, defaultValue));
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoSave)
                    saveValue();
            }
        });
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }

    public void disableAutoSave() {
        this.autoSave = false;
    }

    public void setOnClick(boolean doOnClick, final MyOnClick<Boolean> onClick) {
        if (checkBox == null)
            return;

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("DEBUG_JS", String.format("[MyCheckBox.onClick] autoSave %s", autoSave));
                if (autoSave)
                    saveValue();
                if (onClick != null) {
                    onClick.onMyClick(checkBox, checkBox.isChecked());
                }
            }
        });

        if (doOnClick) {
            if (onClick != null) {
                onClick.onMyClick(checkBox, checkBox.isChecked());
            }
        }
    }

    public boolean isChecked() {
        if (checkBox != null)
            return checkBox.isChecked();
        else
            return false;
    }

    public void refresh() {
        if (checkBox != null && key.isEmpty() == false)
            checkBox.setChecked(keyValueDB.getBool(key, defaultValue));
    }

    public void saveValue() {
        if (checkBox != null && key.isEmpty() == false)
            keyValueDB.put(key, checkBox.isChecked());
    }
}