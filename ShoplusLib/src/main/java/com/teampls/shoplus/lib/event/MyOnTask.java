package com.teampls.shoplus.lib.event;

/**
 * Created by Medivh on 2016-04-15.
 */
public interface MyOnTask<T> {
    void onTaskDone(T result);
}
