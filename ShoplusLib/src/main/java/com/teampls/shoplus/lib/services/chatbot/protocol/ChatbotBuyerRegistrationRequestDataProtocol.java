package com.teampls.shoplus.lib.services.chatbot.protocol;

import org.joda.time.DateTime;

/**
 * 신규 고객등록 신청 데이터 클래스
 * [NOTE] 고객이 내 거래처 목록에 등록되어 있지 않을 경우, 고객이 등록 신청을 할 수 있다.
 *  서버 서비스를 통해 등록을 승인하면 새로운 거래처로 추가하면서 해당 고객의 ChatBot 접근을 승인할 수 있다.
 *
 * @author lucidite
 */
public interface ChatbotBuyerRegistrationRequestDataProtocol extends ChatbotBuyerBaseDataProtocol {
    DateTime getRequestedDateTime();
}
