package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Switch;

import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_map.GlobalBoolean;
import com.teampls.shoplus.lib.database_map.KeyValueBoolean;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.event.MyOnClick;

/**
 * Created by Medivh on 2018-01-23.
 */

public class MySwitch {
    private Switch aSwitch;
    private KeyValueDB keyValueDB;
    private String key = "";
    private boolean autoSave = true;
    private Context context;
    private boolean defaultValue = false;

    public MySwitch(final Context context, View view, int resId, final KeyValueBoolean keyValueBoolean) {
        this(context, (Switch) view.findViewById(resId), keyValueBoolean);
    }

    public MySwitch(final Context context, final Switch aSwitch, final KeyValueBoolean keyValueBoolean) {
        this.context = context;
        this.aSwitch = aSwitch;
        aSwitch.setChecked(keyValueBoolean.get(context));
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoSave)
                    keyValueBoolean.put(context, aSwitch.isChecked());
            }
        });
        // 호환용 코드
        keyValueDB = KeyValueDB.getInstance(context);
        key = keyValueBoolean.key;
        defaultValue = keyValueBoolean.defaultValue;
    }

    public MySwitch(final Context context, final Switch aSwitch, final GlobalBoolean globalBoolean) {
        this.context = context;
        this.aSwitch = aSwitch;
        aSwitch.setChecked(globalBoolean.get(context));
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoSave)
                    globalBoolean.put(context, aSwitch.isChecked());
            }
        });
        // 호환용 코드
        keyValueDB = GlobalDB.getInstance(context);
        key = globalBoolean.key;
        defaultValue = globalBoolean.defaultValue;
    }

    public MySwitch(KeyValueDB keyValueDB, View view, int resId, final String key, boolean defaultValue) {
        this.keyValueDB = keyValueDB;
        this.key = key;
        this.defaultValue = defaultValue;
        aSwitch = (Switch) view.findViewById(resId);
        aSwitch.setChecked(keyValueDB.getBool(key, defaultValue));
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoSave)
                    saveValue();
            }
        });
    }

    public MySwitch(KeyValueDB keyValueDB, Switch aSwitch, final String key, boolean defaultValue) {
        this.keyValueDB = keyValueDB;
        this.key = key;
        this.defaultValue = defaultValue;
        this.aSwitch = aSwitch;
        aSwitch.setChecked(keyValueDB.getBool(key, defaultValue));
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoSave)
                    saveValue();
            }
        });
    }

    public Switch getSwitch() {
        return aSwitch;
    }

    public void disableAutoSave() {
        this.autoSave = false;
    }

    public void setOnClick(boolean doOnClick, final MyOnClick<Boolean> onClick) {
        if (aSwitch == null)
            return;

        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("DEBUG_JS", String.format("[MySwitch.onClick] autoSave %s", autoSave));
                if (autoSave)
                    saveValue();
                if (onClick != null) {
                    onClick.onMyClick(aSwitch, aSwitch.isChecked());
                }
            }
        });

        if (doOnClick) {
            if (onClick != null) {
                onClick.onMyClick(aSwitch, aSwitch.isChecked());
            }
        }
    }

    public boolean isChecked() {
        if (aSwitch != null)
            return aSwitch.isChecked();
        else
            return false;
    }

    public void refresh() {
        if (aSwitch != null)
            aSwitch.setChecked(keyValueDB.getBool(key, defaultValue));
    }

    public void saveValue() {
        keyValueDB.put(key, aSwitch.isChecked());
    }
}