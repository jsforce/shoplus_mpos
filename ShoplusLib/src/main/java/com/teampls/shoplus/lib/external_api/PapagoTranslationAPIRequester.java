package com.teampls.shoplus.lib.external_api;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.datatypes.PapagoTranslationAPIData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * 파파고를 이용하여 한국어를 중국어 간체로 번역한다.
 * 생성 시에 사용자 설정 데이터에 포함된 API 데이터 정보를 전달해야 한다.
 *
 * [STORM-158] 현재 상품의 이름 번역 용도로 제공
 *
 * @author lucidite
 */
public class PapagoTranslationAPIRequester {
    private PapagoTranslationAPIData papago;

    public PapagoTranslationAPIRequester(PapagoTranslationAPIData papago) {
        this.papago = papago;
    }

    public String translateToChinese(String text) throws MyServiceFailureException {
        if (!this.papago.isAvailable()) {
            throw new MyServiceFailureException("NotAvailable", "API Data does not exist. User may not be allowed to use this API.");
        }
        try {
            URL url = new URL(this.papago.getApiUrl());
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setConnectTimeout(5000);
            conn.setReadTimeout(15000);
            conn.setRequestMethod("POST");
            conn.addRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            conn.addRequestProperty("X-Naver-Client-Id", this.papago.getClientId());
            conn.addRequestProperty("X-Naver-Client-Secret", this.papago.getClientSecret());

            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            // [NOTE] KO(한국어) -> zh-CN(중국어 간체)
            writer.write("source=ko&target=zh-CN&text=" + text);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            InputStream inputStream = conn.getInputStream();
            String response =  APIGatewayHelper.getStringFromInputStream(inputStream);
            JSONObject result = new JSONObject(response);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                return result.getJSONObject("message")
                        .getJSONObject("result")
                        .getString("translatedText");
            } else {
                // String errorCode = result.optString("errorCode", "");
                String errorMessage = result.optString("errorMessage", "unknown error");
                throw new MyServiceFailureException("HttpError[" + responseCode + "]", errorMessage);
            }
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        } catch (IOException e) {
            Log.e("DEBUG_TAG", "[ERROR] 통신 관련 입출력 오류: " + e.getClass().getSimpleName());
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        } catch (Exception e) {
            Log.e("DEBUG_TAG", "[ERROR] 통신 관련 입출력 오류 외 기타 오류: " + e.getClass().getSimpleName());
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }
}

