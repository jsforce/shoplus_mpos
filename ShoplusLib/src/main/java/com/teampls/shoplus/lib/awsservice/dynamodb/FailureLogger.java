package com.teampls.shoplus.lib.awsservice.dynamodb;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.awsservice.dynamodb.types.FailureIssueType;
import com.teampls.shoplus.lib.awsservice.dynamodb.types.FailureSeverity;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.database.MyDB;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;

/**
 * 아이템 매칭 및 사용자 선택 결과 logging
 *
 * @author lucidite
 */

public class FailureLogger {
    private static FailureLogger instance;
    private AmazonDynamoDBClient client;
    private DynamoDBMapper mapper;

    public static FailureLogger logger() {
        if (FailureLogger.instance == null) {
            FailureLogger.instance = new FailureLogger();
        }
        return FailureLogger.instance;
    }

    /**
     * 장비별(device-specific) failure 이슈 로그를 전송한다.
     *
     * @param user     사용자 전화번호. 실제 서버에 사용자 전화번호가 남지는 않으며, 중복을 방지하기 위해 hashing된 데이터를 ID로 사용한다.
     * @param severity failure의 심각도
     * @param summary  failure 요약 또는 문제 종류
     * @param os       OS version. android는 메서드 내부에서 prefix('android-')를 추가하므로 표기할 필요 없음
     * @param device   장치명(모델명)
     */
    public void logDeviceSpecificFailure(final String user, final FailureSeverity severity, final String summary, final String os, final String device) {
        try {
            if (client == null) {
                client = new AmazonDynamoDBClient(UserHelper.getCredentialsProvider());
                client.setRegion(Region.getRegion(Regions.AP_NORTHEAST_2));
                mapper = new DynamoDBMapper(client);
            }

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        FailureLog log = new FailureLog();

                        log.setLogId(Base64.encodeToString(user.getBytes(), 0).substring(0, 16));
                        log.setLogTime(APIGatewayHelper.getLogDateTimeString(DateTime.now()));
                        log.setLogType(FailureIssueType.DEVICE_SPECIFIC.toRemoteStr());
                        log.setSeverity(severity.toRemoteStr());
                        log.setOsVersion("android-" + os);
                        log.setSummary(summary);
                        log.setLogData("device=" + device);
                        log.setTTL(System.currentTimeMillis() / 1000 + 2592000);        // 30 days

                        mapper.save(log);
                    } catch (Exception e) {
                        Log.e("DEBUG_JS", String.format("[FailureLogger.run] %s", e.getLocalizedMessage()));
                    }
                }
            };

            Thread logThread = new Thread(runnable);
            logThread.start();
        } catch (Exception e) {
            Log.e("DEBUG_JS", String.format("[FailureLogger.logDeviceSpecificFailure] %s", summary));
            if (!MyAWSConfigs.isProduction()) {
                e.printStackTrace();
            }
            // do nothing (silent escaping)
        }
    }

    public void logDeviceSpecificFailure(Context context, final String summary) {
        logDeviceSpecificFailure(MyDB.getInstance(context).getPrivateNumber(), FailureSeverity.MINOR,
                summary, Integer.toString(MyDevice.getAndroidVersion()), MyDevice.getModelName());
    }
}
