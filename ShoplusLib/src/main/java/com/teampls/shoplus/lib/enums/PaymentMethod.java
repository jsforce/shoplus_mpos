package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2019-02-19.
 */

public enum PaymentMethod {
    Cash, Online, Entrust
}
