package com.teampls.shoplus.lib.view_base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ForgotPasswordContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.ForgotPasswordHandler;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.awsservice.ServiceErrorHelper;
import com.teampls.shoplus.lib.awsservice.cognito.CognitoUtils;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.MyRecord;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2016-10-14.
 */
abstract public class BaseLogin extends BaseActivity implements View.OnClickListener {
    protected EditText etPassword, etPhoneNumber;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myActionBar.hide();
        etPhoneNumber = (EditText) findViewById(R.id.userLogin_phonenumber);
        etPassword = (EditText) findViewById(R.id.userLogin_password);
        progressDialog = new ProgressDialog(context);

        MyView.setFontSizeByDeviceSize(context, etPhoneNumber, etPassword);
        MyView.setFontSizeByDeviceSize(context, getView(), R.id.userLogin_title, R.id.userLogin_login,
                R.id.userLogin_resetPassword, R.id.userLogin_resetPassword_title);
        MyView.setMarginByDeviceSize(context, etPhoneNumber, etPassword);
        MyView.setMarginByDeviceSize(context, getView(), R.id.userLogin_title, R.id.userLogin_login, R.id.userLogin_resetPassword);

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.PHONE) == false) {
            restartApp();
        } else {
            etPhoneNumber.setText(BaseUtils.toPhoneFormat(MyDevice.getMyPhoneNumber(context)));
        }

        setOnClick(R.id.userLogin_login, R.id.userLogin_resetPassword);
    }

    private void restartApp() {
        new MyAlertDialog(context, "전화번호 권한 없음", "전화번호를 확인할 수 없어 앱을 재시작 합니다") {
            @Override
            public void yes() {
                myDB.clear();
                new MyDelayTask(context, 500) {
                    @Override
                    public void onFinish() {
                        MyDevice.openApp(context, context.getPackageName());
                    }
                };
                finishAffinity();
            }

            @Override
            public void no() {
                finish();
            }
        };
    }

    @Override
    public int getThisView() {
        return R.layout.base_user_login;
    }

    protected void onLoginClick(String phoneNumber) {
        progressDialog.setMessage("로그인 중입니다...");
        progressDialog.show();
        String userPoolPhoneNumber = CognitoUtils.convertToCompatiblePhoneNumber(phoneNumber);
        UserHelper.getPool().getUser(userPoolPhoneNumber).signOut(); // 부작용은 없는지 체크할 것
        UserHelper.getPool().getUser(userPoolPhoneNumber).getSessionInBackground(authenticationHandler);
    }

    protected void onResetPasswordClick(String phoneNumber) {
        String userPoolPhoneNumber = CognitoUtils.convertToCompatiblePhoneNumber(phoneNumber);
        UserHelper.getPool().getUser(userPoolPhoneNumber).forgotPasswordInBackground(forgotPasswordHandler);
    }

    public void onClick(View view) {

        if (view.getId() == R.id.userLogin_login) {
            if (Empty.isEmpty(context, etPassword, "비밀번호가 입력되지 않았습니다")) return;
            if (Empty.isEmpty(context, etPhoneNumber, "전화번호가 입력되지 않았습니다")) return;
            onLoginClick(BaseUtils.toSimpleForm(etPhoneNumber.getText().toString()));

        } else if (view.getId() == R.id.userLogin_resetPassword) {
            if (Empty.isEmpty(context, etPhoneNumber, "전화번호가 입력되지 않았습니다")) return;
            new MyAlertDialog(context, "비밀번호 초기화", "문자 인증으로 비밀번호를 초기화 하시겠습니까?\n\n반드시 3분내에 입력하세야 합니다") {
                @Override
                public void yes() {
                    onResetPasswordClick(BaseUtils.toSimpleForm(etPhoneNumber.getText().toString()));
                }
            };
        }
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    abstract public void onLogin();

    AuthenticationHandler authenticationHandler = new AuthenticationHandler() {
        private String userPoolPhoneNumber;
        private String passwordInput;
        private Boolean isAuthenticatedNewly = false;

        @Override
        public void onSuccess(CognitoUserSession cognitoUserSession, CognitoDevice device) {
            UserHelper.setCurrSession(cognitoUserSession);
            UserHelper.updateCredentialsLogin(context);
            if (isAuthenticatedNewly) {
                myDB.register(new MyRecord(BaseUtils.toSimpleForm(userPoolPhoneNumber), passwordInput, true));
            } else {
                String phoneNumber = BaseUtils.toSimpleForm(etPhoneNumber.getText().toString());
                String password = BaseUtils.encode(etPassword.getText().toString(), phoneNumber);
                myDB.register(new MyRecord(phoneNumber, password, true));
            }
            MyUI.toastSHORT(context, String.format("로그인에 성공했습니다"));
            progressDialog.dismiss();
            onLogin();
        }

        @Override
        public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String username) {
            userPoolPhoneNumber = username;
            passwordInput = BaseUtils.encode(etPassword.getText().toString(), BaseUtils.toSimpleForm(userPoolPhoneNumber));
            getUserAuthentication(authenticationContinuation, userPoolPhoneNumber, passwordInput);
            isAuthenticatedNewly = true;
        }

        @Override
        public void getMFACode(MultiFactorAuthenticationContinuation multiFactorAuthenticationContinuation) {
        }

        @Override
        public void onFailure(Exception exception) {
            String userErrorMessage = ServiceErrorHelper.getCognitoIdErrorMessageForUser(exception, true);
            MyUI.toast(context, userErrorMessage);
            MyUI.toastSHORT(context, String.format("신규 유저이시면 이전 페이지로 가서 '새 고객' 버튼을 눌러주세요"));
            etPassword.setText("");
            progressDialog.dismiss();
        }

        @Override
        public void authenticationChallenge(ChallengeContinuation continuation) {
        }
    };


    private void getUserAuthentication(AuthenticationContinuation continuation, String usernameAlias, String password) {
        AuthenticationDetails authenticationDetails = new AuthenticationDetails(usernameAlias, password, null);
        continuation.setAuthenticationDetails(authenticationDetails);
        continuation.continueTask();
    }

    abstract public void onResetPassword();

    ForgotPasswordHandler forgotPasswordHandler = new ForgotPasswordHandler() {
        String newPassword = "";
        Boolean isCancelledByUser = false;

        @Override
        public void getResetCode(ForgotPasswordContinuation continuation) {
            Log.i("DEBUG_TAG", "[DEBUG] Password Reset Code: Destination=" + continuation.getParameters().getDestination());
            new PasswordResetAuthDialog(context, continuation).show();
        }

        // PasswordResetAuthDialog가 정상적으로 종료하면 호출됨
        @Override
        public void onSuccess() {
            final String phoneNumber = BaseUtils.toSimpleForm(etPhoneNumber.getText().toString());
            myDB.resetPassword(phoneNumber, newPassword, true);
            new MyDelayTask(context, 100) {
                @Override
                public void onFinish() {
                    onResetPassword();
                    Log.i("DEBUG_TAG", "[DEBUG] 비밀번호가 초기화되었습니다[" + phoneNumber + "]: " + newPassword);
                }
            };
        }

        public void onFailure(Exception exception) {
            String userErrorMessage;
            if (isCancelledByUser) {
                userErrorMessage = "사용자에 의해 취소되었습니다.";
            } else {
                userErrorMessage = ServiceErrorHelper.getCognitoIdErrorMessageForUser(exception, true);
            }
            MyUI.toast(context, userErrorMessage);
            Log.e("DEBUG_JS", String.format("[UserLogin.onFailure] %s", exception.getLocalizedMessage()));
        }

        class PasswordResetAuthDialog extends BaseDialog {
            private EditText etAuthenCode, etPassword;
            private ForgotPasswordContinuation continuation;
            private LinearLayout passwordContainer;

            public PasswordResetAuthDialog(Context context, ForgotPasswordContinuation continuation) {
                super(context);
                this.continuation = continuation;
                setOnClick(R.id.dialog_input_authenCode_apply);
                setOnClick(R.id.dialog_input_authenCode_cancel);
                setOnClick(R.id.dialog_input_authenCode_clear);
                setOnClick(R.id.dialog_input_authenCode_password_clear);
                passwordContainer = (LinearLayout) findViewById(R.id.dialog_input_authenCode_password_container);

                etAuthenCode = (EditText) findTextViewById(R.id.dialog_input_authenCode_edit, "");
                etPassword = (EditText) findTextViewById(R.id.dialog_input_authenCode_password_edit, "");
            }

            @Override
            public int getThisView() {
                return R.layout.dialog_input_authencode;
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.dialog_input_authenCode_password_clear) {
                    etPassword.setText("");
                } else if (view.getId() == R.id.dialog_input_authenCode_cancel) {
                    isCancelledByUser = true;
                    continuation.continueTask();
                    MyUI.toastSHORT(context, String.format("비밀번호 초기화를 중지하겠습니다"));
                    dismiss();
                } else if (view.getId() == R.id.dialog_input_authenCode_apply) {
                    if (Empty.isEmpty(context, etAuthenCode, "인증번호가 입력되지 않았습니다")) return;
                    if (Empty.isEmpty(context, etPassword, "비밀번호가 입력되지 않았습니다")) return;
                    // 전화번호 혹은 유저가 입력한 번호를 다시 encode해서 사용한다
                    newPassword = BaseUtils.encode(etPassword.getText().toString().trim(), BaseUtils.toSimpleForm(etPhoneNumber.getText().toString()));
                    continuation.setPassword(newPassword);
                    continuation.setVerificationCode(etAuthenCode.getText().toString());
                    continuation.continueTask();
                    MyUI.toastSHORT(context, String.format("비밀번호를 초기화 했습니다"));
                    dismiss();
                } else if (view.getId() == R.id.dialog_input_authenCode_clear) {
                    etAuthenCode.setText("");
                }
            }
        }
    };

}