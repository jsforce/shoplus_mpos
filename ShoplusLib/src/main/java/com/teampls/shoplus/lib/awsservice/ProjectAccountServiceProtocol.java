package com.teampls.shoplus.lib.awsservice;

import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.datatypes.AccountsReceivableChangeLogData;
import com.teampls.shoplus.lib.enums.AccountChangeUpdateOperationType;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

/**
 * @author lucidite
 */

public interface ProjectAccountServiceProtocol {

    /**
     * 특정 거래처의 현재 계정정보(잔금/매입금)를 조회한다.
     *
     * @param userShop
     * @param myUserType 현재 사용자의 타입(도/소매)
     * @param counterpart
     * @return
     * @throws MyServiceFailureException
     */
    AccountsData getAccounts(UserShopInfoProtocol userShop, UserConfigurationType myUserType, String counterpart) throws MyServiceFailureException;

    /**
     * 전체 계정정보(잔금/매입금)를 조회한다. 잔금/매입금 기록이 있는 거래처만 포함한다.
     *
     * (주의) 판매자(도매)용 서비스만 제공한다. 구매자(소매)는 현재 전체를 보는 것이 의미가 없으므로 제외한다.
     * 어플리케이션에서 구매자(소매)로 설정된 경우 이 서비스를 호출하지 않아야 한다.
     *
     * @param userShop
     * @return 거래처(전화번호)를 키로 하고 계정정보를 값으로 하는 맵 데이터
     * @throws MyServiceFailureException
     */
    Map<String, AccountsData> getAccountsSituation(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 특정 거래처의 현재 계정정보를 수동으로 수정한다(한 번에 특정 타입 1종만 수정).
     * 입금/수금을 반영할 때, 또는 초기값을 설정하거나 오차값을 보정할 때 사용한다.
     * (** 소매는 사용 불가)
     *
     * @param userShop
     * @param counterpart
     * @param accountType
     * @param changeAmount 증감할 금액. (+)일 경우 잔액 증가, (-)일 경우 잔액 감소
     * @param userSpecifiedCreatedDateTime 수동으로 일시를 지정할 때 사용, 자동지정할 경우 null로 입력헤야 한다
     * @return
     * @throws MyServiceFailureException
     */
    AccountsData changeAccountAmount(UserShopInfoProtocol userShop, String counterpart, AccountType accountType, int changeAmount, DateTime userSpecifiedCreatedDateTime) throws MyServiceFailureException;

    /**
     * 특정 거래처의 온라인 또는 대납 계정 금액을 대납 또는 온라인 계정으로 이체 처리한다.
     * 온라인은 대납으로, 대납은 온라인으로 정해진 금액만큼 이전한다.
     * (** 소매는 사용 불가)
     *
     * @param userShop
     * @param counterpart
     * @param sourceAccountType 온라인일 경우 지정 금액을 대납으로, 대납일 경우 지정 금액을 온라인으로 이체
     * @param transferAmount 이체 금액 (sourceAccountType에서 차감되고 반대편 계정에서는 금액 추가)
     * @return
     * @throws MyServiceFailureException
     */
    AccountsData transferAccountAmount(UserShopInfoProtocol userShop, String counterpart, AccountType sourceAccountType, int transferAmount) throws MyServiceFailureException;

    /**
     * 특정 거래처 + 특정 계정 타입의 장기 변경이력을 가져온다.
     *
     * @param userShop
     * @param myUserType 현재 사용자의 타입(도/소매)
     * @param counterpart
     * @param accountType
     *
     * @return
     * @throws MyServiceFailureException
     */
    List<AccountChangeLogDataProtocol> getAccountChangeHistory(UserShopInfoProtocol userShop, UserConfigurationType myUserType, String counterpart, AccountType accountType) throws MyServiceFailureException;

    /**
     * 특정 주간의 잔금(온라인 및 대납) 계정 변경이력을 가져온다.
     * [NOTE] 거래잔금을 조회하는 기본 함수로, 기존의 getAccountsReceivableChangeHistory 함수를 대체한다.
     *
     * @param userShop
     * @param myUserType
     * @param startDateOfWeek 해당 주가 시작되는 첫 날짜 (일요일 권장)
     * @return
     * @throws MyServiceFailureException
     */
    AccountsReceivableChangeLogData getWeeklyAccountsReceivableChangeHistory(UserShopInfoProtocol userShop, UserConfigurationType myUserType, DateTime startDateOfWeek) throws MyServiceFailureException;

    /**
     * 거래기록 생성에 의한 특정 잔금 항목을 완료처리하거나 완료처리를 취소한다.
     * (기존 UI 컨셉(특정 항목의 해소 방식)에 익숙한 사용자를 위한 API)
     *
     * @param userShop
     * @param accountType 온라인(online) 또는 대납(entrusted)
     * @param changeLogId 변경하고자 하는 잔금 변경 로그의 ID (거래기록에 의해 생성된 변경 로그만 지원한다.)
     * @param operation 완료처리(clearance) 또는 완료처리 취소(restoration)
     * @param isCrossingAccounts 계정 교차적용, Clearance일 때에만 유효하며, 완료처리 시에 대납->온라인, 온라인->대납(현금)으로 계정을 바꾸어 적용할 때 사용한다.
     * @return 새로 추가된 완료처리 또는 완료처리 취소 로그 (동기화 전에 기존 거래기록에 의한 로그 상태 변경을 반영하려면 이는 별도로 처리해야 한다)
     * @throws MyServiceFailureException
     */
    AccountChangeLogDataProtocol updateAccountChange(UserShopInfoProtocol userShop, AccountType accountType, String changeLogId, AccountChangeUpdateOperationType operation, Boolean isCrossingAccounts) throws MyServiceFailureException;

    /**
     * 결산을 위해 특정 기간 내의 대납 변경 이력을 조회한다.
     *
     * @param userShop
     * @param myUserType 현재 사용자의 타입(도/소매)
     * @param from 조회 시작 시간(결산 시작)
     * @param to 조회 종료 시간(결산 종료)
     * @return 해당 기간 내의 대납 변경 이력을 반환한다 (결산 시에 사용할 수납 금액은 금액이 (-)인 항목의 합산 금액)
     * @throws MyServiceFailureException
     */
    List<AccountChangeLogDataProtocol> downloadEntrustedChangeHistory(UserShopInfoProtocol userShop, UserConfigurationType myUserType, DateTime from, DateTime to) throws MyServiceFailureException;

}
