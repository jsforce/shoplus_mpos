package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2017-12-03.
 */

public class MyEmptyGuide {
    public String message = "";
    private View myContainer, otherContainer;
    private TextView tvMessage, tvContactUs;
    private BaseDBAdapter adapter;
    private View view;

    public MyEmptyGuide(final Context context, View view, BaseDBAdapter adapter, View otherContainer) {
        this(context, view, adapter, otherContainer, R.id.module_empty_container, R.id.module_empty_message, R.id.module_empty_contactUs);
    }

    public MyEmptyGuide(final Context context, View view, BaseDBAdapter adapter, View otherContainer, @IdRes int containerId, int messageId, final int contactUsId) {
        this.view = view;
        myContainer = view.findViewById(containerId);
        this.otherContainer = otherContainer;
        tvMessage = (TextView) view.findViewById(messageId);
        tvContactUs = (TextView) view.findViewById(contactUsId);
        this.adapter = adapter;
        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUI.toastSHORT(context, String.format("안내 페이지로 이동합니다"));
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
            }
        });
        MyView.setTextViewByDeviceSize(context, tvMessage, tvContactUs);
    }

    public void setAdapter(BaseDBAdapter adapter) {
        this.adapter = adapter;
    }

    public void setMessage(String message) {
        tvMessage.setText(message);
    }

    public void setContactUsButton(String title, View.OnClickListener onClickListener) {
        tvContactUs.setText(title);
        tvContactUs.setOnClickListener(onClickListener);
    }

    public void setContactUsButtonText(String title) {
        tvContactUs.setText(title);
    }

    public void setContactUsButton(int visibility) {
        tvContactUs.setVisibility(visibility);
    }


    public void refresh() {
        if (adapter.getCount() == 0) {
            otherContainer.setVisibility(View.GONE);
            myContainer.setVisibility(View.VISIBLE);
        } else {
            otherContainer.setVisibility(View.VISIBLE);
            myContainer.setVisibility(View.GONE);
        }
    }


}
