package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseAccountLogDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.database.AccountLogRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.CAccountLogDB;
import com.teampls.shoplus.lib.enums.AccountChangeOperationType;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;

import java.util.List;

/**
 * Created by Medivh on 2017-11-15.
 */

abstract public class BaseAccountLogDialog extends BaseDialog implements AdapterView.OnItemClickListener {
    private ListView listView;
    private BaseAccountLogDBAdapter adapter;
    protected CAccountLogDB accountLogDB;
    protected UserRecord counterpart;

    public BaseAccountLogDialog(Context context, UserRecord counterpart) {
        super(context);
        setDialogSize(0.95, 0.95);
        this.counterpart = counterpart;
        this.accountLogDB = CAccountLogDB.getInstance(context);
        adapter = new BaseAccountLogDBAdapter(context, accountLogDB) {
            @Override
            public BaseViewHolder getViewHolder() {
                return new LogViewHolder();
            }
        };
        adapter.filterAccountType(AccountType.OnlineAccount, AccountType.EntrustedAccount);
        adapter.blockCancelled(true).doShowAsSummary(true)
                .filterAccountType(AccountType.OnlineAccount, AccountType.EntrustedAccount)
                .filterOperationType(AccountChangeOperationType.Transaction);
        listView = (ListView) findViewById(R.id.dialog_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        findTextViewById(R.id.dialog_list_title, "온라인 / 대납 목록");
        setVisibility(View.GONE, R.id.dialog_list_message);
        downloadCounterpartReceivable(counterpart.phoneNumber);
        setOnClick(R.id.dialog_list_close);
    }

    protected void refresh() {
        if (adapter == null) return;
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                adapter.generate();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
            }
        };
    }

    protected void onDismiss() {

    }

    @Override
    public int getThisView() {
        return R.layout.dialog_list;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_list_close) {
            dismiss();
            onDismiss();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AccountLogRecord record = adapter.getRecord(position);
        onItemClick(record);
    }

    abstract public void onItemClick(AccountLogRecord record);

    private void downloadCounterpartReceivable(final String counterpartPhoneNumber) {
        new CommonServiceTask(context, "BaseAccountLogDialog") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<AccountChangeLogDataProtocol> onlineProtocols =
                        accountService.getAccountChangeHistory(userSettingData.getUserShopInfo(),userSettingData.getUserConfigType(), counterpartPhoneNumber, AccountType.OnlineAccount);
                List<AccountChangeLogDataProtocol> entrustProtocols =
                        accountService.getAccountChangeHistory(userSettingData.getUserShopInfo(), userSettingData.getUserConfigType(), counterpartPhoneNumber, AccountType.EntrustedAccount);
                accountLogDB.set(onlineProtocols, entrustProtocols);
            }

            @Override
            public void onPostExecutionUI() {
                adapter.filterPhoneNumbers(counterpartPhoneNumber);
                refresh();
            }
        };
    }
}
