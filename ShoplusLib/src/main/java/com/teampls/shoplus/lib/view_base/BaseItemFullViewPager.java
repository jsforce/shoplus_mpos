package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.MyUITask;
import com.teampls.shoplus.lib.database_global.ImageRecord;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.datatypes.ItemDataBundle;
import com.teampls.shoplus.lib.datatypes.SpecificItemDataBundle;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.DeleteSafelyDialog;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view.ImagePickerView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2017-03-27.
 */

abstract public class BaseItemFullViewPager extends BaseActivity implements ViewPager.OnPageChangeListener {
    protected Context context = BaseItemFullViewPager.this;
    protected ViewPager viewPager;
    protected BaseFullViewAdapter<ItemRecord> viewAdapter;
    protected static int currentPosition = 0;
    protected static List<ItemRecord> records = new ArrayList<>(0);
    private TextView tvPosition;
    protected ItemDB itemDB;
    protected Set<Integer> visitedPositions = new HashSet<>();

    @Override
    public int getThisView() {
        return R.layout.base_itemfullview_frame;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        visitedPositions.clear();
        setCurrentPosition(currentPosition);
        itemDB = ItemDB.getInstance(context);
        setViewAdapter();
        viewPager = (ViewPager) findViewById(R.id.itemfullview_pager);
        viewPager.setAdapter(viewAdapter);
        viewPager.setCurrentItem(getCurrentPosition()); // startActivity에서 인자로 받음
        viewPager.addOnPageChangeListener(this);
        tvPosition = findTextViewById(R.id.itemfullview_position, "");
        refreshPosition();
    }

    protected void setCurrentPosition(int position) {
        visitedPositions.add(position);
        currentPosition = position;
    }

    public void onPageSelected(int position) {
        setCurrentPosition(position);
        refreshPosition();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }

    abstract protected void setViewAdapter();

    public void removeView(final int position) {
        if (viewAdapter.getCount() <= 0)
            return;

        new MyUITask(context) {
            @Override
            public void doInBackground() {
                boolean atEnd = (position == viewAdapter.getCount() - 1);
                viewAdapter.records.remove(position); // 전체 개수 감소
                viewAdapter.notifyDataSetChanged();
                setCurrentPosition(atEnd? Math.max(0, position - 1) : position); // 0 위치에서 삭제시 -1 발생 가능
                refreshPosition();
            }
        };
    }

    public void removeVisitedPosition(int position) {
        visitedPositions.remove(position);
    }

    public void removeVisitedPositionOld(int position) {
        List<Integer> positions = new ArrayList<>(visitedPositions);
        int removeIndex = 0;
        for (int i = 0; i < positions.size(); i++) {
            int p = positions.get(i);
            if (p == position) {
                removeIndex = i;
            } else if (p > position) {
                positions.set(i, p - 1);
            }
        }
        positions.remove(removeIndex);
        visitedPositions = new HashSet(positions);
    }

    private void refreshPosition() {
        MyUI.setText(context, tvPosition, String.format("%d/%d", getCurrentPosition() + 1, viewAdapter.getCount()));
    }

    public static int getCurrentPosition() {
        return currentPosition;
    }

    @Override
    public void onMyMenuCreate() {
        if (BaseAppWatcher.isNetworkConnected() == false) {
            onMyMenuCreateDisconnected();
            return;
        }
        myActionBar
                .add(MyMenu.changeImage)
                .add(MyMenu.download)
                .add(MyMenu.delete)
                .add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    protected void onMyMenuCreateDisconnected() {
        myActionBar.setTitle("오프라인 모드");
        myActionBar.clear();
        myActionBar
                .add(MyMenu.download)
                .add(MyMenu.close);
    }

    protected void downloadImages() {
        ItemRecord record = viewAdapter.getRecord(getCurrentPosition());
        ImageRecord imageRecord = ItemDB.getInstance(context).images.getRecordBy(record.itemId);
        imageRecord.saveAsFile(context);
    }

    protected void changeMainImage() {
        final ItemRecord itemRecord = records.get(currentPosition);
        ImagePickerView.start(context, 1, true, new MyOnClick<List<String>>() {
            @Override
            public void onMyClick(View view, List<String> imagePaths) {
                if (imagePaths == null || imagePaths.size() == 0)
                    return;

                final String imagePath = imagePaths.get(0);
                if (imagePath == null || imagePath.length() <= 0 || new File(imagePath).exists() == false) {
                    MyUI.toastSHORT(context, String.format("촬영된 이미지에 문제가 발견되었습니다"));
                    return;
                }

                new CommonServiceTask(context, "SilverItemFullViewPager", "이미지 교체...") {
                    @Override
                    public void doInBackground() throws MyServiceFailureException {
                        String standardFilePath = MyDevice.createTempFilePath(BaseAppWatcher.appDir, "jpg");
                        Pair<Boolean, Bitmap> created = MyGraphics.toStandardBitmapAndImageFile(imagePath, standardFilePath);
                        if (created.first) {
                            String newMainRemotePath = itemService.changeMainImage(userSettingData.getUserShopInfo(), itemRecord.itemId, standardFilePath);
                            itemDB.changeMainImage(itemRecord, standardFilePath, newMainRemotePath, created.second);
                        }
                    }

                    @Override
                    public void onPostExecutionUI() {
                        viewAdapter.getFullView(currentPosition).update(currentPosition);
                    }

                };
            }
        });
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case refresh:
                final ItemRecord record = viewAdapter.getRecord(getCurrentPosition());
                itemDB.deleteBy(record.itemId);

                new CommonServiceTask(context, "searchRecord", "상품 다운로드 중...") {
                    @Override
                    public void doInBackground() throws MyServiceFailureException {
                        SpecificItemDataBundle bundle = itemService.getSpecificItem(userSettingData.getUserShopInfo(), record.itemId);
                        itemDB.updateOrInsertWithOptionSync(new ItemDataBundle(bundle));
                    }

                    @Override
                    public void onPostExecutionUI() {
                        BaseViewHolder viewHolder = viewAdapter.getFullView(getCurrentPosition());
                        viewHolder.refresh();
                        viewHolder.refreshAdapter();
                    }
                };
                break;
            case changeImage:
                changeMainImage();
                break;
            case close:
                onFinish();
                break;
            case delete:
                onDeleteClick();
                break;
            case download:
                new MyAlertDialog(context, "이미지 파일로 저장", "이 이미지를 파일로 저장 하시겠습니까?") {
                    public void yes() {
                        downloadImages();
                    }
                };
                break;
        }
    }

    public void onDeleteClick() {
        if (viewAdapter.getCount() <= 0) {
            MyUI.toastSHORT(context, String.format("삭제할 상품이 없습니다"));
            return;
        }

        new DeleteSafelyDialog(context, "상품삭제") {
            @Override
            public void onDeleteClick() {
                deleteItem();
            }
        };
    }

    protected void onItemDeleted(int itemId) {}

    protected void deleteItem() {
        new CommonServiceTask(context, "deleteItem", "삭제중...") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                ItemRecord record = viewAdapter.getRecord(getCurrentPosition());
                itemService.deleteItem(userSettingData.getUserShopInfo(), record.itemId);
                itemDB.deleteBy(record.itemId);
                onItemDeleted(record.itemId); // 추가로 더 지울게 있을때
                MyUI.toastSHORT(context, String.format("삭제 완료"));
            }

            @Override
            public void onPostExecutionUI() {
                if (itemDB.getSize() == 0) {
                    onFinish();
                } else {
                    removeView(getCurrentPosition());
                    removeVisitedPosition(getCurrentPosition());
                    if (viewAdapter.getCount() == 0)
                        onFinish(); //  남아 있는 view가 없음
                }
            }
        };

    }

    abstract public void onFinish();

    @Override
    public void onBackPressed() {
        onFinish();
    }

}
