package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MAccountDB;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.CounterpartTransView;

/**
 * Created by Medivh on 2017-05-05.
 */

abstract public class UserDetailsDialog extends BaseDialog {
    private UserRecord record;
    private TextView tvName, tvAddress, tvReceivable, tvDeposit;
    private Button btReceivable, btDeposit;
    protected Button btFunction;

    public UserDetailsDialog(Context context, UserRecord record) {
        super(context);
        this.record = record;
        setDialogWidth(0.9, 0.7);
        tvName = findTextViewById(R.id.dialog_user_name, record.getNameWithRetail());
        findTextViewById(R.id.dialog_user_phoneNumber, BaseUtils.toPhoneFormat(record.phoneNumber));
        tvAddress = findTextViewById(R.id.dialog_user_address, record.location);
        if (record.location.isEmpty())
            setVisibility(View.GONE, R.id.dialog_user_address);
        setOnClick(R.id.dialog_user_close, R.id.dialog_user_callPhone,
                R.id.dialog_user_show_trans, R.id.dialog_user_set_deposit,
                R.id.dialog_user_updateInfo, R.id.dialog_user_set_receivable);
        setVisibility(View.GONE, R.id.dialog_user_function);
        show();

        tvReceivable = findTextViewById(R.id.dialog_user_receivable);
        tvDeposit = findTextViewById(R.id.dialog_user_deposits);
        btReceivable = findViewById(R.id.dialog_user_set_receivable);
        btDeposit = findViewById(R.id.dialog_user_set_deposit);
        btFunction = findButtonById(R.id.dialog_user_function);

        if (MyApp.get(context) == MyApp.Darnassus) {
            setVisibility(View.GONE, R.id.dialog_user_receivable, R.id.dialog_user_deposits);
        } else {
            refreshAccounts(record);
        }
    }

    public void showFunction(String title) {
        btFunction.setVisibility(View.VISIBLE);
        btFunction.setText(title);
    }

    public void hideFunction() {
        btFunction.setVisibility(View.GONE);
    }

    public UserRecord getRecord() {
        return record;
    }

    private void refreshAccounts(final UserRecord record) {
        if (record.phoneNumber.isEmpty())
            return;

        new CommonServiceTask(context, "UserDetailsDialog") {
            UserConfigurationType userConfigurationType;
            AccountRecord accountRecord;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                userConfigurationType = UserSettingData.getInstance(context).getUserConfigType();
                if (userConfigurationType != null) {
                    AccountsData data = accountService.getAccounts(userSettingData.getUserShopInfo(), userConfigurationType, record.phoneNumber);
                    accountRecord = new AccountRecord(record.phoneNumber, data, record.name);
                    MAccountDB.getInstance(context).updateOrInsert(accountRecord);
                }
            }

            @Override
            public void onPostExecutionUI() {
                if (userConfigurationType == null) {
                    setVisibility(View.GONE, R.id.dialog_user_receivable, R.id.dialog_user_deposits);
                } else {
                    btReceivable.setEnabled(true);
                    btDeposit.setEnabled(true);
                }
                refresh();
            }
        };
    }

    private void refresh() {
        AccountRecord accountRecord = MAccountDB.getInstance(context).getRecordByKey(record.phoneNumber);
        tvReceivable.setText(String.format("잔금 : %s", BaseUtils.toCurrencyStr(accountRecord.getReceivable())));
        tvDeposit.setText(String.format("매입금 : %s", BaseUtils.toCurrencyStr(accountRecord.deposit)));
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_user_details;
    }

    protected void onShowTransClick() {
        CounterpartTransView.startActivity(context, record, null);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_user_close) {
            dismiss();

        } else if (view.getId() == R.id.dialog_user_show_trans) {
            onShowTransClick();

        } else if (view.getId() == R.id.dialog_user_set_receivable) {
            dismiss();
            new SetAccountDialog(context, record, AccountType.OnlineAccount, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    onAccountUpdated();
                }
            });

        } else if (view.getId() == R.id.dialog_user_set_deposit) {
            new SetAccountDialog(context, record, AccountType.DepositAccount, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    refresh();
                    onAccountUpdated();
                }
            });

        } else if (view.getId() == R.id.dialog_user_callPhone) {
            MyDevice.callPhone(context, record.phoneNumber);

        } else if (view.getId() == R.id.dialog_user_function) {
            onFunctionClick();

        } else if (view.getId() == R.id.dialog_user_updateInfo) {
            if (record.isGuest(userSettingData)) {
                MyUI.toastSHORT(context, String.format("[고객] 정보는 수정할 수 없습니다."));
                return;
            }

            if (record.isRetail(userSettingData)) {
                MyUI.toastSHORT(context, String.format("[소매] 정보는 수정할 수 없습니다."));
                return;
            }

            new UpdateUserDialog(context, record, DbActionType.UPDATE) {
                @Override
                public void onUserInputFinish(UserRecord userRecord) {
                    record = userRecord;
                    tvName.setText(record.getNameWithRetail());
                    tvAddress.setText(record.location);
                    onInfoUpdated(userRecord);
                }
            };
        }
    }

    abstract public void onInfoUpdated(UserRecord userRecord);

    abstract public void onAccountUpdated();

    public void onFunctionClick() {
    }

    ;
}