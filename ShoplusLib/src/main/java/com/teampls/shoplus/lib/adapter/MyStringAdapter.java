package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.teampls.shoplus.R;


/**
 * Created by Medivh on 2017-12-20.
 */

public class MyStringAdapter extends BaseDBAdapter<String> {
    private boolean doShowAsSingleLine = false, boundaryLineEnabled = false;

    public MyStringAdapter(Context context) {
        super(context);
    }

    public void showAsSingleLine() {
        this.doShowAsSingleLine = true;
    }

    public void enableBoundaryLine() {
        boundaryLineEnabled = true;
    }

    @Override
    public void generate() {

    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new BaseViewHolder() {
            private TextView textView;

            @Override
            public int getThisView() {
                return R.layout.row_string;
            }

            @Override
            public void init(View view) {
                textView = findTextViewById(context, view, R.id.row_string_textview);
                if (boundaryLineEnabled)
                    textView.setBackground(context.getResources().getDrawable(R.drawable.background_round_button));
            }

            @Override
            public void update(int position) {
                String string = records.get(position);
                if (doShowAsSingleLine)
                    string.replace("\n",".");
                textView.setText(string);
            }
        };
    }
}