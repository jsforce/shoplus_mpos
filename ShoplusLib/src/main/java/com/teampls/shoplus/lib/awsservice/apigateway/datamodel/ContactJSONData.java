package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * (거래처의) 연락처 업로드/다운로드를 위한 데이터 클래스
 *
 * @author lucidite
 */

public class ContactJSONData implements ContactDataProtocol, JSONDataWrapper {
    private String contactNumber;
    private String name;
    private ContactBuyerType priceType;
    private long updated;
    private boolean deleted;
    private String location;
    private boolean priceDiscriminated;

    public ContactJSONData(JSONObject data) {
        this.contactNumber = data.optString("contact", "");
        this.name = data.optString("name", "");
        this.priceType = ContactBuyerType.fromRemoteRetailFlagValue(data.optBoolean("retail", false));
        this.updated = data.optLong("updated", 0);
        this.deleted = data.optString("deleted","") != "";       // acquirer 체크 (merged?)
        this.location = data.optString("location", "");
        this.priceDiscriminated = data.optBoolean("price_disc", false);
    }

    public ContactJSONData(ContactDataProtocol contact) throws JSONException {
        this.contactNumber = contact.getContactNumber();
        this.name = contact.getName();
        this.priceType = contact.getBuyerType();
        this.updated = contact.getUpdatedTimeStamp();
        this.deleted = contact.isDeleted();
        this.location = contact.getLocation();
        this.priceDiscriminated = contact.isPriceDiscriminated();
    }

    @Override
    public String getContactNumber() {
        return this.contactNumber;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public ContactBuyerType getBuyerType() {
        return this.priceType;
    }

    @Override
    public Long getUpdatedTimeStamp() {
        return this.updated;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }

    @Override
    public String getLocation() {
        return this.location;
    }

    @Override
    public boolean isPriceDiscriminated() {
        return this.priceDiscriminated;
    }

    @Override
    public JSONObject getObject() throws JSONException {
        JSONObject data = new JSONObject();
        if (JSONDataUtils.exists(this.contactNumber)) {
            data.put("contact", this.contactNumber);
        }
        if (JSONDataUtils.exists(this.name)) {
            data.put("name", this.name);
        }
        if (this.priceType == ContactBuyerType.RETAIL) {
            data.put("retail", true);
        }
        if (this.updated > 0) {
            data.put("updated", this.updated);
        }
        if (this.deleted) {
            data.put("deleted", "__DUMMY__");
        }
        if (JSONDataUtils.exists(this.location)) {
            data.put("location", this.location);
        }
        if (this.priceDiscriminated) {
            data.put("price_disc", true);
        }
        return data;
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("ContactJSONData instance does not have a JSON array");
    }

    @Override
    public String toString() {
        return "Contact: name=" + this.getName() + ", location=" + this.getLocation()
                + "(" + this.getBuyerType().toUiStr() + ")";
    }

    @Deprecated
    public static Map<String, ContactDataProtocol> fromJSONObject(JSONObject dataObj) throws JSONException {
        Map<String, ContactDataProtocol> result = new HashMap<>();

        Iterator<String> phoneNumbers = dataObj.keys();
        while(phoneNumbers.hasNext()) {
            String phoneNumber = phoneNumbers.next();
            JSONObject contactDataObj = dataObj.getJSONObject(phoneNumber);
            result.put(phoneNumber, new ContactJSONData(contactDataObj));
        }
        return result;
    }
}
