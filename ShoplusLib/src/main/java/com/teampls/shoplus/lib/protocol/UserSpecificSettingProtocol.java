package com.teampls.shoplus.lib.protocol;

/**
 * 개별 사용자 설정 데이터 프로토콜 (로그인한 사용자)
 *
 * @author lucidite
 */

public interface UserSpecificSettingProtocol {
    String getShop();
    void setShop(String shop);
}
