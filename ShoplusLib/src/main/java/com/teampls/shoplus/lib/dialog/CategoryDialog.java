package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.enums.ItemCategorySet;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-03-15.
 */

abstract public class CategoryDialog extends BaseDialog implements View.OnClickListener, AdapterView.OnItemClickListener {
    private CategoryAdapter adapter;
    private ListView listView;
    private MyRadioGroup<ItemCategorySet> rgCategorySet;
    private final String KEY_CATEGORY = "last.selected.category";
    private MyMap<ItemCategoryType, Integer > categoryCountMap = new MyMap<>(0);

    public CategoryDialog(Context context, String title, List<ItemRecord> itemRecords, boolean doShowItemCount,
                          boolean doShowAutoClassification, boolean doShowSelectAll) {
        super(context);

        setDialogWidth(0.95, 0.6);
        setDialogHeight(0.9, 0.6);
        adapter = new CategoryAdapter(context, itemRecords);
        adapter.setViewType(doShowItemCount ? CategoryAdapter.ViewCategoryWithCount : CategoryAdapter.ViewCategoryOnly);

        if (doShowItemCount)
            for (ItemRecord itemRecord : itemRecords)
                categoryCountMap.addInteger(itemRecord.category, 1);

        listView = (ListView) findViewById(R.id.dialog_category_view);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        findTextViewById(R.id.dialog_category_title, title);
        findTextViewById(R.id.dialog_category_autoclassficiation);
        findTextViewById(R.id.dialog_category_all, String.format("전체 보기"));
        setOnClick(R.id.dialog_category_close, R.id.dialog_category_autoclassficiation, R.id.dialog_category_all);
        if (doShowAutoClassification == false)
            setVisibility(View.GONE, R.id.dialog_category_autoclassficiation);
        if (doShowSelectAll == false)
            setVisibility(View.GONE, R.id.dialog_category_all);

        rgCategorySet = new MyRadioGroup<>(context, getView());
        rgCategorySet.add(R.id.dialog_category_cloth, ItemCategorySet.Cloth)
                .add(R.id.dialog_category_shoes, ItemCategorySet.Shoes)
                .add(R.id.dialog_category_etc, ItemCategorySet.ETC);
        rgCategorySet.setKeyAndInit(context, KEY_CATEGORY, ItemCategorySet.Cloth);
        rgCategorySet.setOnClickListener(false, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });
        refresh();
        show();
    }

    private void refresh() {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                adapter.filterCategorySet(rgCategorySet.getCheckedItem());
                adapter.generate();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_category;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_category_close) {
            dismiss();
        } else if (view.getId() == R.id.dialog_category_autoclassficiation) {
            new MyAlertDialog(context, "자동 분류 시작", "미지정 상품을 대상으로 이름을 분석해서 자동으로 분류합니다.\n\n(결과는 임시로만 저장되며 시범 기능입니다. )") {
                @Override
                public void yes() {
                    classifyCategoryByName();
                }
            };
        } else if (view.getId() == R.id.dialog_category_all) {
            onItemClicked(ItemCategoryType.ALL);
            dismiss();
        }
    }

    private void classifyCategoryByName() {
        ItemDB itemDB = ItemDB.getInstance(context);
        MyUI.toastSHORT(context, String.format("분류를 시작합니다"));
        for (ItemRecord record : adapter.itemRecords) {
            if (record.category == ItemCategoryType.NONE) {
                record.category = ItemCategoryType.getType(record.name);
                if (record.id != 0) {
                    itemDB.update(record.id, record);
                } else {
                    ItemRecord dbRecord = itemDB.getRecordBy(record.itemId);
                    dbRecord.category = record.category;
                    itemDB.update(dbRecord.id, dbRecord);
                }
            }
        }
        MyUI.toastSHORT(context, String.format("분류가 끝났습니다"));
        refresh();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        ItemCategoryType category = adapter.getRecord(position);
        onItemClicked(category);
        dismiss();
    }

    abstract public void onItemClicked(ItemCategoryType category);

    public class CategoryAdapter extends BaseDBAdapter<ItemCategoryType> {
        public static final int ViewCategoryWithCount = 0, ViewCategoryOnly = 1;
        private boolean doFilterByCategorySet = false;
        private ItemCategorySet categorySet = ItemCategorySet.Cloth;
        private List<ItemRecord> itemRecords = new ArrayList<>();

        public CategoryAdapter(Context context, List<ItemRecord> itemRecords) {
            super(context);
            this.itemRecords = itemRecords;
        }

        public void filterCategorySet(ItemCategorySet categorySet) {
            this.doFilterByCategorySet = true;
            this.categorySet = categorySet;
        }

        @Override
        public void generate() {
            records.clear();
            for (ItemCategoryType record : ItemCategoryType.values()) {
                if (doFilterByCategorySet)
                    if (record.getSet() != categorySet)
                        continue;
                records.add(record);
            }
        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new BaseViewHolder() {
                private TextView tvName, tvCount;

                @Override
                public int getThisView() {
                    return R.layout.row_item_category;
                }

                @Override
                public void init(View view) {
                    tvName = findTextViewById(context, view, R.id.row_item_category_name);
                    tvCount = findTextViewById(context, view, R.id.row_item_category_count);
                }

                @Override
                public void update(int position) {
                    ItemCategoryType record = records.get(position);
                    tvName.setText(record.toUIStr());
                    int count = categoryCountMap.get(record);
                    tvCount.setText(String.format("%s",  count == 0 ? "-" : count));
                    switch (viewType) {
                        case ViewCategoryWithCount:
                            tvCount.setVisibility(View.VISIBLE);
                            break;
                        case ViewCategoryOnly:
                            tvCount.setVisibility(View.GONE);
                            break;
                    }

                    if (record == ItemCategoryType.ALL) {
                        tvName.setTypeface(Typeface.DEFAULT_BOLD);
                        tvCount.setTypeface(Typeface.DEFAULT_BOLD);
                    } else {
                        tvName.setTypeface(Typeface.DEFAULT);
                        tvCount.setTypeface(Typeface.DEFAULT);
                    }
                }
            };
        }
    }
}


