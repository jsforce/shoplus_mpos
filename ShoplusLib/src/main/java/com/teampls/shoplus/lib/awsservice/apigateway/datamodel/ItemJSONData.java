package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.apigateway.protocol.APIInputModelInterface;
import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author lucidite
 */
public class ItemJSONData implements ItemDataProtocol, APIInputModelInterface, JSONDataWrapper {
    private JSONObject data;

    public ItemJSONData(JSONObject dataObj) {
        this.data = dataObj;
    }

    @Override
    public int getItemId() {
        return data.optInt("id");
    }

    @Override
    public String getSerialNumber() {
        return data.optString("sn", "00-0000");
    }

    @Override
    public String getProviderPhoneNumber() {
        return data.optString("provider");
    }

    @Override
    public ItemStateType getItemState() {
        String stateStr = data.optString("item_state", "");
        return ItemStateType.fromRemoteStr(stateStr);
    }

    @Override
    public ItemCategoryType getCategory() {
        return ItemCategoryType.fromRemoteStr(data.optString("category", null));
    }

    @Override
    public String getItemName() {
        return data.optString("item_name");
    }

    @Override
    public String getChineseName() {
        return data.optString("cn_name");
    }

    @Override
    public String getItemDescription() {
        return data.optString("description");
    }

    @Override
    public String getMainImagePath() {
        return data.optString("main_image");
    }

    @Override
    public int getPrice() {
        return data.optInt("price");
    }

    @Override
    public int getRetailPrice() {
        return data.optInt("retail");
    }

    public String getPrivateNote() {
        return data.optString("note");
    }

    @Override
    public DateTime getCreatedDatetime() {
        return APIGatewayHelper.getDateTimeFromRDSString(data.optString("created"));
    }

    @Override
    public DateTime getModifiedDatetime() {
        String modified = data.optString("modified");
        if (modified == null || modified.equals("null") || modified.isEmpty()) {
            return this.getCreatedDatetime();
        } else {
            return APIGatewayHelper.getDateTimeFromRDSString(modified);
        }
    }

    @Override
    public DateTime getPickedDatetime() {
        String picked = data.optString("picked");
        if (picked == null || picked.equals("null") || picked.isEmpty()) {
            return this.getCreatedDatetime();
        } else {
            return APIGatewayHelper.getDateTimeFromRDSString(picked);
        }
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return this.data;
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("ItemJSONData instance does not have a JSON array");
    }

    @Override
    public String toInputModelString() {
        return data.toString();
    }

    /* [DEPRECATED]
    private boolean exists(String str) { return (str != null) && !str.isEmpty(); }
    private void buildDataObject(int id, String serialNumber, String provider, ItemStateType state,
                                 ItemCategoryType category, String itemName, String chineseName, String description, String mainImagePath,
                                 int price, int retailPrice, String note, DateTime created, DateTime modified, DateTime picked) throws JSONException {

        data = new JSONObject();

        data.put("id", id);
        data.put("sn", serialNumber);
        data.put("provider", provider);
        data.put("item_state", state.toRemoteStr());

        if (category != ItemCategoryType.NONE)
            data.put("category", category.toRemoteStr());
        if (exists(itemName))  data.put("item_name", itemName);
        if (exists(chineseName)) data.put("cn_name", chineseName);
        if (exists(description)) data.put("description", description);
        data.put("main_image", mainImagePath);

        // [SILVER-5] Additional fields: price, retailPrice, note
        if (price > 0) data.put("price", price);                // 0 이하일 경우 필드를 작성하지 않는다. (서버에 null로 업로드)
        if (retailPrice > 0) data.put("retail", retailPrice);
        if (exists(note)) data.put("note", note);

        data.put("created", APIGatewayHelper.getRDSDateTimeString(created));
        if (modified != null) data.put("modified", APIGatewayHelper.getRDSDateTimeString(modified));
        if (picked != null) data.put("picked", APIGatewayHelper.getRDSDateTimeString(picked));
    } */
}
