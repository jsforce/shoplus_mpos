package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.QueryOrderRecord;
import com.teampls.shoplus.lib.database_memory.MItemDB;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.MemberPermission;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view.MyNumberPlusMinus;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view.OptionStockTraceView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2017-03-10.
 */

abstract public class BaseItemOptionDBAdapter extends BaseDBAdapter<ItemOptionRecord> {
    protected ItemDB itemDB;
    protected MItemDB mItemDB;
    protected boolean doSplitUnconfirmed = false, doFilterItemRecords = false, doFilterOptionRecords = false;
    protected MyOnClick<ItemOptionRecord> onRowClick, onStockUpdated, onOptionCountUpdated;
    protected Map<Integer, ItemRecord> selectedItemRecords = new LinkedHashMap<>();
    protected List<ItemOptionRecord> selectedOptionRecords = new ArrayList<>();
    protected int incrementalOfCreationView = 1;
    public MyMap<ItemOptionRecord, Integer> optionCountMap = new MyMap<>(0);

    public BaseItemOptionDBAdapter(Context context, ItemDB itemDB) {
        super(context);
        mode = Mode.FileDB;
        setSortingKey(MSortingKey.MultiField);
        this.itemDB = itemDB;
    //    setDefaultOrdering();
    }

    public BaseItemOptionDBAdapter(Context context, MItemDB mItemDB) {
        super(context);
        mode = Mode.MemoryDB;
        setSortingKey(MSortingKey.MultiField);
        this.mItemDB = mItemDB;
    }

    public void toLogCat(String callLocation) {
        if (callLocation.startsWith("g.")) {
            for (ItemOptionRecord record : getGeneratedRecords())
                record.toLogCat(callLocation);
        } else {
            for (ItemOptionRecord record : getRecords())
                record.toLogCat(callLocation);
        }
    }

    public void setDefaultOrdering() {
        setOrderRecord(ItemOptionDB.Column.key, true);
    }

    public ItemRecord getItemRecordBy(int itemId) {
        if (selectedItemRecords.containsKey(itemId)) {
            return selectedItemRecords.get(itemId);
        } else {
            return getItemRecordBy(itemId);
        }
    }

    public void setIncrementalOfCreationView(int value) {
        this.incrementalOfCreationView = value;
    }

    public void setOnRowClick(MyOnClick<ItemOptionRecord> onRowClick) {
        this.onRowClick = onRowClick;
    }

    public void splitUnconfirmed() {
        doSplitUnconfirmed = true;
    }

    public void setOnStockUpdated(MyOnClick<ItemOptionRecord> onStockUpdated) {
        this.onStockUpdated = onStockUpdated;
    }

    public void setOnOptionCountUpdated(MyOnClick onOptionCountUpdated) {
        this.onOptionCountUpdated = onOptionCountUpdated;
    }

    public BaseItemOptionDBAdapter resetFilters() {
        doSkipGenerate = false;
        doFilterItemRecords = false;
        return this;
    }

    public BaseItemOptionDBAdapter filterItemRecord(ItemRecord itemRecord) {
        List<ItemRecord> itemRecords = new ArrayList<>();
        itemRecords.add(itemRecord);
        return filterItemRecords(itemRecords);
    }

    public BaseItemOptionDBAdapter filterItemRecords(List<ItemRecord> itemRecords) {
        this.doFilterItemRecords = true;
        this.selectedItemRecords.clear();
        for (ItemRecord itemRecord : itemRecords)
            this.selectedItemRecords.put(itemRecord.itemId, itemRecord);
        return this;
    }

    public void setFilterItemRecords(boolean value) {
        doFilterItemRecords = value;
    }

    public void updateItemRecords(List<ItemRecord> itemRecords) {
        for (ItemRecord itemRecord : itemRecords) {
            selectedItemRecords.put(itemRecord.itemId, itemRecord);
        }
    }

    public BaseItemOptionDBAdapter filterItemOptionRecords(List<ItemOptionRecord> optionRecords) {
        this.doFilterOptionRecords = true;
        this.selectedOptionRecords = optionRecords;
        return this;
    }

    @Override
    public void generate() {
        if (doSkipGenerate) return;

        List<ItemOptionRecord> _recordsUnconfirmed = new ArrayList<>();
        List<ItemOptionRecord> optionRecords = new ArrayList<>();

        switch (mode) {
            default:
                List<QueryOrderRecord> queryOrderRecords = new ArrayList<>();
                if (doSortByKey == false) { // fallback을 위해
                    queryOrderRecords.add(new QueryOrderRecord(ItemOptionDB.Column.itemId, true, true));
                    queryOrderRecords.add(new QueryOrderRecord(ItemOptionDB.Column.color, false, false));
                    queryOrderRecords.add(new QueryOrderRecord(ItemOptionDB.Column.sizeOrdinal, true, false));
                }

                if (doFilterItemRecords) {
                    if (selectedItemRecords.size() <= 950) {
                       optionRecords = itemDB.options.getRecordsIN(ItemOptionDB.Column.itemId, BaseUtils.toStringList(new ArrayList(selectedItemRecords.keySet())), queryOrderRecords);
                    } else {
                        for (ItemOptionRecord record : itemDB.options.getRecordsOrderBy(queryOrderRecords)) {
                            if (selectedItemRecords.keySet().contains(record.itemId))
                                optionRecords.add(record);
                        }
                    }
                } else {
                    // option 전체를 펼쳐놓고 보는 일은 없다. 전체를 다 가져오는 것은 매우 비효율적이다.
                    optionRecords = itemDB.options.getRecordsOrderBy(queryOrderRecords);
                }
                break;

            case MemoryDB:
                if (doFilterItemRecords) {
                    optionRecords = mItemDB.options.getRecordsBy(new ArrayList(selectedItemRecords.keySet()));
                } else {
                    // option 전체를 펼쳐놓고 보는 일은 없다. 전체를 다 가져오는 것은 매우 비효율적이다.
                    // 재고 파악할때는 있다...
                    optionRecords = mItemDB.options.getRecords();
                }
                break;
        }

        generatedRecords.clear();
        for (ItemOptionRecord record : optionRecords) {

            if (doSplitUnconfirmed) {
                if (record.confirmed == false) {
                    _recordsUnconfirmed.add(record);
                    continue;
                }
            }

            if (doFilterOptionRecords)
                if (selectedOptionRecords.contains(record) == false)
                    continue;

            record._sortingKey = MSortingKey.MultiField; // item-color-size 정렬
            generatedRecords.add(record);
        }

        if (doSortByKey) {
            Collections.sort(generatedRecords);
            Collections.sort(_recordsUnconfirmed);
        }

        if (doSplitUnconfirmed)
            generatedRecords.addAll(_recordsUnconfirmed);

        if (doSetRecordWhenGenerated)
            applyGeneratedRecords();
    }

    public void sortByItemRecords(List<ItemRecord> selectedItemRecords) {
        MyListMap<Integer, ItemOptionRecord> itemIdOptionsMap = new MyListMap<>();
        for (ItemOptionRecord optionRecord : getGeneratedRecords())
            itemIdOptionsMap.add(optionRecord.itemId, optionRecord);

        List<ItemOptionRecord> results = new ArrayList<>();
        for (ItemRecord record : selectedItemRecords)
            results.addAll(itemIdOptionsMap.get(record.itemId));
        generatedRecords = results;
    }

    private List<ItemOptionRecord> doSortingBySize(List<ItemOptionRecord> records) {
        List<ItemOptionRecord> results = new ArrayList<>();
        for (SizeType sizeType : SizeType.values()) {
            for (ItemOptionRecord record : records) {
                if (record.size == sizeType) {
                    results.add(record);
                    records.remove(record);
                    break;
                }
            }
        }
        return results;
    }

    public class ItemFullViewHolder extends BaseViewHolder implements View.OnClickListener {
        private ImageView ivColor;
        private TextView tvColor, tvSize, tvStock, tvInBound;
        private LinearLayout container;
        private ItemOptionRecord record = new ItemOptionRecord();
        private MyNumberPlusMinus numberPlusMinus;
        private boolean doShowInBoundButton = false;

        @Override
        public int getThisView() {
            return R.layout.row_item_option_cart;
        }

        public void showInBoundButton(boolean value) {
            this.doShowInBoundButton = value;
        }

        @Override
        public void init(View view) {
            container = (LinearLayout) view.findViewById(R.id.row_item_option_cart_container);
            ivColor = (ImageView) view.findViewById(R.id.row_item_option_cart_color_imageview);
            ivColor.setImageResource(R.drawable.background_box);
            tvColor = (TextView) view.findViewById(R.id.row_item_option_cart_color);
            tvSize = (TextView) view.findViewById(R.id.row_item_option_cart_size);
            tvStock = (TextView) view.findViewById(R.id.row_item_option_cart_stock);
            tvInBound = (TextView) view.findViewById(R.id.row_item_option_cart_inStock);

            MyView.setTextViewByDeviceSizeScale(context, 0.5, tvColor, tvSize, tvSize, tvStock, tvInBound);

            // 숫자부분 클릭
            tvStock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (record.isSoldOut) {
                        MyUI.toastSHORT(context, String.format("판매완료 옵션입니다 (변경은 오늘쪽 컬러명 클릭)"));
                        return;
                    }

                    if (MemberPermission.ChangeStockValue.hasPermission(userSettingData) == false) {
                        MemberPermission.ChangeStockValue.toast(context);
                        return;
                    }
                    new UpdateValueDialog(context, "재고수량", "", record.revStock) {
                        @Override
                        public void onApplyClick(String newValue) {
                            updateRevStock(BaseUtils.toInt(newValue));
                        }
                    };
                }
            });

            // 입고버튼 클릭
            tvInBound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (record.isSoldOut) {
                        MyUI.toastSHORT(context, String.format("판매완료 옵션입니다 (변경은 오늘쪽 컬러명 클릭)"));
                        return;
                    }

                    if (MemberPermission.ChangeStockValue.hasPermission(userSettingData) == false) {
                        MemberPermission.ChangeStockValue.toast(context);
                        return;
                    }
                    new UpdateValueDialog(context, "입고수량", "기존 재고와 합산합니다.", 0) {
                        @Override
                        public void onApplyClick(String newValue) {
                            updateRevStock(record.revStock + BaseUtils.toInt(newValue));
                        }
                    };
                }
            });

            // 플러스, 마이너스 버튼 클릭
            numberPlusMinus = new MyNumberPlusMinus(view, Integer.MIN_VALUE);
            numberPlusMinus.setTextView(R.id.row_item_option_cart_stock);
            numberPlusMinus.setPlusMinusButtons(R.id.row_item_option_cart_plus, R.id.row_item_option_cart_minus);
            if (MemberPermission.ChangeStockValue.hasPermission(userSettingData) == false)
                numberPlusMinus.setEnabled(false);
            numberPlusMinus.setOnClick(new MyOnClick<Integer>() {
                @Override
                public void onMyClick(View view, Integer value) {
                    if (BaseAppWatcher.isNetworkConnected() == false)  {
                        MyUI.toastSHORT(context, String.format("네트워크 연결이 필요합니다"));
                    } else if (MemberPermission.ChangeStockValue.hasPermission(userSettingData) == false) {
                        MemberPermission.ChangeStockValue.toast(context);
                    } else if (record.isSoldOut) {
                        MyUI.toastSHORT(context, String.format("판매완료 상태입니다 (변경은 오늘쪽 컬러명 클릭)"));
                    } else {
                        updateRevStock(value);
                    }
                }
            });

            MyView.setTextViewByDeviceSizeScale(context, 0.5, numberPlusMinus.getTextView());

            // 나머지 부분 클릭
            view.findViewById(R.id.row_item_option_cart_colorSizeContainer).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (record == null) return;
                    new OnRowClickDialog(context, record).show();
                }
            });

            if (BaseAppWatcher.isNetworkConnected() == false) {
                tvStock.setEnabled(false);
                numberPlusMinus.setEnabled(false);
                setEnabled(view, false, R.id.row_item_option_cart_colorSizeContainer);
            }
        }

        class OnRowClickDialog extends MyButtonsDialog {

            public OnRowClickDialog(final Context context, final ItemOptionRecord optionRecord) {
                super(context, optionRecord.toStringWithDefault(" / "), "삭제는 재고가 0일때만 할 수 있습니다. (0으로 변경 후 저장)");


                addButton("재고변동 보기", true, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                        OptionStockTraceView.startActivity(context, optionRecord);
                    }
                });

                if (BasePreference.isQREnabled(context) && userSettingData.isActivated(ShoplusServiceType.ItemTrace)) {
                    addButton("QR 재고 보기", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startItemTraceOptionView(optionRecord);
                            dismiss();
                        }
                    });
                }

                addButton(optionRecord.isSoldOut ? "판매중으로" : "판매완료로", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                        new CommonServiceTask(context, "BaseItemOptionDBAdapter", "변경중...") {
                            @Override
                            public void doInBackground() throws MyServiceFailureException {
                                itemService.updateItemOptionState(userSettingData.getUserShopInfo(), optionRecord.toStockKey(), !optionRecord.isSoldOut);
                                optionRecord.isSoldOut = !optionRecord.isSoldOut;
                                itemDB.options.update(optionRecord.id, optionRecord);
                            }

                            @Override
                            public void onPostExecutionUI() {
                                if (onRowClick != null)
                                    onRowClick.onMyClick(null, optionRecord);
                            }
                        };
                    }
                });

                addButton("삭  제", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (optionRecord.stock != 0) {
                            MyUI.toast(context, String.format("실수 방지를 위해 재고가 0일때만 가능합니다 (저장되었는지 확인)"));
                            return;
                        }

                        // 여기서 권한체크가 필요할 수 있다. 삭제권한
                        // 수량 조절 권한과 연동을 시키면 종업원이 잘못 생성한 옵션을 삭제

                        new MyAlertDialog(context, "컬러-사이즈 삭제", "삭제하시겠습니까? 삭제하면 이 옵션과 관련된 재고 기록도 모두 사라집니다") {
                            @Override
                            public void yes() {
                                dismiss();

                                new CommonServiceTask(context, "ItemOptionDBAdapter", "삭제중...") {
                                    @Override
                                    public void doInBackground() throws MyServiceFailureException {
                                        itemService.deleteItemStock(userSettingData.getUserShopInfo(), optionRecord.toStockKey());
                                        switch (mode) {
                                            case FileDB:
                                                itemDB.options.delete(optionRecord.id);
                                                break;
                                            case MemoryDB:
                                                mItemDB.options.deleteByKey(optionRecord.key);
                                                break;
                                        }
                                    }

                                    @Override
                                    public void onPostExecutionUI() {
                                        if (onRowClick != null)
                                            onRowClick.onMyClick(null, optionRecord);
                                        MyUI.toastSHORT(context, String.format("옵션 삭제"));
                                    }

                                    @Override
                                    public void catchException(MyServiceFailureException e) {
                                        super.catchException(e);
                                        if (e.getLocalizedMessage().contains("Nonzero Value"))
                                            MyUI.toast(context, String.format("현재 재고가 0이 아닙니다. 다른 사용자가 사용 중일 수 있습니다"));
                                    }
                                };
                            }
                        };

                    }
                });






            }
        }

        protected void startItemTraceOptionView(ItemOptionRecord itemOptionRecord) {
        }

        private void updateRevStock(int newRevStock) {
            record.revStock = newRevStock;
            switch (mode) {
                case FileDB:
                    itemDB.options.updateOrInsert(record);
                    break;
                case MemoryDB:
                    mItemDB.options.updateOrInsert(record);
                    break;
            }
            refreshStock();
            if (onStockUpdated != null)
                onStockUpdated.onMyClick(null, record);
        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0)
                return;
            this.position = position;
            record = records.get(position);
            ivColor.setBackgroundColor(record.color.colorInt);
            tvColor.setText(UserSettingData.getColorName(record.color));
            tvSize.setText(record.size.uiName);
            tvInBound.setVisibility(doShowInBoundButton ? View.VISIBLE : View.GONE);
            BaseUtils.setTextCancelled(record.isSoldOut, ColorType.Gray, tvSize, tvColor, numberPlusMinus.getTextView());
            numberPlusMinus.setEnabled(!record.isSoldOut);
            refreshStock();
            //      Log.i("DEBUG_JS", String.format("[ItemFullViewHolder.update] record.confirmed? %s, stockDB.confirmed? %s", record.confirmed,  stocks.isConfirmed(record)));
        }

        private void refreshStock() {
            tvStock.setText(String.format("%d", record.revStock));
            container.setBackgroundColor(record.isOnRevision() ? ColorType.LightGrey.colorInt :
                   Color.TRANSPARENT);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.row_item_option_cart_plus) {
            }
        }
    }

    public class NameCheckableViewHolder extends BaseViewHolder {
        private TextView tvName;
        private RadioButton checked;

        @Override
        public int getThisView() {
            return R.layout.row_string_checkable;
        }

        @Override
        public void init(View view) {
            tvName = (TextView) view.findViewById(R.id.row_string_checkable_textview);
            checked = (RadioButton) view.findViewById(R.id.row_string_checkable_checked);
            MyView.setTextViewByDeviceSize(context, tvName, checked);
        }

        @Override
        public void update(int position) {
            ItemOptionRecord record = getRecord(position);
            tvName.setText(record.toStringWithDefault("/"));
            checked.setChecked(clickedPositions.contains(position));
        }
    }

    public class OptionAndQuantityViewHolder extends BaseViewHolder {
        private ImageView ivColor;
        private TextView tvOption, tvQuantity;
        private LinearLayout container;

        @Override
        public int getThisView() {
            return R.layout.row_search_option;
        }

        @Override
        public void init(View view) {
            container = (LinearLayout) view.findViewById(R.id.row_search_option_container);
            tvOption = (TextView) view.findViewById(R.id.row_search_option_name);
            tvQuantity = (TextView) view.findViewById(R.id.row_search_option_quantity);
            tvQuantity.setTypeface(Typeface.DEFAULT_BOLD);
            tvQuantity.setTextColor(ColorType.Blue.colorInt);
            ivColor = view.findViewById(R.id.row_search_option_color);
            ivColor.setImageResource(R.drawable.background_box);
            MyView.setTextViewByDeviceSize(context, tvOption, tvQuantity);
        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0) return;
            ItemOptionRecord record = getRecord(position);
            tvOption.setText(record.toStringWithDefault(" : "));
            tvQuantity.setText(String.format("%d", record.revStock));
            ivColor.setBackgroundColor(record.color.colorInt);

            // clicked position 이 아니라 revStock 으로 표시
            if (record.revStock >= 1) {
                container.setBackgroundColor(ColorType.Khaki.colorInt);
                tvQuantity.setVisibility(View.VISIBLE);
            } else {
                container.setBackgroundColor(ColorType.Beige.colorInt);
                tvQuantity.setVisibility(View.GONE);
            }
            tvQuantity.setVisibility(record.revStock >= 1 ? View.VISIBLE : View.GONE);
        }
    }

    abstract public class CreationViewHolder extends BaseViewHolder {
        protected MyNumberPlusMinus numberPlusMinus;
        protected ItemOptionRecord record = new ItemOptionRecord();
        protected TextView tvName, tvOption, tvOptionCount, tvItemCount;
        private ImageView ivColor;
        private View divider;
        protected LinearLayout container;

        @Override
        public int getThisView() {
            return R.layout.row_creation;
        }

        @Override
        public void init(View view) {
            container = view.findViewById(R.id.row_qr_creation_container);
            tvName = findTextViewById(context, view, R.id.row_qr_creation_name);
            tvOption = findTextViewById(context, view, R.id.row_qr_creation_option);
            tvOptionCount = findTextViewById(context, view, R.id.row_qr_creation_option_count);
            tvItemCount = findTextViewById(context, view, R.id.row_qr_creation_item_count);
            ivColor = view.findViewById(R.id.row_qr_creation_color);
            ivColor.setImageDrawable(context.getResources().getDrawable(R.drawable.background_box));
            divider = view.findViewById(R.id.row_qr_creation_divider);
            numberPlusMinus = new MyNumberPlusMinus(view, 0);
            numberPlusMinus.setTextView(R.id.row_qr_creation_count);
            numberPlusMinus.enableTextViewOnClick(context);
            numberPlusMinus.linkViewToEdit(context, R.id.row_qr_creation_nameContainer); // 이름을 클릭해도 직접 숫자 입력이 나오도록
            numberPlusMinus.setPlusMinusButtons(R.id.row_qr_creation_plus, R.id.row_qr_creation_minus);
            numberPlusMinus.setOnClick(new MyOnClick<Integer>() {
                @Override
                public void onMyClick(View view, Integer newValue) {
                    optionCountMap.put(record, newValue);
                    if (onOptionCountUpdated != null)
                        onOptionCountUpdated.onMyClick(view, record);
                }
            });

        }

        protected ItemRecord getItemRecord(int itemId) {
            switch (mode) {
                default:
                    return itemDB.getRecordBy(itemId);
                case MemoryDB:
                    return itemDB.getRecordBy(itemId);
            }
        }

        protected int getItemCount(int itemId) {
            int result = 0;
            for (ItemOptionRecord optionRecord : optionCountMap.keySet()) {
                if (optionRecord.itemId == itemId)
                    result = result + optionCountMap.get(optionRecord);
            }
            return result;
        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0) return;
            this.position = position;
            record = records.get(position);
            numberPlusMinus.setIncrementalValue(incrementalOfCreationView);
            numberPlusMinus.setTextValue(optionCountMap.get(record));

            int prevItemId = position <= 0 ? 0 : records.get(position - 1).itemId;
            if (record.itemId != prevItemId) {
                if (doFilterItemRecords && selectedItemRecords.containsKey(record.itemId)) {
                    tvName.setText(selectedItemRecords.get(record.itemId).getUiName());
                } else {
                    ItemRecord itemRecord = getItemRecord(record.itemId);
                    tvName.setText(itemRecord.getUiName());
                }
                tvItemCount.setText(String.format("%d", getItemCount(record.itemId)));
                tvItemCount.setVisibility(View.VISIBLE);
            } else {
                tvName.setText("");
                tvItemCount.setVisibility(View.GONE);
            }

            ivColor.setBackgroundColor(record.color.colorInt);
            tvOption.setText(record.toStringWithDefault(" : "));

            if (position >= 1) {
                if (record.itemId != records.get(position - 1).itemId) {
                    divider.setVisibility(View.VISIBLE);
                } else {
                    divider.setVisibility(View.GONE);
                }
            } else {
                divider.setVisibility(View.GONE);
            }
        }

    }

}
