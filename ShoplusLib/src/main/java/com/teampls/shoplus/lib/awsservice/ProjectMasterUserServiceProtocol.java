package com.teampls.shoplus.lib.awsservice;

import com.teampls.shoplus.lib.datatypes.ShopOrganizationData;
import com.teampls.shoplus.lib.enums.UserLevelType;

/**
 * @author lucidite
 */

public interface ProjectMasterUserServiceProtocol {
    /**
     * 내 가게(=사장으로 등록된 가게) 목록 및 근무자 권한설정 정보를 받아온다.
     *
     * @param loginPhoneNumber 내 로그인 사용자 전화번호
     * @return
     * @exception
     */
    ShopOrganizationData getMyShopOrganization(String loginPhoneNumber) throws MyServiceFailureException;

    /**
     * 내 가게 근무자 권한설정을 추가하거나 업데이트한다.
     *
     * @param userPhoneNumber
     * @param shopPhoneNumber
     * @param memberPhoneNumber
     * @param userLevel
     * @param memberName
     * @throws MyServiceFailureException
     */
    void updateShopMember(String userPhoneNumber, String shopPhoneNumber, String memberPhoneNumber, UserLevelType userLevel, String memberName) throws MyServiceFailureException;

    /**
     * 내 가게 근무자 권한설정을 삭제한다.
     *
     * @param userPhoneNumber
     * @param shopPhoneNumber
     * @param memberPhoneNumber
     * @throws MyServiceFailureException
     */
    void deleteShopMember(String userPhoneNumber, String shopPhoneNumber, String memberPhoneNumber) throws MyServiceFailureException;
}
