package com.teampls.shoplus.lib.common;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_map.KeyValueRecord;
import com.teampls.shoplus.lib.database_memory.MAccountDB;
import com.teampls.shoplus.lib.database_memory.MSlipDB;
import com.teampls.shoplus.lib.database_memory.MTransDB;
import com.teampls.shoplus.lib.database_old.TempUnitPriceByBuyer;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.datatypes.ItemOptionData;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.datatypes.MyTriple;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.enums.AutoMessageRequestTimeConfigurationType;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.lib.enums.MemberPermission;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Medivh on 2017-05-21.
 */

public class BaseAwsService {

    public static void cancelSlip(final Context context, final SlipDataKey slipDataKey, final boolean doUpdateItemDB,
                                  final MyOnAsync onCancelSlip, final MyOnAsync onUpdateStock) {

        if (MemberPermission.canCancelTransaction(context, slipDataKey.createdDateTime) == false) {
            MemberPermission.CancelTransactionAfter1Day.toast(context);
            if (onCancelSlip != null)
                onCancelSlip.onSuccess(new SlipFullRecord());
            return;
        }

        new CommonServiceTask(context, "BaseAwsService.cancelSlip", "취소 중입니다") {
            SlipFullRecord copiedFullRecord = new SlipFullRecord();

            @Override
            public void doInBackground() throws MyServiceFailureException {
                copiedFullRecord = new SlipFullRecord();
                // cancel
                final SlipDataProtocol slipDataProtocol = transactionService.cancelTransaction(slipDataKey);

                final List<Integer> itemIds = new ArrayList<>();
                for (SlipItemDataProtocol slipItemDataProtocol : slipDataProtocol.getItems())
                    itemIds.add(slipItemDataProtocol.getItemId());

                adjustStockAsync(context, "BaseAwsService.cancelSlip", true, slipDataProtocol, BaseUtils.getMin(itemIds), doUpdateItemDB, onUpdateStock);
                // copy
                copiedFullRecord = SlipFullRecord.copy(slipDataProtocol);
            }

            @Override
            public void onPostExecutionUI() {
                if (onCancelSlip != null)
                    onCancelSlip.onSuccess(copiedFullRecord);
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                if (onCancelSlip != null)
                    onCancelSlip.onError(e);
                MyUI.toastSHORT(context, String.format("취소하지 못했습니다. 번호를 확인해주세요. 반복시는 새로고침을 해주세요"));
            }
        };
    }

    public static void uploadSlipAndStocks(final Context context, final String location, final SlipRecord slipRecord,
                                           final DateTime createdDateTime, final boolean doUpdateItemDB, final AutoMessageRequestTimeConfigurationType autoMessage,
                                           final MyOnAsync<SlipFullRecord> onPostSlip, final MyOnAsync onUpdateStock) {
        final UserRecord counterpart = UserDB.getInstance(context).getRecord(slipRecord.counterpartPhoneNumber);

        new CommonServiceTask(context, location, "거래 기록 생성중...") {
            private SlipFullRecord newFullRecord = new SlipFullRecord();

            @Override
            public void doInBackground() throws MyServiceFailureException {
                newFullRecord = new SlipFullRecord();
                List<SlipItemDataProtocol> items = new ArrayList<>();
                Set<Integer> itemIds = new HashSet<>();
                PosSlipDB posSlipDB = PosSlipDB.getInstance(context);
                Set<KeyValueRecord> priceDeltaRecords = new HashSet<>();

                for (SlipItemRecord record : posSlipDB.items.getRecordsOrderBySerial(slipRecord.getSlipKey().toSID())) {
                    if (record.itemId > 0) {
                        itemIds.add(record.itemId); // 0 이하는 재고에 반영이 안되므로 오류만 일으킨다

                        // 거래처별 가격 사용시
                        if (counterpart.priceType == ContactBuyerType.WHOLESALE && BasePreference.priceByBuyer.getValue(context)) {
                            KeyValueRecord priceDeltaRecord = TempUnitPriceByBuyer.getInstance(context).getRecordByKey(record.itemId);
                            if (BaseUtils.toInt(priceDeltaRecord.value) != 0) {
                                record.setPriceDelta(BaseUtils.toInt(priceDeltaRecord.value));
                                priceDeltaRecords.add(priceDeltaRecord);
                            }
                        }
                    }
                    items.add(record);
                }
                TempUnitPriceByBuyer.getInstance(context).deleteAll(new ArrayList(priceDeltaRecords));
                TempUnitPriceByBuyer.getInstance(context).clearDuplications();

                // upload to server : slip items, stocks, contact info
                MyTriple<AccountsData, SlipDataProtocol, String> triple = transactionService.postTransaction(
                        UserSettingData.getInstance(context).getUserShopInfo(), slipRecord.counterpartPhoneNumber,
                        createdDateTime, items, slipRecord.onlinePayment, slipRecord.entrustPayment, slipRecord.bill,
                        autoMessage, ""); // 생성 시간 = 현재 시간
                posSlipDB.deleteFromDBs(slipRecord.getSlipKey()); // 기존 장바구니용 slip은 삭제
                newFullRecord = new SlipFullRecord(triple.second); //
                newFullRecord.record.confirmed = true;
                newFullRecord.serviceLink = triple.third;
                posSlipDB.insert(newFullRecord, new ArrayList<String>(), new ArrayList<String>());
                MTransDB.getInstance(context).insert(newFullRecord);
                MSlipDB.getInstance(context).insertPendings(newFullRecord);

                adjustStockAsync(context, callLocation, false, triple.second, BaseUtils.getMin(itemIds), doUpdateItemDB, onUpdateStock);
                MAccountDB.getInstance(context).updateOrInsert(new AccountRecord(newFullRecord.record.counterpartPhoneNumber,
                        triple.first, counterpart.name));
            }

            @Override
            public void onPostExecutionUI() {
                if (onPostSlip != null)
                    onPostSlip.onSuccess(newFullRecord);
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                if (onPostSlip != null)
                    onPostSlip.onError(e);
                MyUI.toastSHORT(context, String.format("거래기록 생성 중 오류가 발생했습니다. 다시 시도를 부탁드립니다"));
            }
        };
    }

    protected static void adjustStockAsync(Context context, String location, final boolean doCancel, final SlipDataProtocol slipDataProtocol,
                                           final int itemIdMin, final boolean doUpdateItemDB, final MyOnAsync onUpdateStock) {
        new CommonServiceTask(context, location) {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                int firstItemId = itemIdMin;
                if (doUpdateItemDB == false)
                    firstItemId = -1;
                Map<ItemStockKey, ItemOptionData> stocks = itemService.adjustStock(
                        UserSettingData.getInstance(context).getUserShopInfo(), slipDataProtocol.getItems(), doCancel, firstItemId);
                if (doUpdateItemDB)  // 미송앱처럼 서버에는 알려야 하지만 로컬에는 반영할 필요가 없을 수 있다
                    ItemDB.getInstance(context).options.updateFast(stocks, null); // stocks ... 전체 stock을 모두 업데이트 하기 때문에 시간이 오래걸림
            }

            @Override
            public void onPostExecutionUI() {
                if (onUpdateStock != null)
                    onUpdateStock.onSuccess("");
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                if (onUpdateStock != null)
                    onUpdateStock.onError(e);
                MyUI.toastSHORT(context, String.format("[오류] 재고에 반영하지 못했습니다. 재고를 확인해주세요"));
            }
        };
    }

}
