package com.teampls.shoplus.lib.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Medivh on 2018-01-30.
 */

public class MyListMap <K, V> extends MyMap<K, List<V>> {

    public MyListMap() {
        super(new ArrayList<V>());
    }

    public List<V> getList(K key) {
        if (containsKey(key) == false)
            return new ArrayList<>();
        else
            return get(key);
    }

    public void add(K key, V value) {
        List<V> records = getList(key);
        records.add(value);
        put(key, records);
    }

    public void updateOrAdd(K key, V value) {
        List<V> records = getList(key);
        if (records.contains(value)) { // object 레벨에서만 비교
            int index = records.indexOf(value);
            records.add(index, value);
        } else {
            records.add(value);
        }
        put(key, new ArrayList(new HashSet(records)));
    }

}