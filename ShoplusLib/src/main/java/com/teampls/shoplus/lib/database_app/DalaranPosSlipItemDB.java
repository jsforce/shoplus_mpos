package com.teampls.shoplus.lib.database_app;

import android.content.Context;

import com.teampls.shoplus.lib.database.PosSlipItemDB;

/**
 * Created by Medivh on 2017-06-02.
 */

public class DalaranPosSlipItemDB extends PosSlipItemDB {
    private static DalaranPosSlipItemDB instance = null;

    public static DalaranPosSlipItemDB getInstance(Context context) {
        if (instance == null)
            instance = new DalaranPosSlipItemDB(context);
        return instance;
    }

    private DalaranPosSlipItemDB(Context context) {
        super(context);
    }

}
