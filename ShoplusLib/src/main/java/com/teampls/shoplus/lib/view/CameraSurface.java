package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;

import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.event.MyOnTask;

import java.io.IOException;
import java.util.List;

/**
 * Created by Medivh on 2017-02-11.
 */

public class CameraSurface extends SurfaceView implements SurfaceHolder.Callback, Camera.PictureCallback {
    private static CameraSurface instance = null;
    public boolean isPreviewRunning = false;
    private boolean isCameraOpened = false;
    protected Camera.Size cameraSize;
    protected int cameraOrientation = 0;
    protected byte[] shotImage = Empty.bytes;
    public SurfaceView surfaceView;
    public SurfaceHolder surfaceHolder;
    protected Camera camera;
    protected ImageView ivTaken;
    private MyOnTask<Bitmap> onPictureTaken;
    private static Context context;

    private CameraSurface(Context context) {
        super(context);
        this.context = context;
    }

    public static CameraSurface getInstance(Context context) {
        if (instance == null)
            instance = new CameraSurface(context);
        CameraSurface.context = context;
        return instance;
    }

    public void setSurface(SurfaceView surfaceView, ImageView pictureTaken, MyOnTask<Bitmap> onPictureTaken) {
        this.surfaceView = surfaceView;
        this.surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        this.ivTaken = pictureTaken;
        this.onPictureTaken = onPictureTaken;
        setPreview();
    }

    public void takePicture() {
        camera.takePicture(null, null, this);
    }

    @Override
    public void onPictureTaken(byte[] bytes, Camera camera) {
        stopPreview();
        shotImage = bytes;
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (cameraOrientation == 90) {
            if (cameraSize.height > 720) {
                options.inSampleSize = 4;
            } else {
                options.inSampleSize = 2;
            }
        } else {
            if (cameraSize.width > 720) {
                options.inSampleSize = 4;
            } else {
                options.inSampleSize = 2;
            }
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(shotImage, 0, shotImage.length, options);
        if (cameraOrientation == 90)
            bitmap = MyGraphics.rotateBitmap(bitmap, 90);
        ivTaken.setVisibility(View.VISIBLE);
        ivTaken.setImageBitmap(bitmap);
        if (onPictureTaken != null)
            onPictureTaken.onTaskDone(bitmap);
    }

    private void setPreview() {
        if (isCameraOpened == false)
            openCamera();
        if (isCameraOpened) {
            try {
                camera.setPreviewDisplay(surfaceHolder);
                // Log.i("DEBUG_JS", String.format("[CameraSurface.setPreview] ..."));
            } catch (IOException e) {
                isCameraOpened = false;
                camera.release();
                e.printStackTrace();
            }
        }
    }

    private boolean openCamera() {
        try {
            if (isCameraOpened == false) {
                camera = Camera.open();
                isCameraOpened = true;
            }
            return true;
        } catch (Exception e) {
            MyUI.toastSHORT(context, String.format("카메라 사용에 문제가 발견되었습니다. 스마트폰 재실행을 권장드립니다"));
            isCameraOpened = false;
            return false;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
//        setPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        stopPreview();
    }

    public void release() {
        if (camera != null) {
            isCameraOpened = false;
            camera.release();
        }
    }

    public void startPreview() {
        if (isCameraOpened == false)
            openCamera();

        if (isCameraOpened && (isPreviewRunning == false)) {
            ivTaken.setVisibility(View.GONE);

            Camera.Parameters parameters = camera.getParameters();
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
            cameraSize = previewSizes.get(0);
            parameters.setPreviewSize(cameraSize.width, cameraSize.height);
            camera.setParameters(parameters);
            if (cameraSize.width > cameraSize.height)
                cameraOrientation = 90;
            camera.setDisplayOrientation(cameraOrientation);
            camera.startPreview();
            isPreviewRunning = true;
            //  Log.i("DEBUG_JS", String.format("[ShotSearchFrag.startPreview] camera size (%dx%d) orientation %d ", cameraSize.width, cameraSize.height, cameraOrientation));
        }
    }

    public void stopPreview() {
        if (isCameraOpened && isPreviewRunning) {
            camera.stopPreview();
            isPreviewRunning = false;
        }
    }
}
