package com.teampls.shoplus.lib.viewSetting;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.MyGuideDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.printer.MyPosPrinter;
import com.teampls.shoplus.lib.view.ImagePickerView;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.MyMessengerSetting;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-04-15.
 */

public class PrinterSettingView extends BaseSettingView implements AdapterView.OnItemClickListener {
    private ListView lvDevices;
    private ArrayAdapter<CharSequence> adapter;
    private final ArrayList<CharSequence> deviceNames = new ArrayList<>();
    private TextView tvBluetoothState, tvImageFilePath;
    private List<BluetoothDevice> devices;
    private MyPosPrinter myPosPrinter;
    private MyMessengerSetting myMessengerSetting;
    private static MyOnTask onTask;

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, PrinterSettingView.class));
    }

    public static void startActivity(Context context, MyOnTask onTask) {
        PrinterSettingView.onTask = onTask;
        context.startActivity(new Intent(context, PrinterSettingView.class));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        setMyPosPrinter(globalDB.getValue(GlobalDB.KEY_PRINTER_Selected));
        myActionBar.setTitle("프린터 설정");
        adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_single_choice, deviceNames);
        lvDevices = (ListView) findViewById(R.id.printer_setting_listview);
        lvDevices.setAdapter(adapter);
        lvDevices.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvDevices.setOnItemClickListener(this);
        tvBluetoothState = findViewById(R.id.printer_setting_bluetooth_state);
        tvImageFilePath = findViewById(R.id.printer_setting_image_filepath);
        myMessengerSetting = new MyMessengerSetting(context, getView());

        setOnClick(R.id.printer_setting_close, R.id.printer_setting_bluetooth_openSetting,
                R.id.printer_setting_test, R.id.printer_setting_smallQR, R.id.printer_setting_normalQR,
               R.id.printer_setting_image_filepath);

        switch (GlobalDB.getInstance(context).getQRSize()) {
            case small:
                ((RadioButton) findViewById(R.id.printer_setting_smallQR)).setChecked(true);
                break;
            case large:
                ((RadioButton) findViewById(R.id.printer_setting_normalQR)).setChecked(true);
                break;
        }

        setCheckBox(R.id.printer_setting_serial, GlobalDB.KEY_PRINTER_SERIAL, false);
        setCheckBox(R.id.printer_setting_separator, GlobalDB.KEY_PRINTER_SEPARATOR, false);
        setCheckBox(R.id.printer_setting_receivable, GlobalDB.doShowReceivables);
        setCheckBox(R.id.printer_setting_memo, GlobalDB.KEY_PRINTER_MEMO, false);
        setCheckBox(R.id.printer_setting_image_checkbox, GlobalDB.KEY_PRINTER_QR_Image, false);

        MyView.setTextViewByDeviceSize(context, getView(), R.id.printer_setting_qrSize_title,
                R.id.printer_setting_display_title, R.id.printer_setting_image_title);

        // refresh는 onResume에서
    }

    @Override
    protected void onDestroy() {
        if (myPosPrinter != null)
            myPosPrinter.disconnectPrinter();
        if (onTask != null)
            onTask.onTaskDone("");
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUserSettingData(new MyOnTask() {
            @Override
            public void onTaskDone(Object result) {
                refresh();
            }
        });
    }

    private void refresh() {
        devices = MyDevice.getBluetoothDevices();
        deviceNames.clear();
        for (BluetoothDevice device : devices)
            deviceNames.add(String.format("%s", device.getName()));

        if (deviceNames.isEmpty())
            deviceNames.add("연결된 기기가 없습니다\n블루투스를 클릭해 연결해주세요");
        if (MyDevice.isBluetoothEnabled()) {
            tvBluetoothState.setText("On");
            tvBluetoothState.setTextColor(ColorType.Black.colorInt);
        } else {
            tvBluetoothState.setText("OFF");
            tvBluetoothState.setTextColor(ColorType.Red.colorInt);
        }

        myMessengerSetting.refresh();

        String imageFilePath = globalDB.getValue(GlobalDB.KEY_PRINTER_QR_Image_Path);
        if (new File(imageFilePath).exists() == false) {
            globalDB.put(GlobalDB.KEY_PRINTER_QR_Image, false);
            globalDB.put(GlobalDB.KEY_PRINTER_QR_Image_Path, "");
            imageFilePath = "";
        }

        if (imageFilePath.isEmpty()) {
            tvImageFilePath.setText("(미입력)");
            tvImageFilePath.setTextColor(ColorType.Gray.colorInt);
        } else {
            tvImageFilePath.setText(MyDevice.getFileNameOnly(imageFilePath, true));
            tvImageFilePath.setTextColor(ColorType.Black.colorInt);
        }

        if (TextUtils.isEmpty(myPosPrinter.getProductName()))
            globalDB.put(GlobalDB.KEY_PRINTER_Selected, "");
        String selectedPrinterName = globalDB.getValue(GlobalDB.KEY_PRINTER_Selected, "NotSelected");
        int position = 0;
        for (CharSequence deviceName : deviceNames) {
            lvDevices.setItemChecked(position, deviceName.equals(selectedPrinterName));
            position++;
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getThisView() {
        return R.layout.base_printer_setting;
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.help)
                .add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case help:
                new PrinterHelpDialog(context).show();
                break;
            case refresh:
                userSettingData.refresh(context, "PrinterSetting.refresh", new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        myMessengerSetting.setCheck(userSettingData.getActiveMessenger());
                        refresh();
                        MyUI.toastSHORT(context, String.format("최신 정보입니다"));
                    }
                });
                break;
            case close:
                finish();
                break;
        }

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.printer_setting_bluetooth_openSetting) {
            MyDevice.openBluetooth(context);

        } else if (view.getId() == R.id.printer_setting_close) {
            finish();

        } else if (view.getId() == R.id.printer_setting_test) {
            myPosPrinter.printTestLine("테스트 인쇄입니다\n\n");

        } else if (view.getId() == R.id.printer_setting_smallQR) {
            GlobalDB.getInstance(context).setQRSize(GlobalDB.QRSize.small);

        } else if (view.getId() == R.id.printer_setting_normalQR) {
            GlobalDB.getInstance(context).setQRSize(GlobalDB.QRSize.large);

        } else if (view.getId() == R.id.printer_setting_serial) {
            GlobalDB.getInstance(context).put(GlobalDB.KEY_PRINTER_SERIAL, ((CheckBox) view).isChecked());

        } else if (view.getId() == R.id.printer_setting_separator) {
            GlobalDB.getInstance(context).put(GlobalDB.KEY_PRINTER_SEPARATOR, ((CheckBox) view).isChecked());

        } else if (view.getId() == R.id.printer_setting_receivable) {
            GlobalDB.doShowReceivables.put(context, ((CheckBox) view).isChecked());

        } else if (view.getId() == R.id.printer_setting_image_filepath) {
            MyUI.toast(context, String.format("500x500 이하의 작은 흑백 이미지로 선택해주세요"));
            ImagePickerView.start(context, 1, true, new MyOnClick<List<String>>() {
                @Override
                public void onMyClick(View view, List<String> imagePaths) {
                    if (imagePaths.size() == 0) // 시작 옵션에 의해 발생하지는 않는다
                        return;
                    String filePath = imagePaths.get(0);
                    globalDB.put(GlobalDB.KEY_PRINTER_QR_Image_Path, filePath);
                    refresh();
                }
            });
        } else {

        }
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        setMyPosPrinter(devices.get(position).getName());
        myPosPrinter.setCurrentPrinter(devices.get(position));
        globalDB.put(GlobalDB.KEY_PRINTER_Selected, devices.get(position).getName());
    }

    private void setMyPosPrinter(String deviceName) {
        myPosPrinter = MyPosPrinter.getInstance(context);
    }

    class PrinterHelpDialog extends MyButtonsDialog {

        public PrinterHelpDialog(final Context context) {
            super(context, "프린터 설정 안내", "QR 이미지를 스크린샷으로 저장 후 출력 체크 옆을 클릭해서  [QR 이미지에서 읽어오기] 를 이용하시면 편리합니다.");

            addButton("내 카톡 QR 확인", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyDevice.openWeb(context, "https://goo.gl/vDsk4e");
                }
            });

            addButton("내 위챗 QR 확인", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MyGuideDialog(context, "내 QR 위치", "위챗을 실행한 후\n\n(하단 탭) 나 > 내 이름 클릭 > 내 QR 코드 > 스크란샷").show();
                }
            });

            addButton("프린터가 안될 때", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MyGuideDialog(context, "다음을 점검해주세요", "1. 배터리 방전?\n2. 뚜껑 열림?\n3. 전원선 중간 어댑터 고장?\n" +
                            "4. 종이 없음?\n5. 블루투스 혼선? (재시작 필요)\n\n블루투스 혼선이 의심시 블루투스에서 기기 해제 후 폰을 재시작. 뚜껑을 연 상태에서 블루투스 연결을 권장").show();
                }
            });


        }
    }
}
