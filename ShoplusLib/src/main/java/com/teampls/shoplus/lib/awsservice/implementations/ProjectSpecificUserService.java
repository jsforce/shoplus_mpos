package com.teampls.shoplus.lib.awsservice.implementations;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ProjectSpecificUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.UserSettingJSONData;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.UserSpecificSettingJSONData;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.protocol.UserSpecificSettingProtocol;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 사용자 체크 관련 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectSpecificUserService implements ProjectSpecificUserServiceProtocol {
    public String getMyUserSpecificSettingResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "user-setting", "user-specific");
    }

    public String getMyUserSettingResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "user-setting");
    }

    @Override
    public UserSettingDataProtocol getUserSetting(String loginUserPhoneNumber) throws MyServiceFailureException {
        try {
            String resource = this.getMyUserSettingResourcePath(loginUserPhoneNumber);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            return new UserSettingJSONData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public UserSettingDataProtocol updateUserSpecificSetting(String loginUserPhoneNumber, UserSpecificSettingProtocol setting) throws MyServiceFailureException {
        try {
            String resource = this.getMyUserSpecificSettingResourcePath(loginUserPhoneNumber);
            JSONObject requestBodyObj = new JSONObject()
                    .put("value", new UserSpecificSettingJSONData(setting).getObject());
            String response = APIGatewayClient.requestPUT(resource, requestBodyObj.toString(), UserHelper.getIdentityToken());
            return new UserSettingJSONData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }
}
