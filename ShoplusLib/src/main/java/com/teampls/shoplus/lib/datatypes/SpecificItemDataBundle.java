package com.teampls.shoplus.lib.datatypes;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.ItemJSONData;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 특정 아이템 데이터와 재고 데이터, 원가 데이터(사장 only)의 번들.
 * 서버로부터 수신한 데이터를 묶어서 반환하기 위한 용도로, Read-only 데이터이다.
 *
 * @author lucidite
 */

public class SpecificItemDataBundle {
    private ItemDataProtocol item;
    private Map<ItemStockKey, ItemOptionData> stock ;
    private int costPrice;

    public SpecificItemDataBundle() {}

    public SpecificItemDataBundle(JSONObject dataObj) throws JSONException {
        JSONObject itemObj = dataObj.getJSONObject("item");
        this.item = new ItemJSONData(itemObj);

        JSONArray stockArr = dataObj.getJSONArray("stock");
        this.stock = ItemOptionData.buildStockFromStockDataArr(stockArr);

        this.costPrice = dataObj.optInt("cost_price", 0);
    }

    public ItemDataProtocol getItem() {
        return item;
    }

    public Map<ItemStockKey, ItemOptionData> getStock() {
        return stock;
    }

    public int getCostPrice() {
        return costPrice;
    }

    public void toLogCat(String callLocation) {
        if (item == null) {
          Log.i("DEBUG_JS", String.format("[SpecificItemDataBundle.toLogCat] %s, item == NULL", callLocation));
        } else {
            new ItemRecord(item).toLogCat(callLocation);
        }
        for (ItemStockKey key : stock.keySet())
            new ItemOptionRecord(key, stock.get(key)).toLogCat(callLocation);
        Log.i("DEBUG_JS", String.format("[%s] costPrice %d",callLocation, costPrice));
    }
}
