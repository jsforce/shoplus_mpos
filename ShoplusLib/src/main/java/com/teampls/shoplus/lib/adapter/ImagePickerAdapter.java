package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.teampls.shoplus.R;
import com.yongchun.library.model.LocalMedia;
import com.yongchun.library.view.ImageSelectorActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


// package com.yongchun.library.adapter; 수정함

public class ImagePickerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TYPE_CAMERA = 1;
    public static final int TYPE_PICTURE = 2;
    private Context context;
    private boolean showCamera = true;
    private boolean enablePreview = true;
    private int maxSelectNum;
    private int selectMode = ImageSelectorActivity.MODE_MULTIPLE;
    private List<LocalMedia> images = new ArrayList<LocalMedia>();
    private List<LocalMedia> selectImages = new ArrayList<LocalMedia>();
    private boolean selectionOrderEnabled = false;

    private OnImageSelectChangedListener imageSelectChangedListener;

    public ImagePickerAdapter(Context context, int maxSelectNum, int mode, boolean showCamera, boolean enablePreview) {
        this.context = context;
        this.selectMode = mode;
        this.maxSelectNum = maxSelectNum;
        this.showCamera = showCamera;
        this.enablePreview = enablePreview;
    }

    public void enableOrder(boolean value) {
        selectionOrderEnabled = value;
    }

    public void bindImages(List<LocalMedia> images) {
        this.images = images;
        notifyDataSetChanged();
    }

    public void bindSelectImages(List<LocalMedia> images) {
        this.selectImages = images;
        notifyDataSetChanged();
        if (imageSelectChangedListener != null) {
            imageSelectChangedListener.onChange(selectImages);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (showCamera && position == 0) {
            return TYPE_CAMERA;
        } else {
            return TYPE_PICTURE;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_CAMERA) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_camera, parent, false);
            return new HeaderViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_picker, parent, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (getItemViewType(position) == TYPE_CAMERA) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            headerHolder.headerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imageSelectChangedListener != null) {
                        imageSelectChangedListener.onTakePhoto();
                    }
                }
            });
        } else {
            final ViewHolder contentHolder = (ViewHolder) holder;
            final LocalMedia image = images.get(showCamera ? position - 1 : position);

            Glide.with(context)
                    .load(new File(image.getPath()))
                    .centerCrop()
                    .thumbnail(0.5f)
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .dontAnimate()
                    .into(contentHolder.ivPicture);

            if (selectMode == ImageSelectorActivity.MODE_SINGLE) {
                contentHolder.ivCheck.setVisibility(View.GONE);
            }


            contentHolder.update(image, isSelected(image));
            //selectImage(contentHolder, isSelected(image));

            if (enablePreview) {
                contentHolder.ivCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        changeCheckboxState(contentHolder, image);
                    }
                });
            }

            contentHolder.contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((selectMode == ImageSelectorActivity.MODE_SINGLE || enablePreview) && imageSelectChangedListener != null) {
                        imageSelectChangedListener.onPictureClick(image, showCamera ? position - 1 : position);
                    } else {
                        changeCheckboxState(contentHolder, image);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return showCamera ? images.size() + 1 : images.size();
    }

    private void changeCheckboxState(ViewHolder contentHolder, LocalMedia record) {
        boolean isChecked = contentHolder.ivCheck.isSelected();
        if (selectImages.size() >= maxSelectNum && !isChecked) {
            Toast.makeText(context, String.format("'최대 : %d개", maxSelectNum), Toast.LENGTH_LONG).show();
            return;
        }

        if (isChecked) {
            for (LocalMedia media : selectImages) {
                if (media.getPath().equals(record.getPath())) {
                    selectImages.remove(media);
                    break;
                }
            }
        } else {
            selectImages.add(record);
            Log.i("DEBUG_JS", String.format("[ImagePickerAdapter.changeCheckboxState] added %s ", record.getPath()));
        }

        contentHolder.update(record, !isChecked);
        //selectImage(contentHolder, !isChecked);
        if (imageSelectChangedListener != null)
            imageSelectChangedListener.onChange(selectImages);

        if (isChecked && selectionOrderEnabled) // 중간에 삭제한 경우에는 전체 숫자를 다시
            notifyDataSetChanged();
    }

    public List<LocalMedia> getSelectedImages() {
        return selectImages;
    }

    public List<LocalMedia> getImages() {
        return images;
    }

    public boolean isSelected(LocalMedia image) {
        for (LocalMedia media : selectImages) {
            if (media.getPath().equals(image.getPath())) {
                return true;
            }
        }
        return false;
    }

    // onItemCheckedChangeListener 역할 --> update로 통합
    public void selectImage(ViewHolder holder, boolean isChecked) {
        holder.ivCheck.setSelected(isChecked);
        if (isChecked) {
            holder.ivPicture.setColorFilter(context.getResources().getColor(R.color.image_overlay2), PorterDuff.Mode.SRC_ATOP);
        } else {
            holder.ivPicture.setColorFilter(context.getResources().getColor(R.color.image_overlay), PorterDuff.Mode.SRC_ATOP);
        }
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        View headerView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            headerView = itemView;
        }
    }

   class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivPicture;
        private ImageView ivCheck;
        private TextView tvOrder;
        private View contentView;
        private int checkedColor = 0, unCheckedColor = 0;

        public ViewHolder(View view) {
            super(view);
            contentView = view;
            ivPicture =  view.findViewById(R.id.row_image_picker_picture);
            ivCheck = view.findViewById(R.id.row_image_picker_check);
            tvOrder = view.findViewById(R.id.row_image_picker_order);
            tvOrder.setVisibility(View.GONE);
            checkedColor = context.getResources().getColor(R.color.image_overlay2);
            unCheckedColor = context.getResources().getColor(R.color.image_overlay);
        }

        public void update(LocalMedia record, boolean isChecked) {
            ivCheck.setSelected(isChecked);
            ivPicture.setColorFilter(isChecked? checkedColor : unCheckedColor, PorterDuff.Mode.SRC_ATOP);

            if (selectionOrderEnabled) {
                ivCheck.setVisibility(View.GONE);
                tvOrder.setVisibility(isChecked? View.VISIBLE : View.GONE);
                tvOrder.setText(String.format("%d",selectImages.indexOf(record)+1));
            } else {
                ivCheck.setVisibility(View.VISIBLE);
                tvOrder.setVisibility(View.GONE);
            }
        }

    }

    public interface OnImageSelectChangedListener {
        void onChange(List<LocalMedia> selectImages);

        void onTakePhoto();

        void onPictureClick(LocalMedia media, int position);
    }

    public void setOnImageSelectChangedListener(OnImageSelectChangedListener imageSelectChangedListener) {
        this.imageSelectChangedListener = imageSelectChangedListener;
    }
}