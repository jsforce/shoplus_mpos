package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.protocol.SlipItemUpdateProtocol;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author lucidite
 */

public class SlipItemUpdateJSONData implements SlipItemUpdateProtocol, JSONDataWrapper {
    private JSONObject data;

    public SlipItemUpdateJSONData(JSONObject data) {
        this.data = data;
    }

    @Override
    public SlipDataKey getReferenceSlipKey() {
        return new SlipDataKey(this.getPhoneNumber(), this.getCreatedDatetime());
    }

    @Override
    public DateTime getUpdatedDatetime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.data.optString("updated"));
    }

    @Override
    public DateTime getCreatedDatetime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.data.optString("created"));
    }

    @Override
    public String getCounterpart() {
        return this.data.optString("counterpart", "");
    }

    @Override
    public SlipItemDataProtocol getSlipItem() {
        JSONObject slipItemObj = this.data.optJSONObject("slip_item");
        return new SlipItemJSONData(slipItemObj);
    }

    private String getPhoneNumber() {
        return this.data.optString("userid", "");
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return this.data;
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("SlipItemUpdateJSONData instance does not have a JSON array");
    }
}
