package com.teampls.shoplus.lib.database_app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.database.SlipItemDB;

/**
 * Created by Medivh on 2017-11-03.
 */

public class DalaranSlipItemDB extends SlipItemDB {

    private static DalaranSlipItemDB instance = null;

    public static DalaranSlipItemDB getInstance(Context context) {
        try {
            Context dalaranContext = context.createPackageContext(MyApp.Dalaran.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            if (instance == null)
                instance = new DalaranSlipItemDB(dalaranContext);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[DalaranItemDB.getInstance] not found"));
        }
        return instance;
    }

    private DalaranSlipItemDB(Context dalaranContext) {
        super(dalaranContext);
    }
}
