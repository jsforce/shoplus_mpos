package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.opencsv.CSVWriter;
import com.teampls.shoplus.lib.database.SlipFullRecord;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2018-03-12.
 */

public class MyFiles {

    public static void createOrAppend(String filePath, List<String> memo, boolean append) {
        try {
            FileWriter fileWriter = new FileWriter(filePath, append);
            for (String line : memo) {
                fileWriter.write(line + "\r\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void appendTextFile(String filePath, List<String> memo) {
        createOrAppend(filePath, memo, true);
    }

    public static void createTextFile(String filePath, List<String> memo) {
        createOrAppend(filePath, memo, false);
    }

    public static void createCsvFile(Context context, String filePath, List<String[]> allLines) {
        try {
            if (filePath.contains(".csv"))
                Log.w("DEBUG_JS", String.format("[MyDevice.createCsvFile] filePath : %s", filePath));
            createFileIfNotExist(new File(filePath));
            FileOutputStream stream = new FileOutputStream(filePath);
            CSVWriter writer = new CSVWriter(new OutputStreamWriter(stream, "EUC-KR"));
            writer.writeAll(allLines);
            writer.close();
        } catch (FileNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[MyDevice.createTextFile] %s not exist", filePath));
        } catch (IOException e) {
            Log.e("DEBUG_JS", String.format("[MyDevice.createTextFile] IO Exception: %s", e.toString()));
        }
        MyDevice.refresh(context, new File(filePath));
    }

    public static void createTextFile(Context context, String filePath, List<String> memo, Charset charSet) {
        try {
            createFileIfNotExist(new File(filePath));
            Writer writer = new OutputStreamWriter(new FileOutputStream(filePath), charSet);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(TextUtils.join("\r\n", memo));
            bufferedWriter.close();
        } catch (UnsupportedEncodingException e) {
            Log.e("DEBUG_JS", String.format("[MyDevice.createTextFile] %s invalid format", charSet.toString()));
        } catch (FileNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[MyDevice.createTextFile] %s not exist", filePath));
        } catch (IOException e) {
            Log.e("DEBUG_JS", String.format("[MyDevice.createTextFile] IO Exception: %s", e.toString()));
        }
        MyDevice.refresh(context, new File(filePath));
    }

    public static void createFileIfNotExist(File file) {
        if (file == null) {
            Log.e("DEBUG_JS", String.format("[MyDevice.createFileIfNotExist] file == null"));
            return;
        }
        try {
            if (file.exists() == false) {
                file.getParentFile().mkdirs();
                Thread.sleep(1);   // to avoid an error: open failed: EBUSY (Device or resource busy)
                file.createNewFile();
                Log.i("DEBUG_JS", String.format("[%s.createIfNotExist] %s is created", MyDevice.class.getSimpleName(), file.toString()));
            }
        } catch (IOException e) {
            Log.e("DEBUG_JS", String.format("Fail to create find %s", file.toString()), e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> read(String filePath) {
        createFileIfNotExist(new File(filePath));
        ArrayList<String> result = new ArrayList<String>(0);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            while (true) {
                String line = reader.readLine();
                if (line == null) break;
                if (result.contains(line) == false)
                    result.add(line);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}
