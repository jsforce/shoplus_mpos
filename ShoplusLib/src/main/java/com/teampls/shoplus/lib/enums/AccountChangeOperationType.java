package com.teampls.shoplus.lib.enums;

/**
 * 잔금/매입금 변경 타입
 *
 * @author lucidite
 */

public enum AccountChangeOperationType {
    Manual("m","직접 입력"),
    Transaction("t", "영수증발행"),
    TransactionCancellation("c","거래취소"),
    Clearance("l","미수건완료"),
    ClearanceRestoration("r","완료건취소"),
    ContactMerging("g","고객합치기"),
    Transfer("f","금액전환");


    private String remoteStr;
    public String uiStr;

    AccountChangeOperationType(String remoteStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
    }

    public static AccountChangeOperationType fromRemoteStr(String remoteStr) {
        for (AccountChangeOperationType type: AccountChangeOperationType.values()) {
            if (type.remoteStr.equals(remoteStr)) {
                return type;
            }
        }
        // throw new IllegalArgumentException("Illegal Account Change Operation Type=" + remoteStr);
        return Manual;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }

    public Boolean isRelatedToCashFlow() {
        switch (this) {
            case Manual:
            case Clearance:
            case ClearanceRestoration: return true;

            case Transaction:
            case TransactionCancellation:
            case ContactMerging:
            case Transfer:
            default: return false;
        }
    }
}
