package com.teampls.shoplus.lib.datatypes;

import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 서버에 아이템 검색을 요청할 때 전달하는 Key 데이터 클래스.
 * TODO 날짜 포맷 체크를 수행하고 있지 않음(서버에서 받은 데이터를 신뢰): 날짜 포맷 여부를 확인해야 하는가?
 *
 * @author lucidite
 */

public class ItemSearchKey {
    private int lastId;
    private String baseDatetimeStr;
    private DateTime dateTime = Empty.dateTime;

    private static DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyMMdd");

    /**
     * 초기 Search Key 생성용 (현재 동기화한 아이템 중 가장 오래된 것을 기준으로 하여 생성 가능)
     *
     * @param lastItem
     */
    public ItemSearchKey(ItemDataProtocol lastItem) {
        this.lastId = lastItem.getItemId();
        this.dateTime = lastItem.getCreatedDatetime();
        this.baseDatetimeStr = dateFormatter.print(lastItem.getCreatedDatetime());
    }

    public ItemSearchKey(ItemRecord record) {
        this.lastId = record.itemId;
        this.dateTime = record.getCreatedDatetime();
        this.baseDatetimeStr = dateFormatter.print(record.getCreatedDatetime());
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] itemId %d, dateTime %s", callLocation, lastId, getDate().toString(BaseUtils.fullFormat)));
    }

    public DateTime getDate() {
        if (Empty.isEmpty(dateTime))
            dateTime = BaseUtils.toDateTime(baseDatetimeStr, dateFormatter);
        return dateTime;
    }

    /**
     * 서버 응답에서 받은 새로운 Search Key를 생성하기 위한 용도
     *
     * @param obj
     * @throws JSONException
     */
    public ItemSearchKey(JSONObject obj) throws JSONException {
        this.lastId = obj.getInt("lastid");
        this.baseDatetimeStr = obj.getString("base");
    }

    public Map<String, String> getQueryParams() {
        Map<String, String> params = new HashMap<>();
        params.put("lastid", String.valueOf(lastId));
        params.put("base", baseDatetimeStr);
        return params;
    }
}
