package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

/**
 * Created by Medivh on 2018-05-12.
 */

public class CAccountLogDB extends MAccountLogDB { // counterpart only
    private static CAccountLogDB instance;

    public static CAccountLogDB getInstance(Context context) {
        if (instance == null)
            instance = new CAccountLogDB(context);
        return instance;
    }

    private CAccountLogDB(Context context) {
        super(context, "CAccountLogDB");
    }

}
