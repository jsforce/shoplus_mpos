package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;

/**
 * Created by Medivh on 2017-04-07.
 */

abstract public class UpdatePriceDialog extends BaseDialog {
    private MyCurrencyEditText etPrice, etRetailPrice;
    private CheckBox cbOneDigitBasicUnit;

    public UpdatePriceDialog(Context context, String title, String message, int price, int retailPrice, int basicUnit) {
        super(context);
        setDialogWidth(0.8, 0.6);
        findTextViewById(R.id.dialog_update_price_title, title);
        findTextViewById(R.id.dialog_update_price_message, message);
        findTextViewById(R.id.dialog_update_price_retail_title);
        etPrice = new MyCurrencyEditText(context, getView(), R.id.dialog_update_price_edittext, R.id.dialog_update_price_edittext_clear);
        etPrice.setTextViewAndBasicUnit(R.id.dialog_update_price_basicUnit, basicUnit);
        etPrice.setOriginalValue(price);
        etRetailPrice = new MyCurrencyEditText(context, getView(), R.id.dialog_update_price_retail_edittext, R.id.dialog_update_price_edittext_retail_clear);
        etRetailPrice.setTextViewAndBasicUnit(R.id.dialog_update_price_retail_basicUnit, basicUnit);
        etRetailPrice.setOriginalValue(retailPrice);
        cbOneDigitBasicUnit = findViewById(R.id.dialog_update_price_isOneDigitBasicUnit);
        cbOneDigitBasicUnit.setChecked(BasePreference.isOneDigitBasicUnit.getValue(context));
        setOnClick(R.id.dialog_update_price_cancel, R.id.dialog_update_price_apply,
                R.id.dialog_update_price_isOneDigitBasicUnit);
        setVisibility(message.isEmpty() ? View.GONE : View.VISIBLE, R.id.dialog_update_price_message);
        findViewById(R.id.dialog_update_price_retail_container).setVisibility(userSettingData.getProtocol().isRetailOn()? View.VISIBLE : View.GONE);
        MyDevice.showKeyboard(context, etPrice.getEditText());
        show();
    }


    @Override
    public int getThisView() {
        return R.layout.dialog_update_price;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_update_price_cancel) {
            onCancelClick();
        } else if (view.getId() == R.id.dialog_update_price_apply) {
            onApplyClick(etPrice.getValue(), etRetailPrice.getValue());
            MyDevice.hideKeyboard(context, etPrice.getEditText());
            dismiss();
        } else if (view.getId() == R.id.dialog_update_price_isOneDigitBasicUnit) {
            userSettingData.setBasicUnit(cbOneDigitBasicUnit.isChecked(), new MyOnTask<Integer>() {
                @Override
                public void onTaskDone(Integer digit) {
                    etPrice.setTextViewAndBasicUnit(R.id.dialog_update_price_basicUnit, digit);
                }
            });
        } else {

        }
    }

    protected void onCancelClick() {
        MyDevice.hideKeyboard(context, etPrice.getEditText());
        dismiss();
    }

    abstract public void onApplyClick(int price, int retailPrice);

}
