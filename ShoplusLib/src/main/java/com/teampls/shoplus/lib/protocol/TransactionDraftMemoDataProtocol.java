package com.teampls.shoplus.lib.protocol;

import org.joda.time.DateTime;

/**
 * Transaction Draft에 첨부되는 메모 데이터 클래스 프로토콜
 *
 * @author lucidite
 */

public interface TransactionDraftMemoDataProtocol {
    DateTime getCreatedDateTime();
    String getMessage();
}
