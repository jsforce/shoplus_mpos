package com.teampls.shoplus.lib.datatypes;

import android.util.Pair;

import com.teampls.shoplus.lib.database_memory.ShopMemberRecord;
import com.teampls.shoplus.lib.enums.UserLevelType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 내 샵 구성 데이터 클래스 (근무자 등의 정보를 포함한다)
 * NOTE. 사장에게 제공되는 데이터
 *
 * @author lucidite
 */

public class ShopOrganizationData {

    class ShopDetails {
        private String name;
        private Map<String, ShopMember> members;

        public ShopDetails(JSONObject dataObj) throws JSONException {
            this.name = dataObj.optString("name", "");
            this.members = new HashMap<>();
            JSONObject membersObj = dataObj.optJSONObject("members");
            if (membersObj != null) {
                Iterator<String> iter = membersObj.keys();
                while(iter.hasNext()) {
                    String memberPhoneNumber = iter.next();
                    JSONObject memberDataObj = membersObj.getJSONObject(memberPhoneNumber);
                    this.members.put(memberPhoneNumber, new ShopMember(memberDataObj));
                }
            }
        }

        public String getName() {
            return name;
        }

        public Map<String, ShopMember> getMembers() {
            return members;
        }
    }

    class ShopMember {
        private String name;
        private UserLevelType userLevel;

        public ShopMember(JSONObject dataObj) throws JSONException {
            this.name = dataObj.optString("name", "");
            this.userLevel = UserLevelType.fromRemoteStr(dataObj.optString("level", ""));
        }

        public String getName() {
            return name;
        }

        public UserLevelType getUserLevel() {
            return userLevel;
        }
    }

    private Map<String, ShopDetails> shops;

    public ShopOrganizationData(JSONObject dataObj) throws JSONException {
        this.shops = new HashMap<>();
        JSONObject shopsObj = dataObj.optJSONObject("shops");
        if (shopsObj != null) {
            Iterator<String> iter = shopsObj.keys();
            while(iter.hasNext()) {
                String shopPhoneNumber = iter.next();
                JSONObject shopDataObj = shopsObj.getJSONObject(shopPhoneNumber);
                this.shops.put(shopPhoneNumber, new ShopDetails(shopDataObj));
            }
        }
    }

    public Map<String, ShopDetails> getShops() {
        return shops;
    }


    public List<ShopMemberRecord> getShopMembers(String shopPhoneNumber) {
        List<ShopMemberRecord> results = new ArrayList<>();
        ShopDetails shopDetails = shops.get(shopPhoneNumber);
        Map<String, ShopMember> members = shopDetails.getMembers();
        for (String memberPhoneNumber : members.keySet()) {
            results.add(new ShopMemberRecord(shopPhoneNumber, memberPhoneNumber, members.get(memberPhoneNumber).userLevel,
                    members.get(memberPhoneNumber).name));
        }
        return results;
    }

    public List<Pair<String, String>> getShopPhoneAndNames() {
        List<Pair<String, String>> results = new ArrayList<>();
        for (String phoneNumber : shops.keySet()) {
            results.add(new Pair(phoneNumber, shops.get(phoneNumber).name));
        }
        return results;
    }

}
