package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;

import com.teampls.shoplus.R;

/**
 * Created by Medivh on 2017-02-14.
 */

public class MyGuideDialog extends BaseDialog {

    public MyGuideDialog(Context context, String title, String message) {
        super(context);
        setDialogWidth(0.9, 0.6);
        findTextViewById(R.id.dialog_user_guide_title, title);
        findTextViewById(R.id.dialog_user_guide_message, message);
        setOnClick(R.id.dialog_user_guide_apply);
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_user_guide;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_user_guide_apply) {
            dismiss();
        }
    }

}
