package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.datatypes.ItemOptionData;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.DBResultType;
import com.teampls.shoplus.lib.enums.ItemSortingKey;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.undercity.enums.ItemStockUpdateType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ItemOptionDB extends BaseDB<ItemOptionRecord> {

    private static ItemOptionDB instance = null;
    protected static int Ver = 1;

    public enum Column {
        _id, key, itemId, color, size, sizeOrdinal, confirmed, stock, revStock, isSoldOut;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    protected ItemOptionDB(Context context) {
        super(context, "ItemOptionDB5", Ver, Column.toStrs()); // 여기 뒤의 숫자로 버전 관리를 한다
    }

    public static ItemOptionDB getInstance(Context context) {
        if (instance == null)
            instance = new ItemOptionDB(context);
        return instance;
    }

    protected ContentValues toContentvalues(ItemOptionRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.key.toString(), record.key);
        contentvalues.put(Column.itemId.toString(), record.itemId);
        contentvalues.put(Column.color.toString(), record.color.toString());
        contentvalues.put(Column.size.toString(), record.size.toString());
        contentvalues.put(Column.sizeOrdinal.toString(), record.size.ordinal());
        contentvalues.put(Column.confirmed.toString(), record.confirmed);
        contentvalues.put(Column.stock.toString(), record.stock);
        contentvalues.put(Column.revStock.toString(), record.revStock);
        contentvalues.put(Column.isSoldOut.toString(), record.isSoldOut);
        return contentvalues;
    }

    @Override
    protected void toLogCat(int id, ItemOptionRecord record, int option, String operation) {
        Log.i("DEBUG_JS", String.format("[%s] %d, %s, %s", getClass().getSimpleName(),
                record.itemId, record.color.toString(), record.size.toString()));
    }

    private boolean has(ItemOptionRecord targetRecord, List<ItemOptionRecord> records) {
        if (records == null)
            records = getRecordsBy(targetRecord.itemId);

        for (ItemOptionRecord record : records) {
            if ((targetRecord.color == record.color) && (targetRecord.size == record.size)) {
                return true;
            }
        }
        return false;
    }

    public ItemOptionRecord getRecordBy(String key) {
        return getRecord(Column.key, key);
    }

    public void deleteInvalids(List<String> validKeys) {
        List<String> invalidKeys = getStrings(Column.key);
        invalidKeys.removeAll(validKeys);
        deleteAll(Column.key, invalidKeys);
    }

    public void deleteInvalids(int itemId, List<String> validKeys) {
        List<String> invalidKeys = getStrings(Column.key, Column.itemId, itemId);
        invalidKeys.removeAll(validKeys);
        deleteAll(Column.key, invalidKeys);
    }

    public void refresh(Map<ItemStockKey, ItemOptionData> stockBundle) {
        refresh(stockBundle, null);
    }

    public static Map<ItemStockKey, ItemOptionData> toNewMap(Map<ItemStockKey, Integer> oldMap) {
        Map<ItemStockKey, ItemOptionData> results = new MyMap<>(new ItemOptionData());
        for (ItemStockKey key : oldMap.keySet())
            results.put(key, new ItemOptionData(oldMap.get(key), false));
        return results;
    }

    public void updateOptionStocks(ItemStockUpdateType stockUpdateType, Map<ItemStockKey, ItemOptionData> stockKeyMap) {
        for (ItemStockKey key : stockKeyMap.keySet()) {
            int currentStock = getRecord(key).stock;
            int updateStock = stockKeyMap.get(key).getStock();
            boolean isSoldout = stockKeyMap.get(key).isSoldOut();
            int stock = 0;
            switch (stockUpdateType) {
                case ORDER:
                    stock = currentStock; // 그대로
                    break;
                case ARRIVAL:
                    stock = currentStock + updateStock; // 증가
                    break;
                case BATCH:
                    stock = updateStock; // 강제 변경
                    break;
                case SALES:
                    stock = currentStock - updateStock; // 감소
                    break;
            }
            ItemOptionRecord record = new ItemOptionRecord(key, new ItemOptionData(stock, isSoldout));
            updateOrInsert(record);
        }
    }

    public void refresh(Map<ItemStockKey, ItemOptionData> stockBundle, MyOnTask onTask) {
       // beginTransaction();
        final List<String> validKeys = new ArrayList<>();
        for (ItemStockKey key : stockBundle.keySet()) {
            ItemOptionRecord record = new ItemOptionRecord(key, stockBundle.get(key));
            validKeys.add(record.key);
            updateOrInsert(record);
            if (onTask != null)
                onTask.onTaskDone("");
        }
      //  endTransaction();
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                deleteInvalids(validKeys);
            }
        };
    }

    public void refreshFast(Map<ItemStockKey, ItemOptionData> stockBundle, boolean doDeleteInvalids, MyOnTask onTask) {
        final List<String> validKeys = new ArrayList<>();
        MyListMap<Integer, ItemOptionRecord> itemOptionMap = new MyListMap<>();
        for (ItemStockKey key : stockBundle.keySet()) {
            ItemOptionRecord record = new ItemOptionRecord(key, stockBundle.get(key));
            itemOptionMap.add(record.itemId, record);
            validKeys.add(record.key);
        }

        for (Integer itemId : itemOptionMap.keySet()) {
            updateOrInsertFast(itemId, itemOptionMap.get(itemId));
            if (onTask != null)
                onTask.onTaskDone("");
        }

        if (doDeleteInvalids)
            deleteInvalids(validKeys);
    }

    public void updateOrInsertFast(int itemId, List<ItemOptionRecord> records) {
        // 동일 itemId에 대해서만 검사
        // 그룹 vs 그룹 업데이트 방식 : records vs dbRecords

        List<ItemOptionRecord> dbRecords = getRecordsBy(itemId);
        for (ItemOptionRecord record : records) {
            // 기존 db에 있는지 검사
            boolean found = false;
            for (ItemOptionRecord dbRecord : dbRecords) {
                // has() 대신
                if ((record.color == dbRecord.color) && (record.size == dbRecord.size)) { // itemId는 이미 동일하므로
                    found = true;
                    if (dbRecord.isSame(record) == false) {
                        update(dbRecord.id, record);
                    } else {
                        //
                    }
                    break;  // 새로 추가함 --- itemId, color, size가 동일한게 더 있을 수 없으므로
                }
            }
            if (found == false) {
                insert(record);
            }
        }
    }

    public DBResult updateOrInsert(ItemOptionRecord record) {
        if (hasValue(Column.key, record.key)) {// item, color, size
            ItemOptionRecord dbRecord = getRecordBy(record.key);
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public void update(Map<ItemStockKey, ItemOptionData> stockBundle) {
        for (ItemStockKey key : stockBundle.keySet()) {
            ItemOptionRecord record = new ItemOptionRecord(key, stockBundle.get(key));
            updateOrInsert(record);
        }
    }

    public List<ItemOptionRecord> recoverDeleted(MyListMap<Integer, ItemOptionRecord> options) {
        List<ItemOptionRecord> results = new ArrayList<>();
        for (int itemId : options.keySet()) {
            List<ItemOptionRecord> myOptions = getRecordsBy(itemId);
            for (ItemOptionRecord option : options.get(itemId)) {
                if (myOptions.contains(option) == false) {
                    Log.w("DEBUG_JS", String.format("[ItemOptionDB.recoverDeleted] Not contains %s", option.toString(",")));
                    insert(option); // 삭제된 것이라는 표시가 필요한데 우선은 그냥 가자
                    results.add(option);
                }
            }
        }
        return results;
    }

    // 재고가 있는 것만 가져옴
    public void updateByPositiveStocks(Map<ItemStockKey, ItemOptionData> stockBundle) {
        for (ItemOptionRecord record : getRecords()) {
            if (stockBundle.keySet().contains(record.toStockKey())) {
                record.stock = stockBundle.get(record.toStockKey()).getStock(); // 있는 값은 그대로
            } else {
                record.stock = Math.min(0, record.stock); // 없는건 0 또는 기존 음수
            }
            update(record.id, record);
        }
    }

    public void updateFast(Map<ItemStockKey, ItemOptionData> stockBundle,MyOnTask onTask) {
        MyListMap<Integer, ItemOptionRecord> itemOptionMap = new MyListMap<>();
        for (ItemStockKey key : stockBundle.keySet()) {
            ItemOptionRecord record = new ItemOptionRecord(key, stockBundle.get(key));
            List<ItemOptionRecord> options = itemOptionMap.getList(record.itemId);
            options.add(record);
            itemOptionMap.put(record.itemId, options);
        }

        for (Integer itemId : itemOptionMap.keySet()) {
            updateOrInsertFast(itemId, itemOptionMap.getList(itemId));
            if (onTask != null)
                onTask.onTaskDone("");
        }
    }

    public boolean refresh(int itemId, ArrayList<ItemOptionRecord> newRecords) {
       List<ItemOptionRecord> oldRecords = getRecordsBy(itemId);

        if (newRecords.size() == 0)
            newRecords.add(new ItemOptionRecord(itemId, ColorType.Default, SizeType.Default));

        for (ItemOptionRecord record : newRecords) {
            if (has(record, oldRecords) == false)
                insert(record);
        }

        for (ItemOptionRecord oldRecord : oldRecords) {
            if (has(oldRecord, newRecords) == false)
                delete(oldRecord.id);
        }
        return true;
    }

//    public void updateOrInsert(int itemId, ArrayList<ItemOptionRecord> records) {
//        if (records.size() == 0)
//            records.addInteger(new ItemOptionRecord(itemId, ColorType.Default, SizeType.Default));
//        ArrayList<ItemOptionRecord> dbRecords = getRecordsBy(itemId);
//
//        for (ItemOptionRecord record : records) {
//            if (dbRecords.contains(record)) {
//
//            } else {
//                insert(record);
//            }
//
//            if (has(record, dbRecords)) {
//                // dbRecord.isSame(record) == true (always)
//            } else {
//                insert(record);
//            }
//        }
//
//        for (ItemOptionRecord dbRecord : dbRecords) {
//            if (has(dbRecord, records) == false)
//                delete(dbRecord.id);
//        }
//    }

    public void deleteBy(int itemId) {
        List<Integer> itemIds = new ArrayList<>();
        itemIds.add(itemId);
        deleteAll(Column.itemId, itemIds);
    }

    @Override
    protected int getId(ItemOptionRecord record) {
        return record.id;
    }

    @Override
    protected ItemOptionRecord getEmptyRecord() {
        return new ItemOptionRecord();
    }

    @Override
    protected ItemOptionRecord createRecord(Cursor cursor) {
        return new ItemOptionRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.key.ordinal()),
                cursor.getInt(Column.itemId.ordinal()),
                ColorType.valueOfDefault(cursor.getString(Column.color.ordinal())),
                SizeType.valueOfDefault(cursor.getString(Column.size.ordinal())),
                BaseUtils.toBoolean(cursor.getInt(Column.confirmed.ordinal())),
                cursor.getInt(Column.stock.ordinal()),
                cursor.getInt(Column.revStock.ordinal()),
                BaseUtils.toBoolean(cursor.getInt(Column.isSoldOut.ordinal()))
        );
    }

    public List<ItemOptionRecord> getRecordsBy(int itemId) {
        return getRecords(Column.itemId, itemId);
    }

    public void toLogCat(String location) {
        boolean isEmpty = true;
        for (ItemOptionRecord record : getRecords()) {
            record.toLogCat(location);
            isEmpty = false;
        }
        if (isEmpty)
            Log.i("DEBUG_JS", String.format("[%s] %s is empty", location, DB_NAME));
    }

    public List<ItemOptionRecord> getOnRevisionRecords(List<Integer> itemIds) {
        List<ItemOptionRecord> results = new ArrayList<>(0);
        for (Integer itemId : itemIds)
            results.addAll(getOnRevisionRecords(itemId));
        return results;
    }

    public List<ItemOptionRecord> getOnRevisionRecords(int itemId) {
        List<ItemOptionRecord> results = new ArrayList<>(0);
        for (ItemOptionRecord record : getRecordsBy(itemId))
            if (record.isOnRevision())
                results.add(record);
        return results;
    }

    public List<ItemOptionRecord> getOnRevisionRecords() {
        List<ItemOptionRecord> results = new ArrayList<>(0);
        for (ItemOptionRecord record : getRecords())
            if (record.isOnRevision())
                results.add(record);
        return results;
    }

    public Map<ItemStockKey, Integer> getOnRevisionBundle(int itemId) {
        Map<ItemStockKey, Integer> results = new HashMap<>(0);
        for (ItemOptionRecord record : getOnRevisionRecords(itemId))
            results.put(record.toStockKey(), record.revStock);
        return results;
    }

    public Map<ItemStockKey, Integer> getOnRevisionBundle(List<Integer> itemIds) {
        Map<ItemStockKey, Integer> results = new HashMap<>(0);
        for (Integer itemId : itemIds)
            results.putAll(getOnRevisionBundle(itemId));
        return results;
    }

    public void confirmRevisions(List<Integer> itemIds) {
        for (Integer itemId : itemIds)
            confirmRevision(itemId);
    }

    public void confirmRevision(int itemId) {
        for (ItemOptionRecord record : getOnRevisionRecords(itemId)) {
            record.confirmed = true;
            record.stock = record.revStock;
            update(record.id, record);
        }
    }

    public void confirmRevisions() {
        for (ItemOptionRecord record : getOnRevisionRecords()) {
            record.confirmed = true;
            record.stock = record.revStock;
            update(record.id, record);
        }
    }

    public boolean hasOnRevision() {
        for (ItemOptionRecord record : getRecords())
            if (record.isOnRevision()) return true;
        return false;
    }

    public boolean hasOnRevision(int itemId) {
        for (ItemOptionRecord record : getRecordsBy(itemId))
            if (record.isOnRevision()) return true;
        return false;
    }

    public boolean hasOnRevision(List<Integer> itemIds) {
        for (int itemId : itemIds) {
            if (hasOnRevision(itemId))
                return true;
        }
        return false;
    }


    public void clearOnRevisions() {
        for (ItemOptionRecord record : getOnRevisionRecords()) {
            if (record.confirmed == false) {
                delete(record.id);
            } else if (record.stock != record.revStock) {
                record.revStock = record.stock;
                update(record.id, record);
            }
        }
    }

    public void clearOnRevisions(int itemId) {
        for (ItemOptionRecord record : getOnRevisionRecords(itemId)) {
            if (record.confirmed == false) {
                delete(record.id);
            } else if (record.stock != record.revStock) {
                record.revStock = record.stock;
                update(record.id, record);
            }
        }
    }

    public void clearOnRevisions(List<Integer> itemIds) {
        for (int itemId : itemIds)
            clearOnRevisions(itemId);
    }

    public int getPositiveStockSum(int itemId) {
        int result = 0;
        for (ItemOptionRecord record : getRecordsBy(itemId))
            result = result + record.getPositiveStock();
        return result;
    }

    public int getPositiveStockSums(List<Integer> itemIds) {
        int result = 0;
        for (ItemOptionRecord record : getRecordsIN(ItemDB.Column.itemId, itemIds, null))
            result = result +  record.getPositiveStock();
        return result;
    }

    public int getPositiveStockSums() {
        int result = 0;
        for (ItemOptionRecord record : getRecords())
            result = result +  record.getPositiveStock();
        return result;
    }

    public MyMap<Integer, Integer> getStockMapByItem() {
        MyMap<Integer, Integer> results = new MyMap<>(0);
        for (ItemOptionRecord optionRecord : getRecords())
            results.addInteger(optionRecord.itemId, optionRecord.getPositiveStock()); // item 별로 볼때는 positive만
        return results;
    }

    public MyMap<ItemOptionRecord, Integer> getStockMap() {
        MyMap<ItemOptionRecord, Integer> results = new MyMap<>(0);
        for (ItemOptionRecord optionRecord : getRecords())
            results.put(optionRecord, optionRecord.stock);
        return results;
    }

    public List<ItemOptionRecord> getSortedRecords(int itemId) {
//        List<QueryOrderRecord> queryOrderRecords = new ArrayList<>();
//        queryOrderRecords.add(new QueryOrderRecord(Column.itemId, true, true));
//        queryOrderRecords.add(new QueryOrderRecord(Column.color, false, false));
//        queryOrderRecords.add(new QueryOrderRecord(Column.sizeOrdinal, true, false));
        //return getRecordsINInts(Column.itemId, new ArrayList<Integer>(itemId), queryOrderRecords);
        List<QueryAndRecord> queryAndRecords = new ArrayList<>();
        queryAndRecords.add(new QueryAndRecord(Column.itemId, itemId));
        List<ItemOptionRecord> records = getRecordsBy(itemId);
        Collections.sort(records);
        return records; // getRecords(queryAndRecords, queryOrderRecords);
    }

    public ItemOptionRecord getRecord(ItemStockKey itemStockKey) {
        List<QueryAndRecord> queryAndRecords = new ArrayList<>();
        queryAndRecords.add(new QueryAndRecord(Column.itemId, itemStockKey.getItemId()));
        queryAndRecords.add(new QueryAndRecord(Column.color, itemStockKey.getColorOption().toString()));
        queryAndRecords.add(new QueryAndRecord(Column.size, itemStockKey.getSizeOption().toString()));
        List<ItemOptionRecord> results = getRecords(queryAndRecords);
        if (results.size() == 1) {
            return results.get(0);
        } else {
            if (results.size() >= 2) {
                Log.w("DEBUG_JS", String.format("[ItemOptionDB.getRecordByKey] duplicated %d, %s, %s : %s", itemStockKey.getItemId(), itemStockKey.getColorOption().toString(),
                        itemStockKey.getSizeOption().toString(), TextUtils.join(",", results)));
                return results.get(0);
            } else {
                return new ItemOptionRecord();
            }
        }
    }

    public Map<ColorType, List<Pair<SizeType, Integer>>> toColorMap(List<ItemOptionRecord> optionRecords) {
        Map<ColorType, List<Pair<SizeType, Integer>>> results = new HashMap<>();
        for (ItemOptionRecord optionRecord : optionRecords) {
            if (results.containsKey(optionRecord.color) == false) {
                // new
                Pair<SizeType, Integer> pair = Pair.create(optionRecord.size, optionRecord.stock);
                List<Pair<SizeType, Integer>> list = new ArrayList<>();
                list.add(pair);
                results.put(optionRecord.color, list);
            } else {
                // exist...
                List<Pair<SizeType, Integer>> sizeTypes = results.get(optionRecord.color);
                sizeTypes.add(Pair.create(optionRecord.size, optionRecord.stock));
                results.put(optionRecord.color, sizeTypes);
            }
        }
        return results;
    }

}