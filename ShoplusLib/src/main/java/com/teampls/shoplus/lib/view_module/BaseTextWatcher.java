package com.teampls.shoplus.lib.view_module;

import android.text.Editable;
import android.text.TextWatcher;

import com.teampls.shoplus.lib.database_global.UserRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-10-09.
 */
abstract public class BaseTextWatcher implements TextWatcher {
    public boolean enabled = true;

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    protected List<UserRecord> filterRecords(Editable s, List<UserRecord> fullRecords) {
        List<UserRecord> results = new ArrayList<>();
        String keyword = s.toString().trim().toLowerCase();
        for (UserRecord record : fullRecords) {
            if (record.name.replace("*", "").toLowerCase().contains(keyword) || record.phoneNumber.contains(keyword))
                results.add(record);
        }
        return results;
    }

}
