package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyImageLoader;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.event.MyOnClick;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-03-27.
 */

// ViewHolder에 해당, 주의! 생성용이므로 현재 보여지는 view와 다르다, 기존 view의 모습은 ViewHolder로 이관된다
abstract public class BaseFullViewAdapter<T> extends PagerAdapter {
    protected Context context;
    protected LayoutInflater inflater;
    private View view;
    protected MyImageLoader imageLoader;
    protected MyOnClick onCancelClick, onFunctionClick;
    protected MyDB myDB;
    protected UserDB userDB;
    protected GlobalDB globalDB;
    protected KeyValueDB keyValueDB;
    protected List<BaseViewHolder> fullViewHolders = new ArrayList<>(); // record-viewHolder 쌍에 의해 표시됨
    protected List<T> records;
    protected UserSettingData userSettingData;

    public BaseFullViewAdapter(Context context, List<T> records) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        myDB = MyDB.getInstance(context);
        userDB = UserDB.getInstance(context);
        globalDB = GlobalDB.getInstance(context);
        keyValueDB = KeyValueDB.getInstance(context);
        imageLoader = MyImageLoader.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
        this.records = records;
    }

    @Override
    public int getCount() {
        return records.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE; //  .notifyDataSetChanged() 했을때 새로 만들게
    }

    public T getRecord(int position) {
        if (position >= records.size() || position < 0) {
            Log.e("DEBUG_JS", String.format("[BaseFullViewAdapter.getRecordByKey] invalid position %d (size %d)", position, records.size()));
            return getEmptyRecord();
        }
        return records.get(position);
    }

    // 메모리 타입에서 수정이 일어난 경우를 위해 추가
    public void setRecord(int position, T record) {
        if (position >= records.size() || position < 0) {
            Log.e("DEBUG_JS", String.format("[BaseFullViewAdapter.getRecordByKey] invalid position %d (size %d)", position, records.size()));
            return;
        }
        records.set(position, record);
    }

    public abstract T getEmptyRecord();

    public List<T> getRecords() {
        return records;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        view = inflater.inflate(getThisView(), null);
        BaseViewHolder viewHolder = getFullViewHolder();
        viewHolder.init(view);
        viewHolder.update(position);
        viewHolder.refresh();
        viewHolder.refreshAdapter();
        fullViewHolders.add(viewHolder);
        container.addView(view);
        return view;
    }

    public boolean hasFullView(int position) {
        for (BaseViewHolder viewHolder : fullViewHolders) {
            if (viewHolder.position == position)
                return true;
        }
        return false;
    }

    public BaseViewHolder getFullView(int position) {
        for (BaseViewHolder viewHolder : fullViewHolders) {
            if (viewHolder.position == position)
                return viewHolder;
        }
        Log.e("DEBUG_JS", String.format("[BaseFullViewAdapter.getFullView] fail to find %d", position));
        return Empty.getViewHolder();
    }



    public List<BaseViewHolder> getFullViews() {
        return fullViewHolders;
    }

    protected abstract BaseViewHolder getFullViewHolder();

    protected abstract int getThisView();

    public void setOnCancelClick(MyOnClick onCancelClick) {
        this.onCancelClick = onCancelClick;
    }

    public void setOnFunctionClick(MyOnClick onFunctionClick) {
        this.onFunctionClick = onFunctionClick;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        for (BaseViewHolder viewHolder : fullViewHolders)
            if (viewHolder.position == position) {
                fullViewHolders.remove(viewHolder);
                break;
            }
    }

    public TextView setText(int viewId, String text) {
        TextView textView = ((TextView) view.findViewById(viewId));
        textView.setText(text);
        return textView;
    }

    public <T extends View> T _findViewById(int viewId) {
        return (T) view.findViewById(viewId);
    }
}
