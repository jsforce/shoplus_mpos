package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.CheckableStringAdapter;
import com.teampls.shoplus.lib.enums.ItemSortingKey;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-12-16.
 */

abstract public class SortingOrderDialog extends BaseDialog implements AdapterView.OnItemClickListener {
    private ListView listView;
    private CheckableStringAdapter adapter;
    private List<String> uiStrRecords = new ArrayList<>();

    public SortingOrderDialog(Context context, String title, ItemSortingKey sortingKey, List<ItemSortingKey> keys) {
        super(context);
        int selectedIndex = 0, index = 0;

        for (ItemSortingKey key : keys) {
            uiStrRecords.add(key.uiStr);
            if (key == sortingKey)
                selectedIndex = index;
            index++;
        }
        adapter = new CheckableStringAdapter(context);
        adapter.setRecords(uiStrRecords);
        adapter.setClickedPosition(selectedIndex);

        listView = (ListView) findViewById(R.id.dialog_listview);
        listView.setOnItemClickListener(this);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setAdapter(adapter);

        findTextViewById(R.id.dialog_list_title, title);
        setOnClick(R.id.dialog_list_close);
        findViewById(R.id.dialog_list_message).setVisibility(View.GONE);
        refresh();
        show();
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_list;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_list_close) {
            dismiss();
        }
    }

    abstract public void onItemClicked(ItemSortingKey sortingKey);

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        ItemSortingKey sortingKey = ItemSortingKey.valueOfUiStr((String) adapter.getItem(position));
        onItemClicked(sortingKey);
        dismiss();
    }
}
