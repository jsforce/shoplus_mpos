package com.teampls.shoplus.lib.protocol;

/**
 * 대금지불방식 데이터 프로토콜
 *
 * @author lucidite
 */

public interface PaymentTermsProtocol {
    /**
     * 온라인 입금 금액을 반환한다. 완료 여부와 상관없이 금액 값을 반환한다.
     * @return
     */
    int getOnlinePayment();

    /**
     * 대납 결제 금액을 반환한다. 완료 여부와 상관없이 금액 값을 반환한다.
     * @return
     */
    int getEntrustedPayment();

    /**
     * 온라인 입금 완료 상태를 반환한다.
     * @return 완료 상태인 경우 true, 미완 상태인 경우 false
     */
    boolean isOnlinePaymentCleared();

    /**
     * 대납 결제 완료 상태를 반환한다.
     * @return 완료 상태인 경우 true, 미완 상태인 경우 false
     * @return
     */
    boolean isEntrustedPaymentCleared();

    /**
     * 온라인 입금 완료 처리된 날짜를 반환한다. 완료되지 않은 경우 빈 문자열을 반환해야 한다.
     * @return 완료 처리된 경우 날짜 문자열(MM/dd), 완료되지 않은 경우 빈 문자열
     */
    String getOnlinePaymentClearedDate();

    /**
     * 대납 결제 완료 처리된 날짜를 반환한다. 완료되지 않은 경우 빈 문자열을 반환해야 한다.
     * @return 완료 처리된 경우 날짜 문자열(MM/dd), 완료되지 않은 경우 빈 문자열
     */
    String getEntrustedPaymentClearedDate();

    /**
     * 전체 결제 완료 상태를 반환한다. 온라인 입금 및 대납 결제 양쪽이 모두 완료되어야 완료 상태가 된다.
     * @return 전체 완료인 경우 true, 미완 상태인 경우 false
     */
    boolean isPaymentCleared();
}
