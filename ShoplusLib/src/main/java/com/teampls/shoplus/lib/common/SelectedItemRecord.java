package com.teampls.shoplus.lib.common;

import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2019-05-24.
 */

public class SelectedItemRecord {
    public ItemRecord itemRecord = new ItemRecord();
    public int unitPriceByBuyer = 0;
    public List<ItemOptionRecord> items = new ArrayList<>();
    public boolean doShowUpdateActivity = false;

    public SelectedItemRecord(ItemRecord itemRecord, int unitPriceByBuyer, List<ItemOptionRecord> items, boolean doShowUpdateActivity) {
        this.itemRecord = itemRecord;
        this.unitPriceByBuyer = unitPriceByBuyer;
        this.items = items;
        this.doShowUpdateActivity = doShowUpdateActivity;
    }

}
