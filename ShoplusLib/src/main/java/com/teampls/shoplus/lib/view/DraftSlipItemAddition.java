package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.CustomerPriceDB;
import com.teampls.shoplus.lib.database_global.PricingRecord;
import com.teampls.shoplus.lib.database_memory.MDraftSlipDB;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseSlipItemAddition;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Medivh on 2017-08-23.
 */

public class DraftSlipItemAddition extends BaseSlipItemAddition {
    protected MDraftSlipDB draftSlipDB;
    private static MyOnClick<List<SlipItemRecord>> onUpdate = null;

    public static void startActivityUpdate(Context context, SlipRecord slipRecord, SlipItemRecord slipItemRecord,
                                           MyOnClick<List<SlipItemRecord>> onUpdate) {
        DraftSlipItemAddition.slipRecord = slipRecord;
        DraftSlipItemAddition.slipItemRecord = slipItemRecord;
        DraftSlipItemAddition.dbActionType = DbActionType.UPDATE;
        DraftSlipItemAddition.onUpdate = onUpdate;
        ((Activity) context).startActivityForResult(new Intent(context, DraftSlipItemAddition.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    protected void setSlipDB() {
        draftSlipDB = MDraftSlipDB.getInstance(context);
    }

    @Override
    protected void initializeSubContainers() {
        if (slipItemRecord.itemId > 0) {
            subContainers.add(Module.name, R.id.module_new_slip_nameContainer, false);
            subContainers.add(Module.quantity, R.id.module_new_slip_quantityContainer, true);
            subContainers.add(Module.unitPrice, R.id.module_new_slip_unitPriceContainer, true);
            subContainers.add(Module.slipType, R.id.module_new_slip_slipTypeContainer, true);
            subContainers.setCurrentModule(Module.quantity);
        } else {
            subContainers.add(Module.name, R.id.module_new_slip_nameContainer, true);
            subContainers.add(Module.quantity, R.id.module_new_slip_quantityContainer, true);
            subContainers.add(Module.unitPrice, R.id.module_new_slip_unitPriceContainer, true);
            subContainers.add(Module.slipType, R.id.module_new_slip_slipTypeContainer, true);
            subContainers.setCurrentModule(Module.name);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        setVisibility(View.GONE, R.id.module_new_slip_advance_outofstock, R.id.module_new_slip_preorder_outofstock);
    }

    @Override
    public void onDeleteClick(SlipItemRecord slipItemRecord) {
        draftSlipDB.items.delete(slipItemRecord);
        onUpdate();
        onFinish();
    }

    private void onUpdate() {
        if (onUpdate != null)
            onUpdate.onMyClick(null, draftSlipDB.items.getRecordsBy(slipRecord.getSlipKey().toSID()));
    }

//    @Override
//    protected void setPricingContainer() {
//        switch (MyApp.get(context)) {
//            case Silvermoon:
//                pricingContainer.setVisibility(BasePreference.isPricingEnabled(context) && slipItemRecord.itemId > 0 ? View.VISIBLE : View.GONE);
//                break;
//            default:
//                pricingContainer.setVisibility(View.GONE);
//                break;
//        }
//    }

    @Override
    public void onApplyClick(DbActionType actionType, SlipItemRecord slipItemRecord) {
        switch (MyApp.get(context)) {
            case Silvermoon:
            case Warspear:
               if (rgPriceMode.getCheckedItem() == PriceMode.ByCustomer && slipItemRecord.itemId > 0
                        && originalSlipItemRecord.unitPrice != slipItemRecord.unitPrice) {
                    PricingRecord record = new PricingRecord(0, userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber(),
                            slipRecord.counterpartPhoneNumber, slipItemRecord.itemId, slipItemRecord.unitPrice, DateTime.now());
                    CustomerPriceDB.getInstance(context).updateOrInsert(record);
                }
                break;
            default:
                break;
        }

        switch (slipItemRecord.slipItemType) {
            default:
                draftSlipDB.items.update(slipItemRecord.id, slipItemRecord);
                onUpdate();
                onFinish();
                break;
            case RETURN:
            case ADVANCE_SUBTRACTION:
            case WITHDRAWAL:
                splitSlipItemRecord(actionType, draftSlipDB.items, true, new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        onUpdate();
                        onFinish();
                    }
                });
                break;
        }
    }
}
