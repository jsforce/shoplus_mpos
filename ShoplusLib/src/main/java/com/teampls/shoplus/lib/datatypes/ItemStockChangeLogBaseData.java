package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 재고수량 일일 Snapshot 로그 데이터
 *
 * @author lucidite
 */
public class ItemStockChangeLogBaseData implements ItemStockChangeLogProtocol {
    private String basetimeStr;
    private int value;

    public ItemStockChangeLogBaseData(JSONObject dataObj) throws JSONException  {
        if (dataObj == null) {
            this.basetimeStr = "";
            this.value = 0;
        } else {
            this.basetimeStr = dataObj.getString("created");
            this.value = dataObj.getInt("value");
        }
    }

    @Override
    public String getLogId() {
        return this.basetimeStr;
    }

    @Override
    public DateTime getUpdatedDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.basetimeStr);
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
