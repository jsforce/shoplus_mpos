package com.teampls.shoplus.lib.awsservice.apigateway;

import com.teampls.shoplus.lib.common.Empty;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author lucidite
 */
public class APIGatewayHelper {
    private static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyMMddHHmmssSSS");
    private static DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyMMdd");
    private static DateTimeFormatter monthFormatter = DateTimeFormat.forPattern("yyMM");
    private static DateTimeFormatter rdsDateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    private static DateTimeFormatter rdsDateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    private static DateTimeFormatter logDateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

    public static DateTime fallbackDatetime = Empty.dateTime;

    /**
     * 클라이언트에서 사용하는 DateTime 인스턴스를 서버에 전송하기 위한 날짜-시간 문자열로 변환한다.
     *
     * @param dateTime
     * @return
     */
    public static String getRemoteDateTimeString(DateTime dateTime) {
        return dateTimeFormatter.print(dateTime);
    }

    /**
     * 클라이언트에서 사용하는 DateTime 인스턴스를 서버에 전송하기 위한 날짜 문자열로 변환한다.
     *
     * @param date
     * @return
     */
    public static String getRemoteDateString(DateTime date) {
        return dateFormatter.print(date);
    }

    /**
     * 클라이언트에서 사용하는 DateTime 인스턴스를 서버에 전송하기 위한 연-월 문자열로 변환한다.
     *
     * @param month
     * @return
     */
    public static String getRemoteMonthString(DateTime month) {
        return monthFormatter.print(month);
    }

    /**
     * 클라이언트에서 사용하는 DateTime 인스턴스를 RDS에 전송하기 위한 날짜 문자열로 변환한다.
     *
     * @param date
     * @return
     */
        public static String getRDSDateTimeString(DateTime date) {
            return rdsDateTimeFormatter.print(date);
    }

    /**
     * 클라이언트에서 사용하는 DateTime 인스턴스를 RDS의 날짜 데이터와 비교하는 파라미터용 날짜 문자열로 변환한다.
     * 주로 서버 요청에 파라미터로 전달하여 RDS의 날짜(시간 제외) 데이터를 비교하기 위한 용도로 사용한다(query용).
     *
     * @param date
     * @return
     */
    public static String getRDSDateOnlyString(DateTime date) {
        return rdsDateFormatter.print(date);
    }

    /**
     * 클라이언트에서 사용하는 DateTime 인스턴스를 서버 로그 시스템에 전송하기 위한 날짜-시간 문자열로 변환한다.
     * @param date
     * @return
     */
    public static String getLogDateTimeString(DateTime date) {
        return logDateTimeFormatter.print(date);
    }

    /**
     * 서버로부터 수신한 날짜-시간 문자열을 클라이언트에서 사용할 DateTime 인스턴스로 변환한다.
     * 문자열 포맷이 잘못된 경우 2016-01-01 00:00을 반환한다.
     * TODO @Sunstrider 포맷이 잘못된 경우 서버로부터 잘못된 문자열을 받은 것이므로 서비스 예외를 발생시킬 것인가?
     *
     * @param remoteDateTimeString
     * @return
     */
    public static DateTime getDateTimeFromRemoteString(String remoteDateTimeString) {
        try {
            return dateTimeFormatter.parseDateTime(remoteDateTimeString);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return fallbackDatetime;
        }
    }

    /**
     * 서버로부터 수신한 날짜 문자열을 클라이언트에서 사용할 DateTime 인스턴스로 변환한다.
     * 문자열 포맷이 잘못된 경우 2016-01-01 00:00을 반환한다.
     * TODO @Sunstrider 포맷이 잘못된 경우 서버로부터 잘못된 문자열을 받은 것이므로 서비스 예외를 발생시킬 것인가?
     *
     * @param remoteDateString
     * @return
     */
    public static DateTime getDateFromRemoteString(String remoteDateString) {
        try {
            return dateFormatter.parseDateTime(remoteDateString);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return fallbackDatetime;
        }
    }

    /**
     * 서버로부터 수신한 월 문자열을 클라이언트에서 사용할 DateTime 인스턴스로 변환한다.
     * 문자열 포맷이 잘못된 경우 2016-01-01 00:00을 반환한다.
     * TODO @Sunstrider 포맷이 잘못된 경우 서버로부터 잘못된 문자열을 받은 것이므로 서비스 예외를 발생시킬 것인가?
     *
     * @param remoteDateString
     * @return
     */
    public static DateTime getMonthFromRemoteString(String remoteDateString) {
        try {
            return monthFormatter.parseDateTime(remoteDateString);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return fallbackDatetime;
        }
    }

    /**
     * RDS로부터 수신한 날짜-시간 문자열을 클라이언트에서 사용할 DateTime 인스턴스로 변환한다.
     * 문자열 포맷이 잘못된 경우 2016-01-01 00:00을 반환한다.
     *
     * @param rdsDateString
     * @return
     */
    public static DateTime getDateTimeFromRDSString(String rdsDateString) {
        try {
            return rdsDateTimeFormatter.parseDateTime(rdsDateString);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return fallbackDatetime;
        }
    }

    /**
     * RDS로부터 수신한 날짜 문자열을 클라이언트에서 사용할 DateTime 인스턴스로 변환한다.
     * 문자열 포맷이 잘못된 경우 2016-01-01 00:00을 반환한다.
     *
     * @param rdsDateString
     * @return
     */
    public static DateTime getDateFromRDSDateOnlyString(String rdsDateString) {
        try {
            return rdsDateFormatter.parseDateTime(rdsDateString);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return fallbackDatetime;
        }
    }

    /**
     * Gets a string from an input stream.
     *
     * @param inputstream	{@code InputStream}
     * @return	a string from the {@code inputstream}
     * @throws IOException    if {@code BufferReader} fails to read a line from the {@code InputStream},
     * 						or {@code BufferReader} fails to reset itself
     */
    public static String getStringFromInputStream(InputStream inputstream) throws IOException {
        BufferedReader bufferedReader = null;
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputstream));
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException exception) {
            throw exception;

        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();			// may throw IOException..
            }
        }
        return stringBuilder.toString();
    }
}
