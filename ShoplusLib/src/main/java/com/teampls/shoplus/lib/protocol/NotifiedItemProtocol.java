package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.protocol.ItemDataProtocol;

import org.joda.time.DateTime;

/**
 * 판매자로부터 알림을 수신한 아이템 관련 정보. 아이템 데이터(구매자용) 및 알림을 수신한 시간 정보를 포함한다.
 *
 * @author lucidite
 */

public interface NotifiedItemProtocol {
    DateTime getNotifiedDatetime();
    ItemDataProtocol getNotifiedItem();
}
