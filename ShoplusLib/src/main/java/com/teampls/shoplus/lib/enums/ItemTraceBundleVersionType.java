package com.teampls.shoplus.lib.enums;

import android.util.Log;

/**
 * @author lucidite
 */
public enum ItemTraceBundleVersionType {
    SINGLE(1, "1"),
    DI(2, "2"),
    TRI(3, "3"),
    TETRA(4, "4"),
    PENTA(5, "P"),
    HEXA(6, "6"),
    HEPTA(7, "7"),
    OCTA(8, "8"),
    NONA(9, "9"),
    DECA(10, "D"),
    BUNDLE_PACK(1, "B");

    private int count;
    private String versionStr;

    ItemTraceBundleVersionType(int count, String versionStr) {
        this.count = count;
        this.versionStr = versionStr;
    }

    public int getCount() {
        return count;
    }

    public String getVersionStr() {
        return versionStr;
    }

    public static ItemTraceBundleVersionType fromTraceDetailsPostfixStr(String postfixStr) {
        if (postfixStr == null || postfixStr.isEmpty()) {
            return ItemTraceBundleVersionType.SINGLE;
        }
        try {
            int versionCountValue = Integer.valueOf(postfixStr);
            for (ItemTraceBundleVersionType bundleVersion: ItemTraceBundleVersionType.values()) {
                if (bundleVersion.count == versionCountValue) {
                    return bundleVersion;
                }
            }
            return ItemTraceBundleVersionType.SINGLE;       // fallback (default)
        } catch (NumberFormatException e) {
            return ItemTraceBundleVersionType.SINGLE;       // fallback (default)
        }
    }

    public static ItemTraceBundleVersionType fromVersionStr(String versionStr) {
        if (versionStr == null || versionStr.isEmpty()) {
            return ItemTraceBundleVersionType.SINGLE;
        }
        for (ItemTraceBundleVersionType bundleVersion: ItemTraceBundleVersionType.values()) {
            if (bundleVersion.versionStr.equals(versionStr)) {
                return bundleVersion;
            }
        }
        return ItemTraceBundleVersionType.SINGLE;       // fallback (default)
    }

    public static final String SEPARATOR = "_";
}
