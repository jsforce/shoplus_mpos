package com.teampls.shoplus.lib.help;

/**
 * Created by Medivh on 2017-01-04.
 */

public class HelpRecord {
    public String title = "", description = "";
    public int resId = 0;

    public HelpRecord(String title, String description, int resId) {
        this.title = title;
        this.resId = resId;
        this.description = description;
    }
}
