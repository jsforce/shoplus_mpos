package com.teampls.shoplus.lib.composition;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Pair;
import android.view.View;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.enums.ImageSizeType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;
import com.teampls.shoplus.lib.view.ImagePickerView;
import com.teampls.shoplus.lib.view.MainProgressBar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2019-02-07.
 */

abstract public class NewItemComposition {
    private Context context;
    private List<String> imagePaths = new ArrayList<>();
    private String taskId = "new.item";
    private MyOnTask onTask;
    private MainProgressBar mainProgressBar;
    private UserSettingData userSettingData;
    public boolean onItemUploading = false;
    private ItemDB itemDB;

    public NewItemComposition(Context context, MainProgressBar mainProgressBar) {
        this.context = context;
        userSettingData = UserSettingData.getInstance(context);
        itemDB = ItemDB.getInstance(context);
        this.mainProgressBar = mainProgressBar;
    }

    public void oneSelectImages(int selectionLimit, MyOnTask onTask) {
        this.onTask = onTask;
        imagePaths.clear();
        ImagePickerView.start(context, true, selectionLimit, true, true, new MyOnClick<List<String>>() {
            @Override
            public void onMyClick(View view, List<String> record) {
                if (record.isEmpty()) {
                    twoMakeItemWithoutImage();
                } else {
                    imagePaths = record;
                    twoMakeItems();
                }
            }
        });
    }

    public void twoMakeItemWithoutImage() {
        new CommonServiceTask(context, "NewItemCreator") {
            private int newItemId = 0;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                onItemUploading = true;
                if (mainProgressBar != null)
                    mainProgressBar.start(taskId, "아이템 등록 중", 1);
                ItemDataProtocol protocol = itemService.makeItemWithoutImage(userSettingData.getUserShopInfo());
                itemDB.insert(new ItemRecord(protocol)); // ItemDB, PathDB, ImageDB, ....
                newItemId = protocol.getItemId();
            }

            @Override
            public void onPostExecutionUI() {
                mainProgressBar.stop(taskId);
                MyUI.toastSHORT(context, String.format("아이템을 등록 했습니다."));
                onNoImageCreate(newItemId);
                onItemUploading = false;
                if (onTask != null)
                    onTask.onTaskDone("");
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                onItemUploading = false;
            }
        };
    }

    abstract public void onNoImageCreate(int newItemId);

    public void twoMakeItems() {
        new CommonServiceTask(context, "NewItemCreator") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                // 표준 이미지로
                onItemUploading = true;
                mainProgressBar.start(taskId, String.format("이미지 변환 중 (%d)", imagePaths.size()), imagePaths.size());
                List<String> standardImagePaths = new ArrayList<>();
                List<Bitmap> bitmaps = new ArrayList<>();
                for (String originalFilePath : imagePaths) {
                    String standardFilePath = MyDevice.createTempFilePath(BaseAppWatcher.appDir, "jpg");
                    Pair<Boolean, Bitmap> result = MyGraphics.toStandardBitmapAndImageFile(originalFilePath, standardFilePath);
                    if (result.first) {
                        standardImagePaths.add(standardFilePath);
                        bitmaps.add(result.second);
                        MyDevice.refresh(context, new File(standardFilePath));
                    }
                    mainProgressBar.next(taskId);
                }

                // 서버에 업로드
                mainProgressBar.refresh(taskId, "이미지 업로드 중", 3);
                final List<ItemDataProtocol> items = itemService.makeItems(userSettingData.getUserShopInfo(), standardImagePaths); // Server
                mainProgressBar.next(taskId);

                // DB 업데이트
                for (int index = 0; index < items.size(); index++) {
                    ItemRecord itemRecord = new ItemRecord(items.get(index));
                    itemDB.insertWithImage(itemRecord, standardImagePaths.get(index), bitmaps.get(index), ImageSizeType.ORIGINAL); // ItemDB, PathDB, ImageDB, ....
                }
                mainProgressBar.next(taskId);

                // Delete Temp files
                MyDevice.deleteFiles(context, standardImagePaths);
                mainProgressBar.next(taskId);

            }

            @Override
            public void onPostExecutionUI() {
                mainProgressBar.stop(taskId);
                MyUI.toastSHORT(context, String.format("아이템을 등록 했습니다."));
                onItemUploading = false;
                if (onTask != null)
                    onTask.onTaskDone("");
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                onItemUploading = false;
            }
        };
    }

}
