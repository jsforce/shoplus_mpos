package com.teampls.shoplus.lib.database_global;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.enums.DBResultType;

import java.util.List;

/**
 * Created by Medivh on 2016-09-13.
 */
public class SlipImagePathDB extends BaseDB<SlipImagePathRecord> {

    private static SlipImagePathDB instance = null;
    private static int Ver = 2;

    // Ver1 : _id, slipDBid (int), imagePath;
    // Ver2 : _id, slipKey (string), imagePath;

    public enum Column {
        _id, slipKey, imagePath;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    private SlipImagePathDB(Context context) {
        super(context, "SlipImagePathDB", "SlipImagePathTable", Ver, Column.toStrs());
    }

    public static SlipImagePathDB getInstance(Context context) {
        if (instance == null)
            instance = new SlipImagePathDB(context);
        return instance;
    }

    @Override
    public void deleteDB() {
        super.deleteDB();
    }

    protected ContentValues toContentvalues(SlipImagePathRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.slipKey.toString(), record.slipKey);
        contentvalues.put(Column.imagePath.toString(), record.imagePath);
        return contentvalues;
    }

    @Override
    protected void toLogCat(int id, SlipImagePathRecord record, int option, String operation) {
        if (option == 0) {
            Log.i("DEBUG_JS", String.format("[%s] <%d> %s, %s", operation,
                    id, record.slipKey, record.imagePath));
        }
    }

    public SlipImagePathRecord getRecordBy(String slipKey, String imagePath) {
        return getRecord(getId(Column.slipKey, slipKey, Column.imagePath, imagePath));
    }

    public DBResult updateOrInsert(SlipImagePathRecord record) {
        if (this.hasValue(Column.slipKey, record.slipKey, Column.imagePath, record.imagePath)) {
            SlipImagePathRecord dbRecord = getRecordBy(record.slipKey, record.imagePath);
            if (dbRecord.isSame(record) == false) // column이 slipDB, imagePath 뿐이므로 무조건 true
                return update(dbRecord.id, record);
            else
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
        } else {
            return insert(record);
        }
    }

    public void toLogCat(int option) {
        for (SlipImagePathRecord record : getRecords())
            toLogCat(record, option, "show");
    }

    public void toLogCat(String slipKey, String operation) {
        for (SlipImagePathRecord record : getRecordsBy(slipKey))
            toLogCat(record, 0, operation);
    }

    public void toLogCat(SlipImagePathRecord record, int option, String operation) {
        toLogCat(record.id, record, option, operation);
    }


    public List<SlipImagePathRecord> getRecordsBy(String slipKey) {
        Cursor cursor = getCursor(Column.slipKey, slipKey);
        List<SlipImagePathRecord> results = getRecords(cursor);
        cursor.close();
        return results;
    }

    public List<String> getPaths(String slipKey) {
        return getStrings(Column.imagePath, Column.slipKey, slipKey);
    }


    @Override
    protected int getId(SlipImagePathRecord record) {
        return record.id;
    }

    @Override
    protected SlipImagePathRecord getEmptyRecord() {
        return  new SlipImagePathRecord();
    }

    @Override
    protected SlipImagePathRecord createRecord(Cursor cursor) {
        return new SlipImagePathRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.slipKey.ordinal()),
                cursor.getString(Column.imagePath.ordinal()));
    }

    public void deleteRecords(String slipKey) {
        deleteAll(Column.slipKey, BaseUtils.toList(slipKey));
    }

}
