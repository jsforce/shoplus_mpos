package com.teampls.shoplus.lib.view_base;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.MainViewAdapter;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BackPressCloseHandler;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseAwsTaskScheduler;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyCountDownTimer;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyImageLoader;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.datatypes.ServiceActivationResult;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.LoginDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.UserConfigTypeDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.AppLinkDialog;
import com.teampls.shoplus.lib.view.MainProgressBar;
import com.teampls.shoplus.lib.view.MyInfoView;
import com.teampls.shoplus.lib.view.MyUsersView;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.viewSetting.PrinterSettingView;

import java.util.ArrayList;

/**
 * Created by Medivh on 2016-08-31.
 */
abstract public class BaseMainViewPager extends BaseActivity implements ViewPager.OnPageChangeListener,
        ActionBar.TabListener {
    protected BackPressCloseHandler backPressCloseHandler;
    protected ViewPager tabViewPager;
    public static MainViewAdapter adapter;
    public static ActionBar tabActionBar;
    public static MainProgressBar mainProgressBar;
    protected MyImageLoader imageLoader;
    protected BaseLoginChecker loginChecker;
    protected BaseAwsTaskScheduler awsTaskScheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        mainProgressBar = new MainProgressBar(context);
        imageLoader = MyImageLoader.getInstance(context);
        myActionBar.setTitle(MyAWSConfigs.isProduction() ? "" : "개발서버");
        backPressCloseHandler = new BackPressCloseHandler(this);
        adapter = new MainViewAdapter(getSupportFragmentManager());

        tabViewPager = (ViewPager) findViewById(R.id.mainPager);
        tabViewPager.setOnPageChangeListener(this);
        tabViewPager.setAdapter(adapter);

        tabActionBar = myActionBar.getActionBar(); //getActionBar();
        tabActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        addTabs(); // adapter, actionBar, tabViewPager 필요

        setBackground(MyApp.getThemeColors(context)); // actionbar 생성 후 색 입히기 가능

        MyView.setTextViewByDeviceSize(context, getView(), R.id.main_progressMessage, R.id.main_errorMessage);

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE))
            MyDevice.createFolders(context, BaseAppWatcher.appDir);
    }


    public ViewPager getPager() {
        if (tabViewPager == null) {
            Log.e("DEBUG_JS", String.format("[BaseMainViewPager.getPager] tabViewPager == NULL"));
            return new ViewPager(context);
        } else
            return tabViewPager;
    }

    @Override
    public void onBackPressed() {
        if (backPressCloseHandler.doubleClicked()) {
            if (awsTaskScheduler.getState().isOnTasking()) {
                if (awsTaskScheduler.getState().isOnTasking()) {
                    MyUI.toastSHORT(context, "지금은 데이터를 다운로드 받고 있어 종료할 수 없습니다. 잠시 후 다시 시도해주세요");
                }
                if (backPressCloseHandler.getTotalDoubleClickCount() >= 3) {
                    new MyAlertDialog(context, "강제 종료", "앱을 강제로 종료하시겠습니까?") {
                        @Override
                        public void yes() {
                            doFinish();
                        }

                        public void no() {
                            backPressCloseHandler.resetTotalDoubleClickCount();
                        }
                    };
                }
            } else {
                doFinish();
            }
        }
    }

    abstract protected void doFinish();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (BaseAppWatcher.checkInstance(this) == false) {
//            Log.e("DEBUG_JS", String.format("[BaseMainViewPager.onActivityResult] instance == NULL"));
//            return;
//        }

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            Log.e("DEBUG_JS", String.format("[BaseMainViewPager.onActivityResult] result Code = %d", resultCode));
            return;
        }
        switch (requestCode) {
            case BaseGlobal.RequestRefresh:
                onMyMenuCreate();
                adapter.refreshFragments();
                break;
            case BaseGlobal.REQUST_MENU_REFRESH:
                onMyMenuCreate();
                break;
        }
    }

    public int getThisView() {
        return R.layout.base_main_pager;
    }

    public void setBackground(Pair<String, String> hexs) {
        if (myActionBar == null) return;
        if (tabActionBar == null) return;

        if (hexs.first.isEmpty() == false) {
            myActionBar.setBackground(hexs.first);
            tabActionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor(hexs.first)));
        }
        if (hexs.second.isEmpty() == false)
            myActionBar.setBottomLine(hexs.second);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseAppWatcher.autoRecover(context, null);
        BaseAppWatcher.onActivityResume(context);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BaseAppWatcher.onActivityPause(context);
    }

    @Override
    protected void onDestroy() {
        BaseAppWatcher.onActivityDestroy(context);
        super.onDestroy();
    }

    protected void deleteTempFiles(boolean doFinishApp) {
        final int timeDelayFileDeletionMs = 40; // 실측은 20ms 정도
        final int fileCount = MyDevice.deleteTempFiles(context, BaseAppWatcher.appDir);
        if (doFinishApp) {
            if (fileCount >= 50)
                MyUI.toastSHORT(context, String.format("임시 생성된 파일을 정리합니다. %d초후 종료하겠습니다", (fileCount * timeDelayFileDeletionMs) / 1000));        // wait until all files are deleted
            new MyDelayTask(context, Math.max(fileCount * timeDelayFileDeletionMs, 500)) {
                @Override
                public void onFinish() {
                    ((Activity) context).finishAffinity();
                }
            };
        }
    }

    abstract public void addTabs();

    public void refreshTabs() {
        tabActionBar.removeAllTabs();
        for (int i = 0; i < adapter.getCount(); i++) {
            switch (MyDevice.getDeviceSize(context)) {
                default:
                    tabActionBar.addTab(tabActionBar.newTab().setText(adapter.getPageTitle(i)).setTabListener(this));
                    break;
//                case W600:
//                    TextView textView = MyView.createTextView(context, adapter.getPageTitle(i).toString(), 18, 0, true, ColorType.White.colorInt);
//                    textView.setLayoutParams(MyView.getParams(context, MyView.PARENT, 40));
//                    adapter.getFragment(i).setCustomTextView(textView);
//                    tabActionBar.addTab(tabActionBar.newTab().setCustomView(textView).setTabListener(this));
//                    break;
//                case W720:
//                case W800:
//                    textView = MyView.createTextView(context, adapter.getPageTitle(i).toString(), 20, 0, true, ColorType.White.colorInt);
//                    textView.setLayoutParams(MyView.getParams(context, MyView.PARENT, 50));
//                    adapter.getFragment(i).setCustomTextView(textView);
//                    tabActionBar.addTab(tabActionBar.newTab().setCustomView(textView).setTabListener(this));
//                    break;
            }

            adapter.notifyDataSetChanged();
        }
    }

    public void hideLastTab() {
        int fragmentCount = adapter.getFragments().size();
        int tabCount = tabActionBar.getTabCount();
        if (fragmentCount == tabCount) {
            tabActionBar.removeTabAt(tabCount - 1);
        }
    }

    public void showLastTab() {
        if (adapter == null) return;
        if (adapter.getFragments() == null) return;
        if (tabActionBar == null) return;
        int fragmentCount = adapter.getFragments().size();
        int tabCount = tabActionBar.getTabCount();
        if (tabCount < fragmentCount) {
            tabActionBar.addTab(tabActionBar.newTab().setTabListener(this));
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab arg0, FragmentTransaction arg1) {
    }

    @Override
    public void onTabUnselected(ActionBar.Tab arg0, FragmentTransaction arg1) {
    }

    @Override
    public void onPageSelected(int position) {
        tabActionBar.setSelectedNavigationItem(position);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        tabViewPager.setCurrentItem(tab.getPosition());

        // unselected -> selected .... 순서를 보장한다
        for (BaseFragment fragment : adapter.getFragments())
            if (fragment.getTabIndex() != tab.getPosition())
                fragment.onUnSelected();

        for (BaseFragment fragment : adapter.getFragments())
            if (fragment.getTabIndex() == tab.getPosition())
                fragment.onSelected();
    }

    public BaseFragment getCurrentFragment() {
        return adapter.getFragment(tabViewPager.getCurrentItem());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        onMyMenuCreate();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case gridUpdateApp:
                MyDevice.openPlayStore(context, getPackageName());
                break;
            case gridSevenstars:
                MyApp.Sevenstars.open(context);
                break;
            case gridSilvermoon:
                MyApp.Silvermoon.open(context);
                break;
            case gridStormwind:
                MyApp.Stormwind2.open(context);
                break;
            case gridDalaran:
                MyApp.Dalaran.open(context);
                break;
            case gridExodar:
                MyApp.Exodar.open(context);
                break;
            case gridCustomers:
                MyUsersView.startActivity(context);
                break;
            case gridPrinterSetting:
                PrinterSettingView.startActivity(context);
                break;
            case gridMyInfo:
                MyInfoView.startActivity(context);
                break;
            case more:
                myActionBar.popupActionNeverMenus();
                break;
            case gridUserQuestion:
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                break;
            case gridShoplusLink:
                new AppLinkDialog(context, MyApp.get(context).androidLink, MyApp.get(context).appleLink).show();
                break;
        }
    }

    protected Intent getSampleItem() {
        String filePath = MyDevice.createTempFilePath(BaseAppWatcher.appDir, "jpg");
        MyGraphics.toFile(context, R.drawable.sample_item, filePath);
        ArrayList<String> paths = new ArrayList<>();
        paths.add(filePath);
        Intent data = new Intent();
        data.putExtra(BaseGlobal.KEY_SELECTED_IMAGE_PATHS, paths);
        return data;
    }

    abstract public class BaseLoginChecker extends MyCountDownTimer {
        protected final int timeWarningMs = 3000;

        public BaseLoginChecker(long interval_ms) {
            super(interval_ms);
        }

        public BaseLoginChecker init() {
            cancel();
            awsTaskScheduler.init();
            return this;
        }

        @Override
        public void onTick(long millsLeft) {
            switch (BaseAppWatcher.getLoginState()) {
                case onTrying:
                    if (elapsedTimeMs == timeWarningMs) {
                        MyUI.toastSHORT(context, String.format("통신 환경이 원활하지 않습니다"));
                    } else if (elapsedTimeMs >= timeWarningMs * 5) {
                        cancel();
                        if (context == null) {
                            Log.e("DEBUG_JS", String.format("[BaseLoginChecker.onTick] context == null"));
                            return;
                        }
                        new MyAlertDialog(context, "통신상황 점검", "통신 상황을 점검하고 앱을 재시작 해주세요") {
                            @Override
                            public void yes() {
                                ((Activity) context).finishAffinity();
                            }
                        };
                    }
                    break;
                case notAuthorized:
                    cancel();
                    MyUI.toastSHORT(context, String.format("로그인 정보가 일치하지 않습니다"));
                    new LoginDialog(context).show();
                    break;
                case onSuccess:
                    break;
                case userNotFound:
                    cancel();
                    MyUI.toast(context, String.format("등록되지 않은 유저입니다. 앱을 종료합니다"));
                    MyDB.getInstance(context).logOut();
                    new MyDelayTask(context, 500) {
                        @Override
                        public void onFinish() {
                            ((Activity) context).finishAffinity();
                        }
                    };
                    break;
                case onAwsError:
                case onError:
                    cancel();
                    new MyAlertDialog(context, "로그인 실패", "통신이나 서버 문제로 로그인하지 못했습니다\n\n앱을 종료 하시겠습니까?") {
                        @Override
                        public void yes() {
                            ((Activity) context).finishAffinity();
                        }
                    };
                    break;
            }

            if (BaseAppWatcher.isLoggedIn()) {
                if (awsTaskScheduler.getState() == TaskState.beforeTasking) {
                    if (BaseAppWatcher.isTokenExpired(context, "LoginChecker")) {
                        new CommonServiceTask(context, "LoginChecker") {
                            @Override
                            public void doInBackground() throws MyServiceFailureException {
                                Log.w("DEBUG_JS", String.format("[LoginChecker] token refrehsed .... "));
                            }
                        };
                    } else {
                        if (adapter.isFragementsCreated())
                            awsTaskScheduler.start();
                    }
                }
            }

            if (awsTaskScheduler.getState().isOnError()) {
                cancel();
                new MyAlertDialog(context, "다운로드 중 오류 발생", String.format("%s\n\n계속 진행하시겠습니까?", awsTaskScheduler.errorMessage)) {
                    @Override
                    public void yes() {
                        adapter.refreshFragments();
                    }

                    public void no() {
                        finishAffinity();
                        MyUI.toastSHORT(context, String.format("앱을 종료합니다"));
                    }
                };
            }

            if (awsTaskScheduler.getState().isFinished()) {
                cancel();
                myActionBar.setRefreshTime();
            }
        }
    }

    public class RegistrationDialog extends MyButtonsDialog {

        public RegistrationDialog(final Context context, ShoplusServiceType serviceType) {
            this(context, "무료 POS 사용 안내", String.format("%s 앱은 유료입니다. 먼저 무료 앱을 설치해서 사용해 보세요", serviceType.uiStr));
        }

        protected RegistrationDialog(Context context, String title, String message) {
            super(context, title, message);
            setDialogWidth(0.95, 0.7);
            setCancelable(false);
            createDialog();
        }

        protected void addWorkingShops() {
            if (userSettingData.getAllowedShops().size() >= 1) {
                tvTitle.setText("근무 매장 연결 안내");
                tvMessage.setText("연결 버튼을 눌러 내가 근무하고 있는 매장을 선택해 주세요.");

                for (final String phoneNumber : userSettingData.getAllowedShops()) {
                    addButton(String.format("[%s] 연결", BaseUtils.toPhoneFormat(phoneNumber)), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            userSettingData.updateWorkingShopPhoneNumber(phoneNumber, new MyOnTask() {
                                @Override
                                public void onTaskDone(Object result) {
                                    MyUI.toastSHORT(context, String.format("%s로 연결했습니다. 앱을 재시작 합니다", phoneNumber));
                                    finishApp(500, true);
                                }
                            });
                        }
                    });
                }
            }
        }

        protected void addContactUs() {
            addButton("앱 사용 문의하기", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                }
            });
        }

        protected void addHomepage() {
            addTitleButton("홈페이지", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyDevice.openWeb(context, BaseGlobal.SHOPL_US);
                }
            });
        }

        protected void addGoToMPos() {
            addButton("무료 POS 등록하기", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyApp.Warspear.open(context);
                    finishApp(500, false);
                }
            });
        }

        protected void addRestartApp() {
            addButton("매장폰 등록 후 재시작", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finishApp(0, true);
                }
            });
        }

        protected void addGetMPosLicenseByKakao() {

            addButton("플러스 친구로 등록하기", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                    MyUI.toast(context, String.format("1:1 채팅 > 무료이용 등록"));
                }
            });
        }

        protected void addGetMPosLicenseDirectly() {
            addButton("등록 요청하기", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DirectRegistrationDialog(context).show();
                }
            });
        }

        protected void addLogout() {
            addButton("로그 아웃", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new MyAlertDialog(context, "로그 아웃", "모든 앱에서 로그 아웃을 하시겠습니까? 저장된 아이디와 비밀번호가 삭제됩니다") {
                        @Override
                        public void yes() {
                            myDB.logOut();
                            finishAffinity();
                        }
                    };
                }
            });
        }

        protected void createDialog() {
            addWorkingShops();
            addGoToMPos();
            addContactUs();
            addHomepage();
            addLogout();
        }

        protected void finishApp(int delayMs, boolean doRestart) {
            new MyDelayTask(context, delayMs) {
                @Override
                public void onFinish() {
                    dismiss();
                    ((Activity) context).finishAffinity();
                }
            };

            if (doRestart) {
                new MyDelayTask(context, delayMs + 500) {
                    @Override
                    public void onFinish() {
                        MyDevice.openApp(context, context.getPackageName());
                    }
                };
            }
        }

        @Override
        public void onCloseClick() {
            MyUI.toastSHORT(context, String.format("앱을 종료합니다"));
            finishApp(500, false);
        }

    }

    public class DirectRegistrationDialog extends BaseDialog {
        private EditText etLocation, etShopName, etIntroducer;

        public DirectRegistrationDialog(Context context) {
            super(context);
            setDialogWidth(0.9, 0.7);
            MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_direct_reg_title,
                    R.id.dialog_direct_reg_message, R.id.dialog_direct_reg_location,
                    R.id.dialog_direct_reg_location_hint, R.id.dialog_direct_reg_shopName,
                    R.id.dialog_direct_reg_introducer_note);
            etShopName = findViewById(R.id.dialog_direct_reg_shopName);
            etShopName.setText(userSettingData.getWorkingShop().getShopName());
            etLocation = findViewById(R.id.dialog_direct_reg_location);
            etLocation.requestFocus();
            etIntroducer = findViewById(R.id.dialog_direct_reg_introducer);
            MyDevice.showKeyboard(context, etLocation);
            setOnClick(R.id.dialog_direct_reg_apply, R.id.dialog_direct_reg_close, R.id.dialog_direct_reg_contactUs);
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_direct_reg;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_direct_reg_apply) {
                if (Empty.isEmpty(context, etLocation, "위치 입력이 필요합니다")) return;
                if (Empty.isEmpty(context, etShopName, "매장 이름 입력이 필요합니다")) return;
                register();
            } else if (view.getId() == R.id.dialog_direct_reg_close) {
                MyDevice.hideKeyboard(context, etLocation);
                dismiss();
            } else if (view.getId() == R.id.dialog_direct_reg_contactUs) {
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
            }
        }

        private void register() {
            new CommonServiceTask(context, "register") {
                ServiceActivationResult result;

                @Override
                public void doInBackground() throws MyServiceFailureException {
                    String location = etLocation.getText().toString();
                    String shopName = etShopName.getText().toString();
                    String introducerPhoneNumber = etIntroducer.getText().toString();
                    result = workingShopService.activateMPOSService(userSettingData.getUserShopInfo(), shopName, location, introducerPhoneNumber);
                }

                @Override
                public void onPostExecutionUI() {
                    if (result.isSuccess()) {
                        MyDevice.hideKeyboard(context, etLocation);
                        dismiss();
                        MyUI.toastSHORT(context, String.format("등록되셨습니다. 재시작을 눌러 주세요"));
                    } else {
                        if (etIntroducer.getText().toString().isEmpty()) {
                            new MyAlertDialog(context, "추천인 없음", "기존에 사용 중인 분의 추천이 없으시면 저희 담당자가 등록해드립니다. 등록하시겠습니까?") {
                                @Override
                                public void yes() {
                                    dismiss();
                                    MyUI.toastSHORT(context, String.format("등록 담당자로 연결합니다"));
                                    MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                                }

                                @Override
                                public void no() {
                                    dismiss();
                                }
                            };

                        } else {
                            Log.e("DEBUG_JS", String.format("[DirectRegistrationDialog.onPostExecutionUI] %s", result.getError().toString()));
                            MyUI.toastSHORT(context, String.format("%s", result.getError().getUiStr()));
                        }
                    }
                }

                @Override
                public void catchException(MyServiceFailureException e) {
                    super.catchException(e);
                    MyUI.toastSHORT(context, String.format("%s", e.getLocalizedMessage()));
                }
            };
        }
    }

    abstract public class NotActivatedDialog extends BaseDialog {
        protected ShoplusServiceType serviceType;
        protected EditText etShopName, etRedeemCode;
        protected LinearLayout workingShopsContainer, redeemContainer;
        protected int marginTopDp = 5;

        public NotActivatedDialog(Context context, ShoplusServiceType serviceType) {
            super(context);
            setDialogWidth(0.9, 0.6);
            setCancelable(false);
            this.serviceType = serviceType;
            findTextViewById(R.id.dialog_deactivated_user_title, String.format("%s 사용안내", serviceType.uiStr));
            findTextViewById(R.id.dialog_deactivated_user_message, String.format("%s 기능은 유료입니다. 가입 문의를 부탁드립니다.", serviceType.uiStr));
            findTextViewById(R.id.dialog_deactivated_user_contactUs, String.format("%s 가입 문의", serviceType.uiStr));
            etShopName = findViewById(R.id.dialog_deactivated_user_shopName);
            etShopName.setText(userSettingData.getWorkingShop().getShopName());
            etRedeemCode = findViewById(R.id.dialog_deactivated_user_redeem_code);
            etRedeemCode.requestFocus();
            workingShopsContainer = findViewById(R.id.dialog_deactivated_workingshops_container);
            redeemContainer = findViewById(R.id.dialog_deactivated_user_redeemContainer);
            addWorkingShops();

            setOnClick(R.id.dialog_deactivated_user_contactUs, R.id.dialog_deactivated_to_buyer,
                    R.id.dialog_deactivated_user_close, R.id.dialog_deactivated_user_send_redeem,
                    R.id.dialog_deactivated_user_logout);

            MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_deactivated_user_title,
                    R.id.dialog_deactivated_user_message);
            MyView.setTextViewByDeviceSize(context, etShopName, etRedeemCode);

            setUI();
        }

        abstract public void setUI();

        @Override
        public int getThisView() {
            return R.layout.dialog_deactivated_user;
        }

        protected void addWorkingShops() {
            if (userSettingData.getAllowedShops().size() >= 1) {
                workingShopsContainer.setVisibility(View.VISIBLE);
                for (final String phoneNumber : userSettingData.getAllowedShops()) {
                    addButton(String.format("[%s] 연결", BaseUtils.toPhoneFormat(phoneNumber)), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            userSettingData.updateWorkingShopPhoneNumber(phoneNumber, new MyOnTask() {
                                @Override
                                public void onTaskDone(Object result) {
                                    MyUI.toastSHORT(context, String.format("%s로 연결했습니다. 앱을 재시작 합니다", phoneNumber));
                                    finishApp(500, true);
                                }
                            });
                        }
                    });
                }
            } else {
                workingShopsContainer.setVisibility(View.GONE);
            }
        }

        public void addButton(String text, View.OnClickListener onClick) {
            LinearLayout.LayoutParams params = MyView.getParams(context, MyView.PARENT, MyView.CONTENT);
            params.setMargins(0, MyDevice.toPixel(context, marginTopDp), 0, 0);
            Button button = new Button(context);
            button.setText(text);
            button.setLayoutParams(params);
            button.setOnClickListener(onClick);
            workingShopsContainer.addView(button);
            MyView.setTextViewByDeviceSize(context, button);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_deactivated_user_contactUs) {
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);

            } else if (view.getId() == R.id.dialog_deactivated_user_logout) {
                new MyAlertDialog(context, "로그 아웃", "모든 앱에서 로그 아웃을 하시겠습니까? 저장된 아이디와 비밀번호가 삭제됩니다") {
                    @Override
                    public void yes() {
                        myDB.logOut();
                        finishAffinity();
                    }
                };

            } else if (view.getId() == R.id.dialog_deactivated_user_close) {
                finishApp(500, false);

            } else if (view.getId() == R.id.dialog_deactivated_user_send_redeem) {
                if (Empty.isEmpty(context, etShopName, "매장명이 입력되지 않았습니다")) return;
                if (Empty.isEmpty(context, etRedeemCode, "정품키가 입력되지 않았습니다")) return;
                activateWithRedeemCode();

            } else if (view.getId() == R.id.dialog_deactivated_to_buyer) {
                new UserConfigTypeDialog(context) {
                    @Override
                    public void onApplyClick(UserConfigurationType userConfigurationType) {
                        dismiss();
                        MyUI.toastSHORT(context, String.format("%s 정보 적용을 위해 앱을 재시작 합니다", userConfigurationType.uiStr));
                        finishApp(500, true);
                    }
                };
            }
        }

        private void activateWithRedeemCode() {
            new CommonServiceTask(context, "activateWithRedeemCode") {
                ServiceActivationResult result;

                @Override
                public void doInBackground() throws MyServiceFailureException {
                    result = workingShopService.activateWithRedeemCode(userSettingData.getUserShopInfo(),
                            serviceType, etRedeemCode.getText().toString().trim(), etShopName.getText().toString());
                }

                @Override
                public void onPostExecutionUI() {
                    if (result.isSuccess()) {
                        MyUI.toast(context, String.format("인증 되셨습니다. 유료 기능을 사용하실 수 있습니다"));
                        finishApp(500, true);
                    } else {
                        MyUI.toast(context, String.format("%s", result.getError().getUiStr()));
                    }
                }

                @Override
                public void catchException(MyServiceFailureException e) {
                    super.catchException(e);
                }
            };
        }

        protected void finishApp(int delayMs, boolean doRestart) {
            new MyDelayTask(context, delayMs) {
                @Override
                public void onFinish() {
                    dismiss();
                    ((Activity) context).finishAffinity();
                }
            };

            if (doRestart) {
                new MyDelayTask(context, delayMs + 500) {
                    @Override
                    public void onFinish() {
                        MyDevice.openApp(context, context.getPackageName());
                    }
                };
            }
        }

    }

}
