package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.protocol.PaymentTermsProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author lucidite
 */

public class PaymentTermsJSONData implements PaymentTermsProtocol, JSONDataWrapper {
    private JSONObject data;

    public PaymentTermsJSONData(JSONObject dataObj) {
        if (dataObj == null) {
            this.data = new JSONObject();
        } else {
            this.data = dataObj;
        }
    }

    public PaymentTermsJSONData(PaymentTermsProtocol paymentTerms) throws JSONException {
        if (paymentTerms.getClass().equals(PaymentTermsJSONData.class)) {
            this.data = ((PaymentTermsJSONData) paymentTerms).getObject();
        } else {
            this.build(paymentTerms.getOnlinePayment(), paymentTerms.getEntrustedPayment(),
                    paymentTerms.getOnlinePaymentClearedDate(), paymentTerms.getEntrustedPaymentClearedDate());
        }
    }

    public PaymentTermsJSONData(int online, int entrusted, String onlineClearedDate, String entrustedClearedDate) throws JSONException {
        this.build(online, entrusted, onlineClearedDate, entrustedClearedDate);
    }

    private void build(int online, int entrusted, String onlineClearedDate, String entrustedClearedDate) throws JSONException {
        this.data = new JSONObject();
        if (online != 0) {
            this.data.put("o", online);
            if (onlineClearedDate != null && !onlineClearedDate.isEmpty()) {
                JSONObject clearanceData = new JSONObject();
                clearanceData.put("d", onlineClearedDate);
                this.data.put("oc", clearanceData);
            }
        }
        if (entrusted != 0) {
            this.data.put("e", entrusted);
            if (entrustedClearedDate != null && !entrustedClearedDate.isEmpty()) {
                JSONObject clearanceData = new JSONObject();
                clearanceData.put("d", entrustedClearedDate);
                this.data.put("ec", clearanceData);
            }
        }
    }

    @Override
    public int getOnlinePayment() {
        return data.optInt("o", 0);
    }

    @Override
    public int getEntrustedPayment() {
        return data.optInt("e", 0);
    }

    @Override
    public boolean isOnlinePaymentCleared() {
        if (this.getOnlinePayment() == 0) {     // 값이 0인 경우 당연완료 (별도 플래그 없음)
            return true;
        }
        return data.has("oc");
    }

    @Override
    public boolean isEntrustedPaymentCleared() {
        if (this.getEntrustedPayment() == 0) {  // 값이 0인 경우 당연완료 (별도 플래그 없음)
            return true;
        }
        return data.has("ec");
    }

    @Override
    public String getOnlinePaymentClearedDate() {
        if (this.getOnlinePayment() == 0) {
            return "";
        }

        JSONObject clearanceData = data.optJSONObject("oc");
        if (clearanceData == null) {
            return "";
        }
        return clearanceData.optString("d", "");
    }

    @Override
    public String getEntrustedPaymentClearedDate() {
        if (this.getEntrustedPayment() == 0) {
            return "";
        }

        JSONObject clearanceData = data.optJSONObject("ec");
        if (clearanceData == null) {
            return "";
        }
        return clearanceData.optString("d", "");
    }

    @Override
    public boolean isPaymentCleared() {
        return this.isOnlinePaymentCleared() && this.isEntrustedPaymentCleared();
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return this.data;
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("PaymentTermsJSONData instance does not have a JSON array");
    }
}
