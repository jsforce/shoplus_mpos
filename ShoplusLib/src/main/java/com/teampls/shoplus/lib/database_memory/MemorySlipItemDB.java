package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-04-01.
 */

public class MemorySlipItemDB extends BaseListDB<SlipItemRecord> {

    protected MemorySlipItemDB(Context context, String dbName) {
        super(context, dbName);
    }

    @Override
    protected SlipItemRecord getEmptyRecord() {
        return new SlipItemRecord();
    }

    public void updateOrInsert(String slipKey, List<SlipItemRecord> records) {
        deleteBy(slipKey);
        insertAll(records);
    }

    public List<SlipItemRecord> getRecordsBy(String slipKey) {
        List<SlipItemRecord> results = new ArrayList<>();
        for (SlipItemRecord slipItemRecord : getRecords()) {
            if (slipItemRecord.slipKey.equals(slipKey))
                results.add(slipItemRecord);
        }
        return results;
    }

    public List<SlipItemRecord> getRecordsBy(List<SlipRecord> slipRecords) {
        List<String> slipKeys = new ArrayList<>();
        for (SlipRecord slipRecord : slipRecords)
            slipKeys.add(slipRecord.getSlipKey().toSID());

        List<SlipItemRecord> results = new ArrayList<>();
        for (SlipItemRecord slipItemRecord : getRecords()) {
            if (slipKeys.contains(slipItemRecord.slipKey))
                results.add(slipItemRecord);
        }
        return results;
    }

    public void deleteBy(String slipKey) {
        List<SlipItemRecord> invalidRecords = getRecordsBy(slipKey);
        deleteAll(invalidRecords);
    }

    public void insertPendings(List<SlipItemRecord> records) {
        for (SlipItemRecord record : records)
            if (record.slipItemType.isPending())
                insert(record);
    }

    public List<SlipItemRecord> getRecordsIN(List<String> slipKeys) {
        List<SlipItemRecord> results = new ArrayList<>();
        for (SlipItemRecord record : getRecords())
            if (slipKeys.contains(record.slipKey))
                results.add(record);
        return results;
    }

    public MyListMap<ItemOptionRecord, SlipItemRecord> getOptionSlipsMap() {
        MyListMap<ItemOptionRecord, SlipItemRecord> results = new MyListMap<>();
        for (SlipItemRecord record : getRecords()) {
            results.add(record.toOption(), record);
        }
        return results;
    }

    @Override
    public void toLogCat(String callLocation) {
        for (SlipItemRecord record : getRecords())
            record.toLogCat(callLocation);
    }

    public SlipSummaryRecord getSummary(String slipKey) {
        return new SlipSummaryRecord(getRecordsBy(slipKey));
    }

    public int getNextSerial(String slipKey) {
        int serial = 0;
        for (SlipItemRecord record : getRecordsBy(slipKey)) {
            if (record.serial != serial) {
                record.serial = serial;
                update(record);
            }
            serial++;
        }
        return serial;
    }
}
