package com.teampls.shoplus.lib.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 매장관리정책 중 잔금변경방식
 *
 * @author lucidite
 */
public enum BalanceChangeMethodType {
    ALL("all", "전체"),
    TRANSACTION_CASE("tr_case", "버튼식"),
    DELTA("delta", "은행식");

    private String remoteStr;
    private String uiStr;

    BalanceChangeMethodType(String remoteStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
    }

    public String toRemoteStr() {
        return remoteStr;
    }

    public String toUiStr() {
        return uiStr;
    }

    public static BalanceChangeMethodType fromRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty()) {
            return ALL;
        }
        for (BalanceChangeMethodType type: BalanceChangeMethodType.values()) {
            if (remoteStr.equals(type.toRemoteStr())) {
                return type;
            }
        }
        return ALL;
    }

    public static List<String> toStringValues() {
        List<String> results = new ArrayList<>();
        for (BalanceChangeMethodType type : values())
            results.add(type.toString());
        return results;
    }
}
