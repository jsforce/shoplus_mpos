package com.teampls.shoplus.lib;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.ProjectTransactionServiceProtocol;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectTransactionService;

/**
 * @author lucidite
 */
public class ProjectTransactionServiceManager {
    public static ProjectTransactionServiceProtocol defaultService(Context context) {
        return new ProjectTransactionService();
    }
}
