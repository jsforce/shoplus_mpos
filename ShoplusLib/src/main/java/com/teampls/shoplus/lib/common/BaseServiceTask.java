package com.teampls.shoplus.lib.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.teampls.shoplus.lib.ProjectAccountServiceManager;
import com.teampls.shoplus.lib.ProjectAdminServiceManager;
import com.teampls.shoplus.lib.ProjectItemServiceManager;
import com.teampls.shoplus.lib.ProjectMainUserServiceManager;
import com.teampls.shoplus.lib.ProjectMasterUserServiceManager;
import com.teampls.shoplus.lib.ProjectSpecificUserServiceManager;
import com.teampls.shoplus.lib.ProjectTransactionServiceManager;
import com.teampls.shoplus.lib.ProjectUserServiceManager;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ProjectAccountServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectAdminServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectItemServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectMainUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectMasterUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectSpecificUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectTransactionServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ServiceErrorHelper;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.awsservice.handler.BaseAuthenticationHandler;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.dialog.MyProgressDialog;
import com.teampls.shoplus.lib.enums.LoginState;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.services.chatbot.ProjectChatbotServiceManager;
import com.teampls.shoplus.lib.services.chatbot.ProjectChatbotServiceProtocol;
import com.teampls.shoplus.lib.services.chatbot.awsservices.ProjectChatbotService;
import com.teampls.shoplus.undercity.ProjectUndercityServiceManager;
import com.teampls.shoplus.undercity.ProjectUndercityServiceProtocol;

/**
 * Created by Medivh on 2017-01-27.
 */

abstract public class BaseServiceTask {
    protected Context context;
    private MyProgressDialog myProgressDialog, otherProgressDialog;
    protected KeyValueDB keyValueDB;
    protected MyDB myDB;
    protected GlobalDB globalDB;
    private boolean doExecuteOnCreateFinish = false;
    protected String callLocation = "";
    protected boolean autoLoginActivated = false;
    public ProjectAccountServiceProtocol accountService;
    public ProjectAdminServiceProtocol adminService;
    public ProjectItemServiceProtocol itemService;
    public ProjectTransactionServiceProtocol transactionService;
    public ProjectMasterUserServiceProtocol masterService;

    public ProjectMainUserServiceProtocol mainShopService; // main shop 설정 (샵별 공통 정보)
    public ProjectUserServiceProtocol workingShopService; // sub shop 설정 (샵별 차별 정보)
    public ProjectSpecificUserServiceProtocol userService;
    public ProjectChatbotServiceProtocol chatbotService;
    public ProjectUndercityServiceProtocol orderService;

    public BaseServiceTask(final Context context, final String callLocation, final String progressMessage) {
        this(context, callLocation);
        if (TextUtils.isEmpty(progressMessage) == false && MyUI.isActiveActivity(context,"") && otherProgressDialog == null) {
            otherProgressDialog = new MyProgressDialog(context);
            otherProgressDialog.setMessage(progressMessage);
            otherProgressDialog.show();
        }
    }

    private void dismissProgress() {
        if (otherProgressDialog != null)
            otherProgressDialog.dismiss();
        if (myProgressDialog != null)
            myProgressDialog.dismiss();
    }

    protected void setProgressMessage(String message) {
        if (otherProgressDialog == null)
            return;
        MyUI.setMessage(context, otherProgressDialog, message);
    }

    public BaseServiceTask(final Context context, final String callLocation) {
        autoLoginActivated = false;
        if (context == null) return;
        this.context = context;
        this.callLocation = callLocation;
        keyValueDB = KeyValueDB.getInstance(context);
        myDB = MyDB.getInstance(context);
        globalDB = GlobalDB.getInstance(context);

        if (MyUI.isActivity(context, "BaseServiceTask") == false) {
            dismissProgress();
            return;
        }

        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myProgressDialog = new MyProgressDialog(context);
                setCommonService();
                setService();
                if (BaseAppWatcher.isLoggedIn() == false) {
                    Log.w("DEBUG_JS", String.format("[%s] isLoggedIn == false ", callLocation));
                    MyUI.toastSHORT(context, String.format("현재 로그인 중입니다. 잠시 후 사용해 주세요"));
                    dismissProgress();
                    return;
                } else {
                    if (BaseAppWatcher.isTokenExpired(context, callLocation)) {
                        Log.w("DEBUG_JS", String.format("[%s] isToken expired", callLocation));
                        autoLogin(true);
                    } else {
                        onCreateFinish();
                    }
                }
            }
        });
    }


    private void setCommonService() {
        accountService = ProjectAccountServiceManager.defaultService(context);
        adminService = ProjectAdminServiceManager.defaultService(context);
        itemService = ProjectItemServiceManager.defaultService(context);
        mainShopService = ProjectMainUserServiceManager.defaultService(context);
        workingShopService = ProjectUserServiceManager.defaultService(context);
        userService = ProjectSpecificUserServiceManager.defaultService(context);
        transactionService = ProjectTransactionServiceManager.defaultService(context);
        masterService = ProjectMasterUserServiceManager.defaultService(context);
        chatbotService = ProjectChatbotServiceManager.defaultService(context);
        orderService = ProjectUndercityServiceManager.defaultService(context);
    }

    abstract public void setService();

    private void onCreateFinish() {
        onPreExecute();
        execute();
    }

    public void onPreExecute() {
        //
    }

    abstract public void doInBackground() throws MyServiceFailureException;

    public void onPostExecutionUI() {
        if (myProgressDialog != null)
            myProgressDialog.dismiss();
    }

    private void execute() {
        new Thread() {
            public void run() {
                try {
                    doInBackground();
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            dismissProgress();
                            onPostExecutionUI();
                        }
                    });
                } catch (final MyServiceFailureException e) {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            dismissProgress();
                            catchException(e);
                        }
                    });
                } catch (Exception e) {
                    dismissProgress();
                    MyUI.toastSHORT(context, String.format("에러 (%s)", e.getLocalizedMessage().toString()));
                    Log.e("DEBUG_JS", String.format("[%s] %s, msg: %s", callLocation, e.getClass().toString(), e.getLocalizedMessage()));
                }
            }
        }.start();
    }

    public void catchException(MyServiceFailureException e) {
        Log.e("DEBUG_JS", String.format("[%s] %s, code: %s, msg: %s", context.getClass().getSimpleName(), callLocation, e.getErrorCode(), e.getLocalizedMessage()));
        String message = e.getLocalizedMessage();
        if (message.contains("Identity token has expired") || message.contains("Token expired")) {
            MyUI.toast(context, "로그인 기간이 만료되었습니다. 자동 로그인을 하는 동안 잠시만 기다려주세요");
            autoLogin(false);
        } else if (message.contains("Task timed out") || message.contains("timeout")) {
            MyUI.toastSHORT(context, String.format("통신 상태가 잠시 좋지 않습니다. 잠시 후 다시 실행해 주세요"));

        } else if (message.contains("Access Denied")) {
            MyUI.toastSHORT(context, String.format("기능 사용 권한이 없습니다. 매장 관리자나 고객지원으로 문의주세요"));

        } else if (e.getErrorCode().equals("FileNotFoundException") && message.startsWith("https://")) {
            MyUI.toastSHORT(context, String.format("요청 경로를 확인해 주세요"));

        } else {
            MyUI.toast(context, ServiceErrorHelper.getMyServiceFailureMessageForUser(e, true));
        }
        if (myProgressDialog != null) {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    myProgressDialog.dismiss();
                }
            });
        }
    }

    private void autoLogin(boolean doExecuteOnCreateFinish) {
        autoLoginActivated = true;
        BaseAppWatcher.setLoginState(context, LoginState.onTrying);
        this.doExecuteOnCreateFinish = doExecuteOnCreateFinish;
        if (myProgressDialog != null) {
            myProgressDialog.setMessage("로그인 중...");
            MyUI.show(context, myProgressDialog);
        }
        UserHelper.getPool().getUser(myDB.getUserPoolNumber()).signOut();
        UserHelper.getPool().getUser(myDB.getUserPoolNumber()).getSessionInBackground(new MyAuthenticationHandler(context));
    }


    class MyAuthenticationHandler extends BaseAuthenticationHandler {

        public MyAuthenticationHandler(Context context) {
            super(context);
        }

        @Override
        public void onNotAuthorized() {

        }

        @Override
        public void onTaskDone(Object result) {
            if (myProgressDialog != null)
                myProgressDialog.dismiss();
            MyUI.toastSHORT(context, String.format("로그인을 했습니다"));
            UserSettingData userSettingData = UserSettingData.getInstance(context);
            if (userSettingData == null) {
                userSettingData.update(new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        if (doExecuteOnCreateFinish)
                            onCreateFinish();
                    }
                });
            } else {
                if (doExecuteOnCreateFinish)
                    onCreateFinish();
            }
        }

        @Override
        public void onFailure(Exception exception) {
            if (myProgressDialog != null)
                myProgressDialog.dismiss();
        }
    }
}
