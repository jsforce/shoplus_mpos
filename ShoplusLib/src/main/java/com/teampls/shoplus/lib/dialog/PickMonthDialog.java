package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.MyStringAdapter;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.event.MyOnClick;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-12-20.
 */

public class PickMonthDialog extends BaseDialog implements AdapterView.OnItemClickListener {
    private ListView listView;
    private MyStringAdapter adapter;
    private MyOnClick<DateTime> onRowClick;
    private String thisMonthTail = " (이번달)";

    public PickMonthDialog(Context context, int year, int month, MyOnClick<DateTime> onRowClick) {
        this(context, year, month, BaseUtils.createMonthDateTime().minusYears(3), onRowClick);
    }

    public PickMonthDialog(Context context, int year, int month, DateTime fromDate, MyOnClick<DateTime> onRowClick) {
        super(context);
        
        this.onRowClick = onRowClick;
        adapter = new MyStringAdapter(context);

        DateTime now = DateTime.now();
        List<String> monthStrings = new ArrayList<>();
        int selectionIndex = 35;
        for (int index = 0 ; index < 42; index++) {
            DateTime dateTime = fromDate.plusMonths(index);
            if (dateTime.getYear() == year && dateTime.getMonthOfYear() == month)
                selectionIndex = index;

            if (dateTime.getYear() == now.getYear() && dateTime.getMonthOfYear() == now.getMonthOfYear()) {
                monthStrings.add(dateTime.toString(BaseUtils.YM_Kor_Format) + thisMonthTail );
            } else {
                monthStrings.add(dateTime.toString(BaseUtils.YM_Kor_Format));
            }

        }
        adapter.setRecords(monthStrings);

        listView = findViewById(R.id.dialog_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);


        findTextViewById(R.id.dialog_list_title, "월 선택");
        findViewById(R.id.dialog_list_message).setVisibility(View.GONE);

        setOnClick(R.id.dialog_list_close);
        show();

        final int position = selectionIndex;

        new MyAsyncTask(context) {
            @Override
            public void onPostExecutionUI() {
                listView.setSelectionFromTop(position, listView.getHeight()/2);
            }
        };
    }


    @Override
    public int getThisView() {
        return R.layout.dialog_list;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_list_close) {
            dismiss();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        DateTime selectedMonth = BaseUtils.toDateTime(adapter.getRecord(i).replace(thisMonthTail,""), BaseUtils.YM_Kor_Format);
        if (onRowClick != null)
            onRowClick.onMyClick(view, selectedMonth);
        dismiss();
    }

}
