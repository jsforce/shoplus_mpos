package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-02-18.
 */

public class MyMultiRadioGroup<T> implements View.OnClickListener {
    public List<RadioButton> radioButtons = new ArrayList<>();
    public RadioButton currentRadioButton;
    private View.OnClickListener onClickListener;
    private View view;
    private List<T> defaultT;
    private double scale = 1.0;
    private Context context;

    public MyMultiRadioGroup(Context context, View view, double scale) {
        this.context = context;
        this.view = view;
        this.scale = scale;
    }

    public void setAsNormalFont(Context context) {
        for (RadioButton radioButton : radioButtons)
            MyView.setAsNormalFont(context, (TextView) radioButton);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public MyMultiRadioGroup add(int resId, T... tag) {
        return add((RadioButton) view.findViewById(resId), tag);
    }

    public void clear() {
        radioButtons.clear();
        // currentRadioButton
    }

    public MyMultiRadioGroup add(RadioButton radioButton, T... tags) {
        radioButton.setOnClickListener(this);
        List<T> results = new ArrayList<>();
        for (T tag : tags)
            results.add(tag);
        radioButton.setTag(results);
        radioButton.setScaleX((float) scale);
        radioButton.setScaleY((float) scale);
        MyView.setTextViewByDeviceSize(context, radioButton);
        radioButtons.add(radioButton);
        return this;
    }

    @Override
    public void onClick(View view) {
        if (view instanceof RadioButton == false) return;
        currentRadioButton = (RadioButton) view;
        for (RadioButton button : radioButtons)
            button.setChecked(button == currentRadioButton);
        if (onClickListener != null)
            onClickListener.onClick(view);
    }

    public void setChecked(T tag) {
        for (RadioButton button : radioButtons) {
            List<T> tags = (List<T>) button.getTag();
            if (tags.contains(tag)) {
                button.setChecked(true);
                currentRadioButton = button;
            } else {
                button.setChecked(false);
            }
        }
    }

    public List<T> getCheckedItem() {
        for (RadioButton button : radioButtons) {
            if (button.isChecked())
                return (List<T>) button.getTag();
        }
        return new ArrayList<T>();
    }

    public RadioButton getCheckedButton() {
        for (RadioButton button : radioButtons) {
            if (button.isChecked())
                return button;
        }
        if (radioButtons.size() == 0)
            return null;
        else
            return radioButtons.get(0);
    }

}
