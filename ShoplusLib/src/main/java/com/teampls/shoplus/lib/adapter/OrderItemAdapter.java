package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyImageLoader;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.OrderItemRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.datatypes.ItemOptionData;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.event.RowEventHandler;
import com.teampls.shoplus.lib.view.MyNumberPlusMinus;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2017-06-10.
 */

public class OrderItemAdapter extends BaseDBAdapter<OrderItemRecord> {
    public static final int OrderCreationView = 0, OrderSharingView = 1;
    private RowEventHandler handler;
    private MyImageLoader imageLoader;
    private int textViewHeightDp = 22, marginTopOrdersDp = 3, marginBottomOrdersDp = 10;
    private Map<Integer, Integer> rowHeightsDp = new HashMap<>();
    private int imageSizePx = 180;

    // creation
    public OrderItemAdapter(Context context, RowEventHandler handler) {
        super(context);
        this.handler = handler;
        imageLoader = MyImageLoader.getInstance(context);

    }

    // sharing
    public OrderItemAdapter(Context context) {
        this(context, null);
        imageSizePx = (MyDevice.getWidth(context) / MyDevice.getOrderingColumnNum(context));
    }

    @Override
    public BaseViewHolder getViewHolder() {
        switch (viewType) {
            default:
            case OrderCreationView:
                return new OrderCreationViewHolder();
            case OrderSharingView:
                return new OrderSharingViewHolder();
        }
    }

    public void setRowHeights() {
        int position = 0;

        int columnNum = MyDevice.getOrderingColumnNum(context);
        for (OrderItemRecord record : records) {
            int rowPosition = position / columnNum;
            int cellHeightDp = MyDevice.toDP(context, imageSizePx) + marginTopOrdersDp + textViewHeightDp * record.slipItems.size() + marginBottomOrdersDp;
            if (position % columnNum == 0) { // row 시작
                rowHeightsDp.put(rowPosition, cellHeightDp);
            } else { // 왼쪽
                if (cellHeightDp > rowHeightsDp.get(rowPosition))
                    rowHeightsDp.put(rowPosition, cellHeightDp);
            }
            position++;
        }

        //   for (int row : rowHeightsDp.keySet()) Log.w("DEBUG_JS", String.format("[OrderItemAdapter.setRowHeights] %d, %d", row, rowHeightsDp.get((Integer) row)));
    }

    public int getHeightDp() {
        switch (viewType) {
            default:
                return 0;
            case OrderSharingView:
                int result = 0;
                for (Integer rowPosition : rowHeightsDp.keySet())
                    result = result + rowHeightsDp.get(rowPosition);
                return result;
        }
    }

    @Override
    public void generate() {
    }

    public void setRecordsByBinding(List<OrderItemRecord> orderItemRecords) {
        // itemId, color, size --> itemId + sub (color-size) 로 변경
        // 단 itemId == 0 인 것들은 무조건 별도로 처리
        // OrderItemRecord : itemId, name만 유의미함
        List<OrderItemRecord> sharingRecords = new ArrayList<>(); // itemId별
        for (OrderItemRecord inputRecord : orderItemRecords) {
            // sub로 추가될 항목 생성 (color - size - 주문개수)
            SlipItemRecord slipItemRecord = new SlipItemRecord(inputRecord.color, inputRecord.size, inputRecord.finalOrder);
            slipItemRecord.quantity = inputRecord.finalOrder; // 반드시 finalOrder이어야 함
            if (slipItemRecord.quantity <= 0)
                continue;

            boolean found = false;
            for (OrderItemRecord sharingRecord : sharingRecords) {
                if ((inputRecord.itemId > 0) && (sharingRecord.itemId == inputRecord.itemId)) {
                    found = true;
                    sharingRecord.addSlipItem(slipItemRecord);
                    break;
                }
            }
            if (found == false) {
                OrderItemRecord orderItemRecord = new OrderItemRecord();
                orderItemRecord.itemId = inputRecord.itemId;
                orderItemRecord.name = inputRecord.name;
                orderItemRecord.addSlipItem(slipItemRecord); // sub항목 추가
                sharingRecords.add(orderItemRecord);
            }
        }

        records = sharingRecords;
    }


    public void updateStocks(MyMap<ItemOptionRecord, Integer> stockMap) {
        for (OrderItemRecord record : getRecords())
            record.stock = stockMap.get(record.toOptionRecord());
    }

    public void setRecordsFromSlipItemsWithStockMap(List<SlipItemRecord> slipItemRecords,MyMap<ItemOptionRecord, Integer> stockMap) {

        // itemId, color, size가 같은 것끼리 모아야 함, OrderItemRecord.slipItems에 원본들을 모은다
        List<OrderItemRecord> _records = new ArrayList<>();
        for (SlipItemRecord slipItemRecord : slipItemRecords) {
            boolean found = false;
            for (OrderItemRecord _record : _records) {
                if (_record.isBundleOf(slipItemRecord)) {
                    found = true;
                    _record.addSlipItem(slipItemRecord);
                    break;
                }
            }
            if (found == false)
                _records.add(new OrderItemRecord(slipItemRecord, Math.max(0, stockMap.get(slipItemRecord.toOption()))));
        }
        records = _records;
        Collections.sort(records);
    }

    public void setRecordsFromSlipItems(List<SlipItemRecord> slipItemRecords, MyMap<ItemStockKey, ItemOptionData> itemMap) {

        // itemId, color, size가 같은 것끼리 모아야 함
        List<OrderItemRecord> _records = new ArrayList<>();
        for (SlipItemRecord slipItemRecord : slipItemRecords) {
            boolean found = false;
            for (OrderItemRecord _record : _records) {
                if (_record.isBundleOf(slipItemRecord)) {
                    found = true;
                    _record.addSlipItem(slipItemRecord);
                    break;
                }
            }
            if (found == false) {
                ItemOptionRecord optionRecord = new ItemOptionRecord(slipItemRecord.itemId, slipItemRecord.color, slipItemRecord.size);
                ItemOptionData optionData = itemMap.get(optionRecord.toStockKey());
                int stock = BaseUtils.toInt(optionData.getStock());
                _records.add(new OrderItemRecord(slipItemRecord, stock));

//                if (!itemMap.isEmpty() && itemMap.containsKey(optionRecord.toStockKey()) == false) {
//                    optionRecord.toLogCat("e.setRecords");
//                    MyUI.toastSHORT(context, String.format("삭제확인필요 : %s", optionRecord.toStringWithName(slipItemRecord.name, "/")));
//                }
            }

        }
        records = _records;
        Collections.sort(records);
    }

    class OrderSharingViewHolder extends BaseViewHolder {
        private TextView tvNum, tvName, tvNoImage;
        private ImageView imageView;
        private LinearLayout container, finalOrders;
        private int columnNum = 2;

        @Override
        public int getThisView() {
            return R.layout.cell_order;
        }

        @Override
        public void init(View view) {
            tvNum = findTextViewById(context, view, R.id.cell_order_num);
            tvName = findTextViewById(context, view, R.id.cell_order_name);
            tvNoImage = findTextViewById(context, view, R.id.cell_order_noImage);
            imageView = setView(view, R.id.cell_order_image);
            container = setView(view, R.id.cell_order_container);
            container.getLayoutParams().width = imageSizePx;
            imageView.getLayoutParams().width = imageSizePx;
            imageView.getLayoutParams().height = imageSizePx;
            tvName.getLayoutParams().width = imageSizePx;
            finalOrders = setView(view, R.id.cell_order_finalOrders);
            columnNum = MyDevice.getOrderingColumnNum(context);
        }

        @Override
        public void update(int position) {
            OrderItemRecord record = records.get(position);
            tvNum.setText(Integer.toString(position + 1));
            tvName.setText(record.name);
            finalOrders.removeAllViews();
            container.getLayoutParams().height = MyDevice.toPixel(context, rowHeightsDp.get(position / columnNum));

            for (SlipItemRecord slipItemRecord : record.slipItems) {
                // order row
                LinearLayout rowContainer = MyView.createContainer(context, LinearLayout.HORIZONTAL, MyView.PARENT, textViewHeightDp);
                rowContainer.setGravity(Gravity.CENTER_VERTICAL);

                // oder color
                LinearLayout imageContainer = MyView.createContainer(context, LinearLayout.HORIZONTAL, textViewHeightDp - 2, textViewHeightDp - 2);
                imageContainer.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                imageContainer.setBackgroundResource(R.drawable.background_box);
                ((LinearLayout.LayoutParams) imageContainer.getLayoutParams()).setMargins(0, 0, MyDevice.toPixel(context, 5), 0);

                ImageView imageView = new ImageView(context);
                imageView.setLayoutParams(MyView.getParams(context, textViewHeightDp - 4, textViewHeightDp - 4));
                imageView.setBackgroundColor(slipItemRecord.color.colorInt);
                imageContainer.addView(imageView);
                rowContainer.addView(imageContainer); // color 체크박스에 공간을 두려고 imageContainer를 사용

                // order text
                String optionString = slipItemRecord.toOptionString(":");
                String message = String.format("%s%s%d개", optionString, optionString.isEmpty() ? "" : " .. ", slipItemRecord.quantity);
                TextView textView = MyView.createTextView(context, message, 16, 0, ColorType.Black.colorInt);
                textView.setGravity(Gravity.LEFT);
                rowContainer.addView(textView);
                finalOrders.addView(rowContainer);
            }

            if (record.itemId > 0) {
                imageLoader.retrieveThumb(userSettingData.getProviderRecord().phoneNumber, record.itemId, new MyOnTask<Bitmap>() {
                    @Override
                    public void onTaskDone(Bitmap result) {
                        imageView.setImageBitmap(result);
                        tvNoImage.setVisibility(Empty.isEmpty(result) ? View.VISIBLE : View.GONE);
                    }
                });
            } else {
                imageView.setImageBitmap(Empty.bitmap);
                tvNoImage.setVisibility(View.VISIBLE);
            }
        }
    }

    class OrderCreationViewHolder extends BaseViewHolder {
        private TextView tvSerial, tvName, tvOption, tvOrder, tvStock, tvFinalOrder;
        private LinearLayout dateContainer, nameContainer, container;
        private View divider;
        private MyNumberPlusMinus numberPlusMinus;

        @Override
        public int getThisView() {
            return R.layout.row_order;
        }

        @Override
        public void init(View view) {
            if (handler == null)
                Log.e("DEBUG_JS", String.format("[OrderCreationViewHolder.initBase] handler == null"));
            container = (LinearLayout) view.findViewById(R.id.row_order_container);
            tvSerial = (TextView) view.findViewById(R.id.row_order_serial);
            tvName = (TextView) view.findViewById(R.id.row_order_name);
            tvOption = (TextView) view.findViewById(R.id.row_order_option);
            tvOrder = (TextView) view.findViewById(R.id.row_order_orderCount);
            tvStock = (TextView) view.findViewById(R.id.row_order_stockCount);
            tvFinalOrder = (TextView) view.findViewById(R.id.row_order_finalOrder);
            tvFinalOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OrderItemRecord record = records.get(position);
                    record.finalOrder = BaseUtils.toInt(tvFinalOrder.getText().toString());
                    handler.onTouch1Click(position, record);
                }
            });
            divider = view.findViewById(R.id.row_order_divider);
            dateContainer = (LinearLayout) view.findViewById(R.id.row_order_dateContainer);
            dateContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handler.onRowClick(position, records.get(position));
                }
            });
            nameContainer = (LinearLayout) view.findViewById(R.id.row_order_nameContainer);
            nameContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handler.onRowClick(position, records.get(position));
                }
            });
            numberPlusMinus = new MyNumberPlusMinus(view, 0);
            numberPlusMinus.setTextView(R.id.row_order_finalOrder);
            numberPlusMinus.setPlusMinusButtons(R.id.row_order_plus, R.id.row_order_minus);
            numberPlusMinus.setOnClick(new MyOnClick<Integer>() {
                @Override
                public void onMyClick(View view, Integer finalOrder) {
                    OrderItemRecord record = records.get(position);
                    record.finalOrder = finalOrder;
                    if (record.finalOrder > 0) {
                        container.setBackgroundColor(ColorType.Khaki.colorInt);
                    } else {
                        container.setBackgroundColor(ColorType.Trans.colorInt);
                    }
                }
            });
        }

        @Override
        public void update(int position) {
            this.position = position;
            OrderItemRecord record = records.get(position);
            if (position == 0) {
                divider.setVisibility(View.VISIBLE);
            } else {
                if (record.itemId != records.get(position - 1).itemId) {
                    divider.setVisibility(View.VISIBLE);
                } else {
                    divider.setVisibility(View.GONE);
                }
            }

            tvSerial.setText(String.format("%d", position + 1));
            tvName.setText(record.name);
            if (record.itemId == 0)
                tvName.setText("*" + record.name);
            tvOption.setText(record.toOptionString(" : ", false));
            tvOrder.setText(String.format("주문:%d", record.quantity));
            tvStock.setText(String.format("재고:%d", record.stock));
            tvFinalOrder.setText(String.format("%d", record.finalOrder));

            if (record.finalOrder > 0) {
                container.setBackgroundColor(ColorType.Khaki.colorInt);
            } else {
                container.setBackgroundColor(ColorType.Trans.colorInt);
            }

            if (record.stock > 0)
                tvStock.setVisibility(View.VISIBLE);
            else
                tvStock.setVisibility(View.INVISIBLE);
        }
    }
}
