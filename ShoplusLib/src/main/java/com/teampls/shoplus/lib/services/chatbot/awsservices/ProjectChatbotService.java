package com.teampls.shoplus.lib.services.chatbot.awsservices;

import android.util.Pair;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.ContactJSONData;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;
import com.teampls.shoplus.lib.services.chatbot.ProjectChatbotServiceProtocol;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotBuyerBaseData;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotBuyerRegistrationRequestData;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotOrderData;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotSettingData;
import com.teampls.shoplus.lib.services.chatbot.datatypes.NonProcessedUtteranceData;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderState;
import com.teampls.shoplus.lib.services.chatbot.enums.OrderRejectReason;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotBuyerBaseDataProtocol;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotBuyerRegistrationRequestDataProtocol;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotOrderDataProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * ProjectChatbotServiceProtocol에 대한 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectChatbotService implements ProjectChatbotServiceProtocol {

    private String getMyBuyerRegistrationRequestsResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "chatbot", "user-auth", "reg-requests");
    }

    private String getABuyerRegistrationRequestResourcePath(String phoneNumber, ChatbotBuyerRegistrationRequestDataProtocol request) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "chatbot", "user-auth", request.getUserKey());
    }

    private String getMyBuyersResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "chatbot", "buyers");
    }

    private String getMyRecentChatbotOrdersResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "chatbot", "orders", "period", "recent");
    }

    private String getAChatbotOrderResourcePath(String phoneNumber, String orderId) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "chatbot", "orders", orderId);
    }

    private String getMyNewArrivalItemIdsResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "chatbot", "new-arrivals");
    }

    private String getRecentNonProcessedUtterancesResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "chatbot", "nonprocessed", "period", "recent");
    }

    private String getNonProcessedUtteranceProcessingPath(String phoneNumber, String utteranceId) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "chatbot", "nonprocessed", utteranceId, "processing");
    }

    // Chatbot 관리 시에는 매장 정보를 사용한다 (멀티샵은 현재 지원하지 않음 - 부수적 문제들 선결 필요)
    private String getPhoneNumberForChatbotService(UserShopInfoProtocol userShop) {
        return userShop.getMyShopOrUserPhoneNumber();
    }

    @Override
    public List<ChatbotBuyerRegistrationRequestDataProtocol> getBuyerRegistrationRequests(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getMyBuyerRegistrationRequestsResourcePath(this.getPhoneNumberForChatbotService(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            List<ChatbotBuyerRegistrationRequestDataProtocol> result = new ArrayList<>();
            JSONArray requestArr = new JSONObject(response).getJSONArray("requests");
            for (int i = 0; i < requestArr.length(); ++i) {
                result.add(new ChatbotBuyerRegistrationRequestData(requestArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public ContactDataProtocol acceptBuyerRegistrationRequests(UserShopInfoProtocol userShop, ChatbotBuyerRegistrationRequestDataProtocol request, String updatedBuyerName, boolean isRetail) throws MyServiceFailureException {
        try {
            JSONObject contactObj = new JSONObject()
                    .put("phone_number", request.getBuyerPhoneNumber())
                    .put("retail", isRetail);
            if (updatedBuyerName == null || updatedBuyerName.isEmpty()) {
                contactObj.put("name", request.getBuyerName());
            } else{
                contactObj.put("name", updatedBuyerName);
            }
            JSONObject requestObj = new JSONObject().put("contact", contactObj);
            String resource = this.getABuyerRegistrationRequestResourcePath(this.getPhoneNumberForChatbotService(userShop), request);
            String response = APIGatewayClient.requestPUT(resource, requestObj.toString(), UserHelper.getIdentityToken());

            JSONObject newContactObj = new JSONObject(response).getJSONObject("contact");
            return new ContactJSONData(newContactObj);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<ChatbotBuyerBaseDataProtocol> getAllBuyers(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getMyBuyersResourcePath(this.getPhoneNumberForChatbotService(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            List<ChatbotBuyerBaseDataProtocol> result = new ArrayList<>();
            JSONArray buyerArr = new JSONObject(response).getJSONArray("buyers");
            for (int i = 0; i < buyerArr.length(); ++i) {
                result.add(new ChatbotBuyerBaseData(buyerArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<ChatbotOrderDataProtocol> getRecentOrders(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getMyRecentChatbotOrdersResourcePath(this.getPhoneNumberForChatbotService(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            List<ChatbotOrderDataProtocol> result = new ArrayList<>();
            JSONArray orderArr = new JSONObject(response).getJSONArray("orders");
            for (int i = 0; i < orderArr.length(); ++i) {
                result.add(new ChatbotOrderData(orderArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    private Pair<Boolean, ChatbotOrderDataProtocol> updateOrderState(UserShopInfoProtocol userShop, String orderId, String requestBody) throws MyServiceFailureException {
        try {
            String resource = this.getAChatbotOrderResourcePath(this.getPhoneNumberForChatbotService(userShop), orderId);
            String response = APIGatewayClient.requestPUT(resource, requestBody, UserHelper.getIdentityToken());

            JSONObject resultObj = new JSONObject(response);
            Boolean isUpdated = Boolean.valueOf(resultObj.getBoolean("is_updated"));
            ChatbotOrderDataProtocol updatedOrder = new ChatbotOrderData(resultObj.getJSONObject("order"));
            return new Pair<>(isUpdated, updatedOrder);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public Pair<Boolean, ChatbotOrderDataProtocol> setOrderInProgress(UserShopInfoProtocol userShop, String orderId) throws MyServiceFailureException {
        try {
            JSONObject paramObj = new JSONObject()
                    .put("state", ChatbotOrderState.IN_PROGRESS.getRemoteStr());
            return this.updateOrderState(userShop, orderId, paramObj.toString());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public Pair<Boolean, ChatbotOrderDataProtocol> rejectOrder(UserShopInfoProtocol userShop, String orderId, OrderRejectReason reason) throws MyServiceFailureException {
        try {
            JSONObject paramObj = new JSONObject()
                    .put("state", ChatbotOrderState.REJECTED.getRemoteStr())
                    .put("reason", reason.getRemoteStr());
            return this.updateOrderState(userShop, orderId, paramObj.toString());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<Integer> getNewArrivalsItemIds(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getMyNewArrivalItemIdsResourcePath(this.getPhoneNumberForChatbotService(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            List<Integer> result = new ArrayList<>();
            JSONArray newArrivalItemIdArr = new JSONObject(response).getJSONArray("item_ids");
            for (int i = 0; i < newArrivalItemIdArr.length(); ++i) {
                result.add(Integer.valueOf(newArrivalItemIdArr.getInt(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public void updateNewArrivalItemIds(UserShopInfoProtocol userShop, List<Integer> itemids) throws MyServiceFailureException {
        try {
            JSONArray itemIdArr = new JSONArray(itemids);
            String requestBody = new JSONObject().put("item_ids", itemIdArr).toString();
            String resource = this.getMyNewArrivalItemIdsResourcePath(this.getPhoneNumberForChatbotService(userShop));
            APIGatewayClient.requestPOST(resource, requestBody, UserHelper.getIdentityToken());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    //////////////////////////////////////////////////////////////////////
    // [ORGR-79] 자동 처리되지 않은 발화 관리 기능

    @Override
    public List<NonProcessedUtteranceData> getRecentNonProcessedUtterances(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getRecentNonProcessedUtterancesResourcePath(this.getPhoneNumberForChatbotService(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            List<NonProcessedUtteranceData> result = new ArrayList<>();
            JSONArray utteranceArr = new JSONObject(response).getJSONArray("utterances");
            for (int i = 0; i < utteranceArr.length(); ++i) {
                try {
                    result.add(new NonProcessedUtteranceData(utteranceArr.getJSONObject(i)));
                } catch (JSONException e) {
                    // TODO Logging
                }
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public NonProcessedUtteranceData processNonProcessedUtterance(UserShopInfoProtocol userShop, String utteranceId, String note) throws MyServiceFailureException {
        try {
            String requestBody = null;
            if (note != null & !note.isEmpty()) {
                requestBody = new JSONObject().put("note", note).toString();
            }
            String resource = this.getNonProcessedUtteranceProcessingPath(this.getPhoneNumberForChatbotService(userShop), utteranceId);
            String response = APIGatewayClient.requestPUT(resource, requestBody, UserHelper.getIdentityToken());

            JSONObject utteranceObj = new JSONObject(response).getJSONObject("utterance");
            return new NonProcessedUtteranceData(utteranceObj);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }


    //////////////////////////////////////////////////////////////////////
    // Chatbot 동작 모드 설정 관련

    private String getChatbotSettingResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "chatbot", "setting");
    }

    @Override
    public ChatbotSettingData getChatbotSetting(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getChatbotSettingResourcePath(this.getPhoneNumberForChatbotService(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            return new ChatbotSettingData(new JSONObject(response).getJSONObject("setting"));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public ChatbotSettingData updateChatbotSetting(UserShopInfoProtocol userShop, ChatbotSettingData setting) throws MyServiceFailureException {
        try {
            String requestBody = new JSONObject()
                    .put("setting", setting.getObject())
                    .toString();
            String resource = this.getChatbotSettingResourcePath(this.getPhoneNumberForChatbotService(userShop));
            String response = APIGatewayClient.requestPUT(resource, requestBody, UserHelper.getIdentityToken());

            return new ChatbotSettingData(new JSONObject(response).getJSONObject("setting"));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }
}
