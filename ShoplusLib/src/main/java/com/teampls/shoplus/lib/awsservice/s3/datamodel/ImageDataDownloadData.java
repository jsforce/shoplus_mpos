package com.teampls.shoplus.lib.awsservice.s3.datamodel;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.teampls.shoplus.lib.protocol.ImageDataProtocol;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author lucidite
 */
public class ImageDataDownloadData implements ImageDataProtocol {
    private byte[] imageByteArray;

    /**
     * 빈 데이터 인스턴스를 만든다. 내부적으로 비어 있는 byte 배열을 생성한다.
     */
    public ImageDataDownloadData() {
        this.imageByteArray = new byte[0];
    }

    /**
     * 다운로드한 InputStream으로부터 이미지 데이터 인스턴스를 생성한다.
     *
     * @param inputStream   서버로부터 다운로드한 input stream
     * @throws IOException
     */
    public ImageDataDownloadData(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int read;
        while ((read = inputStream.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, read);
        }
        imageByteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
    }

    @Override
    public byte[] getByteArray() {
        return this.imageByteArray;
    }

    @Override
    public Bitmap getBitmap() {
        return byteArrayToBitmap(this.imageByteArray);
    }

    @Override
    public String saveTo(File localTargetDir, String filename, Bitmap.CompressFormat compressFormat){
        try {
            if (!localTargetDir.isDirectory()){
                localTargetDir.mkdirs();
            }
            String localFilePath = localTargetDir + "/" + filename;
            FileOutputStream out = new FileOutputStream(localTargetDir + "/" + filename);

            this.getBitmap().compress(compressFormat, 100, out);
            out.close();
            return localFilePath;
        } catch (FileNotFoundException e){
            e.printStackTrace();
            return "";
        } catch (IOException e){
            e.printStackTrace();
            return "";
        }
    }

    private Bitmap byteArrayToBitmap(byte[] $byteArray ) {
        Bitmap bitmap = BitmapFactory.decodeByteArray( $byteArray, 0, $byteArray.length ) ;
        return bitmap ;
    }
}
