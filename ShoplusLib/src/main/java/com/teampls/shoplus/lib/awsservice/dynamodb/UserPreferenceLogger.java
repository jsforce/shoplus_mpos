package com.teampls.shoplus.lib.awsservice.dynamodb;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 사용자 설정 logging
 *
 * @author lucidite
 */

public class UserPreferenceLogger {
    private static UserPreferenceLogger instance;
    private AmazonDynamoDBClient client;
    private DynamoDBMapper mapper;

    public static UserPreferenceLogger logger() {
        if (UserPreferenceLogger.instance == null) {
            UserPreferenceLogger.instance = new UserPreferenceLogger();
        }
        return UserPreferenceLogger.instance;
    }

    /**
     * 사용자 설정 로그를 전송한다.
     *
     * @param projectCodename
     * @param userid
     * @param preference
     * @param value
     */
    public void send(final String projectCodename, final String userid, final String preference, final String value) {
        if (client == null) {
            client = new AmazonDynamoDBClient(UserHelper.getCredentialsProvider());
            client.setRegion(Region.getRegion(Regions.AP_NORTHEAST_2));
            mapper = new DynamoDBMapper(client);
        }

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                UserPreferenceLog log = new UserPreferenceLog();

                log.setUserId(userid);
                log.setPreference(preference);
                log.setValue(value);

                Map<String, String> metadata = new HashMap<>();
                metadata.put("client", "android");
                metadata.put("created", APIGatewayHelper.getRDSDateOnlyString(DateTime.now()));
                metadata.put("project", projectCodename);

                log.setMetadata(metadata);
                mapper.save(log);
            }
        };

        Thread logThread = new Thread(runnable);
        logThread.start();
    }
}
