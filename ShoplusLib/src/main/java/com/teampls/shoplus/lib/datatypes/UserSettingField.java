package com.teampls.shoplus.lib.datatypes;

public enum UserSettingField {
    USER_TYPE("user_type"),
    COLOR_NAMES("color_names"),
    ITEM_TRACE_LOCATIONS("trace_locations"),
    RAW_PRICE_INPUT("raw_price"),
    RETAIL_PRICE("retail"),
    SHOW_NEGATIVE_STOCK_VALUE("n_stock"),
    TIME_SHIFT("time_shift"),
    PROJECT_SILVERMOON_AUTH("silvermoon_auth"),
    PROJECT_DALARAN_AUTH("dalaran_auth"),
    MARGIN_ADDITION_RATE("mar"),
    BALANCE_CHANGE_METHOD("balance_change");

    private String remoteStr;

    UserSettingField(String remoteStr) {
        this.remoteStr = remoteStr;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }
}
