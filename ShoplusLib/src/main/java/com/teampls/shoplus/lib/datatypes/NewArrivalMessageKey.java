package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;

import org.joda.time.DateTime;

/**
 * 신상알림 메시지(링크) 키
 *
 * @author lucidite
 */

public class NewArrivalMessageKey {
    private String sender;
    private String id;

    public NewArrivalMessageKey(String sender, String id) {
        this.sender = sender;
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public String getId() {
        return id;
    }

    public DateTime getCreatedDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewArrivalMessageKey that = (NewArrivalMessageKey) o;

        if (!this.sender.equals(that.sender)) return false;
        return this.id.equals(that.id);

    }

    @Override
    public int hashCode() {
        int result = this.sender.hashCode();
        result = 31 * result + this.id.hashCode();
        return result;
    }
}
