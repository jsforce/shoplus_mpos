package com.teampls.shoplus.lib.awsservice.implementations;

import com.teampls.shoplus.lib.awsservice.ProjectAdminServiceProtocol;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 관리자용 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectAdminService implements ProjectAdminServiceProtocol {

    private String getServiceActivationResourcePath(String admin) {
        return MyAWSConfigs.getResourcePath("admin", admin, "service-activation");
    }

    /**
     * 특정 사용자에게 특정 서비스의 사용 권한을 할당한다.
     *
     * @param admin
     * @param targetUser
     * @param service
     * @throws MyServiceFailureException
     */
    public void activate(String admin, String targetUser, ShoplusServiceType service) throws MyServiceFailureException {
        try {
            String resource = this.getServiceActivationResourcePath(admin);
            JSONObject requestBody = new JSONObject();
            requestBody.put("user", targetUser);
            requestBody.put("service", service.toRemoteStr());
            APIGatewayClient.requestPOST(resource, requestBody.toString(), UserHelper.getIdentityToken());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }
}
