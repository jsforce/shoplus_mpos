package com.teampls.shoplus.lib.database_global;


import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.BaseRecord;
import com.teampls.shoplus.lib.enums.ContactBuyerType;

import org.joda.time.DateTime;

public class LocalUserRecord implements Comparable<LocalUserRecord> {
    public int id = 0;
    public String displayName = "", phoneNumber = "", username = "";
    // local field
    public boolean _showing = true;
    public ContactBuyerType _priceType = ContactBuyerType.WHOLESALE;
    // 추가내용 ContactRecord에 참조

    public LocalUserRecord() {
    }

    public LocalUserRecord(int id, String displayName, String phoneNumber, String username, boolean showing) {
        this.id = id;
        this.displayName = displayName;
        this.phoneNumber = phoneNumber;
        this.username = username;
        if (username == null)
            this.username = "";
        this._showing = showing;
    }

    public LocalUserRecord(String displayName, String phoneNumber) {
        this.displayName = (displayName == null) ? "" : displayName;
        this.phoneNumber = (phoneNumber == null) ? "" : phoneNumber;
        if (phoneNumber.isEmpty() == false && BaseUtils.isInteger(phoneNumber) == false && BaseUtils.isDateTime(phoneNumber) == false)
            Log.w("DEBUG_JS", String.format("[LocalUserRecord.LocalUserRecord] check phoneNumber : %s (%s)", phoneNumber, displayName));
    }

    public LocalUserRecord(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isSame(LocalUserRecord record) {
        return BaseRecord.isSame(this, record);
    }

    public String getName() {
        String result = displayName;
        if (result.isEmpty())
            result = BaseUtils.toPhoneFormat(phoneNumber);
        else if (displayName.startsWith("*"))
            result = result.substring(1, result.length());
        return result;
    }

    public String getNameWithRetail() {
        String result = getName();
        if (_priceType == ContactBuyerType.RETAIL)
            result = result + " (소매)";
        return result;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] <%d> name %s, phone %s, showing? %s, username %s", location, id, displayName,
                phoneNumber, Boolean.toString(_showing), username));
    }

    public boolean isEmpty() {
        return (displayName.isEmpty() && phoneNumber.isEmpty());
    }

    @Override
    public int compareTo(@NonNull LocalUserRecord userRecord) {
        return this.displayName.compareTo(userRecord.displayName);
    }

    public static LocalUserRecord createTemp() {
        return new LocalUserRecord("미입력", DateTime.now().toString(BaseUtils.fullFormat));
    }

    public static boolean isTemp(String phoneNumber) {
        return BaseUtils.isDateTime(phoneNumber);
    }

    public boolean isGuest(UserSettingData userSettingData) {
        return phoneNumber.equals("9" + BaseUtils.reverse(userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber()));
    }

    public static LocalUserRecord createGuest(UserSettingData userSettingData) {
        return new LocalUserRecord("[고객]", "9" + BaseUtils.reverse(userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber()));
    }

    public static LocalUserRecord createRetail(UserSettingData userSettingData) {
        LocalUserRecord result = new LocalUserRecord("[소매]", "8" + BaseUtils.reverse(userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber()));
        result._priceType = ContactBuyerType.RETAIL;
        return result;
    }

    public boolean isRetail(UserSettingData userSettingData) {
        return phoneNumber.equals("8" + BaseUtils.reverse(userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber()));
    }
}
