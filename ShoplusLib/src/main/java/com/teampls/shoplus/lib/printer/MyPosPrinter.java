package com.teampls.shoplus.lib.printer;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.bxl.config.editor.BXLConfigLoader;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.AccountLogRecord;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.MessengerLinkData;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.enums.MessengerType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.enums.TransType;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.File;
import java.util.List;

import jpos.JposException;
import jpos.POSPrinter;
import jpos.POSPrinterConst;
import jpos.config.JposEntry;

import static com.teampls.shoplus.lib.database_global.GlobalDB.KEY_RECEIPT_ShowDeposit;

/**
 * Created by Medivh on 2017-04-12.
 */

public class MyPosPrinter extends POSPrinter {
    private static MyPosPrinter instance = null;
    protected String currentProductName = "";
    protected BXLConfigLoader bxlConfigLoader;
    protected static Context context;
    protected UserDB userDB;
    protected GlobalDB globalDB;
    protected boolean opened = false;
    protected static final int station = POSPrinterConst.PTR_S_RECEIPT;
    protected UserSettingData userSettingData;
    protected KeyValueDB keyValueDB;

    public static MyPosPrinter getInstance(Context context) {
        if (instance == null)
            instance = new MyPosPrinter(context);
        MyPosPrinter.context = context;
        return instance;
    }

    protected MyPosPrinter(Context context) {
        super(context);
        this.context = context;
        userDB = UserDB.getInstance(context);
        globalDB = GlobalDB.getInstance(context);
        keyValueDB = KeyValueDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
        refreshCurrentDevice();
    }

    //  MyPosPrinter에 저장이 되어 있어서 설정을 새로 읽어와야 함
    public void refreshCurrentDevice() {
        bxlConfigLoader = new BXLConfigLoader(context); // 이전에 연결된 프린터 정보를 저장하는 곳
        try {
            bxlConfigLoader.openFile();
            List<JposEntry> entries = bxlConfigLoader.getEntries();
            for (JposEntry entry : entries)
                currentProductName = entry.getLogicalName();
        } catch (Exception e) {
            bxlConfigLoader.newFile();
            //Log.e("DEBUG_JS", String.format("[MyPosPrinter.MyPosPrinter] %s", e.getLocalizedMessage()));
        }
    }

    @Override
    public synchronized void close() throws JposException {
        super.close();
        opened = false;
    }

    @Override
    public synchronized void open(String logicalDeviceName) throws JposException {
        if (opened && currentProductName.equals(logicalDeviceName))
            return;
        super.open(logicalDeviceName);
        opened = true;
    }

    public boolean hasPairedDevice() {
        return !currentProductName.isEmpty();
    }

    public void disconnectPrinter() {
        try {
            close();
        } catch (JposException e) {
            e.printStackTrace();
        }
    }

    public void setCurrentPrinter(BluetoothDevice device) {
        try {
            String productName = PosPrinterModel.toProductName(device.getName());
            if (opened)
                close();
            currentProductName = productName;
            for (Object entry : bxlConfigLoader.getEntries()) {
                JposEntry jposEntry = (JposEntry) entry;
                bxlConfigLoader.removeEntry(jposEntry.getLogicalName());
            }
            bxlConfigLoader.addEntry(currentProductName, BXLConfigLoader.DEVICE_CATEGORY_POS_PRINTER,
                    currentProductName, BXLConfigLoader.DEVICE_BUS_BLUETOOTH, device.getAddress());
            bxlConfigLoader.saveFile();
            open(currentProductName);
        } catch (Exception e) {
            Log.e("DEBUG_JS", String.format("[MyPosPrinter.setCurrentPrinter] %s", e.getLocalizedMessage()));
        }
    }

    public String getProductName() {
        return currentProductName;
    }

    public void printTestLine(String data) {
        if (isAvailable() == false)
            return;

        try {
            open(currentProductName);
            claim(0);
            setDeviceEnabled(true);
            printNormal(station, data + "\n");
        } catch (JposException e) {
            handleError(e);
        } finally {
            try {
                close();
            } catch (JposException e) {
                e.printStackTrace();
            }
        }
    }

    protected boolean isAvailable() {
        if (MyDevice.isBluetoothEnabled() == false) {
            MyUI.toastSHORT(context, String.format("블루투스가 꺼져있습니다"));
            return false;
        }

        if (currentProductName.isEmpty()) {
            MyUI.toastSHORT(context, String.format("연결된 프린터가 없습니다"));
            return false;
        }

        return true;
    }

    public void printAccount(AccountRecord accountRecord, AccountLogRecord accountLogRecord) {
        if (isAvailable() == false)
            return;

        try {
            open(currentProductName);
            claim(0);
            setDeviceEnabled(true);

            // data
            UserRecord counterpart = userDB.getRecord(accountRecord.counterpartPhoneNumber);
            DateTime salesDate = accountLogRecord.createdDateTime;
            String salesDateStr = String.format("%s(%s) %s", salesDate.toString(BaseUtils.YMD_Kor_Format), BaseUtils.getDayOfWeekKor(salesDate), salesDate.toString(BaseUtils.HS_Kor_Format));
            String address = userSettingData.getWorkingShop().getDetailedAddress();

            // datetime
            printResetLine();
            printLine(salesDateStr, EscSeq.LeftJustify, EscSeq.FontNormal);
            printLine("");

            // shop name
            printLine(userSettingData.getProviderRecord().getName(), EscSeq.Center, EscSeq.Double, EscSeq.Bold);
            printLine("", EscSeq.LeftJustify, EscSeq.FontNormal, EscSeq.DisableBold);

            // shop phones and location
            if (userSettingData.getWorkingShop().getShopPhoneNumber(0).isEmpty() == false)
                printLine("T." + userSettingData.getWorkingShop().getShopPhoneNumber(0));
            if (userSettingData.getWorkingShop().getShopPhoneNumber(1).isEmpty() == false)
                printLine("T." + userSettingData.getWorkingShop().getShopPhoneNumber(1));
            if (address.isEmpty() == false)
                printLine(address);
            printLine("");

            printLine(String.format("거래처:%s", counterpart.getName()), EscSeq.DoubleWide);
            printResetLine();
            printLine(String.format("수납금:%s", BaseUtils.toCurrencyStr(-accountLogRecord.variation)), EscSeq.DoubleWide);
            printLine(String.format("%s으로 수납", accountLogRecord.accountType.uiStr), EscSeq.FontNormal);
            printDivider();
            printLine(String.format("이전잔금: %s", BaseUtils.toCurrencyStr(accountRecord.getReceivable() - accountLogRecord.variation)));
            printLine(String.format("수납금액: %s", BaseUtils.toCurrencyStr(accountLogRecord.variation)));
            printLine(String.format("현재잔액: %s", BaseUtils.toCurrencyStr(accountRecord.getReceivable())));
            printDivider();
            printLine("감사합니다");
            printTail();

        } catch (JposException e) {
            handleError(e);
        } finally {
            try {
                close();
            } catch (JposException e) {
                e.printStackTrace();
            }
        }

    }

    public void printPreview(SlipFullRecord slipFullRecord) {
        if (isAvailable() == false)
            return;

        try {
            open(currentProductName);
            claim(0);
            setDeviceEnabled(true);

            // datetime
            printResetLine();
            printLine("== 작성 중인 영수증 입니다 ==", EscSeq.LeftJustify, EscSeq.FontNormal);
            printLine("");

            // shop name
            String shopName = userSettingData.getProviderRecord().name;
            printLine(shopName.isEmpty() ? "(미입력)" : shopName, EscSeq.Center, EscSeq.Double, EscSeq.Bold);
            printLine("", EscSeq.LeftJustify, EscSeq.FontNormal, EscSeq.DisableBold);

            SlipSummaryRecord summary = new SlipSummaryRecord(slipFullRecord.items);
            EscSeq fontSize = getFontSize(); // 기본적으로 크게
            Pair<String, String> headers = getHeaders(TransType.bill);

            printLine(String.format("%s%s", headers.first, userDB.getRecord(slipFullRecord.getCounterpartPhoneNumber()).getName()), fontSize);
            printLine(String.format("%s%s", headers.second, BaseUtils.toCurrencyStr(summary.amount)), fontSize);
            if (globalDB.getBool(GlobalDB.KEY_RECEIPT_ShowPreorderSums)) {
                if (summary.preorderSum != 0)
                    printLine(String.format("(주문:%s)", BaseUtils.toCurrencyStr(summary.preorderSum)), EscSeq.FontNormal);
                if (summary.sampleSum != 0 || summary.consignSum != 0)
                    printLine(String.format("(샘플/위탁:%s)", BaseUtils.toCurrencyStr(summary.sampleSum + summary.consignSum)), EscSeq.FontNormal);
            }

            printDivider();
            int serial = 0;
            for (SlipItemRecord slipItemRecord : slipFullRecord.items) {
                serial++;
                String nameString = slipItemRecord.getPosName(context);
                String colorString = (slipItemRecord.color == ColorType.Default) || (slipItemRecord.getColorName().isEmpty()) ? "" : String.format("%s%s", ":", slipItemRecord.getColorName());
                String sizeString = (slipItemRecord.size == SizeType.Default) ? "" : String.format("%s%s", ":", slipItemRecord.size.uiName);
                printLine(String.format("%2d [%s] %s", serial, slipItemRecord.slipItemType.uiName, nameString + colorString + sizeString), EscSeq.LeftJustify, EscSeq.FontNormal);
                String unitPriceStr = BaseUtils.toCurrencyStr(slipItemRecord.unitPrice).replace("원", "");
                String sumStr = slipItemRecord.slipItemType.getSignStr() + BaseUtils.toCurrencyStr(slipItemRecord.unitPrice * slipItemRecord.quantity).replace("원", "");
                printLine(String.format("%s %s = %s", unitPriceStr, slipItemRecord.getQuantityStr(context), slipItemRecord.slipItemType.getSign() == 0 ? "(0)" : sumStr), EscSeq.RightJustify);
                printDivider();
            }
            printTail();
        } catch (JposException e) {
            handleError(e);
        } finally {
            try {
                close();
            } catch (JposException e) {
                e.printStackTrace();
            }
        }
    }

    private EscSeq getFontSize() {
        EscSeq result = EscSeq.DoubleWide;
        return result;
    }

    private Pair<String, String> getHeaders(TransType transtype) {
        String counterpartHeader = "거래처:";
        String amountHeader = String.format("%s:", transtype.uiStr);
        switch (PosPrinterModel.get(currentProductName)) {
            default:
                break;
            case R210:
            case R200II:
            case R215:
                counterpartHeader = "";
                amountHeader = "";
                break;
        }
        return Pair.create(counterpartHeader, amountHeader);
    }

    protected void printDateTime(DateTime dateTime) throws JposException {
        String salesDateStr = String.format("%s(%s) %s", dateTime.toString(BaseUtils.YMD_Kor_Format), BaseUtils.getDayOfWeekKor(dateTime), dateTime.toString(BaseUtils.HS_Kor_Format));
        printLine(salesDateStr, EscSeq.LeftJustify, EscSeq.FontNormal);
        printLine("");
    }

    protected void printDateTime(DateTime dateTime, TransType transType) throws JposException {
        String salesDateStr = String.format("%s(%s) %s / %s", dateTime.toString(BaseUtils.YMD_Kor_Format),
                BaseUtils.getDayOfWeekKor(dateTime), dateTime.toString(BaseUtils.HS_Kor_Format), transType.uiStr);
        printLine(salesDateStr, EscSeq.LeftJustify, EscSeq.FontNormal);
        printLine("");
    }

    protected void printImage() throws JposException {
        // public relations image
        String filePath = globalDB.getValue(GlobalDB.KEY_PRINTER_QR_Image_Path);
        if (new File(filePath).exists() == false) {
            globalDB.put(GlobalDB.KEY_PRINTER_QR_Image, false);
            globalDB.put(GlobalDB.KEY_PRINTER_QR_Image_Path, "");
        }

        if (globalDB.getBool(GlobalDB.KEY_PRINTER_QR_Image)) {
            if (filePath.isEmpty() == false) {
                printBitmap(station, filePath, getRecLineWidth() * 2 / 3, POSPrinterConst.PTR_BM_CENTER);
                //printBitmap(station, bitmap, getRecLineWidth()/2, POSPrinterConst.PTR_BM_CENTER);
                printLine("");
            }
        }
    }

    protected void printShopNameAndInfo(String shopName, String address, EscSeq fontSize) throws JposException {
        printLine(shopName.isEmpty() ? "(미입력)" : shopName, EscSeq.Center, EscSeq.Double, EscSeq.Bold);
        printLine("", EscSeq.LeftJustify, EscSeq.FontNormal, EscSeq.DisableBold);

        // shop phones and location
        if (userSettingData.getWorkingShop().getShopPhoneNumber(0).isEmpty() == false)
            printLine("T." + userSettingData.getWorkingShop().getShopPhoneNumber(0), fontSize);
        if (userSettingData.getWorkingShop().getShopPhoneNumber(1).isEmpty() == false)
            printLine("T." + userSettingData.getWorkingShop().getShopPhoneNumber(1), fontSize);
        if (address.isEmpty() == false)
            printLine(address, fontSize);
        printLine("", EscSeq.FontNormal);
    }

    private void printQRCodes() throws JposException {
        int qrSize;
        switch (globalDB.getQRSize()) {
            default:
                qrSize = 7;
                break;
            case small:
                qrSize = 5;
                break;
        }

        MyMap<MessengerType, MessengerLinkData> messengerLinkMap = userSettingData.getMessengerLinkMap();
        if (globalDB.getBool(GlobalDB.KEY_PRINTER_QR_Kakao)) {
            MessengerLinkData messengerData = messengerLinkMap.get(userSettingData.getActiveMessenger());
            if (messengerData.getURLStr().isEmpty() == false) {
                printBarCode(station, messengerData.getURLStr(), POSPrinterConst.PTR_BCS_QRCODE, 0, qrSize, POSPrinterConst.PTR_BC_CENTER, POSPrinterConst.PTR_BC_TEXT_NONE);
                printLine(messengerData.getType().engStr, EscSeq.Center);
            }
        }

        if (globalDB.getBool(GlobalDB.KEY_PRINTER_QR_Wechat)) {
            MessengerLinkData weChatData = messengerLinkMap.get(MessengerType.WECHAT);
            if (weChatData.getURLStr().isEmpty() == false) {
                printBarCode(station, weChatData.getURLStr(), POSPrinterConst.PTR_BCS_QRCODE, 0, qrSize, POSPrinterConst.PTR_BC_CENTER, POSPrinterConst.PTR_BC_TEXT_NONE);
                printLine("WeChat", EscSeq.Center);
            }
        }
    }

    private void printMemo() throws JposException {
        String memoString = PosPrinterModel.get(currentProductName).getMemoLine();
        String string = PosPrinterModel.get(currentProductName).getLine();
        printNormal(station, EscSeq.LeftJustify.toEscString() + EscSeq.FontNormal.toEscString() + memoString + "\n\n\n\n\n\n\n\n");
        printNormal(station, EscSeq.LeftJustify.toEscString() + EscSeq.FontNormal.toEscString() + string + "\n");
    }

    private void printMemo(String memo) throws JposException {
        String memoString = PosPrinterModel.get(currentProductName).getMemoLine();
        String string = PosPrinterModel.get(currentProductName).getLine();
        printNormal(station, EscSeq.LeftJustify.toEscString() + EscSeq.FontNormal.toEscString() + "\n"+memoString + "\n");
        printNormal(station, EscSeq.DoubleWide.toEscString() + "\n"+memo + "\n");
        printNormal(station, EscSeq.FontNormal.toEscString() + string + "\n");
    }

    public void printSlip(SlipFullRecord slipFullRecord, AccountRecord accountRecord, DateTime accountDownloadTime, int receiptValue,
                          TransType transType, DbActionType actionType, String privateMemo) {
        if (isAvailable() == false)
            return;

        try {
            open(currentProductName);
            claim(0);
            setDeviceEnabled(true);

            // data
            UserRecord counterpart = userDB.getRecord(slipFullRecord.getCounterpartPhoneNumber());
            SlipSummaryRecord slipSummaryRecord = new SlipSummaryRecord(slipFullRecord.items, globalDB.getNoQuantityKeywords());
            String delimiter = ":";
            PosPrinterModel model = PosPrinterModel.get(currentProductName);
            List<String> noQuantityKeyword = globalDB.getNoQuantityKeywords();

            // reset
            printResetLine();
            printDateTime(slipFullRecord.record.salesDateTime, transType);
            printImage();
            printShopNameAndInfo(userSettingData.getProviderRecord().name, userSettingData.getWorkingShop().getDetailedAddress(), EscSeq.FontNormal);

            // slips
            Pair<String, String> headers = getHeaders(transType);
            EscSeq fontSize = getFontSize();
            printLine(String.format("%s%s", headers.first, counterpart.getName()), fontSize);
            printResetLine();
            printLine(String.format("%s%s", headers.second, BaseUtils.toCurrencyStr(slipSummaryRecord.amount + slipFullRecord.record.bill)), fontSize);
            if (globalDB.getBool(GlobalDB.KEY_RECEIPT_ShowQuantitySum))
                printLine(String.format("수량:%d개", slipSummaryRecord.quantity, fontSize));
            if (globalDB.getBool(GlobalDB.KEY_RECEIPT_ShowPreorderSums)) {
                if (slipSummaryRecord.preorderSum != 0)
                    printLine(String.format("(주문:%s)", BaseUtils.toCurrencyStr(slipSummaryRecord.preorderSum)), EscSeq.FontNormal);
                if (slipSummaryRecord.sampleSum != 0 || slipSummaryRecord.consignSum != 0)
                    printLine(String.format("(샘플/위탁:%s)", BaseUtils.toCurrencyStr(slipSummaryRecord.sampleSum + slipSummaryRecord.consignSum)), EscSeq.FontNormal);
            }

            printResetLine();
            if (slipFullRecord.record.bill != 0)
                printLine(String.format("잔금청구:%s", BaseUtils.toCurrencyOnlyStr(slipFullRecord.record.bill)), EscSeq.FontNormal);
            int cashPayment = slipSummaryRecord.amount - (slipFullRecord.record.onlinePayment + slipFullRecord.record.entrustPayment);
            if (cashPayment != 0)
                printLine(String.format("[현장결제] %s", BaseUtils.toCurrencyOnlyStr(cashPayment)), EscSeq.FontNormal);
            if (slipFullRecord.record.onlinePayment != 0)
                printLine(String.format("[온라인청구] %s", BaseUtils.toCurrencyOnlyStr(slipFullRecord.record.onlinePayment)), EscSeq.FontNormal);
            if (slipFullRecord.record.entrustPayment != 0)
                printLine(String.format("[대납청구] %s", BaseUtils.toCurrencyOnlyStr(slipFullRecord.record.entrustPayment)), EscSeq.FontNormal);
            printDivider();

            // slip items
            int serial = 0;
            for (SlipItemRecord slipItemRecord : slipFullRecord.items) {
                boolean noQuantityItem = noQuantityKeyword.contains(slipItemRecord.name);
                serial++;
                String nameString = slipItemRecord.getPosName(context);
                String colorString = (slipItemRecord.color == ColorType.Default) || (slipItemRecord.getColorName().isEmpty()) ? "" : String.format("%s%s", delimiter, slipItemRecord.getColorName());
                String sizeString = (slipItemRecord.size == SizeType.Default) ? "" : String.format("%s%s", delimiter, slipItemRecord.size.uiName);
                String typeString = noQuantityItem ? "기타" : slipItemRecord.slipItemType.uiName;

                if (globalDB.getBool(GlobalDB.KEY_PRINTER_SERIAL, false) == false)
                    printLine(String.format("[%s] %s", typeString, nameString + colorString + sizeString), EscSeq.LeftJustify, model.getNormalFont());
                else
                    printLine(String.format("%2d [%s] %s", serial, typeString, nameString + colorString + sizeString), EscSeq.LeftJustify, model.getNormalFont());
                String unitPriceStr = BaseUtils.toCurrencyStr(slipItemRecord.unitPrice).replace("원", "");
                String sumStr = slipItemRecord.slipItemType.getSignStr() + BaseUtils.toCurrencyStr(slipItemRecord.unitPrice * slipItemRecord.quantity).replace("원", "");
                if (noQuantityItem) {
                    printLine(String.format("%s", sumStr), EscSeq.RightJustify, model.getNormalFont());
                } else if (globalDB.getBool(GlobalDB.KEY_RECEIPT_UseHangulQuantity)) {
                    printLine(String.format("%s x %d개 = %s", unitPriceStr, slipItemRecord.quantity, slipItemRecord.slipItemType.getSign() == 0 ? "(0)" : sumStr), EscSeq.RightJustify, model.getNormalFont());
                } else {
                    printLine(String.format("%s x %d = %s", unitPriceStr, slipItemRecord.quantity, slipItemRecord.slipItemType.getSign() == 0 ? "(0)" : sumStr), EscSeq.RightJustify, model.getNormalFont());
                }
                if (globalDB.getBool(GlobalDB.KEY_PRINTER_SEPARATOR, false))
                    printDivider();
            }
            if (globalDB.getBool(GlobalDB.KEY_PRINTER_SEPARATOR, false) == false)
                printDivider();

            if (GlobalDB.doShowPlusMinusSum.get(context) && (slipSummaryRecord.plusQuantity > 0 && slipSummaryRecord.minusQuantity > 0)) {
                printLine(String.format("== 판매합산 == %3d개 %11s", slipSummaryRecord.plusQuantity, BaseUtils.toCurrencyOnlyStr(slipSummaryRecord.plusSum)), EscSeq.Center);
                printLine(String.format("== 빼기합산 == %3d개 %11s", slipSummaryRecord.minusQuantity, BaseUtils.toCurrencyOnlyStr(slipSummaryRecord.minusSum)), EscSeq.Center);
                printDivider();
            }

            printReceivables(actionType, accountRecord, receiptValue, slipFullRecord, accountDownloadTime);
            printEmphasizedAccounts();
            if (globalDB.getBool(KEY_RECEIPT_ShowDeposit) && accountRecord.deposit > 0)
                printLine(String.format("매입금 : %s", BaseUtils.toCurrencyStr(accountRecord.deposit)), EscSeq.LeftJustify);
            printMemoAddress();
            printLine("");
            printLine(String.format("[증빙번호] %s", slipFullRecord.getKey().createdDateTime.toString(BaseUtils.KEY_Format)));
            printQRCodes();

            if (privateMemo.isEmpty() == false)
                printMemo(privateMemo);

            if (globalDB.getBool(GlobalDB.KEY_PRINTER_MEMO, false))
                printMemo();
            printTail();

        } catch (JposException e) {
            handleError(e);
        } finally {
            try {
                close();
            } catch (JposException e) {
                e.printStackTrace();
            }
        }
    }

    private void printReceivables(DbActionType actionType, AccountRecord accountRecord, int receiptValue, SlipFullRecord slipFullRecord, DateTime accountDownloadTime) throws JposException {
        if (globalDB.doShowReceivables.get(context) == false)
            return;
        int thisReceivable = slipFullRecord.record.onlinePayment + slipFullRecord.record.entrustPayment;
        switch (actionType) {
            case INSERT:
                printLine(String.format("%s : %11s", "거래전 잔금", BaseUtils.toCurrencyOnlyStr(accountRecord.getReceivable() + receiptValue - thisReceivable)), EscSeq.Center);
                if (thisReceivable != 0)
                    printLine(String.format("%s : %11s", "이거래 잔금", BaseUtils.toCurrencySign(thisReceivable)), EscSeq.Center);
                if (receiptValue != 0)
                    printLine(String.format("%s : %11s", "  받은 잔금", BaseUtils.toCurrencySign(-receiptValue)), EscSeq.Center);
                printLine(String.format("%s : %11s", "거래후 잔금", BaseUtils.toCurrencyOnlyStr(accountRecord.getReceivable())), EscSeq.Center);
                printDivider();
                break;
            default:
                printLine(String.format("잔  금 : %s", BaseUtils.toCurrencyStr(accountRecord.getReceivable())), EscSeq.LeftJustify);
                Duration duration = new Duration(slipFullRecord.record.createdDateTime, accountDownloadTime);
                //Duration duration = new Duration(slipFullRecord.record.createdDateTime, accountDownloadTime);
                if (duration.getStandardMinutes() >= 2)
                    printLine(String.format("(%s 기준)", accountDownloadTime.toString(BaseUtils.YMD_HS_Kor_Format), EscSeq.LeftJustify));
                break;
        }
    }

    private void printMemoAddress() throws JposException {
        if (userSettingData.getWorkingShop().getNotice().isEmpty() == false)
            printLine(userSettingData.getWorkingShop().getNotice());
        if (userSettingData.getWorkingShop().getBuildingAddress().isEmpty() == false)
            printLine(userSettingData.getWorkingShop().getBuildingAddress() + " " + userSettingData.getWorkingShop().getDetailedAddress());

    }

    private void printEmphasizedAccounts() throws JposException {
        boolean accountFound = false;
        for (String account : userSettingData.getWorkingShop().get3Accounts()) {
            if (account.isEmpty() == false) {
                if (accountFound == false) {
                    printResetLine();
                    accountFound = true;
                }
                printLine(account, EscSeq.Bold, EscSeq.LeftJustify);
            }
        }
        if (accountFound)
            printResetLine();
    }

    public void printSlipItems(MSortingKey sortingKey, String title, List<SlipItemRecord> slipItemRecords) {
        if (isAvailable() == false)
            return;
        try {
            open(currentProductName);
            claim(0);
            setDeviceEnabled(true);

            printResetLine();
            printDateTime(DateTime.now());
            printLine("챙길상품 목록", EscSeq.Center, EscSeq.Double, EscSeq.Bold);
            printResetLine();
            if (title.isEmpty() == false)
                printLine(title);

            DateTime currentDay = Empty.dateTime;
            String currentCounterpartName = "";
            SlipItemType currentSlipItemType = SlipItemType.SALES;
            String currentItemName = "";
            for (SlipItemRecord record : slipItemRecords) {

                switch (sortingKey) {
                    // Date : 생성일, CounterpartName : 거래처 이름, Type : 거래종류, Name : 상품이름
                    default:
                    case Date:
                        if (currentDay.equals(BaseUtils.toDay(record.salesDateTime)) == false) {
                            currentDay = BaseUtils.toDay(record.salesDateTime);
                            printResetLine();
                            printLine(BaseUtils.toStringWithWeekDay(currentDay, BaseUtils.MD_Kor_Format), EscSeq.DoubleWide);
                            printResetLine();
                        }

                        if (currentCounterpartName.equals(record._counterpartName) == false) {
                            if (currentCounterpartName.isEmpty() == false)
                                printResetLine();
                            currentCounterpartName = record._counterpartName;
                        }

                        printLine(String.format("%s (%s) %s...%s", BaseUtils.toLimitString(record._counterpartName, 15), record.slipItemType.uiName, record.toNameAndOptionString("/"), String.format("%d개", record.quantity)));
                        break;

                    case CounterpartName:
                        if (currentCounterpartName.equals(record._counterpartName) == false) {
                            printResetLine();
                            currentCounterpartName = record._counterpartName;
                            printLine(currentCounterpartName, EscSeq.DoubleWide);
                            printResetLine();
                        }
                        printLine(String.format("%s (%s) %s...%s", record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format), record.slipItemType.uiName, record.toNameAndOptionString("/"), String.format("%d개", record.quantity)));
                        break;

                    case Type:
                        if (currentSlipItemType != record.slipItemType) {
                            currentSlipItemType = record.slipItemType;
                            printResetLine();
                            printLine(record.slipItemType.uiName, EscSeq.DoubleWide);
                            printResetLine();
                        }
                        printLine(String.format("%s %s..%s...%s", record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format), BaseUtils.toLimitString(record._counterpartName, 15), record.toNameAndOptionString("/"), String.format("%d개", record.quantity)));
                        break;
                    case Name:
                        if (currentItemName.equals(record.name) == false) {
                            currentItemName = record.name;
                            printResetLine();
                        }
                        printLine(String.format("%s %s (%s) %s...%s", record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format), BaseUtils.toLimitString(record._counterpartName, 15), record.slipItemType.uiName, record.toNameAndOptionString("/"), String.format("%d개", record.quantity)));
                        break;
                }

            }
            printTail();

        } catch (JposException e) {
            handleError(e);
        } finally {
            try {
                close();
            } catch (JposException e) {
                e.printStackTrace();
            }
        }
    }

    public void printNameCards() {
        if (isAvailable() == false)
            return;
        try {
            open(currentProductName);
            claim(0);
            setDeviceEnabled(true);

            // reset
            printResetLine();
            printDateTime(DateTime.now());
            printImage();
            printShopNameAndInfo(userSettingData.getProviderRecord().name,
                    userSettingData.getWorkingShop().getDetailedAddress(),
                    GlobalDB.doEnlargePhoneNumberNameCard.get(context) ? EscSeq.DoubleWide : EscSeq.FontNormal);
            printEmphasizedAccounts();
            printMemoAddress();
            printQRCodes();
            if (GlobalDB.doAddMemoSpaceNameCard.get(context))
                printMemo();
            printTail();

        } catch (JposException e) {
            handleError(e);
        } finally {
            try {
                close();
            } catch (JposException e) {
                e.printStackTrace();
            }
        }
    }

    public void printCounterpartAccounts(UserRecord counterpart, AccountRecord accountRecord, List<AccountLogRecord> records) {
        if (isAvailable() == false)
            return;

        try {
            open(currentProductName);
            claim(0);
            setDeviceEnabled(true);

            printResetLine();
            printDateTime(DateTime.now());
            printShopNameAndInfo(userSettingData.getProviderRecord().name,
                    userSettingData.getWorkingShop().getDetailedAddress(), EscSeq.FontNormal);

            Pair<String, String> headers = getHeaders(TransType.bill); // 일단 이렇게
            EscSeq fontSize = getFontSize();
            printLine(String.format("%s%s", headers.first, counterpart.getName()), fontSize);
            printResetLine();
            printLine(String.format("잔금:%s", BaseUtils.toCurrencyStr(accountRecord.getReceivable())), fontSize);

            printDivider();
            for (AccountLogRecord record : records) {
                printLine(String.format("%s %s", BaseUtils.toStringWithWeekDay(record.createdDateTime, BaseUtils.YMDHmFormat), record.operation.uiStr), EscSeq.LeftJustify);
                if (record.isCleared()) {
                    printLine(String.format("[%s] %s %s", record.getAccountLogSummaryType().uiStr, record.updatedDateTime.toString(BaseUtils.MD_FixedSize_Format), BaseUtils.toCurrencyStr(record.account)), EscSeq.RightJustify);
                } else {
                    printLine(String.format("%s", BaseUtils.toCurrencyStr(record.account)), EscSeq.RightJustify);
                }
                printDivider();
            }

            if (BaseUtils.isValidPhoneNumber(counterpart.phoneNumber)) {
                printLine("");
                printLine("[샵플미송잔금] 앱에서도 상세한 내용을 보실 수 있습니다.");
            }

            printTail();

        } catch (JposException e) {
            handleError(e);
        } finally {
            try {
                close();
            } catch (JposException e) {
                e.printStackTrace();
            }
        }

    }

    protected void handleError(JposException e) {
        String message = e.getLocalizedMessage();
        if (message.contains("Receiver not registered")) {
            MyUI.toast(context, String.format("블루투스 연결 이상 : 폰을 재시작하거나 앱 프로세스를 날려주세요"));
        } else {
            MyUI.toastSHORT(context, String.format("%s", e.getMessage()));
        }
        Log.e("DEBUG_JS", String.format("[MyPosPrinter.printString] %s", e.getLocalizedMessage()));
    }

    protected void printTail() {
        try {
            printLine("", EscSeq.LeftJustify, EscSeq.FontNormal);
            printLine("");
            printLine("");

            switch (PosPrinterModel.get(currentProductName)) {
                default:
                    break;
                case Q300:
                    printLine("");
                    printLine("");
                    printLine("");
                    cutPaper(90);
                    break;
            }
        } catch (JposException e) {
            handleError(e);
        }
    }

    protected void printResetLine() throws JposException {
        printNormal(station, EscSeq.FontNormal.toEscString() + " " + EscSeq.DisableBold.toEscString() +
                " " + EscSeq.DisableUnderline.toEscString() + " " + EscSeq.LeftJustify.toEscString() + "\n");
    }

    protected void printLine(String string, EscSeq... esc) throws JposException {
        for (EscSeq seq : esc)
            string = seq.toEscString() + string;
        printNormal(station, string + "\n");
    }

    protected void printDivider() throws JposException {
        String string = PosPrinterModel.get(currentProductName).getLine();
        printNormal(station, EscSeq.LeftJustify.toEscString() + EscSeq.FontNormal.toEscString() + string + "\n");
    }

    public void printFonts() {
        try {
            open(currentProductName);
            claim(0);
            setDeviceEnabled(true);

            for (EscSeq escSeq : EscSeq.values()) {
                printLine(escSeq.toString(), escSeq);
            }
        } catch (JposException e) {
            handleError(e);
        }
    }

    protected enum EscSeq {
        // Font A (12x24, Normal), Font B (9x17, small), Font C (9x24, medium)
        Default(""), FontLarge("aM"), FontSmall("bM"), FontMedium("cM"), FontNormal("N"), LeftJustify("lA"), Center("cA"), RightJustify("rA"),
        Bold("bC"), DisableBold("!bC"), Underline("uC"), DisableUnderline("!uC"), Single("1C"), DoubleWide("2C"), DoubleHigh("3C"), Double("4C"),
        Horizon1("1hC"), Horizon2("2hC"), Horizon3("3hC"), Horizon4("4hC"), Horizon5("5hC"), Horizon6("6hC"), Horizon7("7hC"), Horizon8("8hC"),
        Vertical1("1vC"), Vertical2("2vC"), Vertical3("3vC"), Vertical4("4vC"), Vertical5("5vC"), Vertical6("6vC"), Vertical7("7vC"), Vertical8("8vC");

        private static String ESCAPE_CHARACTERS = new String(new byte[]{0x1b, 0x7c});
        private String escapeStr = "";

        EscSeq(String escapeStr) {
            this.escapeStr = escapeStr;
        }

        public String toEscString() {
            return ESCAPE_CHARACTERS + escapeStr;
        }
    }

}

/*
                    //      printLine(String.format("안녕하세요. %s", shopName.isEmpty()? "" : shopName+"입니다."), EscSeq.Center, EscSeq.FontNormal);
                    //       Bitmap bitmap = MyGraphics.toBitmap(filePath);
                    //     BitmapOptions options = new BitmapOptions();
                    //      options.inGrayScale = true;
                    //            options.inBrightness = (byte) 0x80;
 */