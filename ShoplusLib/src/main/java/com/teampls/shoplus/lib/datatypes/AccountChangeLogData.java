package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.enums.AccountChangeOperationType;
import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 잔금 및 매입 변경 이력 데이터 클래스
 *
 * @author lucidite
 */

public class AccountChangeLogData implements AccountChangeLogDataProtocol {

    private String provider;
    private String created;
    private int sortKey;
    private String buyer;
    private int changeAmount;
    private int currentAmount;
    private Boolean cleared;
    private Boolean cancelled;
    private Boolean crossingAccounts;
    private String updated;
    private AccountChangeOperationType operationType;

    public AccountChangeLogData(JSONObject data) throws JSONException {
        this.provider = data.getString("userid");
        this.created = data.getString("created");
        this.sortKey = data.optInt("t2l", 0);
        this.buyer = data.optString("counterpart", "");
        this.changeAmount = data.optInt("c_amount", -1);
        this.currentAmount = data.optInt("u_amount", -1);
        this.cleared = data.optBoolean("cleared", false);
        this.cancelled = data.optBoolean("cancelled", false);
        this.crossingAccounts = data.optBoolean("crossing", false);
        this.updated = data.optString("updated");
        this.operationType = AccountChangeOperationType.fromRemoteStr(data.optString("op_type", ""));
    }

    @Override
    public String getLogId() {
        return this.created;
    }

    @Override
    public DateTime getCreatedDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.created);
    }

    @Override
    public int getSortKey() {
        // 서버 내 설정 변경사항에 대한 APP-level 대응 처리
        return (this.sortKey > 1577836800) ? this.sortKey : this.sortKey + 142128000;
    }

    @Override
    public String getProvider() {
        return this.provider;
    }

    @Override
    public String getBuyer() {
        return this.buyer;
    }

    @Override
    public int getChangeAmount() {
        return this.changeAmount;
    }

    @Override
    public int getCurrentAmount() {
        return this.currentAmount;
    }

    @Override
    public Boolean isCleared() {
        return this.cleared;
    }

    @Override
    public Boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public Boolean isCrossingAccounts() {
        if (this.isCleared()) {
            return this.crossingAccounts;
        } else {
            return false;       // 완료되지 않은 경우 의미를 갖지 않으므로 false를 반환한다.
        }
    }

    @Override
    public Boolean isRelatedToCashFlow() {
        return this.getOperationType().isRelatedToCashFlow();
    }

    @Override
    public DateTime getUpdatedDateTime() {
        if (this.updated == null) {
            return this.getCreatedDateTime();
        } else {
            return APIGatewayHelper.getDateTimeFromRemoteString(this.updated);
        }
    }

    @Override
    public AccountChangeOperationType getOperationType() {
        return this.operationType;
    }
}
