package com.teampls.shoplus.lib.database;

import android.content.Context;

import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.database_map.KeyValueRecord;

/**
 * Created by Medivh on 2017-07-11.
 */

public class ServiceLinkDB extends KeyValueDB {
    private static ServiceLinkDB instance = null;
    private static int Ver = 1;

    public static ServiceLinkDB getInstance(Context context) {
        if (instance == null)
            instance = new ServiceLinkDB(context);
        return instance;
    }

    private ServiceLinkDB(Context context) {
        super(context, "ServiceLinkDB", Ver, Column.toStrs());
    }

    public void deleteBy(String slipKey) {
        KeyValueRecord record = getRecord(Column.key, slipKey);
        delete(record.id);
    }

}
