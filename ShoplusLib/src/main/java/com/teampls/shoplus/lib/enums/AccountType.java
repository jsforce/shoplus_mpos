package com.teampls.shoplus.lib.enums;

/**
 * 잔금/매입금 타입
 *
 * @author lucidite
 */

public enum AccountType {
    OnlineAccount("onl", "온라인", ColorType.Blue),
    EntrustedAccount("ent", "대납", ColorType.Indigo),
    DepositAccount("dep", "매입", ColorType.OliveDrab);

    private String remoteStr;
    public String uiStr = "";
    public int colorInt = 0;

    AccountType(String remoteStr, String uiStr, ColorType colorType) {
        SlipItemType t;
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
        this.colorInt = colorType.colorInt;
    }

    public static AccountType fromRemoteStr(String remoteStr) {

        for (AccountType type: AccountType.values()) {
            if (type.remoteStr.equals(remoteStr)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Illegal Account Type=" + remoteStr);
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }
}
