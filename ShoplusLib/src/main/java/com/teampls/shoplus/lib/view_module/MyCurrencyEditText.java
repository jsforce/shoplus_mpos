package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.support.annotation.IdRes;
import android.text.Editable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2017-11-15.
 */

public class MyCurrencyEditText {
    private EditText editText;
    private MyTextWatcher myTextWatcher = new MyTextWatcher();
    private int basicUnit = 1;
    private View myView;
    private MyOnClick<Integer> onValueChanged;
    private CheckBox cbIsOneDigit;
    private TextView tvBasicUnit;
    private int value = 0, originalValue = 0;
    private Context context;

    public MyCurrencyEditText(Context context, View view, @IdRes final int editTextId, @IdRes int clearViewId) {
        this.myView = view;
        this.context = context;
        this.editText = (EditText) view.findViewById(editTextId);
        MyView.setTextViewByDeviceSize(context, (TextView) editText);

        if (editText != null) {
            editText.addTextChangedListener(myTextWatcher);
            editText.setHint("0");
        }
        final View clearView = view.findViewById(clearViewId);
        if (clearView != null) {
            clearView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.requestFocus();
                    setValue(0);
                }
            });
        }
    }

    public void showKeyboard() {
        MyDevice.showKeyboard(context, editText);
    }

    public void setOnValueChanged(MyOnClick<Integer> onValueChanged) {
        this.onValueChanged = onValueChanged;
    }

    public void setPlusMinusButton(int plusResId, int minusResId) {
        myView.findViewById(plusResId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value = getValue();
                value = value + basicUnit;
                setValue(value);
            }
        });

        myView.findViewById(minusResId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value = getValue();
                value = value - basicUnit;
                setValue(value);
            }
        });
    }

    public MyCurrencyEditText setTextViewAndBasicUnit(@IdRes int basicUnitId, int basicUnit) {
        tvBasicUnit = (TextView) myView.findViewById(basicUnitId);
        MyView.setTextViewByDeviceSize(context, tvBasicUnit);
        setBasicUnit(basicUnit);
        return this;
    }

    // basic unit 이 변경되면 value 표시 부분도 같이 변경되야 함
    private void setBasicUnit(int basicUnit) {
        this.basicUnit = basicUnit;
        if (tvBasicUnit != null)
            tvBasicUnit.setVisibility(basicUnit == 1000 ? View.VISIBLE : View.GONE);
        setValue(originalValue);
    }

    public MyCurrencyEditText setTextViewAndBasicUnitCheckBox(@IdRes int basicUnitId, @IdRes int checkBoxId, boolean defaultValue) {
        cbIsOneDigit = (CheckBox) myView.findViewById(checkBoxId);
        cbIsOneDigit.setChecked(defaultValue);
        MyView.setTextViewByDeviceSize(context, cbIsOneDigit);
        setTextViewAndBasicUnit(basicUnitId, cbIsOneDigit.isChecked() ? 1 : 1000);
        cbIsOneDigit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isOneDigit) {
                setBasicUnit(isOneDigit ? 1 : 1000);
                if (onCheckBoxChangeTask != null)
                    onCheckBoxChangeTask.onTaskDone(isOneDigit);
            }
        });
        return this;
    }

    private MyOnTask<Boolean> onCheckBoxChangeTask;

    public void setOnCheckBoxChanged(MyOnTask<Boolean> onTask) {
        this.onCheckBoxChangeTask = onTask;
    }

    public int getValue() {
        return value; // BaseUtils.toInt(editText.getText().toString()) * basicUnit;
    }

    public void setOriginalValue(int value) {
        originalValue = value;
        setValue(value);
    }

    private void setValue(int value) {
        // compute value by considering basic unit
        // 1000원 절삭 문제 주의
        // basicUnit때문에 10,600을 입력해도 10,000으로 설정될 수 있다
        myTextWatcher.activated = false;

        if (basicUnit == 0) {
            this.value = 0;
        } else {
            this.value = Math.round(value / basicUnit) * basicUnit;
        }

        if (this.value == 0) {
            this.value = 0;
            editText.setText("");
        } else {
            editText.setText(BaseUtils.toCurrencyOnlyStr(this.value / basicUnit));
            editText.setSelection(editText.getText().toString().length());
        }
        //Log.i("DEBUG_JS", String.format("[MyCurrencyEditText.setRepresentValue] %d, %d, %s", this.value, basicUnit, BaseUtils.toCurrencyOnlyStr(this.value)));

        if (onValueChanged != null)
            onValueChanged.onMyClick(null, this.value);

        myTextWatcher.activated = true;
    }

    public EditText getEditText() {
        return editText;
    }

    class MyTextWatcher extends BaseTextWatcher {
        public boolean activated = true;

        @Override
        public void afterTextChanged(Editable s) {
            if (activated == false) return;
            activated = false;
            if (s.toString().equals("-")) {
                value = -0;
                editText.setText("-");
                editText.setSelection(editText.getText().toString().length()); // 커서는 맨뒤에
            } else if (s.toString().equals("0")) {
                setValue(0);
            } else if (s.toString().equals("")) {
                // 그대로 둔다 (back space 버튼으로 숫자를 다 지울 경우 발생)
            } else {
                setValue(BaseUtils.toInt(s.toString())*basicUnit);
            }
            activated = true;
        }

    }

}
