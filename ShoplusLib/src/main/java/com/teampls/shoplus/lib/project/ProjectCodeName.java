package com.teampls.shoplus.lib.project;

/**
 * @author lucidite
 */
public enum ProjectCodeName {
    TELDRASSIL("t"),
    DALARAN("d");

    private String uniliteralCode;

    ProjectCodeName(String uniliteralCode) {
        this.uniliteralCode = uniliteralCode;
    }

    public String toUniliteralCode() {
        return this.uniliteralCode;
    }
}
