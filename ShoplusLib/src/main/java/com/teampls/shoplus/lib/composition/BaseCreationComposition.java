package com.teampls.shoplus.lib.composition;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseSlipItemDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_map.KeyValueBoolean;
import com.teampls.shoplus.lib.database_memory.MAccountDB;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.UpdateUserDialog;
import com.teampls.shoplus.lib.dialog.UserDetailsDialog;
import com.teampls.shoplus.lib.enums.AutoMessageRequestTimeConfigurationType;
import com.teampls.shoplus.lib.enums.ChoiceMode;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.CounterpartTransView;
import com.teampls.shoplus.lib.view.MyAllUsersView;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.viewSetting.ReceiptSettingView;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import com.teampls.shoplus.lib.view_module.MyCheckBox;
import com.teampls.shoplus.lib.view_module.MyCreationDateHeader;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;
import com.teampls.shoplus.lib.view_module.MyKeyValueEditText;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2019-02-14.
 */

// UI 요소만 모아놓고 Data 요소는 상속 클래스에서 구현
abstract public class BaseCreationComposition extends ViewComposition implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    static public int thisView = R.layout.base_slip_creation_main;
    protected ExpandableHeightListView lvSlipItems;
    protected TextView tvSummary, tvMemo, tvPlusQuantitySum, tvPlusAmountSum, tvMinusQuantitySum, tvMinusAmountSum,
            tvCounterpart, tvAddSlipItem, tvMemoMore;
    protected LinearLayout plusMinusSumContainer, plusContainer, minusContainer;
    protected BaseSlipItemDBAdapter adapter;
    protected int listViewDirectionOnResume = MyView.NO_DIRECTION;
    protected MyEmptyGuide myEmptyGuide;
    protected boolean accountDownloaded = false;
    protected UserRecord counterpart = new UserRecord();
    protected TaskState onUploading = TaskState.beforeTasking;
    protected int retryCount = 0;
    protected ImageView ivSelected;
    protected ScrollView scrollview;

    public BaseCreationComposition(Activity activity) {
        super(activity);
        adapter = setAdapter();
        scrollview = findViewById(R.id.slip_creation_scrollview);

        lvSlipItems = activity.findViewById(R.id.slip_creation_listview);
        lvSlipItems.setExpanded(true);
        lvSlipItems.setOnItemClickListener(this);
        lvSlipItems.setOnItemLongClickListener(this);
        lvSlipItems.setAdapter(adapter);

        tvSummary = findViewById(R.id.slip_creation_summary);
        tvCounterpart = findTextViewById(R.id.slip_creation_counterpart);
        tvMemo = findViewById(R.id.slip_creation_memo);
        tvPlusQuantitySum = findViewById(R.id.slip_creation_plus_quantity_sum);
        tvPlusAmountSum = findViewById(R.id.slip_creation_plus_amount_sum);
        tvMinusQuantitySum = findViewById(R.id.slip_creation_minus_quantity_sum);
        tvMinusAmountSum = findViewById(R.id.slip_creation_minus_amount_sum);
        tvMemoMore = findViewById(R.id.slip_creation_memo_more);
        plusMinusSumContainer = findViewById(R.id.slip_creation_plusminus_container);
        plusMinusSumContainer.setVisibility(View.GONE);
        plusContainer = findViewById(R.id.slip_creation_plus_container);
        minusContainer = findViewById(R.id.slip_creation_minus_container);
        MyView.setTextViewByDeviceSize(this.context, plusContainer);
        MyView.setTextViewByDeviceSize(this.context, minusContainer);
        ivSelected = findViewById(R.id.slip_creation_item_selected);

        new MyCreationDateHeader(this.context, getView(), R.id.slip_creation_date,
                R.id.slip_creation_reset_date, R.id.slip_creation_set_date);

        setOnClick(R.id.slip_creation_cancel, R.id.slip_creation_apply, R.id.slip_creation_delete,
                R.id.slip_creation_addByList, R.id.slip_creation_addByManual,
                R.id.slip_creation_addByName, R.id.slip_creation_function,
                R.id.slip_creation_addBySerial, R.id.slip_creation_addByQR,
                R.id.slip_creation_addFromDalaran, R.id.slip_creation_addDC,
                R.id.slip_creation_addPercentDC, R.id.slip_creation_print,
                R.id.slip_creation_counterpart, R.id.slip_creation_summary,
                R.id.slip_creation_withdrawDeposit, R.id.slip_creation_setting, R.id.slip_creation_memo,
                R.id.slip_creation_table_name, R.id.slip_creation_table_type, R.id.slip_creation_bottom_scroll_setting, R.id.slip_creation_addDelivery,
                R.id.slip_creation_addSlipitem, R.id.slip_creation_changeAll,
                R.id.slip_creation_addTax, R.id.slip_creation_refresh,
                R.id.slip_creation_item_selected);

        MyView.setTextViewByDeviceSize(context, getView(), R.id.slip_creation_table_unitprice,
                R.id.slip_creation_table_quantity, R.id.slip_creation_table_sum);
        MyView.setImageViewSizeByDeviceSizeScale(context, 1.0, ivSelected);

        setMyEmptyGuide();
    }

    public void setScrollMenusVisibility(int visibility) {
        for (View view : MyView.getChildViews((LinearLayout) findViewById(R.id.slip_creation_scrollMenus_container)))
            view.setVisibility(visibility);
    }

    private void setMyEmptyGuide() {
        myEmptyGuide = new MyEmptyGuide(context, getView(), adapter, findViewById(R.id.slip_creation_scrollview),
                R.id.slip_creation_empty_container, R.id.slip_creation_empty_message,
                R.id.slip_creation_empty_contactUs);
        String emptyString = "아래 [이름검색], [이미지검색]으로\n영수증을 작성하세요.\n\n잔금, 매입금 확인은\n거래처 이름을 클릭하세요";
        myEmptyGuide.setMessage(emptyString);
        myEmptyGuide.setContactUsButton("판매상품 입력", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAtEmptyGuide(getView().findViewById(R.id.slip_creation_addByName));
                MyUI.toastSHORT(context, String.format("이름검색 화면으로 이동합니다"));
            }
        });
    }

    private void onClickAtEmptyGuide(View view) {
        onClick(view);
    }

    abstract protected BaseSlipItemDBAdapter setAdapter();

    public void scrollListView(final int direction) {

        lvSlipItems.post(new Runnable() {
            @Override
            public void run() {
                switch (direction) {
                    case MyView.TOP:
                        //lvSlipItems.setSelection(0);
                        scrollview.fullScroll(View.FOCUS_UP);
                        break;
                    case MyView.BOTTOM:
                        //lvSlipItems.setSelection(adapter.getCount() - 1);
                        scrollview.fullScroll(View.FOCUS_DOWN);
                        break;
                }
            }
        });
    }

    public void refreshCounterpart(UserRecord counterpart) {
        if (counterpart.isTemp(counterpart.phoneNumber) || counterpart.getName().isEmpty()) // 전화번호가 아니라 날짜면 임시고객 상태
            counterpart.name = "(미입력)";
        tvCounterpart.setText(String.format("%s", counterpart.getName()));
    }

    protected void downloadAccount(final UserRecord counterpart) {
        if (userDB.has(counterpart.phoneNumber) == false) {
            accountDownloaded = true;
            return;
        }

        new CommonServiceTask(context, "downloadAccount") {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                AccountsData data = accountService.getAccounts(userSettingData.getUserShopInfo(), userSettingData.getUserConfigType(), counterpart.phoneNumber);
                MAccountDB.getInstance(context).updateOrInsert(new AccountRecord(counterpart.phoneNumber, data, counterpart.name));
                accountDownloaded = true;
            }

            @Override
            public void onPostExecutionUI() {
                if (GlobalDB.doShowReceivablesToMemo.get(context))
                    refresh();
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                accountDownloaded = true;
            }
        };
    }

    protected KeyValueBoolean doShowQrSearchButton = new KeyValueBoolean("slipCreation.show.QRSearchButton");
    protected KeyValueBoolean doShowSerialSearchButton = new KeyValueBoolean("slipCreation.show.serialSearchButton");
    protected KeyValueBoolean doShowAddDC = new KeyValueBoolean("slipCreation.show.addDC", true);
    protected KeyValueBoolean doShowAddPercentDC = new KeyValueBoolean("slipCreation.show.addPercentDC", true);
    protected KeyValueBoolean doShowAddDalaran = new KeyValueBoolean("slipCreation.show.addDalaran", true);
    protected KeyValueBoolean doShowPrint = new KeyValueBoolean("slipCreation.show.print", true);
    protected KeyValueBoolean doShowDelivery = new KeyValueBoolean("slipCreation.show.delivery", true);
    protected KeyValueBoolean doShowTax = new KeyValueBoolean("slipCreation.show.tax", true);
    protected KeyValueBoolean doShowChangeAll = new KeyValueBoolean("slipCreation.show.changeAll", true);

    protected void refreshBySettings() {
        doShowQrSearchButton.setDefaultValue(userSettingData.isActivated(ShoplusServiceType.SaaS));
        setVisibility(doShowQrSearchButton.get(context) ? View.VISIBLE : View.GONE, R.id.slip_creation_addByQR);
        setVisibility(doShowSerialSearchButton.get(context) ? View.VISIBLE : View.GONE, R.id.slip_creation_addBySerial);
        setVisibility(doShowAddDC.get(context) ? View.VISIBLE : View.GONE, R.id.slip_creation_addDC);
        setVisibility(doShowAddPercentDC.get(context) ? View.VISIBLE : View.GONE, R.id.slip_creation_addPercentDC);
        setVisibility(doShowAddDalaran.get(context) ? View.VISIBLE : View.GONE, R.id.slip_creation_addFromDalaran);
        setVisibility(doShowPrint.get(context) ? View.VISIBLE : View.GONE, R.id.slip_creation_print);
        setVisibility(doShowDelivery.get(context) ? View.VISIBLE : View.GONE, R.id.slip_creation_addDelivery);
        setVisibility(doShowTax.get(context) ? View.VISIBLE : View.GONE, R.id.slip_creation_addTax);
        setVisibility(doShowChangeAll.get(context) ? View.VISIBLE : View.GONE, R.id.slip_creation_changeAll);
    }


    protected void refreshTitles(SlipSummaryRecord slipSummaryRecord, String memo) {
        int receivable = MAccountDB.getInstance(context).getRecordByKey(counterpart.phoneNumber).getReceivable();
        if (receivable > 0 && GlobalDB.doShowReceivablesToMemo.get(context)) {
            tvMemo.setVisibility(View.VISIBLE);
            String string = String.format("잔금 : %s", BaseUtils.toCurrencyStr(receivable));
            if (memo.isEmpty() == false)
                string = string + "\n" + memo;
            tvMemo.setText(string);
        } else {
            tvMemo.setVisibility(TextUtils.isEmpty(memo) ? View.GONE : View.VISIBLE);
            tvMemo.setText(memo);
        }

        // UI 표시
        if (GlobalDB.getInstance(context).getBool(GlobalDB.KEY_RECEIPT_ShowQuantitySum))
            tvSummary.setText(String.format("%s건 %d개 %s", slipSummaryRecord.count, slipSummaryRecord.quantity, BaseUtils.toCurrencyStr(slipSummaryRecord.amount)));
        else
            tvSummary.setText(String.format("%s건 %s", slipSummaryRecord.count, BaseUtils.toCurrencyStr(slipSummaryRecord.amount)));

        setPlusMinusSum(context, slipSummaryRecord, plusMinusSumContainer, plusContainer, tvPlusQuantitySum, tvPlusAmountSum,
                minusContainer, tvMinusQuantitySum, tvMinusAmountSum);
    }

    public static void setPlusMinusSum(Context context, SlipSummaryRecord slipSummaryRecord, LinearLayout plusMinusSumContainer, LinearLayout plusContainer, TextView tvPlusQuantitySum,
                                       TextView tvPlusAmountSum, LinearLayout minusContainer, TextView tvMinusQuantitySum, TextView tvMinusAmountSum) {

        if (GlobalDB.doShowPlusMinusSum.get(context) && (slipSummaryRecord.plusQuantity > 0 && slipSummaryRecord.minusQuantity > 0)) {
            plusMinusSumContainer.setVisibility(View.VISIBLE);
            if (slipSummaryRecord.plusQuantity > 0) {
                plusContainer.setVisibility(View.VISIBLE);
                tvPlusQuantitySum.setText(String.format("%d개", slipSummaryRecord.plusQuantity));
                tvPlusAmountSum.setText(String.format("%s", BaseUtils.toCurrencyOnlyStr(slipSummaryRecord.plusSum)));
            } else {
                plusContainer.setVisibility(View.GONE);
            }
            if (slipSummaryRecord.minusQuantity > 0) {
                minusContainer.setVisibility(View.VISIBLE);
                tvMinusQuantitySum.setText(String.format("%d개", slipSummaryRecord.minusQuantity));
                tvMinusAmountSum.setText(String.format("%s", BaseUtils.toCurrencyOnlyStr(slipSummaryRecord.minusSum)));
            } else {
                minusContainer.setVisibility(View.GONE);
            }
        } else {
            plusMinusSumContainer.setVisibility(View.GONE);
        }
    }

    // common
    abstract protected void refresh();

    abstract protected SlipItemRecord createNewSlipItem(String name, int quantity, int unitPrice, SlipItemType type);

    abstract protected void addNewSlip(SlipItemRecord newSlipItemRecord);

    abstract protected SlipSummaryRecord getSlipSummaryRecord();

    // Header
    abstract protected void onCounterpartClick();

    abstract protected void addTransFromPrevs(SlipFullRecord prevFullRecord);

    abstract protected void onSummaryClick();

    protected void onSettingClick() {
//
    }

    public void refreshViewByChoiceMode(ChoiceMode choiceMode) {
        adapter.clearClicked();
        switch (choiceMode) {
            default:
            case Single:
                adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_SINGLE);
                ivSelected.setVisibility(View.GONE);
                break;
            case Multiple:
                adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_MULTIPLE);
                ivSelected.setVisibility(View.VISIBLE);
                break;
        }
        adapter.notifyDataSetChanged();
    }

    // Body
    abstract protected void onTabTypeClick();

    abstract protected void onTabNameClick();

    // Scroll menus
    abstract protected void onAddByNameClick(ItemSearchType itemSearchType);

    abstract protected void onAddByListClick();

    abstract protected void onAddByManualClick();

    abstract protected void onAddByQRClick();

    abstract protected void onWithdrawDepositClick();

    abstract protected void onAddDCClick();

    abstract protected void onAddPercentDCClick();

    abstract protected void onAddFromDalaranClick();

    protected ChoiceMode choiceMode = ChoiceMode.Single;

    private void onChangeAll() {
        new OnChangeAllClickDialog(context).show();
    }

    class OnChangeAllClickDialog extends MyButtonsDialog {

        public OnChangeAllClickDialog(final Context context) {
            super(context, "일괄변경", "항목 타입을 한꺼번에 변경합니다");

            addButton("전체 변경", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    new ChangeAllDialog(context, new MyOnTask<SlipItemType>() {
                        @Override
                        public void onTaskDone(SlipItemType result) {
                            choiceMode = ChoiceMode.Single;
                            setSlipItemTypeAs(result);
                            refreshViewByChoiceMode(choiceMode);
                        }
                    });
                }
            });

            addButton("선택후 변경", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    choiceMode = ChoiceMode.Multiple;
                    MyUI.toastSHORT(context, String.format("선택 후 '선택완료'를 눌러주세요"));
                    refreshViewByChoiceMode(choiceMode);
                }
            });
        }
    }

    private void onItemsForChangeSelected() {
        new ChangeAllDialog(context, new MyOnTask<SlipItemType>() {
            @Override
            public void onTaskDone(SlipItemType result) {
                setSlipItemTypeAs(result);
                choiceMode = ChoiceMode.Single;
                refreshViewByChoiceMode(choiceMode);
            }
        });
    }

    protected void setSlipItemTypeAs(SlipItemType slipItemTypeAs) {
        //
    }

    protected List<SlipItemType> getSlipItemTypes() {
        return new ArrayList<>();
    }

    class ChangeAllDialog extends MyButtonsDialog {

        public ChangeAllDialog(Context context, final MyOnTask<SlipItemType> onTask) {
            super(context, "일괄 변경", "다음 상태로 변경합니다");
            if (getSlipItemTypes().isEmpty())
                return;
            List<Pair<String, SlipItemType>> items = new ArrayList<>();
            for (SlipItemType slipItemType : getSlipItemTypes())
                items.add(Pair.create(slipItemType.uiName, slipItemType));
            addRadioButtons(items, null, new MyOnTask<SlipItemType>() {
                @Override
                public void onTaskDone(SlipItemType slipItemType) {
                    dismiss();
                    if (onTask != null)
                        onTask.onTaskDone(slipItemType);
                }
            });
            show();
        }
    }

    protected void onRefreshClick() {
    }

    abstract protected void onPrintClick();

    abstract protected void onScrollSettingClick();

    // Buttons
    abstract protected void onApplyClick();

    abstract protected void onDeleteClick();

    abstract protected void onFunctionClick();


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.slip_creation_print) {
            onPrintClick();

        } else if (view.getId() == R.id.slip_creation_refresh) {
            onRefreshClick();

        } else if (view.getId() == R.id.slip_creation_item_selected) {
            onItemsForChangeSelected();

        } else if (view.getId() == R.id.slip_creation_bottom_scroll_setting) {
            onScrollSettingClick();

        } else if (view.getId() == R.id.slip_creation_addBySerial) {
            onAddByNameClick(ItemSearchType.BySerialNum);

        } else if (view.getId() == R.id.slip_creation_addByQR) {
            onAddByQRClick();

        } else if (view.getId() == R.id.slip_creation_summary) {
            onSummaryClick();

        } else if (view.getId() == R.id.slip_creation_table_type) {
            new MyAlertDialog(context, "구분으로 재정렬", "구분으로 재정렬 하시겠습니까?") {
                @Override
                public void yes() {
                    onTabTypeClick();
                }
            };

        } else if (view.getId() == R.id.slip_creation_table_name) {
            new MyAlertDialog(context, "이름순 정렬", "이름순으로 재정렬 하시겠습니까?") {
                @Override
                public void yes() {
                    onTabNameClick();
                }
            };

        } else if (view.getId() == R.id.slip_creation_setting) {
            onSettingClick();

        } else if (view.getId() == R.id.slip_creation_withdrawDeposit) {
            if (accountDownloaded == false) {
                MyUI.toastSHORT(context, String.format("매입 정보를 확인 중입니다. 잠시 후 다시 시도해주세요."));
                return;
            }
            onWithdrawDepositClick();

        } else if (view.getId() == R.id.slip_creation_counterpart) {
            onCounterpartClick();

        } else if (view.getId() == R.id.slip_creation_addFromDalaran) {
            onAddFromDalaranClick();

        } else if (view.getId() == R.id.slip_creation_addByName || view.getId() == R.id.slip_creation_addSlipitem) {
            onAddByNameClick(ItemSearchType.ByName);

        } else if (view.getId() == R.id.slip_creation_addByList) {
            onAddByListClick();

        } else if (view.getId() == R.id.slip_creation_addByManual) {
            onAddByManualClick();

        } else if (view.getId() == R.id.slip_creation_addPercentDC) {
            onAddPercentDCClick();

        } else if (view.getId() == R.id.slip_creation_addDC) {
            onAddDCClick();

        } else if (view.getId() == R.id.slip_creation_changeAll) {
            onChangeAll();

        } else if (view.getId() == R.id.slip_creation_delete) {
            onDeleteClick();

        } else if (view.getId() == R.id.slip_creation_cancel) {
            onCancelClick();

        } else if (view.getId() == R.id.slip_creation_function) {
            onFunctionClick();

        } else if (view.getId() == R.id.slip_creation_apply) {
            onApplyClick();

        } else if (view.getId() == R.id.slip_creation_addTax) {
            onAddTaxClick();

        } else if (view.getId() == R.id.slip_creation_addDelivery) {
            onAddDeliveryClick();
        }
    }

    protected void onAddTaxClick() {
        SlipSummaryRecord slipSummaryRecord = getSlipSummaryRecord();
        int tax = (int) (slipSummaryRecord.amount * 0.1);
        SlipItemRecord taxRecord = createNewSlipItem(SlipItemType.VALUE_ADDED_TAX.uiName, 1, tax, SlipItemType.VALUE_ADDED_TAX);
        addNewSlip(taxRecord);
    }

    protected void onAddDeliveryClick() {
        SlipItemRecord deliveryRecord = createNewSlipItem(SlipItemType.DeliveryName, 1, GlobalDB.myDeliveryCost.get(context), SlipItemType.MISC_EXPENSES);
        addNewSlip(deliveryRecord);
    }

    protected void onCancelClick() {
        context.finish();
    }

    abstract public class WithdrawDepositDialog extends BaseDialog {
        private TextView tvRemainder;
        private int withdrawMax = 0, salesAmount = 0;
        private LinearLayout todayDepositContainer;
        private MyCurrencyEditText etWithdrawDeposit;

        public WithdrawDepositDialog(Context context, int salesAmount, int currentDeposit, int todayDeposit) {
            super(context);
            setDialogWidth(0.95, 0.7);
            this.salesAmount = salesAmount;
            // 관리를 안하고 있을 경우 withdrawMax <= 0 이 될 수 있다.
            this.withdrawMax = currentDeposit + todayDeposit;
            int withdrawValue = 0;
            if (userSettingData.getBasicUnit() == 1000) {
                withdrawValue = Math.min(withdrawMax, (int) Math.round(salesAmount / 1000 * 0.3333333333333) * 1000);
            } else {
                withdrawValue = Math.min(withdrawMax, (int) Math.round(salesAmount / 100 * 0.3333333333333) * 100);
            }
            if (withdrawValue < 0)
                withdrawValue = 0;
            todayDepositContainer = findViewById(R.id.dialog_withdraw_deposit_todayContainer);
            todayDepositContainer.setVisibility(todayDeposit == 0 ? View.GONE : View.VISIBLE);
            findTextViewById(R.id.dialog_withdraw_deposit_salesAmount, BaseUtils.toCurrencyOnlyStr(salesAmount));
            findTextViewById(R.id.dialog_withdraw_current_deposit, BaseUtils.toCurrencyOnlyStr(currentDeposit));
            findTextViewById(R.id.dialog_withdraw_today_deposit, BaseUtils.toCurrencyOnlyStr(todayDeposit));
            tvRemainder = findTextViewById(R.id.dialog_withdraw_deposit_remains, "");
            refreshTextViews(withdrawValue);

            etWithdrawDeposit = new MyCurrencyEditText(context, getView(), R.id.dialog_withdraw_deposit_value, R.id.dialog_withdraw_deposit_clear_value);
            etWithdrawDeposit.setOriginalValue(withdrawValue);
            etWithdrawDeposit.setTextViewAndBasicUnit(R.id.dialog_withdraw_deposit_value_000, userSettingData.getBasicUnit());
            etWithdrawDeposit.setOnValueChanged(new MyOnClick<Integer>() {
                @Override
                public void onMyClick(View view, Integer record) {
                    refreshTextViews(record);
                }
            });

            setOnClick(R.id.dialog_withdraw_deposit_help, R.id.dialog_withdraw_deposit_cancel,
                    R.id.dialog_withdraw_deposit_apply, R.id.dialog_withdraw_deposit_all);
            show();

            MyDevice.showKeyboard(context, etWithdrawDeposit.getEditText());

            MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_withdraw_deposit_title,
                    R.id.dialog_withdraw_deposit_salesAmount, R.id.dialog_withdraw_deposit_salesAmount_title,
                    R.id.dialog_withdraw_current_deposit, R.id.dialog_withdraw_deposit_title,
                    R.id.dialog_withdraw_today_deposit, R.id.dialog_withdraw_today_deposit_title,
                    R.id.dialog_withdraw_deposit_value, R.id.dialog_withdraw_deposit_value_title, R.id.dialog_withdraw_deposit_value_000,
                    R.id.dialog_withdraw_deposit_remains, R.id.dialog_withdraw_deposit_remains_title,
                    R.id.dialog_withdraw_deposit_cancel, R.id.dialog_withdraw_deposit_apply);
        }

        private void refreshTextViews(int withdrawValue) {
            tvRemainder.setText(BaseUtils.toCurrencyOnlyStr(withdrawMax - withdrawValue));
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_withdraw_deposit;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_withdraw_deposit_help) {
                MyDevice.openWeb(context, "http://wp.me/p8qpsT-jI");

            } else if (view.getId() == R.id.dialog_withdraw_deposit_cancel) {
                MyDevice.hideKeyboard(context, etWithdrawDeposit.getEditText());
                dismiss();

            } else if (view.getId() == R.id.dialog_withdraw_deposit_all) {
                etWithdrawDeposit.setOriginalValue(Math.min(withdrawMax, salesAmount));
                refreshTextViews(etWithdrawDeposit.getValue());
                onApplyClick(etWithdrawDeposit.getValue());
                MyDevice.hideKeyboard(context, etWithdrawDeposit.getEditText());
                dismiss();

            } else if (view.getId() == R.id.dialog_withdraw_deposit_apply) {
                final int withdrawValue = etWithdrawDeposit.getValue();
                if ((withdrawMax > 0) && (withdrawMax < withdrawValue)) {
                    new MyAlertDialog(context, "매입금 확인 요청", "현 매입금보다 큰 금액을 빼시겠습니까?") {
                        @Override
                        public void yes() {
                            onApplyClick(withdrawValue);
                            MyDevice.hideKeyboard(context, etWithdrawDeposit.getEditText());
                            dismiss();
                        }
                    };
                } else {
                    onApplyClick(withdrawValue);
                    MyDevice.hideKeyboard(context, etWithdrawDeposit.getEditText());
                    dismiss();
                }
            }

        }

        abstract public void onApplyClick(int withdrawValue);
    }

    abstract protected void onChangeBuyerClick(UserRecord newBuyer);

    public class CounterpartDialog extends UserDetailsDialog {
        private boolean addTaxEnabled = false;

        public CounterpartDialog(final Context context, final UserRecord counterpart) {
            super(context, counterpart);
            showFunction("거래처 변경");
        }

        public CounterpartDialog enableAddTax() {
            addTaxEnabled = true;
            return this;
        }

        @Override
        // 거래처 변경
        public void onFunctionClick() {
            new MyAlertDialog(context, "(주의) 거래처 변경", "변경하면 문제가 될 내용은 없는지 체크하셨나요?") {
                @Override
                public void yes() {
                    dismiss();
                    MyAllUsersView.startActivity(context, new MyOnClick<UserRecord>() {
                        @Override
                        public void onMyClick(View view, UserRecord counterpart) {
                            onChangeBuyerClick(counterpart);
                        }
                    });
                }
            };
        }

        @Override
        protected void onShowTransClick() {
            CounterpartTransView.startActivity(context, counterpart, new MyOnTask<SlipFullRecord>() {
                @Override
                public void onTaskDone(SlipFullRecord oldFullRecord) {
                    addTransFromPrevs(oldFullRecord);
                    dismiss();
                }
            });

            if (addTaxEnabled) {
                CounterpartTransView.setOnSumClick(new MyOnClick<Pair<DateTime, Integer>>() {
                    @Override
                    public void onMyClick(View view, Pair<DateTime, Integer> pair) {
                        SlipItemRecord taxRecord = createNewSlipItem(String.format("%d월 부가세", pair.first.getMonthOfYear()), 1, pair.second, SlipItemType.VALUE_ADDED_TAX);
                        addNewSlip(taxRecord);
                        dismiss();
                    }
                });
            }

        }

        @Override
        public void onInfoUpdated(UserRecord userRecord) {
            refreshCounterpart(userRecord);
        }

        @Override
        public void onAccountUpdated() {

        }

    }

    protected class ScrollBarSettingDialog extends MyButtonsDialog {
        private String KEY_Make_byQR = "slipCreation.make.byQR";

        public ScrollBarSettingDialog(final Context context) {
            super(context, "화면 설정", "빠른 작성을 위해 사용할 버튼을 체크합니다.");

            addCheckBox("택배비 버튼", doShowDelivery, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "택배비 표시" : "택배비 숨김");
                    refreshBySettings();
                }
            });

            addCheckBox("부가세 버튼", doShowTax, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "부가세 표시" : "부가세 숨김");
                    refreshBySettings();
                }
            });

            addCheckBox("일괄처리 버튼", doShowChangeAll, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "일괄처리 표시" : "일괄처리 숨김");
                    refreshBySettings();
                }
            });

            addCheckBox("QR검색 버튼", doShowQrSearchButton, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "QR검색 표시" : "QR검색 숨김");
                    refreshBySettings();
                }
            });

            addCheckBox("QR코드로 바로 영수증 작성", KEY_Make_byQR, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "QR영수증 표시" : "QR영수증 숨김");
                    refreshBySettings();
                }
            });

            addCheckBox("번호검색 버튼", doShowSerialSearchButton, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "번호검색 표시" : "번호검색 숨김");
                    refreshBySettings();
                }
            });

            addCheckBox("단가할인 버튼", doShowAddDC, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "단가할인 표시" : "단가할인 숨김");
                    refreshBySettings();
                }
            });

            addCheckBox("결제할인(%) 버튼", doShowAddPercentDC, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "결제할인 표시" : "결제할인 숨김");
                    refreshBySettings();
                }
            });

            addCheckBox("미송자료추가 버튼", doShowAddDalaran, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "미송자료 표시" : "미송자료추가 숨김");
                    refreshBySettings();
                }
            });

            addCheckBox("인쇄하기 버튼", doShowPrint, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    MyUI.toastSHORT(context, result ? "프린트 표시" : "프린트 숨김");
                    refreshBySettings();
                }
            });

            addButton("영수증 설정으로", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReceiptSettingView.startActivity(context);
                    dismiss();
                }
            });
        }
    }


    enum dcMode {amount, unitPrice}

    abstract public class PercentDiscountDialog extends BaseDialog {
        private TextView tvAmount, tvDcAmount;
        //private EditText etPercent;
        private MyKeyValueEditText etPercent;
        private MyCheckBox cb100unit;
        private int amount = 0, dcAmount = 0;
        private MyRadioGroup<dcMode> mode;
        private String KEY_MODE = "percent.discount.mode", KEY_PERCENT = "percent.discount.value",
                KEY_IS_100BasicUnit = "discount.100.basicunit";

        public PercentDiscountDialog(Context context, int amount) {
            super(context);
            setDialogWidth(0.9, 0.6);
            this.amount = amount;
            tvAmount = (TextView) findViewById(R.id.dialog_percent_discount_amount);
            tvDcAmount = (TextView) findViewById(R.id.dialog_percent_discount_dcAmount);

            etPercent = new MyKeyValueEditText(context, getView(), R.id.dialog_percent_discount_percent, KEY_PERCENT, "");
            etPercent.setOnClearClick(R.id.dialog_percent_discount_clear_percent);
            cb100unit = new MyCheckBox(keyValueDB, getView(), R.id.dialog_percent_discount_100unit, KEY_IS_100BasicUnit, false);
            cb100unit.disableAutoSave();

            setOnClick(R.id.dialog_percent_discount_apply, R.id.dialog_percent_discount_cancel);

            mode = new MyRadioGroup(context, getView());
            mode.add(R.id.dialog_percent_discount_mode_amount, dcMode.amount);
            mode.add(R.id.dialog_percent_discount_mode_unitprice, dcMode.unitPrice);
            mode.setChecked(dcMode.valueOf(keyValueDB.getValue(KEY_MODE, dcMode.amount.toString())));
            mode.setOnClickListener(false, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    keyValueDB.put(KEY_MODE, mode.getCheckedItem().toString());
                    refreshViews();
                }
            });

            tvAmount.setText(BaseUtils.toCurrencyStr(amount));
            show();

            MyDevice.showKeyboard(context, etPercent.getEditText());


            etPercent.setAfterTextChanged(false, new MyOnTask<String>() {
                @Override
                public void onTaskDone(String result) {
                    refreshViews();
                }
            });

            cb100unit.setOnClick(true, new MyOnClick<Boolean>() {
                @Override
                public void onMyClick(View view, Boolean record) {
                    refreshViews();
                }
            });

        }

        private void refreshViews() {
            int percent = BaseUtils.toInt(etPercent.getValue());
            int basicUnit = cb100unit.isChecked() ? 100 : 1000;

            switch (mode.getCheckedItem()) {
                case amount:
                    setVisibility(View.VISIBLE, R.id.dialog_percent_discount_amount_container,
                            R.id.dialog_percent_discount_dc_container);
                    dcAmount = BaseUtils.round(amount * percent / 100.0, basicUnit);
                    break;
                case unitPrice:
                    dcAmount = 0;
                    setVisibility(View.GONE, R.id.dialog_percent_discount_amount_container,
                            R.id.dialog_percent_discount_dc_container);
                    break;
            }
            tvDcAmount.setText(BaseUtils.toCurrencyStr(dcAmount));
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_percent_discount;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_percent_discount_apply) {
                cb100unit.saveValue();
                etPercent.saveValue();
                onApplyClick(BaseUtils.toInt(etPercent.getValue()), dcAmount,
                        mode.getCheckedItem() == dcMode.unitPrice, cb100unit.isChecked() ? 100 : 1000);
                dismiss();
            } else if (view.getId() == R.id.dialog_percent_discount_cancel) {
                dismiss();
            }
        }

        abstract public void onApplyClick(int percent, int amount, boolean isUnitPriceMode, int basicUnit);

    }

    abstract public class DiscountDialog extends BaseDialog {
        private EditText etQuantity;
        private MyCurrencyEditText etUnitPrice;
        private TextView tvAmount;
        private MyTextWatcher myTextWatcher = new MyTextWatcher();

        public DiscountDialog(Context context) {
            super(context);
            setDialogWidth(0.9, 0.6);
            etQuantity = findViewById(R.id.dialog_discount_quantity);
            tvAmount = findTextViewById(R.id.dialog_discount_amount);
            etQuantity.addTextChangedListener(myTextWatcher);

            etUnitPrice = new MyCurrencyEditText(context, getView(), R.id.dialog_discount_unitprice, R.id.dialog_discount_clear_unitprice);
            etUnitPrice.setOnValueChanged(new MyOnClick<Integer>() {
                @Override
                public void onMyClick(View view, Integer record) {
                    refreshtvAmount();
                }
            });
            refreshtvAmount();

            MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_discount_title,
                    R.id.dialog_discount_unitprice_title, R.id.dialog_discount_quantity_title,
                    R.id.dialog_discount_amount_title, R.id.dialog_discount_amount);

            setOnClick(R.id.dialog_discount_clear_quantity, R.id.dialog_discount_cancel, R.id.dialog_discount_apply);

            show();
            MyDevice.showKeyboard(context, etUnitPrice.getEditText());
        }

        private void refreshtvAmount() {
            tvAmount.setText(BaseUtils.toCurrencyOnlyStr(etUnitPrice.getValue() * BaseUtils.toInt(etQuantity.getText().toString(), 1)));
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_discount;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_discount_clear_quantity) {
                myTextWatcher.enabled = false;
                etQuantity.setText("");
                refreshtvAmount();
            } else if (view.getId() == R.id.dialog_discount_cancel) {
                dismiss();
            } else if (view.getId() == R.id.dialog_discount_apply) {
                onApplyClick(etUnitPrice.getValue(), BaseUtils.toInt(etQuantity.getText().toString(), 1));
                dismiss();
            }
        }

        abstract public void onApplyClick(int unitPrice, int quantity);

        class MyTextWatcher extends BaseTextWatcher {

            @Override
            public void afterTextChanged(Editable s) {
                if (enabled == false) return;
                refreshtvAmount();
            }
        }
    }

    abstract protected void postTransaction(int onlinePayment, int entrustPayment, int billValue, AutoMessageRequestTimeConfigurationType autoMessage);

    protected class AutoMessageDialog extends MyButtonsDialog {

        public AutoMessageDialog(final Context context, final int onlinePayment, final int entrustPayment, final int billValue) {
            super(context, "영수증 자동전송", "영수증을 자동으로 카카오톡에 보내시겠습니까?");

            addButton("지금 전송", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    postTransaction(onlinePayment, entrustPayment, billValue, AutoMessageRequestTimeConfigurationType.INSTANTLY);
                }
            });

            addButton("오전 9시 예약 전송", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    postTransaction(onlinePayment, entrustPayment, billValue, AutoMessageRequestTimeConfigurationType.NEXT_MORNING);
                }
            });

            addButton("전송하지 않고 완료", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    postTransaction(onlinePayment, entrustPayment, billValue, AutoMessageRequestTimeConfigurationType.DO_NOT_SEND);
                }
            });

            addTitleButton("설정", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyUI.toast(context, String.format("첫화면 > .. > [설정하기] 에서 이 화면을 끌 수 있습니다"));
                }
            });
        }
    }

    abstract protected void setCounterpart(String phoneNumber);

    protected class TempUserDialog extends MyButtonsDialog {

        public TempUserDialog(final Context context) {
            super(context, "거래처 미지정", "거래처를 아직 입력하지 않았습니다.");
            addButton("일반고객으로 입력", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCounterpart(UserRecord.createGuest(userSettingData).phoneNumber);
                    onApplyClick();
                    dismiss();
                }
            });

            addButton("새 거래처 등록", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new UpdateUserDialog(context, new UserRecord(), DbActionType.INSERT) {
                        @Override
                        public void onUserInputFinish(UserRecord userRecord) {
                            setCounterpart(userRecord.phoneNumber);
                            onApplyClick();
                            dismiss();
                        }
                    };
                }
            });

        }
    }

}

