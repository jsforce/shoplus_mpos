package com.teampls.shoplus.lib.event;

/**
 * Created by Medivh on 2016-07-30.
 */
public interface MyOnListTask<T> {
    void onRowFinish(T object);
    void onListFinish(T object);
}
