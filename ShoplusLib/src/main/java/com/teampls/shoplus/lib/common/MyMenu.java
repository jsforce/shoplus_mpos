package com.teampls.shoplus.lib.common;

import android.view.MenuItem;

import com.teampls.shoplus.R;

public enum MyMenu {
    addItemCamera(R.drawable.action_add_item_camera),
    addItemFolder(R.drawable.action_add_item_folder),
    addImage(R.drawable.action_add_image),
    addReceipt(R.drawable.action_add_receipt),
    addCounterpart(R.drawable.action_add_counterpart),
    mergeCounterpart(R.drawable.action_merge_users),
    counterpart(R.drawable.action_customers),
    mergeSelected(R.drawable.action_merge_selected),
    refresh(R.drawable.action_refresh),
    openDrafts(R.drawable.action_draft_slip),
    more(R.drawable.action_more),
    download(R.drawable.action_download),
    downloadAll(R.drawable.action_download),
    close(R.drawable.action_close),
    sendTalk(R.drawable.action_send_kakao),
    checkStocks(R.drawable.action_check_stocks),
    delete(R.drawable.action_delete),
    imageInfo(R.drawable.action_image_info),
    getItemFromDB(R.drawable.action_from_itemdb),
    setItemAsSaled(R.drawable.action_as_saled),
    inquiry(R.drawable.action_inquiry),
    pullup(R.drawable.action_pullup),
    items(R.drawable.action_items),
    scanQR(R.drawable.action_scan_qr),
    qrHistory(R.drawable.action_qr_slip),
    sendAsExcel(R.drawable.action_excel),
    byLocation(R.drawable.action_by_location),
    stockHistory(R.drawable.action_stock_history),
    stockVariation(R.drawable.action_stock_variation),

    makeSlip(R.drawable.action_make_slip),
    makeSlipOnCreating(R.drawable.action_make_slip_unread),
    basicInfo(R.drawable.action_basic_info),
    sendStory(R.drawable.action_kas),
    sendKakao(R.drawable.action_send_kakao),
    sendWechat(R.drawable.action_send_wechat),
    keyboard(R.drawable.action_keyboard),
    save(R.drawable.action_save),
    print(R.drawable.action_print),
    printSetting(R.drawable.action_print_setting),
    cancelSlip(R.drawable.action_cancel_slip),
    dalaran(R.drawable.action_dalaran),
    exodar(R.drawable.action_exodar),
    ironforge(R.drawable.action_ironforge),
    silvermoon(R.drawable.action_silvermoon),
    stormwind(R.drawable.action_stormwind),
    purchaseList(R.drawable.action_purchase_list),
    resetData(R.drawable.action_reset),
    searchByName(R.drawable.action_search_by_name),
    searchByItem(R.drawable.action_search_by_item),
    searchByCounterpart(R.drawable.action_search_by_customer),
    searchBuyers(R.drawable.action_search_buyers),
    help(R.drawable.action_user_guide),
    autoInput(R.drawable.action_auto_input),
    resetInput(R.drawable.action_reset_input),
    addItem(R.drawable.action_add_item),
    getPendings(R.drawable.action_pendings),
    inputManually(R.drawable.action_input_manually),
    switchAccount(R.drawable.action_switch_account),
    setting(R.drawable.action_setting),
    contactUs(R.drawable.action_contact_us),
    addReceivableReceipt(R.drawable.action_add_receivable),
    userGuide(R.drawable.action_user_guide),
    addressBook(R.drawable.action_addressbook),
    changeImage(R.drawable.action_change_image),
    openQrSlips(R.drawable.action_qr_slip),
    showQrPrintDate(R.drawable.action_qr_print_date),
    changeDateTime(R.drawable.action_change_datetime),
    setPeriod(R.drawable.action_set_period),
    shareFile(R.drawable.action_share_file),
    inboundRecord(R.drawable.action_inbound),
    summary(R.drawable.action_summary),
    inquryItem(R.drawable.action_inquiry_item),
    mall(R.drawable.action_mall),
    mallUncheck(R.drawable.action_mall_uncheck),
    clearData(R.drawable.action_clear_data),
    settleAccount(R.drawable.action_settlement),

    gridSetting(R.drawable.grid_setting),
    gridMyInfo(R.drawable.grid_myinfo),
    gridPrinterSetting(R.drawable.grid_printer_setting),
    gridSyncAddressbook(R.drawable.grid_sync_addressbook),
    gridCustomers(R.drawable.grid_customers),
    gridTest(R.drawable.grid_test),
    gridTest2(R.drawable.grid_test2),
    gridClose(R.drawable.grid_close),
    gridShoplusLink(R.drawable.grid_shoplus_link),
    gridResetDB(R.drawable.grid_reset_dbs),
    gridDeleteItems(R.drawable.grid_delete_items),
    gridMassiveHandling(R.drawable.grid_massive_handling),
    gridChangeState(R.drawable.grid_change_state),
    gridPreviousItems(R.drawable.grid_previous_items),
    gridClearLocalDB(R.drawable.grid_clear_db),
    gridUserQuestion(R.drawable.grid_plus_friend),
    gridUserQuestionS3(R.drawable.grid_plus_friend),
    gridOrderQrCode(R.drawable.grid_qrcode),
    gridSilvermoon(R.drawable.grid_silvermoon),
    gridDalaran(R.drawable.grid_dalaran),
    gridExodar(R.drawable.grid_exodar),
    gridIronforge(R.drawable.grid_ironforge),
    gridStormwind(R.drawable.grid_stormwind),
    gridSevenstars(R.drawable.grid_sevenstars),
    gridHelpTroubleNotif(R.drawable.grid_trouble_notif),
    gridHelpRegister(R.drawable.grid_help_register),
    gridOrder(R.drawable.grid_order),
    gridUpdateApp(R.drawable.grid_update_app),
    gridCustomColorNames(R.drawable.grid_custom_color_names),
    gridUserGuide(R.drawable.grid_user_guide),
    gridPrintNameCards(R.drawable.grid_namecards),
    gridReceiptSetting(R.drawable.grid_setting_receipt),
    gridLocation(R.drawable.grid_location),
    gridPendings(R.drawable.grid_pendings),
    gridReceivable(R.drawable.grid_receivable),
    gridExcel(R.drawable.grid_excel_setting),
    gridPrintQR(R.drawable.grid_print_qr),
    gridQrSearch(R.drawable.search_qr),
    gridQrHistory(R.drawable.grid_qr_history),
    gridStockHistory(R.drawable.grid_stock_history),
    gridMall(R.drawable.grid_mall),
    gridMallSetting(R.drawable.grid_mall_setting),
    gridMallUnchecked(R.drawable.grid_mall_unchecked),
    gridHelpAddNew(R.drawable.grid_help_addnew);

    public int index = 0, iconRes = R.drawable.ic_launcher, actionEnum = MenuItem.SHOW_AS_ACTION_ALWAYS;
    public String korStr = "";

    MyMenu(int resId) {
        this.index = this.ordinal();
        this.iconRes = resId;
        this.korStr = this.toString();
        if (this.korStr.startsWith("grid"))
            this.actionEnum = MenuItem.SHOW_AS_ACTION_NEVER;
    }

};
