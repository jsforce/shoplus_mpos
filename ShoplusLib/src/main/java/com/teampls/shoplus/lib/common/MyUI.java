package com.teampls.shoplus.lib.common;

import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.view_base.BaseFragment;

public class MyUI {

    public static void refresh(final Context context, final BaseFragment fragment) {
        if (isActiveActivity(context, "refreshDay") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (fragment == null) return;
                fragment.refresh();
            }
        });
    }

    public static boolean isActiveActivity(Context context, String message) {
        if (context == null) {
            if (!message.isEmpty())
                Log.e("DEBUG_JS", String.format("[MyUI.isActiveActivity] context == null : %s", message));
            return false;
        }

        if (context instanceof Activity == false) {
            if (!message.isEmpty())
                Log.w("DEBUG_JS", String.format("[MyUI.isActiveActivity] context = NOT activity : %s", message));
            return false;
        }

        if (((Activity) context).isFinishing()) {
            if (!message.isEmpty())
                Log.w("DEBUG_JS", String.format("[MyUI.isActiveActivity] %s not running : %s",
                        context.getClass().getSimpleName(), message));
            return false;
        }

        return true;
    }

    public static boolean isActivity(Context context, String message) {
        if (context == null) {
            if (!message.isEmpty())
                Log.e("DEBUG_JS", String.format("[MyUI.isActivity] context == null : %s", message));
            return false;
        }

        if (context instanceof Activity == false) {
            if (!message.isEmpty())
                Log.w("DEBUG_JS", String.format("[MyUI.isActivity] context = NOT activity : %s", message));
            return false;
        }
        return true;
    }

    public static void setSelection(final Context context, final Spinner spinner, final int index) {
        if (isActiveActivity(context, "setSelection") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (spinner == null) return;
                spinner.setSelection(index);
            }
        });
    }

    public static void startActivity(final Context context, final Class<?> activity) {
        if (isActiveActivity(context, "startActivity") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (context == null) return;
                Intent intent = new Intent(context, activity);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                ((Activity) context).startActivity(intent);
            }
        });
    }

    public static void clear(final Context context, final Menu menu) {
        if (isActiveActivity(context, "menu") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                menu.clear();
            }
        });
    }

    public static void setVisibility(final Context context, final View view, final boolean visible) {
        if (isActiveActivity(context, "setVisiblity") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (visible) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public static void setVisibility(final Context context, final View view, final int visibility) {
        if (isActiveActivity(context, "setVisibilty") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                view.setVisibility(visibility);
            }
        });
    }

    public static void setText(final Context context, final Object object, final String text) {
        if (isActiveActivity(context, text) == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (object instanceof TextView) {
                    ((TextView) object).setText(text);
                } else if (object instanceof Tab) {
                    ((Tab) object).setText(text);
                }
            }
        });
    }

    public static void setEnabled(final Context context, final Object object, final boolean enabled) {
        if (isActiveActivity(context, "setDisabled") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (object instanceof View) {
                    ((View) object).setEnabled(enabled);
                }
            }
        });
    }

    public static void notify(final Context context, final Object adapter) {
        if (adapter == null) return;
        if (isActiveActivity(context, "notify") == false) return;

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (adapter instanceof BaseDBAdapter) {
                    ((BaseDBAdapter) adapter).notifyDataSetChanged();
                } else if (adapter instanceof BaseAdapter) {
                    ((BaseAdapter) adapter).notifyDataSetChanged();
                } else if (adapter instanceof PagerAdapter) {
                    ((PagerAdapter) adapter).notifyDataSetChanged();
                } else if (adapter instanceof FragmentStatePagerAdapter) {
                    ((FragmentStatePagerAdapter) adapter).notifyDataSetChanged();
                } else if (adapter instanceof FragmentPagerAdapter) {
                    ((FragmentPagerAdapter) adapter).notifyDataSetChanged();
                } else {
                    return;
                }
                //Log.i("DEBUG_JS",String.format("[%s] %s is refreshing",context.getClass().getSimpleName(), adapter.getClass().getSimpleName()));
            }
        });
    }

    public static void toast(final Context context, final String message) {
        if (isActiveActivity(context, message) == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (context == null || message == null) return;
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    // SHORT보다 짧게 보여주려면 별도의 방법을 써야 한다
    public static void toastSHORT(Context context, final String message) {
        if (isActiveActivity(context, message) == false) {
            if (isActiveActivity(BaseAppWatcher.getCurrentActivity(), message) == false)
                return;
            else
                context = BaseAppWatcher.getCurrentActivity();
        }

        final Context finalContext = context;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (finalContext == null) return;
                Toast.makeText(finalContext, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void toastSHORT(final Context context, final String message, final int gravity) {
        if (isActiveActivity(context, message) == false)
            return;

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
                toast.setGravity(gravity, 0, 0);
                toast.show();
            }
        });
    }

    public static void show(final Context context, final ProgressDialog progressDialog) {
        if (isActiveActivity(context, "progressDialog") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (progressDialog == null) return;
                progressDialog.show();
            }
        });
    }

    public static void show(final Context context, final ProgressDialog progressDialog, final String message) {
        if (isActiveActivity(context, "progressDialog") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (progressDialog == null) return;
                progressDialog.setMessage(message);
                progressDialog.show();
            }
        });
    }

    public static void setMessage(final Context context, final ProgressDialog progressDialog, final String message) {
        if (isActiveActivity(context, "progressDialog") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (progressDialog == null) return;
                progressDialog.setMessage(message);
            }
        });
    }

}
