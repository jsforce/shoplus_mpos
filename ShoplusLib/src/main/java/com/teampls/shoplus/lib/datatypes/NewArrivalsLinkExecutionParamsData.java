package com.teampls.shoplus.lib.datatypes;

/**
 * 카카오링크의 실행 파라미터에 작성하는 문자열을 작성하기 위한 클래스
 * 여기서 작성한 문자열을 카카오링크 모바일 앱 연결에 파라미터로 작성하여 보내고,
 * 수신처리할 경우 이 파라미터 문자열을 서버로 전송하여 저장한다.
 *
 * @author lucidite
 */

public class NewArrivalsLinkExecutionParamsData {
    private String linkUrl;
    private String mainImagePath; // 고객이 열었을때 대표 이미지를 보기 위해

    public NewArrivalsLinkExecutionParamsData(String linkUrl, String mainImagePath) {
        this.linkUrl = linkUrl;
        this.mainImagePath = mainImagePath;
    }

    public String getExecutionParamsStr() {
        String result = "link=" + this.linkUrl;
        if (this.mainImagePath != null && !this.mainImagePath.isEmpty()) {
            result = result + "&image=" + this.mainImagePath;
        }
        return result;
    }
}
