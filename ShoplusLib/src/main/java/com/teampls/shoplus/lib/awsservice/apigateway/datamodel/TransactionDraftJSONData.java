package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.protocol.TransactionDraftDataProtocol;
import com.teampls.shoplus.lib.protocol.TransactionDraftMemoDataProtocol;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucidite
 */

public class TransactionDraftJSONData implements TransactionDraftDataProtocol {
    private JSONObject data;

    public TransactionDraftJSONData(JSONObject dataObj) {
        this.data = dataObj;
    }

    @Override
    public long getDraftId() {
        return this.data.optLong("draft_id", 0);
    }

    @Override
    public long getVersionId() {
        return this.data.optLong("updated", 0);     // 업데이트되지 않은 경우 version=0
    }

    @Override
    public DateTime getCreatedDatetime() {
        return new DateTime(
                this.getDraftId(),                  // draft id: Unix Epoch time, milliseconds
                DateTimeZone.forOffsetHours(9));
    }

    @Override
    public DateTime getUpdatedDatetime() {
        return new DateTime(
                this.data.optLong("updated", 0) * 1000,
                DateTimeZone.forOffsetHours(9));
    }

    @Override
    public String getRecipient() {
        return this.data.optString("recipient", "");
    }

    @Override
    public List<SlipItemDataProtocol> getTransactionItems() {
        List<SlipItemDataProtocol> result = new ArrayList<>();
        try {
            JSONArray transactionItemArr = this.data.getJSONArray("draft_items");
            for (int i = 0; i < transactionItemArr.length(); ++i) {
                JSONObject transactionItemObj = transactionItemArr.getJSONObject(i);
                result.add(new SlipItemJSONData(transactionItemObj));
            }
            return result;
        } catch (JSONException e) {
            return result;
        }
    }

    @Override
    public List<TransactionDraftMemoDataProtocol> getMemoList() {
        List<TransactionDraftMemoDataProtocol> result = new ArrayList<>();
        try {
            JSONArray transactionMemoArr = this.data.getJSONArray("memo");
            for (int i = 0; i < transactionMemoArr.length(); ++i) {
                JSONObject transactionMemoObj = transactionMemoArr.getJSONObject(i);
                result.add(new TransactionDraftMemoJSONData(transactionMemoObj));
            }
            return result;
        } catch (JSONException e) {
            return result;
        }
    }
}
