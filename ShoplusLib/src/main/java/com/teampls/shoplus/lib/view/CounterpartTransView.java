package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseSlipDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.database_memory.MemorySlipDB;
import com.teampls.shoplus.lib.database_memory.MemorySlipItemDB;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.MyDateTimeNavigator;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Medivh on 2017-12-06.
 */

public class CounterpartTransView extends BaseActivity implements AdapterView.OnItemClickListener {
    protected ListView listView;
    public TransDBAdapter adapter;
    private TextView tvSum;
    protected static UserRecord counterpart = new UserRecord();
    protected TransDB transDB;
    protected static MyOnTask<SlipFullRecord> onCopyClick;
    private static MyOnClick<Pair<DateTime, Integer>> onSumClick;
    private CheckBox cbShowCancelled;
    private MyDateTimeNavigator monthlyNavigator;
    private DateTime validMonth = new DateTime(2019, 3, 1, 0, 0); // 2019년 3월부터 월별 기록을 사용할 수 있음
    private SlipSummaryRecord summaryRecord = new SlipSummaryRecord();
    private MyEmptyGuide myEmptyGuide;

    public static void startActivity(Context context, UserRecord counterpart, MyOnTask<SlipFullRecord> onCopyClick) {
        CounterpartTransView.counterpart = counterpart;
        CounterpartTransView.onCopyClick = onCopyClick;
        CounterpartTransView.onSumClick = null;
        context.startActivity(new Intent(context, CounterpartTransView.class));
    }

    public static void setOnSumClick(MyOnClick<Pair<DateTime, Integer>> onSumClick) {
        CounterpartTransView.onSumClick = onSumClick;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        transDB = new TransDB(context);
        adapter = new TransDBAdapter(context, transDB);
        adapter.setSortingKey(MSortingKey.Date);

        listView = findViewById(R.id.trans_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);

        tvSum = findTextViewById(R.id.trans_sum, "");

        setDateTimeNavigator();
        setMyEmptyGuide();

        setVisibility(View.VISIBLE, R.id.trans_headerContainer);
        if (onSumClick == null)
            tvSum.setBackground(null);

        cbShowCancelled = findViewById(R.id.trans_showCancelled);
        cbShowCancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });

        setOnClick(R.id.trans_sum);

        downloadTransactions(counterpart.phoneNumber, monthlyNavigator.getDateTime());
    }

    private void setMyEmptyGuide() {
        myEmptyGuide = new MyEmptyGuide(context, getView(), adapter, listView);
        myEmptyGuide.setMessage("거래내역이 없습니다");
        myEmptyGuide.setContactUsButton(View.GONE);
    }

    private void setDateTimeNavigator() {
        monthlyNavigator = new MyDateTimeNavigator(context, BaseUtils.createMonthDateTime(), getView(), MyDateTimeNavigator.MyMode.Month);
        monthlyNavigator.setOnApplyClick(new MyOnClick<DateTime>() {
            @Override
            public void onMyClick(View view, DateTime record) {
                monthlyNavigator.setDateTime(record);
                onTargetMonthSelected(record);
            }
        });
        monthlyNavigator.setLeftMonthButton(new MyOnClick<DateTime>() {
            @Override
            public void onMyClick(View view, DateTime month) {
                monthlyNavigator.setDateTime(month);
                onTargetMonthSelected(month);
            }
        });
        monthlyNavigator.setRightButton(MyDateTimeNavigator.MyMode.Month, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // refresh
                monthlyNavigator.downloadedDateTimes.clear();
                downloadTransactions(counterpart.phoneNumber, monthlyNavigator.getDateTime());
            }
        });
    }

    private void onTargetMonthSelected(DateTime month) {
        adapter.filterPeriod(month, month.plusMonths(1).minusMillis(1));
        if (monthlyNavigator.downloadedDateTimes.contains(month)) {
            refresh();
        } else {
            downloadTransactions(counterpart.phoneNumber, month);
        }
    }

    @Override
    public int getThisView() {
        return R.layout.activity_trans;
    }

    @Override
    public void onClick(final View view) {
        super.onClick(view);
        if (view.getId() == R.id.trans_sum) {
            if (onSumClick != null) {
                new MyAlertDialog(context, "부가세 발행", String.format("%d월 부가세 %s 를 추가 하시겠습니까?\n\n(택배비, 부가세 등 제외 10프로)",
                        monthlyNavigator.getDateTime().getMonthOfYear(), BaseUtils.toCurrencyStr(summaryRecord.getTax()))) {

                    @Override
                    public void yes() {
                        onSumClick.onMyClick(view, Pair.create(monthlyNavigator.getDateTime(), summaryRecord.getTax()));
                        finish();
                    }
                };
            }
        }
    }

    public void downloadTransactions(final String phoneNumber, final DateTime month) {
        String message = "";
        if (month.isBefore(validMonth)) {
           message = String.format("%d월 기록 다운로드 중", month.getMonthOfYear());
        } else {
            message = String.format("%d년 %d월 이전 모든 기록 다운 중", validMonth.getYear(), validMonth.getMonthOfYear());
        }

        new CommonServiceTask(context, "downloadTransactions", message) {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<SlipDataProtocol> slipDataProtocols;
                if (month.isBefore(validMonth)) {
                    slipDataProtocols = transactionService.getOldCounterpartSpecificTransactions(userSettingData.getUserShopInfo(), phoneNumber, userSettingData.isBuyer());
                    monthlyNavigator.downloadedDateTimes.addAll(BaseUtils.getMonthsBetween(2017, 5, validMonth.minusMonths(1).getYear(), validMonth.minusMonths(1).getMonthOfYear()));
                    transDB.refreshBefore(slipDataProtocols, validMonth);
                } else {
                    slipDataProtocols = transactionService.getMontlyCounterpartSpecificTransactions(userSettingData.getUserShopInfo(), phoneNumber, month, userSettingData.isBuyer());
                    monthlyNavigator.downloadedDateTimes.add(month);
                    transDB.refreshMonth(slipDataProtocols, month);
                }

            }

            @Override
            public void onPostExecutionUI() {
                refresh();
            }
        };
    }

    public void refresh() {
        if (adapter == null) return;
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                adapter.blockCancelled(!cbShowCancelled.isChecked());
                adapter.generate(false);
            }

            @Override
            public void onPostExecutionUI() {
                adapter.applyGeneratedRecords();
                adapter.notifyDataSetChanged();
                myActionBar.setTitle(String.format("%s : %d건", counterpart.getName(), adapter.getCount()), true);

                summaryRecord = new SlipSummaryRecord(transDB.items.getRecordsBy(adapter.getRecords()));
                tvSum.setText(BaseUtils.toCurrencyStr(summaryRecord.amount));

                myEmptyGuide.refresh();
            }
        };

    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case close:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SlipRecord slipRecord = adapter.getRecord(position);
        if (onCopyClick != null) {
            new OnItemClickDialog(context, slipRecord).show();
        } else {
            doShowTrans(transDB.getFullRecord(slipRecord.getSlipKey().toSID()));
        }
    }

    class OnItemClickDialog extends MyButtonsDialog {

        public OnItemClickDialog(Context context, final SlipRecord slipRecord) {
            super(context, String.format("%s 거래 선택", slipRecord.createdDateTime.toString(BaseUtils.MD_Kor_Format)), "");
            final SlipFullRecord fullRecord = transDB.getFullRecord(slipRecord.getSlipKey().toSID());

            if (onCopyClick != null) {
                addButton("복사하기", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onCopyClick.onTaskDone(fullRecord);
                        dismiss();
                        finish();
                    }
                });
            }

            addButton("원본보기", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doShowTrans(fullRecord);
                }
            });

        }
    }

    public void doShowTrans(SlipFullRecord slipFullRecord) {
        SimplePosSlipSharingView.startActivity(context, slipFullRecord);
    }


    class TransDB extends MemorySlipDB {

        public TransDB(Context context) {
            super(context, "TransViewDB");
            items = new TransItemDB(context);
        }

    }

    public class TransItemDB extends MemorySlipItemDB {
        private TransItemDB(Context context) {
            super(context, "Trans.ItemDB");
        }
    }

    class TransDBAdapter extends BaseSlipDBAdapter {

        public TransDBAdapter(Context context, MemorySlipDB transDB) {
            super(context, transDB);
        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new ViewHolder();
        }
    }
}
