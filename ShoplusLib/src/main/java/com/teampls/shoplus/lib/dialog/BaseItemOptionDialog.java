package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.support.annotation.IdRes;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseColorAdapter;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemCategorySet;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.viewSetting.ColorSettingView;
import com.teampls.shoplus.lib.view_module.MyCheckBoxs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-03-17.
 */
//  유사 UI : ColorSettingView
public class BaseItemOptionDialog extends BaseDialog implements AdapterView.OnItemClickListener {
    private BaseColorAdapter adapter;
    private GridView gridView;
    private MyCheckBoxs<SizeType> cbSizeTypes;
    private int itemId = 0;
    private RadioGroup rgSizeGroups, rgShoeSizeGroups;
    private List<LinearLayout> sizeContainers = new ArrayList<>();

    public BaseItemOptionDialog(Context context, int itemId) {
        super(context);
        this.itemId = itemId;
        setDialogWidth(0.95, 0.8);
        switch (MyDevice.getDeviceSize(context)) {
            default:
                adapter = new BaseColorAdapter(context, 1);
                break;
            case W600:
            case W720:
            case W800:
                adapter = new BaseColorAdapter(context, 0.8);
                break;
        }

        adapter.setViewType(BaseColorAdapter.ViewOptionDialog).setRecords(ColorType.getMainColors());
        adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_MULTIPLE);
        gridView = (GridView) findViewById(R.id.dialog_item_option_colors);
        gridView.setOnItemClickListener(this);
        gridView.setAdapter(adapter);
        setOnClick(R.id.dialog_item_option_cancel, R.id.dialog_item_option_apply,
                R.id.dialog_item_option_colorNameSetting, R.id.dialog_item_option_sizeSetting);
        setSizes(ItemDB.getInstance(context).getRecordBy(itemId).category.getSet());
        MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_item_option_title,
                R.id.dialog_item_option_colorNameSetting, R.id.dialog_item_option_sizeSetting);
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_item_option;
    }

    private void refreshColor() {
        adapter.generate();
        adapter.notifyDataSetChanged();
    }

    private void setSizes(ItemCategorySet categorySet) {
        // RadioGroup
        rgShoeSizeGroups = (RadioGroup) findViewById(R.id.dialog_item_option_shoe_size);
        rgShoeSizeGroups.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.dialog_item_option_shoe_size_220_255) {
                    setVisibility(View.VISIBLE, R.id.dialog_item_option_shoe_size_220_255_container);
                    setVisibility(View.GONE, R.id.dialog_item_option_shoe_size_260_300_container);
                } else if (checkedId == R.id.dialog_item_option_shoe_size_260_300) {
                    setVisibility(View.GONE, R.id.dialog_item_option_shoe_size_220_255_container);
                    setVisibility(View.VISIBLE, R.id.dialog_item_option_shoe_size_260_300_container);
                }
            }
        });

        rgSizeGroups = (RadioGroup) findViewById(R.id.dialog_item_option_size);
        rgSizeGroups.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.dialog_item_option_size_default) {
                    setSizeContainerVisibility(0);
                } else if (checkedId == R.id.dialog_item_option_size_0_12) {
                    setSizeContainerVisibility(1);
                } else if (checkedId == R.id.dialog_item_option_size_14_30) {
                    setSizeContainerVisibility(2);
                } else if (checkedId == R.id.dialog_item_option_size_32_42) {
                    setSizeContainerVisibility(3);
                } else if (checkedId == R.id.dialog_item_option_size_80_110) {
                    setSizeContainerVisibility(4);
                }
            }
        });
        sizeContainers.add((LinearLayout) findViewById(R.id.dialog_item_option_size_def_container));
        sizeContainers.add((LinearLayout) findViewById(R.id.dialog_item_option_size_0_12_container));
        sizeContainers.add((LinearLayout) findViewById(R.id.dialog_item_option_size_14_30_container));
        sizeContainers.add((LinearLayout) findViewById(R.id.dialog_item_option_size_32_42_container));
        sizeContainers.add((LinearLayout) findViewById(R.id.dialog_item_option_size_80_110_container));

        switch (categorySet) {
            case Shoes:
                setVisibility(View.GONE, R.id.dialog_item_option_size, R.id.dialog_item_option_check_container);
                setVisibility(View.VISIBLE, R.id.dialog_item_option_shoe_size, R.id.dialog_item_option_shoe_check_container);
                rgShoeSizeGroups.check(R.id.dialog_item_option_shoe_size_220_255);
                setVisibility(View.VISIBLE, R.id.dialog_item_option_shoe_size_220_255_container);
                setVisibility(View.GONE, R.id.dialog_item_option_shoe_size_260_300_container);
                break;

            default:
                setVisibility(View.VISIBLE, R.id.dialog_item_option_size, R.id.dialog_item_option_check_container);
                setVisibility(View.GONE, R.id.dialog_item_option_shoe_size, R.id.dialog_item_option_shoe_check_container);
                rgSizeGroups.check(R.id.dialog_item_option_size_default);
                setSizeContainerVisibility(0);
                break;
        }

        // CheckBox
        cbSizeTypes = new MyCheckBoxs<>(context, getView());
        for (SizeType sizeType : SizeType.values()) {
            if (sizeType == SizeType.Default)
                continue;
            CheckBox checkBox = MyView.getView(context, getView(), String.format("dialog_item_option_size_%s", sizeType.toString()));
            if (checkBox == null) {
                Log.e("DEBUG_JS", String.format("[BaseItemOptionDialog.setSizes] %s not found", sizeType.toString()));
                continue;
            }
            checkBox.setText(sizeType.uiName);
            MyView.setTextViewByDeviceSize(context, checkBox);
            cbSizeTypes.add(checkBox, sizeType);
        }
    }

    private void setSizeContainerVisibility(int index) {
        int position = 0;
        for (LinearLayout container : sizeContainers) {
            if (position == index)
                container.setVisibility(View.VISIBLE);
            else
                container.setVisibility(View.GONE);
            position++;
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_item_option_cancel) {
            onCancelClick();
        } else if (view.getId() == R.id.dialog_item_option_colorNameSetting) {
            ColorSettingView.startActivity(context, new MyOnClick() {
                @Override
                public void onMyClick(View view, Object record) {
                    refreshColor();
                }
            });

        } else if (view.getId() == R.id.dialog_item_option_sizeSetting) {
            final MyButtonsDialog myButtonsDialog = new MyButtonsDialog(context, "상품종류 선택", "선택한 상품의 사이즈들을 보실 수 있습니다");
            myButtonsDialog.addButton("의류 사이즈 보기", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setVisibility(View.VISIBLE, R.id.dialog_item_option_size, R.id.dialog_item_option_check_container);
                    setVisibility(View.GONE, R.id.dialog_item_option_shoe_size, R.id.dialog_item_option_shoe_check_container);
                    rgSizeGroups.check(R.id.dialog_item_option_size_default);
                    MyUI.toastSHORT(context, String.format("의류 사이즈로 변경되었습니다"));
                    myButtonsDialog.dismiss();
                }
            });
            myButtonsDialog.addButton("신발 사이즈 보기", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setVisibility(View.GONE, R.id.dialog_item_option_size, R.id.dialog_item_option_check_container);
                    setVisibility(View.VISIBLE, R.id.dialog_item_option_shoe_size, R.id.dialog_item_option_shoe_check_container);
                    rgShoeSizeGroups.check(R.id.dialog_item_option_shoe_size_220_255);
                    MyUI.toastSHORT(context, String.format("신발 사이즈로 변경되었습니다"));
                    myButtonsDialog.dismiss();
                }
            });
            myButtonsDialog.show();

        } else if (view.getId() == R.id.dialog_item_option_apply) {
            onApplyClick();
            dismiss();
        }
    }

    public void onCancelClick() {
        dismiss();
    }

    public void onApplyClick() {
        ItemDB itemDB = ItemDB.getInstance(context);
        ItemRecord itemRecord = itemDB.getRecordBy(itemId);
        List<ColorType> clickedColors = adapter.getSortedClickedRecords();
        List<SizeType> clickedSizes = cbSizeTypes.getCheckedItems();
        if (clickedColors.size() == 0)
            clickedColors.add(ColorType.Default);
        if (clickedSizes.size() == 0)
            clickedSizes.add(SizeType.Default);
        for (ColorType color : clickedColors) {
            for (SizeType size : clickedSizes) {
                itemDB.options.updateOrInsert(new ItemOptionRecord(itemRecord.itemId, color, size, false));
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        adapter.onItemClick(view, position);
        adapter.notifyDataSetChanged();
    }

}

