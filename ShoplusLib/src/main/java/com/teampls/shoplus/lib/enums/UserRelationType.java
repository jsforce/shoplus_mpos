package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2016-11-17.
 */

public enum UserRelationType {
    NOT_DEFINED("미지정"), PROVIDER("공급자"), CUSTOMER("고객");
    private String korStr = "";
    UserRelationType(String korStr) {
        this.korStr = korStr;
    }

    public String toKorString() {
        return korStr;
    }
}
