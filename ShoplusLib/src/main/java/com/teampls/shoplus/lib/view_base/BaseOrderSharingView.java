package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.OrderItemAdapter;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.OrderItemRecord;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;

import org.joda.time.DateTime;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-09-20.
 */

abstract public class BaseOrderSharingView extends BaseActivity {
    protected GridView gridView;
    protected OrderItemAdapter adapter;
    protected LinearLayout printView;
    protected TextView tvWriter, tvSummary, tvOrderer, tvReceiver, tvMemo;
    protected ScrollView container;
    protected static List<OrderItemRecord> orderItemRecords = new ArrayList<>(); // itemId, color, size별
    protected final String KEY_WRITER = "order.sharing.writer", KEY_RECEIVER = "order.sharing.receiver";
    protected Button btCancel;

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    public int getThisView() {
        return R.layout.activity_order_sharing;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        adapter = new OrderItemAdapter(context);
        adapter.setViewType(OrderItemAdapter.OrderSharingView);
        adapter.setRecordsByBinding(orderItemRecords);
        adapter.setRowHeights();

        printView = (LinearLayout) findViewById(R.id.order_sharing_container);
        gridView = (GridView) findViewById(R.id.order_sharing_gridview);
        gridView.setNumColumns(MyDevice.getOrderingColumnNum(context));
        gridView.setAdapter(adapter);

        findTextViewById(R.id.order_sharing_date, String.format("%s (%s) %s", DateTime.now().toString(BaseUtils.YMD_Kor_Format),
                BaseUtils.getDayOfWeekKor(DateTime.now()), DateTime.now().toString(BaseUtils.HS_Kor_Format)));
        tvWriter = setView(R.id.order_sharing_writer);
        tvOrderer = setView(R.id.order_sharing_orderer);
        tvReceiver = setView(R.id.order_sharing_receiver);
        tvMemo = setView(R.id.order_sharing_memo);
        container = setView(R.id.order_sharing_scrollview);
        int finalOrder = 0;
        for (OrderItemRecord orderItemRecord : orderItemRecords)
            finalOrder = finalOrder + orderItemRecord.finalOrder;
        tvSummary = findTextViewById(R.id.order_sharing_summary, String.format("%d건 %d개", adapter.getCount(), finalOrder));

        btCancel = findViewById(R.id.order_sharing_cancel);

        setOnClick(R.id.order_sharing_cancel, R.id.order_sharing_receiver, R.id.order_sharing_memo,
                R.id.order_sharing_writer, R.id.order_sharing_prev);
        refresh();
        refreshView();
    }

    protected void refreshView() {
        String writer = keyValueDB.getValue(KEY_WRITER);
        String receiver = keyValueDB.getValue(KEY_RECEIVER);
        tvWriter.setText(String.format("작성 : %s", writer));
        tvOrderer.setText(String.format("%s 발주서", userSettingData.getProviderRecord().name));
        tvReceiver.setText(String.format("%s 귀하", receiver));

        if (writer.isEmpty()) {
            onClick(tvWriter);
            return;
        } else if (receiver.isEmpty()) {
            onClick(tvReceiver);
            return;
        }
    }

    public void refresh() {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                adapter.generate();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
                gridView.getLayoutParams().height = MyDevice.toPixel(context, adapter.getHeightDp() + 5);
                new MyDelayTask(context, 1) {
                    @Override
                    public void onFinish() {
                        container.fullScroll(ScrollView.FOCUS_UP);
                    }
                };
            }
        };
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.order_sharing_prev) {
            doFinishForResult();
        } else if (view.getId() == R.id.order_sharing_cancel) {
            finishOrdering();
        } else if (view.getId() == R.id.order_sharing_receiver) {
            new UpdateValueDialog(context, "수신처 이름", "받으시는 곳 상호명을 입력해주세요", keyValueDB.getValue(KEY_RECEIVER)) {
                @Override
                public void onApplyClick(String newValue) {
                    keyValueDB.put(KEY_RECEIVER, newValue);
                    refreshView();
                }
            };
        } else if (view.getId() == R.id.order_sharing_memo) {
            new UpdateValueDialog(context, "메모 입력", "추가로 전달할 내용을 입력해주세요", tvMemo.getText().toString()) {
                @Override
                public void onApplyClick(String newValue) {
                    tvMemo.setText(newValue);
                }
            };
        } else if (view.getId() == R.id.order_sharing_writer) {
                new UpdateValueDialog(context, "작성자 이름", "작성하시는 분의 이름을 입력해주세요", keyValueDB.getValue(KEY_WRITER)) {
                    @Override
                    public void onApplyClick(String newValue) {
                        keyValueDB.put(KEY_WRITER, newValue);
                        refreshView();
                    }
                };
        }
    }


    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.download)
                .add(MyMenu.sendWechat)
                .add(MyMenu.sendKakao)
                .add(MyMenu.help)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case help:
                new OrderSharingUserGuide(context).show();
                break;
            case download:
                Bitmap bitmap = MyGraphics.toBitmap(printView);
                String filePath = MyDevice.createTempFilePath(BaseAppWatcher.appDir, "jpg");
                boolean successful = MyGraphics.toFile(bitmap, filePath);
                if (successful) {
                    MyDevice.refresh(context, new File(filePath));
                    MyUI.toastSHORT(context, String.format("%s 저장했습니다", filePath));
                }
                break;
            case sendWechat:
                MyUI.toast(context, String.format("위챗을 실행 중입니다. 고화질을 위해서는 '원본받기'를 하시도록 안내 해주세요"));
                MyDevice.sendWechat(context, MyGraphics.toBitmap(printView));
                break;
            case sendKakao:
                MyUI.toast(context, String.format("카톡을 실행 중입니다. 고화질을 위해서 카톡에서 '원본' 전송 옵션으로 설정해 주세요"));
                MyDevice.sendKakao(context, MyGraphics.toBitmap(printView));
                break;
            case close:
                finishOrdering();
                break;
        }
    }

    abstract public void finishOrdering();

    class OrderSharingUserGuide extends BaseDialog {

        public OrderSharingUserGuide(Context context) {
            super(context);
            setSize(0.95, -1);
            setOnClick(R.id.dialog_order_sharing_close);
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_image_quality_guide;
        }

        @Override
        public void onClick(View v) {
            dismiss();
        }
    }


}
