package com.teampls.shoplus.lib.datatypes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.enums.SlipItemType;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 거래기록의 판매/반품내역조회 결과 데이터 클래스
 * 특정 아이템으로 거래기록을 검색했을 때의 데이터를 반환하기 위해 사용한다.
 *
 * @author lucidite
 */

public class TransactionLookupData implements Comparable<TransactionLookupData> {
    private String transactionId;
    private Boolean cancellation;
    private String buyer;
    private SlipItemType transactionItemType;
    private ColorType color;
    @Nullable
    private String customColorName;
    private SizeType size;
    private int quantity;
    public boolean cancelled = false; // 이후 취소되었을때 이를 표시

    public TransactionLookupData()  {
    }

    public TransactionLookupData(JSONObject dictData) throws JSONException {
        this.transactionId = dictData.getString("tr_id");
        this.cancellation = dictData.optBoolean("cancel", false);
        this.buyer = dictData.getString("buyer");
        this.transactionItemType = SlipItemType.getFromRemoteTypeString(dictData.getString("t"));

        this.color = ColorType.ofRemoteStr(dictData.optString("c"));
        this.customColorName = dictData.optString("a");
        this.size = SizeType.ofRemoteStr(dictData.optString("s"));
        this.quantity = dictData.getInt("q");
    }

    // exodar에서 아이템 사간 고객들 조회 후 미송정보까지 원할때 사용, 임시로만 사용한다
    public TransactionLookupData(SlipRecord slipRecord, SlipItemRecord slipItemRecord) {
        this.transactionId = APIGatewayHelper.getRemoteDateTimeString(slipItemRecord.salesDateTime);
        this.cancellation = slipRecord.cancelled;
        this.buyer = slipRecord.counterpartPhoneNumber;
        this.transactionItemType = slipItemRecord.slipItemType;
        this.color = slipItemRecord.color;
        this.customColorName = slipItemRecord.getCustomColorName();
        this.size = slipItemRecord.size;
        this.quantity = slipItemRecord.quantity;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] id %s, %s, %s, color %s (%s), size %s x %d", location,
                transactionId, buyer, transactionItemType.toString(), color.getOriginalUiName(), customColorName, size.uiName, quantity));
    }

    public String getTransactionId() {
        return transactionId;
    }

    public DateTime getTransactionDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.transactionId);
    }

    public Boolean isCancellation() {
        return cancellation;
    }

    public String getBuyer() {
        return buyer;
    }

    public SlipItemType getType() {
        return transactionItemType;
    }

    /**
     * 옵션 컬러 데이터. 사전에 약속된 컬러 code 문자열로부터 변환한 데이터로,
     * 사용자 정의 컬러 이름 데이터 유무와 상관없이 ColorType의 컬러 옵션 정보를 반환한다.
     *
     * @return
     */
    public ColorType getOptionColor() {
        return color;
    }

    /**
     * 거래(전표) 항목의 판매아이템 컬러 이름.
     * 사용자 지정 컬러 이름이 있는 경우 이를 반환하고, 없는 경우 기본 컬러셋의 이름을 반환한다.
     * @return
     */
    public String getOptionColorName() {
        if (this.customColorName != null && !this.customColorName.isEmpty()) {
            return this.customColorName;
        } else {
            return this.color.getOriginalUiName();
        }
    }

    public SizeType getOptionSize() {
        return size;
    }

    public int getQuantity() {
        return quantity;
    }

    public SlipDataKey getSlipKey(Context context) {
        switch (UserSettingData.getInstance(context).getUserConfigType()) {
            case BUYER:
                Log.e("DEBUG_JS", String.format("[TransactionLookupData.getSlipDataKey] user config type == BUYER"));
                break;
            case PROVIDER:
                break;
        }
        return new SlipDataKey(UserSettingData.getInstance(context).getProviderRecord().phoneNumber, getTransactionDateTime());
    }

    @Override
    public int compareTo(@NonNull TransactionLookupData record) {
        if (getTransactionDateTime().isEqual(record.getTransactionDateTime()))
            return 0;
        return getTransactionDateTime().isAfter(record.getTransactionDateTime())? -1 : 1; // 시간역순
    }

    public String getQuantityStr(Context context) {
        if (GlobalDB.getInstance(context).getBool(GlobalDB.KEY_RECEIPT_UseHangulQuantity)) {
            return String.format("%d개", quantity);
        } else {
            return String.format("x %d", quantity);
        }
    }
}
