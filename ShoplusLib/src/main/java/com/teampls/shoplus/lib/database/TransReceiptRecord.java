package com.teampls.shoplus.lib.database;

import android.util.Log;

import com.teampls.shoplus.lib.enums.TransType;

/**
 * Created by Medivh on 2017-10-18.
 */

public class TransReceiptRecord {
    public int id = 0, receipt = 0;
    public String slipKey = "";
    public TransType transType = TransType.receipt;

    public TransReceiptRecord() {};

    public TransReceiptRecord(int id, String slipKey, int receipt, TransType transType) {
        this.id = id;
        this.slipKey = slipKey;
        this.receipt = receipt;
        this.transType = transType;
    }

    public boolean isSame(TransReceiptRecord record) {
        return BaseRecord.isSame(this, record);
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %s, %d, %s", callLocation, slipKey, receipt, transType));
    }
}
