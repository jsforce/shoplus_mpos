package com.teampls.shoplus.lib.database_map;

import android.content.Context;

/**
 * Created by Medivh on 2019-01-15.
 */

abstract public class BaseDBValue<T> {
    public String key = "";
    public T defaultValue;
    protected KeyValueDB keyValueDB;

    public BaseDBValue(String key, T defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    abstract public KeyValueDB getDB(Context context);

    abstract public  void put(Context context, T value);

    abstract public T get(Context context);

    public boolean isExist(Context context) {
        return getDB(context).hasKey(key);
    }

}
