package com.teampls.shoplus.lib.database_global;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.enums.DBResultType;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


public class LocalUserDB extends BaseDB<LocalUserRecord> {
    private static LocalUserDB instance = null;
    protected static int Ver = 1;
    private boolean updateFound = false;

    public enum Column {
        _id, displayName, phoneNumber, username, showing;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    public static LocalUserDB getInstance(Context context) {
        if (instance == null)
            instance = new LocalUserDB(context);
        return instance;
    }

    protected LocalUserDB(Context context) {
        super(context, "UserDB", Ver, Column.toStrs());
    }

    public LocalUserDB(Context context, String DBname, int DBversion, String[] columns) {
        super(context, DBname, DBversion, columns);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    protected int getId(LocalUserRecord record) {
        return record.id;
    }

    @Override
    public int delete(int id) {
        String name = getString(Column.displayName, id);
        String phoneNumber = getString(Column.phoneNumber, id);
        Log.w("DEBUG_JS", String.format("[%s] %d, %s, %s, is deleted", getClass().getSimpleName(), id, name, phoneNumber));
        return super.delete(id);
    }

    public void delete(String phoneNumber) {
        LocalUserRecord record = getRecord(phoneNumber);
        if (record.id > 0)
            delete(record.id);
    }

    protected ContentValues toContentvalues(LocalUserRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.displayName.toString(), record.displayName);
        contentvalues.put(Column.phoneNumber.toString(), record.phoneNumber);
        contentvalues.put(Column.username.toString(), record.username);
        contentvalues.put(Column.showing.toString(), record._showing);
        return contentvalues;
    }

    @Override
    protected void toLogCat(int id, LocalUserRecord record, int option, String operation) {
        if (option == 0) {
            Log.i("DEBUG_JS", String.format("[%s] %s, phone %s, showing? %s", operation,
                    record.displayName, record.phoneNumber, Boolean.toString(record._showing)));
        }
    }

    public boolean has(String phoneNumber) {
        return hasValue(Column.phoneNumber, phoneNumber);
    }

    public DBResult update(LocalUserRecord record) {
        if (record.id == 0)
            record.id = getRecord(record.phoneNumber).id;
        return update(record.id, record);
    }

    public boolean isUpdateFound() {
        return updateFound;
    }

    public boolean refresh(Map<String, ContactDataProtocol> protocols) {
        updateFound = false;
        boolean updated = false;
        boolean doShowLogCat = (this.getSize() >= 1);
        List<String> validPhoneNumbers = new ArrayList<>();
        for (String phoneNumber : protocols.keySet()) {
            if (phoneNumber.isEmpty()) continue;
            ContactDataProtocol protocol = protocols.get(phoneNumber);
            LocalUserRecord userRecord = new LocalUserRecord(protocol.getName(), phoneNumber);

            DBResult result = updateOrInsert(userRecord);
            if (result.resultType.updatedOrInserted()) {
                updated = true;
                updateFound = true;
            }
            validPhoneNumbers.add(phoneNumber);
            if ((result.resultType == DBResultType.INSERTED) && doShowLogCat)
                Log.w("DEBUG_JS", String.format("[LocalUserDB.refreshDay] %s (%s) is newly added...", protocol.getName(), phoneNumber));
        }
        deleteInvalid(validPhoneNumbers); // 서버 정보와 같도록 유지
        return updated;
    }

    public DBResult updateOrInsert(LocalUserRecord record) {
        LocalUserRecord dbRecord = getRecord(record.phoneNumber);
        if (dbRecord.id > 0) {
            if (dbRecord.isSame(record) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public LocalUserRecord getRecord(String phoneNumber) {
        LocalUserRecord result = getRecord(getId(Column.phoneNumber, phoneNumber));
        return result;
    }

    public void toLogCat(int id) {
        for (LocalUserRecord record : getRecords())
            toLogCat(record.id, record, id, "LocalUserDB.show");
    }

    public void toLogCat(String location) {
        if (getSize() == 0) {
            Log.i("DEBUG_JS", String.format("[%s] userDB == empty", location));
            return;
        }
        for (LocalUserRecord record : getRecords())
            record.toLogCat(location);
    }

    public void toLogCat(LocalUserRecord record, int option, String location) {
        toLogCat(record.id, record, option, location);
    }

    public static ArrayList<LocalUserRecord> remove(ArrayList<LocalUserRecord> records, String phoneNumber) {
        for (LocalUserRecord record : records) {
            if (record.phoneNumber.equals(phoneNumber)) {
                records.remove(record);
                break;
            }
        }
        return records;
    }

    @Override
    protected LocalUserRecord getEmptyRecord() {
        return new LocalUserRecord();
    }

    @Override
    protected LocalUserRecord createRecord(Cursor cursor) {
        return new LocalUserRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.displayName.ordinal()),
                cursor.getString(Column.phoneNumber.ordinal()),
                cursor.getString(Column.username.ordinal()),
                BaseUtils.toBoolean(cursor.getInt(Column.showing.ordinal()))
        );
    }

    public List<LocalUserRecord> getSameNameAndPhoneRecords() {
        List<LocalUserRecord> results = new ArrayList<>(0);
        for (LocalUserRecord record : getRecords()) {
            if (record.phoneNumber.isEmpty()) continue;
            if (record.phoneNumber.equals(BaseUtils.toSimpleForm(record.displayName)))
                results.add(record);
        }
        return results;
    }

    public void deleteInvalid(List<String> validPhoneNumbers) {
        deleteInvalid(validPhoneNumbers, false);
    }

    public void deleteInvalid(List<String> validPhoneNumbers, boolean doKeepEmptyPhoneNumber) {
        if (doKeepEmptyPhoneNumber)
            validPhoneNumbers.add("");
        List<String> invalidsPhoneNumbers = getStrings(Column.phoneNumber);
        invalidsPhoneNumbers.removeAll(validPhoneNumbers);
        deleteAll(Column.phoneNumber, invalidsPhoneNumbers);
    }

    public static List<LocalUserRecord> sortByName(List<LocalUserRecord> records) {
        String indexName, targetName;
        for (int i = 0; i < records.size(); i++) {
            int index = i;
            indexName = records.get(i).displayName;
            for (int j = i + 1; j < records.size(); j++) {
                targetName = records.get(j).displayName;
                //Log.i("DEBUG_JS", String.format("[LocalUserDB.sortByName] index (%d) %s, target (%d) %s, compare %d", index, indexName, j, targetName, targetName.compareTo(indexName)));
                if ((targetName.compareTo(indexName)) < 0) {
                    index = j;
                    indexName = targetName;
                }
            }
            if (index != i) {
                LocalUserRecord foreNameRecord = records.get(index);
                records.set(index, records.get(i));
                records.set(i, foreNameRecord);
            }
            //   Log.w("DEBUG_JS", String.format("[LocalUserDB.sortByName] %dth : foreName %s (%d)", i, foreNameRecord.name, index));
        }
        return records;
    }

    public static List<LocalUserRecord> sortByCount(List<LocalUserRecord> records, Map<String, Integer> counts) {
        int indexDealCount = 0, targetDealCount = 0;
        for (int i = 0; i < records.size(); i++) {
            int index = i;
            if (counts.containsKey(records.get(index).phoneNumber)) {
                indexDealCount = counts.get(records.get(index).phoneNumber);
            } else {
                indexDealCount = 0;
            }
            for (int j = i + 1; j < records.size(); j++) {
                if (counts.containsKey(records.get(j).phoneNumber)) {
                    targetDealCount = counts.get(records.get(j).phoneNumber);
                } else {
                    targetDealCount = 0;
                }
                if (targetDealCount > indexDealCount) {
                    index = j;
                    indexDealCount = targetDealCount;
                }
            }
            if (index != i) {
                LocalUserRecord latestRecord = records.get(index);
                records.set(index, records.get(i));
                records.set(i, latestRecord);
            }
        }
        return records;
    }

    public static List<LocalUserRecord> sortByDate(List<LocalUserRecord> records, Map<String, DateTime> dateTimes) {
        DateTime indexDate = Empty.dateTime, targetDate = Empty.dateTime;

        for (int i = 0; i < records.size(); i++) {
            int index = i;
            if (dateTimes.containsKey((records.get(index).phoneNumber))) {
                indexDate = dateTimes.get(records.get(index).phoneNumber);
            } else {
                indexDate = Empty.dateTime;
            }
            for (int j = i + 1; j < records.size(); j++) {
                if (dateTimes.containsKey((records.get(j).phoneNumber))) {
                    targetDate = dateTimes.get(records.get(j).phoneNumber);
                } else {
                    targetDate = Empty.dateTime;
                }
                //Log.w("DEBUG_JS", String.format("[LocalUserDB.sortByPurchaseDate] index %d date %s, j = %d date %s, after? %s ... index = j, index, indexDate.toSimpleString(MyUtils.fullFormat), j, targetDate.toSimpleString(MyUtils.fullFormat), Boolean.toSimpleString(targetDate.isAfter(indexDate))));
                if (targetDate.isAfter(indexDate)) {
                    index = j;
                    indexDate = targetDate;
                }
            }
            if (index != i) {
                LocalUserRecord latestRecord = records.get(index);
                records.set(index, records.get(i));
                records.set(i, latestRecord);
            }
        }
        return records;
    }

    public void cleanUp() {
        for (LocalUserRecord record : getRecords())
            if (record.isEmpty())
                delete(record.id);
    }

    public MyMap<String, String> getNameMap() { // phoneNumber - name
        cleanUp();
        MyMap<String, String> results = new MyMap("");
        for (LocalUserRecord record : getRecords())
            results.put(record.phoneNumber, record.getName());
        return results;
    }

    public Map<String, String> getNameMap(List<String> phoneNumbers) { // phoneNumber - name
        cleanUp();
        Map<String, String> results = new HashMap<>();
        phoneNumbers = new ArrayList(new HashSet(phoneNumbers));
        phoneNumbers.remove("");
        for (LocalUserRecord userRecord : getRecordsIN(Column.phoneNumber, phoneNumbers, null))
            results.put(userRecord.phoneNumber, userRecord.getName());
        return results;
    }

    public MyMap<String, LocalUserRecord> getMap() { // phoneNumber - name
        cleanUp();
        MyMap<String, LocalUserRecord> results = new MyMap<>(new LocalUserRecord());
        for (LocalUserRecord userRecord : getRecords())
            results.put(userRecord.phoneNumber, userRecord);
        return results;
    }

}
