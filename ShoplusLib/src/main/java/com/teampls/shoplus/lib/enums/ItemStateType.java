package com.teampls.shoplus.lib.enums;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 판매 아이템 상태
 *
 * @author lucidite
 */

public enum ItemStateType {
    PREPARING("p", "준비중"),
    DISPLAYED("d", "판매중"),
    STORED("s", "재고"),
    SOLDOUT("o", "판매완료");

    private String remoteStr;
    private String uiStr;

    ItemStateType(String remoteStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }

    public String toUIStr() {
        return this.uiStr;
    }

    public List<ItemStateType> toList() {
        List<ItemStateType> results = new ArrayList<>();
        results.add(this);
        return results;
    }

    public ItemStateType getNext() {
        if (this.ordinal() == values().length-1) {
            return values()[0];
        } else {
            return values()[this.ordinal()+1];
        }
    }

    public boolean isEnd() {
        if (values().length >= 1)
            return this == values()[values().length-1];
        else
            return true;
    }

    public static ItemStateType fromRemoteStr(String remoteStr) {
        // 해당하는 값이 없을 경우 기본값으로 판매중 상태를 반환한다.
        if (remoteStr == null) {
            return ItemStateType.DISPLAYED;
        }
        for (ItemStateType state: ItemStateType.values()) {
            String compareTo = state.toRemoteStr();
            if (compareTo.equals(remoteStr)) {
                return state;
            }
        }
        return ItemStateType.DISPLAYED;
    }

    public static String toRemoteParam(Set<ItemStateType> states) {
        String param = "";
        for (ItemStateType state: states) {
            param += state.toRemoteStr();
        }
        return param;
    }

    public static List<ItemStateType> getAll() {
        List<ItemStateType> results = new ArrayList<>();
        for (ItemStateType itemStateType : values())
            results.add(itemStateType);
        return results;
    }
}
