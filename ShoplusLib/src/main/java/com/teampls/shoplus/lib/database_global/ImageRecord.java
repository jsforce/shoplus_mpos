package com.teampls.shoplus.lib.database_global;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.datatypes.MerchandiseData;
import com.teampls.shoplus.lib.protocol.ImageDataProtocol;

import java.io.File;
import java.io.InputStream;

public class ImageRecord implements ImageDataProtocol {

    public int id = 0, itemId = 0, imageId = 0; // imageId : 신상알림 sub image 구별용
    public String localPath = "", remotePath = "";
    public byte[] image = Empty.bytes;

    public ImageRecord() {
    };

    public ImageRecord(int itemId, String remotePath, ImageDataProtocol protocol) {
        this.itemId = itemId;
        this.remotePath = remotePath;
        this.image = protocol.getByteArray();
    }

    public ImageRecord(MerchandiseData data, ImageDataProtocol protocol) {
        this.itemId = data.getItemid();
        this.remotePath = data.getMainImagePath();
        this.image = protocol.getByteArray();

    }

    public ImageRecord(int id, int itemId, int imageId, String localPath, String remotePath, byte[] image) {
        this.id = id;
        this.itemId = itemId;
        this.imageId = imageId;
        this.localPath = localPath;
        this.remotePath = remotePath;
        this.image = image;
    }

    public ImageRecord(int itemId, String localPath, String remotePath, byte[] image) {
        this.itemId = itemId;
        this.localPath = localPath;
        this.remotePath = remotePath;
        this.image = image;
    }

    public ImageRecord(int itemId, String localPath) {
        this.itemId = itemId;
        this.localPath = localPath;
    }

    public ImageRecord(String localPath, String remotePath, byte[] image) {
        this.localPath = localPath;
        this.remotePath = remotePath;
        this.image = image;
    }

    public ImageRecord(String remotePath, byte[] image) {
        this.remotePath = remotePath;
        this.image = image;
    }

    public boolean isSame(ImageRecord record) {
        if (itemId != record.itemId) return false;
        if (imageId != record.imageId) return false;
        if (localPath.equals(record.localPath) == false) return false;
        if (remotePath.equals(record.remotePath) == false) return false;
        // byte는 조사하지 않는다.
        return true;
    }


    public ImageRecord(int itemId, byte[] image, String localPath) {
        this.itemId = itemId;
        this.localPath = localPath;
        MyGraphics.setExifOrientation(localPath);
        this.image = image;
    }

    // from server data
    public ImageRecord(int itemId, String remotePath, InputStream stream) {
        this.itemId = itemId;
        this.remotePath = remotePath;
        this.image = MyGraphics.toByte(stream); // 주의! stream에 담긴 내용은 읽는 순간 없어진다. (1회용)
    }

    @Override
    public byte[] getByteArray() {
        return image;
    }

    public void setImage(Bitmap bitmap) {
        this.image = MyGraphics.toByte(bitmap);
    }

    public Bitmap getBitmap() {
        return MyGraphics.toBitmap(image);
    }

    public Bitmap getBitmap(int sampling) {
        return MyGraphics.toBitmap(image, sampling);
    }

    public Bitmap getStandardBitmap(int sampling) {
        Bitmap bitmap = getBitmap();
        int refWidth = MyGraphics.getStandardImageWidth(bitmap.getWidth(), bitmap.getHeight()); // 720 (or 960)
        if (bitmap.getWidth() < refWidth) { // 이미지 크기가 기준 크기에 미달하면
    //        Log.i("DEBUG_JS", String.format("[ImageRecord.getStandardBitmap] image %dx%d (requested sampling %d)",bitmap.getWidth(), bitmap.getHeight(), sampling));
            return bitmap;
        } else {
            return getBitmap(sampling);
        }
    }

    @Override
    public String saveTo(File localTargetDir, String filename, Bitmap.CompressFormat compressFormat) {
        boolean result = MyGraphics.toFile(image, localTargetDir+filename);
        if (result) {
            return localTargetDir+filename;
        } else {
            return "";
        }
    }

    public boolean saveAsFile(Context context) {
        return saveAsFile(context, true, BasePreference.isDownloadToCameraFolder(context));
    }

    public boolean saveAsFile(Context context, boolean doShowToast) {
        return saveAsFile(context, doShowToast, BasePreference.isDownloadToCameraFolder(context));
    }

    private boolean saveAsFile(Context context, boolean doShowToast, boolean doSaveCameraFolder) {
        if (image.length <= 0) {
            MyUI.toastSHORT(context, String.format("이미지 정보가 없습니다"));
            return false;
        }
        String dir = BaseAppWatcher.appDir;
        if (doSaveCameraFolder)
            dir = MyDevice.cameraDir;
        String filePath = MyDevice.createCurrentTimeFilePath(dir,"s","jpg");
        boolean result = MyGraphics.toFile(image, filePath);
        if (result) {
            if (doShowToast)
                MyUI.toastSHORT(context, String.format("%s 폴더에 저장", dir.replace(MyDevice.rootDir,"")));
            MyDevice.refresh(context, new File(filePath));
        } else {
            Log.e("DEBUG_JS", String.format("[QbImageRecord.saveAsFile] %s, failed %s", context.getClass().getSimpleName(), filePath));
        }
        return result;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] id %d, itemId %d, imageId %d, local %s, remote %s, imageSize %d",
                location, id, itemId, imageId, localPath, remotePath, (int) image.length/1000));
    }

}
