package com.teampls.shoplus.lib.database;

import com.teampls.shoplus.lib.common.Empty;

import org.joda.time.DateTime;

public class MonthlyCounterpartRecord {
    public int id = 0, quantity = 0, amount = 0;
    public boolean isSumup = false;
    public DateTime monthDateTime = Empty.dateTime;
    public String phoneNumber = "", name = "";

    public MonthlyCounterpartRecord() {
    }

    public MonthlyCounterpartRecord(int id, String phoneNumber, String name, int amount, int quantity, DateTime monthDateTime, boolean isSumup) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.amount = amount;
        this.quantity = quantity;
        this.monthDateTime = monthDateTime;
        this.isSumup = isSumup;
    }

    public MonthlyCounterpartRecord(DateTime monthDateTime, String phoneNumber, String name, SlipSummaryRecord summaryRecord) {
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.amount = summaryRecord.amount;
        this.quantity = summaryRecord.quantity;
        this.monthDateTime = monthDateTime;
        this.isSumup = false;
    }

    public boolean isSame(MonthlyCounterpartRecord record) {
        return BaseRecord.isSame(this, record);
    }
}
