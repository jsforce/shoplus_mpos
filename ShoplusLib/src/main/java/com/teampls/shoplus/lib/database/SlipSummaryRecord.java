package com.teampls.shoplus.lib.database;

import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.enums.SlipItemType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2016-09-07.
 */
public class SlipSummaryRecord {
    public int count = 0, amount = 0, quantity = 0, traceIdCount = 0, tradeSum = 0; // 영수증 라인 수, amount = 금액합산 quantity = 상품개수 합산, tradeSum = 택배비, 부가세등 제외
    public Set<SlipItemType> slipItemTypes = new HashSet<>();
    public Set<Integer> itemIds = new HashSet<>();
    public Set<String> slipKeys = new HashSet<>();
    public List<SlipItemRecord> returnAndDepositItems = new ArrayList<>();
    public int salesQuantity = 0, advanceQuantity = 0, preorderQuantity = 0, consignQuantity = 0, sampleQuantity = 0,
            returnQuantity = 0, depositQuantity = 0, etcQuantity = 0, clearQuantity = 0, withdrawalQuantity = 0, discountQuantity = 0,
            advanceClearQuantity = 0;
    public int salesSum = 0, advanceSum = 0, preorderSum = 0, consignSum = 0, sampleSum = 0,
            returnSum = 0, depositSum = 0, etcSum = 0, clearSum = 0, withdrawalSum = 0, discountSum = 0,
            advanceClearSum = 0;
    public int plusSum = 0, minusSum = 0, zeroSum = 0, plusQuantity = 0, minusQuantity = 0; // 금액별 합산
    public int stockPlusQuantity = 0, stockMinusQuantity = 0, stockAdvancePreorderQuantity = 0; // 재고별 합산

    public void toLogCat(String callLocatin) {
        Log.i("DEBUG_JS", String.format("[%s] items: %s, amount %d, quantity %d", callLocatin, BaseUtils.toStr(new ArrayList(itemIds)), amount, quantity));
    }

    public Pair<Integer, Integer> getSalesSum() { // 판매, 미송, 샘플/위탁/주문 완료 .. 미송은 잡힌 시점으로 계산
        return Pair.create(salesQuantity + advanceQuantity + clearQuantity, salesSum + advanceSum + clearSum); //
    }

    public Pair<Integer, Integer> getReturnsSum() { // 매입은 금액상으로는 0이지만 실제 반품이 이루어진 것이므로 포함, 대신 withdrawal을 뺀다
        return Pair.create(returnQuantity + depositQuantity, returnSum + depositSum);
    }

    public SlipSummaryRecord() {
    }

    public SlipSummaryRecord(List<SlipItemRecord> records) {
        this(records, new ArrayList<String>());
    }

    public SlipSummaryRecord(List<SlipItemRecord> records, List<String> keywordsNoQuantity) {
        boolean doCheckKeyword = keywordsNoQuantity.size() >= 1;
        List<SlipItemType> noQuantityTypes = SlipItemType.getNoQuantityTypes();
        for (SlipItemRecord slipItemRecord : records) {

            traceIdCount = traceIdCount + slipItemRecord.traceIds.size();
            amount = amount + slipItemRecord.quantity * slipItemRecord.unitPrice * slipItemRecord.slipItemType.getSign();
            if (slipItemRecord.slipItemType.isTradeType()) // 택배비, 부가세 제외 금액
                tradeSum = tradeSum + slipItemRecord.quantity * slipItemRecord.unitPrice * slipItemRecord.slipItemType.getSign();
            if (doCheckKeyword) {
                if (noQuantityTypes.contains(slipItemRecord.slipItemType) || keywordsNoQuantity.contains(slipItemRecord.name)) {
                    // no count
                } else {
                    quantity = quantity + slipItemRecord.quantity;
                }
            } else {
                quantity = quantity + slipItemRecord.quantity;
            }
            slipItemTypes.add(slipItemRecord.slipItemType);

            int rowAmount = slipItemRecord.quantity * slipItemRecord.unitPrice;
            switch (slipItemRecord.slipItemType) {
                // plus
                case SALES:
                    salesSum = salesSum + rowAmount;
                    salesQuantity = salesQuantity + slipItemRecord.quantity;
                    break;
                case ADVANCE:
                    advanceSum = advanceSum + rowAmount;
                    advanceQuantity = advanceQuantity + slipItemRecord.quantity;
                    break;
                case SAMPLE_CLEAR:
                case CONSIGN_CLEAR:
                case PREORDER_CLEAR:
                    clearSum = clearSum + rowAmount;
                    clearQuantity = clearQuantity + slipItemRecord.quantity;
                    break;

                // minus
                case RETURN:
                    returnSum = returnSum + rowAmount;
                    returnQuantity = returnQuantity + slipItemRecord.quantity;
                    returnAndDepositItems.add(slipItemRecord);
                    break;
                case WITHDRAWAL:
                case ADVANCE_SUBTRACTION:
                    withdrawalSum = withdrawalSum + rowAmount;
                    withdrawalQuantity = withdrawalQuantity + slipItemRecord.quantity;
                    break;
                case DISCOUNT:
                    discountSum = discountSum + rowAmount;
                    discountQuantity = discountQuantity + slipItemRecord.quantity;
                    break;

                // etc
                case DEPOSIT:
                    depositSum = depositSum + rowAmount;
                    depositQuantity = depositQuantity + slipItemRecord.quantity;
                    returnAndDepositItems.add(slipItemRecord);
                    break;
                case PREORDER:
                case PREORDER_OUT_OF_STOCK:
                    preorderSum = preorderSum + rowAmount;
                    preorderQuantity = preorderQuantity + slipItemRecord.quantity;
                    break;
                case CONSIGN:
                    consignSum = consignSum + rowAmount;
                    consignQuantity = consignQuantity + slipItemRecord.quantity;
                    break;
                case SAMPLE:
                    sampleSum = sampleSum + rowAmount;
                    sampleQuantity = sampleQuantity + slipItemRecord.quantity;
                    break;
                case ADVANCE_CLEAR:
                    advanceClearSum = advanceClearSum + rowAmount;
                    advanceClearQuantity = advanceClearQuantity + slipItemRecord.quantity;

                case MISC_EXPENSES: // 택배비
                case VALUE_ADDED_TAX: // 부가세
                default:
                    etcSum = etcSum + rowAmount;
                    etcQuantity = etcQuantity + slipItemRecord.quantity;
                    break;
            }

            itemIds.add(slipItemRecord.itemId);
            slipKeys.add(slipItemRecord.slipKey);
            count = count + 1;

            switch (slipItemRecord.slipItemType.getSign()) {
                case 1:
                    plusSum = plusSum + rowAmount;
                    plusQuantity = plusQuantity + slipItemRecord.quantity;
                    break;
                case -1:
                    minusSum = minusSum - rowAmount;
                    minusQuantity = minusQuantity + slipItemRecord.quantity;
                    break;
                case 0:
                    zeroSum = zeroSum + rowAmount;
                    break;
            }

            switch (slipItemRecord.slipItemType.getStockSign()) {
                case 1:
                    stockPlusQuantity = stockPlusQuantity + slipItemRecord.quantity;
                    break;
                case -1:
                    stockMinusQuantity = stockMinusQuantity + slipItemRecord.quantity;
                    // 미송, 주문 배송을 따로 집계 (바로 나가는 물류라서 그러는 것 같음)
                    if (slipItemRecord.slipItemType == SlipItemType.ADVANCE_CLEAR || slipItemRecord.slipItemType == SlipItemType.PREORDER_CLEAR)
                        stockAdvancePreorderQuantity = stockAdvancePreorderQuantity + slipItemRecord.quantity;
                    break;
                case 0:
                    break;
            }
            //Log.i("DEBUG_JS", String.format("[SlipItemDB.getSummary] slipKey %d, conservative %d, rowAmount %d (%d), type %s", slipKey, slipItemRecord.consecutiveNumber, rowAmount, rowAmount,  slipItemRecord.slipItemType.uiName));
        }
    }

    public int getStockQuantityExceptAdvance() {
        return stockPlusQuantity - stockMinusQuantity + stockAdvancePreorderQuantity;
    }

    public int getAdvanceStockQuantity() {
        return stockAdvancePreorderQuantity;
    }

    public List<String> toStringListPendings() {
        List<String> results = new ArrayList<>();
        if (advanceQuantity > 0)
            results.add(String.format("- 미송 %d건, %s", advanceQuantity, BaseUtils.toCurrencyStr(advanceSum)));
        if (preorderQuantity > 0)
            results.add(String.format("- 주문 %d건, %s", preorderQuantity, BaseUtils.toCurrencyStr(preorderSum)));
        if (consignQuantity > 0)
            results.add(String.format("- 위탁 %d건, %s", consignQuantity, BaseUtils.toCurrencyStr(consignSum)));
        if (sampleQuantity > 0)
            results.add(String.format("- 샘플 %d건, %s", sampleQuantity, BaseUtils.toCurrencyStr(sampleSum)));
        if (depositQuantity > 0)
            results.add(String.format("- 매입 %d건, %s", depositQuantity, BaseUtils.toCurrencyStr(depositSum)));
        return results;
    }

    public List<String> toStringListSignSum() {
        List<String> results = new ArrayList<>();
        results.add(String.format("(+) 총액 : %s", BaseUtils.toCurrencyStr(plusSum)));
        results.add(String.format("(-) 총액 : -%s", BaseUtils.toCurrencyStr(minusSum)));
        results.add(String.format("(0) 총액 : (%s)", BaseUtils.toCurrencyStr(zeroSum)));

        results.add("\n== 참고 ==");
        if (salesQuantity > 0)
            results.add(String.format("판매 %d개 :  %s", salesQuantity, BaseUtils.toCurrencyStr(salesSum)));
        if (advanceQuantity > 0)
            results.add(String.format("미송 %d개 : %s", advanceQuantity, BaseUtils.toCurrencyStr(advanceSum)));
        if (clearQuantity > 0)
            results.add(String.format("기타결제 %d개 : %s", clearQuantity, BaseUtils.toCurrencyStr(clearSum)));

        if (returnSum > 0)
            results.add(String.format("반품 %d개 : -%s", returnQuantity, BaseUtils.toCurrencyStr(returnSum)));
        if (withdrawalSum > 0)
            results.add(String.format("빼기/환불 %d개, -%s", withdrawalQuantity, BaseUtils.toCurrencyStr(withdrawalSum)));
        if (discountSum > 0)
            results.add(String.format("할인 %d개 : -%s", discountQuantity, BaseUtils.toCurrencyStr(discountSum)));

        if (preorderQuantity > 0)
            results.add(String.format("주문 %d개 : (%s)", preorderQuantity, BaseUtils.toCurrencyStr(preorderSum)));
        if (sampleQuantity > 0)
            results.add(String.format("샘플 %d개 : (%s)", sampleQuantity, BaseUtils.toCurrencyStr(sampleSum)));
        if (consignQuantity > 0)
            results.add(String.format("위탁 %d개 : (%s)", consignQuantity, BaseUtils.toCurrencyStr(consignSum)));

        return results;
    }


    public int getTax() {
        return Math.round((amount - etcSum)*0.1f);
    }
}
