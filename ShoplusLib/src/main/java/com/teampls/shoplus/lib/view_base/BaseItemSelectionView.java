package com.teampls.shoplus.lib.view_base;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.dialog.SortingOrderDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.enums.ItemSortingKey;
import com.teampls.shoplus.lib.event.MyOnClick;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-10-07.
 */

abstract public class BaseItemSelectionView extends BaseItemsView implements View.OnClickListener {
    protected TextView tvSelectionCount, tvSorting;
    protected ItemDB itemDB;
    protected Button btApply, btFunction;
    private static final String KEY_Filter_Index = "itemSelectionView.filter.index";
    protected String KEY_SEARCH = "";
    protected List<ItemSortingKey> sortingKeys = new ArrayList<>();
    protected ItemSortingKey selectedSortingKey = ItemSortingKey.ItemId;
    protected RadioButton rbButton1, rbButton2, rbButton3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        tvSelectionCount = findTextViewById(R.id.item_selection_headerCount,"");
        tvSorting = findTextViewById(R.id.item_selection_sorting);
        btApply = (Button) findViewById(R.id.item_selection_apply);
        btFunction = findViewById(R.id.item_selection_function);
        sortingKeys = ItemSortingKey.getDefaultValues();
        rbButton1 = (RadioButton) findViewById(R.id.item_selection_showAll);
        rbButton2 = (RadioButton) findViewById(R.id.item_selection_excludeSent);
        rbButton3 = (RadioButton) findViewById(R.id.item_selection_showLast);
        setOnClick(R.id.item_selection_recent, R.id.item_selection_range, R.id.item_selection_function,
                R.id.item_selection_close, R.id.item_selection_apply, R.id.item_selection_name,
                R.id.item_selection_sorting);

    }

    @Override
    public int getThisView() {
        return R.layout.base_item_selection;
    }

    @Override
    protected void setItemDB() {
        itemDB = ItemDB.getInstance(context);
    }

    @Override
    protected void setUiComponents() {
        setOnClick(R.id.item_selection_showAll, R.id.item_selection_excludeSent,
                R.id.item_selection_showLast);

        switch (keyValueDB.getInt(KEY_Filter_Index)) {
            default:
                ((RadioButton) findViewById(R.id.item_selection_showAll)).setChecked(true);
                onClick(findViewById(R.id.item_selection_showAll));
                break;
            case 1:
                ((RadioButton) findViewById(R.id.item_selection_excludeSent)).setChecked(true);
                onClick(findViewById(R.id.item_selection_excludeSent));
                break;
            case 2:
                ((RadioButton) findViewById(R.id.item_selection_showLast)).setChecked(true);
                onClick(findViewById(R.id.item_selection_showLast));
                break;
        }

    }

    @Override
    protected void setGridView() {
        gridView = (GridView) findViewById(R.id.item_selection_gridview);
    }

    public void refreshHeader() {
        if (adapter == null) return;
        tvSelectionCount.setText(String.format("%d/%d", adapter.clickedPositions.size(), adapter.getRecords().size()));
        btFunction.setText(adapter.getSortedClickedRecords().size() == 0? "전체선택" : "모두취소");
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        adapter.onItemClick(view, position);
        refreshHeader();
    }

    abstract public void onApplyClick(View view);

    public void onSortingClick() {
        new SortingOrderDialog(context, "다음 순서로 보기",selectedSortingKey, sortingKeys) {
            @Override
            public void onItemClicked(ItemSortingKey sortingKey) {
                selectedSortingKey = sortingKey;
                refreshUiAndSetAdapterBySortings(sortingKey);
                refresh();
            }
        };
    };

    public void refreshUiAndSetAdapterBySortings(ItemSortingKey key) {
        // header text
        tvSorting.setText(key.uiStr);
        switch (key) {
            default:
                tvSorting.setTextColor(ColorType.Blue.colorInt);
                break;
            case ItemId:
                tvSorting.setTextColor(ColorType.Black.colorInt);
                break;
        }

        // adapter
        adapter.resetShowing();
        adapter.setSortingKey(key.mSortingKey);
        switch (key) {
            default:
                break;
            case ItemId:
                adapter.showDate().showName().showState();
                break;
            case Name:
                adapter.showChar().showName().showState();
                break;
            case StockCount:
                adapter.showName().showState();
                break;
        }

    }

    @Override
    public void onClick(final View view) {
        super.onClick(view);
        if (view.getId() == R.id.item_selection_close) {
            finish();

        } else if (view.getId() == R.id.item_selection_sorting) {
            onSortingClick();

        } else if (view.getId() == R.id.item_selection_apply) {
            onApplyClick(view);

        } else if (view.getId() == R.id.item_selection_recent) {
            if (adapter.isEmpty()) return;
            adapter.markAsClickedAt(BaseUtils.toDay(adapter.getRecord(0).pickedDateTime));
            refreshHeader();

        } else if (view.getId() == R.id.item_selection_range) {
            if (adapter.clickedPositions.size() < 2) {
                MyUI.toastSHORT(context, String.format("시작과 끝을 선택해주세요"));
                return;
            }
            adapter.markAsClickedBetween(BaseUtils.getMin(adapter.clickedPositions), BaseUtils.getMax(adapter.clickedPositions));
            refreshHeader();

        } else if (view.getId() == R.id.item_selection_function) {
            if (adapter.getSortedClickedRecords().size() == 0) {
                adapter.setAllClicked();
            } else {
                adapter.clickedPositions.clear();
            }
            adapter.notifyDataSetChanged();
            refreshHeader();

        } else if (view.getId() == R.id.item_selection_name) {
            ItemSearchType itemSearchType = ItemSearchType.getFrom(keyValueDB, KEY_SEARCH);
            BaseItemSearchByName.startActivity(context, itemSearchType, KEY_SEARCH, true, true, new MyOnClick<ItemRecord>() {
                @Override
                public void onMyClick(View view, ItemRecord record) {
                    if (record.name.equals(BaseItemSearchByName.KEY_SHOW_ALL)) {
                       MyUI.toastSHORT(context, String.format("전체 선택은 지원하지 않습니다"));
                    } else {
                        boolean found = false;
                        int selectedPosition = 0;
                        for (int position = 0; position < adapter.getCount(); position++) {
                            if (adapter.getRecord(position).itemId == record.itemId) {
                                selectedPosition = position;
                                MyUI.toastSHORT(context, String.format("%s %s가 선택됐습니다",
                                        record.createdDateTime.toString(BaseUtils.MD_Kor_Format), record.getUiName()));
                                found = true;
                                break;
                            }
                        }

                        if (found == false) {
                            adapter.addRecord(record);
                            selectedPosition = adapter.getCount()-1;
                            MyUI.toastSHORT(context, String.format("현재 리스트에 없어 %s를 추가합니다", record.getUiName()));
                        }

                        adapter.setClickedPosition(selectedPosition);
                        adapter.updateCell(selectedPosition);
                        gridView.smoothScrollToPosition(selectedPosition);
                        refreshHeader();
                    }
                }
            }, null);

        } else if (view.getId() == R.id.item_selection_showAll) {
            setFilterKey(0);
            onShowAllClick();

        } else if (view.getId() == R.id.item_selection_excludeSent) {
            setFilterKey(1);
            onExcludeSentClick();

        } else if (view.getId() == R.id.item_selection_showLast) {
            setFilterKey(2);
            onShowLast();
        }
    }

    protected void onShowAllClick() {
        adapter.clearClicked();
        adapter.resetFiltering();
        refresh();
    }

    protected void setFilterKey(int index) {
        keyValueDB.put(KEY_Filter_Index, index);
    }

    public void onExcludeSentClick() {

    }

    public void onShowLast() {

    }

    }
