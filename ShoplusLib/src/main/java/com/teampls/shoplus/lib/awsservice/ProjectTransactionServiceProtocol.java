package com.teampls.shoplus.lib.awsservice;

import android.support.annotation.Nullable;

import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.datatypes.MyTriple;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.datatypes.TransactionLookupData;
import com.teampls.shoplus.lib.datatypes.TransactionNoteData;
import com.teampls.shoplus.lib.enums.AutoMessageRequestTimeConfigurationType;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.protocol.TransactionDraftDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

import org.joda.time.DateTime;

import java.util.List;

/**
 * @author lucidite
 */

public interface ProjectTransactionServiceProtocol {
    /**
     * 새로운 거래기록을 추가한다.
     *
     * @param userShop
     * @param counterpart
     * @param createdDatetime
     * @param transactionItems
     * @param onlinePayment
     * @param entrustedPayment
     * @param demandForUnpayment [SILVER-496] 거래 전 잔금에 대한 청구금액 (이 거래의 총 청구액은 이 값 + 거래에 따른 온라인/대납 청구액이다)
     * @param autoMessageRequestTimeConfig 자동 메시지 전송 시간 설정,
     *                                     자동메시지서비스 가입자만 설정해야 하며, 미가입자는 DO_NOT_SEND 또는 null로 설정해야 한다.
     *                                     (서버에서 별도로 확인하지 않을 수 있으므로 클라이언트에서 권한 체크 필수)
     * @param linkedChatbotOrderId [ORGR-16] 챗봇 주문으로부터 거래기록을 생성하는 경우 해당 주문의 ID를 전달 (해당 주문 자동 완료처리)
     * @return 거래처 계정정보(잔금 및 매입금 정보) 및 상품사진 링크(for 신상알림 고객 only) 주소
     *         [주의] 현재 상품사진 링크는 신상알림 고객 여부를 체크하여 포함하지 않는다. 따라서 앱에서 신상알림 고객 여부를 확인하여 링크를 사용해야 한다.
     *              (즉, 신상알림 고객이 아닌 경우 링크가 있더라도 사용하지 말아야 함)
     *
     * @throws MyServiceFailureException
     */
    MyTriple<AccountsData, SlipDataProtocol, String> postTransaction(
            UserShopInfoProtocol userShop, String counterpart, DateTime createdDatetime, List<SlipItemDataProtocol> transactionItems,
            int onlinePayment, int entrustedPayment, int demandForUnpayment, AutoMessageRequestTimeConfigurationType autoMessageRequestTimeConfig, String linkedChatbotOrderId)
            throws MyServiceFailureException;

    /**
     * 특정 거래기록을 조회한다.
     *
     * @param userShop
     * @param transactionId
     * @param isBuyer
     * @return
     * @throws MyServiceFailureException
     */
    SlipDataProtocol getATransaction(UserShopInfoProtocol userShop, String transactionId, Boolean isBuyer) throws MyServiceFailureException;

    /**
     * 최근 거래기록을 다운로드한다. 조회 기간은 서버에서 결정한다.
     *
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    List<SlipDataProtocol> getRecentTransactions(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 특정 아이템의 판매/반품 데이터를 조회한다 - 2018년 4월까지의 이력만 반환한다.
     *
     * @param userShop
     * @param itemid 검색하고자 하는 아이템의 ID
     * @return
     * @throws MyServiceFailureException
     */
    List<TransactionLookupData> searchTransactionDataOfItem(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException;

    /**
     * 특정 월 & 특정 아이템의 판매/반품 데이터를 조회한다 - 2018년 5월 이후의 이력만 반환한다.
     *
     * @param userShop
     * @param itemid
     * @param month
     * @return
     * @throws MyServiceFailureException
     */
    List<TransactionLookupData> searchMonthlyTransactionDataOfItem(UserShopInfoProtocol userShop, int itemid, DateTime month) throws MyServiceFailureException;

    /**
     * 특정 거래기록을 취소 처리한다.
     *
     * @param slipDataKey
     * @return 취소된 거래기록
     * @throws MyServiceFailureException
     */
    SlipDataProtocol cancelTransaction(SlipDataKey slipDataKey) throws MyServiceFailureException;

    /**
     * 작성 중인 거래기록 초안 데이터를 공유한다(신규 업로드).
     *
     * @param userShop
     * @param recipient 거래처 전화번호(ID)
     * @param transactionItems 거래기록 항목 목록
     * @param memo 메모
     * @return 신규 추가된 거래기록 초안 데이터
     * @throws MyServiceFailureException
     */
    TransactionDraftDataProtocol postDraft(UserShopInfoProtocol userShop, String recipient, List<SlipItemDataProtocol> transactionItems, String memo) throws MyServiceFailureException;

    /**
     * 현재 공유된 거래기록 초안 데이터 목록을 조회한다.
     *
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    List<TransactionDraftDataProtocol> getDrafts(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 공유되어 있는 거래기록 초안 데이터를 새로운 항목으로 업데이트한다.
     * transaction items의 전체 항목을 그대로 overwrite한다 (수정/삭제된 항목 및 변경되지 않은 항목 전체를 업로드해야 한다).
     *
     * @param userShop
     * @param draftId 변경하고자 하는 초안 ID
     * @param versionId 변경하고자 하는 초안의 version ID
     * @param transactionItems 새로운 항목 데이터 일체
     * @param memo 메모, 기존 메모 목록 마지막에 추가됨
     * @return 변경된 거래기록 초안 데이터
     * @throws MyServiceFailureException
     */
    TransactionDraftDataProtocol updateDraft(UserShopInfoProtocol userShop, long draftId, long versionId, List<SlipItemDataProtocol> transactionItems, String memo) throws MyServiceFailureException;

    /**
     * 공유되어 있는 거래기록 초안 데이터를 삭제한다.
     *
     * @param userShop
     * @param draftId 삭제하고자 하는 초안 ID
     * @param versionId 변경하고자 하는 초안의 version ID
     * @return 삭제된 거래기록 초안 데이터
     * @throws MyServiceFailureException
     */
    TransactionDraftDataProtocol deleteDraft(UserShopInfoProtocol userShop, long draftId, long versionId) throws MyServiceFailureException;

    /**
     * [SILVER-371] 특정 상품의 특정 고객 최종가격을 반환한다. 없을 경우 null을 반환한다.
     * [NOTE] 정상적인 경우 0일수는 없으므로 0을 반환할 수도 있을 것이나, 의미상으로 null이 올바르다.
     *
     * @param userShop
     * @param itemid
     * @param buyer
     * @return
     * @throws MyServiceFailureException
     */
    @Nullable
    Integer getBuyerSpecificPrice(UserShopInfoProtocol userShop, int itemid, String buyer) throws MyServiceFailureException;

    /**
     * 특정 거래처의 거래기록 전체를 다운로드한다. (거래기록의 전역 보존기한 내, 보존기한은 서버에서 설정함)
     * 내가 생성한 것과 내가 받은 것 양쪽을 모두 받는다.
     * @author JsForce
     * @deprecated 월별로 교체 (getMontlyCounterpartSpecificTransactions, getOldCounterpartSpecificTransactions)
     *
     * @param userShop
     * @param counterpartPhoneNumber
     * @param isBuyer
     * @return
     * @throws MyServiceFailureException
     */
  //  List<SlipDataProtocol> downloadCounterpartSpecificTransactions(UserShopInfoProtocol userShop, String counterpartPhoneNumber, Boolean isBuyer) throws MyServiceFailureException;

    /**
     * [EXO-121] 특정 거래처의 월별 거래기록을 조회한다. 2019년 3월 이후부터 사용할 수 있다.
     *
     * @param userShop
     * @param counterpartPhoneNumber
     * @param month 조회할 월, 일 이하의 데이터는 사용하지 않는다. 2019년 3월 이후여야 한다.
     * @param isBuyer
     * @return
     * @throws MyServiceFailureException
     */
    List<SlipDataProtocol> getMontlyCounterpartSpecificTransactions(UserShopInfoProtocol userShop, String counterpartPhoneNumber, DateTime month, Boolean isBuyer) throws MyServiceFailureException;

    /**
     * [EXO-121] 특정 거래처의 오래된 거래기록을 조회한다. 2019년 2월까지의 거래기록을 반환한다.
     *
     * @param userShop
     * @param counterpartPhoneNumber
     * @return
     * @throws MyServiceFailureException
     */
    List<SlipDataProtocol> getOldCounterpartSpecificTransactions(UserShopInfoProtocol userShop, String counterpartPhoneNumber, Boolean isBuyer) throws MyServiceFailureException;

    /**
     * 특정 기간의 거래기록을 다운로드한다.
     *
     * @param userShop
     * @param dateTimeFrom 검색을 시작할 시간
     * @param dateTimeTo 검색을 종료할 시간
     * @param isBuyer
     * @return
     * @throws MyServiceFailureException
     */
    List<SlipDataProtocol> getTransactions(UserShopInfoProtocol userShop, DateTime dateTimeFrom, DateTime dateTimeTo, Boolean isBuyer) throws MyServiceFailureException;

    /**
     * [SILVER-499] 특정 거래기록에 첨부된 노트 목록을 조회한다.
     *
     * @param userShop
     * @param transactionId
     * @return
     * @throws MyServiceFailureException
     */
    List<TransactionNoteData> getTransactionNotes(UserShopInfoProtocol userShop, String transactionId) throws MyServiceFailureException;

    /**
     * [SILVER-499] 특정 거래기록에 새 노트를 첨부한다.
     * [NOTE] 한 번에 너무 길지 않게 작성하는 것을 권장한다. (필요할 경우 글자제한 또는 절사하는 것을 고려)
     *
     * @param userShop
     * @param transactionId
     * @param noteText 추가할 노트 내용 (텍스트)
     * @return 업데이트된 노트 목록을 반환한다.
     * @throws MyServiceFailureException
     */
    List<TransactionNoteData> postTransactionNote(UserShopInfoProtocol userShop, String transactionId, String noteText) throws MyServiceFailureException;
}
