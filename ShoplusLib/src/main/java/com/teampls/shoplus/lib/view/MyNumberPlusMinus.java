package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.event.MyOnClick;

/**
 * Created by Medivh on 2017-04-26.
 */

public class MyNumberPlusMinus implements View.OnClickListener {
    private TextView textView;
    private boolean isEditText = false;
    private int plusResId = 0, minusResId = 0;
    private int number = 0;
    private int LOW_LIMIT = 1;
    private View view;
    private MyOnClick<Integer> myOnClick;
    private boolean enabled = true;
    private int incrementalValue = 1;

    public MyNumberPlusMinus(View view, int lowLimit) {
        this(view, 1, lowLimit);
    }

    public MyNumberPlusMinus(View view, int incrementalValue, int lowLimit) {
        this.view = view;
        this.incrementalValue = incrementalValue;
        this.LOW_LIMIT = lowLimit;
        if (view == null)
            Log.e("DEBUG_JS", String.format("[MyNumberPlusMinus.MyNumberPlusMinus] view == null"));
    }

    public MyNumberPlusMinus setTextView(int resId) {
        textView = (TextView) view.findViewById(resId);
        return this;
    }

    public void enableTextViewOnClick(final Context context) {
        if (textView != null) {
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new UpdateValueDialog(context, "직접 입력", "", number) {
                        @Override
                        public void onApplyClick(String newValue) {
                            number = BaseUtils.toInt(newValue);
                            textView.setText(Integer.toString(number));
                            if (myOnClick != null)
                                myOnClick.onMyClick(view, number);
                        }
                    };
                }
            });
        }
    }

    public void linkViewToEdit(final Context context, int viewId) {
        View myView = view.findViewById(viewId);
        if (myView != null) {
            myView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new UpdateValueDialog(context, "직접 입력", "", number) {
                        @Override
                        public void onApplyClick(String newValue) {
                            number = BaseUtils.toInt(newValue);
                            textView.setText(Integer.toString(number));
                            if (myOnClick != null)
                                myOnClick.onMyClick(view, number);
                        }
                    };
                }
            });
        }
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextValue(int value) {
        number = value;
        textView.setText(Integer.toString(number));
    }

    public void setIncrementalValue(int value) {
        this.incrementalValue = value;
    }

    public void setEnabled(boolean value) {
        this.enabled = value;
        BaseUtils.setTextCancelled(!enabled, textView);
        if (plusButton != null && plusButton instanceof ImageView) {
            MyView.setEnabled((ImageView) plusButton, enabled);
            MyView.setEnabled((ImageView) minusButton, enabled);
        }
    }

    public MyNumberPlusMinus setEditView(int resId) {
        this.isEditText = true;
        textView = (EditText) view.findViewById(resId);
        textView.clearFocus();
        return this;
    }

    private View plusButton, minusButton;

    public MyNumberPlusMinus setPlusMinusButtons(int plusResId, int minusResId) {
        this.plusResId = plusResId;
        this.minusResId = minusResId;
        if (view != null) {
            plusButton = view.findViewById(plusResId);
            plusButton.setOnClickListener(this);
            minusButton = view.findViewById(minusResId);
            minusButton.setOnClickListener(this);
        }
        return this;
    }

    public void setOnClick(MyOnClick<Integer> myOnClick) {
        this.myOnClick = myOnClick;
    }

    @Override
    public void onClick(View view) {
        if (enabled == false) {
            if (myOnClick != null)
                myOnClick.onMyClick(view, number);
            return;
        }

        if (view.getId() == plusResId) {
            number = BaseUtils.toInt(textView.getText().toString());
            if (number < LOW_LIMIT)
                number = LOW_LIMIT;
            number = number + incrementalValue;
            textView.setText(Integer.toString(number));
            if (isEditText)
                ((EditText) textView).setSelection(textView.getText().toString().length());
            if (myOnClick != null)
                myOnClick.onMyClick(view, number);

        } else if (view.getId() == minusResId) {
            number = BaseUtils.toInt(textView.getText().toString());
            number = number - incrementalValue;
            if (number < LOW_LIMIT)
                number = LOW_LIMIT;
            textView.setText(Integer.toString(number));
            if (isEditText)
                ((EditText) textView).setSelection(textView.getText().toString().length());
            if (myOnClick != null)
                myOnClick.onMyClick(view, number);
        }
    }

    public int getValue() {
        number = BaseUtils.toInt(textView.getText().toString());
        if (number < LOW_LIMIT)
            number = LOW_LIMIT;
        return number;
    }
}
