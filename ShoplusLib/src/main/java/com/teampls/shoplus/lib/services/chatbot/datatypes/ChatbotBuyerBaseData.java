package com.teampls.shoplus.lib.services.chatbot.datatypes;

import android.util.Log;

import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotBuyerBaseDataProtocol;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author lucidite
 */
public class ChatbotBuyerBaseData implements ChatbotBuyerBaseDataProtocol {
    private String userKey;
    private String buyerName;
    private String buyerPhoneNumber;
    private boolean isAuthed;

    public ChatbotBuyerBaseData() {
        userKey = "";
        buyerName = "";
        buyerPhoneNumber = "";
    }

    public ChatbotBuyerBaseData(JSONObject json) throws JSONException {
        this.userKey = json.getString("user_key");
        this.buyerPhoneNumber = json.getString("user_id");
        this.buyerName = json.optString("buyer_name", this.buyerPhoneNumber);
        this.isAuthed = json.optBoolean("is_auth", false);
    }

    public ChatbotBuyerBaseData(ChatbotBuyerBaseDataProtocol protocol) {
        this.userKey = protocol.getUserKey();
        this.buyerName = protocol.getBuyerName();
        this.buyerPhoneNumber = protocol.getBuyerPhoneNumber();
        this.isAuthed = protocol.isAuthed();
    }

    @Override
    public String getUserKey() {
        return this.userKey;
    }

    @Override
    public String getBuyerName() {
        return this.buyerName;
    }

    @Override
    public String getBuyerPhoneNumber() {
        return this.buyerPhoneNumber;
    }

    @Override
    public boolean isAuthed() {
        return this.isAuthed;
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %s, %s, %s", callLocation, buyerName, buyerPhoneNumber, isAuthed));
    }
}
