package com.teampls.shoplus.lib.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.adapter.MainViewAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.database_global.AddressbookDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.OldAddressbookDB;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.dialog.UserConfigTypeDialog;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;

import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2017-01-21.
 */

abstract public class BaseAwsTaskScheduler {
    public TaskState counterpartDownloading = TaskState.beforeTasking;
    public TaskState userSettingDownloading = TaskState.beforeTasking;
    protected MainViewAdapter adapter;
    protected Context context;
    protected UserDB userDB;
    protected AddressbookDB addressbookDB;
    public static String taskId = "AwsTaskScheduler";
    public String errorMessage = "";
    protected ProgressDialog progressDialog;
    protected GlobalDB globalDB;
    protected UserSettingData userSettingData;
    public boolean finished = false;

    public BaseAwsTaskScheduler(Context context, MainViewAdapter adapter) {
        this.context = context;
        this.adapter = adapter;
        userDB = UserDB.getInstance(context);
        globalDB = GlobalDB.getInstance(context);
        addressbookDB = AddressbookDB.getInstance(context);
        progressDialog = new ProgressDialog(context);
        userSettingData = UserSettingData.getInstance(context);
        finished = false;
        init();
    }

    abstract public void init();

    abstract public void start();

    protected void onTaskFinish(String callTask) {
        //  Log.i("DEBUG_JS", String.format("[BaseAwsTaskScheduler.onTaskFinish] %s", callTask));
        onTaskFinish();
    }

    protected void onTaskFinish() {
        if (getState().isFinished()) {
            refreshFragments();
            finished = true;
            Log.i("DEBUG_JS", String.format("[AwsTaskScheduler] downloading is finished..."));
        }
    }

    protected void refreshFragments() {
        new MyUITask(context) {
            @Override
            public void doInBackground() {
                adapter.refreshFragments();
            }
        };
    }

    public void downloadCounterparts(final MyOnTask onTask) {
        if (counterpartDownloading.isOnTasking()) return;
        counterpartDownloading = TaskState.onTasking;

        new MyServiceTask(context, "downloadCounterparts") {
            boolean counterpartUpdated = false;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                Pair<Long, List<ContactDataProtocol>> contactData = workingShopService.getContactUpdates(userSettingData.getUserShopInfo(),
                        userDB.getLatestTimeStamp()); // 전화-이름 정보
                counterpartUpdated = userDB.update(contactData);
            }

            @Override
            public void onPostExecutionUI() {
                counterpartDownloading = TaskState.afterTasking;
                if ((onTask != null) && counterpartUpdated)
                    onTask.onTaskDone(counterpartUpdated);
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                errorMessage = "고객 정보 다운로드 중 에러가 발생했습니다";
                counterpartDownloading = TaskState.onError;
            }
        };
    }

    protected void restartApp(String message) {
        MyUI.toast(context, message);
        new MyDelayTask(context, 1000) {
            @Override
            public void onFinish() {
                MyDevice.openApp(context, context.getPackageName());
            }
        };
        ((Activity) context).finishAffinity();
    }

    abstract protected void clearLocalDBs();

    protected void updateUserSetting(final MyOnTask onTask) {
        //  userSettingDownloading = TaskState.afterTasking;
        userSettingData.init(context, "updateUserSetting", new MyOnAsync() { // download와 바로 연결됨

            @Override
            public void onSuccess(Object result) {
                userSettingDownloading = TaskState.afterTasking;
                if (userSettingData.isWorkingShopChanged) {
                    clearLocalDBs();
                }

                if (BaseAppWatcher.isNewlyRegistered(context) || (userSettingData.getUserConfigType() == UserConfigurationType.NOT_DEFINED)) {
                    BaseAppWatcher.setNewlyRegistered(context, false);
                    new UserConfigTypeDialog(context) {
                        @Override
                        public void onApplyClick(UserConfigurationType userConfigurationType) {
                            restartApp(String.format("%s 정보 적용을 위해 앱을 재시작 합니다", userConfigurationType.uiStr));
                        }
                    };

                } else if (userSettingData.isUserTypeChanged) {
                    // 재설치 후 기존 도/소매 정보를 가져온 경우, 가입 후 기존 도/소매를 변경한 경우
                    restartApp("도/소매 정보 적용을 위해 앱을 재시작 합니다");

                } else if (userSettingData.isUserLevelChanged) {
                    restartApp("사용자 정보가 변경되어 앱을 재시작 합니다");

                } else if (userSettingData.isHasMasterChanged) {
                    restartApp("사장님 권한이 변경되어서 앱을 재시작 합니다");

                } else {
                    if (onTask != null)
                        onTask.onTaskDone("");
                }
            }

            @Override
            public void onError(Exception e) {
                userSettingDownloading = TaskState.onError;
                errorMessage = "사용자 설정에 오류가 확인되었습니다";
            }
        });
    }

    abstract protected List<TaskState> getTaskStates();

    public TaskState getState() {
        TaskState result = TaskState.beforeTasking;
        int beforeCount = 0, finishedCount = 0, errorCount = 0;
        int index = 0;
        for (TaskState taskState : getTaskStates()) {
            if (taskState == TaskState.beforeTasking)
                beforeCount++;
            if (taskState.isFinished())
                finishedCount++;
            if (taskState.isOnError())
                errorCount++;
            //   Log.i("DEBUG_JS", String.format("[BaseAwsTaskScheduler.getState] %d, %s", index, taskState.toString()));
            index++;
        }
        if (beforeCount >= getTaskStates().size()) {
            result = TaskState.beforeTasking;
        } else if (finishedCount >= getTaskStates().size()) {
            result = TaskState.afterTasking;
        } else if (errorCount >= 1) {
            result = TaskState.onError;
        } else {
            result = TaskState.onTasking;
        }
        //    Log.i("DEBUG_JS", String.format("[AwsTaskScheduler.getState] %s", result.toString()));
        return result;
    }

    protected void toLogCatTaskStates() {
        int index = 0;
        for (TaskState taskState : getTaskStates()) {
            Log.i("DEBUG_JS", String.format("[BaseAwsTaskScheduler.TaskStates] %d, %s", index, taskState.toString()));
            index++;
        }
    }

    abstract class MyServiceTask extends BaseServiceTask {

        public MyServiceTask(Context context, String location) {
            super(context, location);
        }

        @Override
        public void setService() {
        }
    }

}
