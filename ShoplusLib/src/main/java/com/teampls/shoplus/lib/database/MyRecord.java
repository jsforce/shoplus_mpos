package com.teampls.shoplus.lib.database;

import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database_global.UserRecord;

public class MyRecord {
    public int id = 0;
    public String username = "", phoneNumber = "", nickname = "";
	public String password = "";
    public boolean confirmed = false;
	public MyRecord() {}
	
	public MyRecord(int id, String username, String phoneNumber, String nickname, String password, boolean confirmed) {
		this.id = id;
		this.username = username;
		this.phoneNumber = phoneNumber;
		this.nickname = nickname;
		this.password = password;
        this.confirmed = confirmed;
	}
	
	public MyRecord(String username, String phoneNumber, String nickname, String password) {
		this.username = username;
		this.phoneNumber = phoneNumber;
		this.nickname = nickname;
		this.password = password;
	}

    public MyRecord(String phoneNumber, String password, boolean confirmed) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.confirmed = confirmed;
    }

    public UserRecord toUserRecord() {
        return new UserRecord(nickname, phoneNumber);
    }

    public void toLogCat(String callLocation) {
        Log.w("DEBUG_JS", String.format("[%s] %s, %s, %s, %s*****, ? %s", callLocation, username, phoneNumber, nickname, BaseUtils.toSubStr(password, 4), confirmed));
    }
}
