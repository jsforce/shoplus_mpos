package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2017-03-25.
 */

abstract public class DeleteSafelyDialog extends BaseDialog {
    private String confirmKey = "";
    private EditText etKey;

    public DeleteSafelyDialog(Context context) {
        this(context, "상품 삭제");
    }

    public DeleteSafelyDialog(Context context, String title) {
        this(context, title, "", "", "", "", "");
    }

    public DeleteSafelyDialog(Context context, String title, String message, String confirmKey, String applyText, String funcText, String cancelText) {
        super(context);
        setDialogWidth(0.95, 0.8);
        this.confirmKey = confirmKey;
        if (this.confirmKey.isEmpty())
            this.confirmKey = BaseUtils.getRandomNumber(6);
        if (message.isEmpty())
            message = String.format("안전한 삭제를 위해 [%s] 를 입력해주세요", this.confirmKey);
        if (applyText.isEmpty())
            applyText = "삭  제";
        if (TextUtils.isEmpty(funcText))
            setVisibility(View.GONE, R.id.dialog_number_input_function);
        if (cancelText.isEmpty())
            cancelText = "취  소";

        etKey = (EditText) findViewById(R.id.dialog_number_input_text);
        findTextViewById(R.id.dialog_number_input_title, title);
        findTextViewById(R.id.dialog_number_input_apply, applyText, false);
        findTextViewById(R.id.dialog_number_input_message, message);
        findTextViewById(R.id.dialog_number_input_cancel, cancelText);
        findTextViewById(R.id.dialog_number_input_function, funcText);

        setOnClick(R.id.dialog_number_input_apply, R.id.dialog_number_input_cancel,
                R.id.dialog_number_input_clear, R.id.dialog_number_input_function);
        show();
        MyDevice.showKeyboard(context, etKey);

        MyView.setTextViewByDeviceSizeScale(context, getView(), R.id.dialog_number_input_text);
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_number_input;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_number_input_apply) {
            if (etKey.getText().toString().trim().equals(confirmKey)) {
                onDeleteClick();
                MyDevice.hideKeyboard(context, etKey);
                dismiss();
            } else {
                MyUI.toastSHORT(context, String.format("숫자가 일치하지 않습니다. 확인해주세요"));
            }

        } else if (view.getId() == R.id.dialog_number_input_function) {
            if (etKey.getText().toString().trim().equals(confirmKey)) {
                boolean doDismiss = onFuncClick();
                if (doDismiss) {
                    MyDevice.hideKeyboard(context, etKey);
                    dismiss();
                }
            } else {
                MyUI.toastSHORT(context, String.format("숫자가 일치하지 않습니다. 확인해주세요"));
            }
        } else if (view.getId() == R.id.dialog_number_input_cancel) {
            MyDevice.hideKeyboard(context, etKey);
            dismiss();
        } else if (view.getId() == R.id.dialog_number_input_clear) {
            etKey.setText("");
        }
    }

    abstract public void onDeleteClick();

    public boolean onFuncClick() {
        return true;
    }
}
