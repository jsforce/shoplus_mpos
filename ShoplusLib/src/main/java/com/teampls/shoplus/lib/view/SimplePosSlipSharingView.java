package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.view_base.BasePosSlipSharingView;

/**
 * Created by Medivh on 2018-11-10.
 */

public class SimplePosSlipSharingView extends BasePosSlipSharingView {

    public static void startActivity(Context context, SlipFullRecord slipFullRecord) {
        SimplePosSlipSharingView.slipFullRecord = slipFullRecord;
        BasePosSlipSharingView.itemOptionRecord = new ItemOptionRecord();
        actionType = DbActionType.READ;
        ((Activity) context).startActivity(new Intent(context, SimplePosSlipSharingView.class));
    }

    public static void startActivity(Context context, SlipFullRecord slipFullRecord, ItemOptionRecord itemOptionRecord) {
        SimplePosSlipSharingView.slipFullRecord = slipFullRecord;
        BasePosSlipSharingView.itemOptionRecord = itemOptionRecord;
        actionType = DbActionType.READ;
        ((Activity) context).startActivity(new Intent(context, SimplePosSlipSharingView.class));
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.clear();
        if (userSettingData.isProvider()) {
            myActionBar
                    .add(MyMenu.basicInfo)
                    .add(MyMenu.sendKakao)
                    .add(MyMenu.shareFile)
                    .add(MyMenu.printSetting)
                    .add(MyMenu.close);

        } else if (userSettingData.isBuyer()) {
            myActionBar.add(MyMenu.sendKakao)
                    .add(MyMenu.shareFile)
                    .add(MyMenu.close);
        }
    }

    @Override
    protected void cancelSlip(boolean doCopy) {
        //
    }
}
