package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.OrderItemRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.datatypes.MyTriple;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.enums.WorkingTime;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

/**
 * Created by Medivh on 2016-09-04.
 */
public class BaseUtils {
    static final private String dateFormat = "yyyy-MM-dd HH:mm:ss";
    static final public SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
    static final public DateTimeFormatter serverFormat = DateTimeFormat.forPattern(" yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    static final public DateTimeFormatter fullFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    static final public DateTimeFormatter YMDHmFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
    static final public DateTimeFormatter YMD_Format = DateTimeFormat.forPattern("yyyy-MM-dd");
    static final public DateTimeFormatter Hm_Format = DateTimeFormat.forPattern("HH:mm");
    static final public DateTimeFormatter KEY_Format = DateTimeFormat.forPattern("yyMMdd-HHmm-ssSSS");
    static final public DateTimeFormatter MDHm_Kor_Format = DateTimeFormat.forPattern("M월d일 HH:mm");
    static final public DateTimeFormatter MDHm_Format = DateTimeFormat.forPattern("MM/dd HH:mm");
    static final public DateTimeFormatter MDHm_Twolines_Format = DateTimeFormat.forPattern("MM/dd\nHH:mm");
    static final public DateTimeFormatter File_Format = DateTimeFormat.forPattern("yyyyMMdd_HHmmss_SSS");
    static final public DateTimeFormatter YMD_Kor_Format = DateTimeFormat.forPattern("y년 M월 d일");
    static final public DateTimeFormatter YM_Kor_Format = DateTimeFormat.forPattern("yyyy년 MM월");
    static final public DateTimeFormatter YMD_HS_Kor_Format = DateTimeFormat.forPattern("y년 M월 d일 HH:mm");
    //static final public DateTimeFormatter MDH_Kor_Format = DateTimeFormat.forPattern("M월 d일 H시");
    static final public DateTimeFormatter MD_Kor_Format = DateTimeFormat.forPattern("M월d일");
    static final public DateTimeFormatter MD_Fixed_Kor_Format = DateTimeFormat.forPattern("MM월dd일");
    static final public DateTimeFormatter MD_FixedSize_Format = DateTimeFormat.forPattern("MM/dd");
    static final public DateTimeFormatter MD_Format = DateTimeFormat.forPattern("M/d");
    static final public DateTimeFormatter HS_Kor_Format = DateTimeFormat.forPattern("H시m분");
    static final public DateTimeFormatter HS_Fixed_Format = DateTimeFormat.forPattern("HH:mm");
    static final public DateTimeFormatter Hms_Format = DateTimeFormat.forPattern("HH:mm:ss");
    static final public DateTimeFormatter TempUser_Format = DateTimeFormat.forPattern("yyMMddHHmmss");
    static final public DateTimeFormatter Delivery_Format = DateTimeFormat.forPattern("yyMMddHHmmssSSS");
    static final public DateTimeFormatter ItemSerial_Format = DateTimeFormat.forPattern("yy");
    static final private DecimalFormat currencyFormat = new DecimalFormat("#,###");

    static final private String alphabetLower = "abcdefghijklmnopqrstuvwxyz";
    static final private String numbers = "1234567890";
    static final private String specialCharacters = "~!@#$%^&*()";
    private static Random random = new Random();

    public static String[] toStrs(Enum[] values) {
        String[] columns = new String[values.length];
        for (int i = 0; i < values.length; i++) {//
            columns[i] = values[i].toString();
        }
        return columns;
    }

    public static int IntegerValueOf(String str, int defaultValue) {
        try {
            return Integer.valueOf(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static <T> List<T> clone(List<T> records) {
        List<T> results = new ArrayList<>();
        for (T record : records)
            results.add(record);
        return results;
    }

    public static void setBackgroundColorWithBox(ImageView view, ColorType color) {
        view.setImageResource(R.drawable.background_box);
        view.setBackgroundColor(color.colorInt);
    }

    public static MyTriple<Double, Double, Boolean> getTimeSetting(Context context) {
        return MyTriple.create(GlobalDB.DayStartTime.get(context), GlobalDB.NightStartTime.get(context),
                GlobalDB.IsDayAndNightWorking.get(context));
    }

    public static MyMap<Integer, Integer> mergeCounts(MyMap<Integer, Integer> map1, MyMap<Integer, Integer> map2) {
        Set<Integer> itemIds = new HashSet<>();
        itemIds.addAll(map1.keySet());
        itemIds.addAll(map2.keySet());
        MyMap<Integer, Integer> results = new MyMap<>(0);
        for (Integer itemId : itemIds)
            results.addInteger(itemId, map1.get(itemId) + map2.get(itemId));
        return results;
    }

    public static String toSignString(int value, String zeroStr) {
        if (value == 0)
            return zeroStr;
        else if (value > 0) {
            return String.format("+%d", value);
        } else {
            return String.format("%d", value);
        }
    }

    public static String toSignString(int value) {
        return toSignString(value, "");
    }

    public static String reverse(String string) {
        return new StringBuffer(string).reverse().toString();
    }

    public static boolean isDateTime(String string) {
        return !Empty.isEmpty(toDateTime(string, true));
    }

    public static Drawable getDrawable(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 21) {
            return ContextCompat.getDrawable(context, id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }

    public static List<String> split(String string, String delimiter) {
        List<String> result = new ArrayList<>();
        if (TextUtils.isEmpty(string))
            return result;
        return toList(string.split(delimiter));
    }

    public static String toItemSerialStr(String serialStr) {
        if (serialStr.length() == 4)
            return String.format("%s-%s", DateTime.now().toString(BaseUtils.ItemSerial_Format), serialStr);
        else if (serialStr.length() == 6)
            return String.format("%s-%s", serialStr.substring(0, 2), serialStr.substring(2));
        else if (serialStr.length() == 7 && serialStr.contains("-")) {
            return serialStr;
        } else {
            return "";
        }
    }


    public static <T> T getNext(Context context, List<T> records, T record, T defaultT) {
        if (records.size() == 0)
            return defaultT;
        int index = records.indexOf(record);
        if (index == -1)
            return defaultT;
        if (index + 1 >= records.size()) {
            MyUI.toastSHORT(context, String.format("마지막 입니다"));
            return records.get(index);
        } else
            return records.get(index + 1);
    }

    public static <T> T getPrev(Context context, List<T> records, T record, T defaultT) {
        if (records.size() == 0)
            return defaultT;
        int index = records.indexOf(record);
        if (index == -1)
            return defaultT;
        if (index - 1 < 0) {
            MyUI.toastSHORT(context, String.format("처음입니다"));
            return records.get(0);
        } else
            return records.get(index - 1);
    }

    public static int getPercent(int numerator, int denominator) {
        return (int) (((long) numerator * 100) / denominator);
    }

    public static int getMarginPercent(int unitPrice, int cost) {
        if (unitPrice == 0)
            return 0;
        else
            return (int) (((long) 100 * (unitPrice - cost)) / unitPrice);
    }

    public static int round(double value, int basicUnit) {
        return (int) Math.round(value / basicUnit) * basicUnit;
    }

    public static int ceil(double value, int basicUnit) {
        return (int) (Math.ceil(value / basicUnit) * basicUnit);
    }

    public static int getMin(Set<Integer> arrayList) {
        return getMin(new ArrayList<Integer>(arrayList));
    }

    public static DateTime min(DateTime dateTime1, DateTime dateTime2) {
        if (dateTime1.isBefore(dateTime2)) {
            return dateTime1;
        } else {
            return dateTime2;
        }
    }

    public static int getMin(List<Integer> arrayList) {
        if (arrayList == null)
            return 0;
        if (arrayList.size() == 0)
            return 0;
        return Collections.min(arrayList);
    }

    public static int getMax(Set<Integer> arrayList) {
        return getMax(new ArrayList<>(arrayList));
    }

    public static int getMax(List<Integer> arrayList) {
        if (arrayList == null)
            return 0;
        if (arrayList.size() == 0)
            return 0;
        return Collections.max(arrayList);
    }

    public static long getMaxLong(List<Long> arrayList) {
        if (arrayList == null)
            return 0;
        if (arrayList.size() == 0)
            return 0;
        return Collections.max(arrayList);
    }

    public static DateTime createCustomDateTime(DateTime targetDateTime, int hour) {
        DateTime nowDateTime = DateTime.now();
        return new DateTime(
                targetDateTime.getYear(), targetDateTime.getMonthOfYear(), targetDateTime.getDayOfMonth(),
                hour,
                nowDateTime.getMinuteOfHour(), nowDateTime.getSecondOfMinute(), nowDateTime.getMillisOfSecond());
    }

    public static <T> List<String> toStringList(List<T> values) {
        if (values.size() == 0)
            return new ArrayList<>();

        List<String> results = new ArrayList<>();
        Object value0 = values.get(0);
        if (value0 instanceof Integer) {
            for (T value : values)
                results.add(Integer.toString((Integer) value));
        } else if (value0 instanceof Boolean) {
            for (T value : values)
                results.add(BaseUtils.toDbStr((Boolean) value));
        } else if (value0 instanceof String) {
            for (T value : values)
                results.add(String.valueOf(value));
        } else if (value0 instanceof Double || value0 instanceof Float) {
            for (T value : values)
                results.add(Double.toString((Double) value));
        } else {
            for (T value : values)
                results.add(value.toString());
            Log.e("DEBUG_JS", String.format("[BaseUtils.toStringList] not defined data type : %s", value0.getClass().getSimpleName()));
        }
        return results;
    }

    public static <T> String[] toStringArray(List<T> values) {
        List<String> stringList = toStringList(values);
        return stringList.toArray(new String[stringList.size()]);
    }

    public static List<String> toList(String... strings) {
        List<String> results = new ArrayList<>();
        for (String string : strings)
            results.add(string);
        return results;
    }

    public static Map<ItemStockKey, Integer> toStockMap(MyMap<ItemOptionRecord, Integer> optionMap, int countLimit) {
        Map<ItemStockKey, Integer> result = new HashMap<>();
        for (ItemOptionRecord record : optionMap.keySet()) {
            if (optionMap.get(record) >= countLimit)  // 개수가 있는 것만
                result.put(record.toStockKey(), optionMap.get(record));
        }
        return result;
    }

    public static Map<ItemStockKey, Integer> toStockMap(List<OrderItemRecord> orderItemRecords, int countLimit) {
        Map<ItemStockKey, Integer> result = new HashMap<>();
        for (OrderItemRecord orderItemRecord : orderItemRecords) {
            if (orderItemRecord.finalOrder >= countLimit)
                result.put(orderItemRecord.toStockKey(), orderItemRecord.finalOrder);
        }
        return result;
    }


    public static MyMap<ItemOptionRecord, Integer> toOptionMap(Map<ItemStockKey, Integer> stockMap) {
        MyMap<ItemOptionRecord, Integer> result = new MyMap<>(0);
        for (ItemStockKey record : stockMap.keySet()) {
            result.put(record.toOptionRecord(), stockMap.get(record));
        }
        return result;
    }

    public static List<Integer> toIntegerList(List<String> strings) {
        List<Integer> results = new ArrayList<>();
        for (String string : strings) {
            try {
                results.add(Integer.valueOf(string));
            } catch (NumberFormatException e) {
                //
            }
        }
        return results;
    }

    public static <T> List<T> toList(T record) {
        List<T> results = new ArrayList<>();
        results.add(record);
        return results;
    }

    public static <T> List<T> toList(T... records) {
        List<T> results = new ArrayList<>();
        for (T record : records)
            results.add(record);
        return results;
    }

    public static Set<String> toSet(boolean removeEmpty, String... strings) {
        Set<String> results = new HashSet<>();
        for (String string : strings) {
            if (removeEmpty && string.isEmpty())
                continue;
            results.add(string);
        }
        return results;
    }

    public static double toDoubleHour(int hour, int minutes) {
        return ((double) hour + ((double) minutes) / 60.0);
    }

    public static Pair<Integer, Integer> toHourAndMin(double floatHour) {
        int hour = (int) Math.floor(floatHour);
        return Pair.create(hour, (int) ((floatHour - hour) * 60)); // 8.5시 경우 처리 --> 8시 + 30분
    }

    public static Pair<DateTime, DateTime> splitPeriod(DateTime fromDate, DateTime toDate, double splitTime, int partNum) {
        Pair<Integer, Integer> toHourAndMin = toHourAndMin(splitTime);
        DateTime splitDate = BaseUtils.toDay(fromDate).plusHours(toHourAndMin.first).plusMinutes(toHourAndMin.second);

        if (partNum == 0) {
            if (fromDate.isBefore(splitDate) == false) {
                Log.w("DEBUG_JS", String.format("[BaseUtils.splitPeriod] input error : from %s > splite %s", fromDate, splitDate));
                return Pair.create(fromDate, toDate);
            }
            return Pair.create(fromDate, splitDate.minusMillis(1));
        } else if (partNum == 1) {
            if (splitDate.isBefore(toDate) == false)
                return Pair.create(toDate, splitDate);
            return Pair.create(splitDate, toDate);
        } else {
            Log.w("DEBUG_JS", String.format("[BaseUtils.splitPeriod] invalid part num : %d", partNum));
            return Pair.create(fromDate, toDate);
        }
    }

    public static Pair<DateTime, DateTime> getDayPeriod(double fromHour, int dayShift) {
        DateTime fromDate = BaseUtils.createDay().minusDays(dayShift); // 기본은 0시
        if (fromHour > 0) {
            Pair<Integer, Integer> fromHourAndMin = toHourAndMin(fromHour);
            fromDate = fromDate.plusHours(fromHourAndMin.first).plusMinutes(fromHourAndMin.second);  //  설정 시간으로 (ex 8시 30분)
            if (DateTime.now().getHourOfDay() < fromHour)
                fromDate = fromDate.minusDays(1); // 아직 기준시간 이전이면 (ex 새벽 5시) 어제 8시 30분 부터
        }

        DateTime toDate = fromDate.plusDays(1).minusMillis(1); // 시작+24시간
        if (toDate.isAfter(DateTime.now()))
            toDate = DateTime.now(); // 현재시간보다 이후면 현재시간으로

        if (fromDate.isAfter(toDate))
            Log.e("DEBUG_JS", String.format("[BaseUtils.getDayPeriod] period error : from %s, to %s", fromDate.toString(), toDate.toString()));

        return Pair.create(fromDate, toDate);
    }

    public static Pair<DateTime, DateTime> toPeriod(DateTime day, MyTriple<Double, Double, Boolean> timeSetting, WorkingTime workingTime) {
        DateTime fromDate = BaseUtils.toDay(day);
        Pair<Integer, Integer> fromHourAndMin = toHourAndMin(timeSetting.first);
        fromDate = fromDate.plusHours(fromHourAndMin.first).plusMinutes(fromHourAndMin.second);  //  설정 시간으로 (ex 8시 30분)
        DateTime toDate =  fromDate.plusDays(1).minusMillis(1);
        switch (workingTime) {
            default:
            case AllDay:
               return Pair.create(fromDate, toDate);
            case Day:
                return BaseUtils.splitPeriod(fromDate, toDate, timeSetting.second, 0);
            case Night:
                return BaseUtils.splitPeriod(fromDate, toDate, timeSetting.second, 1);
        }
    }

    public static void setTextWithVisbility(TextView textView, String header, String str) {
        if (TextUtils.isEmpty(str.trim())) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setTextColor(ColorType.Black.colorInt);
            textView.setText(header + str);
        }
    }

    public static void setText(TextView textView, String header, String str) {
        setText(textView, TextUtils.isEmpty(str), header, str, false);
    }

    public static void setText(TextView textView, String header, String str, boolean cancelled) {
        setText(textView, TextUtils.isEmpty(str), header, str, cancelled);
    }

    public static void setText(TextView textView, boolean isEmpty, String header, String str, boolean cancelled) {
        if (isEmpty) {
            textView.setTextColor(ColorType.Gray.colorInt);
            textView.setText(header + "(미입력)");
        } else {
            textView.setTextColor(ColorType.Black.colorInt);
            textView.setText(header + str);
        }
        textView.setPaintFlags(cancelled ? textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG
                : textView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public static void setTextCancelled(TextView textView, boolean cancelled) {
        textView.setPaintFlags(cancelled ? textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG
                : textView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public static void setTextCancelled(boolean cancelled, TextView... textViews) {
        for (TextView textView : textViews)
            setTextCancelled(textView, cancelled);
    }

    public static void setTextCancelled(boolean cancelled, ColorType textColor, TextView... textViews) {
        for (TextView textView : textViews) {
            setTextCancelled(textView, cancelled);
            textView.setTextColor(cancelled ? textColor.colorInt : textColor.Black.colorInt);
        }
    }

    public static void setText(TextView textView, String str) {
        setText(textView, "", str, false);
    }

    public static String toLimitString(String str, int limit) {
        if (TextUtils.isEmpty(str))
            return "";
        if (str.length() <= limit)
            return str;
        return str.substring(0, limit) + "..";
    }

    public static <K, V> Map<K, V> sortByKey(Map<K, V> map) {
        return new TreeMap<>(map);
    }

    public static <K, V> Map<K, V> sortReverseByKey(Map<K, V> map) {
        Map<K, V> result = new TreeMap<>(Collections.<K>reverseOrder());
        result.putAll(map);
        return result;
    }


    // 같은 상품내에서 option만 sorting
    public static List<SlipItemRecord> sortOnlyOption(List<SlipItemRecord> records) {
        if (records.size() == 0)
            return new ArrayList<>();

        List<SlipItemRecord> copiedRecords = BaseUtils.clone(records);
        MSortingKey originalSortingKey = records.get(0)._sortingKey;
        MyListMap<Integer, SlipItemRecord> slipItemMap = new MyListMap<>();
        List<Integer> itemIds = new ArrayList<>();
        for (SlipItemRecord record : copiedRecords) {
            if (itemIds.contains(record.itemId) == false)
                itemIds.add(record.itemId);
            record._sortingKey = MSortingKey.MultiField;
            slipItemMap.add(record.itemId, record);
        }

        List<SlipItemRecord> results = new ArrayList<>();
        for (int itemId : itemIds) { //  상품순서는 유지
            List<SlipItemRecord> recordsByItem = slipItemMap.get(itemId);
            Collections.sort(recordsByItem);
            results.addAll(recordsByItem);
        }

        for (SlipItemRecord record : results)
            record._sortingKey = originalSortingKey;
        return results;
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    public static String encode(String str, String phoneNumber) {
        //    Log.i("DEBUG_JS", String.format("[BaseUtils.encode] %s, %s", str, phoneNumber));
        String result = toSHA256(str);
        if (result.length() >= 10)
            result = result.substring(0, 10);
        if (phoneNumber.isEmpty())
            phoneNumber = "0";
        result = "s" + result + phoneNumber.substring(phoneNumber.length() - 1);
        //    Log.i("DEBUG_JS", String.format("[BaseUtils.encode] result = %s", result));
        return result;
    }

    public static String toSHA256(String str) {
        String SHA = "";
        try {
            MessageDigest sh = MessageDigest.getInstance("SHA-256");
            sh.update(str.getBytes());
            byte byteData[] = sh.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            SHA = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            SHA = null;
        }
        return SHA;
    }

    public static void toastOneTimeHelp(Context context, String key, String message) {
        if (KeyValueDB.getInstance(context).getBool(key, true)) {
            KeyValueDB.getInstance(context).put(key, false);
            MyUI.toast(context, message);
        }
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        phoneNumber = toSimpleForm(phoneNumber);
        if (isInteger(phoneNumber) == false) return false;  // 모두 숫자?
        switch (phoneNumber.length()) {     // 자리수는 10 또는 11
            case 10:
            case 11:
                break;
            default:
                return false;
        }

        List<String> validHeaders = BaseUtils.toList("010", "011", "016", "017", "018", "019");
        return (validHeaders.contains(phoneNumber.substring(0, 3)));
    }

    public static Intent getIntent(Context context, Class<?> activity) {
        Intent intent = new Intent(context, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP); //  | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public static Intent getIntentWithClearStacks(Context context, Class<?> activity) {
        Intent intent = new Intent(context, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public static DateTime toDateTime(long timestamp) {
        return new DateTime(new Date(timestamp));
    }

    // url = file path or whatever suitable URL you want.
    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static boolean isBetween(int value, int from, int to) {
        return (from <= value) && (value <= to);
    }

    public static boolean isBetween(DateTime target, DateTime from, DateTime to) {
        if (target.isBefore(from)) return false;
        if (target.isAfter(to)) return false;
        return true;
    }

    public static boolean isBetweenByHm(DateTime dateTime, double from, double to) {
        double Hm = dateTime.getHourOfDay() + dateTime.getMinuteOfHour() / 60.0;
        return (from <= Hm) && (Hm < to);
    }

    public static String toCurrencyStr(int number) {
        return currencyFormat.format(number) + "원";
    }

    public static String toCurrencyOnlyStr(int number) {
        return currencyFormat.format(number);
    }

    public static String toCurrencyInverseSign(int number) {
        return String.format("%s%s", getSignStr(-number), currencyFormat.format(number).replace("+", "").replace("-", ""));
    }

    private static String getSignStr(int number) {
        if (number > 0)
            return "+";
        else if (number < 0)
            return "-";
        else
            return "";
    }

    public static String toCurrencySign(int number) {
        return String.format("%s%s", getSignStr(number), currencyFormat.format(number).replace("+", "").replace("-", ""));
    }

    public static String toSignStr(int number) {
        if (number > 0)
            return String.format("+%s", Integer.toString(number));
        else if (number == 0)
            return String.format("--");
        else if (number < 0)
            return String.format("%s", Integer.toString(number));
        else
            return "";
    }


    public static String toCommaStr(int number) {
        return currencyFormat.format(number);
    }

    public static String toCommaStr(List<?> list) {
        String result = Empty.string;
        if (list.size() <= 0) return result;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(0) instanceof String) {
                result = result + String.format("%s,", list.get(i));
            } else if (list.get(0) instanceof Integer) {
                result = result + String.format("%d,", list.get(i));
            } else if (list.get(0) instanceof SlipItemType) {
                // Log.i("DEBUG_JS", String.format("[MyUtils.toCommaStr] %d, %s, %s", i, ((SlipItemType) list.get(i)).toSimpleString(), ((SlipItemType) list.get(i)).toUiStr()));
                result = result + String.format("%s, ", ((SlipItemType) list.get(i)).uiName);
            }
        }
        result = result.trim();
        return result.substring(0, result.length() - 1);
    }

    public static String toPhoneFormat(String phoneNumber) {
        // +82-10-1234-5678, 010-1234-5678, 01012345678, _, 02-1234-5678, fdsf, .....다양한 입력 형태에 대응
        if (phoneNumber == null) return "";
        if (phoneNumber.isEmpty()) return "";
        if (isValidPhoneNumber(toSimpleForm(phoneNumber)) == false)
            return phoneNumber;
        phoneNumber = toSimpleForm(phoneNumber);
        if (phoneNumber.length() == 10) {
            return String.format("%s-%s-%s", phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), phoneNumber.substring(7, phoneNumber.length()));
        } else if (phoneNumber.length() == 11) {
            return String.format("%s-%s-%s", phoneNumber.substring(0, 3), phoneNumber.substring(3, 7), phoneNumber.substring(7, phoneNumber.length()));
        } else {
            return phoneNumber;
        }
    }

    public static String toSimpleForm(String phoneFormat) {
        return phoneFormat.replace("+82", "0").replace(" ", "").replace("-", "");
    }

    public static String getRandomPassword(int len) {
        StringBuilder string = new StringBuilder(len);
        int position = random.nextInt(len);
        for (int i = 0; i < len; i++) {
            if (i == position) {
                string.append(numbers.charAt(random.nextInt(numbers.length())));
            } else {
                string.append(alphabetLower.charAt(random.nextInt(alphabetLower.length())));
            }
        }
        return string.toString().trim();
    }

    public static String getRandomNumber(int len) {
        StringBuilder string = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            string.append(numbers.charAt(random.nextInt(numbers.length())));
        return string.toString().trim();
    }

    public static boolean isSameDay(DateTime date1, DateTime date2) {
        return ((date1.getYear() == date2.getYear()) && (date1.getMonthOfYear() == date2.getMonthOfYear())
                && (date1.getDayOfMonth() == date2.getDayOfMonth()));
    }

    public static String to4DStr(int num) {
        String numStr = Integer.toString(num);
        if (num < 0) {
            numStr = "0000";
        } else if (num < 10) {
            numStr = "000" + numStr;
        } else if (num < 100) {
            numStr = "00" + numStr;
        } else if (num < 1000) {
            numStr = "0" + numStr;
        }
        return numStr;
    }

    public static ArrayList<Integer> reverse(ArrayList<Integer> ints) {
        Collections.sort(ints);
        Collections.reverse(ints);
        return ints;
    }

//    public static List<Integer> toList(int[] ints) {
//        List<Integer> results = new ArrayList<Integer>(0);
//        for (int i : ints) {
//            results.addInteger(i);
//        }
//        return results;
//    }
//
//    public static List<String> toList(String[] strs) {
//        ArrayList<String> results = new ArrayList<String>(0);
//        for (String str : strs)
//            results.addInteger(str);
//        return results;
//    }
//
//    public static List<String> toList(String str) {
//        return toList(new String[]{str});
//    }

    public static int[] toInts(ArrayList<Integer> integers) {
        int[] results = new int[integers.size()];
        int i = 0;
        for (Integer integer : integers) {
            results[i] = integer;
            i = i + 1;
        }
        return results;
    }


    public static String toSubStr(String string, int length) {
        //		Log.i("DEBUG_JS",String.format("[BaseUtils.toSubStr] %s",((string.length() <= length)? string : string.substring(1, length))));
        return ((string.length() <= length) ? string : string.substring(1, length));
    }

    public static String toSubStrEnd(String string, int length) {
        //		Log.i("DEBUG_JS",String.format("[BaseUtils.toSubStr] %s",((string.length() <= length)? string : string.substring(1, length))));
        return ((string.length() <= length) ? string : string.substring(string.length() - length, string.length()));
    }

    public static String toStr(DateTime dateTime) {
        if (dateTime == null)
            dateTime = Empty.dateTime;
        //Log.i("DEBUG_JS",String.format("[BaseUtils.toStr] %s", dateTime.toDbString()));
        return fullFormat.print(dateTime);
    }

    public static String toStr(DateTime dateTime, DateTimeFormatter format) {
        if (dateTime == null)
            dateTime = Empty.dateTime;
        return format.print(dateTime);
    }

    public static DateTime toDateTime(String dataTimeStr, DateTimeFormatter format) {
        return DateTime.parse(dataTimeStr, format);
    }

    public static DateTime toDateTime(String dateTimeStr) {
        return toDateTime(dateTimeStr, false);
    }

    private static DateTime toDateTime(String dateTimeStr, boolean doIgnoreError) {
        try {
            if (dateTimeStr == null) {
                return Empty.dateTime;
            } else if (dateTimeStr.isEmpty()) {
                return Empty.dateTime;
            } else if (dateTimeStr.contains("T")) {
                return DateTime.parse(dateTimeStr);
            } else {
                return fullFormat.parseDateTime(dateTimeStr);
            }

        } catch (IllegalArgumentException e) {
            if (doIgnoreError == false)
                Log.e("DEBUG_JS", String.format("[toDateTime] invalid dateTime format : %s", dateTimeStr));
            return Empty.dateTime;
        }
    }

    public static DateTime createMonthDateTime() {
        return toMonth(DateTime.now().getYear(), DateTime.now().getMonthOfYear());
    }

    public static DateTime createDay() {
        return toDay(DateTime.now());
    }

    public static DateTime toMonth(int year, int month) {
        return new DateTime(year, month, 1, 0, 0);
    }

    public static DateTime toMonth(DateTime dateTime) {
        return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(), 1, 0, 0);
    }

    public static String getMDWithWeekDay(DateTime dateTime) {
        return String.format("%s (%s)", dateTime.toString(BaseUtils.MD_FixedSize_Format), getDayOfWeekKor(dateTime));
    }

    public static String toStringWithWeekDay(DateTime dateTime, DateTimeFormatter formatter) {
        if (formatter == YMDHmFormat) {
            return String.format("%s %s %s", dateTime.toString(YMD_Format), getDayOfWeekKor(dateTime), dateTime.toString(Hm_Format));
        } else if (formatter == MDHm_Format || formatter == MDHm_Kor_Format) {
            return String.format("%s %s %s", dateTime.toString(MD_Format), getDayOfWeekKor(dateTime), dateTime.toString(Hm_Format));
        } else if (formatter == MDHm_Twolines_Format) {
            return String.format("%s %s\n%s", dateTime.toString(MD_Format), getDayOfWeekKor(dateTime), dateTime.toString(Hm_Format));
        } else if (formatter == MD_Kor_Format) {
            return String.format("%s (%s)", dateTime.toString(formatter), getDayOfWeekKor(dateTime));
        } else {
            return String.format("%s (%s)", dateTime.toString(formatter), getDayOfWeekKor(dateTime));
        }
    }

    public static DateTime createWeek() {
        return toWeek(DateTime.now());
    }

    public static DateTime toWeek(DateTime dateTime) {
        if (dateTime.getDayOfWeek() == DateTimeConstants.SUNDAY) {
            return toDay(dateTime);
        } else {
            return BaseUtils.toDay(dateTime).withDayOfWeek(DateTimeConstants.MONDAY).minusDays(1); // SUNDAY
        }
    }

    public static String getDayOfWeekKor(DateTime dateTime) {
        switch (dateTime.getDayOfWeek()) {
            case 1:
                return "월";
            case 2:
                return "화";
            case 3:
                return "수";
            case 4:
                return "목";
            case 5:
                return "금";
            case 6:
                return "토";
            case 7:
                return "일";
        }
        return "";
    }

    public static DateTime toDay(DateTime dateTime) {
        return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), 0, 0);
    }

    public static DateTime toDateTime(int hour) {
        DateTime now = DateTime.now();
        int days = hour / 24;
        return new DateTime(now.getYear(), now.getMonthOfYear(), now.getDayOfMonth(), hour % 24, 0).plusDays(days);
    }

    public static DateTime toDateTime(Calendar calendar) {
        if (calendar == null)
            calendar = Empty.calendar();
        return toDateTime(toStr(calendar));
    }

    public static Calendar toCal(DateTime dateTime) {
        if (dateTime == null)
            dateTime = Empty.dateTime;
        return toCal(toStr(dateTime));
    }

    public static String toStr(Calendar calendar) {
        if (calendar == null)
            calendar = Empty.calendar();
        return formatter.format(calendar.getTime());
    }

    public static Calendar toCal(String string) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(formatter.parse(string));
        } catch (ParseException e) {
            Log.e("DEBUG_JS", String.format("[BaseUtils.toCal] invalid calendar string %s", string));
        }
        return calendar;
    }

    public static boolean isInteger(String str) {
        if (str == null) return false;
        Pattern integerPattern = Pattern.compile("[+-]?\\d+");
        return integerPattern.matcher(str).matches();
    }

    public static boolean isFloat(String str) {
        if (str == null) return false;
        Pattern floatPattern = Pattern.compile("[+-]?\\d+\\.?\\d+");
        Pattern integerPattern = Pattern.compile("[+-]?\\d+");
        return floatPattern.matcher(str).matches() || integerPattern.matcher(str).matches();
    }

    public static int toInt(String str) {
        return toInt(str, 0);
    }

    public static int toInt(Integer integer) {
        if (integer == null)
            return 0;
        else
            return integer;
    }

    public static int toInt(String str, int defaultValue) {
        if (TextUtils.isEmpty(str))
            return defaultValue;
        str = str.replace(",", "");
        try {
            if (isInteger(str)) {
                return Integer.valueOf(str);
            } else {
                return defaultValue;
            }
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static float toFloat(String str, float defaultValue) {
        return (float) toDouble(str, defaultValue);
    }

    public static double toDouble(String str, double defaultValue) {
        if (TextUtils.isEmpty(str))
            return defaultValue;
        str = str.replace(",", "");
        try {
            if (isFloat(str)) {
                return Double.valueOf(str);
            } else {
                return defaultValue;
            }
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static int toCurrencyInt(String str) {
        if (TextUtils.isEmpty(str))
            return 0;
        str = str.replace(",", "").replace("원", "").trim();
        if (isInteger(str)) {
            return BaseUtils.IntegerValueOf(str, 0);
        } else {
            return 0;
        }
    }

    public static String toStr(String[] strings) {
        if (strings.length <= 0)
            return Empty.string;
        String result = "";
        for (int i = 0; i < strings.length; i++) {
            result = result + strings[i] + " ";
        }
        return result;
    }


    public static int toIndex(int size) throws IllegalArgumentException {
        if (size < 0) {
            throw new IllegalArgumentException(String.format("[toIndex] invalid size %d", size));
        }
        return (size - 1);
    }

    public static boolean isEmail(String Email) {
        if (Email == null) return false;
        //return Pattern.matches("^([0-9a-zA-Z_-]+)@([0-9a-zA-Z_-]+)(\\.[0-9a-zA-Z_-]+){1,2}$",Email.trim());
        // ������ jeongs.choi@samsung.com ���� ���� .�� ������ �ȴ�.
        return Pattern.matches("[\\w\\~\\-\\.]+@[\\w\\~\\-]+(\\.[\\w\\~\\-]+)+", Email.trim());
    }


    public static boolean toBoolean(int value) throws IllegalArgumentException {
        if ((Integer) value == null) {
            Log.e("DEBUG_JS", String.format("[BaseUtils.toBoolean] null value is found"));
            return false;
        }
        if ((value == 0) || (value == 1)) {
            return (value == 1);
        } else {
            throw new IllegalArgumentException(String.format("[toBoolean] %d is not boolean", value));
        }
    }

    public static int toInt(boolean value) {
        return ((value) ? 1 : 0);
    }

    public static boolean hasError(int value) {
        return (value == -1);
    }

    public static String toMessage(boolean bool, String trueMessage, String falseMessage) {
        if (bool) {
            return trueMessage;
        } else {
            return falseMessage;
        }
    }

    public static String toLine(String str) {
        return str + "\n";
    }

    public static String toDbStr(boolean value) {
        return Integer.toString(toInt(value));
    }

    public static String toDbStr(int value) {
        return Integer.toString(value);
    }

    public static String toStr(boolean value) {
        if (value == true) {
            return "True";
        } else {
            return "False";
        }
    }

    public static String toStr(Intent value) {
        if (value == null) {
            return "null";
        } else
            return "not null";
    }

    public static boolean isExist(String fullPath) {
        File file = new File(fullPath);
        return (file.exists());
    }


    public static void toFile(Context context, String text, String fileName) {
        File file = new File(fileName);
        MyFiles.createFileIfNotExist(file);
        try {
            FileOutputStream out = new FileOutputStream(file);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
            writer.write(text);
            writer.close();
            out.close();
            MyDevice.refresh(context, file);
            //Log.i("DEBUG_JS",String.format("[toFile] %s Saved...",fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String toStr(String fileName) {
        String line, text = "";
        File file = new File(fileName);
        if (file.exists() == false) {
            return Empty.string;
        }

        try {
            FileInputStream in = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            while ((line = reader.readLine()) != null) {
                text = text + line;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static List<DateTime> getMonthsBetween(int fromYear, int fromMonth, int toYear, int toMonth) {
        DateTime fromDateTime = new DateTime(fromYear, fromMonth, 1, 0, 0);
        DateTime toDateTime = new DateTime(toYear, toMonth, 1, 0, 0);
        if (toDateTime.isBefore(fromDateTime))
            return new ArrayList<>(); // 입력정보 오류

        List<DateTime> results = new ArrayList<>();
        for (int month = 0; month < 12*5; month++) { // 최근 5년까지만 조사
            DateTime dateTime = toDateTime.minusMonths(month);
            if (dateTime.isBefore(fromDateTime))
                break;
            results.add(dateTime);
        }
        return results;
    }

    public static DateTime getRandomDateTime() {
        Random random = new Random();
        return new DateTime(2010 + random.nextInt(5), 1 + random.nextInt(12),
                1 + random.nextInt(28), random.nextInt(24), random.nextInt(60), random.nextInt(60));
    }

    public static String[] getFileNameList(String dirName, String ext) {
        try {
            FilenameFilter fileFilter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.endsWith(".png");
                } //end accept
            };
            File file = new File(dirName);
            File[] files = file.listFiles(fileFilter);
            String[] titleList = new String[files.length];
            for (int i = 0; i < files.length; i++) {
                titleList[i] = files[i].getName();
            }
            return titleList;
        } catch (Exception e) {
            return new String[]{};
        }
    }


    public static <T extends Enum<T>> T toEnum(Class<T> c, String string) {
        if (c != null && string != null) {
            try {
                return Enum.valueOf(c, string.trim().toUpperCase());
            } catch (IllegalArgumentException ex) {
            }
        }
        return null;
    }

    public static String toStr(int[] ints) {
        String result = Empty.string;

        if (ints.length >= 1) {
            result = String.format("size = %d :", ints.length);
            for (int i = 0; i < ints.length; i++) {
                result = result + String.format("[%d] %d,", i, ints[i]);
            }
        }
        return result;
    }

    public static String toStr(Set<?> records) {
        return toStr(new ArrayList<>(records));
    }

    public static String toStr(List<?> objects) {
        List<String> results = new ArrayList<>();
        if (objects.isEmpty()) return "";

        for (Object object : objects) {
            if (object instanceof String) {
                results.add((String) object);
            } else if (object instanceof Integer) {
                results.add(Integer.toString((Integer) object));
            } else {
                results.add(object.toString());
            }
        }
        return TextUtils.join(",", results);
    }

}
