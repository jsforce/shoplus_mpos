package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.MyCheckBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Medivh on 2019-02-07.
 */

public class ItemNameDialog extends BaseDialog {
    private EditText etName; // edit text in dialog
    private MyCheckBox cbCheckSameName;
    private List<LinearLayout> containers = new ArrayList<>();
    private List<List<String>> defaultKeywords = new ArrayList<>();
    private ItemDB itemDB;
    private int itemId = 0;
    private final List<String> KEY_Containers = Arrays.asList(new String[]{"itemname.dialog.container1", "itemname.dialog.container2", "itemname.dialog.container3"});
    private MyOnTask<Pair<ItemRecord, ItemRecord>> onTask;

    public ItemNameDialog(Context context, int itemId, MyOnTask<Pair<ItemRecord, ItemRecord>> onTask) {
        super(context);
        this.itemId = itemId;
        this.onTask = onTask;
        setDialogWidth(0.95, 0.8);
        cbCheckSameName = new MyCheckBox(globalDB, getView(), R.id.dialog_itemname_checkSameName,
                "check.same.name", true);
        itemDB = ItemDB.getInstance(context);

        etName = findViewById(R.id.dialog_itemname_edittext);
        etName.setText(itemDB.getRecordBy(itemId).name);
        etName.setSelection(etName.getText().length());
        setOnClick(R.id.dialog_itemname_apply, R.id.dialog_itemname_cancel, R.id.dialog_itemname_clear,
                R.id.dialog_itemname_setKeywords);
        MyDevice.showKeyboard(context, etName, 300);

        MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_itemname_textview, R.id.dialog_itemname_edittext);
        containers.add((LinearLayout) findViewById(R.id.dialog_itemname_keywordContainer1));
        containers.add((LinearLayout) findViewById(R.id.dialog_itemname_keywordContainer2));
        containers.add((LinearLayout) findViewById(R.id.dialog_itemname_keywordContainer3));
        // 6 x 3으로 공간을 고정하자
        defaultKeywords.add(Arrays.asList(new String[]{"JK", "OPS", "T", "BL", "CD", "스카프"}));
        defaultKeywords.add(Arrays.asList(new String[]{"NB", "니트", "CT", "바지", "SK", "SL"}));
        defaultKeywords.add(Arrays.asList(new String[]{"JP", "MTM", "나시", "조끼", "PT", "TC"}));
        refreshKeywords();

        setVisibility(View.GONE, R.id.dialog_itemname_message);
        show();
    }

    private void refreshKeywords() {
        if (containers.size() != KEY_Containers.size()) {
            Log.e("DEBUG_JS", String.format("[ItemNameDialog.refreshKeywords] containers.size() != KEY_Containers.length"));
            return;
        }

        int index = 0;
        int minWidthPx = MyDevice.toPixel(context, 40);
        for (LinearLayout container : containers) {
            String containerStrings = globalDB.getValue(KEY_Containers.get(index));
            if (containerStrings.isEmpty())
                containerStrings = keyValueDB.getValue(KEY_Containers.get(index)); // 기존 설정과 호환
            container.removeAllViews();
            List<String> keywords1 = TextUtils.isEmpty(containerStrings) ? defaultKeywords.get(index) : Arrays.asList(containerStrings.split(","));
            container.setVisibility(keywords1.size() == 0 ? View.GONE : View.VISIBLE);
            for (String keyword : keywords1) {
                TextView textView = MyView.createTextView(context, keyword, 16, 5, ColorType.Black.colorInt);
                textView.setBackgroundColor(ColorType.Azure.colorInt);
                textView.setMinWidth(minWidthPx);
                textView = MyView.setMargin(context, textView, 0, 0, 5, 0);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String selectedString = ((TextView) view).getText().toString().trim();
                        String name = etName.getText().toString().trim();
                        etName.setText(String.format("%s%s%s", name, name.isEmpty() ? "" : " ", selectedString));
                        etName.setSelection(etName.getText().length());
                    }
                });
                MyView.setTextViewByDeviceSize(context, textView);
                container.addView(textView);
            }
            index++;
        }
    }

    private List<List<String>> getKeywords() {
        List<List<String>> results = new ArrayList<>();
        for (LinearLayout container : containers) {
            List<String> subResult = new ArrayList<>();
            for (View textView : MyView.getChildViews(container))
                subResult.add(((TextView) textView).getText().toString());
            results.add(subResult);
        }
        return results;
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_item_name;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_itemname_clear) {
            etName.setText("");

        } else if (view.getId() == R.id.dialog_itemname_apply) {
            final ItemRecord itemRecord = itemDB.getRecordBy(itemId);
            final ItemRecord updatedRecord = itemRecord.clone();
            updatedRecord.name = etName.getText().toString();
            ItemCategoryType itemCategoryType = ItemCategoryType.getType(updatedRecord.name);
            if (itemCategoryType != ItemCategoryType.NONE)
                updatedRecord.category = itemCategoryType;

            if (cbCheckSameName.isChecked() && itemDB.hasStringNoCase(ItemDB.Column.name, etName.getText().toString().trim())) {
                new MyAlertDialog(context, "같은 이름 발견", "같은 이름으로 등록하시겠습니까?") {
                    @Override
                    public void yes() {
                        if (onTask != null)
                            onTask.onTaskDone(Pair.create(itemRecord, updatedRecord));
                        MyDevice.hideKeyboard(context, etName);
                        dismiss();
                    }
                };
            } else {
                if (onTask != null)
                    onTask.onTaskDone(Pair.create(itemRecord, updatedRecord));
                MyDevice.hideKeyboard(context, etName);
                dismiss();
            }
        } else if (view.getId() == R.id.dialog_itemname_cancel) {
            MyDevice.hideKeyboard(context, etName);
            dismiss();

        } else if (view.getId() == R.id.dialog_itemname_setKeywords) {
            new KeywordSettingDialog(context, getKeywords()) {
                @Override
                protected void onApplyClick() {
                    refreshKeywords();
                }
            };
        }
    }

    abstract class KeywordSettingDialog extends BaseDialog {
        private List<LinearLayout> containers = new ArrayList<>();
        private List<List<String>> keywords = new ArrayList<>();

        public KeywordSettingDialog(Context context, List<List<String>> keywords) {
            super(context);
            setDialogWidth(0.95, 0.6);
            this.keywords = keywords;
            findTextViewById(R.id.dialog_itemname_textview, "빠른입력단어 수정");
            findTextViewById(R.id.dialog_itemname_message, "클릭해서 원하는 단어로 수정하세요");
            setVisibility(View.GONE, R.id.dialog_itemname_container,
                    R.id.dialog_itemname_checkSameName, R.id.dialog_itemname_setKeywords);
            containers.add((LinearLayout) findViewById(R.id.dialog_itemname_keywordContainer1));
            containers.add((LinearLayout) findViewById(R.id.dialog_itemname_keywordContainer2));
            containers.add((LinearLayout) findViewById(R.id.dialog_itemname_keywordContainer3));
            setOnClick(R.id.dialog_itemname_apply, R.id.dialog_itemname_cancel);
            refreshKeywords();
            show();
        }

        private void refreshKeywords() {
            if (containers.size() != keywords.size()) {
                Log.e("DEBUG_JS", String.format("[KeywordSettingDialog.refreshKeywords] containers.size() != keywords.size()"));
                return;
            }
            int minWidthPx = MyDevice.toPixel(context, 40);
            for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
                LinearLayout container = containers.get(rowIndex);
                container.removeAllViews();
                List<String> keywordsRow = keywords.get(rowIndex);
                for (int columnIndex = 0; columnIndex < 6; columnIndex++) {
                    String keyword = columnIndex < keywordsRow.size() ? keywordsRow.get(columnIndex) : "---";
                    TextView textView = MyView.createTextView(context, keyword, 16, 5, ColorType.Black.colorInt);
                    textView.setBackground(BaseUtils.getDrawable(context, R.drawable.background_round_button));
                    textView.setMinWidth(minWidthPx);
                    textView = MyView.setMargin(context, textView, 0, 0, 5, 0);
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View view) {
                            String title = ((TextView) view).getText().toString().trim();
                            new UpdateValueDialog(context, "빠른입력 수정", "용어를 수정할 수 있습니다", title) {
                                @Override
                                public void onApplyClick(String newValue) {
                                    ((TextView) view).setText(newValue);
                                }
                            };
                        }
                    });
                    MyView.setTextViewByDeviceSize(context, textView);
                    container.addView(textView);
                }
            }
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_item_name;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_itemname_apply) {
                int index = 0;
                for (List<String> keywords : getKeywords()) {
                    globalDB.put(KEY_Containers.get(index), TextUtils.join(",", keywords));
                    index++;
                }
                dismiss();
                onApplyClick();

            } else if (view.getId() == R.id.dialog_itemname_cancel) {
                dismiss();
            }
        }

        abstract protected void onApplyClick();

        private List<List<String>> getKeywords() {
            List<List<String>> results = new ArrayList<>();
            for (LinearLayout container : containers) {
                List<String> subResult = new ArrayList<>();
                for (View textView : MyView.getChildViews(container))
                    subResult.add(((TextView) textView).getText().toString());
                results.add(subResult);
            }
            return results;
        }
    }
}
