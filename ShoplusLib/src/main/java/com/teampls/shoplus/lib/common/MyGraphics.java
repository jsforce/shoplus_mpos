package com.teampls.shoplus.lib.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.GlobalHistogramBinarizer;
import com.google.zxing.common.HybridBinarizer;
import com.teampls.shoplus.lib.enums.ColorType;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyGraphics {
    public enum ScaleReference {width, height, auto}

    public static final int JPEG_QUALITY = 92;
    public static final int PNG_QUALITY = 100;
    public static final int DefaultWidth = 720;

    public static String decodeQrImage(Context context, String imagePath) {
        File file = new File(imagePath);
        if (file.exists() == false) {
            MyUI.toastSHORT(context, String.format("파일이 존재하지 않습니다"));
            return "";
        }
        Bitmap bitmap = MyGraphics.toBitmap(imagePath);
        Reader reader = new MultiFormatReader();
        int[] pixelArray = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(pixelArray, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        LuminanceSource source = new RGBLuminanceSource(bitmap.getWidth(), bitmap.getHeight(), pixelArray);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(source));
        try {
            Result result = reader.decode(binaryBitmap);
            return result.getText();
        } catch (NotFoundException e) {
            return decodeQrCode(context, MyGraphics.removeBoundary(bitmap, 10, true)); // retry (10% 여백 제거하고)
        } catch (ChecksumException e) {
            Log.e("DEBUG_JS", String.format("[MainViewPager.onMyClick] Check sum : %s", e.getLocalizedMessage()));
        } catch (FormatException e) {
            Log.e("DEBUG_JS", String.format("[MainViewPager.onMyClick] Format : %s", e.getLocalizedMessage()));
        }
        return "";
    }

    private static String decodeQrCodeGlobal(Context context, Bitmap bitmap, LuminanceSource source) {
        try {
            Result result = new MultiFormatReader().decode(new BinaryBitmap(new GlobalHistogramBinarizer(source)));
            return result.getText();
        } catch (NotFoundException e) {
            Log.i("DEBUG_JS", String.format("[MyGraphics.decodeQrCodeGlobal] %s", e.getLocalizedMessage()));
            return decodeQrCode(context, MyGraphics.removeBoundary(bitmap, 10, true)); // retry (10% 여백 제거하고)
        } catch (Exception e) {
            Log.i("DEBUG_JS", String.format("[MyGraphics.decodeQrCodeGlobal] %s", e.getLocalizedMessage()));
        }
        return "";
    }

    private static String decodeQrCode(Context context, Bitmap bitmap) {
        Reader reader = new MultiFormatReader();
        int[] pixelArray = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(pixelArray, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        LuminanceSource source = new RGBLuminanceSource(bitmap.getWidth(), bitmap.getHeight(), pixelArray);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(source));
        try {
            Result result = reader.decode(binaryBitmap);
            return result.getText();
        } catch (NotFoundException e) {
            MyUI.toast(context, String.format("QR 코드가 발견되지 않았습니다"));
        } catch (ChecksumException e) {
            Log.e("DEBUG_JS", String.format("[MainViewPager.onMyClick] Check sum : %s", e.getLocalizedMessage()));
        } catch (FormatException e) {
            Log.e("DEBUG_JS", String.format("[MainViewPager.onMyClick] Format : %s", e.getLocalizedMessage()));
        }
        return "";
    }

    public static Bitmap createQRcode(Context context, int sizeDp, String string) {
        try {
            int sizePx = MyDevice.toPixel(context, sizeDp);
            BitMatrix result = new MultiFormatWriter().encode(string, BarcodeFormat.QR_CODE, sizePx, sizePx, null);
            int w = result.getWidth();
            int h = result.getHeight();
            int[] pixels = new int[w * h];
            for (int y = 0; y < h; y++) {
                int offset = y * w;
                for (int x = 0; x < w; x++) {
                    pixels[offset + x] = result.get(x, y) ? ColorType.Black.colorInt : ColorType.White.colorInt;
                }
            }
            Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
            return bitmap;
        } catch (WriterException e) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.createQRcode] %s", e.getLocalizedMessage()));
            return Empty.bitmap;
        }
    }

    public static byte[] getStandardImageByte(byte[] bytes) {
        if (bytes.length / 1e6 >= 2) {
            Log.w("DEBUG_JS", String.format("[ImageRecord.ImageRecord] before %dKB >= 2MB", (int) (bytes.length / 1e3)));
            Bitmap bitmap = toBitmap(bytes);
            if (bitmap.getHeight() >= bitmap.getWidth())
                bitmap = MyGraphics.resizeByWidth(bitmap, BaseGlobal.ImageWidthMax);
            else
                bitmap = MyGraphics.resizeByHeight(bitmap, BaseGlobal.ImageWidthMax);
            byte[] result = MyGraphics.toByte(bitmap, Bitmap.CompressFormat.JPEG, 94);
            Log.w("DEBUG_JS", String.format("[ImageRecord.ImageRecord] after %dKB", (int) (result.length / 1e3)));
            return result;
        } else {
            return bytes;
        }
    }

    public static Bitmap toBitmap(View view) {
        if (view == null) return Empty.bitmap;
        if (view.getWidth() < 0 || view.getHeight() < 0) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.toBitmap] view %d x %d", view.getWidth(), view.getHeight()));
            return Empty.bitmap;
        }
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }

    public static Bitmap cropCenter(Bitmap bitmap) {
        if ((bitmap.getWidth() <= 0) || (bitmap.getHeight() <= 0)) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.removeBoundary] bitmap size (%dx%d)", bitmap.getWidth(), bitmap.getHeight()));
            return bitmap;
        }
        return Bitmap.createBitmap(bitmap, bitmap.getWidth() / 3, bitmap.getHeight() / 3, bitmap.getWidth() / 3, bitmap.getHeight() / 3);
    }

    public static Bitmap removeBoundary(Bitmap bitmap, int cutoffRate, boolean isOriginalSize) {
        if ((bitmap.getWidth() <= 0) || (bitmap.getHeight() <= 0)) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.removeBoundary] bitmap size (%dx%d)", bitmap.getWidth(), bitmap.getHeight()));
            return bitmap;
        }
        if (cutoffRate == 0 || cutoffRate >= 100)
            return bitmap;
        else {
            int widthOffset = bitmap.getWidth() * cutoffRate / 100;
            int heightOffset = bitmap.getHeight() * cutoffRate / 100;
            Bitmap result = Bitmap.createBitmap(bitmap, widthOffset, heightOffset, bitmap.getWidth() - widthOffset, bitmap.getHeight() - heightOffset);
            if (isOriginalSize)
                return resize(result, bitmap.getWidth(), bitmap.getHeight());
            else
                return result;
        }
    }

    public static List<String> createStandardImage(Context context, String dir, Bitmap bitmap) {
        List<String> results = new ArrayList<>();
        if (bitmap == null) return results;
        if (Empty.isEmpty(bitmap)) return results;

        String standardFilePath = MyDevice.createTempFilePath(dir, "jpg");
        boolean created = MyGraphics.toFile(bitmap, standardFilePath);
        if (created) {
            return createStandardImage(context, dir, Arrays.asList(standardFilePath));
        } else {
            MyUI.toastSHORT(context, String.format("이미지 변환에 실패했습니다"));
            return results;
        }
    }

    public static List<String> createStandardImage(Context context, String dir, List<String> originalFilePaths) {
        List<String> results = new ArrayList<>();
        if (originalFilePaths == null) return results;
        if (originalFilePaths.size() <= 0) return results;

        for (String originalFilePath : originalFilePaths) {
            if ((originalFilePath.length() <= 0) || (new File(originalFilePath).exists() == false)) {
                Log.e("DEBUG_JS", String.format("[MainViewPager.createStandardImage] invalid file : %s", originalFilePath));
                continue;
            }
            String standardFilePath = MyDevice.createTempFilePath(dir, "jpg");
            boolean created = MyGraphics.toStandardImageFile(originalFilePath, standardFilePath);
            if (created) {
                results.add(standardFilePath);
                MyDevice.refresh(context, new File(standardFilePath));
            }
        }
        return results;
    }

    public static String getImageInfo(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
        if (filePath == null) return "";
        options.inJustDecodeBounds = true;
        options.inPurgeable = true; // inPurgeable is used to free up memory while required
        BitmapFactory.decodeFile(filePath, options);
        return String.format("path = %s, (%d x %d), %d kB", filePath.replace("/storage/emulated/0/", ""), options.outWidth, options.outHeight, new File(filePath).length() / 1000);
    }

    public static String getImageInfo(Bitmap bitmap) {
        if (bitmap == null) return "";
        return String.format("(%d x %d) %dkB", bitmap.getWidth(), bitmap.getHeight(), bitmap.getByteCount() / 1000);
    }

    public static Bitmap resizeAndPortrait(String sourcePath, String targetPath, int scaleFactor, int widthRef) {
        if (new File(sourcePath).exists() == false) return Empty.bitmap;
        if (targetPath == null) return Empty.bitmap;
        Bitmap bitmap = toBitmapScaleFactor(sourcePath, scaleFactor);
        if (MyGraphics.getExifOrientation(sourcePath) == 90)
            bitmap = MyGraphics.rotateBitmap(bitmap, 90);

        if (bitmap.getWidth() > bitmap.getHeight()) // landscape
            widthRef = widthRef * bitmap.getWidth() / bitmap.getHeight();

        if (bitmap.getWidth() > widthRef)
            bitmap = resizeByWidth(bitmap, widthRef);

        MyGraphics.toFile(bitmap, targetPath);
        //MyDevice.updateOrInsert(context, new File(targetPath));
        return bitmap;
    }

    public static Bitmap resizeBySampling(Bitmap bitmap, int scaleFactor) {
        byte[] bytes = MyGraphics.toByte(bitmap);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inPreferredConfig = Config.RGB_565;
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = (int) scaleFactor;
        bmOptions.inPurgeable = true;
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, bmOptions);

    }

    private static String toPortrait(Context context, String sourcePath) {
        if (BaseUtils.isExist(sourcePath) == false) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.toPortrait] %s not found", sourcePath));
            return "";
        }

        int degree = MyGraphics.getExifOrientation(sourcePath);
        switch (degree) {
            default:
                return sourcePath;
            case 90:
                Bitmap bitmap = MyGraphics.toBitmap(sourcePath, BaseGlobal.ImageWidthMax, ScaleReference.height);
                bitmap = MyGraphics.rotateBitmap(bitmap, degree);
                MyGraphics.toFile(bitmap, MyDevice.tempJPG);
                MyDevice.refresh(context, new File(MyDevice.tempJPG));
                return MyDevice.tempJPG;
        }
    }

    public static Bitmap resizeWithRatio(Bitmap originalBitmap, int width, int height) {
        float originalWidth = originalBitmap.getWidth(), originalHeight = originalBitmap.getHeight();
        Bitmap newBitmap = Bitmap.createBitmap((int) width, (int) height, Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        float scale = width / originalWidth;
        float xTranslation = 0.0f, yTranslation = (height - originalHeight * scale) / 2.0f;
        Matrix transformation = new Matrix();
        transformation.postTranslate(xTranslation, yTranslation);
        transformation.preScale(scale, scale);
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        canvas.drawBitmap(originalBitmap, transformation, paint);
        return newBitmap;
    }

    public static Bitmap resizeCenterCrop(Bitmap originalBitmap, int width, int height) {
        int originalWidth = originalBitmap.getWidth();
        int originalHeight = originalBitmap.getHeight();
        float xScale = (float) width / originalWidth;
        float yScale = (float) height / originalHeight;
        float scale = Math.max(xScale, yScale);

        //get the resulting size after scaling
        float scaledWidth = scale * originalWidth;
        float scaledHeight = scale * originalHeight;

        //figure out where we should translate to
        float dx = (width - scaledWidth) / 2;
        float dy = (height - scaledHeight) / 2;

        Bitmap newBitmap = Bitmap.createBitmap(width, height, originalBitmap.getConfig());
        Canvas canvas = new Canvas(newBitmap);
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        matrix.postTranslate(dx, dy);
        canvas.drawBitmap(originalBitmap, matrix, null);
        return newBitmap;
    }

    public static synchronized void setExifOrientation(String filepath) {
        try {
            ExifInterface exif = new ExifInterface(filepath);
            exif.setAttribute(ExifInterface.TAG_ORIENTATION, String.valueOf(ExifInterface.ORIENTATION_NORMAL));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static synchronized int getExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null; //exchangeable image file format

        try {
            exif = new ExifInterface(filepath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (exif == null) return 0;


        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
        if (orientation == -1) return 0;

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                degree = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                degree = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                degree = 270;
                break;
        }
        return degree;
    }

    public synchronized static Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        if (bitmap == null)
            return Empty.bitmap;

        if ((degrees % 360) == 0)
            return bitmap;

        Matrix matrix = new Matrix();
        matrix.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
        try {
            Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            //Log.i("DEBUG_JS", String.format("[BaseGlobal.rotateBitmap] image rotated %d", degrees));
            if (bitmap != newBitmap)
                bitmap = newBitmap;
            return newBitmap;
        } catch (OutOfMemoryError ex) {
            Log.i("DEBUG_JS", String.format("[BaseGlobal.rotateBitmap] out of memory"));
            return bitmap;
        }
    }

    public static Uri googleDriveToUri(Context context, Uri uri) {
        return MyGraphics.toUri(context, MyGraphics.toBitmap(context, uri));
    }

    // Pictures에 저장됨
    public static Uri toUri(Context context, Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, PNG_QUALITY, bytes);
        String path = Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    public static boolean toFile(byte[] byteArray, String filePath) {
        if (byteArray.length <= 0)
            Log.w("DEBUG_JS", String.format("[MyGraphics.toFile] size = 0, check file..."));
        File file = new File(filePath);
        MyFiles.createFileIfNotExist(file);
        try {
            FileOutputStream stream = new FileOutputStream(filePath);
            stream.write(byteArray);
            stream.close();
            Thread.sleep(1);
            // Log.i("DEBUG_JS", String.format("[MyGraphics.toFile] byte %dkB -> file %dkB", (int) byteArray.length/1000, (int) file.length()/1000));
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean toFile(Context context, int drawableResId, String filePath) {
        try {
            InputStream inputStream = context.getResources().openRawResource(+drawableResId);
            OutputStream outputStream = new FileOutputStream(new File(filePath));
            byte buff[] = new byte[1024];
            int len;
            while ((len = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, len);
            }
            outputStream.close();
            inputStream.close();
            return true;
        } catch (IOException e) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.toFile] %s", e.toString()));
            return false;
        }
    }


    public static boolean toFile(Bitmap bitmap, String filePath) {
        MyFiles.createFileIfNotExist(new File(filePath));
        try {
            FileOutputStream out = new FileOutputStream(filePath);
            if (filePath.substring(filePath.lastIndexOf(".") + 1).equals("png")) {
                bitmap.compress(CompressFormat.PNG, PNG_QUALITY, out);
            } else {
                bitmap.compress(CompressFormat.JPEG, JPEG_QUALITY, out);
            }
            out.flush();
            out.close();
            Thread.sleep(1);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static File toFile(Context context, Uri imageUri, String imageFileName) {
        MyFiles.createFileIfNotExist(new File(imageFileName));
        try {
            InputStream input = ((Activity) context).getContentResolver().openInputStream(imageUri);
            BufferedInputStream in = new BufferedInputStream(input);
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(imageFileName));
            byte[] buffer = new byte[input.available()];
            in.read(buffer);
            do {
                out.write(buffer);
            } while (in.read(buffer) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new File(imageFileName);
    }

    public static Bitmap resizeByWidth(Bitmap bitmap, int width) {
        int height = (width * bitmap.getHeight()) / bitmap.getWidth();
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    public static Bitmap resizeByHeight(Bitmap bitmap, int height) {
        int width = (height * bitmap.getWidth()) / bitmap.getHeight();
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    public static Bitmap resize(Bitmap bitmap, int width, int height) {
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    public static void resize(String imageFileName, int width, int height) {
        if (BaseUtils.isExist(imageFileName) == false) {
            throw new IllegalArgumentException(String.format("[resize] not exist: %s", imageFileName));
        }
        Bitmap bitmap = toBitmap(imageFileName);
        if (bitmap == null) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.resize] %s : bitmap == null", imageFileName));
            return;
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        try {
            FileOutputStream out = new FileOutputStream(imageFileName);
            bitmap.compress(CompressFormat.JPEG, JPEG_QUALITY, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bitmap.recycle();
        }
    }

    public static int getIntFromColor(int Red, int Green, int Blue) {
        Red = (Red << 16) & 0x00FF0000; //Shift red 16-bits and mask out other stuff
        Green = (Green << 8) & 0x0000FF00; //Shift Green 8-bits and mask out other stuff
        Blue = Blue & 0x000000FF; //Mask out anything not blue.

        return 0xFF000000 | Red | Green | Blue; //0xFF000000 for 100% Alpha. Bitwise OR everything together.
    }

    public static String toHex(int Red, int Green, int Blue) {
        return Integer.toHexString(toInt(Red, Green, Blue));
    }

    public static int toInt(int Red, int Green, int Blue) {
        Red = (Red << 16) & 0x00FF0000; //Shift red 16-bits and mask out other stuff
        Green = (Green << 8) & 0x0000FF00; //Shift Green 8-bits and mask out other stuff
        Blue = Blue & 0x000000FF; //Mask out anything not blue.

        return 0xFF000000 | Red | Green | Blue; //0xFF000000 for 100% Alpha. Bitwise OR everything together.
    }

    public static byte[] toByte(Bitmap bitmap) {
        return toByte(bitmap, Bitmap.CompressFormat.PNG, PNG_QUALITY);
    }

    public static byte[] toByte(Bitmap bitmap, Bitmap.CompressFormat format, int quality) {
        if (bitmap == null) {
            return Empty.bytes;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(format, quality, stream);
        byte[] imageInByte = stream.toByteArray();
        return imageInByte;
    }


    public static Bitmap toBitmap(byte[] byteArray) {
        return toBitmap(byteArray, 1);
    }

    public static Bitmap toBitmap(byte[] byteArray, int inSampleSize) {
        if (byteArray == null) {
            return Empty.bitmap;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
        options.inPurgeable = true; // inPurgeable is used to free up memory while required
        options.inSampleSize = inSampleSize;
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
        if (bitmap == null) {
            return Empty.bitmap;
        } else {
            return bitmap;
        }
    }

    public static BitmapFactory.Options getBitmapOption(byte[] byteArray) {
        BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
        if (byteArray == null) {
            return options;
        }
        options.inJustDecodeBounds = true;
        options.inPurgeable = true; // inPurgeable is used to free up memory while required
        BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
        return options;

    }

    public static byte[] toByte(InputStream is) {
        byte[] resBytes = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int read = -1;
        try {
            while ((read = is.read(buffer)) != -1) {
                bos.write(buffer, 0, read);
            }
            resBytes = bos.toByteArray();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resBytes;
    }

    public static byte[] toByte(String filePath) {
        File file = new File(filePath);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.toByte] %s not found", filePath));
            bytes = Empty.bytes;
        } catch (IOException e) {
            bytes = Empty.bytes;
        }
        return bytes;
    }

    public static int getSampleSize(int originalWidth, int originalHeight, int targetWidth, int targetHeight, ScaleReference reference) {
        double scaleFactor = 1.0;
        int result = 0;
        switch (reference) {
            case width:
                scaleFactor = targetWidth / originalWidth;
                break;
            case height:
                scaleFactor = targetHeight / originalHeight;
                break;
            case auto:
                if (Math.abs(targetWidth - originalWidth) >= Math.abs(targetHeight - originalHeight)) {
                    result = getSampleSize(originalWidth, originalHeight, targetWidth, targetHeight, ScaleReference.width);
                } else {
                    result = getSampleSize(originalWidth, originalHeight, targetWidth, targetHeight, ScaleReference.height);
                }
                break;
        }
        if (1 / scaleFactor > 1) {
            return (int) (1 / scaleFactor); // has round error
        } else {
            return 1;
        }
    }


    public static int getDimen(double originalWidth, double originalHeight, double targetWidth, double targetHeight, ScaleReference reference) {
        double scaleFactor = 1.0;
        double result = 0;
        switch (reference) {
            case width:
                scaleFactor = targetWidth / originalWidth;
                result = originalHeight * scaleFactor;
                //Log.i("DEBUG_JS", String.format("[MyGraphics.getDimen] original (%.0f x %.0f), target (%.0f, %.0f), new height = %.0f", originalWidth, originalHeight, targetWidth, targetHeight, result));
                break;
            case height:
                scaleFactor = targetHeight / originalHeight;
                result = (int) (originalWidth * scaleFactor);
                break;
            case auto:
                if (Math.abs(targetWidth - originalWidth) >= Math.abs(targetHeight - originalHeight)) {
                    result = getDimen(originalWidth, originalHeight, targetWidth, targetHeight, ScaleReference.width);
                } else {
                    result = getDimen(originalWidth, originalHeight, targetWidth, targetHeight, ScaleReference.height);
                }
                break;
        }
        return (int) result;
    }

    public static ImageView resize(ImageView imageView, Bitmap refBitmap, ScaleReference reference) {
        int imageViewWidth = imageView.getWidth();
        int imageViewHeight = imageView.getHeight();
        if ((imageViewWidth == 0) || (imageViewHeight == 0)) {
            return imageView;
        }

        double scaleFactor = 1.0;
        switch (reference) {
            case width:
                scaleFactor = imageViewWidth / refBitmap.getWidth();
                imageView.getLayoutParams().height = (int) (refBitmap.getHeight() * scaleFactor);
                break;
            case height:
                scaleFactor = imageViewHeight / refBitmap.getHeight();
                imageView.getLayoutParams().width = (int) (refBitmap.getWidth() * scaleFactor);
                break;
        }
        imageView.requestLayout();
        Log.i("DEBUG_JS", String.format("[MyGraphics.resize] imageView (%d x %d) -> (%d x %d) : originalImage (%d x %d), scaleFactor %.1f "
                , imageViewWidth, imageViewHeight, imageView.getWidth(), imageView.getHeight(), refBitmap.getWidth(), refBitmap.getHeight(), scaleFactor));
        return imageView;
    }

    public static Pair<Double, Double> getPortraitDimen(String imagePath) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bmOptions);
        double photoWidth = bmOptions.outWidth;
        double photoHeight = bmOptions.outHeight;
        if (MyGraphics.getExifOrientation(imagePath) == 90) {
            photoWidth = bmOptions.outHeight;
            photoHeight = bmOptions.outWidth;
        }
        ;
        return Pair.create(photoWidth, photoHeight);
    }

    public static String getStandardImageFile(String selectedImagePath) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, bmOptions);
        double photoWidth = bmOptions.outWidth;
        double photoHeight = bmOptions.outHeight;
        int degree = MyGraphics.getExifOrientation(selectedImagePath);
        if (degree == 90) {
            photoWidth = bmOptions.outHeight;
            photoHeight = bmOptions.outWidth;
        }
        ;
        int scaleFactor = MyGraphics.getScaleFactor(photoWidth, photoHeight, BaseGlobal.ImageWidthMax, MyGraphics.ScaleReference.width);

        String standardFilePath;
        if ((scaleFactor == 1) && (degree == 0) && (photoWidth <= BaseGlobal.ImageWidthMax)) {
            standardFilePath = selectedImagePath;
            Log.i("DEBUG_JS", String.format("[MyGraphics.getStandardImageFile] original file %s", MyGraphics.getImageInfo(selectedImagePath)));
        } else {
            standardFilePath = MyDevice.tempJPG;
            Bitmap bitmap = MyGraphics.resizeAndPortrait(selectedImagePath, standardFilePath, scaleFactor, BaseGlobal.ImageWidthMax);
            Log.i("DEBUG_JS", String.format("[MyGraphics.getStandardImageFile] scale %d, %s -> %s", scaleFactor, MyGraphics.getImageInfo(selectedImagePath), MyGraphics.getImageInfo(standardFilePath)));
        }
        return standardFilePath;
    }

    public static int getStandardImageWidth(double width, double height) {
        if (height >= width) { // portrait
            return BaseGlobal.ImageWidthMax;
        } else {  // landscape
            return (int) ((BaseGlobal.ImageWidthMax * width) / height);
        }
    }

    public static boolean toStandardImageFile(String sourceFilePath, String targetFilePath) {
        Pair<Boolean, Bitmap> result = toStandardBitmapAndImageFile(sourceFilePath, targetFilePath);
        return result.first;
    }

    public static Pair<Boolean, Bitmap> toStandardBitmapAndImageFile(String sourceFilePath, String targetFilePath) {
        if (new File(sourceFilePath).exists() == false)
            return Pair.create(false, Empty.bitmap);

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(sourceFilePath, bmOptions);
        Log.i("DEBUG_JS", String.format("[MyGraphics.toStandardBitmapAndImageFile] bitmap size %d x %d", bmOptions.outWidth, bmOptions.outHeight));
        int refWidth = getStandardImageWidth(bmOptions.outWidth, bmOptions.outHeight); // 720 (or 960)
        int degree = getExifOrientation(sourceFilePath);
        int scaleFactor = getStandardScaleFactor(bmOptions.outWidth, bmOptions.outHeight);
        int fileSize = ((int) (new File(sourceFilePath).length())) / 1000; // kB

        Bitmap bitmap = toBitmapScaleFactor(sourceFilePath, scaleFactor);  // resized by sampling
        if (bitmap == null) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.toStandardBitmapAndImageFile] bitmap = null"));
            return Pair.create(false, Empty.bitmap);
        }

        if (bmOptions.outWidth > refWidth)
            bitmap = resizeByWidth(bitmap, refWidth);  // resize to (720 x ?) or (? x 720)

        bitmap = rotateBitmap(bitmap, degree); // for samsung camera
        Pair<Boolean, Bitmap> result;
        if ((bmOptions.outWidth <= refWidth) && (degree == 0) && (fileSize <= BaseGlobal.ImageFileSizeMax)) {
            result = Pair.create(copy(sourceFilePath, targetFilePath), bitmap); // use original file
        } else {
            result = Pair.create(toFile(bitmap, targetFilePath), bitmap); // generated with bitmap
        }
        Log.i("DEBUG_JS", String.format("[MyGraphics.toStandard...] original (%dx%d) %d kB %s -> standard (%dx%d) %d kB %s",
                bmOptions.outWidth, bmOptions.outHeight, new File(sourceFilePath).length() / 1000, sourceFilePath,
                bitmap.getWidth(), bitmap.getHeight(), new File(targetFilePath).length() / 1000, targetFilePath));
        return result;
    }

    public static byte[] toStandardByte(Bitmap bitmap) {
        if (bitmap == null) {
            return Empty.bytes;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, PNG_QUALITY, stream);
        if (stream.toByteArray().length / 1000 > BaseGlobal.ImageFileSizeMax) {  //  500kByte
            stream.reset();
            bitmap.compress(CompressFormat.JPEG, 94, stream);
            //   Log.i("DEBUG_JS", String.format("[MyGraphics.toStandardByte] 94 Compress %d", stream.toByteArray().length/1000));
        }
        return stream.toByteArray();
    }

    public static int getScaleFactor(String imagePath, double refSize, ScaleReference reference) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bmOptions);
        double photoWidth = bmOptions.outWidth;
        double photoHeight = bmOptions.outHeight;
        if (MyGraphics.getExifOrientation(imagePath) == 90) {
            photoWidth = bmOptions.outHeight;
            photoHeight = bmOptions.outWidth;
        }
        ;
        int scaleFactor = getScaleFactor(photoWidth, photoHeight, refSize, reference);
        // Log.i("DEBUG_JS", String.format("[MyGraphics.getScaleFactor] original (%d, %d) rotation (%d) target (%d) x 1/%d = target (%d, %d)", bmOptions.outWidth, bmOptions.outHeight, MyGraphics.getExifOrientation(imagePath),(int) refSize, (int) scaleFactor, (int) (photoWidth/scaleFactor), (int) (photoHeight/scaleFactor)));
        return scaleFactor;
    }

    public static int getScaleFactor(Bitmap bitmap, double refSize, ScaleReference reference) {
        return getScaleFactor(bitmap.getWidth(), bitmap.getHeight(), refSize, reference);
        // Log.i("DEBUG_JS", String.format("[MyGraphics.getScaleFactor] original (%d, %d) rotation (%d) target (%d) x 1/%d = target (%d, %d)", bmOptions.outWidth, bmOptions.outHeight, MyGraphics.getExifOrientation(imagePath),(int) refSize, (int) scaleFactor, (int) (photoWidth/scaleFactor), (int) (photoHeight/scaleFactor)));
    }

    public static int getScaleFactor(double photoWidth, double photoHeight, double refSize, ScaleReference reference) {
        double scaleFactor = 1;

        if (photoWidth > photoHeight) // if landscape
            refSize = refSize * photoWidth / photoHeight; // refSize : 720 -> 960

        if (refSize > 0) {
            switch (reference) {
                case width:
                    scaleFactor = photoWidth / refSize;
                    break;
                case height:
                    scaleFactor = photoHeight / refSize;
                    break;
                default:
                    scaleFactor = Math.min(photoWidth / refSize, photoHeight / refSize);
                    break;
            }
        }

        if (scaleFactor >= 8) {
            scaleFactor = 8;
        } else if (scaleFactor >= 4) {
            scaleFactor = 4;
        } else if (scaleFactor >= 2) {
            scaleFactor = 2;
        } else {
            scaleFactor = 1;
        }
        return (int) scaleFactor;

        // ex) ref = 540, width = 956, ... scaleFactor = 1.77... => 1
        // ex) ref = 360, width = 720, ... scaleFactor = 2 => 2
    }

    public static int getStandardScaleFactor(double photoWidth, double photoHeight) {
        double scaleFactor = 1.0;
        int refSize = getStandardImageWidth(photoWidth, photoHeight);

        if (refSize > 0)
            scaleFactor = photoWidth / refSize;

        if (scaleFactor >= 8) {
            scaleFactor = 8;
        } else if (scaleFactor >= 4) {
            scaleFactor = 4;
        } else if (scaleFactor >= 2) {
            scaleFactor = 2;
        } else {
            scaleFactor = 1;
        }
        return (int) scaleFactor;

        // ex) ref = 540, width = 956, ... scaleFactor = 1.77... => 1
        // ex) ref = 360, width = 720, ... scaleFactor = 2 => 2
    }

    public static Bitmap toBitmapScaleFactor(String imagePath, int scaleFactor) {
        File file = new File(imagePath);
        if (file.exists() == false) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.toBitmap] %s Not exists", imagePath));
            return Empty.bitmap;
        }

        if (file.length() <= 0) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.toBitmap] %s invalid file (size = 0)", imagePath));
            return Empty.bitmap;
        }

        try {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inPreferredConfig = Config.RGB_565;
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = (int) scaleFactor;
            bmOptions.inPurgeable = true;
            return BitmapFactory.decodeFile(imagePath, bmOptions);
        } catch (OutOfMemoryError e) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.toBitmap] %s", e.getLocalizedMessage()));
            return Empty.bitmap;
        }
    }

    public static Bitmap toBitmap(InputStream inputStream) {
        return BitmapFactory.decodeStream(inputStream);
    }

    public static Bitmap toBitmapWidth(String imagePath, int refWidth) {
        int degree = getExifOrientation(imagePath);
        // 삼성폰 때문에
        if (degree == 90) {
            int scaleFactor = getScaleFactor(imagePath, refWidth, ScaleReference.height);
            Bitmap bitmap = toBitmapScaleFactor(imagePath, scaleFactor);
            return rotateBitmap(bitmap, 90);
        } else {
            int scaleFactor = getScaleFactor(imagePath, refWidth, ScaleReference.width);
            return toBitmapScaleFactor(imagePath, scaleFactor);
        }
    }

    @Deprecated
    private static Bitmap toBitmap(String imagePath, double refSize, ScaleReference reference) {
        int scaleFactor = getScaleFactor(imagePath, refSize, reference);
        return toBitmapScaleFactor(imagePath, scaleFactor);
    }

    public static Bitmap toBitmap(String imagePath) {
        return toBitmapScaleFactor(imagePath, 1);
    }

    public static Bitmap toBitmap(Context context, Uri uri) {
        Bitmap bitmap = Empty.bitmap;
        if (uri == null) return bitmap;
        try {
            bitmap = Images.Media.getBitmap(((Activity) context).getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static boolean copy(String source, String target) {
        try {
            InputStream in = new FileInputStream(source);
            OutputStream out = new FileOutputStream(target);
            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            return true;
        } catch (IOException e) {
            Log.e("DEBUG_JS", String.format("[MyGraphics.copy] %s", e.getLocalizedMessage()));
            return false;
        }
    }

    public static int getHeight(int width, int imageWidth, int imageHeight) {
        return (width * imageHeight) / imageWidth;
    }

    public static int getHeightPx(Context context, int widthMarginDp, Bitmap bitmap) {
        int widthPx = MyDevice.getWidth(context) - MyDevice.toPixel(context, widthMarginDp * 2);
        return getHeight(widthPx, bitmap.getWidth(), bitmap.getHeight());
    }
}
