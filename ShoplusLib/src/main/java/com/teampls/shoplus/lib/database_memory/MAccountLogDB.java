package com.teampls.shoplus.lib.database_memory;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database.AccountLogRecord;
import com.teampls.shoplus.lib.datatypes.AccountsReceivableChangeLogData;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2018-05-12.
 */

public class MAccountLogDB extends BaseMapDB<String, AccountLogRecord> {
    private static MAccountLogDB instance;

    public static MAccountLogDB getInstance(Context context) {
        if (instance == null)
            instance = new MAccountLogDB(context);
        return instance;
    }

    private MAccountLogDB(Context context) {
        super(context, "MAccountLogDB");
    }

    protected MAccountLogDB(Context context, String dbName) {
        super(context, dbName);
    }

    @Override
    protected AccountLogRecord getEmptyRecord() {
        return new AccountLogRecord();
    }

    @Override
    public String getKeyValue(AccountLogRecord record) {
        return record.getKey();
    }

    public void refresh(AccountsReceivableChangeLogData data, DateTime week) {
        DateTime fromDate = week;
        DateTime toDate = week.plusWeeks(1).minusMillis(1);
        List<AccountLogRecord> oldRecords = new ArrayList<>();
        for (AccountLogRecord record : getRecords())
            if (BaseUtils.isBetween(record.getCreatedDateTime(), fromDate, toDate))
                oldRecords.add(record);
        deleteAll(oldRecords);
        for (AccountChangeLogDataProtocol online : data.getOnlineChanges()) {
            AccountLogRecord record = new AccountLogRecord(online, AccountType.OnlineAccount);
            insert(record);
        }
        for (AccountChangeLogDataProtocol entrust : data.getEntrustedChanges()) {
            AccountLogRecord record = new AccountLogRecord(entrust, AccountType.EntrustedAccount);
            insert(record);
        }
    }

    @Override
    public void toLogCat(String callLocation) {
        for (AccountLogRecord record : getRecords())
            record.toLogCat(callLocation);
    }

    public void set(AccountsReceivableChangeLogData data) {
        set(data.getOnlineChanges(), data.getEntrustedChanges());
    }

    public void set(List<AccountChangeLogDataProtocol> onlines, List<AccountChangeLogDataProtocol> entrusts) {
        clear();
        for (AccountChangeLogDataProtocol online : onlines) {
            AccountLogRecord record = new AccountLogRecord(online, AccountType.OnlineAccount);
            insert(record);

        }
        for (AccountChangeLogDataProtocol entrust : entrusts) {
            AccountLogRecord record = new AccountLogRecord(entrust, AccountType.EntrustedAccount);
            insert(record);
        }
    }

    public List<AccountLogRecord> getRecordsOrderByKey() {
        List<AccountLogRecord> results = new ArrayList<>();
        for (AccountLogRecord record : getRecords()) {
            record.sortingKey = MSortingKey.ID; // sort by sortkey (생성순서)
            results.add(record);
        }
        Collections.sort(results); // 이 자체가 내림차순으로 정리되어 있음 -- 단정할 수 없네
        return results;
    }

    // 이전 로그는 업데이트 하고 새 로그를 입력
    public void updateAndInsert(String sourceKey, AccountLogRecord newRecord, boolean isCrossing) {
        if (hasKey(getKeyValue(newRecord))) {
            Log.e("DEBUG_JS", String.format("[AccountLogDB.updateAndInsert] has ALREADY record %s",
                    newRecord.getKey()));
            return;
        }

        // crossing이 되면 source와 new사이의 accountType이 달라지게 된다.
        if (isCrossing) {
            switch (newRecord.accountType) {
                case OnlineAccount:
                    newRecord.accountType = AccountType.EntrustedAccount;
                    break;
                case EntrustedAccount:
                    newRecord.accountType = AccountType.OnlineAccount;
                    break;
            }
        }
        insert(newRecord);

        // DB에 있으면 업데이트 (오래된건 없을 수 있음)
        if (hasKey(sourceKey)) {
            AccountLogRecord sourceDbRecord = getRecordByKey(sourceKey); //, Column.accountType, newRecord.accountType.toString());
            switch (newRecord.operation) {
                case Clearance:
                    sourceDbRecord.cleared = true;
                    break;
                case ClearanceRestoration:
                    sourceDbRecord.cleared = false;
                    break;
                case TransactionCancellation:
                    sourceDbRecord.cancelled = true;
                    break;
                default:
                    break;
            }
            sourceDbRecord.updatedDateTime = DateTime.now();
            sourceDbRecord.isCrossing = isCrossing; // 교차가 됐다는 것을 남김
            update(sourceDbRecord);
        }
    }

    public void update(AccountsReceivableChangeLogData data) {
        for (AccountChangeLogDataProtocol online : data.getOnlineChanges()) {
            AccountLogRecord record = new AccountLogRecord(online, AccountType.OnlineAccount);
            updateOrInsert(record);
        }
        for (AccountChangeLogDataProtocol entrust : data.getEntrustedChanges()) {
            AccountLogRecord record = new AccountLogRecord(entrust, AccountType.EntrustedAccount);
            updateOrInsert(record);
        }
    }

    public List<String> getProviders() {
        Set<String> results = new HashSet<>();
        for (AccountLogRecord record : getRecords())
            results.add(record.provider);
        return new ArrayList(results);
    }

    public void clearByDays(int days) {
        List<AccountLogRecord> oldRecords = new ArrayList<>();
        DateTime deadline = BaseUtils.createDay().minusDays(days);
        for (AccountLogRecord record : getRecords())
            if (record.createdDateTime.isBefore(deadline))
                oldRecords.add(record);
        deleteAll(oldRecords);
    }

}
