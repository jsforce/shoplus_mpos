package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyKeyboard;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.SlipItemDB;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.CustomerPriceDB;
import com.teampls.shoplus.lib.database_global.ImageRecord;
import com.teampls.shoplus.lib.database_memory.MemorySlipItemDB;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;
import com.teampls.shoplus.lib.view_module.MySubContainers;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-05-02.
 */

abstract public class BaseSlipItemAddition extends BaseActivity {
    protected enum Module {name, quantity, unitPrice, slipType}

    protected MySubContainers<Module> subContainers;
    protected EditText etName, etQuantity;
    protected static DbActionType dbActionType = DbActionType.INSERT;
    protected static SlipRecord slipRecord;
    protected static SlipItemRecord slipItemRecord;
    protected static List<SlipItemRecord> slipItemRecords = new ArrayList<>();
    protected static List<ImageRecord> imageRecords = new ArrayList<>();
    private MyKeyboard myKeyboard;
    protected MyRadioGroup<SlipItemType> slipItemTypes, singleExchanges;
    private final int KEY_WORD = 5000;
    private ScrollView svImage;
    protected LinearLayout slipItemTypeHeader, slipItemExchangeContainer,
            singleExchangeContainer, slipTypeContainer;
    protected MyCurrencyEditText etUnitPrice;
    protected CheckBox cbShowSingleExchanges;
    protected SlipItemRecord originalSlipItemRecord;

    protected enum PriceMode {Default, ByCustomer, Retail}

    protected MyRadioGroup<PriceMode> rgPriceMode;
    protected Button btDirectApply, btDelete;

    @Override
    public int getThisView() {
        return R.layout.base_new_slipitem;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        setSlipDB();
        originalSlipItemRecord = slipItemRecord.clone();

        // Global setting
        myActionBar.hide();
        svImage = (ScrollView) findViewById(R.id.newitem_scrollview);
        subContainers = new MySubContainers<>(context, getView());
        initializeSubContainers();
        setSubContainers();

        // 중앙 삭제버튼 설정
        MyView.setColorFilter(findViewById(R.id.module_new_slip_delete), ColorType.SkyBlue);
        switch (slipRecord.generation) {
            case AUTO_GENERATED:
                findViewById(R.id.module_new_slip_delete).setVisibility(View.GONE);
                break;
            case AUTO_GENERATED_CARBON_COPY: // 남이 발행한 것
                findViewById(R.id.module_new_slip_delete).setVisibility(View.GONE);
                break;
            case NONE:
            case MANUAL:
                break;
        }
        if (dbActionType == DbActionType.INSERT)
            findViewById(R.id.module_new_slip_delete).setVisibility(View.GONE);

        // slip item type setting
        slipItemTypeHeader = (LinearLayout) findViewById(R.id.module_new_slip_slipTypeHeader);
        slipItemExchangeContainer = (LinearLayout) findViewById(R.id.module_new_slip_exchangeContainer);
        slipItemExchangeContainer.setVisibility(View.GONE);

        slipItemTypes = new MyRadioGroup<>(context, getView(), 1.0, this);
        slipItemTypes.add(R.id.module_new_slip_sales, SlipItemType.SALES).add(R.id.module_new_slip_return, SlipItemType.RETURN).add(R.id.module_new_slip_deposit, SlipItemType.DEPOSIT).add(R.id.module_new_slip_withdrawal, SlipItemType.WITHDRAWAL)
                .add(R.id.module_new_slip_advance, SlipItemType.ADVANCE).add(R.id.module_new_slip_advance_clear, SlipItemType.ADVANCE_CLEAR).add(R.id.module_new_slip_advance_withdral, SlipItemType.ADVANCE_SUBTRACTION).add(R.id.module_new_slip_advance_outofstock, SlipItemType.ADVANCE_OUT_OF_STOCK)
                .add(R.id.module_new_slip_consign, SlipItemType.CONSIGN).add(R.id.module_new_slip_consign_clear, SlipItemType.CONSIGN_CLEAR).add(R.id.module_new_slip_consign_return, SlipItemType.CONSIGN_RETURN)
                .add(R.id.module_new_slip_sample, SlipItemType.SAMPLE).add(R.id.module_new_slip_sample_clear, SlipItemType.SAMPLE_CLEAR).add(R.id.module_new_slip_sample_return, SlipItemType.SAMPLE_RETURN)
                .add(R.id.module_new_slip_preorder, SlipItemType.PREORDER).add(R.id.module_new_slip_preorder_clear, SlipItemType.PREORDER_CLEAR).add(R.id.module_new_slip_preorder_cancel, SlipItemType.PREORDER_WITHDRAW).add(R.id.module_new_slip_preorder_outofstock, SlipItemType.PREORDER_OUT_OF_STOCK)
                .add(R.id.module_new_slip_exchange, SlipItemType.EXCHANGE_SALES).add(R.id.module_new_slip_exchange_sales_pending, SlipItemType.EXCHANGE_RETURN_PENDING).add(R.id.module_new_slip_exchange_return_pending, SlipItemType.EXCHANGE_SALES_PENDING);
        if (BasePreference.isReturnsDisabled(context))
            slipItemTypes.setVisibility(View.GONE, SlipItemType.RETURN);
        // 사용되지 않는 속성에 대한 On / Off는 상속 클래스에서 결정
        slipTypeContainer = (LinearLayout) findViewById(R.id.module_new_slip_typeContainer);

        // exchange single input
        singleExchanges = new MyRadioGroup<>(context, getView(), 1.0, this);
        singleExchanges.add(R.id.module_new_slip_singleExchange_sales, SlipItemType.EXCHANGE_SALES)
                .add(R.id.module_new_slip_singleExchange_return, SlipItemType.EXCHANGE_RETURN)
                .add(R.id.module_new_slip_singleExchange_sales_pending, SlipItemType.EXCHANGE_SALES_PENDING)
                .add(R.id.module_new_slip_singleExchange_return_pending, SlipItemType.EXCHANGE_RETURN_PENDING)
                .add(R.id.module_new_slip_singleExchange_tax, SlipItemType.VALUE_ADDED_TAX)
                .add(R.id.module_new_slip_singleExchange_expense, SlipItemType.MISC_EXPENSES)
        ;
        singleExchangeContainer = (LinearLayout) findViewById(R.id.module_new_slip_singleExchangeContainer);

        cbShowSingleExchanges = findCheckBoxById(R.id.module_new_slip_show_singleExchangeAndSoOn);
        cbShowSingleExchanges.setChecked(slipItemRecord.slipItemType.isExchangeTypeOrTaxOrExpense());
        setSlipItemTypeModule(slipItemRecord.slipItemType.isExchangeTypeOrTaxOrExpense());

        // name
        etName = findEditTextById(R.id.module_new_slip_edit, slipItemRecord.name);
        setNameButtons((ViewGroup) findViewById(R.id.module_new_slip_keywordContainer1), (ViewGroup) findViewById(R.id.module_new_slip_keywordContainer2), (ViewGroup) findViewById(R.id.module_new_slip_keywordContainer3));

        // quantity
        etQuantity = findEditTextById(R.id.module_new_slip_quantity, (slipItemRecord.quantity == 0) ? "" : Integer.toString(slipItemRecord.quantity));
        etQuantity.setSelection(etQuantity.getText().toString().length());

        // unit price -- 가격
        etUnitPrice = new MyCurrencyEditText(context, getView(), R.id.module_new_slip_unitPrice, R.id.module_new_slip_clearUnitPrice);
        etUnitPrice.setPlusMinusButton(R.id.module_new_slip_unitPrice_plus, R.id.module_new_slip_unitPrice_minus);
        etUnitPrice.setTextViewAndBasicUnitCheckBox(R.id.module_new_slip_basicUnit, R.id.module_new_slip_isOneDigit, userSettingData.isOneDigitBasicUnit());
        etUnitPrice.setOriginalValue(slipItemRecord.unitPrice);

        rgPriceMode = new MyRadioGroup<PriceMode>(context, getView());
        rgPriceMode.add(R.id.module_new_slip_default_price, PriceMode.Default);
        rgPriceMode.add(R.id.module_new_slip_byCustomer_price, PriceMode.ByCustomer);
        rgPriceMode.add(R.id.module_new_slip_retail_price, PriceMode.Retail);
        rgPriceMode.setChecked(PriceMode.Default);
        if (slipItemRecord.itemId > 0) {
            if (userSettingData.getProtocol().isRetailOn() && userDB.getRecord(slipRecord.counterpartPhoneNumber).priceType == ContactBuyerType.RETAIL) {
                rgPriceMode.setChecked(PriceMode.Retail);
                rgPriceMode.setVisibility(View.VISIBLE, PriceMode.Default, PriceMode.Retail);
            } else {
                // 도소매 구분 안하거나 도매라면
                if (BasePreference.priceByBuyer.getValue(context)) {
                    rgPriceMode.setChecked(PriceMode.ByCustomer); // 도매 + 거래처가 사용
                    rgPriceMode.setVisibility(View.VISIBLE, PriceMode.Default, PriceMode.ByCustomer);
                }
            }
        }
        final ItemRecord itemRecord = ItemDB.getInstance(context).getRecordBy(slipItemRecord.itemId); // 수동추가의 경우 itemId = 0
        rgPriceMode.setOnClickListener(true, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int unitPrice = slipItemRecord.unitPrice;
                if (slipItemRecord.itemId > 0) { // 존재하는 상품에 대해서만
                    switch (rgPriceMode.getCheckedItem()) {
                        case Default:
                            unitPrice = itemRecord.unitPrice;
                            break;
                        case ByCustomer:
                            unitPrice = CustomerPriceDB.getInstance(context).getPrice(userSettingData.getUserShopInfo(), slipRecord.counterpartPhoneNumber,
                                    slipItemRecord.itemId, slipItemRecord.unitPrice);
                            break;
                        case Retail:
                            unitPrice = itemRecord.retailPrice == 0 ? unitPrice : itemRecord.retailPrice;
                            break;
                    }
                }
                etUnitPrice.setOriginalValue(unitPrice);
            }
        });
        findTextViewById(R.id.module_new_slip_unitPrice_preview, String.format("기본 : %s", BaseUtils.toCurrencyOnlyStr(itemRecord.unitPrice)));

        // 글자크기 조절
        MyView.setTextViewByDeviceSize(context, getView(), R.id.module_new_slip_name_title, R.id.module_new_slip_quantity_title,
                R.id.module_new_slip_unitprice_title, R.id.module_new_slip_type_title);
        MyView.setImageViewSizeByDeviceSizeScale(context, getView(), 0.5, R.id.module_new_slip_clearName,
                R.id.module_new_slip_clearQuantity, R.id.module_new_slip_clearUnitPrice,
                R.id.module_new_slip_quantity_plus, R.id.module_new_slip_quantity_minus,
                R.id.module_new_slip_unitPrice_plus, R.id.module_new_slip_unitPrice_minus
        );

        myKeyboard = new MyKeyboard(context, findViewById(R.id.newitem_container), etName);
        myKeyboard.show();
        createImageView(imageRecords);

        btDirectApply = findButtonById(R.id.module_new_slip_directApply);
        btDelete = findButtonById(R.id.module_new_slip_delete);

        setOnClick(R.id.module_new_slip_clearName, R.id.module_new_slip_clearQuantity,
                R.id.module_new_slip_quantity_plus, R.id.module_new_slip_quantity_minus, R.id.module_new_slip_show_singleExchangeAndSoOn2
        );

        refreshSubContainers();
    }

    private void setSlipItemTypeModule(boolean isExchangeTypeOrTax) {
        if (isExchangeTypeOrTax) {
            slipItemTypeHeader.setVisibility(View.VISIBLE); // 헤더는 항상 보이게
            slipTypeContainer.setVisibility(View.GONE);
            singleExchangeContainer.setVisibility(View.VISIBLE);
            if (slipItemRecord.slipItemType.isExchangeTypeOrTaxOrExpense() == false)
                slipItemRecord.slipItemType = SlipItemType.EXCHANGE_SALES;
            singleExchanges.setChecked(slipItemRecord.slipItemType);
        } else {
            // slipItemTypeHeader.setVisibility --> refreshSubContainers에 맡기자
            slipTypeContainer.setVisibility(View.VISIBLE);
            singleExchangeContainer.setVisibility(View.GONE);
            if (slipItemRecord.slipItemType == SlipItemType.NONE)
                slipItemRecord.slipItemType = SlipItemType.SALES;
            slipItemTypes.setChecked(slipItemRecord.slipItemType);
        }
    }

    private void setSubContainers() {
        subContainers.setPrevClick(R.id.module_new_slip_prev, new MyOnClick<Boolean>() {
            @Override
            public void onMyClick(View view, Boolean isFirst) {
                btDirectApply.setVisibility(View.VISIBLE);
                if (isFirst) {
                    onFinish();
                } else {
                    refreshSubContainers();
                }
            }
        });

        subContainers.setNextClick(R.id.module_new_slip_next, new MyOnClick<Boolean>() {
            @Override
            public void onMyClick(View view, Boolean isLast) {
                btDelete.setVisibility(View.GONE);
                if (isLast) {
                    slipItemRecord = updateRecordFromUI();
                    onApplyClick(dbActionType, slipItemRecord);
                } else {
                    btDirectApply.setVisibility(subContainers.isLastContainer() ? View.GONE : View.VISIBLE);
                    refreshSubContainers();
                }
            }
        });
    }

    abstract protected void setSlipDB();

    abstract protected void initializeSubContainers();

    private void refreshSubContainers() {
        switch (subContainers.getCurrentModule()) {
            case name:
                etName.requestFocus();
                myKeyboard.setFocusView(etName);
                break;
            case quantity:
                etQuantity.requestFocus();
                myKeyboard.setFocusView(etQuantity);
                break;
            case unitPrice:
                etUnitPrice.getEditText().requestFocus();
                myKeyboard.setFocusView(etUnitPrice.getEditText());
                //       setPricingContainer();
                break;
            case slipType:
                if (isExchangeable(slipItemRecords, BaseUtils.toInt(etQuantity.getText().toString()))) { // 불량 (같은것 개수2), 사이즈, 컬러 교환 (다른것 개수1)
                    if (cbShowSingleExchanges.isChecked() == false) { // 이미 교환 화면이 아닐때
                        slipItemTypeHeader.setVisibility(View.GONE);
                        slipItemExchangeContainer.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }
    }

    public boolean isExchangeable(List<SlipItemRecord> slipItemRecords, int quantity) {
        if (slipItemRecords.size() == 1 && quantity == 2) { // 같은 상품 2개, 불량 교환일때, 수동입력에서도 가능
            return true;
        } else if (slipItemRecords.size() == 2) { // 컬러, 사이즈 교환
            SlipItemRecord record1 = slipItemRecords.get(0);
            SlipItemRecord record2 = slipItemRecords.get(1);
            if (record1.itemId == record2.itemId && record1.quantity == record2.quantity) { // 같은 아이템에, 같은 개수만
                if (((record1.color == record2.color) && (record1.size == record2.size)) == false) { // 옵션은 다를때
                    // 둘다 sales일때만?
                    return true;
                }
            }
        }
        return false;
    }

    private void createImageView(List<ImageRecord> records) {
        LinearLayout container = (LinearLayout) findViewById(R.id.newitem_imageContainer);
        int imageViewWidth = MyDevice.getWidth(context) - MyDevice.toPixel(context, (5 + 5) * 2); // layout padding = 5 + 10
        for (ImageRecord imageRecord : records) {
            Bitmap bitmap = imageRecord.getBitmap();
            ImageView imageView = MyView.createImageView(context, imageRecord.getBitmap());
            int imageViewHeight = (imageViewWidth * bitmap.getHeight()) / bitmap.getWidth();
            container.addView(imageView, MyView.getParams(context, MyView.PARENT, MyDevice.toDP(context, imageViewHeight)));
        }
    }

    private SlipItemRecord updateRecordFromUI() {
        slipItemRecord.name = etName.getText().toString().trim();
        slipItemRecord.quantity = (etQuantity.getText().toString().isEmpty() ? 1 : Integer.valueOf(etQuantity.getText().toString()));
        slipItemRecord.unitPrice = etUnitPrice.getValue();
        if (cbShowSingleExchanges.isChecked()) {
            if (singleExchanges.currentRadioButton.getTag() != null)
                slipItemRecord.slipItemType = (SlipItemType) singleExchanges.currentRadioButton.getTag();
        } else {
            if (slipItemTypes.currentRadioButton.getTag() != null)
                slipItemRecord.slipItemType = (SlipItemType) slipItemTypes.currentRadioButton.getTag();
        }
        slipItemRecord.salesDateTime = slipRecord.salesDateTime;
        return slipItemRecord;
    }

    protected void onFinish() {
        myKeyboard.hide();
        keyValueDB.put("SlipItemAddition.prevSlipKey", slipItemRecord.slipKey);
        keyValueDB.put("SlipItemAddition.scrollPos", svImage.getScrollY());
        doFinishForResult();
    }

    abstract public void onDeleteClick(SlipItemRecord slipItemRecord);


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.module_new_slip_delete) {
            new MyAlertDialog(context, "항목 삭제", String.format("이 항목을 (%s:%s:%s) 삭제 하시겠습니까?",
                    slipItemRecord.name, slipItemRecord.getColorName(), slipItemRecord.size.uiName)) {
                @Override
                public void yes() {
                    onDeleteClick(slipItemRecord);
                }
            };

        } else if (view.getId() == R.id.module_new_slip_directApply) {
            slipItemRecord = updateRecordFromUI();
            onApplyClick(dbActionType, slipItemRecord);

        } else if (view.getId() == R.id.module_new_slip_clearName) {
            etName.setText("");

        } else if (view.getId() == R.id.module_new_slip_clearQuantity) {
            etQuantity.setText("");

        } else if (view.getId() == R.id.module_new_slip_quantity_plus) {
            int quantity = BaseUtils.toInt(etQuantity.getText().toString());
            if (quantity == 0)
                quantity = 1;
            quantity = quantity + 1;
            etQuantity.setText(String.format("%d", quantity));
            etQuantity.setSelection(etQuantity.getText().toString().length());

        } else if (view.getId() == R.id.module_new_slip_quantity_minus) {
            int quantity = BaseUtils.toInt(etQuantity.getText().toString());
            quantity = quantity - 1;
            if (quantity < 1)
                quantity = 1;
            etQuantity.setText(String.format("%d", quantity));
            etQuantity.setSelection(etQuantity.getText().toString().length());

        } else if (view.getId() == R.id.module_new_slip_show_singleExchangeAndSoOn) {
            // 교환을 클릭하고 판매로 넘어오는 경우 상태도 같이 변경해주지 않으면 이후 로직이 꼬인다
            if (cbShowSingleExchanges.isChecked()) {
                slipItemRecord.slipItemType = SlipItemType.EXCHANGE_SALES;
            } else {
                slipItemRecord.slipItemType = SlipItemType.SALES;
            }
            setSlipItemTypeModule(cbShowSingleExchanges.isChecked());
            refreshSubContainers();

        } else if (view.getId() == R.id.module_new_slip_show_singleExchangeAndSoOn2) {
            cbShowSingleExchanges.setChecked(true);
            slipItemRecord.slipItemType = SlipItemType.EXCHANGE_SALES;
            setSlipItemTypeModule(cbShowSingleExchanges.isChecked());
            slipItemTypeHeader.setVisibility(View.VISIBLE); //  헤더 표시

        } else if (view.getId() == -1) {
            String name = etName.getText().toString().trim();
            String selectedString = ((TextView) view).getText().toString().trim();
            switch ((Integer) view.getTag()) {
                case KEY_WORD:
                    etName.setText(String.format("%s%s%s", name, name.isEmpty() ? "" : " ", selectedString));
                    break;
            }
            etName.setSelection(etName.getText().length());
        }
    }

    abstract public void onApplyClick(DbActionType actionType, SlipItemRecord slipItemRecord);

    private void setNameButtons(ViewGroup... layouts) {
        for (ViewGroup layout : layouts) {
            for (int i = 0; i < layout.getChildCount(); i++) {
                View view = layout.getChildAt(i);
                if (view instanceof Button) {
                    view.setOnClickListener(this);
                    view.setTag(KEY_WORD);
                    MyView.setTextViewByDeviceSize(context, (TextView) view);
                }
            }
        }
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    // 반품 금액에 1/3룰 적용
    protected void splitSlipItemRecord(final DbActionType actionType, final SlipItemDB items, final Boolean doToastResult, final MyOnTask onTask) {
        final List<SlipItemRecord> itemsExceptMe = items.getRefreshedRecordsBy(slipRecord.getSlipKey().toSID()); // 기존 항목들
        switch (actionType) {
            case INSERT:
                break;
            case UPDATE:
                itemsExceptMe.remove(slipItemRecord.serial);  // 합산시 자기 자신을 빼야...
                break;
        }
        final SlipSummaryRecord slipSummaryRecord = new SlipSummaryRecord(itemsExceptMe);
        final int maxAmount = slipItemRecord.unitPrice * slipItemRecord.quantity;
        new ReturnValueControlDialog(context, slipItemRecord.slipItemType, slipSummaryRecord.amount, maxAmount) {
            @Override
            public void onApplyClick(int substractionValue, int depositValue) {
                if (substractionValue == maxAmount) { // 변동이 없는 경우, 전액 반품 혹은 전액 빼기
                    switch (actionType) {
                        case INSERT:
                            items.insert(slipItemRecord);
                            break;
                        case UPDATE:
                            items.update(slipItemRecord.id, slipItemRecord);
                            break;
                    }

                } else if (depositValue == maxAmount) { // 전액 매입으로 (이 경우는 매입을 쓰겠지만...)
                    slipItemRecord.slipItemType = SlipItemType.DEPOSIT;
                    switch (actionType) {
                        case INSERT:
                            items.insert(slipItemRecord);
                            break;
                        case UPDATE:
                            items.update(slipItemRecord.id, slipItemRecord);
                            break;
                    }

                } else { // 일부 반품, 일부 매입
                    slipItemRecord.unitPrice = substractionValue;
                    slipItemRecord.quantity = 1;

                    SlipItemRecord depositRecord = new SlipItemRecord();
                    depositRecord.slipKey = slipItemRecord.slipKey;
                    depositRecord.salesDateTime = DateTime.now();
                    depositRecord.name = String.format("잔매입%s", slipItemRecord.name.isEmpty() ? "" : String.format(" (%s)", slipItemRecord.name));
                    depositRecord.quantity = 1;
                    depositRecord.unitPrice = depositValue;
                    depositRecord.slipItemType = SlipItemType.DEPOSIT; // 잔매입으로 수정

                    switch (actionType) {
                        case INSERT:
                            items.insert(slipItemRecord); // 차감액 수정
                            depositRecord.serial = slipItemRecord.serial + 1; // 새로 추가되는 것과 나란히 추가 (2개가 추가됨) ** 주의해야 함 **
                            items.insert(depositRecord);
                            if (doToastResult)
                                MyUI.toastSHORT(context, String.format("[%s, %s, %s] 추가", slipItemRecord.name, slipItemRecord.getColorName()
                                        , slipItemRecord.size.uiName));
                            break;
                        case UPDATE:
                            items.update(slipItemRecord.id, slipItemRecord);
                            depositRecord.serial = items.getNextSerial(slipItemRecord.slipKey); // 현재 것 다음에 추가
                            items.insert(depositRecord);
                            if (doToastResult)
                                MyUI.toastSHORT(context, String.format("[%s, %s, %s] 수정", slipItemRecord.name,
                                        slipItemRecord.getColorName(), slipItemRecord.size.uiName));
                            break;
                    }
                }
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    // 쓸일이 없기 때문에 검증되지 않았다
    protected void splitSlipItemRecord(final DbActionType actionType, final MemorySlipItemDB items, final Boolean doToastResult, final MyOnTask onTask) {
        final List<SlipItemRecord> itemsExceptMe = items.getRecordsBy(slipRecord.getSlipKey().toSID()); // 기존 항목들
        switch (actionType) {
            case INSERT:
                break;
            case UPDATE:
                boolean contained = itemsExceptMe.remove(slipItemRecord);  // 합산시 자기 자신을 빼야...
                if (contained == false)
                    slipItemRecord.toLogCat("e.Not found");
                break;
        }
        final SlipSummaryRecord slipSummaryRecord = new SlipSummaryRecord(itemsExceptMe);
        final int maxAmount = slipItemRecord.unitPrice * slipItemRecord.quantity;
        new ReturnValueControlDialog(context, slipItemRecord.slipItemType, slipSummaryRecord.amount, maxAmount) {
            @Override
            public void onApplyClick(int substractionValue, int depositValue) {
                if (substractionValue == maxAmount) { // 변동이 없는 경우, 전액 반품 혹은 전액 빼기
                    switch (actionType) {
                        case INSERT:
                            items.insert(slipItemRecord);
                            break;
                        case UPDATE:
                            items.update(slipItemRecord);
                            break;
                    }

                } else if (depositValue == maxAmount) { // 전액 매입으로 (이 경우는 매입을 쓰겠지만...)
                    slipItemRecord.slipItemType = SlipItemType.DEPOSIT;
                    switch (actionType) {
                        case INSERT:
                            items.insert(slipItemRecord);
                            break;
                        case UPDATE:
                            items.update(slipItemRecord);
                            break;
                    }

                } else { // 일부 반품, 일부 매입
                    slipItemRecord.unitPrice = substractionValue;
                    slipItemRecord.quantity = 1;

                    SlipItemRecord depositRecord = new SlipItemRecord();
                    depositRecord.slipKey = slipItemRecord.slipKey;
                    depositRecord.salesDateTime = DateTime.now();
                    depositRecord.name = String.format("잔매입%s", slipItemRecord.name.isEmpty() ? "" : String.format(" (%s)", slipItemRecord.name));
                    depositRecord.quantity = 1;
                    depositRecord.unitPrice = depositValue;
                    depositRecord.slipItemType = SlipItemType.DEPOSIT; // 잔매입으로 수정

                    switch (actionType) {
                        case INSERT:
                            items.insert(slipItemRecord); // 차감액 수정
                            depositRecord.serial = slipItemRecord.serial + 1; // 새로 추가되는 것과 나란히 추가 (2개가 추가됨) ** 주의해야 함 **
                            items.insert(depositRecord);
                            if (doToastResult)
                                MyUI.toastSHORT(context, String.format("[%s, %s, %s] 추가", slipItemRecord.name, slipItemRecord.getColorName()
                                        , slipItemRecord.size.uiName));
                            break;
                        case UPDATE:
                            items.update(slipItemRecord.id, slipItemRecord);
                            depositRecord.serial = items.getNextSerial(slipItemRecord.slipKey); // 현재 것 다음에 추가
                            items.insert(depositRecord);
                            if (doToastResult)
                                MyUI.toastSHORT(context, String.format("[%s, %s, %s] 수정", slipItemRecord.name,
                                        slipItemRecord.getColorName(), slipItemRecord.size.uiName));
                            break;
                    }
                }
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }


    public abstract class ReturnValueControlDialog extends BaseDialog {
        private MyCurrencyEditText etSubstraction;
        private TextView tvRemains;
        private int subtractionMax = 0;

        public ReturnValueControlDialog(Context context, SlipItemType slipItemType, int sum, final int subtractionMax) {
            super(context);
            setDialogWidth(0.9, 0.7);
            this.subtractionMax = subtractionMax;
            tvRemains = findTextViewById(R.id.dialog_slip_substraction_remains);
            etSubstraction = new MyCurrencyEditText(context, getView(), R.id.dialog_slip_substraction_adjustment,
                    R.id.dialog_slip_substraction_clear_value);
            etSubstraction.setTextViewAndBasicUnitCheckBox(R.id.dialog_slip_substraction_basicUnit,
                    R.id.dialog_slip_substraction_isOneDigit, false);
            etSubstraction.setOriginalValue(Math.min(Math.round(sum / 3), subtractionMax));
            etSubstraction.setOnValueChanged(new MyOnClick<Integer>() {
                @Override
                public void onMyClick(View view, Integer value) {
                    tvRemains.setText(BaseUtils.toCurrencyOnlyStr(subtractionMax - value));
                }
            });
            tvRemains.setText(BaseUtils.toCurrencyOnlyStr(subtractionMax - etSubstraction.getValue()));

            findTextViewById(R.id.dialog_slip_substraction_salesAmount, BaseUtils.toCurrencyOnlyStr(sum)); // 총 결제액
            findTextViewById(R.id.dialog_slip_substraction_target, BaseUtils.toCurrencyOnlyStr(subtractionMax)); // 최대 차감액

            switch (slipItemType) {
                default:
                    break;
                case RETURN:
                    findTextViewById(R.id.dialog_slip_substraction_title, "반품금액 조정");
                    findTextViewById(R.id.dialog_slip_substraction_target_title, "반품요청 :");
                    findTextViewById(R.id.dialog_slip_substraction_adjustment_title, "반품금액 :");
                    break;
                case ADVANCE_SUBTRACTION: // 전액 환불 돼야 함
                case WITHDRAWAL:
                    findTextViewById(R.id.dialog_slip_substraction_title, "빼기금액 조정");
                    findTextViewById(R.id.dialog_slip_substraction_target_title, "빼기요청 :");
                    findTextViewById(R.id.dialog_slip_substraction_adjustment_title, "빼기금액 :");
                    findTextViewById(R.id.dialog_slip_substraction_all, "전액빼기");
                    break;
            }

            setOnClick(R.id.dialog_slip_substraction_cancel,
                    R.id.dialog_slip_substraction_apply,
                    R.id.dialog_slip_substraction_all);
            show();
            MyDevice.showKeyboard(context, etSubstraction.getEditText());

            MyView.setTextViewByDeviceSize(context, getView(),
                    R.id.dialog_slip_substraction_salesAmount_title,
                    R.id.dialog_slip_substraction_remains_title);
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_slip_substraction;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_slip_substraction_cancel) {
                MyDevice.hideKeyboard(context, etSubstraction.getEditText());
                dismiss();

            } else if (view.getId() == R.id.dialog_slip_substraction_apply) {
                if (subtractionMax < etSubstraction.getValue()) {
                    MyUI.toastSHORT(context, String.format("조정 금액은 %s 이하만 가능합니다", BaseUtils.toCurrencyStr(subtractionMax)));
                    return;
                }
                onApplyClick(etSubstraction.getValue(), subtractionMax - etSubstraction.getValue());
                MyDevice.hideKeyboard(context, etSubstraction.getEditText());
                dismiss();

            } else if (view.getId() == R.id.dialog_slip_substraction_all) {
                etSubstraction.setOriginalValue(subtractionMax);
                onApplyClick(subtractionMax, 0);
                MyDevice.hideKeyboard(context, etSubstraction.getEditText());
                dismiss();
            }

        }

        public abstract void onApplyClick(int substractionValue, int depositValue);
    }

    abstract public class ExchangeDialog extends BaseDialog {
        private SlipItemRecord record1, record2;
        private RadioButton rbReturn1, rbReturn2, rbSales1, rbSales2;

        public ExchangeDialog(Context context, SlipItemRecord record1, SlipItemRecord record2) {
            super(context);
            setDialogSize(0.9, -1);
            setCancelable(false);
            this.record1 = record1;
            this.record2 = record2;
            String option1 = record1.toOptionString(" : ", false);
            String option2 = record2.toOptionString(" : ", false);
            rbReturn1 = (RadioButton) findViewById(R.id.dialog_exchange_slipitems_return1);
            rbReturn2 = (RadioButton) findViewById(R.id.dialog_exchange_slipitems_return2);
            rbReturn1.setText(option1);
            rbReturn2.setText(option2);

            rbSales1 = (RadioButton) findViewById(R.id.dialog_exchange_slipitems_sales1);
            rbSales2 = (RadioButton) findViewById(R.id.dialog_exchange_slipitems_sales2);
            rbSales1.setText(option1);
            rbSales2.setText(option2);

            setOnClick(R.id.dialog_exchange_slipitems_return1, R.id.dialog_exchange_slipitems_return2,
                    R.id.dialog_exchange_slipitems_cancel, R.id.dialog_exchange_slipitems_apply);
            show();
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_exchange_slipitems;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_exchange_slipitems_return1) {
                rbSales2.setChecked(true);
            } else if (view.getId() == R.id.dialog_exchange_slipitems_return2) {
                rbSales1.setChecked(true);
            } else if (view.getId() == R.id.dialog_exchange_slipitems_cancel) {
                dismiss();
            } else if (view.getId() == R.id.dialog_exchange_slipitems_apply) {
                int tempDbId = 0;
                switch (record1.slipItemType) {
                    case EXCHANGE_SALES: // 교환
                        if (rbReturn1.isChecked()) {
                            record1.slipItemType = SlipItemType.EXCHANGE_RETURN;
                            record2.slipItemType = SlipItemType.EXCHANGE_SALES;
                        } else if (rbReturn2.isChecked()) {
                            record1.slipItemType = SlipItemType.EXCHANGE_SALES;
                            record2.slipItemType = SlipItemType.EXCHANGE_RETURN;
                            tempDbId = record1.id;
                            record1.id = record2.id;
                            record2.id = tempDbId;
                        }
                        break;
                    case EXCHANGE_RETURN_PENDING: // 선교환
                        if (rbReturn1.isChecked()) {
                            record1.slipItemType = SlipItemType.EXCHANGE_RETURN_PENDING;
                            record2.slipItemType = SlipItemType.EXCHANGE_SALES;
                        } else if (rbReturn2.isChecked()) {
                            record1.slipItemType = SlipItemType.EXCHANGE_SALES;
                            record2.slipItemType = SlipItemType.EXCHANGE_RETURN_PENDING;
                            tempDbId = record1.id;
                            record1.id = record2.id;
                            record2.id = tempDbId;
                        }
                        break;
                    case EXCHANGE_SALES_PENDING: // 선반품
                        if (rbReturn1.isChecked()) {
                            record1.slipItemType = SlipItemType.EXCHANGE_RETURN;
                            record2.slipItemType = SlipItemType.EXCHANGE_SALES_PENDING;
                        } else if (rbReturn2.isChecked()) {
                            record1.slipItemType = SlipItemType.EXCHANGE_SALES_PENDING;
                            record2.slipItemType = SlipItemType.EXCHANGE_RETURN;
                            tempDbId = record1.id;
                            record1.id = record2.id;
                            record2.id = tempDbId;
                        }
                        break;
                }
                onApplyClick(record1, record2);
                dismiss();
            }
        }

        abstract public void onApplyClick(SlipItemRecord record1, SlipItemRecord record2);
    }
}
