package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.datatypes.TwoStringRecord;


/**
 * Created by Medivh on 2017-12-20.
 */

public class MyTwoStringAdapter extends BaseDBAdapter<TwoStringRecord> {

    public MyTwoStringAdapter(Context context) {
        super(context);
    }

    @Override
    public void generate() {

    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new BaseViewHolder() {
            private TextView textView1, textView2;

            @Override
            public int getThisView() {
                return R.layout.row_two_string;
            }

            @Override
            public void init(View view) {
                textView1 = findTextViewById(context, view, R.id.row_two_string1);
                textView2 = findTextViewById(context, view, R.id.row_two_string2);
            }

            @Override
            public void update(int position) {
                TwoStringRecord record = records.get(position);
                textView1.setText(record.string1);
                textView2.setText(record.string2);
            }
        };
    }
}