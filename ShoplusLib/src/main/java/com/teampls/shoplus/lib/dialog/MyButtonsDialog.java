package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.widget.CompoundButtonCompat;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.database_map.KeyValueBoolean;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-07-22.
 */

public class MyButtonsDialog extends BaseDialog {
    private LinearLayout buttonContainer;
    protected TextView tvTitle, tvMessage, tvTitleButton;
    private int marginTopDp = 5;
    protected Button btClose;
    private MyOnTask onCloseClickTask;
    protected EditText etEditText;

    public MyButtonsDialog(Context context, String title, String message) {
        super(context);
        buttonContainer = (LinearLayout) findViewById(R.id.dialog_buttons_buttons_container);
        tvTitle = findTextViewById(R.id.dialog_buttons_title, title);
        tvMessage = findTextViewById(R.id.dialog_buttons_message, message);
        tvTitleButton = findTextViewById(R.id.dialog_buttons_title_button, "");
        etEditText = findViewById(R.id.dialog_buttons_edittext);
        tvTitleButton.setVisibility(View.GONE);
        btClose = findViewById(R.id.dialog_buttons_close);
        if (TextUtils.isEmpty(title))
            tvTitle.setVisibility(View.GONE);
        setMessage(message);
        setOnClick(R.id.dialog_buttons_close, R.id.dialog_buttons_title_close);
        setDialogWidth(0.9, 0.7); // 기본크기
        if (MyDevice.isTablet(context)) {
            MyView.setPaddingByDeviceSizeScale(context, findViewById(R.id.dialog_buttons_container));
        }
    }

    public void setMessage(String message) {
        if (TextUtils.isEmpty(message))
            tvMessage.setVisibility(View.GONE);
        else {
            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setText(message);
        }
    }

    public void setTopMargin(int marginDp) {
        this.marginTopDp = marginDp;
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) btClose.getLayoutParams();
        params.setMargins(0, MyDevice.toPixel(context, marginTopDp), 0, 0);
    }

    public void showEditText() {
        etEditText.setVisibility(View.VISIBLE);
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_buttons;
    }

    public void addText(String text) {
        TextView textView = MyView.createTextView(context, text, 16, 1, ColorType.Black.colorInt);
        MyView.setTextViewByDeviceSize(context, textView);
        buttonContainer.addView(textView);
    }

    public void addLine(int topMarginDp) {
        View view = MyView.createLine(context, 1);
        view = MyView.setMargin(context, view, 5, 5 + topMarginDp, 5, 5);
        buttonContainer.addView(view);
    }

    public <T> List<RadioButton> addRadioButtons(List<Pair<String, T>> items, T selected, final MyOnTask<T> onClick) {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                T tag = (T) v.getTag();
                if (onClick != null)
                    onClick.onTaskDone(tag);
            }
        };

        List<RadioButton> radioButtons = new ArrayList<>();
        RadioGroup radioGroup = MyView.createRadioGroup(context, MyView.PARENT, MyView.CONTENT, MyView.VERTICAL, MyView.LEFT);
        int id = 0;
        for (Pair<String, T> pair : items) {
            RadioButton radioButton = MyView.createRadioButton(context, pair.first, 18);
            radioButton.setTag(pair.second);
            radioButton.setId(id);
            if (selected != null)
                radioButton.setChecked(pair.second == selected);
            radioButton.setOnClickListener(onClickListener);
            MyView.setTextViewByDeviceSize(context, radioButton);
            radioGroup.addView(radioButton);
            radioButtons.add(radioButton);
            id++;
        }
        buttonContainer.addView(radioGroup);
        return radioButtons;
    }

    public <T> List<RadioButton> addRadioButtons(String text1, T tag1, String text2, T tag2, T selected, final MyOnTask<T> onClick) {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                T tag = (T) v.getTag();
                if (onClick != null)
                    onClick.onTaskDone(tag);
            }
        };

        List<RadioButton> radioButtons = new ArrayList<>();
        RadioGroup radioGroup = MyView.createRadioGroup(context, MyView.PARENT, MyView.CONTENT, MyView.HORIZONTAL, MyView.LEFT);
        RadioButton radioButton1 = MyView.createRadioButton(context, text1, 18);
        radioButton1.setTag(tag1);
        radioButton1.setId(0);
        radioButton1.setChecked(tag1 == selected);
        radioButton1.setOnClickListener(onClickListener);
        MyView.setTextViewByDeviceSize(context, radioButton1);
        radioGroup.addView(radioButton1);

        RadioButton radioButton2 = MyView.createRadioButton(context, text2, 18);
        radioButton2.setTag(tag2);
        radioButton2.setId(R.id.buttonId);
        radioButton2.setChecked(tag2 == selected);
        radioButton2.setOnClickListener(onClickListener);
        radioGroup.addView(radioButton2);
        MyView.setTextViewByDeviceSize(context, radioButton2);
        buttonContainer.addView(radioGroup);

        radioButtons.add(radioButton1);
        radioButtons.add(radioButton2);
        return radioButtons;
    }

    public void addButton(String text, View.OnClickListener onClick) {
        addButton(text, true, onClick);
    }

    public void addButton(String text, boolean enabled, View.OnClickListener onClick) {
        LinearLayout.LayoutParams params = MyView.getParams(context, MyView.PARENT, MyView.CONTENT);
        params.setMargins(0, MyDevice.toPixel(context, marginTopDp), 0, 0);
        Button button = new Button(context);
        button.setText(text);
        button.setEnabled(enabled);
        button.setLayoutParams(params);
        button.setOnClickListener(onClick);
        MyView.setTextViewByDeviceSize(context, button);
        buttonContainer.addView(button);
    }

    public CheckBox addCheckBox(String text, final MyOnTask<Boolean> onClick) {
        return addCheckBox(text, true, "", false, onClick);
    }

    public CheckBox addCheckBox(String text, boolean isChecked, final MyOnTask<Boolean> onClick) {
        return addCheckBox(text, true, "", isChecked, onClick);
    }

    public CheckBox addCheckBox(String text, KeyValueBoolean keyValueBoolean, final MyOnTask<Boolean> onClick) {
        return addCheckBox(text, true, keyValueBoolean.key, keyValueBoolean.defaultValue, onClick);
    }

    public CheckBox addCheckBox(String text, final String key, final MyOnTask<Boolean> onClick) {
        return addCheckBox(text, true, key, false, onClick);
    }

    public CheckBox addCheckBox(String text, final String key, boolean defaultValue, final MyOnTask<Boolean> onClick) {
        return addCheckBox(text, true, key, defaultValue, onClick);
    }

    public CheckBox addCheckBox(String text, boolean enabled, final String key, boolean defaultValue, final MyOnTask<Boolean> onClick) {
        LinearLayout.LayoutParams params = MyView.getParams(context, MyView.PARENT, MyView.CONTENT);
        params.setMargins(0, MyDevice.toPixel(context, marginTopDp), 0, 0);
        final CheckBox checkBox = new CheckBox(context);
        checkBox.setText(text);
        checkBox.setTextSize(16);
        checkBox.setEnabled(enabled);
        checkBox.setLayoutParams(params);
        if (MyDevice.getAndroidVersion() <= 21)
            CompoundButtonCompat.setButtonTintList(checkBox, ColorStateList.valueOf(ColorType.CheckBoxColor.colorInt));
        checkBox.setChecked(KeyValueDB.getInstance(context).getBool(key, defaultValue));
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyValueDB.getInstance(context).put(key, checkBox.isChecked());
                if (onClick != null)
                    onClick.onTaskDone(checkBox.isChecked());
            }
        });
        MyView.setTextViewByDeviceSize(context, checkBox);
        buttonContainer.addView(checkBox);
        return checkBox;
    }

    public void addTitleButton(String text, View.OnClickListener onClick) {
        tvTitleButton.setVisibility(View.VISIBLE);
        tvTitleButton.setText(text);
        tvTitleButton.setOnClickListener(onClick);
        MyView.setTextViewByDeviceSize(context, tvTitleButton);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_buttons_close || view.getId() == R.id.dialog_buttons_title_close) {
            onCloseClick();
            dismiss();
        }
    }

    public void onCloseClick() {
        if (onCloseClickTask != null)
            onCloseClickTask.onTaskDone("");
    }

    public void setOnCloseClick(MyOnTask onTask) {
        this.onCloseClickTask = onTask;
    }

    public void doDismissMyButtonsDialog() {
        dismiss();
    }

}
