package com.teampls.shoplus.lib.event;

/**
 * Created by Medivh on 2016-11-06.
 */
public interface MyOnAsync<T> {
    void onSuccess(T result);
    void onError(Exception e);
}
