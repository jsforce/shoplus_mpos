package com.teampls.shoplus.lib;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.ProjectAdminServiceProtocol;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectAdminService;

/**
 * @author lucidite
 */
public class ProjectAdminServiceManager {
    public static ProjectAdminServiceProtocol defaultService(Context context) {
        return new ProjectAdminService();
    }
}
