package com.teampls.shoplus.lib.awsservice.s3;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.s3.datamodel.ImageDataDownloadData;
import com.teampls.shoplus.lib.protocol.ImageDataProtocol;
import com.teampls.shoplus.lib.awsservice.s3.S3Helper;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lucidite
 */
public class ImageDownloader {
    private AWSCredentials credential;
    private String bucketName;

    public ImageDownloader(AWSCredentials credential, String bucketName) {
        this.credential = credential;
        this.bucketName = bucketName;
    }
    public ImageDataProtocol download(String remoteImagePath) throws IOException, AmazonClientException {
        AmazonS3Client s3Client = S3Helper.getClient(credential);
        // GET Item Image from AWS S3
        S3Object object = s3Client.getObject(
                new GetObjectRequest(bucketName, remoteImagePath));
        InputStream objectData = object.getObjectContent();
        ImageDataProtocol image = new ImageDataDownloadData(objectData);
        objectData.close();
        return image;
    }
}
