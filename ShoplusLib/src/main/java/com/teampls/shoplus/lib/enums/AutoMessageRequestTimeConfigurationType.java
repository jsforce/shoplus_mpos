package com.teampls.shoplus.lib.enums;

/**
 * 자동 메시지 전송 시간 관련 설정
 *
 * @author lucidite
 */
public enum AutoMessageRequestTimeConfigurationType {
    DO_NOT_SEND(-1),
    INSTANTLY(0),
    NEXT_MORNING(9);

    private int command;

    AutoMessageRequestTimeConfigurationType(int command) {
        this.command = command;
    }

    public int getCommandValue() {
        return this.command;
    }
}
