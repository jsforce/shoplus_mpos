package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2017-03-07.
 */

abstract public class BaseModuleView implements View.OnClickListener, AdapterView.OnItemClickListener  {
    protected Context context;
    protected View container;
    protected View view;

    public BaseModuleView(Context context, View view, View container) {
        this.context = context;
        this.container = container;
        this.view = view;
    }

    public void setOnClick(int... resIds) {
        for (int resId : resIds)
            setOnClick(resId);
    }

    public View setOnClick(int viewId) {
        View v = view.findViewById(viewId);
        if (v instanceof TextView)
            MyView.setTextViewByDeviceSize(context, (TextView) v);
//        else if (v instanceof ImageView)
//            MyView.setImageViewSizeByDeviceSizeScale(context, 0.8, (ImageView) v);
        v.setOnClickListener(this);
        return v;
    }

    public void show() {
        container.setVisibility(View.VISIBLE);
    }

    public void hide() {
        container.setVisibility(View.GONE);
    }

    public void setVisibility(int visibility, int... resIds) {
        for (int resId : resIds) {
            View v = view.findViewById(resId);
            v.setVisibility(visibility);
        }
    }

    abstract public void refresh();

}
