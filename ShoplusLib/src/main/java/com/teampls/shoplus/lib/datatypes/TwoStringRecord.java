package com.teampls.shoplus.lib.datatypes;

import android.util.Log;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2018-07-11.
 */

public class TwoStringRecord<T> implements  Comparable<TwoStringRecord> {
    public String string1 = "", string2 = "";
    public T id;

    public TwoStringRecord(T id, String string1, String string2) {
        this.id = id;
        this.string1 = string1;
        this.string2 = string2;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] %s, %s, %s", location, id.toString(), string1, string2));
    }

    @Override
    public int compareTo(TwoStringRecord o) {
        if (o.id instanceof DateTime)
            return ((DateTime) id).compareTo((DateTime) o.id);
        else
            return string1.compareTo(o.string1);
    }
}
