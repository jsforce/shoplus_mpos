package com.teampls.shoplus.lib.datatypes;

import java.util.ArrayList;
import java.util.List;

/**
 * Gate-keeping 및 사용자 권한 확인 용도의 서비스 식별자
 *
 * @author lucidite
 */

public enum ShoplusServiceType {
    NONE("", ""),
    MPOS("mPOS", "mpos"),
    POS("카탈로그", "pos"),
    SLIPS("미송", "slip"),
    ItemTrace("재고", "item-trace"),
    NewArrivals("신상알림", "new-arrivals"),
    Receipt("영수증", "receipt"),
    ChatBot("챗봇", "chatbot"),
    SaaS("창고관리", "saas"),
    AutoMessaging("자동메시지", "auto-msg"),
    Groupelicium("엘리시움 매장용", "elicium");

    public String uiStr = "";
    private String remoteStr = "";

    ShoplusServiceType(String serviceName, String remoteStr) {
        this.uiStr = serviceName;
        this.remoteStr = remoteStr;
    }


    public static ShoplusServiceType fromRemoteStr(String remoteStr) {
        if (remoteStr == null) {
            return NONE;
        }
        for (ShoplusServiceType service: ShoplusServiceType.values()) {
            if (service.remoteStr.equals(remoteStr)) {
                return service;
            }
        }
        return NONE;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }

    public List<ShoplusServiceType> getCompatibles() {
        List<ShoplusServiceType> results = new ArrayList<>();
        results.add(this);
        switch (this) {
            default:
                break;
            case ItemTrace:
            case POS:
                results.add(SaaS); // ItemTrace, POS는 SaaS도 호환가능한 권한으로 인정한다는 의미
                break;
        }
        return results;
    }
}
