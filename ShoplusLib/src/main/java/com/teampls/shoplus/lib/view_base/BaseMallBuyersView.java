package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MRequestedBuyers;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotBuyerBaseData;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotBuyerRegistrationRequestData;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotBuyerBaseDataProtocol;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotBuyerRegistrationRequestDataProtocol;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2019-05-10.
 */

abstract public class BaseMallBuyersView extends BaseActivity implements AdapterView.OnItemClickListener {
    protected ListView listView;
    protected RequestedBuyerDBAdapter requestedAdapter;
    protected AllBuyerDBAdapter allBuyerDBAdapter;
    protected MyEmptyGuide myEmptyGuide;
    protected Button btCancel, btApply;
    protected CheckBox cbShowAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;

        requestedAdapter = new RequestedBuyerDBAdapter(context);
        allBuyerDBAdapter = new AllBuyerDBAdapter(context);
        listView = findViewById(R.id.listview_listview);
        listView.setAdapter(requestedAdapter);
        listView.setOnItemClickListener(this);
        btApply = findViewById(R.id.listview_apply);
        btCancel = findViewById(R.id.listview_cancel);
        cbShowAll = findViewById(R.id.listview_checkbox);

        setVisibility(View.VISIBLE, R.id.listview_button_container);
        setOnClick(R.id.listview_apply, R.id.listview_cancel);

        myEmptyGuide = new MyEmptyGuide(context, getView(), requestedAdapter, listView, R.id.listview_empty_container,
                R.id.listview_empty_message, R.id.listview_empty_contactUs);
        myEmptyGuide.setMessage("등록요청을 한\n거래처가 없습니다");
        myEmptyGuide.setContactUsButton(View.GONE);

        cbShowAll.setText("등록고객 보기");
        cbShowAll.setVisibility(View.VISIBLE);
        cbShowAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbShowAll.isChecked()) {
                    myEmptyGuide.setAdapter(allBuyerDBAdapter);
                    downloadAllBuyers();
                } else {
                    myEmptyGuide.setAdapter(requestedAdapter);
                    downloadRequestedBuyers();
                }
            }
        });

        downloadRequestedBuyers();
    }

    protected void downloadRequestedBuyers() {
        new CommonServiceTask(context, "downloadRequestedBuyers", "요청 거래처 다운중...") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<ChatbotBuyerRegistrationRequestDataProtocol> protocols = chatbotService.getBuyerRegistrationRequests(userSettingData.getUserShopInfo());
                List<ChatbotBuyerRegistrationRequestData> data = new ArrayList<>();
                for (ChatbotBuyerRegistrationRequestDataProtocol protocol : protocols)
                    data.add(new ChatbotBuyerRegistrationRequestData(protocol));
                MRequestedBuyers.getInstance(context).refresh(data);
                requestedAdapter.setRecords(data);
            }

            @Override
            public void onPostExecutionUI() {
                listView.setAdapter(requestedAdapter);
                refresh();
            }
        };
    }

    protected void downloadAllBuyers() {
        new CommonServiceTask(context, "downloadAllBuyers", "모든 등록처 다운중...") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<ChatbotBuyerBaseDataProtocol> protocols = chatbotService.getAllBuyers(userSettingData.getUserShopInfo());
                List<ChatbotBuyerBaseData> data = new ArrayList<>();
                for (ChatbotBuyerBaseDataProtocol protocol : protocols)
                    data.add(new ChatbotBuyerBaseData(protocol));
                allBuyerDBAdapter.setRecords(data);
            }

            @Override
            public void onPostExecutionUI() {
                listView.setAdapter(allBuyerDBAdapter);
                refresh();
            }
        };
    }


    @Override
    public int getThisView() {
        return R.layout.base_listview;
    }

    protected void refresh() {
        allBuyerDBAdapter.notifyDataSetChanged();
        requestedAdapter.notifyDataSetChanged();
        myEmptyGuide.refresh();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        if (cbShowAll.isChecked()) {
            final ChatbotBuyerBaseData record = allBuyerDBAdapter.getRecord(position);
            UserRecord counterpart = userDB.getRecord(record.getBuyerPhoneNumber());
            String name = counterpart.name.isEmpty()? record.getBuyerName() : counterpart.name;
            MyUI.toastSHORT(context, String.format("%s(%s)", name, BaseUtils.toPhoneFormat(record.getBuyerPhoneNumber())));
        } else {
            final ChatbotBuyerRegistrationRequestData record = requestedAdapter.getRecord(position);
            new InsertBuyerDialog(context, record, new MyOnTask<Pair<String, Boolean>>() {
                @Override
                public void onTaskDone(final Pair<String, Boolean> result) {
                    new CommonServiceTask(context, "acceptBuyer", "") {
                        @Override
                        public void doInBackground() throws MyServiceFailureException {
                            ContactDataProtocol protocol = chatbotService.acceptBuyerRegistrationRequests(userSettingData.getUserShopInfo(), record, result.first, result.second);
                            userDB.updateOrInsert(new UserRecord(protocol));
                        }

                        @Override
                        public void onPostExecutionUI() {
                            requestedAdapter.removeRecord(position);
                            MyUI.toastSHORT(context, String.format("등록했습니다"));
                            refresh();
                        }
                    };
                }
            });
        }
    }

    public class AllBuyerDBAdapter extends BaseDBAdapter<ChatbotBuyerBaseData> {

        public AllBuyerDBAdapter(Context context) {
            super(context);
        }

        @Override
        public void generate() {

        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new BaseViewHolder() {
                public ImageView ivIcon;
                public TextView tvName, tvAuthed;
                protected ChatbotBuyerBaseDataProtocol record;

                @Override
                public int getThisView() {
                    return R.layout.row_user;
                }

                @Override
                public void init(View view) {
                    tvName = findTextViewById(context, view, R.id.row_user_name);
                    tvAuthed = findTextViewById(context, view, R.id.row_user_phoneNumber);
                    ivIcon = view.findViewById(R.id.row_user_icon);
                }

                @Override
                public void update(int position) {
                    record = records.get(position);
                    UserRecord counterpart = userDB.getRecord(record.getBuyerPhoneNumber());
                    String name = counterpart.name.isEmpty()? record.getBuyerName() : counterpart.name;
                    tvName.setText(String.format("%s (%s)", name, BaseUtils.toPhoneFormat(record.getBuyerPhoneNumber())));
                    tvAuthed.setText(record.isAuthed() ? "문자인증" : "--");
                    tvAuthed.setTextColor(record.isAuthed() ? ColorType.Black.colorInt : ColorType.Blue.colorInt);
                }
            };
        }
    }


    public class RequestedBuyerDBAdapter extends BaseDBAdapter<ChatbotBuyerRegistrationRequestData> {

        public RequestedBuyerDBAdapter(Context context) {
            super(context);
        }

        @Override
        public void generate() {

        }

        @Override
        public BaseViewHolder getViewHolder() {

            return new BaseViewHolder() {
                public ImageView ivIcon;
                public TextView tvName, tvDate;
                protected ChatbotBuyerRegistrationRequestData record;

                @Override
                public int getThisView() {
                    return R.layout.row_user;
                }

                @Override
                public void init(View view) {
                    tvName = findTextViewById(context, view, R.id.row_user_name);
                    tvDate = findTextViewById(context, view, R.id.row_user_phoneNumber);
                    ivIcon = view.findViewById(R.id.row_user_icon);
                }

                @Override
                public void update(int position) {
                    record = records.get(position);
                    UserRecord counterpart = userDB.getRecord(record.getBuyerPhoneNumber());
                    String name = counterpart.name.isEmpty()? record.getBuyerName() : counterpart.name;
                    tvName.setText(String.format("%s (%s)", name, BaseUtils.toPhoneFormat(record.getBuyerPhoneNumber())));
                    tvDate.setText(record.getRequestedDateTime().toString(BaseUtils.MD_FixedSize_Format));
                    tvDate.setTextColor(ColorType.Blue.colorInt);
                }
            };
        }
    }


    // UpdateUserDialog 유사
    class InsertBuyerDialog extends BaseDialog {
        private ChatbotBuyerRegistrationRequestData record = new ChatbotBuyerRegistrationRequestData();
        private EditText etName, etPhoneNumber;
        private CheckBox cbIsRetailPrice;
        private MyOnTask<Pair<String, Boolean>> onTask;

        public InsertBuyerDialog(Context context, ChatbotBuyerRegistrationRequestData record, MyOnTask<Pair<String, Boolean>> onTask) {
            super(context);
            this.record = record;
            this.onTask = onTask;
            setDialogWidth(0.9, 0.7);
            etName = findEditTextById(R.id.dialog_user_input_name, record.getBuyerName());
            etPhoneNumber = findEditTextById(R.id.dialog_user_input_phoeNumber, BaseUtils.toPhoneFormat(record.getBuyerPhoneNumber()));
            etPhoneNumber.setEnabled(false); // 수정금지
            setVisibility(View.GONE, R.id.dialog_user_input_address, R.id.dialog_user_input_address_clear,
                    R.id.dialog_user_input_addLocal, R.id.dialog_user_input_temp_phoneNumber,
                    R.id.dialog_user_input_phoeNumber_guide);
            findTextViewById(R.id.dialog_user_input_title, "신규 등록");

            cbIsRetailPrice = findViewById(R.id.dialog_user_input_isRetailPrice);
            cbIsRetailPrice.setVisibility(userSettingData.getProtocol().isRetailOn() ? View.VISIBLE : View.GONE);

            setOnClick(R.id.dialog_user_input_cancel, R.id.dialog_user_input_apply,
                    R.id.dialog_user_input_name_clear);

            show();
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_user_input;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_user_input_cancel) {
                dismiss();

            } else if (view.getId() == R.id.dialog_user_input_apply) {
                if (Empty.isEmpty(context, etName, "이름을 입력해주세요")) return;

                String name = etName.getText().toString().trim();
                if (userDB.hasValue(UserDB.Column.name, name)) {
                    MyUI.toastSHORT(context, String.format("같은 이름이 있습니다"));
                    return;
                }

                if (onTask != null)
                    onTask.onTaskDone(Pair.create(name, cbIsRetailPrice.isChecked()));
                dismiss();

            } else if (view.getId() == R.id.dialog_user_input_name_clear) {
                etName.setText("");

            }
        }
    }
}
