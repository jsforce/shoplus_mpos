package com.teampls.shoplus.lib.view_module;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.IdRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.PickDateDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.MemberPermission;
import com.teampls.shoplus.lib.view.MyView;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2017-11-27.
 */

public class MyCreationDateHeader {
    private TextView tvDate, tvtNotice;
    private GlobalDB globalDB;
    private Context context;
    private UserSettingData userSettingData;

    public MyCreationDateHeader(final Context context, View view, @IdRes int showDate, @IdRes int resetDate, @IdRes int setDate) {
        this(context, view, showDate, resetDate, setDate, -1);
    }

    public MyCreationDateHeader(final Context context, View view, @IdRes int showDate, @IdRes int resetDate,
                                @IdRes int setDate, @IdRes int notice) {
        if (view == null)
            view = ((ViewGroup) ((Activity) context).findViewById(android.R.id.content)).getChildAt(0);
        this.context = context;
        globalDB = GlobalDB.getInstance(context);
        tvDate = (TextView) view.findViewById(showDate);

        userSettingData = UserSettingData.getInstance(context);

        TextView tvResetDate = view.findViewById(resetDate);
        tvResetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MyAlertDialog(context, "현재시간으로 변경", "영수증 시간을 현재 시간으로 변경하시겠습니까?") {
                    @Override
                    public void yes() {
                        globalDB.put(GlobalDB.KEY_IS_CUSTOM_DATE, false);
                        if (userSettingData.getProtocol().getTimeShift() != 0)
                            MyUI.toastSHORT(context, String.format("%s시간이 적용되고 있습니다 (설정 : 사장님)", BaseUtils.toSignStr(userSettingData.getProtocol().getTimeShift())));
                        refreshHeader();
                    }
                };
            }
        });

        TextView tvSetDate =  view.findViewById(setDate);
       tvSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MemberPermission.SetTransactionDate.hasPermission(userSettingData)== false) {
                    MemberPermission.SetTransactionDate.toast(context);
                    return;
                }

                new PickDateDialog(context, "지정날짜 선택", true, 180) {
                    @Override
                    public void onApplyClick(DateTime finalDateTime) {
                        globalDB.put(GlobalDB.KEY_IS_CUSTOM_DATE, true);
                        globalDB.put(GlobalDB.KEY_CUSTOM_DATE, finalDateTime.toString());
                        refreshHeader();
                    }
                };
            }
        });

        if (notice >= 0)
            tvtNotice = (TextView) view.findViewById(notice);

        MyView.setTextViewByDeviceSize(context, tvDate, tvResetDate, tvSetDate);
        refreshHeader();
    }

    private void refreshHeader() {
        DateTime createdDateTime = globalDB.getCreatedDateTime(UserSettingData.getInstance(context));
        if (BasePreference.isShowWeekday(context))
            tvDate.setText(String.format("%s %s %s", createdDateTime.toString(BaseUtils.YMD_Kor_Format),
                    BaseUtils.getDayOfWeekKor(BaseUtils.createDay()), createdDateTime.toString(BaseUtils.Hm_Format)));
        else
            tvDate.setText(createdDateTime.toString(BaseUtils.YMD_HS_Kor_Format));

        if (globalDB.getBool(GlobalDB.KEY_IS_CUSTOM_DATE, false)) {
            tvDate.setTypeface(Typeface.DEFAULT_BOLD);
            tvDate.setTextColor(ColorType.Red.colorInt); // 빨간색으로 경고
            if (tvtNotice != null)
                tvtNotice.setVisibility(View.VISIBLE);
        } else {
            tvDate.setTypeface(Typeface.DEFAULT);
            tvDate.setTextColor(ColorType.DarkGray.colorInt);
            if (tvtNotice != null)
                tvtNotice.setVisibility(View.GONE);
        }
    }

}
