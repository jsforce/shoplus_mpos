package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ForgotPasswordContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.ForgotPasswordHandler;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.CheckableStringAdapter;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ServiceErrorHelper;
import com.teampls.shoplus.lib.awsservice.cognito.CognitoUtils;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.BaseRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.datatypes.MyShopInformationData;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyGuideDialog;
import com.teampls.shoplus.lib.dialog.SearchStringDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.dialog.UserConfigTypeDialog;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.view_base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-04-08.
 */

public class MyInfoView extends BaseActivity {
    private TextView tvName, tvPermission, tvPhone1, tvPhone2, tvAccount1, tvAccount2, tvAccount3,
            tvAddress, tvLocation, tvMemo, tvSlipUserType, tvShopPhoneNumber, tvServer, tvOsVersion,
            tvAppVersion, tvDisplay, tvActivated, tvTerms, tvPrivacy, tvResetPassword,
            tvMainPhoneNumber, tvMyPhoneNumber, tvPublisherPhoneNumber, tvClone;
    private List<String> buildingAddresses = new ArrayList<>();
    private boolean doRestartApp = false;
    private MyShopInformationData shopData;

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, MyInfoView.class));
    }

    @Override
    public int getThisView() {
        return R.layout.base_myinfo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.setTitle("앱 정보 및 설정");
        myActionBar.hideRefreshTime();
        shopData = userSettingData.cloneShopData();
        tvPermission = findTextViewById(R.id.myinfo_permission, String.format("권한 : %s", MyDevice.getPermissionString(context)));
        tvShopPhoneNumber = findTextViewById(R.id.myinfo_shopPhoneNumber);
        tvMyPhoneNumber = findTextViewById(R.id.myinfo_myPhoneNumber, "");
        tvMainPhoneNumber = findTextViewById(R.id.myinfo_mainPhoneNumber, "");
        tvPublisherPhoneNumber = findTextViewById(R.id.myinfo_publisherPhoneNumber, "");
        tvClone = (TextView) findViewById(R.id.myinfo_publisherPhoneNumber_clone);
        tvServer = findTextViewById(R.id.myinfo_server, String.format("서버 : %s", MyAWSConfigs.isProduction() ? "운영" : "테스트"));
        tvOsVersion = findTextViewById(R.id.myinfo_osVersion, String.format("안드로이드 : %d", Build.VERSION.SDK_INT));
        tvAppVersion = findTextViewById(R.id.myinfo_appVersion, String.format("앱버전 : %s", MyDevice.getAppVersionName(context)));
        tvDisplay = findTextViewById(R.id.myinfo_display, String.format("화면크기 : %d x %d", MyDevice.getWidthDP(context), MyDevice.getHeightDP(context)));
        tvTerms = findTextViewById(R.id.myinfo_terms, "앱 약관 보기");
        tvPrivacy = findTextViewById(R.id.myinfo_privacy, "개인정보 보호정책 보기");

        List<String> registerStrs = new ArrayList<>();
        if (userSettingData.hasMaster())
            registerStrs.add("사장님 기능");
        for (ShoplusServiceType type : userSettingData.getActivations())
            registerStrs.add(type.uiStr);
        if (registerStrs.size() == 0)
            registerStrs.add("-");
        tvActivated = findTextViewById(R.id.myinfo_activated, String.format("가입 : %s", TextUtils.join(", ", registerStrs))); // 이제 미가입은 없어야 한다, 간편장부는 있다

        // 영수증 생성용 정보
        buildingAddresses = getBuildingAddresses();
        tvName = findTextViewById(R.id.myinfo_name, "");
        tvPhone1 = findTextViewById(R.id.myinfo_phoneNumber1, "");
        tvPhone2 = findTextViewById(R.id.myinfo_phoneNumber2, "");
        tvAccount1 = findTextViewById(R.id.myinfo_account1, "");
        tvAccount2 = findTextViewById(R.id.myinfo_account2, "");
        tvAccount3 = findTextViewById(R.id.myinfo_account3, "");
        tvAddress = findTextViewById(R.id.myinfo_address, "");
        tvLocation = findTextViewById(R.id.myinfo_location, "");
        tvMemo = findTextViewById(R.id.myinfo_memo, "");
        tvSlipUserType = findTextViewById(R.id.myinfo_slipUserType, "");
        tvResetPassword =findTextViewById(R.id.myinfo_reset_password);

        setOnClick(R.id.myinfo_close, R.id.myinfo_terms, R.id.myinfo_privacy, R.id.myinfo_memo,
                R.id.myinfo_phoneNumber1, R.id.myinfo_phoneNumber2, R.id.myinfo_account1, R.id.myinfo_account2,
                R.id.myinfo_account3, R.id.myinfo_address, R.id.myinfo_location,
                R.id.myinfo_name_container, R.id.myinfo_slipUserType_container,
                R.id.myinfo_permission_container,
                R.id.myinfo_reset_password_container, R.id.myinfo_apply,
                R.id.myinfo_logout, R.id.myinfo_shopPhoneNumber_container,
                R.id.myinfo_mainPhoneNumber_container, R.id.myinfo_myPhoneNumber,
                R.id.myinfo_publisherPhoneNumber, R.id.myinfo_publisherPhoneNumber_clone
        );

    }

    private void downloadUserSettingData() {
        new CommonServiceTask(context, "downloadMyShopData") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                // 근무매장이 변경되면 user level과 shop data가 변경된다.
                UserSettingDataProtocol userSettingDataProtocol = userService.getUserSetting(myDB.getPrivateNumber());
                userSettingData.set(userSettingDataProtocol);
                shopData = userSettingData.cloneShopData();
            }

            @Override
            public void onPostExecutionUI() {
                refreshView();
            }
        };
    }

    private void refreshView() {
        tvMyPhoneNumber = findTextViewById(R.id.myinfo_myPhoneNumber, String.format("내번호 : %s (%s)",
                BaseUtils.toPhoneFormat(myDB.getPrivateNumber()), userSettingData.getUserLevelType().toUIStr()));

        BaseUtils.setText(tvName, "매장명 : ", shopData.getShopName());
        BaseUtils.setText(tvShopPhoneNumber, "근무매장 : ", BaseUtils.toPhoneFormat(userSettingData.getUserShopInfo().getShopPhoneNumber()));
        BaseUtils.setText(tvMainPhoneNumber, "자매매장 : ", BaseUtils.toPhoneFormat(userSettingData.getUserShopInfo().getMainPhoneNumber()));
        BaseUtils.setText(tvPublisherPhoneNumber, "구독매장 : ", BaseUtils.toPhoneFormat(globalDB.getValue(GlobalDB.KEY_PublisherPhoneNumber)));
        tvClone.setVisibility(globalDB.getValue(GlobalDB.KEY_PublisherPhoneNumber).isEmpty() ? View.GONE : View.VISIBLE);

        BaseUtils.setText(tvPermission, "권한 : ", MyDevice.getPermissionString(context));
        BaseUtils.setText(tvSlipUserType, "영업구분 : ", userSettingData.getUserConfigType().uiStr);

        BaseUtils.setText(tvPhone1, "매장번호1 : ", shopData.getShopPhoneNumber(0));
        BaseUtils.setText(tvPhone2, "매장번호2 : ", shopData.getShopPhoneNumber(1));

        BaseUtils.setText(tvAccount1, "계좌1 : ", shopData.getAccountNumber(0));
        BaseUtils.setText(tvAccount2, "계좌2 : ", shopData.getAccountNumber(1));
        BaseUtils.setText(tvAccount3, "계좌3 : ", shopData.getAccountNumber(2));

        BaseUtils.setText(tvAddress, "건물주소 : ", shopData.getBuildingAddress());
        BaseUtils.setText(tvLocation, "매장위치 : ", shopData.getDetailedAddress());

        BaseUtils.setText(tvMemo, "메모 : ", shopData.getNotice());

        setVisibility(userSettingData.isProvider(), R.id.myinfo_mainPhoneNumber_container, R.id.myinfo_shopPhoneNumber_container,
                R.id.myinfo_activated);

        setVisibility(userSettingData.isProvider(), tvPhone1, tvPhone2, tvAccount1, tvAccount2, tvAccount3, tvAddress,
                tvLocation, tvMemo);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUserSettingData(new MyOnTask() {
            @Override
            public void onTaskDone(Object result) {
                shopData = userSettingData.cloneShopData();
                refreshView();
            }
        });
    }

    protected void checkRestartAndFinish() {
        if (doRestartApp) {
            doRestartApp = false;
            MyDevice.restartApp(context, "중요 설정이 변경되어 앱을 재시작합니다");
        } else {
            finish();
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.contactUs)
                .add(MyMenu.close);
        if (MyAWSConfigs.isMaster && myDB.getPrivateNumber().equals("01037860747"))
            myActionBar.add(MyMenu.setting);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case setting:
                if (MyAWSConfigs.isMaster)
                    onHockeyAppExists();
                break;
            case contactUs:
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                break;
            case close:
                checkShopDataAndFinish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        checkShopDataAndFinish();
    }

    private void checkShopDataAndFinish() {
        if (BaseRecord.isSame(shopData, userSettingData.getWorkingShop())) {
            checkRestartAndFinish();
        } else {
            new MyAlertDialog(context, "변경 내용 저장", "변경된 내용이 있습니다. 저장하시겠습니까?") {
                @Override
                public void yes() {
                    userSettingData.updateWorkingShopData(shopData, new MyOnTask() {
                        @Override
                        public void onTaskDone(Object result) {
                            MyUI.toastSHORT(context, String.format("설정을 적용했습니다"));
                            checkRestartAndFinish();
                        }
                    });
                }

                @Override
                public void no() {
                    checkRestartAndFinish();
                }
            };
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view); // 이거 빠지면 메뉴 동작안함, 주의

        // shopData는 마지막에 저장
        if (view.getId() == R.id.myinfo_name_container) {
            new UpdateValueDialog(context, "매장명 입력", "", shopData.getShopName()) {
                public void onApplyClick(String newStr) {
                    shopData.setShopName(newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_publisherPhoneNumber) {
            new UpdateValueDialog(context, "구독매장 번호 입력", "상품 정보를 복사해올 매장의 번호를 입력해주세요. 등록은 고객지원으로 문의주세요",
                    globalDB.getValue(GlobalDB.KEY_PublisherPhoneNumber)) {
                public void onApplyClick(String newStr) {
                    if (newStr.isEmpty() == false && BaseUtils.isValidPhoneNumber(newStr) == false) {
                        MyUI.toastSHORT(context, String.format("올바른 번호가 아닙니다"));
                        return;
                    }

                    globalDB.put(GlobalDB.KEY_PublisherPhoneNumber, BaseUtils.toSimpleForm(newStr));
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_publisherPhoneNumber_clone) {
            final String publisherPhoneNumber = globalDB.getValue(GlobalDB.KEY_PublisherPhoneNumber);
            if (publisherPhoneNumber.isEmpty()) {
                MyUI.toastSHORT(context, String.format("구독매장 번호가 없습니다"));
                return;
            }

            new MyAlertDialog(context, "구독매장 상품 복사", String.format("%s 매장의 상품을 내 매장으로 복사하시겠습니까? (1회 100개씩)", BaseUtils.toPhoneFormat(publisherPhoneNumber))) {
                @Override
                public void yes() {

                    new CommonServiceTask(context, "clone publisher", "복사 중...") {
                        int cloneCount = 0;

                        @Override
                        public void doInBackground() throws MyServiceFailureException {
                            cloneCount = itemService.cloneItems(userSettingData.getUserShopInfo(), publisherPhoneNumber);
                        }

                        @Override
                        public void onPostExecutionUI() {
                            if (cloneCount == 0)
                                MyUI.toastSHORT(context, String.format("새로 복사된 상품이 없습니다"));
                            else
                                MyUI.toast(context, String.format("%d개의 상품이 복사되었습니다. 첫화면에서 [새로고침]을 해주세요", cloneCount));
                        }

                    };

                }
            };


            ////////////////////////////////////////////////////
            // 근무매장 변경 (샵 리스트가 뜸)
            // /////////////////////////////////////////////////
        } else if (view.getId() == R.id.myinfo_shopPhoneNumber_container) {

            if (userSettingData.getAllowedShops().size() == 0) {
                new MyGuideDialog(context, "근무매장 등록필요", "등록된 근무매장이 없습니다.\n\n" +
                        "등록은 사장님만 하실 수 있으며 사장님이신 경우는 사장님 등록을 고객지원으로 해주세요").show();
                return;
            }

            new AllowedShopsDialog(context) {
                @Override
                public void onApply(String phoneNumber) {
                    if (phoneNumber.equals(userSettingData.getUserShopInfo().getShopPhoneNumber())) {
                        MyUI.toastSHORT(context, String.format("변경되지 않았습니다"));
                    } else {
                        MyUI.toastSHORT(context, String.format("변경되었습니다. 모든 앱은 재시작해야 안전합니다"));
                        userSettingData.updateWorkingShopPhoneNumber(phoneNumber, new MyOnTask() {
                            @Override
                            public void onTaskDone(Object result) {
                                // 근무 매장에 변동이 생겼으므로 앱을 재시작한다
                                doRestartApp = true;
                                checkRestartAndFinish();
                            }
                        });

                    }

                }
            };

            ////////////////////////////////////////////////////
            // 도/소매 변경
            // /////////////////////////////////////////////////
        } else if (view.getId() == R.id.myinfo_slipUserType_container) {
            final UserConfigurationType preUserConfigurationType = userSettingData.getUserConfigType();
            new UserConfigTypeDialog(context) {
                @Override
                public void onApplyClick(UserConfigurationType userConfigurationType) {
                    if (preUserConfigurationType != userConfigurationType)
                        doRestartApp = true;
                    if (userConfigurationType == UserConfigurationType.BUYER) // 번호는 이미 초기화 했고
                        shopData.setShopName(userSettingData.getMyRecord().name); // 이름도 초기화 해야 한다
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_myPhoneNumber) {
            new MyGuideDialog(context, "내번호와 근무매장", "매장폰인 경우에는 추가 입력이 없어도 되고 사장님, 매니저, 직원의" +
                    " 경우에는 근무 매장의 번호를 아래에 입력해주세요.").show();

        } else if (view.getId() == R.id.myinfo_apply) {

            userSettingData.updateWorkingShopData(shopData, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    MyUI.toastSHORT(context, String.format("설정을 적용했습니다"));
                    checkRestartAndFinish();
                }
            });


        } else if (view.getId() == R.id.myinfo_mainPhoneNumber_container) {
            new MyGuideDialog(context, "자매매장 안내", "멀티샵(여러개 매장)을 운영하실때 사용하는 기능입니다. " +
                    "하나의 매장에서 입력한 정보를 전체 매장이 공유합니다" +
                    "\n\n등록은 고객지원으로 문의주세요").show();

        } else if (view.getId() == R.id.myinfo_logout) {
            new MyAlertDialog(context, "로그 아웃", "모든 앱에서 로그 아웃을 하시겠습니까? 저장된 아이디와 비밀번호가 삭제됩니다") {
                @Override
                public void yes() {
                    myDB.logOut();
                    finishAffinity();
                }
            };

        } else if (view.getId() == R.id.myinfo_reset_password_container) {
            new MyAlertDialog(context, "[경고] 비밀번호 변경", "비밀번호를 변경하시겠습니까?\n\n공용 계정이면 다른 사용자에게 알려주셔야 합니다.") {
                @Override
                public void yes() {
                    String userPoolPhoneNumber = CognitoUtils.convertToCompatiblePhoneNumber(myDB.getPrivateNumber());
                    UserHelper.getPool().getUser(userPoolPhoneNumber).forgotPasswordInBackground(new ResetPasswordHandler());
                }
            };

        } else if (view.getId() == R.id.myinfo_terms) {
            MyDevice.openWeb(context, BaseGlobal.TermsOfServiceUrl);

        } else if (view.getId() == R.id.myinfo_privacy) {
            MyDevice.openWeb(context, BaseGlobal.PrivacyPolicyUrl);

        } else if (view.getId() == R.id.myinfo_permission_container) {
            MyDevice.openAppSetting(context);

        } else if (view.getId() == R.id.myinfo_close) {
            checkShopDataAndFinish();

        } else if (view.getId() == R.id.myinfo_phoneNumber1) {
            new UpdateValueDialog(context, "전화번호 입력", "", shopData.getShopPhoneNumber(0)) {
                public void onApplyClick(String newStr) {
                    shopData.updatePhoneNumber(0, newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_phoneNumber2) {
            new UpdateValueDialog(context, "전화번호 입력", "", shopData.getShopPhoneNumber(1)) {
                public void onApplyClick(String newStr) {
                    shopData.updatePhoneNumber(1, newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_account1) {
            if (userSettingData.isMasterOrNoMaster() == false) {
                MyUI.toastSHORT(context, String.format("사장님만 변경할 수 있습니다"));
                return;
            }

            new UpdateValueDialog(context, "계좌 입력", "", shopData.getAccountNumber(0)) {
                public void onApplyClick(String newStr) {
                    shopData.updateAccountNumber(0, newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_account2) {
            if (userSettingData.isMasterOrNoMaster() == false) {
                MyUI.toastSHORT(context, String.format("사장님만 변경할 수 있습니다"));
                return;
            }

            new UpdateValueDialog(context, "계좌 입력", "", shopData.getAccountNumber(1)) {
                public void onApplyClick(String newStr) {
                    shopData.updateAccountNumber(1, newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_account3) {
            if (userSettingData.isMasterOrNoMaster() == false) {
                MyUI.toastSHORT(context, String.format("사장님만 변경할 수 있습니다"));
                return;
            }

            new UpdateValueDialog(context, "계좌 입력", "", shopData.getAccountNumber(2)) {
                public void onApplyClick(String newStr) {
                    shopData.updateAccountNumber(2, newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_address) {
            new SearchStringDialog(context, "주소 입력", buildingAddresses) {
                @Override
                public void onItemClick(String string) {
                    String buildingAddress = string;
                    if (string.contains("]")) {
                        String buildingName = string.split("]")[0].replace("[", "");
                        buildingAddress = string.split("]")[1].trim();
                        if (shopData.getDetailedAddress().isEmpty()) // 건물이름 자동으로 등록
                            shopData.setDetailedAddress(buildingName);
                    }
                    shopData.setBuildingAddress(buildingAddress);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_location) {
            new UpdateValueDialog(context, "매장 위치 입력", "", shopData.getDetailedAddress()) {
                public void onApplyClick(String newStr) {
                    shopData.setDetailedAddress(newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.myinfo_memo) {
            new UpdateValueDialog(context, "메모 입력", "", shopData.getNotice()) {
                public void onApplyClick(String newStr) {
                    shopData.setNotice(newStr);
                    refreshView();
                }
            };
        }


    }

    public static List<String> getBuildingAddresses() {
        List<String> results = new ArrayList<>();
        results.add("[제일평화] 04567 서울시 중구 마장로 13");
        results.add("[서평화] 04568 서울시 중구 마장로1가길 33");
        results.add("[남평화] 04567 서울시 중구 장충단로 282-10");
        results.add("[청평화] 04568 서울시 중구 청계천로 334");
        results.add("[동평화] 04568 서울시 중구 청계천로 318");
        results.add("[신평화] 04567 서울시 중구 청계천로 298");
        results.add("[뉴존] 04566 서울시 중구 을지로 45길 62");
        results.add("[디오트] 04568 서울시 중구 다산로 293");
        results.add("[테크노] 04568 서울시 중구 마장로 1가길 17");
        results.add("[광희패션몰] 04567 서울시 중구 마장로1길 21");
        results.add("[벨포스트] 04567 서울시 중구 마장로 19");
        results.add("[유어스] 04566 서울시 중구 마장로 22");
        results.add("[디자이너클럽] 04566 서울시 중구 을지로45길 72");
        results.add("[엘리시움] 04568 서울시 중구 마장로 1길 18");
        results.add("[스튜디오W] 04568 서울시 중구 마장로 1길 22");
        results.add("[아트플라자] 04568 서울시 중구 마장로 1길 28");
        results.add("[맥스타일] 04567 서울시 중구 마장로 3");
        results.add("[APM] 04566 서울시 중구 마장로 40");
        results.add("[APM PLACE] 04565 서울시 중구 을지로 276");
        results.add("[헬로우 APM] 04564 서울시 중구 장충단로 253");
        results.add("[TEAM204] 04566 서울시 중구 마장로 30");
        results.add("[밀리오레] 04563 서울시 중구 장충단로 263");
        results.add("[두타몰] 04563 서울시 중구 장충단로 275 두산타워");
        return results;
    }


    class ResetPasswordHandler implements ForgotPasswordHandler {
        private String newPassword = "";
        private Boolean isCancelledByUser = false;

        @Override
        public void onSuccess() {
            final String phoneNumber = myDB.getPrivateNumber();
            myDB.resetPassword(phoneNumber, newPassword, true);
            new MyDelayTask(context, 100) {
                @Override
                public void onFinish() {
                    Log.i("DEBUG_TAG", "[DEBUG] 비밀번호가 초기화되었습니다[" + phoneNumber + "]: " + newPassword);
                    new MyDelayTask(context, 1000) {
                        @Override
                        public void onFinish() {
                            MyDevice.openApp(context, context.getPackageName());
                        }
                    };
                    finishAffinity();
                }
            };
        }

        @Override
        public void getResetCode(ForgotPasswordContinuation continuation) {
            Log.i("DEBUG_TAG", "[DEBUG] Password Reset Code: Destination=" + continuation.getParameters().getDestination());
            new PasswordResetAuthDialog(context, continuation).show();
        }

        @Override
        public void onFailure(Exception exception) {
            String userErrorMessage;
            if (isCancelledByUser) {
                userErrorMessage = "사용자에 의해 취소되었습니다.";
            } else {
                userErrorMessage = ServiceErrorHelper.getCognitoIdErrorMessageForUser(exception, true);
            }
            MyUI.toast(context, userErrorMessage);
            Log.e("DEBUG_JS", String.format("[UserLogin.onFailure] %s", exception.getLocalizedMessage()));
        }

        class PasswordResetAuthDialog extends BaseDialog {
            private EditText etAuthenCode, etPassword;
            private ForgotPasswordContinuation continuation;
            private LinearLayout passwordContainer;

            public PasswordResetAuthDialog(Context context, ForgotPasswordContinuation continuation) {
                super(context);
                this.continuation = continuation;
                setOnClick(com.teampls.shoplus.R.id.dialog_input_authenCode_apply);
                setOnClick(com.teampls.shoplus.R.id.dialog_input_authenCode_cancel);
                setOnClick(com.teampls.shoplus.R.id.dialog_input_authenCode_clear);
                setOnClick(com.teampls.shoplus.R.id.dialog_input_authenCode_password_clear);
                passwordContainer = (LinearLayout) findViewById(com.teampls.shoplus.R.id.dialog_input_authenCode_password_container);

                etAuthenCode = (EditText) findTextViewById(com.teampls.shoplus.R.id.dialog_input_authenCode_edit, "");
                etPassword = (EditText) findTextViewById(com.teampls.shoplus.R.id.dialog_input_authenCode_password_edit, "");
            }

            @Override
            public int getThisView() {
                return com.teampls.shoplus.R.layout.dialog_input_authencode;
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == com.teampls.shoplus.R.id.dialog_input_authenCode_password_clear) {
                    etPassword.setText("");
                } else if (view.getId() == com.teampls.shoplus.R.id.dialog_input_authenCode_cancel) {
                    isCancelledByUser = true;
                    continuation.continueTask();
                    MyUI.toastSHORT(context, String.format("비밀번호 초기화를 중지하겠습니다"));
                    dismiss();
                } else if (view.getId() == com.teampls.shoplus.R.id.dialog_input_authenCode_apply) {
                    if (Empty.isEmpty(context, etAuthenCode, "인증번호가 입력되지 않았습니다")) return;
                    if (Empty.isEmpty(context, etPassword, "비밀번호가 입력되지 않았습니다")) return;
                    // 전화번호 혹은 유저가 입력한 번호를 다시 encode해서 사용한다
                    newPassword = BaseUtils.encode(etPassword.getText().toString().trim(), myDB.getPrivateNumber());
                    continuation.setPassword(newPassword);
                    continuation.setVerificationCode(etAuthenCode.getText().toString());
                    continuation.continueTask();
                    MyUI.toastSHORT(context, String.format("비밀번호를 초기화 했습니다"));
                    dismiss();
                } else if (view.getId() == com.teampls.shoplus.R.id.dialog_input_authenCode_clear) {
                    etAuthenCode.setText("");
                }
            }
        }
    }

    abstract class AllowedShopsDialog extends BaseDialog implements AdapterView.OnItemClickListener {
        private ListView listView;
        private CheckableStringAdapter adapter;

        public AllowedShopsDialog(Context context) {
            super(context);
            setDialogWidth(0.9, 0.6);
            findTextViewById(R.id.dialog_list_title, "근무매장 선택");
            findTextViewById(R.id.dialog_list_message, "근무매장은 사장님이 내 번호를 추가해 주시면 나타납니다. 근무매장이 해제되면 매장폰으로 변경됩니다.");

            List<String> allowedShops = new ArrayList<>();
            allowedShops.add("근무매장 해제");
            int position = 1, selectedPosition = 0;
            for (String phoneNumber : userSettingData.getAllowedShops()) {
                allowedShops.add(BaseUtils.toPhoneFormat(phoneNumber));
                if (phoneNumber.equals(userSettingData.getUserShopInfo().getShopPhoneNumber())) {
                    selectedPosition = position;
                }
                position++;
            }
            adapter = new CheckableStringAdapter(context);
            adapter.setRecords(allowedShops);
            adapter.setClickedPosition(selectedPosition);

            listView = (ListView) findViewById(R.id.dialog_listview);
            listView.setOnItemClickListener(this);
            listView.setAdapter(adapter);

            setOnClick(R.id.dialog_list_close);
            show();
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_list;
        }


        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_list_close) {
                dismiss();
            }
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
            new MyAlertDialog(context, "근무매장 변경시 주의점", "[작성 중인 영수증]은 삭제됩니다\n\n 새 매장으로 변경하시겠습니까?") {
                @Override
                public void yes() {
                    if (position == 0) {
                        onApply("");
                    } else {
                        onApply(BaseUtils.toSimpleForm(adapter.getRecord(position)));
                    }
                    dismiss();
                }
            };
        }

        abstract public void onApply(String phoneNumber);
    }

    private void onHockeyAppExists() {
        if (myDB.getPrivateNumber().equals("01037860747") == false)
            return;

        new UpdateValueDialog(context, "설정번호", "", 0) {
            @Override
            public void onApplyClick(String newValue) {
                if (BaseUtils.toInt(newValue) == 2016) {
                    new UpdateValueDialog(context, "근무매장번호", "", 0, 0) {
                        @Override
                        public void onApplyClick(String newValue) {
                            if (newValue.startsWith("010") == false)
                                newValue = "010" + newValue;
                            userSettingData.setWorkingShopPhoneNumberForMaster(newValue);
                            doRestartApp = true;
                            checkRestartAndFinish();
                            userDB.clear(); // 업데이트 된것만 가져오는데 시간 정보가 최신이라 refresh가 안되는 문제가 있음
                        }
                    };
                }
            }
        };
    }
}
