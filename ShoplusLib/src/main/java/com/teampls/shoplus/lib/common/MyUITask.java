package com.teampls.shoplus.lib.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

/**
 * Created by Medivh on 2016-09-26.
 */
public class MyUITask {
    private Context context;
    private String location = "";


    public MyUITask(final Context context) {
        this(context, "");
    }

    public MyUITask(final Context context, String location) {
        this.context = context;
        this.location = location;

        if (MyUI.isActivity(context, "MyAsyncTask") == false)
            return;

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                onPreExecute();
                execute();
            }
        });

    }

    public void onPreExecute() {
    }

    public void doInBackground() {
    }

    private void execute() {
        try {
            doInBackground();
            onPostExecution();
        } catch (Exception e) {
            catchException(e);
        }
    }

    public void onPostExecution() {
    }

    public void catchException(Exception e) {
        Log.e("DEBUG_JS", String.format("[MyUITask.run] %s.%s : %s", context.getClass().getSimpleName(), location, e.getLocalizedMessage()));
    }
}
