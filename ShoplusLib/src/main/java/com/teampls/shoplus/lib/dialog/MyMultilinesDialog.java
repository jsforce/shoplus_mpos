package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;

import java.util.List;

/**
 * Created by Medivh on 2017-08-24.
 */

public class MyMultilinesDialog extends BaseDialog {
    private List<String> lines;
    private MyOnTask<List<String>> onTask;

    public MyMultilinesDialog(Context context, String title, List<String> lines) {
        super(context);
        setDialogWidth(0.95, 0.95);
        this.lines = lines;
        findTextViewById(R.id.dialog_multilines_title, title);
        findTextViewById(R.id.dialog_multilines_textview, TextUtils.join("\n", lines));
        setOnClick(R.id.dialog_multilines_close, R.id.dialog_multilines_functions);
    }

    public MyMultilinesDialog(Context context, String title,String line) {
        this(context, title, BaseUtils.toList(line));
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_multilines;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_multilines_close) {
            dismiss();

        } else if (view.getId() == R.id.dialog_multilines_functions) {
            if (onTask != null)
                onTask.onTaskDone(lines);
            dismiss();
        }
    }

    public void setOnFunctionClick(String title, MyOnTask<List<String>> onTask) {
        Button btFunction = findViewById(R.id.dialog_multilines_functions);
        btFunction.setText(title);
        btFunction.setVisibility(View.VISIBLE);
        this.onTask = onTask;
    }
}
