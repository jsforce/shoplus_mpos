package com.teampls.shoplus.lib.database;


import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2016-09-01.
 */
public class PosAndSourceLinkRecord {
    public int id = 0, sourceSlipItemDbId = 0, posSlipItemDbId = 0;
    public String counterpartPhoneNumber = "";
    public DateTime updateDateTime = Empty.dateTime;

    public PosAndSourceLinkRecord() {
    }

    public PosAndSourceLinkRecord(int id, int posSlipItemDbId, DateTime updateDateTime,
                                  String counterpartPhoneNumber, int sourceSlipItemDbId) {
        this.id = id;
        this.posSlipItemDbId = posSlipItemDbId;
        this.updateDateTime = updateDateTime;
        this.counterpartPhoneNumber = counterpartPhoneNumber;
        this.sourceSlipItemDbId = sourceSlipItemDbId;
    }

    public PosAndSourceLinkRecord(int posSlipItemDbId, int sourceSlipItemDbId, String counterpartPhoneNumber) {
        this(0, posSlipItemDbId, DateTime.now(), counterpartPhoneNumber, sourceSlipItemDbId);
    }

    public boolean isSame(PosAndSourceLinkRecord record) {
        if (posSlipItemDbId != record.posSlipItemDbId) return false;
        if (updateDateTime.isEqual(record.updateDateTime) == false) return false;
        if (counterpartPhoneNumber.equals(record.counterpartPhoneNumber) == false) return false;
        if (sourceSlipItemDbId != record.sourceSlipItemDbId) return false;
        return true;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] <%d>, my %d, %s, %s, external %d", location,
                id, posSlipItemDbId, updateDateTime.toString(BaseUtils.fullFormat), counterpartPhoneNumber,
                sourceSlipItemDbId));
    }

}
