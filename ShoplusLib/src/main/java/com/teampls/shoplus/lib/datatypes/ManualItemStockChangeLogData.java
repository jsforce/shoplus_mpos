package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 사용자에 의한 수동 재고수량 수정 로그
 *
 * @author lucidite
 */
public class ManualItemStockChangeLogData implements ItemStockChangeLogProtocol {
    private String logId;
    private DateTime updated;
    private int value;
    private boolean batchUpdate;

    public ManualItemStockChangeLogData(JSONObject dataObj) throws JSONException {
        if (dataObj == null) {
            this.logId = "";
            this.updated = APIGatewayHelper.fallbackDatetime;
            this.value = 0;
            this.batchUpdate = false;
        } else {
            this.logId = dataObj.getString("log_id");
            this.value = dataObj.getInt("stock_val");
            String updatedStr = dataObj.getString("updated");
            String[] updatedStrElems = updatedStr.split("-", 2);
            this.updated = APIGatewayHelper.getDateTimeFromRemoteString(updatedStrElems[0]);
            this.batchUpdate = (updatedStrElems.length >= 2 && updatedStrElems[1].toLowerCase().equals("b"));
        }
    }

    @Override
    public String getLogId() {
        return logId;
    }

    @Override
    public DateTime getUpdatedDateTime() {
        return updated;
    }

    @Override
    public int getValue() {
        return value;
    }

    /**
     * 일괄수정에 의한 수동 변동인지 반환한다.
     * @return
     */
    public boolean isBatchUpdate() {
        return batchUpdate;
    }
}
