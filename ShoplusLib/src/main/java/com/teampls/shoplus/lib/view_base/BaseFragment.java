package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CompoundButtonCompat;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUITask;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.view.MyView;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Medivh on 2016-06-05.
 */
abstract public class BaseFragment extends Fragment implements View.OnClickListener {
    protected Context context;
    protected KeyValueDB keyValueDB;
    protected UserDB userDB;
    protected GlobalDB globalDB;
//    protected TextView customTextView;
    protected MyDB myDB;
    protected View myView;
    public boolean selected = false, isViewCreated = false;
    protected UserSettingData userSettingData;
    private Set<View> resizedViews = new HashSet<>();
    public boolean isValidInstance = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isValidInstance = true;
        Log.i("DEBUG_JS", String.format("==%s==", getClass().getSimpleName()));
        this.context = getContext();
        isViewCreated = false;
        keyValueDB = KeyValueDB.getInstance(context);
        userDB = UserDB.getInstance(context);
        myDB = MyDB.getInstance(context);
        globalDB = GlobalDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isValidInstance = false;
    }

//    public void setCustomTextView(TextView textView) {
//        this.customTextView = textView;
//    }

    public void start(MyOnAsync onAsync) {};

    public void refreshData(){};

    abstract public void refresh();

    public abstract String getTitle();

    abstract public int getTabIndex();

    //  public void onTabSelected 에서 호출
    public void onSelected() {
        selected = true;
    }

    public void onUnSelected() {
        selected = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isValidInstance == false) {
            Log.w("DEBUG_JS", String.format("[%s.onResume] isValidInstance == false", getClass().getSimpleName()));
            MyDevice.restartApp(context, "앱을 재시작 합니다");
        }
    }

    public View setOnClick(View view, int resId) {
        View result = view.findViewById(resId);
        result.setOnClickListener(this);

        if (result instanceof TextView)
            resizeViews((TextView) result);

        if (result instanceof CheckBox && MyDevice.getAndroidVersion() <= 21)
            CompoundButtonCompat.setButtonTintList((CheckBox) result, ColorStateList.valueOf(ColorType.CheckBoxColor.colorInt));
        return result;
    }

    private void resizeViews(TextView view) {
        if (resizedViews.contains(view) == false) {
            MyView.setTextViewByDeviceSize(context, view);
            resizedViews.add(view);
        }
    }

    public void setOnClick(View view, int... resIds) {
        for (int resId :  resIds)
            setOnClick(view, resId);
    }

    public TextView findTextViewById(View view, int resId, String title) {
        TextView result = (TextView) view.findViewById(resId);
        result.setText(title);
        resizeViews((TextView) result);
        return result;
    }

    public TextView findTextViewById(View view, int resId) {
        TextView result = (TextView) view.findViewById(resId);
        resizeViews((TextView) result);
        return result;
    }

    public void refreshUi(Context context) {
        new MyUITask(context, "BaseFragment") {
            @Override
            public void doInBackground() {
                refresh();
            }
        };
    }

    protected void setVisibility(View view, int visibility, int... resIds) {
        for (int resId : resIds) {
            view.findViewById(resId).setVisibility(visibility);
        }
    }

    protected void setVisibility(View view, int visibility, View... uiViews) {
        for (View uiView : uiViews)
          uiView.setVisibility(visibility);
    }

}
