package com.teampls.shoplus.lib.protocol;


import com.teampls.shoplus.lib.datatypes.NewArrivalMessageKey;

/**
 * (카카오링크를 탭하여) 수신한 신상알림 메시지(링크) 데이터
 *
 * @author lucidite
 */

public interface ReceivedNewArrivalsMessageDataProtocol {
    NewArrivalMessageKey getMessageKey();
    String getMainImagePath();
    String getMessage();
}
