package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseUserDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyKeyboard;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.composition.UserComposition;
import com.teampls.shoplus.lib.database_global.AddressbookDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.UpdateUserDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-02-22.
 */

public class MyAllUsersView extends BaseActivity implements AdapterView.OnItemClickListener {
    private AllUsersAdapter adapter;
    private ListView listView;
    private EditText etName;
    private MyKeyboard myKeyboard;
    private List<UserRecord> fullRecords = new ArrayList<>();
    private AddressbookDB addressbookDB;
    private static MyOnClick<UserRecord> onItemClick;
    private static final String KEY_showAddress = "myAllUsersView.show.Address";
    private UserComposition userComposition;

    public static void startActivity(Context context, MyOnClick<UserRecord> onItemClick) {
        MyAllUsersView.onItemClick = onItemClick;
        context.startActivity(new Intent(context, MyAllUsersView.class));
    }

    @Override
    public int getThisView() {
        return R.layout.base_user_search;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        userComposition = new UserComposition(context);
        userComposition.cleanUp();

        addressbookDB = AddressbookDB.getInstance(context);

        adapter = new AllUsersAdapter(context);

        myActionBar.setCheckBox(0, "주소록 ", keyValueDB.getBool(KEY_showAddress, true), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyValueDB.put(KEY_showAddress, ((CheckBox) view).isChecked());
                refreshFullRecords(((CheckBox) view).isChecked());
            }
        });

        etName = findViewById(R.id.user_search_name);
        if (MyDevice.getAndroidVersion() <  27)
            etName.setHint("이름 또는 전화번호");
        etName.addTextChangedListener(new MyTextWatcher());
        listView = (ListView) findViewById(R.id.user_search_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        myKeyboard = new MyKeyboard(context, findViewById(R.id.user_search_container), etName);

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.READ_CONTACTS)) {
            if (addressbookDB.isEmpty() && addressbookDB.isChecked() == false)
                userComposition.syncAddressBook(new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        refreshFullRecords(true);
                        addressbookDB.setChecked(true);
                        if (addressbookDB.isEmpty())
                            Log.w("DEBUG_JS", String.format("[MyAllUsersView] addressbook is empty"));
                    }
                });
        } else {
            MyDevice.requestPermission(context, MyDevice.UserPermission.READ_CONTACTS);
            MyUI.toast(context, String.format("주소록 권한이 없습니다. 쉬운 거래처 등록을 위해 사용하고자 합니다."));
        }
        setOnClick(R.id.user_search_clear, R.id.user_search_add, R.id.user_search_temp, R.id.user_search_guest);
        setVisibility(View.VISIBLE, R.id.user_search_temp, R.id.user_search_guest);

        // [일반고객]이 없으면 자동추가
        UserRecord guestUser = UserRecord.createGuest(userSettingData);
        UserRecord retailUser = UserRecord.createRetail(userSettingData);
        List<UserRecord> notRegistereds = new ArrayList<>();
        if (userDB.has(guestUser.phoneNumber) == false)
            notRegistereds.add(guestUser);
        if (userDB.has(retailUser.phoneNumber) == false)
            notRegistereds.add(retailUser);

        if (notRegistereds.isEmpty()) {
            refreshFullRecords(keyValueDB.getBool(KEY_showAddress, true));
        } else {
            userComposition.updateUsers("MyAllUser", notRegistereds, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    refreshFullRecords(keyValueDB.getBool(KEY_showAddress, true));
                }
            });
        }

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_CONTACTS) == false) {
            Log.w("DEBUG_JS", String.format("[MyAllUsersView.onCreate] request permission WRITE_CONTACTS...."));
            MyDevice.requestPermission(context, MyDevice.UserPermission.WRITE_CONTACTS);
        }
    }

    private void refreshFullRecords(boolean doIncludeAddress) {
        if (doIncludeAddress) {
            fullRecords = userComposition.getAllSortedRecords();
        } else {
            fullRecords = userComposition.getUserDBRecords();
        }
        adapter.setRecords(fullRecords);
        adapter.refreshRetailUsers();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (MyDevice.UserPermission.values()[requestCode]) {
            case READ_CONTACTS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (addressbookDB.isEmpty())
                        userComposition.syncAddressBook(new MyOnTask() {
                            @Override
                            public void onTaskDone(Object result) {
                                refreshFullRecords(true);
                            }
                        });
                } else {
                    finish();
                }
                break;
            case WRITE_CONTACTS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Log.w("DEBUG_JS", String.format("[MyAllUsersView.onCreate] has permission WRITE_CONTACTS automatically"));
                break;
        }
    }

    public void refresh() {
        if (adapter == null) return;
        //adapter.generate();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.add(MyMenu.addCounterpart)
                .add(MyMenu.refresh)
                .add(MyMenu.counterpart)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case addCounterpart:
                addNewUser("");
                break;
            case counterpart:
                MyUsersView.startActivity(context);
                break;
            case refresh:
                new OnRefreshDialog(context).show();
                break;
            case keyboard:
                myKeyboard.switchKeyboard();
                break;
            case close:
                finish();
                break;
        }
    }

    class OnRefreshDialog extends MyButtonsDialog {

        public OnRefreshDialog(final Context context) {
            super(context, "새로고침", "");
            addButton("거래처 (최근 변경)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    userComposition.downloadCounterparts(new MyOnTask<List<UserRecord>>() {
                        @Override
                        public void onTaskDone(List<UserRecord> result) {
                            MyUI.toast(context, String.format("변경 : %s", TextUtils.join(",", UserRecord.getNames(result))));
                            refreshFullRecords(keyValueDB.getBool(KEY_showAddress, true));

                        }
                    });
                }
            });

            addButton("거래처 (재조사)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    final int prevSize = userDB.getSize();
                    userDB.clear();
                    userComposition.downloadCounterparts(new MyOnTask<List<UserRecord>>() {
                        @Override
                        public void onTaskDone(List<UserRecord> result) {
                            MyUI.toast(context, String.format("%d → %d 명", prevSize, result.size()));
                            refreshFullRecords(keyValueDB.getBool(KEY_showAddress, true));
                        }
                    });
                }
            });

            addButton("내 폰 주소록", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    userComposition.syncAddressBook(new MyOnTask() {
                        @Override
                        public void onTaskDone(Object result) {
                            refreshFullRecords(true);
                        }
                    });
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BaseGlobal.RequestRefresh:
                refreshFullRecords(keyValueDB.getBool(KEY_showAddress, true));
                break;
        }
    }

    private void addNewUser(String name) {
        new UpdateUserDialog(context, new UserRecord(name, ""), DbActionType.INSERT) {
            @Override
            public void onUserInputFinish(final UserRecord userRecord) {
                refreshFullRecords(true);
                new MyAlertDialog(context, String.format("%s 선택", userRecord.getName()), String.format("입력하신 %s를 선택하시겠습니까?", userRecord.getName())) {
                    @Override
                    public void yes() {
                        if (onItemClick != null)
                            onItemClick.onMyClick(getView(), userRecord); // view는 더미
                        finish(); // context finished 오류를 만들 수 있다
                    }
                };
            }
        };
    }

    @Override
    public void onClick(final View view) {
        super.onClick(view);
        if (view.getId() == R.id.user_search_clear) {
            etName.setText("");

        } else if (view.getId() == R.id.user_search_add) {
            addNewUser(etName.getText().toString());

        } else if (view.getId() == R.id.user_search_temp) {
            new MyAlertDialog(context, "영수증 먼저", "먼저 영수증을 쓰고 나중에 고객을 선택하시겠습니까?") {
                @Override
                public void yes() {
                    if (onItemClick != null)
                        onItemClick.onMyClick(view, UserRecord.createTemp());
                    finish();
                }
            };
        } else if (view.getId() == R.id.user_search_guest) {
            MyUI.toast(context, String.format("일반고객으로 발행합니다"));
            if (onItemClick != null)
                onItemClick.onMyClick(view, UserRecord.createGuest(userSettingData));
            finish();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
        final UserRecord record = adapter.getRecord(position);
        new MyAlertDialog(context, "거래처 선택", String.format("%s (%s) 를 선택 하셨습니까?", record.name, BaseUtils.toPhoneFormat(record.phoneNumber))) {
            @Override
            public void yes() {
                if (userDB.has(record.phoneNumber) == false) {
                    userComposition.uploadUser("MyAllUsersView.onItemClick", record,
                            new MyOnTask() {
                                @Override
                                public void onTaskDone(Object result) {
                                    if (onItemClick != null) {
                                        new MyDelayTask(context, 50) {
                                            @Override
                                            public void onFinish() {
                                                onItemClick.onMyClick(view, record);
                                            }
                                        };
                                    }
                                    finish();
                                }
                            }
                    );
                } else {
                    if (onItemClick != null) {
                        new MyDelayTask(context, 50) {
                            @Override
                            public void onFinish() {
                                onItemClick.onMyClick(view, record);
                            }
                        };
                    }
                    finish();
                }
            }
        };
    }

    class MyTextWatcher extends BaseTextWatcher {

        @Override
        public void afterTextChanged(Editable s) {
            adapter.setRecords(filterRecords(s, fullRecords));
            refresh();
        }
    }

    class AllUsersAdapter extends BaseUserDBAdapter {

        public AllUsersAdapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new BaseUserDBAdapter.DefaultViewHolder() {
                public void update(int position) {
                    super.update(position);
                    if (userDB.has(record.phoneNumber) == false) {
                        name.setTextColor(ColorType.Gray.colorInt);
                        phoneNumber.setTextColor(ColorType.Gray.colorInt);
                        ivIcon.setImageResource(R.drawable.user_gray);
                    } else {
                        name.setTextColor(ColorType.Black.colorInt);
                        phoneNumber.setTextColor(ColorType.Black.colorInt);
                        ivIcon.setImageResource(R.drawable.user_black);
                    }
                }
            };
        }
    }

}
