package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.printer.MyPosPrinter;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.AccountLogRecord;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.viewSetting.PrinterSettingView;
import com.teampls.shoplus.lib.view_base.BaseActivity;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2017-04-29.
 */

public class AccountSharingView extends BaseActivity {
    protected static AccountLogRecord accountLogRecord;
    protected static AccountRecord accountRecord;
    private LinearLayout printView;
    private MyPosPrinter myPosPrinter;
    private TextView tvProvider, tvPhone1, tvPhone2, tvLocation;

    public static void startActivityForResult(Context context, AccountRecord accountRecord, AccountLogRecord accountLogRecord) {
        AccountSharingView.accountRecord = accountRecord;
        AccountSharingView.accountLogRecord = accountLogRecord;
        ((Activity) context).startActivityForResult(new Intent(context, AccountSharingView.class), 0);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    public int getThisView() {
        return R.layout.base_account_sharing;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        setMyPosPrinter(globalDB.getValue(GlobalDB.KEY_PRINTER_Selected));
        tvProvider = findTextViewById(R.id.account_sharing_provider, "");
        tvPhone1 = findTextViewById(R.id.account_sharing_phone1, "");
        tvPhone2 = findTextViewById(R.id.account_sharing_phone2, "");
        tvLocation = findTextViewById(R.id.account_sharing_location, "");

        printView = (LinearLayout) findViewById(R.id.account_sharing_container);
        UserRecord counterpart = userDB.getRecord(accountLogRecord.getCounterpart(context));
        findTextViewById(R.id.account_sharing_counterpart, counterpart.getName() + " 귀하");
        findTextViewById(R.id.account_sharing_value, BaseUtils.toCurrencyStr(-accountLogRecord.variation));
        findTextViewById(R.id.account_sharing_account, accountLogRecord.accountType.uiStr);
        findTextViewById(R.id.account_sharing_before, BaseUtils.toCurrencyStr(accountRecord.getReceivable() - accountLogRecord.variation));
        findTextViewById(R.id.account_sharing_variation, BaseUtils.toCurrencyStr(accountLogRecord.variation));
        findTextViewById(R.id.account_sharing_after, BaseUtils.toCurrencyStr(accountRecord.getReceivable()));

        DateTime salesDate = accountLogRecord.createdDateTime;
        findTextViewById(R.id.account_sharing_date, String.format("%s (%s) %s", salesDate.toString(BaseUtils.YMD_Kor_Format),
                BaseUtils.getDayOfWeekKor(salesDate), salesDate.toString(BaseUtils.HS_Kor_Format)));

        setOnClick(R.id.account_sharing_provider, R.id.account_sharing_cancel, R.id.account_sharing_print);
        refreshView();
    }

    private void setMyPosPrinter(String deviceName) {
        myPosPrinter = MyPosPrinter.getInstance(context);
//        String productName = PosPrinterModel.toProductName(deviceName);
//        switch (PosPrinterModel.get(productName)) {
//            default:
//
//                break;
//            case R200:
//                myPosPrinter = MyR200Printer.getInstance(context);
//                break;
//        }
    }

    private void refreshView() {
        UserRecord provider;
        if (UserSettingData.getInstance(context).isProvider()) {
            provider = userSettingData.getProviderRecord();
        } else {
            provider = userDB.getRecord(accountLogRecord.getCounterpart(context));
        }
        BaseUtils.setText(tvProvider, provider.name);

        if (userSettingData.isProvider()) {
            setVisibility(View.VISIBLE, tvPhone1, tvPhone2, tvLocation);
            BaseUtils.setText(tvPhone1, "T.", userSettingData.getWorkingShop().getShopPhoneNumber(0));
            BaseUtils.setText(tvPhone2, "T.",  userSettingData.getWorkingShop().getShopPhoneNumber(1));
            BaseUtils.setText(tvLocation, "", userSettingData.getWorkingShop().getDetailedAddress());
        } else {
            setVisibility(View.GONE, tvPhone1, tvPhone2, tvLocation);
        }

    }

    @Override
    protected void onDestroy() {
        myPosPrinter.disconnectPrinter();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.account_sharing_cancel) {
            finish();

        } else if (view.getId() == R.id.account_sharing_print) {
            if (MyDevice.isBluetoothEnabled() == false) {
                MyUI.toastSHORT(context, String.format("블루투스가 꺼져있습니다. 프린터 설정을 해주세요"));
                return;
            }

            if (myPosPrinter.hasPairedDevice() == false) {
                MyUI.toastSHORT(context, String.format("연결된 장치가 없습니다. 프린터 설정을 해주세요"));
                return;
            }

            new MyAlertDialog(context, "거래기록 출력", "거래 기록을 출력하시겠습니까?") {
                @Override
                public void yes() {
                    myPosPrinter.printAccount(accountRecord, accountLogRecord);
                }
            };
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.basicInfo) // 1
                .add(MyMenu.sendKakao)
    //            .addInteger(MyMenu.sendSMS)
                .add(MyMenu.printSetting);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case printSetting:
                PrinterSettingView.startActivity(context);
                break;

            case sendKakao:
                if (MyDevice.hasKakao(context)) {
                    MyUI.toastSHORT(context, String.format("카카오톡을 실행합니다."));
                    MyDevice.sendKakaoAccount(context, MyGraphics.toBitmap(printView));
                }
                break;

            case basicInfo:
                PosSlipInfoView.startActivity(context);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshView();
    }

}