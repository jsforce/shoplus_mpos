package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 발주 도착에 따른 재고수량 변동 로그
 *
 * @author lucidite
 */
public class ArrivalItemStockChangeLogData implements ItemStockChangeLogProtocol {
    private String logId;
    private int value;
    private boolean cancelled;

    public ArrivalItemStockChangeLogData(JSONObject dataObj) throws JSONException {
        if (dataObj == null) {
            this.logId = "";
            this.value = 0;
            this.cancelled = false;
        } else {
            this.logId = dataObj.getString("created");
            this.value = dataObj.getInt("value");
            this.cancelled = dataObj.optBoolean("cancelled", false);
        }
    }

    @Override
    public String getLogId() {
        return logId;
    }

    @Override
    public DateTime getUpdatedDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.logId);
    }

    @Override
    public int getValue() {
        return value;
    }

    /**
     * [NOTE] 현재는 발주 도착 건을 취소하는 기능을 지원하지 않는다.
     * @return
     */
    public boolean isCancelled() {
        return cancelled;
    }
}
