package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2017-08-01.
 */

public enum MyBoolean {
    UnDecided("무료가입"), True("유료가입"), False("미가입");

    public String uiStr = "";

    MyBoolean(String uiStr) {
        this.uiStr = uiStr;
    }
}
