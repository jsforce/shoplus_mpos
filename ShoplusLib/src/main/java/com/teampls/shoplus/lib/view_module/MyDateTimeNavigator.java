package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.support.annotation.IdRes;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.dialog.PickDateDialog;
import com.teampls.shoplus.lib.dialog.PickMonthDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view.MyView;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2017-12-08.
 */

public class MyDateTimeNavigator  implements View.OnClickListener {
    private TextView tvYear, tvMonth, tvDay, tvLeftButton, tvRightButton;
    private ImageView ivNext, ivPrev;
    private DateTime dateTime = Empty.dateTime, fromLimitDateTime = Empty.dateTime, toLimitDateTime = Empty.dateTime;
    private int prevId, nextId;
    private MyOnClick<DateTime> onClickListener;
    private boolean prevOffsetEnabled = false;
    private int prevOffset = 1;
    private boolean isRecent = false;
    private List<DateTime> customDateTimes = new ArrayList<>();
    public Set<DateTime> downloadedDateTimes = new HashSet<>();
    private LinearLayout container;

    public enum MyMode {Month, Week, Day, Custom}

    private MyMode timeMode = MyMode.Month;
    private boolean doLimitPeriod = false;
    private Context context;
    private View myView;

    public MyDateTimeNavigator(Context context, DateTime dateTime, View view, int prevId, int nextId) {
        this.context = context;
        myView = view;
        this.dateTime = dateTime;
        this.prevId = prevId;
        this.nextId = nextId;
        ivNext = (ImageView) view.findViewById(nextId);
        ivPrev = (ImageView) view.findViewById(prevId);
        ivPrev.setOnClickListener(this);
        ivNext.setOnClickListener(this);
        MyView.setImageViewSizeByDeviceSize(context, ivNext, ivPrev);
        downloadedDateTimes.clear();
    }

    public MyDateTimeNavigator(Context context, DateTime dateTime, View view, MyMode mode) {
        this.context = context;
        myView = view;
        this.dateTime = dateTime;
        switch (mode) {
            case Day:
                // module_daily_navigation
                this.prevId = R.id.daily_navigation_prev;
                this.nextId = R.id.daily_navigation_next;
                setAsDayMode(view, R.id.daily_navigation_year, R.id.daily_navigation_month, R.id.daily_navigation_day);
                setContainer(R.id.daily_navigation_container);
                break;
            case Month:
                // module_monthly_navigation
                this.prevId = R.id.monthly_header_prev;
                this.nextId = R.id.monthly_header_next;
                setAsMonthMode(view, R.id.monthly_year, R.id.monthly_month);
                setContainer(R.id.monthly_navigation_container);
                break;
        }

        ivNext = view.findViewById(nextId);
        ivPrev = view.findViewById(prevId);
        ivPrev.setOnClickListener(this);
        ivNext.setOnClickListener(this);
        MyView.setImageViewSizeByDeviceSize(context, ivNext, ivPrev);
        downloadedDateTimes.clear();
    }

    public MyDateTimeNavigator setContainer(int resId) {
        container = myView.findViewById(resId);
        return this;
    }

    public void setVisibility(int visibility) {
        if (container != null)
            container.setVisibility(visibility);
    }

    public boolean isRecentMode() {
        return isRecent;
    }

    private List<DateTime> selectedDays = new ArrayList<>();

    public void highlightCalendar(List<DateTime> days) {
        selectedDays = days;
    }

    public void setLeftCalendarButton(MyMode mode, final MyOnClick<DateTime> onCalendarSelected) {
        switch (mode) {
            case Day:
                setLeftCalendarButton(R.id.daily_navigation_calendar, onCalendarSelected);
                break;
            case Month:
                // setLeftCalendarButton 와는 다른 동작임
                break;
        }
    }

    public void setRightButton(MyMode mode, final View.OnClickListener onButtonClicked) {
        switch (mode) {
            case Day:
                setRightButton(R.id.daily_navigation_recent, onButtonClicked);
                break;
            case Month:
                setRightButton(R.id.monthly_navigation_refresh, onButtonClicked);
                break;
        }
    }

    public void setLeftCalendarButton(int resId, final MyOnClick<DateTime> onCalendarSelected) {
        tvLeftButton = (TextView) myView.findViewById(resId);
        tvLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                isRecent = false;
                setTextColor(ColorType.Black);
                int limitDays = 365;
                if (doLimitPeriod)
                    limitDays = (int) new Duration(fromLimitDateTime, DateTime.now()).getStandardDays();
                PickDateDialog pickDateDialog = new PickDateDialog(context, "날짜 선택", getDateTime(), false, limitDays) { // 미래가 문제인데...
                    @Override
                    public void onApplyClick(DateTime selectedDateTime) {
                        if (onCalendarSelected != null)
                            onCalendarSelected.onMyClick(view, selectedDateTime);
                    }
                };
                pickDateDialog.highlightDays(selectedDays);
            }
        });
        MyView.setTextViewByDeviceSize(context, tvLeftButton);
    }

    public void setRightRecentButton(MyMode mode, final View.OnClickListener onClick) {
        switch (mode) {
            case Day:
                setRightRecentButton(R.id.daily_navigation_recent, onClick);
                break;
        }
    }

    public void setRightRecentButton(int recent, final View.OnClickListener onClick) {
        tvRightButton = (TextView) myView.findViewById(recent);
        tvRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRecent = true;
                setTextColor(ColorType.DarkGray);
                if (onClick != null)
                    onClick.onClick(view);
            }
        });
        MyView.setTextViewByDeviceSize(context, tvRightButton);
    }

    public void setLeftMonthButton(final MyOnClick<DateTime> onClick) {
        tvLeftButton = myView.findViewById(R.id.monthly_change_month);
        tvLeftButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new PickMonthDialog(context, dateTime.getYear(), dateTime.getMonthOfYear(), new MyOnClick<DateTime>() {
                    @Override
                    public void onMyClick(View view, DateTime record) {
                        if (onClick != null)
                            onClick.onMyClick(myView, record);
                    }
                });
            }
        });
    }

    public void setLeftButton(int recent, final View.OnClickListener onClick) {
        tvLeftButton = (TextView) myView.findViewById(recent);
        tvLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRecent = false;
                if (onClick != null)
                    onClick.onClick(view);
            }
        });
        MyView.setTextViewByDeviceSize(context, tvLeftButton);
    }

    public void setRightButton(int recent, final View.OnClickListener onClick) {
        tvRightButton = (TextView) myView.findViewById(recent);
        tvRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRecent = false;
                if (onClick != null)
                    onClick.onClick(view);
            }
        });
        MyView.setTextViewByDeviceSize(context, tvRightButton);
    }

    public void setLimit(DateTime fromLimitDateTime, DateTime toLimitDateTime) {
        doLimitPeriod = true;
        this.fromLimitDateTime = fromLimitDateTime;
        this.toLimitDateTime = toLimitDateTime;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public MyDateTimeNavigator setAsMonthMode(View view, @IdRes int yearId, int monthId) {
        timeMode = MyMode.Month;
        tvYear = (TextView) view.findViewById(yearId);
        tvMonth = (TextView) view.findViewById(monthId);
        tvYear.setOnClickListener(this);
        tvMonth.setOnClickListener(this);
        refreshTextViews();
        MyView.setTextViewByDeviceSize(context, tvYear, tvMonth);
        return this;
    }

    public MyDateTimeNavigator setAsWeekMode(int dayId) {
        timeMode = MyMode.Week;
        tvDay = (TextView) myView.findViewById(dayId);
        refreshTextViews();
        MyView.setTextViewByDeviceSize(context, tvDay);
        return this;
    }

    public MyDateTimeNavigator setAsDayMode(View view, @IdRes int yearId, int monthId, int dayId) {
        timeMode = MyMode.Day;
        tvYear = (TextView) view.findViewById(yearId);
        tvMonth = (TextView) view.findViewById(monthId);
        tvDay = (TextView) view.findViewById(dayId);
        tvYear.setOnClickListener(this);
        tvMonth.setOnClickListener(this);
        tvDay.setOnClickListener(this);
        refreshTextViews();
        MyView.setTextViewByDeviceSize(context, tvYear, tvMonth, tvDay);
        return this;
    }

    public MyDateTimeNavigator setAsDayMode(View view, int dayId) {
        timeMode = MyMode.Day;
        tvDay = (TextView) view.findViewById(dayId);
        tvDay.setOnClickListener(this);
        refreshTextViews();
        MyView.setTextViewByDeviceSize(context, tvDay);
        return this;
    }

    public MyDateTimeNavigator setAsCustomMode(List<DateTime> dateTimes, View view, int dayId) {
        timeMode = MyMode.Custom;
        customDateTimes = dateTimes;
        tvDay = (TextView) view.findViewById(dayId);
        refreshTextViews();
        MyView.setTextViewByDeviceSize(context, tvDay);
        if (dateTimes.size() >= 1) {
            DateTime first = dateTimes.get(0);
            DateTime last = dateTimes.get(dateTimes.size() - 1);
            if (first.isBefore(last))
                setLimit(first, last);
            else
                setLimit(last, first);
        }
        return this;
    }

    public void setDateTime(DateTime dateTime) {
        checkOutofLimitAndApply(dateTime);
        refreshTextViews();
    }

    public void setTextColor(ColorType colorType) {
        if (tvYear != null)
            tvYear.setTextColor(colorType.colorInt);
        if (tvMonth != null)
            tvMonth.setTextColor(colorType.colorInt);
        if (tvDay != null)
            tvDay.setTextColor(colorType.colorInt);
    }

    public void setYearText(String text) {
        if (tvYear != null)
            tvYear.setText(text);
    }

    public void setDayText(String text) {
        if (tvDay != null)
            tvDay.setText(text);
    }

    public void setMonthText(String text) {
        if (tvMonth != null)
            tvMonth.setText(text);
    }

    public void setPrevOffset(int offset) {
        prevOffsetEnabled = (offset >= 2) ? true : false;
        prevOffset = offset;
    }

    public void setOnApplyClick(MyOnClick<DateTime> onClickListener) {
        this.onClickListener = onClickListener;
    }

    private boolean checkOutofLimitAndApply(DateTime dateTime) {
        if (doLimitPeriod == false) {
            this.dateTime = dateTime;
            return false;
        } else {
            if (BaseUtils.isBetween(dateTime, fromLimitDateTime, toLimitDateTime)) {
                this.dateTime = dateTime;
                return false;
            } else {
                MyUI.toastSHORT(context, String.format("검색 범위가 아닙니다"));
                return true;
            }
        }
    }

    public void clickPrevButton() {
        onClick(ivPrev);
    }

    @Override
    public void onClick(View view) {
        boolean isOutofLimit = false;
        if (view.getId() == prevId) {
            switch (timeMode) {
                case Day:
                    isOutofLimit = checkOutofLimitAndApply(dateTime.minusDays(1));
                    break;
                case Week:
                    isOutofLimit = checkOutofLimitAndApply(dateTime.minusWeeks(1));
                    break;
                case Month:
                    isOutofLimit = checkOutofLimitAndApply(dateTime.minusMonths(prevOffsetEnabled ? prevOffset : 1));
                    break;
                case Custom:
                    isOutofLimit = checkOutofLimitAndApply(BaseUtils.getPrev(context, customDateTimes, dateTime, Empty.dateTime));
                    break;
            }
        } else if (view.getId() == nextId) {
            switch (timeMode) {
                case Day:
                    isOutofLimit = checkOutofLimitAndApply(dateTime.plusDays(1));
                    break;
                case Week:
                    isOutofLimit = checkOutofLimitAndApply(dateTime.plusWeeks(1));
                    break;
                case Month:
                    isOutofLimit = checkOutofLimitAndApply(dateTime.plusMonths(1));
                    break;
                case Custom:
                    isOutofLimit = checkOutofLimitAndApply(BaseUtils.getNext(context, customDateTimes, dateTime, Empty.dateTime));
                    break;
            }
        } else {
            // 숫자 버튼을 눌렀을때
        }
        if (isOutofLimit == false) {
            setTextColor(ColorType.Black);
            refreshTextViews();
            if (onClickListener != null) {
                onClickListener.onMyClick(view, dateTime);
            } else {
                Log.e("DEBUG_JS", String.format("[MyDateTimeNavigator.onClick] onClickListener = null"));
            }
        }
    }

    private void refreshTextViews() {
        switch (timeMode) {
            case Day:
                if (tvYear != null)
                    tvYear.setText(String.format("%d년", dateTime.getYear()));
                if (tvMonth != null)
                    tvMonth.setText(String.format("%d월", dateTime.getMonthOfYear()));
                tvDay.setText(String.format("%s일 (%s)", dateTime.getDayOfMonth(), BaseUtils.getDayOfWeekKor(dateTime)));
                break;
            case Week:
                tvDay.setText(String.format("%s 일 ~ %s 토",
                        BaseUtils.toWeek(dateTime).toString(BaseUtils.MD_FixedSize_Format),
                        BaseUtils.toWeek(dateTime).plusDays(7).minusMillis(1).toString(BaseUtils.MD_FixedSize_Format)));
                break;
            case Month:
                tvYear.setText(String.format("%d년", dateTime.getYear()));
                tvMonth.setText(String.format("%d월", dateTime.getMonthOfYear()));
                break;
            case Custom:
                tvDay.setText(BaseUtils.toStringWithWeekDay(dateTime, BaseUtils.MDHm_Format));
                break;
        }
    }

    public Pair<DateTime, DateTime> getFromToDates() {
        DateTime date = BaseUtils.toDay(dateTime); // 일별 기준으로 변경
        switch (timeMode) {
            default:
            case Day:
                return Pair.create(date, date.plusDays(1).minusMillis(1));
            case Week:
                dateTime = BaseUtils.toWeek(dateTime);
                return Pair.create(dateTime, dateTime.plusWeeks(1).minusMillis(1));
            case Month:
                return Pair.create(BaseUtils.toMonth(dateTime), BaseUtils.toMonth(dateTime).plusMonths(1).minusMillis(1));
        }
    }

}
