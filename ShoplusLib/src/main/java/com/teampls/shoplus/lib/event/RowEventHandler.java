package com.teampls.shoplus.lib.event;

/**
 * Created by Medivh on 2017-06-10.
 */

abstract public class RowEventHandler<T> {
    abstract public void onRowClick(int position, T record);
    abstract  public void onTouch1Click(int position, T record);
}
