package com.teampls.shoplus.lib.view_base;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-11-23.
 */

abstract public class BaseImageViewPager extends BaseActivity implements ViewPager.OnPageChangeListener {
    protected BaseViewPager viewPager;
    protected PagerAdapter adapter;
    public int currentPosition = 0;
    protected static List<Integer> ids = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        viewPager = (BaseViewPager) findViewById(R.id.imagePager_pager);
        setAdapter();
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setCurrentItem(currentPosition);
        refreshHeader();
    }

    abstract protected void setAdapter();

    @Override
    public int getThisView() {
        return R.layout.base_imagepager;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //
    }

    private void refreshHeader() {
        myActionBar.setTitle(String.format("%d/%d", currentPosition + 1, adapter.getCount()));
    }

    @Override
    public void onPageSelected(int position) {
        if (position > currentPosition) {

        } else if (position < currentPosition) {

        }
        currentPosition = position;
        refreshHeader();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        //
    }
}

