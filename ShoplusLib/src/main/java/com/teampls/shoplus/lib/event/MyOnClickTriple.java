package com.teampls.shoplus.lib.event;

import android.view.View;

/**
 * Created by Medivh on 2017-04-02.
 */

public interface MyOnClickTriple<F, S, T> {
    public void onMyClick(View view, F first, S second, T third);
}
