package com.teampls.shoplus.lib.awsservice.config;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;

/**
 * 운영서버 설정
 *
 * @author lucidite
 */

public class ProdAWSConfigs extends MyAWSConfigs {
    private final String USER_POOL_ID = "ap-northeast-2_Q7N0O8Sko";
    private final String USER_POOL_CLIENT_ID = "2ku1u3qpltb5i83q52r8e4mk8k";
    private final String USER_POOL_CLIENT_SECRET = "au5rpv0qjnhc5jpm68l5nl8mahtch6g99tl4225e7jht64m3clk";
    private final String IDENTITY_POOL_ID = "ap-northeast-2:b354c99a-0939-48eb-afce-66310e77d1ce";
    private final String APIGATEWAY_INVOKE_URL = "https://zurxigghgj.execute-api.ap-northeast-2.amazonaws.com/prod";
    private String S3_SLIPS_BUCKET_NAME = "project-dalaran-slips";
    private String S3_ITEMS_BUCKET_NAME = "project-stormwind-items";
    private String S3_RECEIPTS_BUCKET_NAME = "project-darnassus-receipts";

    @Override
    public String getUserPoolId() {
        return USER_POOL_ID;
    }

    @Override
    public String getUserPoolClientId() {
        return USER_POOL_CLIENT_ID;
    }

    @Override
    public String getUserPoolClientSecret() {
        return USER_POOL_CLIENT_SECRET;
    }

    @Override
    public String getIdentityPoolId() {
        return IDENTITY_POOL_ID;
    }

    @Override
    public String getApigatewayInvokeUrl() {
        return APIGATEWAY_INVOKE_URL;
    }

    @Override
    public String getS3SlipsBucketName() {
        return S3_SLIPS_BUCKET_NAME;
    }

    @Override
    public String getS3ItemsBucketName() {
        return S3_ITEMS_BUCKET_NAME;
    }

    @Override
    public String getS3ReceiptsBucketName() {
        return S3_RECEIPTS_BUCKET_NAME;
    }
}
