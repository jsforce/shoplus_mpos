package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import android.support.annotation.Nullable;
import android.util.Log;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.apigateway.protocol.APIInputModelInterface;
import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.datatypes.ItemTraceIdData;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemTraceAttachErrorCode;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author lucidite
 */
public class SlipItemJSONData implements SlipItemDataProtocol, JSONDataWrapper, APIInputModelInterface {
    private JSONObject data;
    private static DateTimeFormatter updatedDateFormatter = DateTimeFormat.forPattern("MM/dd");
    private static DecimalFormat decimalFomatter = new DecimalFormat("0.###");

    public SlipItemJSONData(JSONObject dataObj) {
        if (dataObj == null) {
            this.data = new JSONObject();
        } else {
            this.data = dataObj;
        }
    }

    public SlipItemJSONData(SlipItemDataProtocol slipItem) throws JSONException {
        if (slipItem == null) {
            this.data = new JSONObject();
        } else if (slipItem.getClass().equals(SlipItemJSONData.class)) {
            this.data = ((SlipItemJSONData)slipItem).data;
        } else {
            update(slipItem.getItemId(), slipItem.getItemName(), slipItem.getColor(), slipItem.getCustomColorName(), slipItem.getSize(),
                    slipItem.getUnitPrice(), slipItem.getPriceDelta(), slipItem.getQuantity(), slipItem.getItemType(), slipItem.getUpdatedDate(), slipItem.getItemTraces());
        }
    }

    private void update(int itemid, String name, ColorType color, String customColorName, SizeType size, int unitPrice, Integer priceDelta, int quantity, SlipItemType type, String updated, Set<String> itemTraces) throws JSONException {
        data = new JSONObject();
        if (itemid > 0) {
            data.put("i", itemid);
        }
        if (!name.isEmpty()) {
            data.put("n", name);
        }
        String colorStr = color.toRemoteStr();
        if (colorStr != null && !colorStr.isEmpty()) {
            data.put("c", colorStr);
        }
        if (customColorName != null && !customColorName.isEmpty()) {
            data.put("a", customColorName);
        }
        String sizeStr = size.toRemoteStr();
        if (sizeStr != null && !sizeStr.isEmpty()) {
            data.put("s", sizeStr);
        }

        if (unitPrice % MyAWSConfigs.REMOTE_ITEM_PRICE_UNIT == 0) {
            int remoteUnitPrice = unitPrice / MyAWSConfigs.REMOTE_ITEM_PRICE_UNIT;
            data.put("p", String.valueOf(remoteUnitPrice));
        } else {
            String remoteUnitPriceStr = decimalFomatter.format(Double.valueOf(unitPrice) / MyAWSConfigs.REMOTE_ITEM_PRICE_UNIT);
            data.put("p", remoteUnitPriceStr);
        }
        if (priceDelta != null && priceDelta != 0) { // 0 : reset 부분은 일단 pass
            data.put("pd", priceDelta);
           // Log.w("DEBUG_JS", String.format("[SlipItemJSONData.update] %s", data)); // 거래처별 가격 올리는 부분
        }

        data.put("q", quantity);
        data.put("t", type.toRemoteString());
        if (!updated.isEmpty()) {
            data.put("u", updated);
        }

        // 재고추적 QR코드를 이용한 거래기록 처리용도 필드
        if (itemTraces != null && !itemTraces.isEmpty()) {
            JSONArray itemTraceArr = new JSONArray();
            for (String itemTrace: itemTraces) {
                itemTraceArr.put(itemTrace);
            }
            data.put("tk", itemTraceArr);
        }
    }

    @Override
    public int getItemId() {
        return data.optInt("i", 0);
    }

    @Override
    public String getItemName() {
        return data.optString("n", "");
    }

    @Override
    public ColorType getColor() {
        String remoteStr = data.optString("c", "");
        return ColorType.ofRemoteStr(remoteStr);
    }

    @Nullable
    @Override
    public String getCustomColorName() {
        return data.optString("a", null);
    }

    @Override
    public String getColorName() {
        if (this.getCustomColorName() != null && !this.getCustomColorName().isEmpty()) {
            return this.getCustomColorName();
        } else {
            return this.getColor().getOriginalUiName();
        }
    }

    @Override
    public SizeType getSize() {
        String sizeStr = data.optString("s", "");
        if (sizeStr.isEmpty()) {
            return SizeType.Default;
        } else {
            return SizeType.ofRemoteStr(sizeStr);
        }
    }

    @Override
    public int getUnitPrice() {
        try {
            String priceString = data.getString("p");
            if (BaseUtils.isInteger(priceString)) {
                return BaseUtils.toInt(priceString) * MyAWSConfigs.REMOTE_ITEM_PRICE_UNIT;
            } else if (BaseUtils.isFloat(priceString)) {
                double doublePriceValue = data.getDouble("p");
                double price = doublePriceValue *  (double) MyAWSConfigs.REMOTE_ITEM_PRICE_UNIT;
                return (int) Math.round(price);
            } else {
                String strPriceValue = data.optString("p", "0");
                return (int) (Double.valueOf(strPriceValue).doubleValue() * (double) MyAWSConfigs.REMOTE_ITEM_PRICE_UNIT);
            }
        } catch (JSONException e) {
            String strPriceValue = data.optString("p", "0");
            return (int) (Double.valueOf(strPriceValue).doubleValue() * (double) MyAWSConfigs.REMOTE_ITEM_PRICE_UNIT);
        }
    }

    @Override
    public int getQuantity() {
        return data.optInt("q", 0);
    }

    @Override
    public SlipItemType getItemType() {
        return SlipItemType.getFromRemoteTypeString(data.optString("t", ""));
    }

    @Override
    public String getUpdatedDate() {
        return data.optString("u", "");
    }

    @Override
    public Boolean isItemTraceAttached() {
        JSONArray itemTraceArr = data.optJSONArray("tk");
        if (itemTraceArr == null) {
            return false;
        } else {
            return itemTraceArr.length() > 0;
        }
    }

    @Override
    public Set<String> getItemTraces() {
        Set<String> result = new HashSet<>();
        JSONArray itemTraceArr = data.optJSONArray("tk");
        if (itemTraceArr != null) {
            for (int i = 0; i < itemTraceArr.length(); ++i) {
                String itemTrace = itemTraceArr.optString(i, "");
                if (!itemTrace.isEmpty()) {
                    result.add(itemTrace);
                }
            }
        }
        return result;
    }

    @Override
    public ItemTraceAttachErrorCode attachItemTrace(String itemTrace) {
        if (!this.getItemType().isReleasingType()) {
            return ItemTraceAttachErrorCode.UNAVAILABLE_SLIP_ITEM_TYPE;
        } else if (this.getItemId() <= 0) {
            return ItemTraceAttachErrorCode.NO_LINKED_ITEM;
        } else {
            ItemTraceIdData traceIdData = new ItemTraceIdData(itemTrace);
            if (!traceIdData.isValid()) {
                return ItemTraceAttachErrorCode.INVALID_TRACE_ID;
            }
            ItemStockKey currentOption = new ItemStockKey(this.getItemId(), this.getColor(), this.getSize());
            if (currentOption != traceIdData.getItemOption()) {
                return ItemTraceAttachErrorCode.OPTION_MISMATCH;
            }
            Set<String> currentTraces = this.getItemTraces();
            if (currentTraces.contains(itemTrace)) {
                return ItemTraceAttachErrorCode.ALREADY_ATTACHED;
            }

            try {
                // 매번 JSONArray를 다시 구성하는 것은 비효율적이나,
                // 한 거래항목에 QR 코드가 많이 첨부되지 않는다는 것을 가정하여 재구성 (내부 데이터 구조와 독립적으로 메서드 구현)
                JSONArray itemTraceArr = new JSONArray();
                for (String currentTrace: currentTraces) {
                    itemTraceArr.put(currentTrace);
                }
                this.data.put("tk", itemTraceArr);
                return ItemTraceAttachErrorCode.OK;
            } catch (JSONException e) {
                Log.i("DEBUG_TAG", e.getMessage());
                return ItemTraceAttachErrorCode.MISC;
            }
        }
    }

    @Nullable
    @Override
    public Integer getPriceDelta() {
        try {
            int priceDelta = data.getInt("pd");
            return Integer.valueOf(priceDelta);
        } catch (JSONException e) {
            return null;
        }
    }

    @Override
    public void setPriceDelta(int itemReferencePrice) {
        try {
            int priceDelta = this.getUnitPrice() - itemReferencePrice;
            this.data.put("pd", priceDelta);
        } catch (JSONException e) {
            Log.i("DEBUG_TAG", e.getMessage());
        }
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return data;
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("SlipItemJSONData instance does not have a JSON array");
    }

    @Override
    public String toString() {
        return "SLIP ITEM[" + getItemType() + "] " + getItemName() + "/" + getColor().getOriginalUiName()
                + "/" + getSize().toRemoteStr() + ": " + getUnitPrice() + " x " + getQuantity();
    }

    @Override
    public String toInputModelString() {
        return data.toString();
    }

    public static JSONArray toJSONArray(List<SlipItemDataProtocol> items) throws JSONException {
        JSONArray itemsArr = new JSONArray();
        for (SlipItemDataProtocol item: items) {
            itemsArr.put(new SlipItemJSONData(item).getObject());
        }
        return itemsArr;
    }

    /**
     * 서버로 업데이트된 아이템을 전송하기 전에 수정일자 정보를 작성하기 위해 사용한다.
     * 참고: 서비스 메서드 내부에서 호출하기 위한 용도. 서비스 메서드 외부에서 사용되지 말아야 한다.
     * @throws JSONException
     */
    public void setUpdatedDate() throws JSONException {
        String updatedDateStr = updatedDateFormatter.print(DateTime.now());
        data.put("u", updatedDateStr);
    }
}
