package com.teampls.shoplus.lib.datatypes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 서비스(라이선스) 활성화 시도에 따른 결과를 반환하기 위한 데이터 클래스.
 * 사용자에 의해 활성화되는 경우에 쓰인다(트라이얼 또는 리딤코드에 의한 활성화 시도).
 *
 * @author lucidite
 */

public class ServiceActivationResult {
    private Boolean success;
    private ServiceActivationError error;

    public ServiceActivationResult(JSONObject dataObj) throws JSONException {
        this.success = dataObj.getBoolean("activated");
        String errorCode = dataObj.optString("error_code", "");
        this.error = ServiceActivationError.fromRemoteStr(errorCode);
    }

    public Boolean isSuccess() {
        return success;
    }

    public ServiceActivationError getError() {
        return error;
    }

    public enum ServiceActivationError {
        NONE("", ""),
        INVALID_CODE("InvalidCode", "올바르지 않은 이용권 키 입니다."),
        REDEEM_CODE_APPLIED_USER("RedeemCodeAppliedUser", "이미 이용권을 사용하셨습니다."),
        ALREADY_USED_OR_ACTIVATED("AlreadyUsedOrActivated", "이용권이 만료되었습니다."),
        INVALID_INTRODUCER("InvalidIntroducer", "추천인 정보가 올바르지 않습니다.");

        private String remoteStr;
        private String uiStr;

        ServiceActivationError(String remoteStr, String uiStr) {
            this.remoteStr = remoteStr;
            this.uiStr = uiStr;
        }

        public static ServiceActivationError fromRemoteStr(String remoteStr) {
            if (remoteStr == null) {
                return NONE;
            }
            for (ServiceActivationError errorCode : ServiceActivationError.values()) {
                if (errorCode.remoteStr.equals(remoteStr)) {
                    return errorCode;
                }
            }
            return NONE;
        }

        public String getUiStr() {
            return uiStr;
        }

    }
}
