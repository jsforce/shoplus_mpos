package com.teampls.shoplus.lib.common;

import android.support.annotation.IdRes;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-05-26.
 */

public class MyContainers<T> {
    private List<Container<T>> containers = new ArrayList<>();
    private T currentEnum;

    public MyContainers add(View view, T enumValue, @IdRes int resId, boolean valid) {
        containers.add(new Container<T>(view, enumValue, resId, valid));
        return this;
    }

    public T getCurrentEnum() {
        return currentEnum;
    }

    public List<T> getValidEnums() {
        List<T> results = new ArrayList<>();
        for (Container<T> record : containers)
            if (record.valid)
                results.add(record.enumValue);
        return results;
    }

    public void setCurrentEnum(T enumValue) {
        currentEnum = enumValue;
        refreshLayout(currentEnum);
    }

    private void refreshLayout(T enumValue) {
        for (Container container : containers)
            container.setVisibility(container.enumValue == enumValue);
    }

    public void prev(MyOnTask onStart) {
        List<T> validEnums = getValidEnums();
        if (validEnums.size() == 0) return;
        int index = validEnums.indexOf(currentEnum);
        if (index == 0) {
            if (onStart != null)
                onStart.onTaskDone("");
        } else {
            currentEnum = validEnums.get(index - 1);
            refreshLayout(currentEnum);
        }
    }

    public void next(MyOnClick onClick) {
        List<T> validEnums = getValidEnums();
        if (validEnums.size() == 0) return;
        int index = validEnums.indexOf(currentEnum);
        if (onClick != null)
            onClick.onMyClick(null, "");
    }

    public void next(MyOnTask onEnd) {
        List<T> validEnums = getValidEnums();
        if (validEnums.size() == 0) return;
        int index = validEnums.indexOf(currentEnum);

        if (index == validEnums.size() - 1) {
            if (onEnd != null)
                onEnd.onTaskDone("");
        } else {
            currentEnum = validEnums.get(index + 1);
            refreshLayout(currentEnum);
        }
    }

    public class Container<T> {
        public T enumValue;
        public int resId = 0;
        public boolean valid = true;
        private LinearLayout container;

        public Container(View view, T enumValue, @IdRes int resId, boolean valid) {
            this.enumValue = enumValue;
            this.resId = resId;
            this.valid = valid;
            container = (LinearLayout) view.findViewById(resId);
        }

        public void setVisibility(boolean visible) {
            container.setVisibility(visible ? View.VISIBLE : View.GONE);
        }

    }
}