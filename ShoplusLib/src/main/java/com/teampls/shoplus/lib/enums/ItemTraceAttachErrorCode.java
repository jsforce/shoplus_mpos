package com.teampls.shoplus.lib.enums;

/**
 * QR코드를 이용한 거래기록 작성에서 발생할 수 있는 에러 코드
 *
 * @author lucidite
 */

public enum ItemTraceAttachErrorCode {
    OK(""),
    INVALID_TRACE_ID("올바르지 않은 QR코드입니다"),
    UNAVAILABLE_SLIP_ITEM_TYPE("실제 출고되는 거래항목 유형이 아닙니다"),
    NO_LINKED_ITEM("상품이 연결되지 않은 거래항목입니다"),
    OPTION_MISMATCH("컬러 또는 사이즈가 일치하지 않습니다"),
    ALREADY_ATTACHED("이미 추가된 QR코드입니다"),
    MISC("QR코드 연결 오류입니다");

    private String uiStr;

    ItemTraceAttachErrorCode(String uiStr) {
        this.uiStr = uiStr;
    }
}
