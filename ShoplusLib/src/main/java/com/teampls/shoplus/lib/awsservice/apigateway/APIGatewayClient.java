package com.teampls.shoplus.lib.awsservice.apigateway;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.external_api.PapagoTranslationAPIRequester;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class APIGatewayClient {
    /**
     * GET 요청을 API Gateway로 전송한다.
     *
     * @param resourceURL   API Gateway의 자원 경로
     * @param params    GET parameter, parameter name-value set의 mapping data
     * @param identityToken User Pools의 id token
     * @return
     * @throws MyServiceFailureException    200 이외의 응답 코드를 수신한 경우
     */
    public static String requestGET(String resourceURL, Map<String, String> params, String identityToken) throws MyServiceFailureException {
        // TODO @Sunstrider parameter escaping
        if ((params != null) && (params.size() > 0)) {
            resourceURL += "?";
            int size = params.size();
            for (Map.Entry<String, String> param : params.entrySet()) {
                resourceURL += (param.getKey() + "=" + param.getValue());
                size--;
                if (size > 0) {
                    resourceURL += "&";
                }
            }
        }
        return request(resourceURL, "GET", null, identityToken, MyAWSConfigs.getCurrentConfig().getApigatewayApiKey());
    }

    /**
     * POST 요청을 API Gateway로 전송한다 (application/json).
     *
     * @param resourceURL   API Gateway의 자원 경로
     * @param body  Request Body, JSON String 포맷을 가져야 한다.
     * @param identityToken User Pools의 id token
     * @return
     * @throws MyServiceFailureException    200 이외의 응답 코드를 수신한 경우
     */
    public static String requestPOST(String resourceURL, String body, String identityToken) throws MyServiceFailureException {
        return request(resourceURL, "POST", body, identityToken, MyAWSConfigs.getCurrentConfig().getApigatewayApiKey());
    }

    /**
     * PUT 요청을 API Gateway로 전송한다 (application/json).
     *
     * @param resourceURL   API Gateway의 자원 경로
     * @param body  Request Body, JSON String 포맷을 가져야 한다.
     * @param identityToken User Pools의 id token
     * @return
     * @throws MyServiceFailureException    200 이외의 응답 코드를 수신한 경우
     */
    public static String requestPUT(String resourceURL, String body, String identityToken) throws MyServiceFailureException {
        return request(resourceURL, "PUT", body, identityToken, MyAWSConfigs.getCurrentConfig().getApigatewayApiKey());
    }

    /**
     * DELETE 요청을 API Gateway로 전송한다 (application/json).
     *
     * @param resourceURL   API Gateway의 자원 경로
     * @param params    GET parameter, parameter name-value set의 mapping data
     * @param identityToken User Pools의 id token
     * @return
     * @throws MyServiceFailureException    200 이외의 응답 코드를 수신한 경우
     */
    public static String requestDELETE(String resourceURL, Map<String, String> params, String identityToken) throws MyServiceFailureException {
        // TODO @Sunstrider parameter escaping
        if ((params != null) && (params.size() > 0)) {
            resourceURL += "?";
            int size = params.size();
            for (Map.Entry<String, String> param : params.entrySet()) {
                resourceURL += (param.getKey() + "=" + param.getValue());
                size--;
                if (size > 0) {
                    resourceURL += "&";
                }
            }
        }
        return request(resourceURL, "DELETE", null, identityToken, MyAWSConfigs.getCurrentConfig().getApigatewayApiKey());
    }

    private static String request(String resourceURL, String method, String body, String identityToken, String apiKey) throws MyServiceFailureException {
        try {
            URL url = new URL(resourceURL);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setConnectTimeout(5000);
            conn.setReadTimeout(15000);
            conn.setRequestMethod(method);
            conn.addRequestProperty("Content-Type", "application/json");
            conn.addRequestProperty("Authorization", identityToken);
            if (apiKey != null)
                conn.addRequestProperty("x-api-key", apiKey);
            if (body == null) body = "{}";     // default json string

            // [DALAR-46] 현재 POST와 PUT만 사용한다.
            // PATCH도 사용했으나 안드로이드 구버전과의 호환성 문제(API 19)로 PUT으로 대체한다.
            String[] outputMethodList = {"POST", "PATCH", "PUT"};
            for (String m : outputMethodList) {
                if (method.toUpperCase().equals(m)) {
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    OutputStream os = conn.getOutputStream();

                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(body);
                    writer.flush();
                    writer.close();
                    os.close();
                    break;
                }
            }

            int responseCode = conn.getResponseCode();
            InputStream inputStream = conn.getInputStream();
            String response =  APIGatewayHelper.getStringFromInputStream(inputStream);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                return response;
            } else {
                String errorMessage = new JSONObject(response).getString("message");
                throw new MyServiceFailureException("HttpError[" + responseCode + "]", errorMessage);
            }
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        } catch (IOException e) {
            Log.e("DEBUG_TAG", "[ERROR] 통신 관련 입출력 오류: " + e.getClass().getSimpleName());
            // e.printStackTrace();
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        } catch (Exception e) {
            Log.e("DEBUG_TAG", "[ERROR] 통신 관련 입출력 오류 외 기타 오류: " + e.getClass().getSimpleName());
            // e.printStackTrace();
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }
}
