package com.teampls.shoplus.lib.database_old;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.enums.SlipGenerationType;

import java.util.List;

/**
 * Created by Medivh on 2016-09-07.
 */
public class OldSlipDB extends BaseDB<OldSlipRecord> {
    private static OldSlipDB instance = null;
    protected static int Ver = 6;

    public enum Column {
        _id, ownerPhoneNumber, counterpartPhoneNumber, createdDateTime, salesDateTime, confirmed,
        onlinePayment, onlinePaid, onlinePaidDateStr, entrustPayment, entrustPaid, entrustPaidDateStr, generation,
        completed, cancelled, cancelledDateTime;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    protected OldSlipDB(Context context) {
        super(context, "SlipDB", "SlipDBTable", Ver, Column.toStrs());
    }

    protected OldSlipDB(Context context, String DBName, int DBversion, String[] columns) {
        super(context, DBName, DBversion, columns);
    }

    public static OldSlipDB getInstance(Context context) {
        if (instance == null)
            instance = new OldSlipDB(context);
        return instance;
    }

    protected ContentValues toContentvalues(OldSlipRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.ownerPhoneNumber.toString(), record.ownerPhoneNumber);
        contentvalues.put(Column.counterpartPhoneNumber.toString(), record.counterpartPhoneNumber);
        contentvalues.put(Column.createdDateTime.toString(), record.createdDateTime.toString());
        contentvalues.put(Column.salesDateTime.toString(), record.salesDateTime.toString());
        contentvalues.put(Column.confirmed.toString(), record.confirmed);
        contentvalues.put(Column.onlinePayment.toString(), record.onlinePayment);
        contentvalues.put(Column.onlinePaid.toString(), record.onlinePaid);
        contentvalues.put(Column.onlinePaidDateStr.toString(), record.onlinePaidDateStr);
        contentvalues.put(Column.entrustPayment.toString(), record.entrustPayment);
        contentvalues.put(Column.entrustPaid.toString(), record.entrustPaid);
        contentvalues.put(Column.entrustPaidDateStr.toString(), record.entrustPaidDateStr);
        contentvalues.put(Column.generation.toString(), record.generation.toString());
        contentvalues.put(Column.completed.toString(), record.completed);
        contentvalues.put(Column.cancelled.toString(), record.cancelled);
        contentvalues.put(Column.cancelledDateTime.toString(), record.cancelledDateTime.toString());
        return contentvalues;
    }

    @Override
    protected void toLogCat(int id, OldSlipRecord record, int option, String operation) {
        if (option == 0 || option == 1) {
            Log.i("DEBUG_JS", String.format("[%s] <%d> own %s, counter %s, created %s, sales %s",
                    operation, id,
                    record.ownerPhoneNumber,
                    record.counterpartPhoneNumber,
                    record.createdDateTime.toString(BaseUtils.fullFormat),
                    record.salesDateTime.toString(BaseUtils.fullFormat)
            ));
        }
    }

    public void toLogCat(String location) {
        for (OldSlipRecord record : getRecords())
            record.toLogCat(location);
    }

    public List<OldSlipRecord> getRecords() {
        Cursor cursor = getCursorOrderBy(Column.salesDateTime, true);
        List<OldSlipRecord> results = getRecords(cursor);
        cursor.close();
        return results;
    }

    @Override
    protected int getId(OldSlipRecord record) {
        return record.id;
    }

    @Override
    protected OldSlipRecord getEmptyRecord() {
        return new OldSlipRecord();
    }

    @Override
    protected OldSlipRecord createRecord(Cursor cursor) {
        return new OldSlipRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.ownerPhoneNumber.ordinal()),
                cursor.getString(Column.counterpartPhoneNumber.ordinal()),
                BaseUtils.toDateTime(cursor.getString(Column.createdDateTime.ordinal())),
                BaseUtils.toDateTime(cursor.getString(Column.salesDateTime.ordinal())),
                BaseUtils.toBoolean(cursor.getInt(Column.confirmed.ordinal())),
                cursor.getInt(Column.onlinePayment.ordinal()),
                BaseUtils.toBoolean(cursor.getInt(Column.onlinePaid.ordinal())),
                cursor.getString(Column.onlinePaidDateStr.ordinal()),
                cursor.getInt(Column.entrustPayment.ordinal()),
                BaseUtils.toBoolean(cursor.getInt(Column.entrustPaid.ordinal())),
                cursor.getString(Column.entrustPaidDateStr.ordinal()),
                SlipGenerationType.valueOf(cursor.getString(Column.generation.ordinal())),
                BaseUtils.toBoolean(cursor.getInt(Column.completed.ordinal())),
                BaseUtils.toBoolean(cursor.getInt(Column.cancelled.ordinal())),
                BaseUtils.toDateTime(cursor.getString(Column.cancelledDateTime.ordinal())),
                false
        );
    }

    public MyMap<String, OldSlipRecord> getMap() {
        MyMap<String, OldSlipRecord> results = new MyMap<>(getEmptyRecord());
        for (OldSlipRecord record : getRecords())
            results.put(record.getSlipKey().toSID(), record);
        return results;
    }

}
