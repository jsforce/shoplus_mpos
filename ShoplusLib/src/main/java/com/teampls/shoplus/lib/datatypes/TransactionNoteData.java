package com.teampls.shoplus.lib.datatypes;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * [SILVER-499] 거래기록에 첨부된 개별 노트 데이터
 * [NOTE] 한 거래기록에 여러 개의 노트 데이터를 첨부할 수 있다.
 *
 * @author lucidite
 */
public class TransactionNoteData implements Comparable<TransactionNoteData> {
    private DateTime createdDateTime;
    private String noteText;
    private String creator;

    private static long BASE_TIMESTAMP = 1546268400;
    private static String NOTE_KEY_PREFIX = "n";

    public TransactionNoteData(String noteKeyStr, String noteText) {
        String[] keyElems = noteKeyStr.split("-", 2);
        String noteKey = keyElems[0].replace(NOTE_KEY_PREFIX, "");
        long timestampInSecond = BASE_TIMESTAMP + (Long.valueOf(noteKey) / 100);
        this.createdDateTime = new DateTime(timestampInSecond * 1000);
        this.noteText = noteText;
        if (keyElems.length >= 2) {
            this.creator = keyElems[1];
        } else {
            this.creator = "";
        }
    }

    /**
     * 노트 작성 시간을 조회한다.
     * @return
     */
    public DateTime getCreatedDateTime() {
        return createdDateTime;
    }

    /**
     * 노트 내용을 조회한다.
     * @return
     */
    public String getNoteText() {
        return noteText;
    }

    /**
     * 노트 작성자 정보를 조회한다. 매장과 동일한 경우에는 빈 문자열을 반환한다.
     * [NOTE] 사장 기능을 사용하고 있는 경우에만 실질적인 값을 가질 수 있다.
     *
     * @return 작성자가 매장 번호와 다른 경우(종업원/사장) 작성자의 ID(휴대전화번호)
     */
    public String getCreator() {
        return creator;
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // Comparable

    @Override
    public int compareTo(@NonNull TransactionNoteData o) {
        return this.createdDateTime.compareTo(o.createdDateTime);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // Service Response Processing

    public static List<TransactionNoteData> buildFromResponse(JSONObject response) {
        List<TransactionNoteData> result = new ArrayList<>();
        try {
            JSONObject noteDataObj = response.getJSONObject("note");
            Iterator<String> iter = noteDataObj.keys();
            while (iter.hasNext()) {
                String noteKey = iter.next();
                String noteText = noteDataObj.optString(noteKey, "");
                if (!noteText.isEmpty()) {
                    result.add(new TransactionNoteData(noteKey, noteText));
                }
            }
            Collections.sort(result);
            return result;
        } catch (JSONException e) {
            return result;
        }
    }
}
