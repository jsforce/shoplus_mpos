package com.teampls.shoplus.lib.common;

/**
 * Created by Medivh on 2016-04-18.
 */
public class BaseKey {
    public static final String appAlive = "appAlive";
    public static final String appForeground = "appForeground";
    public static final String versionCode = "versionCode";
    public static final String isVersionUp = "isVersionUp";
    public static final String isAdmin = "isAdmin";
    public static final String loginTime = "loginTime";
    public static final String recentImageFolder = "recentImageFolder";
    public static final String loginFailed = "loginFailed";
    public static final String isProduction = "isProduction";
    public static final String recentItems = "recentItems";
    //public static final String itemCategory = "items.category";


    // Admin
    public static final String  latestsellerPhoneNumber = "latestsellerPhoneNumber";

    public static String updated(String userEmail) {
        return String.format("updated%s", userEmail);
    }
}
