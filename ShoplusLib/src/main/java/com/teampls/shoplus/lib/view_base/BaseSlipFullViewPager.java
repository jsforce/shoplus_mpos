package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.MyUITask;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.SlipDB;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_memory.DbType;
import com.teampls.shoplus.lib.database_memory.MemorySlipDB;
import com.teampls.shoplus.lib.dialog.MyGuideDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-04-05.
 */

abstract public class BaseSlipFullViewPager<T> extends BaseActivity {
    protected Context context = BaseSlipFullViewPager.this;
    protected ViewPager viewPager;
    protected BaseFullViewAdapter<T> viewAdapter;
    protected static int currentPosition = 0;
    protected static List<SlipRecord> records = new ArrayList<>(0);
    private TextView tvPosition;
    protected ItemDB itemDB;
    protected static SlipDB slipDB;
    protected static MemorySlipDB mSlipDB;
    protected static DbType dbType = DbType.SQLite;

    @Override
    public int getThisView() {
        return R.layout.base_itemfullview_frame;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        setCurrentPosition(currentPosition);
        itemDB = ItemDB.getInstance(context);
        setSlipDB();
        setViewAdapter();
        viewPager = (ViewPager) findViewById(R.id.itemfullview_pager);
        viewPager.setAdapter(viewAdapter);
        viewPager.setCurrentItem(getCurrentPosition()); // startActivity에서 인자로 받음
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setCurrentPosition(position);
                refreshPosition();
                myActionBar.setTitle(viewAdapter.getFullView(position).getTitle());
            }
        });
        if (viewAdapter.hasFullView(currentPosition))
            myActionBar.setTitle(viewAdapter.getFullView(currentPosition).getTitle());
        tvPosition = findTextViewById(R.id.itemfullview_position, "");
        refreshPosition();
        new MyDelayTask(context, 500) {
            @Override
            public void onFinish() {
                myActionBar.setTitle(viewAdapter.getFullView(getCurrentPosition()).getTitle());
            }
        };
        myActionBar.setTitleOnClick(new ViewPager.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myActionBar.getTitle().getText().toString().isEmpty() == false)
                    new MyGuideDialog(context, "상세보기", myActionBar.getTitle().getText().toString()).show();
            }
        });
    }

    public void setSlipDB() {
        slipDB = SlipDB.getInstance(context);
    }

    protected void setCurrentPosition(int position) {
        currentPosition = position;
    }

    protected int getCurrentPosition() {
        return currentPosition;
    }

    abstract protected void setViewAdapter();

    public void removeView(final int position) {
        if (viewAdapter.getCount() <= 0)
            return;

        new MyUITask(context) {
            @Override
            public void doInBackground() {
                boolean atEnd = (position == viewAdapter.getCount() - 1);
                viewAdapter.records.remove(position); // 전체 개수 감소
                viewAdapter.notifyDataSetChanged();
                setCurrentPosition(atEnd ? position - 1 : position);
                refreshPosition();
            }
        };
    }

    private void refreshPosition() {
        MyUI.setText(context, tvPosition, String.format("%d/%d", getCurrentPosition() + 1, viewAdapter.getCount()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("DEBUG_JS", String.format("[BaseSlipFullViewPager.onResume] ....."));
        //viewAdapter.refreshDay();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case BaseGlobal.RequestRefresh:
                viewAdapter.notifyDataSetChanged();
                break;
        }
    }

    abstract public void onFinish();

    @Override
    public void onBackPressed() {
        onFinish();
    }


    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case close:
                onFinish();
                break;
        }
    }

}
