package com.teampls.shoplus.lib.database_global;

import android.content.Context;

import com.teampls.shoplus.lib.database_map.KeyValueBoolean;
import com.teampls.shoplus.lib.database_map.KeyValueDB;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-10-01.
 */
public class AddressbookDB extends UserDB {
    private static int Ver = 1;
    private static AddressbookDB instance = null;

    // 1 : App folder내 저장
    // 2 : common folder내 저장

    public static AddressbookDB getInstance(Context context) {
        if (instance == null)
            instance = new AddressbookDB(context);
        return instance;
    }

    private AddressbookDB(Context context) {
        super(context, "NewAddressbookDB", Ver, UserDB.Column.toStrs());
    }

    public List<UserRecord> getRecordsExcept(List<String> ignorePhoneNumbers) {
        List<UserRecord> results = new ArrayList<>();
        for (UserRecord record : getRecords()) {
            if (ignorePhoneNumbers.contains(record.phoneNumber))
                continue;
            results.add(record);
        }
        return results;
    }

    public DBHelper getDBHelper() {
        return new DBHelper(context, getTeamPlPath());
    }

    public void setChecked(boolean value) {
        KeyValueDB.getInstance(context).put("addressbookDB.isChecked", value);
    }

    public boolean isChecked() {
        return KeyValueDB.getInstance(context).getBool("addressbookDB.isChecked");
    }

}
