package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.database.QueryOrderRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.event.MyOnClick;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

public abstract class BaseDBAdapter<T> extends BaseAdapter {
    protected Context context;
    protected LayoutInflater inflater = null;
    protected List<T> records = new ArrayList<>(0), generatedRecords = new ArrayList<>();
    private BaseViewHolder viewHolder;
    protected int viewType = 0;
    public List<Integer> clickedPositions = new ArrayList<>(0); // resetClickedPosition on list
    protected boolean doSkipGenerate = false, doSortByKey = false;
    public static final int CHOICE_MODE_NONE = 0, CHOICE_MODE_SINGLE = 1, CHOICE_MODE_MULTIPLE = 2,
            CHOICE_MODE_ORDERED_MULTIPLE = 3;
    protected int choiceMode = CHOICE_MODE_NONE;
    protected MyOnClick<T> onMoreClick;
    public static String KEY_MORE = "<이전자료 더보기>";
    protected MyDB myDB;
    protected GlobalDB globalDB;
    protected UserDB userDB;
    protected int scrollState = SCROLL_STATE_IDLE;
    protected Map<Integer, BaseViewHolder> viewHolderMap = new HashMap<>();
    public int firstVisibleItem = 0;
    protected List<QueryOrderRecord> orderRecords = new ArrayList<>();
    protected UserSettingData userSettingData;
    public MSortingKey sortingKey = MSortingKey.Date;

    protected enum Mode {FileDB, MemoryDB}

    protected Mode mode = Mode.FileDB;
    protected boolean doSetRecordWhenGenerated = true, doFilterDateTime = false;
    protected DateTime fromDateTime = Empty.dateTime, toDateTime = Empty.dateTime;

    public BaseDBAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        myDB = MyDB.getInstance(context);
        userDB = UserDB.getInstance(context);
        globalDB = GlobalDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
    }

    public BaseDBAdapter setViewType(int viewType) {
        this.viewType = viewType;
        return this;
    }

    public void setFilterPeriod(boolean value) {
        doFilterDateTime = value;
    }

    public BaseDBAdapter filterPeriod(DateTime fromDateTime, DateTime toDateTime) {
        doFilterDateTime = true;
        this.fromDateTime = fromDateTime;
        this.toDateTime = toDateTime;
        return this;
    }

    public BaseDBAdapter setOrderRecords(List<QueryOrderRecord> orderRecords) {
        this.orderRecords = orderRecords;
        return this;
    }

    public BaseDBAdapter setOrderRecord(Enum<?> column, boolean isDescending) {
        this.orderRecords = new QueryOrderRecord(column, false, isDescending).toList();
        return this;
    }

    public BaseDBAdapter setOrderRecord(Enum<?> column, boolean isInteger, boolean isDescending) {
        this.orderRecords = new QueryOrderRecord(column, isInteger, isDescending).toList();
        return this;
    }

    public BaseDBAdapter setSortingKey(MSortingKey sortingKey) {
        doSortByKey = true;
        this.sortingKey = sortingKey;
        return this;
    }

    public void toLogCatOrdering(String callLocation) {
        for (QueryOrderRecord orderRecord : orderRecords)
            orderRecord.toLogCat(callLocation);
    }

    public void setScrollState(int scollState) {
        this.scrollState = scollState;
//        if (scollState != SCROLL_STATE_IDLE )
//           setViewHolder0Creation(false);
    }

    public BaseDBAdapter addRecord(T record) {
        records.add(record);
        return this;
    }

    public BaseDBAdapter addRecords(int position, List<T> records) {
        records.addAll(position, records);
        return this;
    }

    private boolean onGenerating = false;

    public void generate(boolean doSetRecordWhenGenerate) {
        if (onGenerating) {
            Log.w("DEBUG_JS", String.format("[%s.generate] called doubly", getClass().getSimpleName()));
        }
        onGenerating = true;

        this.doSetRecordWhenGenerated = doSetRecordWhenGenerate;
        generate();

        onGenerating = false;
    }

    public void applyGeneratedRecords() {
        if (onGenerating) {
            Log.w("DEBUG_JS", String.format("[%s.applyGeneratedRecords] called doubly", getClass().getSimpleName()));
        }

        List<T> _records = new ArrayList<>();
        for (T record : generatedRecords)
            _records.add(record);
        records = _records;
    }

    public void addRecord(int position, T record) {
        records.add(position, record);
    }

    public void removeRecord(int position) {
        if (position >= records.size() || position < 0) return;
        records.remove(position);
    }

    public void setRecord(int position, T record) {
        if (position >= records.size() || position < 0) return;
        records.set(position, record);
    }

    public void clear() {
        generatedRecords.clear();
        records.clear();
        clickedPositions.clear();
    }

    public BaseDBAdapter clearClicked() {
        clickedPositions.clear();
        return this;
    }

    public BaseDBAdapter addMoreRecord(T record, MyOnClick<T> onMoreClick) {
        records.add(record);
        this.onMoreClick = onMoreClick;
        return this;
    }

    public List<T> getSortedClickedRecords() {
        List<T> results = new ArrayList<>();
        List<Integer> sortedPositions = new ArrayList(clickedPositions);
        Collections.sort(sortedPositions);
        for (int position : sortedPositions)
            results.add(getRecord(position));
        return results;
    }

    public List<T> getClickedRecords() {
        List<T> results = new ArrayList<>();
        for (int position : clickedPositions)
            results.add(getRecord(position));
        return results;
    }

    public BaseDBAdapter setChoiceMode(int choiceMode) {
        this.choiceMode = choiceMode;
        return this;
    }

    public int getChoiceMode() {
        return choiceMode;
    }

    public void setClickedPosition(int position) {
        onItemClick(null, position);
    }

    public void setAllClicked() {
        clickedPositions.clear();
        int position = 0;
        int clickedPositionCount = 0;
        for (T record : records) {
            clickedPositions.add(position);
            position++;
            clickedPositionCount++;
            if (doCheckClickedPositionLimit && clickedPositionCount >= clickedPositionsLimit) {
                MyUI.toastSHORT(context, String.format("최대 %d개", clickedPositionsLimit));
                break;
            }
        }
    }

    protected int clickedPositionsLimit = Integer.MAX_VALUE;
    protected boolean doCheckClickedPositionLimit = false;

    public void setClickedPositionsLimit(int value) {
        doCheckClickedPositionLimit = true;
        this.clickedPositionsLimit = value;
    }

    public void onItemClick(View view, int position) {
        if (position >= records.size() || position < 0) return;
        switch (choiceMode) {
            case CHOICE_MODE_NONE:
                if (clickedPositions.contains(position) == false) // 중복방지
                    clickedPositions.add(position);
                if (view != null)
                    updateCell(view, position);
                break;
            case CHOICE_MODE_SINGLE:
                clickedPositions.clear();
                clickedPositions.add(position);
                notifyDataSetChanged();
                break;
            case CHOICE_MODE_MULTIPLE:
                setClickedPositionOnMultipleMode(position);
                if (view != null)
                    updateCell(view, position);
                break;
            case CHOICE_MODE_ORDERED_MULTIPLE:
                boolean isChecked = setClickedPositionOnMultipleMode(position); // 기존에 체크?
                if (isChecked)
                    notifyDataSetChanged(); // 삭제가 됬으므로 전부 다시
                else if (view != null)
                    updateCell(view, position);
                break;
        }
    }

    private boolean setClickedPositionOnMultipleMode(int position) {
        if (clickedPositions.contains(position)) {
            clickedPositions.remove((Integer) position);
            return true;
        } else {
            // new item
            if (doCheckClickedPositionLimit && clickedPositions.size() >= clickedPositionsLimit) {
                MyUI.toastSHORT(context, String.format("최대 %d개", clickedPositionsLimit));
            } else {
                clickedPositions.add(position);
            }
            return false;
        }
    }

    public void updateCell(View cellView, int position) {
        BaseViewHolder viewHolder = (BaseViewHolder) cellView.getTag();
        if (viewHolder != null)
            viewHolder.update(position);
    }

    public void updateCell(int position) {
        if (getCount() == 0)
            return;
        BaseViewHolder viewHolder = viewHolderMap.get(position);
        if (viewHolder != null) {
            viewHolder.update(position);
        } else {
            Log.w("DEBUG_JS", String.format("[BaseDBAdapter.updateCell] viewHolder %d == null", position));
        }
    }

    public BaseDBAdapter setRecords(List<T> records) {
        doSkipGenerate = true;
        setGeneratedRecords(records); // 자동으로 해줘야
        this.records = records;
        return this;
    }

    public BaseDBAdapter setGeneratedRecords(List<T> records) {
        doSkipGenerate = true;
        this.generatedRecords = records;
        return this;
    }

    public BaseDBAdapter resetRecords() {
        doSkipGenerate = false;
        generatedRecords.clear();
        records.clear();
        return this;
    }

    public boolean isSkipGeneration() {
        return doSkipGenerate;
    }

    public List<Integer> getClickedPositions() {
        return new ArrayList<>(clickedPositions);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return records.size();
    }

    @Override
    public Object getItem(int position) {
        return records.get(position);
    }

    public T getRecord(int position) {
        return (T) getItem(position);
    }

    public List<T> getRecords() {
        return records;
    }

    public List<T> getGeneratedRecords() {
        return generatedRecords;
    }

//    public List<T> getRecordsBy(int position, int Limit) {
//        if (records.isEmpty()) return records;
//        int positionMax = getCount()-1;
//        return records.subList(position, Math.min(position+Limit, getCount()));
//    }

    abstract public void generate();

    public abstract BaseViewHolder getViewHolder();

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            viewHolder = getViewHolder();
            view = inflater.inflate(viewHolder.getThisView(), null);
            viewHolder.init(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (BaseViewHolder) view.getTag();
        }
        viewHolder.update(position);
        viewHolderMap.put(position, viewHolder);
        return view;
    }

    protected void setTextTitle(TextView textView, boolean doShow, String title) {
        if (doShow) {
            textView.setVisibility(title.isEmpty() || title.equals("null") ? View.GONE : View.VISIBLE);
            textView.setText(title);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    public class StringViewHolder extends BaseViewHolder {
        private TextView tvString;

        @Override
        public int getThisView() {
            return R.layout.row_string;
        }

        @Override
        public void init(View view) {
            tvString = (TextView) view.findViewById(R.id.row_string_textview);
        }

        @Override
        public void update(int position) {
            tvString.setText(String.format("%s", getRecord(position)));
        }
    }

    private class EmptyViewHolder extends BaseViewHolder {

        @Override
        public int getThisView() {
            return 0;
        }

        @Override
        public void init(View view) {

        }

        @Override
        public void update(int position) {

        }
    }

}
