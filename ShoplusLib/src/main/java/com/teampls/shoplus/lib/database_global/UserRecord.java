package com.teampls.shoplus.lib.database_global;


import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.BaseRecord;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserRecord implements ContactDataProtocol, Comparable<UserRecord> {
    public int id = 0;
    public String phoneNumber = "", name = "";
    public ContactBuyerType priceType = ContactBuyerType.WHOLESALE;
    public String location = "";
    public boolean deleted = false;
    public long timeStamp = 0;
    private boolean privatePriceEnabled = false;

    public UserRecord() {
    }

    // UserRecord record = (UserRecord) protocol 의 강제 캐스팅 방식은 위험
    public UserRecord(ContactDataProtocol protocol) {
        this(0, protocol.getContactNumber(), protocol.getName(), protocol.getBuyerType(), protocol.getLocation(), protocol.isDeleted(), protocol.getUpdatedTimeStamp());
    }

    public UserRecord(int id, String phoneNumber, String name, ContactBuyerType priceType, String location, boolean isDeleted, long timeStamp) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.priceType = priceType;
        this.location = location;
        this.deleted = isDeleted;
        this.timeStamp = timeStamp;
    }

    public UserRecord(String name, String phoneNumber) {
        this.name = (name == null) ? "" : name;
        this.phoneNumber = (phoneNumber == null) ? "" : phoneNumber;
        if (phoneNumber.isEmpty() == false && BaseUtils.isInteger(phoneNumber) == false && BaseUtils.isDateTime(phoneNumber) == false)
            Log.w("DEBUG_JS", String.format("[LocalUserRecord.LocalUserRecord] check phoneNumber : %s (%s)", phoneNumber, name));
    }

    public UserRecord(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isSame(UserRecord record) {
        return BaseRecord.isSame(this, record);
    }

    @Override
    public String getContactNumber() {
        return phoneNumber;
    }

    @Override
    public String getName() {
        String result = name;
        if (result.isEmpty())
            result = BaseUtils.toPhoneFormat(phoneNumber);
        else if (name.startsWith("*"))
            result = result.substring(1, result.length());
        return result;
    }

    @Override
    public ContactBuyerType getBuyerType() {
        return priceType;
    }

    @Override
    public Long getUpdatedTimeStamp() {
        return timeStamp;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public boolean isPriceDiscriminated() {
        return privatePriceEnabled;
    }

    public String getNameWithRetail() {
        String result = getName();
        if (priceType == ContactBuyerType.RETAIL)
            result = result + " (소매)";
        return result;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] <%d> %s, %s, %s, deleted? %s, %d",
                location, id, phoneNumber, name, priceType.toString(), deleted,  timeStamp));
    }

    public boolean isEmpty() {
        return (name.isEmpty() && phoneNumber.isEmpty());
    }

    @Override
    public int compareTo(@NonNull UserRecord userRecord) {
        return this.name.compareTo(userRecord.name);
    }

    public static UserRecord createTemp() {
        return new UserRecord("미입력", DateTime.now().toString(BaseUtils.fullFormat));
    }

    public static boolean isTemp(String phoneNumber) {
        return BaseUtils.isDateTime(phoneNumber);
    }

    public boolean isGuest(UserSettingData userSettingData) {
        return phoneNumber.equals("9" + BaseUtils.reverse(userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber()));
    }

    public static UserRecord createGuest(UserSettingData userSettingData) {
        return new UserRecord("[고객]", "9" + BaseUtils.reverse(userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber()));
    }

    public static UserRecord createRetail(UserSettingData userSettingData) {
        UserRecord result = new UserRecord("[소매]", "8" + BaseUtils.reverse(userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber()));
        result.priceType = ContactBuyerType.RETAIL;
        return result;
    }

    public boolean isRetail(UserSettingData userSettingData) {
        return phoneNumber.equals("8" + BaseUtils.reverse(userSettingData.getUserShopInfo().getMyShopOrUserPhoneNumber()));
    }

    public List<UserRecord> toList() {
        List<UserRecord> results = new ArrayList<>();
        results.add(this);
        return results;
    }

    public static List<String> getNames(List<UserRecord> records) {
        Set<String> results = new HashSet<>();
        for (UserRecord record : records)
            results.add(record.name);
        return new ArrayList(results);
    }
}
