package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.enums.DBResultType;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

abstract public class BaseDB<T> {
    public String DB_NAME = null;
    protected String DB_TABLE;
    protected String OLD_DB_TABLE;
    protected int DB_VERSION;
    protected String[] columns, keyColumn;
    protected String[] columnAttrs = new String[0];
    protected DBHelper helper;
    public Context context;
    protected SQLiteDatabase database = null;
    protected String KEY_ID;
    protected final int KEY_ID_Index = 0;
    protected boolean showing = false, upgraded = false;
    private boolean onTransaction = false;
    protected final String multiItemDelimiter = ",";

    public BaseDB(Context context, String DBname, String DBtable, int DBversion, String[] columns) {
        this.DB_NAME = DBname;
        this.DB_TABLE = DBtable;
        this.DB_VERSION = DBversion;
        this.columns = columns;
        KEY_ID = columns[KEY_ID_Index];
        keyColumn = new String[]{KEY_ID};

        if (context == null) {
            Log.e("DEBUG_JS", String.format("[BaseDB] context = null, DB name = %s, DB table = %s ", DBname, DBtable));
            if (this.context == null)
                Log.e("DEBUG_JS", String.format("[%s] has no valid context", getClass().getSimpleName()));
        } else {
            this.context = context;
        }

    }

    public BaseDB(Context context, String DBName, int DBversion, String[] columns) {
        this(context, DBName, DBName + "Table", DBversion, columns);
    }

    public void setShowing(boolean value) {
        showing = value;
    }

    public DBResult insert(T record) {
        if (isOpen() == false) open();
        ContentValues contentvalues = toContentvalues(record);
        int id = (int) database.insert(DB_TABLE, null, contentvalues);
        if (id <= 0) {
            Log.e("DEBUG_JS", String.format("[%s.insert] failed id %d", getClass().getSimpleName(), getLastId() + 1));
            return new DBResult(id, DBResultType.INSERT_FAILED);
        }
        return new DBResult(id, DBResultType.INSERTED);
    }

    public DBResult update(int id, T record) {
        if (id <= 0) {
            Log.w("DEBUG_JS", String.format("[%s.update] id == %d", DB_NAME, id));
            return new DBResult(id, DBResultType.FAILED);
        }
        if (isOpen() == false) open();
        ContentValues contentvalues = toContentvalues(record);
        int result = database.update(DB_TABLE, contentvalues, KEY_ID + "=" + id, null);
        if (result <= 0) {
            Log.e("DEBUG_JS", String.format("[%s.update] failed id %d", getClass().getSimpleName(), id));
            return new DBResult(id, DBResultType.NOT_FOUND);
        }
        return new DBResult(id, DBResultType.UPDATED);
    }

    public DBResult updateOrInsert(T record) {
        T dbRecord = getRecordByKey(record); // key
        int id = getId(dbRecord);
        if (id > 0) {
            if (BaseRecord.isSame(record, dbRecord) == false) {
                return update(id, record);
            } else {
                return new DBResult(id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    // Child에서 구현해서 써야 한다
    protected T getRecordByKey(T record) {
        Log.e("DEBUG_JS", String.format("[BaseDB.getRecordByKey] %s not implemented", DB_NAME));
        return record;
    }

    public void clear() {
        if (isOpen() == false) open();
        if (database != null)
            database.delete(DB_TABLE, null, null);
        //Log.w("DEBUG_JS", String.format("[%s.clear] %s", DB_NAME, DB_TABLE));
    }

    public T getRecord(int id) {
        if (isOpen() == false) open();
        if (id <= 0) return getEmptyRecord();
        Cursor cursor = getCursor(id);
        if (cursor == null) {
            Log.e("DEBUG_JS", String.format("[%s.getRecordByKey] cursor (id:%d) == null", getClass().getSimpleName(), id));
            return getEmptyRecord();
        }
        T record = getRecord(cursor);
        cursor.close();
        return record;
    }

    protected T getRecord(Cursor cursor) {
        if (cursor == null) {
            Log.e("DEBUG_JS", String.format("[%s.getRecordByKey] cursor = null", getClass().getSimpleName()));
            return getEmptyRecord();
        }

        if (cursor.getCount() <= 0) {
            return getEmptyRecord();
        }

        int id = cursor.getInt(Column._id.ordinal());
        if (id == 0)
            return getEmptyRecord();

        return createRecord(cursor);
    }

    public T getTopRecord(Enum<?> column, boolean isDes) {
        List<T> results = getRecordsOrderBy(column, isDes);
        if (results.size() == 0)
            return getEmptyRecord();
        else
            return results.get(0);
    }

    public List<Integer> getIds(List<T> records) {
        List<Integer> ids = new ArrayList<>();
        for (T record : records)
            ids.add(getId(record));
        return ids;
    }

    public List<T> getRecords() {
        return getRecords(getCursor());
    }

    public List<T> getRecordsMoreThan(Enum<?> column, int integer) {
        String selection = String.format("%s >= %d", column.toString(), integer);
        return getRecords(getCursor(columns, selection, null, null, null, null, ""));
    }

    public List<T> getRecordsBy(Enum<?> dateColumn, int year, int month) {
        return getRecordsBy(dateColumn, year, month, new QueryOrderRecord(dateColumn, false, true).toList());
    }

    public List<T> getRecordsBy(Enum<?> dateColumn, int year, int month, List<QueryOrderRecord> orderRecords) {
        DateTime fromDate = BaseUtils.toMonth(year, month);
        DateTime toDate = fromDate.plusMonths(1).minusMillis(1);
        return getRecordsBetween(dateColumn, fromDate.toString(), toDate.toString(), orderRecords);
    }

    public List<T> getRecords(List<QueryAndRecord> queryAndRecords, List<QueryOrderRecord> queryOrderRecords) {
        Pair<String, String[]> selections = MyQuery.createSelections(queryAndRecords);
        return getRecords(getCursor(columns, selections.first, selections.second, null, null, MyQuery.createOrderBy(queryOrderRecords), null));
    }

    public List<T> getRecordsBetween(Enum<?> column, String from, String to, List<QueryOrderRecord> queryOrderRecords) {
        Pair<String, String[]> selections = MyQuery.createSelectionBETWEEN(column, from, to);
        return getRecords(getCursor(columns, selections.first, selections.second, null, null, MyQuery.createOrderBy(queryOrderRecords), null));
    }

    public List<String> getStringsBETWEEN(Enum<?> column, String from, String to, List<QueryOrderRecord> queryOrderRecords) {
        Pair<String, String[]> selections = MyQuery.createSelectionBETWEEN(column, from, to);
        return getStrings(column, selections.first, selections.second, null, null, MyQuery.createOrderBy(queryOrderRecords));
    }

    public List<T> getRecords(Enum<?> column, boolean value) {
        return getRecords(getCursor(column, value));
    }

    public List<T> getRecords(Enum<?> column, int value) {
        return getRecords(getCursor(column, value));
    }

    public List<T> getRecords(Enum<?> column, String value) {
        return getRecords(getCursor(column, value));
    }

    public List<T> getRecords(Enum<?> column1, int value1, Enum<?> column2, int value2) {
        return getRecords(column1, Integer.toString(value1), column2, Integer.toString(value2));
    }

    public <K> List<T> getRecordsIN(Enum<?> column, List<K> items, List<QueryOrderRecord> queryOrderRecords) {
        if (items.size() <= 0)
            return new ArrayList<>();

        if (items.size() <= 950) {
            Pair<String, String[]> selections = MyQuery.createSelectionIN(column, items);
            return getRecords(getCursor(columns, selections.first, selections.second, null, null, MyQuery.createOrderBy(queryOrderRecords), null));

        } else {
            // 950개 이상을 요구했을때 ... 가능한 없애야 한다.
            Log.e("DEBUG_JS", String.format("[%s.getRecordsIN] %s, requested item size %d > 950", DB_NAME, column.toString(), items.size()));
            List<T> results = new ArrayList<>();
            int totalCount = items.size(), startIndex = 0, splitCount = 950;
            while (totalCount > 0) {
                Pair<String, String[]> selections = MyQuery.createSelectionIN(column, items.subList(startIndex, Math.min(startIndex + splitCount, items.size())));
                List<T> result = getRecords(getCursor(columns, selections.first, selections.second, null, null, MyQuery.createOrderBy(queryOrderRecords), null));
                results.addAll(result);
                Log.w("DEBUG_JS", String.format("[%s.getRecordsIN] copy %d ~ %d", DB_NAME, startIndex,  Math.min(startIndex + splitCount, items.size())-1));
                startIndex = startIndex + splitCount;
                totalCount = totalCount - splitCount;
            }
            return results;
        }
    }

    public List<String> getStringsIN(Enum<?> targetColumn, Enum<?> column, List<Integer> integers, List<QueryOrderRecord> queryOrderRecords) {
        if (integers.size() <= 0)
            return new ArrayList<>();
        Pair<String, String[]> selections = MyQuery.createSelectionIN(column, integers);
        return getStrings(targetColumn, selections.first, selections.second, null, null, MyQuery.createOrderBy(queryOrderRecords));
    }

    public List<String> getStringsIN(Enum<?> targetColumn, Enum<?> column, List<String> strings) {
        if (strings.size() <= 0)
            return new ArrayList<>();
        Pair<String, String[]> selections = MyQuery.createSelectionIN(column, strings);
        return getStrings(targetColumn, selections.first, selections.second, null, null, null);
    }

    public List<T> getRecords(Enum<?> column1, String value1, Enum<?> column2, String value2) {
        if ((column1 == null) || (column2 == null)) {
            Log.e("DEBUG_JS", String.format("[%s.getIds] %s, %s = null", DB_NAME, column1.toString(), column2.toString()));
            return new ArrayList<T>(0);
        }
        String selection = String.format("%s=? AND %s=?", column1.toString(), column2.toString());
        String[] selectionArgs = new String[]{value1, value2};
        return getRecords(getCursor(columns, selection, selectionArgs, null, null, null, null));
    }

    public List<T> getRecords(List<QueryAndRecord> queryAndRecords) {
        Pair<String, String[]> selections = MyQuery.createSelections(queryAndRecords);
        return getRecords(getCursor(columns, selections.first, selections.second, null, null, null, null));
    }

    public T getRecordByQuery(List<QueryAndRecord> queryAndRecords) {
        List<T> results = getRecords(queryAndRecords);
        return getRecord(results, String.format("by query"));
    }

    public List<String> getStringsByQuery(Enum<?> column, List<QueryAndRecord> queryAndRecords) {
        Pair<String, String[]> selections = MyQuery.createSelections(queryAndRecords);
        return getStrings(column, selections.first, selections.second, null, null, null);
    }

    public T getRecord(Enum<?> column1, String value1, Enum<?> column2, String value2) {
        List<T> records = getRecords(column1, value1, column2, value2);
        T record = getRecord(records, String.format("column1 %s, value1 %s, column2 %s, value2 %s", column1.toString(), value1, column2.toString(), value2));
        return record;
    }

    private T getRecord(List<T> records, String errorMessage) {
        if (records.size() == 1) {
            return records.get(0);
        } else if (records.size() == 0) {
            return getEmptyRecord();
        } else {
            isPrimaryKeyDuplicated = true;
            for (T record : records)
                duplicatedIds.add(getId(record));
            Log.e("DEBUG_JS", String.format("[%s.getRecordByKey] count %d, %s", DB_NAME, records.size(), errorMessage));
            return records.get(records.size() - 1);
        }
    }

    public void clearDuplications() {
        if (isPrimaryKeyDuplicated) {
            int idMax = BaseUtils.getMax(new ArrayList(duplicatedIds));
            for (int id : duplicatedIds) {
                if (id != idMax)
                    delete(id);
            }
            Log.w("DEBUG_JS", String.format("[BaseDB.clearDuplications] %s", duplicatedIds.toString()));
        }
        isPrimaryKeyDuplicated = false;
    }

    public void cleanUpBySize(int sizeLimit) {
        if (isOpen() == false)
            open();
        int size = getSize();
        if (size <= sizeLimit) return;
        int count = 0;
        int limit = size - sizeLimit;
        List<Integer> oldIds = new ArrayList<>();
        for (T record : getRecordsOrderBy(Column._id, false)) {
            oldIds.add(getId(record));
            count++;
            if (count >= limit)
                break;
        }
        deleteAll(Column._id, oldIds);
        if (oldIds.size() >= 1)
            Log.w("DEBUG_JS", String.format("[%s.cleanUpBySize] size %d, limits %d, deletes %d, id %d~%d",
                    DB_NAME, size, sizeLimit, oldIds.size(), oldIds.get(0), oldIds.get(oldIds.size()-1)));
    }

    protected abstract int getId(T record);

    public T getRecord(Enum<?> column, String value) {
        return getRecord(getRecords(column, value), String.format("%s, %s", column.toString(), value));
    }

    public List<T> getRecords(Cursor cursor) {
        List<T> records = new ArrayList<>(0);
        if (cursor == null) return records;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return records;
        }
        cursor.moveToFirst();
        do {
            records.add(getRecord(cursor));
        } while (cursor.moveToNext());
        cursor.close();
        return records;
    }

    abstract protected T getEmptyRecord();

    abstract protected T createRecord(Cursor cursor);

    abstract protected ContentValues toContentvalues(T record);

    protected void toLogCat(int id, T record, int option, String operation) {
    }

    public boolean tableExists(String tableName) {
//		if (isOpen() == false) open();
        boolean result = false;
        Cursor cursor = null;
        try {
            cursor = database.rawQuery(MyQuery.tableExists(tableName), null);
            if (cursor == null) return result;
            result = (cursor.getCount() >= 1);
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return result;
    }

    public void setColumnValue(Enum<?> column, String value) {
        if (isOpen() == false) open();
        database.execSQL(MyQuery.updateColumnValue(DB_TABLE, column.toString(), String.format("'%s'", value)));
    }

    public void setColumnValue(Enum<?> column, int value) {
        setColumnValue(column, Integer.toString(value));
    }

    public void setColumnValue(Enum<?> column, boolean value) {
        setColumnValue(column, BaseUtils.toInt(value));
    }

    private String toQueryValue(Object value) {
        if (value instanceof String) {
            return String.format("'%s'", value);
        } else {
            return String.valueOf(value);
        }
    }

    public void beginTransaction() {
        if (isOpen() == false) open();
        onTransaction = true;
        database.beginTransaction();
    }

    public void endTransaction() {
        if (isOpen() == false) open();
        database.setTransactionSuccessful();
        database.endTransaction();
        onTransaction = false;
    }

    public List<String> getColumns(String tableName) {
        if (isOpen() == false) open();
        if (tableName.equals(Empty.string)) {
            tableName = DB_TABLE;
        }
        ArrayList<String> result = new ArrayList<String>(0);
        Cursor cursor = null;
        try {
            cursor = database.rawQuery(MyQuery.getTableInfo(tableName), null);
            if (cursor == null) return result;
            if (cursor.getCount() <= 0) {
                cursor.close();
                return result;
            }

            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                result.add(cursor.getString(1));
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return result;
    }

    public List<String> getColumns() {
        return getColumns(DB_TABLE);
    }

    public boolean isOpen() {
        if ((helper == null) || (database == null)) {
            return false;
        } else {
            return true;
        }
        //return (helper != null) || (database );
    }

    public void open() {
        try {
            helper = getDBHelper();
            database = helper.getWritableDatabase();
            if (database == null)
                Log.e("DEBUG_JS", String.format("[%s.open] database == null", DB_NAME));
        } catch (SQLException e) {
            Log.e("DEBUG_JS", String.format("[%s] fail to open %s, %d : %s", getClass().getSimpleName(), DB_TABLE, DB_VERSION, e.getLocalizedMessage()));
            MyUI.toastSHORT(context, String.format("문제가 발견되었습니다. 앱 재설치를 추천드립니다."));
        }

//        if (isColumnsValid() == false) {
//            Log.e("DEBUG_JS", String.format("[BaseDB.BaseDB] columns are invalid..."));
//            deleteDB();
//        }
    }

    public DBHelper getDBHelper() {
        return new DBHelper(context);
    }

    public String getTeamPlPath() {
        return String.format("%s/%s.db", MyDevice.teamplDir, DB_NAME);
    }

    public void close() {
        if (helper != null) helper.close();
        helper = null;
        //		Log.i("DEBUG_JS",String.format("[BaseDB.reset] %s, %s reset", DB_NAME, DB_TABLE));
    }

    public int delete(int id) {
        if (isOpen() == false) open();
        return database.delete(DB_TABLE, KEY_ID + "=" + id, null);
    }

    public <T> void deleteAll(Enum<?> column, T value) {
        List<T> results = new ArrayList<>();
        results.add(value);
        deleteAll(column, results);
    }

    public <T> void deleteAll(Enum<?> column, List<T> values) {
        if (values.isEmpty()) return;
        if (isOpen() == false) open();
        if (values.size() >= 950) { // SQL에서 999개 까지만 허용
            int iterationCount = (values.size() / 500) + 1;
            for (int i = 0; i < iterationCount; i++) {
                Log.i("DEBUG_JS", String.format("[BaseDB.deleteFromDBs] delete %d ~ %d", i*500, Math.min(values.size(), (i+1)*500)));
                Pair<String, String[]> pair = MyQuery.createSelectionIN(column, values.subList(i*500, Math.min(values.size(), (i+1)*500)));
                database.delete(DB_TABLE, pair.first, pair.second);
            }
        } else {
            Pair<String, String[]> pair = MyQuery.createSelectionIN(column, values);
            database.delete(DB_TABLE, pair.first, pair.second);
        }
        //database.execSQL(MyQuery.deleteRowsIN(DB_TABLE, column, values)); .. 시간의 경우 T 문자가 unrecognized token 에러를 발생시킴
    }

    public void deleteAll(List<T> records) {
        if (records.isEmpty()) return;
        if (isOpen() == false) open();
        List<Integer> ids = new ArrayList<>();
        for (T record : records)
            ids.add(getId(record));
        deleteAll(Column._id, ids);
    }



    public void deleteOrMore(int id) {  // >=
        if (isOpen() == false) open();
        database.delete(DB_TABLE, KEY_ID + ">=" + id, null);
    }

    public void deleteDB() {
        if (isOpen() == false) open();
        try {
            database.execSQL(MyQuery.delete(DB_TABLE));
            //	database.execSQL(MyQuery.delete("old"+DB_TABLE));
            helper.onCreate(database);
            Log.w("DEBUG_JS", String.format("[%s] delete DB : %s", DB_NAME, context.getPackageName()));
        } catch (SQLException e) {
            Log.e("DEBUG_JS", String.format("[BaseDB.deleteDB]"), e);
        }
    }

    public boolean isEmpty() {
        return (getSize() <= 0);
    }

    public int getSize() {
        if (isOpen() == false) open();
        return (int) DatabaseUtils.queryNumEntries(database, DB_TABLE);
    }

    public List<String> getStrings(Enum<?> interest, Enum<?> column, boolean value) {
        return getStrings(interest, column, toString(value));
    }

    private String toString(boolean value) {
        return (value ? "1" : "0");
    }

    public List<String> getStrings(Enum<?> interest, Enum<?> column, int value) {
        return getStrings(interest, column, Integer.toString(value));
    }

    public List<String> getStrings(Enum<?> interest, Enum<?> column, String value) {
        String selection = null;
        String[] selectionArgs = null;
        if (column != null) {
            selection = column.toString() + "=?";
            selectionArgs = new String[]{value};
        }
        return getStrings(interest, selection, selectionArgs, null, null, null);
    }

    protected Cursor getCursor(Enum<?> column, boolean value) {
        return getCursor(column, toString(value));
    }

    protected Cursor getCursor(Enum<?> column, int value) {
        return getCursor(column, Integer.toString(value));
    }

    protected Cursor getCursor(Enum<?> column, String value) {
        return getCursorOrderBy(column, value, null, "", false);
    }

    protected Cursor getCursor(String condition) {
        return getCursor(columns, condition, null, null, null, null, "");
    }

    public List<String> getStrings(Enum<?> column) {
        return getStrings(column, null, null, null, null, null);
    }

    private List<String> getStrings(Enum<?> interest, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        if (isOpen() == false) open();
        ArrayList<String> result = new ArrayList<String>(0);
        if (isEmpty()) return result;

        Cursor cursor = getCursor(new String[]{interest.toString()}, selection, selectionArgs, groupBy, having, orderBy, "");
        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }

        int inx = 0;
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            result.add(cursor.getString(0));
            inx = inx + 1;
        }
        cursor.close();
        //Log.i("DEBUG_JS",String.format("[BaseDB.getIds] %s", BaseUtils.toStr(result)));
        return result;
    }

    public List<Integer> getIntegers(Enum<?> column) {
        return getIntegers(column, null, null, null, null, null);
    }

    public List<Long> getLongs(Enum<?> column) {
        return getLongs(column, null, null, null, null, null);
    }

    protected Cursor getCursor(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String methodStr) {
        if (isOpen() == false) open();
        if (database == null) {
            Log.e("DEBUG_JS", String.format("[%s.getCursor] database == null", getClass().getSimpleName()));
            return Empty.getCursor();
        }
        // name, columns, where, where condition, group, having, order
        try {
            Cursor cursor = database.query(DB_TABLE, columns, selection, selectionArgs, groupBy, having, orderBy);
            if (cursor == null) {
                Log.e("DEBUG_JS", String.format("[BaseDB.%s] %s count = null: selection = %s", DB_NAME, selection));
            } else if (cursor.getCount() <= 0) {
                //Log.w("DEBUG_JS",String.format("[BaseDB.%s] %s count <= 0: selection = %s",methodStr,DB_NAME,selection));
            } else {
                cursor.moveToFirst();
            }
            return cursor;
        } catch (SQLException e) {
            if (e.getLocalizedMessage().contains("no such column"))
                Log.e("DEBUG_JS", String.format("[%s.getCursor] %s", DB_NAME, e.getLocalizedMessage()));
            return Empty.getCursor();
        }
    }

    public boolean isColumnsValid() {
        try {
            if (isOpen() == false) open();
            Cursor cursor = database.query(DB_TABLE, columns, KEY_ID + "=" + 0, null, null, null, null);
            cursor.close();
            return true;
        } catch (SQLException e) {
            Log.e("DEBUG_JS", String.format("[%s.getCursor] %s", DB_NAME, e.getLocalizedMessage()));
            return false;
        }
    }

    public List<Integer> getIntegers(Enum<?> interest, Enum<?> column, String value) {
        String selection = null;
        String[] selectionArgs = null;
        if (column != null) {
            selection = column.toString() + "=?";
            selectionArgs = new String[]{value};
        }
        return getIntegers(interest, selection, selectionArgs, null, null, null);
    }

    public List<Integer> getIntegers(Enum<?> interest, Enum<?> column, Boolean value) {
        return getIntegers(interest, column, toString(value));
    }

    public List<Integer> getIntegers(Enum<?> interest, Enum<?> column, int value) {
        return getIntegers(interest, column, Integer.toString(value));
    }

    private ArrayList<Integer> getIntegers(Enum<?> interest, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        if (isOpen() == false) open();
        ArrayList<Integer> result = new ArrayList<Integer>(0);
        if (isEmpty()) return result;

        Cursor cursor = getCursor(new String[]{interest.toString()}, selection, selectionArgs, groupBy, having, orderBy, "getIntegers");

        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }

        int inx = 0;
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            result.add(cursor.getInt(0));
            inx = inx + 1;
        }

        cursor.close();
        //Log.i("DEBUG_JS",String.format("[BaseDB.getIds] %s", BaseUtils.toStr(result)));
        return result;
    }

    private List<Long> getLongs(Enum<?> interest, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        List<Long> result = new ArrayList<>(0);
        if (isEmpty()) return result;

        Cursor cursor = getCursor(new String[]{interest.toString()}, selection, selectionArgs, groupBy, having, orderBy, "getLongs");
        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }

        int inx = 0;
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            result.add(cursor.getLong(0));
            inx = inx + 1;
        }
        cursor.close();
        //Log.i("DEBUG_JS",String.format("[BaseDB.getIds] %s", BaseUtils.toStr(result)));
        return result;
    }

    private enum Column {_id}

    public ArrayList<Integer> getIds(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return getIntegers(Column._id, selection, selectionArgs, groupBy, having, orderBy);
    }

    public List<Integer> getIds() {
        return getIds(null, null, null, null, null);
    }

    public List<Integer> getIdsInReverse() {
        return getIds(null, null, null, null, "_id DESC");
    }

    public List<Integer> getIds(Enum<?> column, String[] args) {
        if (column == null) {
            Log.e("DEBUG_JS", String.format("[BaseDB.getIds] %s, %s = null", DB_NAME, column.toString()));
            return new ArrayList<Integer>(0);
        }
        String selection = "";
        for (int i = 0; i < args.length; i++) {
            if (i > 0)
                selection = selection + " OR ";
            selection = selection + column.toString() + "=?";
        }
        String[] selectionArgs = args;
        return getIds(selection, selectionArgs, null, null, null);
    }

    public List<T> getRecordsBySubstring(Enum<?> column, String substring) {
        if (isOpen() == false) open();
        ArrayList<T> result = new ArrayList<>();
        if (column == null) {
            Log.e("DEBUG_JS", String.format("[BaseDB.getIds] %s, %s = null", DB_NAME, column.toString()));
            return result;
        }
        String[] selectionArgs = new String[]{"%" + substring + "%"};
        Cursor cursor = database.rawQuery(String.format("SELECT * FROM %s WHERE %s LIKE ?", DB_TABLE, column.toString()), selectionArgs);
        cursor.moveToFirst();
        return getRecords(cursor);
    }

    public List<Integer> getIds(Enum<?> column1, int value1, Enum<?> column2, int value2) {
        return getIds(column1, Integer.toString(value1), column2, Integer.toString(value2));
    }

    public int getId(Enum<?> column1, int value1, Enum<?> column2, int value2) {
        return getId(column1, Integer.toString(value1), column2, Integer.toString(value2));
    }

    public int getId(Enum<?> column1, String value1, Enum<?> column2, String value2) {
        String columnValueStr = String.format("%s, %s, and %s, %s", column1.toString(), value1, column2.toString(), value2);
        return getId(getIds(column1, value1, column2, value2), columnValueStr);
    }

    public int getId(Enum<?> column1, String value1, Enum<?> column2, String value2, Enum<?> column3, String value3) {
        String columnValueStr = String.format("%s, %s, and %s, %s and %s, %s", column1.toString(), value1, column2.toString(), value2, column3.toString(), value3);
        return getId(getIds(column1, value1, column2, value2, column3, value3), columnValueStr);
    }

    private int getId(List<Integer> ids, String columnValueStr) {
        if (ids.size() == 1) {
            return ids.get(0);
        } else if (ids.size() == 0) {
            return 0;
        } else {
            Log.e("DEBUG_JS", String.format("[%s.getId] ids.size == %d, %s, (id:%s)", DB_NAME, ids.size(), columnValueStr, BaseUtils.toStr(ids)));
//            int index = 0;
//            for (int id : ids) {
//                if (index != ids.size() - 1) {
//                    Log.e("DEBUG_JS", String.format("[BaseDB.getId] delete id:%d", id));
//                    delete(id);
//                }
//                index++;
//            }
            return ids.get(ids.size() - 1);
        }
    }


    public List<Integer> getIds(Enum<?> column1, int value1, Enum<?> column2, String value2) {
        return getIds(column1, Integer.toString(value1), column2, value2);
    }

    public int getId(Enum<?> column1, int value1, Enum<?> column2, String value2) {
        return getId(column1, Integer.toString(value1), column2, value2);
    }

    public List<Integer> getIds(Enum<?> column1, String value1, Enum<?> column2, String value2) {
        if ((column1 == null) || (column2 == null)) {
            Log.e("DEBUG_JS", String.format("[%s.getIds] %s, %s = null", DB_NAME, column1.toString(), column2.toString()));
            return new ArrayList<Integer>(0);
        }
        String selection = String.format("%s=? AND %s=?", column1.toString(), column2.toString());
        String[] selectionArgs = new String[]{value1, value2};
        return getIds(selection, selectionArgs, null, null, null);
    }

    public List<Integer> getIds(Enum<?> column1, String value1, Enum<?> column2, String value2, Enum<?> column3, String value3) {
        if ((column1 == null) || (column2 == null) || (column3 == null)) {
            Log.e("DEBUG_JS", String.format("[%s.getIds] %s, %s = null", DB_NAME, column1.toString(), column2.toString()));
            return new ArrayList<Integer>(0);
        }
        String selection = String.format("%s=? AND %s=? AND %s=?", column1.toString(), column2.toString(), column3.toString());
        String[] selectionArgs = new String[]{value1, value2, value3};
        return getIds(selection, selectionArgs, null, null, null);
    }

    public List<Integer> getIds(Enum<?> column, String value) {
        return getIds(column, new String[]{value});
    }

    public List<Integer> getIds(Enum<?> column, boolean value) {
        return getIds(column, (value ? "1" : "0"));
    }

    public List<Integer> getIds(Enum<?> column, int value) {
        return getIds(column, Integer.toString(value));
    }

    public List<Integer> getIdsOrderBy(Enum<?> column, boolean isDescending) {
        return getIdsOrderBy(column, "", isDescending);
    }

    public List<Integer> getIdsOrderByInt(Enum<?> column, boolean isDescending) {
        return getIdsOrderBy(column, "+0", isDescending);
    }

    private List<Integer> getIdsOrderBy(Enum<?> column, String option, boolean isDescending) {
        String orderBy = null;
        if (isDescending) {
            orderBy = String.format("case when %s like '' then 2 else 1 end,%s%s DESC", column.toString(), column.toString(), option);
        } else {
            orderBy = String.format("case when %s like '' then 2 else 1 end,%s%s ASC", column.toString(), column.toString(), option);
            // orderBy = String.format("%s%s ASC", column.toDbString(), option);
        }
        return getIds(null, null, null, null, orderBy);
    }

    public List<T> getRecordsOrderBy(Enum<?> sortingColumn, boolean isDescending) {
        return getRecordsOrderBy(null, null, sortingColumn, "", isDescending);
    }

    public List<T> getRecordsOrderByInt(Enum<?> sortingColumn, boolean isDescending) {
        return getRecordsOrderBy(null, null, sortingColumn, "+0", isDescending);
    }

    public List<T> getRecordsOrderBy(Enum<?> column, int value, Enum<?> sortingColumn, boolean isDescending) {
        return getRecordsOrderBy(column, Integer.toString(value), sortingColumn, "", isDescending);
    }

    public List<T> getRecordsOrderBy(Enum<?> column, String value, Enum<?> sortingColumn, String option, boolean isDescending) {
        return getRecords(getCursorOrderBy(column, value, sortingColumn, option, isDescending));
    }

    protected Cursor getCursorOrderBy(Enum<?> column, boolean isDescending) {
        return getCursorOrderBy(null, null, column, "", isDescending);
    }

    protected Cursor getCursorOrderByInt(Enum<?> column, boolean isDescending) {
        return getCursorOrderBy(null, null, column, "+0", isDescending);
    }

    // option :  integer는 "+0"
    protected Cursor getCursorOrderBy(Enum<?> column, String value, Enum<?> sortingColumn, String option, boolean isDescending) {
        String selection = null;
        String[] selectionArgs = null;
        if (column != null) {
            selection = column.toString() + "=?";
            selectionArgs = new String[]{value}; // selection이 ?로 지정된 경우 조건에 해당
        }

        String orderBy = null;
        if (sortingColumn != null) {
            if (isDescending) {
                orderBy = String.format("%s%s DESC", sortingColumn.toString(), option);
            } else {
                orderBy = String.format("%s%s ASC", sortingColumn.toString(), option);
            }
        }

        return getCursor(columns, selection, selectionArgs, null, null, orderBy, "");
    }

    public List<T> getRecordsOrderBy(List<QueryOrderRecord> queryOrderRecords) {
        return getRecords(getCursorOrderBy(queryOrderRecords));
    }

    protected Cursor getCursorOrderBy(List<QueryOrderRecord> queryOrderRecords) {
        return getCursor(columns, null, null, null, null, MyQuery.createOrderBy(queryOrderRecords), "");
    }

    protected boolean isPrimaryKeyDuplicated = false;
    protected Set<Integer> duplicatedIds = new HashSet<>();

    public int getId(Enum<?> candidateKey, String value) {
        List<Integer> ids = getIds(candidateKey, value);
        int result = Empty.integer;
        if (ids.size() == 1) {
            result = ids.get(0);
        } else if (ids.size() == 0) {
            // Log.w("DEBUG_JS", String.format("[%s.getId] ids.length = 0, column = %s, value = %s", DB_NAME, candidateKey.toDbString(), value));
        } else {
            isPrimaryKeyDuplicated = true;
            duplicatedIds.addAll(ids);
            Log.e("DEBUG_JS", String.format("[%s.getId] ids.length = %d, column = %s, value = %s", DB_NAME, ids.size(), candidateKey.toString(), value));
            return ids.get(ids.size() - 1);
        }
        return result;
    }

    public int getId(Enum<?> candidateKey, int value) {
        return getId(candidateKey, Integer.toString(value));
    }

    public boolean hasValue(Enum<?> column, String value) {
        if (isEmpty()) {
            return false;
        }
        if (column == null) {
            return false;
        }

       // return getStrings(column).contains(value); // --> 실험해보자
        return (getIds(column, new String[]{value}).size() >= 1);
    }

    public boolean hasStringNoCase(Enum<?> column, String value) {
        String selection = String.format("%s LIKE? ", column.toString()); //  =?대신 LIKE? 사용
        String[] selectionArgs = new String[]{value};
        return (getIds(selection, selectionArgs, null, null, null).size() >= 1);
    }

    public boolean hasValue(Enum<?> column, int value) {
        return hasValue(column, Integer.toString(value));
    }

    public boolean hasValue(Enum<?> column, long value) {
        return hasValue(column, Long.toString(value));
    }

    public boolean hasValue(Enum<?> column1, int value1, Enum<?> column2, int value2) {
        return hasValue(column1, Integer.toString(value1), column2, Integer.toString(value2));
    }

    public boolean hasValue(Enum<?> column1, String value1, Enum<?> column2, String value2) {
        if (isEmpty()) return false;
        if ((column1 == null) || (column2 == null)) return false;
        return (getIds(column1, value1, column2, value2).size() >= 1);
    }

    public boolean hasValue(Enum<?> column1, String value1, Enum<?> column2, String value2, Enum<?> column3, String value3) {
        if (isEmpty()) return false;
        if ((column1 == null) || (column2 == null) || (column3 == null)) return false;
        return (getIds(column1, value1, column2, value2, column3, value3).size() >= 1);
    }

    public boolean hasValue(Enum<?> column1, int value1, Enum<?> column2, String value2) {
        if (isEmpty()) return false;
        if ((column1 == null) || (column2 == null)) return false;
        return (getIds(column1, Integer.toString(value1), column2, value2).size() >= 1);
    }

    public boolean hasValue(Enum<?> column1, boolean value1, Enum<?> column2, String value2) {
        return hasValue(column1, BaseUtils.toDbStr(value1), column2, value2);
    }

    public boolean hasValue(Enum<?> column, boolean value) {
        return hasValue(column, Integer.toString(BaseUtils.toInt(value)));
    }

    public String getString(Enum<?> column, int id) {
        if (isEmpty()) return Empty.string;
        if (isOpen() == false) open();
        String result = "";
        Cursor cursor = getCursor(new String[]{column.toString()}, KEY_ID + "=" + id, null, null, null, null, "getString");
        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }
        result = cursor.getString(0);
        cursor.close();
        return result;
    }

    protected Cursor getCursor(int id) {
        return getCursor(columns, KEY_ID + "=" + id, null, null, null, null, "");
    }

    protected Cursor getCursor() {
        return getCursor(null, null, null, null, null, null, "");
    }

    public int getInt(Enum<?> column, int id) {
        String result = getString(column, id);
        if (result.equals(Empty.string)) {
            return Empty.integer;
        } else {
            return Integer.valueOf(result);
        }
    }

    public boolean getBoolean(Enum<?> column, int id) {
        return BaseUtils.toBoolean(getInt(column, id));
    }

    public Bitmap getBitmap(Enum<?> column, int id) {
        return MyGraphics.toBitmap(getImage(column, id));
    }

    public byte[] getImage(Enum<?> column, int id) {
        byte[] result = new byte[0];
        if (isEmpty()) return result;

        Cursor cursor = getCursor(new String[]{column.toString()}, KEY_ID + "=" + id, null, null, null, null, "getImageFast");
        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }
        result = cursor.getBlob(0);
        cursor.close();
        return result;
    }

    public boolean updateString(Enum<?> column, int id, String value) {
        if (isOpen() == false) open();
        if (id > this.getLastId()) {
            Log.e("DEBUG_JS", String.format("[BaseDB.updateString] %s, id %d > last id %d", DB_NAME, id, getLastId()));
            return false;
        }
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(column.name(), value);
        int row = database.update(DB_TABLE, contentvalues, KEY_ID + "=" + id, null);
        if (row == -1) {
            Log.w("DEBUG_JS", String.format("[BaseDB.updateString] %s, column %s id %d value %s not updated", DB_NAME, column.toString(), id, value));
            return false;
        } else {
            if (showing)
                Log.i("DEBUG_JS", String.format("[%s.update] [%d] column %s = %s", DB_NAME, id, column.toString(), value));
            return true;
        }
    }

    public boolean updateDateTime(Enum<?> column, int id, DateTime dateTime) {
        return updateString(column, id, BaseUtils.toStr(dateTime));
    }

    public boolean updateInt(Enum<?> column, int id, int value) {
        return updateString(column, id, Integer.toString(value));
    }

    public boolean updateBoolean(Enum<?> column, int id, boolean value) {
        return updateString(column, id, Integer.toString(BaseUtils.toInt(value)));
    }

    public int getFirstId() {
        int result = 0;
        Cursor cursor = getCursor(keyColumn, null, null, null, null, null, "getFirstId");
        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }
        result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    public int getLastId() {
        List<Integer> ids = getIds();
        if (ids.size() == 0)
            return 0;
        return getIds().get(ids.size() - 1);
    }

    public int getNextId(int id) {
        List<Integer> ids = getIds();
        int nextId = id;
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == id) {
                if (i == ids.size() - 1) {    // last row
                    nextId = ids.get(0);
                    break;
                } else {
                    nextId = ids.get(i + 1);        // the others
                    break;
                }
            }
        }

        int result = 0;
        Cursor cursor = getCursor(keyColumn, KEY_ID + "=" + nextId, null, null, null, null, "getNextId");
        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }

        result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    public int getPrevId(int id) {
        List<Integer> ids = getIds();
        int prevId = id;
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == id) {
                if (i == 0) {    // first row
                    prevId = ids.get(ids.size() - 1);
                    break;
                } else {
                    prevId = ids.get(i - 1);        // the others
                    break;
                }
            }
        }

        int result = 0;
        Cursor cursor = getCursor(keyColumn, KEY_ID + "=" + prevId, null, null, null, null, "getPrevId");
        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }

        result = cursor.getInt(0);
        cursor.close();
        return result;
    }

//    public void deleteNotExist(ArrayList<Integer> oldIds) {
//        ArrayList<Integer> currentIds = getIds();
//        for (int oldId : oldIds) {
//            if (currentIds.contains(oldId) == false)
//                delete(oldId);
//        }
//    }

    public void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("DEBUG_JS", String.format("[%s] %s upgrade %d -> %d and deleteDB", getClass().getSimpleName(), DB_NAME, oldVersion, newVersion));
        db.execSQL(MyQuery.delete(DB_TABLE));
        helper.onCreate(db);
        upgraded = true;
    }

    // Helper.checkInstanceAndRestart
    public void create(SQLiteDatabase db) {
        if (columnAttrs == null || columnAttrs.length != columns.length) {
            db.execSQL(MyQuery.create(DB_TABLE, columns));
        } else {
            db.execSQL(MyQuery.create(DB_TABLE, columns, columnAttrs));
        }
    }

    // Helper
    public class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String filePath) {
            super(context, filePath, null, DB_VERSION);
        }

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            create(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            upgrade(db, oldVersion, newVersion);
        }
    }
}
