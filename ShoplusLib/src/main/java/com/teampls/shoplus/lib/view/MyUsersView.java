package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseUserDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyKeyboard;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.UpdateUserDialog;
import com.teampls.shoplus.lib.dialog.UserDetailsDialog;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.enums.MemberPermission;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.composition.UserComposition;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;

import java.util.List;

/**
 * Created by Medivh on 2017-04-15.
 */

public class MyUsersView extends BaseActivity implements AdapterView.OnItemClickListener {
    private BaseUserDBAdapter adapter;
    private ListView listView;
    protected EditText etName;
    protected MyKeyboard myKeyboard;
    private static MyOnClick<UserRecord> onUserClick;
    private static boolean hasCallback = false;
    private static boolean doFilterSlipCounterparts = false;
    private List<UserRecord> fullRecords;
    private static boolean doFinish = true;
    public static String KEY_SHOW_ALL = "<전체보기>";
    private UserComposition userComposition;

    public static void startActivity(Context context) {
        MyUsersView.startActivity(context, false, null);
    }

    public static void startActivity(Context context, MyOnClick<UserRecord> onUserClick) {
        MyUsersView.startActivity(context, true, onUserClick);
    }

    public static void startActivityNotFinish(Context context, MyOnClick<UserRecord> onUserClick) {
        MyUsersView.startActivity(context, false, onUserClick);
    }

    public static void startActivity(Context context, boolean doFinish, MyOnClick<UserRecord> onUserClick) {
        MyUsersView.hasCallback = (onUserClick == null ? false : true);
        MyUsersView.onUserClick = onUserClick;
        MyUsersView.doFinish = doFinish;
        // 유저 정보를 수정할 수 있기 때문
        ((Activity) context).startActivityForResult(new Intent(context, MyUsersView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    public int getThisView() {
        return R.layout.base_user_search;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;

        userComposition = new UserComposition(context);
        userDB = UserDB.getInstance(context);
        userDB.cleanUp();

        etName = (EditText) findViewById(R.id.user_search_name);
        if (MyDevice.getAndroidVersion() < 27)
            etName.setHint("이름 또는 전화번호");
        etName.addTextChangedListener(new MyTextWatcher());
        adapter = new BaseUserDBAdapter(context) {
            @Override
            public BaseViewHolder getViewHolder() {
                return new DefaultViewHolder();
            }
        };

        listView = (ListView) findViewById(R.id.user_search_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);

     myKeyboard = new MyKeyboard(context, findViewById(R.id.user_search_container), etName);
        setOnClick(R.id.user_search_clear, R.id.user_search_add);

        if (hasCallback)
            adapter.addFirstRecord(KEY_SHOW_ALL);
        if (doFilterSlipCounterparts)
            adapter.filterSlipCounterparts();

        MyView.setTextViewByDeviceSize(context, getView(), R.id.user_search_name, R.id.user_search_add);
        MyView.setImageViewSizeByDeviceSizeScale(context, 0.5, (ImageView) findViewById(R.id.user_search_clear));

        initFullRecords();

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_CONTACTS) == false) {
            Log.w("DEBUG_JS", String.format("[MyUsersView.onCreate] request permission WRITE_CONTACTS...."));
            MyDevice.requestPermission(context, MyDevice.UserPermission.WRITE_CONTACTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_CONTACTS))
            Log.w("DEBUG_JS", String.format("[MyUsersView.onCreate] has permission WRITE_CONTACTS automatically"));
    }

    private void initFullRecords() {
        adapter.resetRecords(); // 기존 내용 삭제, 각종 필터 삭제 (doSkipGenerate)
        adapter.setSortingKey(MSortingKey.Name);
        adapter.generate();
        fullRecords = adapter.getRecords();
        if (hasCallback)
            fullRecords.add(0, new UserRecord(KEY_SHOW_ALL, ""));
        refresh();
    }

    public void refresh() {
        if (adapter == null) return;
        new MyAsyncTask(context, "MyUsersView") {
            @Override
            public void doInBackground() {
                adapter.generate(false);
            }

            @Override
            public void onPostExecutionUI() {
                // 적용이 안되는 이유를 알아보자
                adapter.applyGeneratedRecords();
                adapter.notifyDataSetChanged();
            }
        };
    }

    private void addNewUser(String name) {
        new UpdateUserDialog(context, new UserRecord(name, ""), DbActionType.INSERT) {
            @Override
            public void onUserInputFinish(UserRecord userRecord) {
                fullRecords.add(0, userRecord);
                etName.setText(userRecord.name);
                etName.setSelection(userRecord.name.length());
                refresh();
            }
        };
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.user_search_clear) {
            etName.setText("");
        } else if (view.getId() == R.id.user_search_add) {
            addNewUser(etName.getText().toString());
        }
    }

    public void doFinish() {
        myKeyboard.hide();
        doFinishForResult();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
        if (hasCallback) {
            if (onUserClick != null) {
                new MyDelayTask(context, 100) {
                    @Override
                    public void onFinish() {
                        onUserClick.onMyClick(view, adapter.getRecord(position)); // activity를 종료하는 것과 다른 view를 생성할때 충돌을 막기 위해
                    }
                };
            }
            if (doFinish)
                doFinish();
        } else {
            new UserDetailsDialog(context, adapter.getRecord(position)) {
                @Override
                public void onInfoUpdated(UserRecord userRecord) {
                    refresh();
                }

                @Override
                public void onAccountUpdated() {
                    //
                }
            };
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.add(MyMenu.addCounterpart)
                .add(MyMenu.refresh)
                .add(MyMenu.mergeCounterpart)
                .add(MyMenu.addressBook)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case addressBook:
                MyAddressbookView.startActivity(context, new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        initFullRecords();
                    }
                });
                break;

            case mergeCounterpart:
                if (MemberPermission.MergeCounterpart.hasPermission(userSettingData) == false) {
                    MemberPermission.MergeCounterpart.toast(context);
                    return;
                }

                MyUserMergerView.startActivity(context, etName.getText().toString(), new MyOnTask<List<UserRecord>>() {
                    @Override
                    public void onTaskDone(List<UserRecord> mergedUsers) {
                        initFullRecords();
                    }
                });
                MyUI.toastSHORT(context, String.format("통합하실 거래처들을 선택해주세요"));

                break;
            case addCounterpart:
                addNewUser("");
                break;
            case refresh:
                new OnRefreshDialog(context).show();
                break;
            case sendKakao:
                MyDevice.openApp(context, BaseGlobal.KakaoPackage);
                break;
            case keyboard:
                myKeyboard.switchKeyboard();
                break;
            case close:
                myKeyboard.hide();
                doFinishForResult();
                break;
        }
    }

    class OnRefreshDialog extends MyButtonsDialog {

        public OnRefreshDialog(final Context context) {
            super(context, "새로고침","[전체 재조사]는 문제가 발생했을때만 사용해주세요");
            addButton("최근 변경만 조사", new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    dismiss();
                    userComposition.downloadCounterparts(new MyOnTask<List<UserRecord>>() {
                        @Override
                        public void onTaskDone(List<UserRecord> result) {
                            MyUI.toastSHORT(context, String.format("변경 : %s", TextUtils.join(",",UserRecord.getNames(result))));
                            initFullRecords();
                        }
                    });
                }
            });
            addButton("전체 재조사", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    final int prevSize = userDB.getSize();
                    userDB.clear();
                    userComposition.downloadCounterparts(new MyOnTask<List<UserRecord>>() {
                        @Override
                        public void onTaskDone(List<UserRecord> result) {
                            MyUI.toast(context, String.format("%d → %d 명", prevSize, result.size()));
                            initFullRecords();
                        }
                    });
                }
            });
        }
    }

    class MyTextWatcher extends BaseTextWatcher {

        @Override
        public void afterTextChanged(Editable s) {
            adapter.setRecords(filterRecords(s, fullRecords));
            refresh();
        }
    }

}
