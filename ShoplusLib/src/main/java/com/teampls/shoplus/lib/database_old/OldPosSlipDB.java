package com.teampls.shoplus.lib.database_old;

import android.content.Context;

/**
 * Created by Medivh on 2017-05-22.
 */

public class OldPosSlipDB extends OldSlipDB {
    private static OldPosSlipDB instance = null;

    public static OldPosSlipDB getInstance(Context context) {
        if (instance == null)
            instance = new OldPosSlipDB(context);
        return instance;
    }

    protected OldPosSlipDB(Context context) {
        super(context, "PosSlipDB", Ver, Column.toStrs());
    }

}
