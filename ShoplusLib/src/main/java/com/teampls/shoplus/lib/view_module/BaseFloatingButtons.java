package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.ImageView;

import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-11-13.
 */

abstract public class BaseFloatingButtons implements View.OnClickListener {
    protected View view;
    private List<ImageView> buttons = new ArrayList<>();
    private ImageView showButton, hideButton;
    protected boolean isShowing = false;

    public BaseFloatingButtons(View view) {
        this.view = view;
        hide();
    }

    public List<ImageView> getButtons() {
        return buttons;
    }

    public BaseFloatingButtons add(@IdRes int... resIds) {
        if (view != null) {
            for (int resId : resIds) {
                ImageView imageView = view.findViewById(resId);
                if (imageView != null) {
                    imageView.setTag(resId);
                    imageView.setOnClickListener(this);
                    buttons.add(imageView);
                }
            }
        }
        return this;
    }

    public BaseFloatingButtons addShowAndHide(@IdRes int showResId, @IdRes int hideResId) {
        showButton = (ImageView) view.findViewById(showResId);
        showButton.setVisibility(View.VISIBLE);
        showButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show();
            }
        });
        hideButton = (ImageView) view.findViewById(hideResId);
        hideButton.setVisibility(View.GONE);
        hideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();
            }
        });
        return this;
    }

    public void setIconSize(Context context, int iconSizeDp) {
        for (ImageView button : buttons)
            MyView.setImageSize(context, iconSizeDp, button);
    }

    public void hide() {
        for (ImageView button : buttons)
            button.setVisibility(View.GONE);
        if (hideButton != null)
            hideButton.setVisibility(View.GONE);
        if (showButton != null)
            showButton.setVisibility(View.VISIBLE);
        isShowing = false;
    }

    public void show() {
        for (ImageView button : buttons)
            button.setVisibility(View.VISIBLE);
        if (hideButton != null)
            hideButton.setVisibility(View.VISIBLE);
        if (showButton != null)
            showButton.setVisibility(View.GONE);
        isShowing = true;
    }

    public void setImageSizeByDeviceSize(Context context) {
        for (ImageView button : buttons)
            MyView.setImageViewSizeByDeviceSize(context, button);
        MyView.setImageViewSizeByDeviceSize(context, showButton);
        MyView.setImageViewSizeByDeviceSize(context, hideButton);
    }

}
