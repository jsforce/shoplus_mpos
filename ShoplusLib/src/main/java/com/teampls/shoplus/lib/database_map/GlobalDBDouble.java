package com.teampls.shoplus.lib.database_map;

import android.content.Context;

import com.teampls.shoplus.lib.database_global.GlobalDB;

/**
 * Created by Medivh on 2018-11-19.
 */

public class GlobalDBDouble extends BaseDBValue<Double> {

    public GlobalDBDouble(String key, double defaultValue) {
        super(key, defaultValue);
    }

    @Override
    public KeyValueDB getDB(Context context) {
        return GlobalDB.getInstance(context);
    }

    @Override
    public void put(Context context, Double value) {
        getDB(context).put(key, value);
    }

    @Override
    public Double get(Context context) {
        return getDB(context).getDouble(key, defaultValue);
    }
}
