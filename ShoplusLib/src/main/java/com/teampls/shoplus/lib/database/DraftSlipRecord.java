package com.teampls.shoplus.lib.database;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.protocol.TransactionDraftDataProtocol;
import com.teampls.shoplus.lib.protocol.TransactionDraftMemoDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-08-22.
 */

public class DraftSlipRecord implements Comparable<DraftSlipRecord> {
    public int id = 0;
    public long draftId = 0, versionId = 0;
    public DateTime createdDateTime = Empty.dateTime, updatedDateTime = Empty.dateTime;
    public String recipientPhoneNumber = "";
    public List<MemoRecord> memos = new ArrayList();
    public MSortingKey sortingKey = MSortingKey.Date;
    // provider.phoneNumber와 createdDateTime을 통해 slipKey를 만들 수 있다

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] <%d> draft %d, version %d, created %s, updated %s, recipient %s, memos %s",
                location, id, draftId, versionId, createdDateTime.toString(BaseUtils.fullFormat), updatedDateTime.toString(BaseUtils.fullFormat), recipientPhoneNumber, ""));
    }

    public SlipRecord toSlipRecord(Context context) {
      SlipRecord result = new SlipRecord();
        result.createdDateTime = createdDateTime;
        if (updatedDateTime.isAfter(createdDateTime))
            result.salesDateTime = updatedDateTime;
        else
            result.salesDateTime = createdDateTime;
        result.ownerPhoneNumber = UserSettingData.getInstance(context).getProviderRecord().phoneNumber;
        result.counterpartPhoneNumber = recipientPhoneNumber;
        result.confirmed = false;
        return result;
    };

    public DraftSlipRecord() {};

    public DraftSlipRecord(int id, long draftId, long versionId, DateTime createdDateTime, DateTime updatedDateTime,
                           String recipientPhoneNumber, List<MemoRecord> memos) {
        this.id = id;
        this.draftId = draftId;
        this.versionId = versionId;
        this.createdDateTime = createdDateTime;
        this.updatedDateTime = updatedDateTime;
        this.recipientPhoneNumber = recipientPhoneNumber;
        this.memos = memos;
    }

    public DraftSlipRecord(TransactionDraftDataProtocol protocol) {
        this.draftId = protocol.getDraftId();
        this.versionId = protocol.getVersionId();
        this.createdDateTime = protocol.getCreatedDatetime();
        this.updatedDateTime = protocol.getUpdatedDatetime();
        this.recipientPhoneNumber = protocol.getRecipient();
        memos.clear();
        for (TransactionDraftMemoDataProtocol memo : protocol.getMemoList())
            memos.add(new MemoRecord(memo));
    }

    public boolean isSame(DraftSlipRecord record) {
        if (draftId != record.draftId) return false;
        if (versionId != record.versionId) return false;
        if (createdDateTime.equals(record.createdDateTime) == false) return false;
        if (updatedDateTime.equals(record.updatedDateTime) == false) return false;
        if (recipientPhoneNumber.endsWith(record.recipientPhoneNumber) == false) return false;
        if (memos.size() != record.memos.size()) return false;
        int index = 0;
        for (MemoRecord memo : memos) {
            if (memo.equals(record.memos.get(index)) == false)
                return false;
            index++;
        }
        return true;
    }

    public SlipDataKey getSlipDataKey(Context context) {
        return new SlipDataKey(UserSettingData.getInstance(context).getProviderRecord().phoneNumber, createdDateTime);

    }

    @Override
    public int compareTo(@NonNull DraftSlipRecord o) {
        switch (sortingKey) {
            default:
                return -createdDateTime.compareTo(o.createdDateTime);
        }
    }
}
