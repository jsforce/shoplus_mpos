package com.teampls.shoplus.lib.common;

import android.app.Activity;
import android.app.ListActivity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.awsservice.handler.BaseAuthenticationHandler;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.enums.LoginState;
import com.teampls.shoplus.lib.event.MyOnTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by Medivh on 2016-11-29.
 */

public class BaseAppWatcher extends Service {
    private static boolean isValidInstance = false;
    public static String appDir = "";
    private static Stack<Activity> activities = new Stack<>();
    //  private static ScreenWatcher screenWatcher = new ScreenWatcher();
    public static boolean isServerChanged = false;
    private static boolean networkConnected = true;
    private static MyApp myApp;
    protected static boolean activated = false;
    protected boolean initialized = false;
    protected MyCountDownTimer tokenWatcher;
    protected final int tickTime = 60 * 1000; // 1min
    private static LoginState loginState = LoginState.Default;
    private static final String KEY_isNewlyRegistered = "is.newly.registered";
    private static boolean onRestarting = false;
    public static final int MallItemCountLimit = 100;
    // service의 주임무는 토큰감시 (50분 지나갔는지)

    // Welcome의 기능을 가지고 와서 앱 중간에 메모리 소실시 복구를 해보자
    public static void autoRecover(Context context, final MyOnTask onTask) {
        if (context == null) {
            Log.w("DEBUG_JS", String.format("[BaseAppWatcher.autoLogIn] context == null"));
            return;
        }
        if (MyDevice.networkConnected(context) == false) {
            MyUI.toastSHORT(context, String.format("네트워크가 끊겨있습니다"));
            return;
        }

        if (isValidInstance)
            return;
        MyUI.toastSHORT(context, String.format("자동복구중..."));
        Log.w("DEBUG_JS", String.format("[BaseAppWatcher.autoRecover] isValidInstance == False"));

        // BaseWelcome.init()
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setNetworkConnected(true);
        UserHelper.init(context);

        // this.init()
        BaseAppWatcher.appDir = MyDevice.teamplDir + String.format("/%s", MyApp.get(context).toString());
        if (MyDevice.teamplDir.isEmpty())
            Log.w("DEBUG_JS", String.format("[BaseAppWatcher.autoLogIn] MyDevice.temaplDir isEmpty"));
        myApp = MyApp.get(context);
        MyDevice.setFontScale(context, BasePreference.getFontScale(context));

        // BaseWelcome.selectNextActivity()
        setValidInstance(true);
        context.startService(new Intent(context, BaseAppWatcher.class).setPackage(context.getPackageName()));

        // token
        if (isTokenExpired(context, "recover")) {
            MyDB myDB = MyDB.getInstance(context);
            setLoginState(context, LoginState.onTrying);
            UserHelper.getPool().getUser(myDB.getUserPoolNumber()).signOut();
            UserHelper.getPool().getUser(myDB.getUserPoolNumber()).getSessionInBackground(new MyAuthenticationHandler(context, onTask)); // 여기 종료시 UserSetting 확인
        } else {
            // userSetting
            UserSettingData.getInstance(context).autoRefresh();
        }


    }

    static class MyAuthenticationHandler extends BaseAuthenticationHandler {
        private MyOnTask onTask;

        public MyAuthenticationHandler(Context context, MyOnTask onTask) {
            super(context);
            this.onTask = onTask;
        }

        @Override
        public void onNotAuthorized() {
            MyUI.toastSHORT(context, String.format("인증 실패"));
        }

        @Override
        public void onTaskDone(Object result) {
            setLoginState(context, LoginState.onSuccess);
            UserSettingData.getInstance(context).autoRefresh();
            if (onTask != null)
                onTask.onTaskDone("");
        }

    }

    // 변수 저장시 KeyValueDB를 쓰는 이유는 Notification에서도 값을 읽어야 하기 때문
    public static void init(Context context, boolean isProduction, String appDir) {
        BaseAppWatcher.appDir = MyDevice.teamplDir + appDir;
        myApp = MyApp.get(context);
        loginState = LoginState.Default;
        onRestarting = false;
        clearActivities("init");
        if (KeyValueDB.getInstance(context).hasKey(BaseKey.isProduction))
            isServerChanged = (isProduction != KeyValueDB.getInstance(context).getBool(BaseKey.isProduction, true)); // 최초 설치시 문제
        KeyValueDB.getInstance(context).put(BaseKey.loginFailed, false);
        KeyValueDB.getInstance(context).put(BaseKey.appForeground, false);
        KeyValueDB.getInstance(context).put(BaseKey.isProduction, isProduction);
    }

    public static boolean checkAppPermission(Context context) {
        List<String> messages = new ArrayList<>();
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.CAMERA) == false)
            messages.add("카메라");
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.READ_CONTACTS) == false)
            messages.add("주소록");
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE) == false)
            messages.add("파일");
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.PHONE) == false)
            messages.add("전화");
        if (messages.size() >= 1)
            MyUI.toast(context, String.format("%s 권한이 없습니다", TextUtils.join(",", messages)));
        return (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE));
    }

    public static void setNewlyRegistered(Context context, boolean value) {
        KeyValueDB.getInstance(context).put(KEY_isNewlyRegistered, value);
    }

    public static boolean isNewlyRegistered(Context context) {
        return KeyValueDB.getInstance(context).getBool(KEY_isNewlyRegistered, false);
    }

    public static void setActivated(Boolean value) {
        activated = value;
    }

    public static boolean isActivated() {
        return BaseAppWatcher.activated; // null 값이 될 수 있음에 주의!
    }

    public static void setNetworkConnected(boolean value) {
        BaseAppWatcher.networkConnected = value;
    }

    public static boolean isNetworkConnected() {
        return networkConnected;
    }

    public static void setValidInstance(boolean value) {
        isValidInstance = value;
    }

    public static String getSimpleDir() {
        String result = appDir.replace(MyDevice.rootDir, "");
        if (result.endsWith("/"))
            result = result.substring(0, result.length() - 1);
        return result;
    }

    public static void setLoginState(final Context context, final LoginState loginState) {
        BaseAppWatcher.loginState = loginState;
        Log.w("DEBUG_JS", String.format("[AppWatcher.setLoginState] %s, log-in : %s", context.getClass().getSimpleName(), loginState.toString()));
        if (loginState == LoginState.onSuccess)
            KeyValueDB.getInstance(context).put(BaseKey.loginTime, System.currentTimeMillis());
    }

    @Override
    public void onDestroy() {
        onWatcherStop();
        super.onDestroy();
    }

    public static boolean isLoggedIn() {
        return loginState == LoginState.Default.onSuccess;
    }

    public static LoginState getLoginState() {
        return loginState;
    }

    public static boolean checkInstance(Activity activity) {
        return isValidInstance;
    }

    public static boolean checkInstanceAndRestart(final Activity activity) {
        if (isValidInstance) {
            if (activity == null)
                return false; // onCreate - refreshTab - onTabSelected 순서로 넘어오는 경우에는 activity가 null이다
            else
                return true;
        }

        if (onRestarting) // 중복 재시작 방지
            return false;
        onRestarting = true;
        if (activity instanceof ListActivity) {
            // Preference에서 넘어왔을때
        } else if (activity instanceof Activity) {
            activity.setContentView(R.layout.base_empty);
        } else {
            Log.e("DEBUG_JS", String.format("[BaseAppWatcher.checkInstanceAndRestart] invalid activity: %s", activity.getClass().getSimpleName()));
        }

        // restart
        new MyDelayTask(activity, 1000) {
            @Override
            public void onFinish() {
                MyDevice.openApp(activity, activity.getPackageName());
                onRestarting = false;
                //activity.startActivity(new Intent(activity,  welcomeClass));
                Log.w("DEBUG_JS", String.format("[%s.checkInstanceAndRestart] inValidInstance == FALSE, restarting....", activity.getClass().getSimpleName()));
            }
        };
        activity.finishAffinity();  // 실제 종료될 시간을 줘야 한다
        return false;
    }

    public static void onActivityCreate(Activity activity) {
        activities.push(activity);
    }

    public static void onResume(Context context, String currentActivityName) {
        //GlobalDB.getInstance(context).put(myApp.getKeyCurrentActivity(), currentActivityName);
        Log.i("DEBUG_JS", String.format("[BaseAppWatcher.onResume] %s", currentActivityName));
    }

//    @Deprecated
//    private static void startScreenWatcher(Activity activity) {
//        try {
//            IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
//            filter.addAction(Intent.ACTION_SCREEN_OFF);
//            activity.registerReceiver(BaseAppWatcher.screenWatcher, filter);
//        } catch (IllegalArgumentException e) {
//            Log.w("DEBUG_JS", String.format("[BaseAppWatcher.startScreenWatcher] already registered..."));
//        }
//    }
//
//    public static boolean isScreenOn() {
//        return screenWatcher.isScreenOn;
//    }

    public static boolean isForeground(Context context) {
        return KeyValueDB.getInstance(context).getBool(BaseKey.appForeground);
    }

    public static void onActivityResume(Context context) {
        //   Log.i("DEBUG_JS", String.format("[BaseAppWatcher.onActivityResume] %s", context.getClass().getSimpleName()));
        KeyValueDB.getInstance(context).put(BaseKey.appForeground, true);
    }

    public static void onActivityPause(Context context) {
        // Log.i("DEBUG_JS", String.format("[BaseAppWatcher.onActivityPause] %s", context.getClass().getSimpleName()));
        KeyValueDB.getInstance(context).put(BaseKey.appForeground, false);
    }

    public static void onActivityDestroy(Context context) {
        if (activities.isEmpty()) {
            Log.w("DEBUG_JS", String.format("[%s.onActivityDestroy] activities == empty", context.getClass().getSimpleName()));
            return;
        } else {
            activities.pop();
        }
        //   showActivityStack();
    }

    public static
    @Nullable
    Activity getCurrentActivity() {
        if (activities == null)
            return null;
        if (activities.isEmpty()) {
            return null;
        } else {
            return activities.peek();
        }
    }

    public static boolean hasActiveActivity(Context context) {
        if (activities == null) return false;
        if (activities.isEmpty()) return false;
        if (isValidInstance == false) return false;
        if (isForeground(context) == false) return false;
        return true;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private static void clearActivities(String callLocation) {
        activities.clear();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            // 메모리 상황으로 강제 종료 후 상황 호전으로 재시작되었을때
            onWatcherStop();
        } else {
            // 앱 시작시 호출
            onWatcherStart();
        }

        switch (flags) {
            case START_STICKY:
                Log.w("DEBUG_JS", String.format("[BaseAppWatcher.onStartCommand] START_STICKY"));
                break;
            case START_NOT_STICKY:
                Log.w("DEBUG_JS", String.format("[BaseAppWatcher.onStartCommand] START_NOT_STICKY"));
                break;
            case START_REDELIVER_INTENT:
                Log.w("DEBUG_JS", String.format("[BaseAppWatcher.onStartCommand] START_REDELIVER_INTENT"));
                break;
        }
        return super.onStartCommand(intent, flags, startId);
    }

    protected void onWatcherStart() {
        // Log.i("DEBUG_JS", String.format("[BaseAppWatcher.onWatcherStart] ...."));
        KeyValueDB.getInstance(getApplicationContext()).put(BaseKey.appAlive, true);
        isValidInstance = true;
        if (tokenWatcher != null)
            tokenWatcher.cancel();
        tokenWatcher = new MyCountDownTimer(tickTime) {
            @Override
            public void onTick(long millsLeft) {
                if (initialized == false) {
                    initialized = true;
                    return;
                }
                Context context = getApplicationContext();

                if (context == null) {
                    Log.w("DEBUG_JS", String.format("[AppWatcher.onTick] context == null"));
                    return;
                }

                if (isValidInstance == false)
                    Log.w("DEBUG_JS", String.format("[AppWatcher.onTick] isValidInstance == FALSE"));

            }
        };
        //  tokenWatcher.start(); 타이머는 필요시까지 가동하지 않는다
        if (initialized = false)
            initialized = true;
    }

    // 인스턴스가 삭제되었다가 실행될 수 있으므로 변수가 있어서는 안된다.
    protected void onWatcherStop() {
        stopSelf();
        KeyValueDB.getInstance(getApplicationContext()).put(BaseKey.appAlive, false);
        KeyValueDB.getInstance(getApplicationContext()).put(BaseKey.appForeground, false);
        isValidInstance = false;
        myApp = MyApp.get(getApplicationContext());
        clearActivities("onWatcherStop");
        if (MyDevice.hasPermission(getApplicationContext(), MyDevice.UserPermission.WRITE_STORAGE))
            GlobalDB.getInstance(getApplicationContext()).put(myApp.getKeyCurrentActivity(), "");
        if (tokenWatcher != null)
            tokenWatcher.cancel();
        Log.i("DEBUG_JS", String.format("[AppWatcher.onWatcherStop] ..."));
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) { // Recent app list에서 제거될때
        Log.w("DEBUG_JS", String.format("[AppWatcher.onTaskRemoved] ...."));
        onWatcherStop();
    }

//    static class ScreenWatcher extends BroadcastReceiver {
//        public boolean isScreenOn = true;
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
//                isScreenOn = false;
//                // Log.i("DEBUG_JS", String.format("[ScreenWatcher.onReceive] %s, Screen off", context.getClass().getSimpleName()));
//            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
//                isScreenOn = true;
//                //    Log.i("DEBUG_JS", String.format("[ScreenWatcher.onReceive] %s, Screen on", context.getClass().getSimpleName()));
//            }
//        }
//    }

    public static boolean isTokenExpired(Context context, String location) {
        long loginPeriod_ms = System.currentTimeMillis() - KeyValueDB.getInstance(context).getLong(BaseKey.loginTime);
        if (loginPeriod_ms / (1000 * 60) >= 50) {
            return true;
        } else {
            return false;
        }

    }
}

/*
  @Deprecated
    private static void generateNotification(Context context, String title, String message, Class popupActivity) {
        if (BasePreference.isNotifyingMessage(context) == false) return;
        boolean doVibrate = BasePreference.isVibrating(context);
        boolean doSound = BasePreference.isSounding(context);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.vvip_notify))
                .setSmallIcon(R.drawable.send_notif_small)
                .setContentTitle(title)
                .setContentText(message)
                .setTicker(message)
                .setAutoCancel(true);
        if (isForeground(context) || (popupActivity == null)) {
            builder.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT));
        } else {
            Intent intent = new Intent(context, popupActivity);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            builder.setContentIntent(PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT));
        }
        if (doVibrate)
            builder.setVibrate(new long[]{1000, 1000, 1000});
        if (doSound)
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.build().flags |= Notification.FLAG_AUTO_CANCEL;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }

 */