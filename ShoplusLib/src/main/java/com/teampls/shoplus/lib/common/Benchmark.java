package com.teampls.shoplus.lib.common;

import android.util.Log;

/**
 * Created by Medivh on 2016-05-10.
 */
public class Benchmark {
    private long prevTimeMillis = 0;

    public Benchmark() {
        start();
        Log.w("DEBUG_JS", String.format("[Benchmark.Benchmark]  === start ==="));
    }

    public void start() {
        prevTimeMillis = System.currentTimeMillis();
    };

    public String check(String taskName) {
        long currentTimeMills = System.currentTimeMillis();
        String result =  String.format("%.3f s", ((double) (currentTimeMills - prevTimeMillis)) / 1000);
        Log.w("DEBUG_JS", String.format("[%s] %s: %s", getClass().getSimpleName(), taskName, result));
        prevTimeMillis = currentTimeMills;
        return result;
    }

    public long getTimeMs(boolean resetTime) {
        long currentTimeMills = System.currentTimeMillis();
        if (resetTime)
            prevTimeMillis = currentTimeMills;
        return System.currentTimeMillis()-currentTimeMills;
    }
}