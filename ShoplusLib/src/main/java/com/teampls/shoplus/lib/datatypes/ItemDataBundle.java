package com.teampls.shoplus.lib.datatypes;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.ItemJSONData;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 아이템 데이터와 재고 데이터, 원가 데이터(사장 only)의 번들.
 * 서버로부터 수신한 데이터를 묶어서 반환하기 위한 용도로, Read-only 데이터이다.
 *
 * @author lucidite
 */

public class ItemDataBundle {
    private List<ItemDataProtocol> items;
    private Map<ItemStockKey, ItemOptionData> stock;
    private Map<Integer, Integer> costPrice;
    private List<Integer> bookmarks;

    public ItemDataBundle(SpecificItemDataBundle specificItemDataBundle) { // single item
        this.items = new ArrayList<>();
        items.add(specificItemDataBundle.getItem());
        this.stock = specificItemDataBundle.getStock();
        this.costPrice = new HashMap<>();
        costPrice.put(specificItemDataBundle.getItem().getItemId(), specificItemDataBundle.getCostPrice());

        // [NOTE] 개별 상품 정보에는 bookmark 정보가 포함되어 있지 않다.
        // 앱에서는 상품 동기화 시점에 이미 전체 bookmark 목록을 알고 있어야 한다 (기본 정보).
        this.bookmarks = new ArrayList<>();
    }

    public ItemDataBundle(List<ItemDataProtocol> items, Map<ItemStockKey, ItemOptionData> stocks) {
        this.items = items;
        this.stock = stocks;
        this.costPrice = new HashMap<>();
        this.bookmarks = new ArrayList<>();
    }

    public ItemDataBundle(JSONObject dataObj) throws JSONException {
        this.items = new ArrayList<>();
        this.costPrice = new HashMap<>();
        this.bookmarks = new ArrayList<>();

        JSONArray itemsArr = dataObj.getJSONArray("items");
        for (int i=0; i < itemsArr.length(); ++i) {
            JSONObject itemObj = itemsArr.getJSONObject(i);
            this.items.add(new ItemJSONData(itemObj));
        }

        JSONArray stockArr = dataObj.getJSONArray("stock");
        this.stock = ItemOptionData.buildStockFromStockDataArr(stockArr);

        JSONArray costPriceArr = dataObj.optJSONArray("cost_price");
        if (costPriceArr != null) {
            for (int i = 0; i < costPriceArr.length(); ++i) {
                JSONObject costPriceObj = costPriceArr.getJSONObject(i);
                int itemid = costPriceObj.getInt("id");
                int costPrice = costPriceObj.getInt("value");
                this.costPrice.put(itemid, costPrice);
            }
        }

        JSONArray bookmarkArr = dataObj.optJSONArray("bookmarks");
        if (bookmarkArr != null) {
            for (int i = 0; i < bookmarkArr.length(); ++i) {
                int bookmark = bookmarkArr.optInt(i, 0);
                if (bookmark > 0) {
                    this.bookmarks.add(Integer.valueOf(bookmark));
                }
            }
        }
    }

    public ItemDataBundle(JSONArray itemsArr) throws JSONException {
        this.items = new ArrayList<>();
        this.stock = new HashMap<>();
        this.costPrice = new HashMap<>();
        this.bookmarks = new ArrayList<>();

        for (int i=0; i < itemsArr.length(); ++i) {
            JSONObject itemObj = itemsArr.getJSONObject(i);
            this.items.add(new ItemJSONData(itemObj));
        }
    }

    public static ItemStockKey buildItemStockKey(JSONObject stockObj) throws JSONException {
        int itemid = stockObj.getInt("id");
        String colorStr = stockObj.optString("color", "");
        String sizeStr = stockObj.optString("size", "");

        return new ItemStockKey(itemid, ColorType.ofRemoteStr(colorStr), SizeType.ofRemoteStr(sizeStr));
    }

    public List<ItemDataProtocol> getItems() {
        return items;
    }

    public Map<ItemStockKey, ItemOptionData> getStock() {
        return stock;
    }

    public Map<Integer, Integer> getCostPrice() {
        return costPrice;
    }

    public List<Integer> getBookmarks() {
        return bookmarks;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[ItemDataBundle.toLogCat] == items =="));
        for (ItemDataProtocol itemDataProtocol : items)
            new ItemRecord(itemDataProtocol).toLogCat(location);

        Log.i("DEBUG_JS", String.format("[ItemDataBundle.toLogCat] == costs =="));
        for (int itemId : costPrice.keySet())
            Log.i("DEBUG_JS", String.format("[ItemDataBundle.toLogCat] itemId %d, cost %d", itemId, costPrice.get(itemId)));

    }

    public void toLogCatOption(String location) {
        for (ItemStockKey option : stock.keySet()) {
            Log.i("DEBUG_JS", String.format("[%s] %s : %d", location, option.toString(" / "), stock.get(option)));
        }
    }



}
