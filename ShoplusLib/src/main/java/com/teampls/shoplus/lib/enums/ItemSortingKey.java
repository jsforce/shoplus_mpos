package com.teampls.shoplus.lib.enums;

import com.teampls.shoplus.lib.database_memory.MSortingKey;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-12-16.
 */

public enum ItemSortingKey {
    ItemId("최신순", MSortingKey.ID), RecentVisited("최근조회순", MSortingKey.Scheduled),
    StockCount("재고순", MSortingKey.Count), Name("가나다순", MSortingKey.Name);
    public String uiStr = "";
    public MSortingKey mSortingKey;

    ItemSortingKey(String uiStr, MSortingKey mSortingKey) {
        this.uiStr = uiStr;
        this.mSortingKey = mSortingKey;
    }

    public static List<ItemSortingKey> getDefaultValues() {
        List<ItemSortingKey> results = new ArrayList<>();
        results.add(ItemId);
        results.add(StockCount);
        results.add(Name);
        return results;
    }

    public static ItemSortingKey valueOfWithDefault(String name) {
        try {
            return valueOf(name);
        } catch (IllegalArgumentException e) {
            return ItemSortingKey.ItemId;
        }
    }

    public static ItemSortingKey valueOfUiStr(String uiString) {
        for (ItemSortingKey value : values()) {
            if (value.uiStr.equals(uiString)) {
                return value;
            }
        }
        return ItemId;
    }

    public static ItemSortingKey valueOfMSortingKey(MSortingKey mSortingKey) {
        for (ItemSortingKey itemSortingKey : values())
            if (itemSortingKey.mSortingKey == mSortingKey)
                return itemSortingKey;
        return ItemSortingKey.ItemId;
    }
}
