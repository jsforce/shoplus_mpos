package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import com.teampls.shoplus.lib.protocol.TransactionDraftMemoDataProtocol;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;

/**
 * @author lucidite
 */

public class TransactionDraftMemoJSONData implements TransactionDraftMemoDataProtocol {
    private JSONObject data;

    public TransactionDraftMemoJSONData(JSONObject dataObj) {
        this.data = dataObj;
    }

    @Override
    public DateTime getCreatedDateTime() {
        return new DateTime(
                this.data.optLong("memo_id", 0) * 1000,
                DateTimeZone.forOffsetHours(9));
    }

    @Override
    public String getMessage() {
        return this.data.optString("msg", "");
    }
}
