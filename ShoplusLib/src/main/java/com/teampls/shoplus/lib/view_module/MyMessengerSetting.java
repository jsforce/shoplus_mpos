package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.datatypes.MessengerLinkData;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.MessengerType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.ImagePickerView;
import com.teampls.shoplus.lib.view_base.BaseModule;

import java.util.List;

/**
 * Created by Medivh on 2019-01-31.
 */

public class MyMessengerSetting extends BaseModule {
    private LinearLayout talkContainer, friendContainer;
    private MyRadioGroup<MessengerType> rgMessengerType;
    private TextView tvKakaoQrUrl, tvFriendUrl, tvWechatQrUrl;
    private MyCheckBox cbPrintKakaoQR, cbPrintFriendQR;

    public MyMessengerSetting(final Context context, final View view) {
        super(context, view);
        tvKakaoQrUrl = findTextViewById(R.id.messenger_kakaoQr_url, "");
        tvFriendUrl = findTextViewById(R.id.messenger_friend_url, "");
        tvWechatQrUrl = findTextViewById(R.id.messenger_wechatQr_url, "");
        talkContainer = view.findViewById(R.id.messenger_kakao_container);
        friendContainer = view.findViewById(R.id.messenger_friend_container);
        cbPrintKakaoQR = new MyCheckBox(globalDB, view, R.id.messenger_kakaoQr_checkbox, GlobalDB.KEY_PRINTER_QR_Kakao, false);
        cbPrintFriendQR = new MyCheckBox(globalDB, view, R.id.messenger_friend_checkbox, GlobalDB.KEY_PRINTER_QR_Kakao, false);
        new MyCheckBox(globalDB, view, R.id.messenger_wechatQr_checkbox, GlobalDB.KEY_PRINTER_QR_Wechat, false);

        rgMessengerType = new MyRadioGroup<>(context, view);
        rgMessengerType.add(R.id.messenger_messenger_useTalk, MessengerType.KAKAOTALK);
        rgMessengerType.add(R.id.messenger_messenger_useFriend, MessengerType.PLUSFRIEND);
        rgMessengerType.setChecked(userSettingData.getActiveMessenger());
        rgMessengerType.setOnClickListener(false, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUI.toastSHORT(context, String.format("주소를 입력하면 %s로 연결됩니다", rgMessengerType.getCheckedItem().uiStr));
                refresh();
            }
        });

        setOnClick(R.id.messenger_friend_url, R.id.messenger_kakaoQr_url, R.id.messenger_wechatQr_url);
    }

    public void setCheck(MessengerType messengerType) {
        rgMessengerType.setChecked(messengerType);
    }

    public void refresh() {
        switch (rgMessengerType.getCheckedItem()) {
            case KAKAOTALK:
                talkContainer.setVisibility(View.VISIBLE);
                friendContainer.setVisibility(View.GONE);
                cbPrintKakaoQR.refresh();
                break;
            case PLUSFRIEND:
                talkContainer.setVisibility(View.GONE);
                friendContainer.setVisibility(View.VISIBLE);
                cbPrintFriendQR.refresh();
                break;
        }

        MyMap<MessengerType, MessengerLinkData> messengerLinkMap = userSettingData.getMessengerLinkMap();
        String kakaoValue = messengerLinkMap.get(MessengerType.KAKAOTALK).getURLStr();
        if (kakaoValue.isEmpty()) {
            tvKakaoQrUrl.setText("(미입력)");
            tvKakaoQrUrl.setTextColor(ColorType.Gray.colorInt);
        } else {
            tvKakaoQrUrl.setText(kakaoValue);
            tvKakaoQrUrl.setTextColor(ColorType.Black.colorInt);
        }

        String friendValue = messengerLinkMap.get(MessengerType.PLUSFRIEND).getURLStr();
        if (friendValue.isEmpty()) {
            tvFriendUrl.setText("(미입력)");
            tvFriendUrl.setTextColor(ColorType.Gray.colorInt);
        } else {
            tvFriendUrl.setText(friendValue);
            tvFriendUrl.setTextColor(ColorType.Black.colorInt);
        }

        String weChatValue = messengerLinkMap.get(MessengerType.WECHAT).getURLStr();
        if (weChatValue.isEmpty()) {
            tvWechatQrUrl.setText("(미입력)");
            tvWechatQrUrl.setTextColor(ColorType.Gray.colorInt);
        } else {
            tvWechatQrUrl.setText(weChatValue);
            tvWechatQrUrl.setTextColor(ColorType.Black.colorInt);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.messenger_friend_url) {
            new OnURLClick(context, MessengerType.PLUSFRIEND, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    rgMessengerType.setChecked(userSettingData.getActiveMessenger());
                    refresh();
                }
            });

        } else if (view.getId() == R.id.messenger_kakaoQr_url) {
            new OnURLClick(context, MessengerType.KAKAOTALK, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    rgMessengerType.setChecked(userSettingData.getActiveMessenger());
                    refresh();
                }
            });

        } else if (view.getId() == R.id.messenger_wechatQr_url) {
            new OnURLClick(context, MessengerType.WECHAT, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    rgMessengerType.setChecked(userSettingData.getActiveMessenger());
                    refresh();
                }
            });
        }
    }

    public static class OnURLClick extends MyButtonsDialog {
        private MyOnTask onTask;

        public OnURLClick(Context context, MessengerType messengerType, MyOnTask onTask) {
            super(context, messengerType.uiStr + " 주소 입력", "");
            this.onTask = onTask;
            switch (messengerType) {
                case KAKAOTALK:
                    onTalkClick();
                    show();
                    break;
                case WECHAT:
                    onWeChatClick();
                    show();
                    break;
                case PLUSFRIEND:
                    onFriendClick(); // 버튼창은 띄우지 않는다
                    break;
                default:
                    break;
            }
        }

        private void onTalkClick() {
            addButton("QR 이미지에서 읽어오기", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ImagePickerView.start(context, 1, true, new MyOnClick<List<String>>() {
                        @Override
                        public void onMyClick(View view, List<String> imagePaths) {
                            if (imagePaths.size() == 0) // 시작 옵션에 의해 발생하지는 않는다
                                return;
                            String kakaoLink = MyGraphics.decodeQrImage(context, imagePaths.get(0));
                            if (kakaoLink.isEmpty())
                                return;
                            if (kakaoLink.contains(MessengerLinkData.kakaoHeader) == false) {
                                MyUI.toastSHORT(context, String.format("카카오톡 주소가 아닙니다. 확인해 주세요."));
                                return;
                            }
                            dismiss();
                            userSettingData.updateMessenger(MessengerType.KAKAOTALK, kakaoLink, onTask);
                            MyUI.toastSHORT(context, "카카오톡으로 연결합니다");
                        }
                    });
                }
            });

            addButton("직접 입력하기", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new UpdateValueDialog(context, "카카오 주소 입력", "", userSettingData.getMessengerLinkMap().get(MessengerType.KAKAOTALK).getURLStr()) {
                        @Override
                        public void onApplyClick(String newValue) {
                            if ((newValue.isEmpty() == false) && (newValue.contains(MessengerLinkData.kakaoHeader) == false))
                                MyUI.toastSHORT(context, String.format("카카오톡 주소가 아닙니다. 확인해 주세요."));
                            dismiss();
                            userSettingData.updateMessenger(MessengerType.KAKAOTALK, newValue, onTask);
                            MyUI.toastSHORT(context, "카카오톡으로 연결합니다");
                        }
                    };
                }
            });
        }

        private void onWeChatClick() {
            addButton("QR 이미지에서 읽어오기", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ImagePickerView.start(context, 1, true, new MyOnClick<List<String>>() {
                        @Override
                        public void onMyClick(View view, List<String> imagePaths) {
                            if (imagePaths.size() == 0) // 시작 옵션에 의해 발생하지는 않는다
                                return;
                            String weChatLink = MyGraphics.decodeQrImage(context, imagePaths.get(0));
                            if (weChatLink.contains(MessengerLinkData.weChatHeader) == false) {
                                MyUI.toastSHORT(context, String.format("위챗 주소가 아닙니다. 확인해 주세요."));
                                return;
                            }
                            dismiss();
                            userSettingData.updateMessenger(MessengerType.WECHAT, weChatLink, onTask);
                        }
                    });
                }
            });

            addButton("직접 입력하기", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new UpdateValueDialog(context, "위챗 주소 입력", "", userSettingData.getMessengerLinkMap().get(MessengerType.WECHAT).getURLStr()) {
                        @Override
                        public void onApplyClick(String newValue) {
                            if ((newValue.isEmpty() == false) && (newValue.contains(MessengerLinkData.weChatHeader) == false))
                                MyUI.toastSHORT(context, String.format("위챗 주소가 아닙니다. 확인해 주세요."));
                            dismiss();
                            userSettingData.updateMessenger(MessengerType.WECHAT, newValue, onTask);
                        }
                    };
                }
            });
        }

        private void onFriendClick() {
            new UpdateValueDialog(context, "플러스 친구 주소 입력", "플러스친구화면 > 우상단 ... > URL 복사하기",
                    userSettingData.getMessengerLinkMap().get(MessengerType.PLUSFRIEND).getURLStr()) {
                @Override
                public void onApplyClick(String newValue) {
                    if ((newValue.isEmpty() == false) && (newValue.contains(MessengerLinkData.plusHeader) == false))
                        MyUI.toastSHORT(context, String.format("플러스 친구 주소가 아닙니다. 확인해 주세요."));
                    dismiss();
                    userSettingData.updateMessenger(MessengerType.PLUSFRIEND, newValue, onTask);
                    MyUI.toastSHORT(context, "플러스 친구로 연결합니다");
                }
            };
        }
    }
}
