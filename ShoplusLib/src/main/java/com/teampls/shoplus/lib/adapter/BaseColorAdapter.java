package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2017-03-31.
 */

public class BaseColorAdapter extends BaseDBAdapter<ColorType> {
    public static final int ViewOptionDialog = 0, ViewSlipAddition = 1, ColorSettingView = 2;
    private double dialogWidthRatio = 1.0;

    public BaseColorAdapter(Context context, double dialogWidthRatio) {
        super(context);
        this.dialogWidthRatio = dialogWidthRatio;
    }

    @Override
    public void generate() {

    }

    @Override
    public BaseViewHolder getViewHolder() {
        switch (viewType) {
            case ViewOptionDialog:
                return new CheckableViewHolder(5);
            case ViewSlipAddition:
                return new CheckableViewHolder(5);
            case ColorSettingView:
                return new ViewHolder(5);
        }
        return new ViewHolder();
    }

    class ViewHolder extends BaseViewHolder {
        private ImageView ivColor, ivChecked;
        private TextView tvName;
        private int columnNum = 0;
        private FrameLayout container;

        public ViewHolder() {};

        public ViewHolder(int columnNum) {
            this.columnNum = columnNum;
        }

        @Override
        public int getThisView() {
            return R.layout.cell_color;
        }

        @Override
        public void init(View view) {
            container = (FrameLayout) view.findViewById(R.id.cell_color_container);
            ivColor = (ImageView) view.findViewById(R.id.cell_color_imageView);
            tvName = findTextViewById(context, view, R.id.cell_color_name);
            ivChecked = (ImageView) view.findViewById(R.id.cell_color_checked);
            ivChecked.setVisibility(View.GONE);
            if (columnNum > 0) {
                int width = MyDevice.getWidth(context) / columnNum;
                int height = (int) (width * 0.6 * dialogWidthRatio);
              //  FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
             //   container.setLayoutParams(params);
                ivColor.getLayoutParams().width = width;
                ivColor.getLayoutParams().height = height;
            }
            MyView.setImageViewSizeByDeviceSize(context, ivChecked);
        }

        @Override
        public void update(int position) {
            ColorType color = records.get(position);
            ivColor.setBackgroundColor(color.colorInt);
            setTextTitle(tvName, true, UserSettingData.getColorName(color));
            tvName.setTextColor(color.textColor);
         //   Log.i("DEBUG_JS", String.format("[LogViewHolder.update] %s, %s", color.toString(), globalDB.getUiName(color)));
        }
    }

    class CheckableViewHolder extends BaseViewHolder {
        private ImageView ivColor, ivChecked;
        private FrameLayout container;
        private TextView tvName;
        private int columnNum = 0;

        @Override
        public int getThisView() {
            return R.layout.cell_color;
        }

        public CheckableViewHolder(int columnNum) {
            this.columnNum = columnNum;
        }

        @Override
        public void init(View view) {
            container = (FrameLayout) view.findViewById(R.id.cell_color_container);
            ivColor = (ImageView) view.findViewById(R.id.cell_color_imageView);
            tvName = (TextView) view.findViewById(R.id.cell_color_name);
            ivChecked = (ImageView) view.findViewById(R.id.cell_color_checked);
            if (columnNum > 0) {
                int width = MyDevice.getWidth(context) / columnNum;
                int height = (int) (width * 0.6 * dialogWidthRatio);
                ivColor.getLayoutParams().width = width;
                ivColor.getLayoutParams().height = height;
            }
            MyView.setTextViewByDeviceSize(context, tvName);
        }

        @Override
        public void update(int position) {
            ColorType color = records.get(position);
            ivColor.setBackgroundColor(color.colorInt);
            setTextTitle(tvName, true, UserSettingData.getColorName(color));
            tvName.setTextColor(color.textColor);
            ivChecked.setVisibility(clickedPositions.contains(position)? View.VISIBLE : View.GONE);
        }
    }
}
