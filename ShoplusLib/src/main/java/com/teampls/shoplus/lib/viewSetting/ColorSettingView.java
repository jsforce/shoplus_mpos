package com.teampls.shoplus.lib.viewSetting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseColorAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseActivity;

import java.util.Map;

/**
 * Created by Medivh on 2017-09-12.
 */
//  유사 UI : BaseItemOptionDialog
public class ColorSettingView extends BaseActivity implements AdapterView.OnItemClickListener {
    private BaseColorAdapter adapter;
    private GridView gridView;
    private static MyOnClick onCloseClick;

    public static void startActivity(Context context, MyOnClick onCloseClick) {
        ColorSettingView.onCloseClick = onCloseClick;
        context.startActivity(new Intent(context, ColorSettingView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        adapter = new BaseColorAdapter(context, 1);
        adapter.setViewType(BaseColorAdapter.ColorSettingView).setRecords(ColorType.getMainColors());
        gridView = (GridView) findViewById(R.id.color_setting_gridView);
        gridView.setOnItemClickListener(this);
        gridView.setAdapter(adapter);
        setOnClick(R.id.color_setting_cancel);
        MyView.setTextViewByDeviceSize(context, getView(), R.id.color_setting_guide, R.id.color_setting_cancel);
    }

    @Override
    public int getThisView() {
        return R.layout.base_color_setting;
    }

    public void refresh() {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                adapter.generate();
            };

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
            }
        };
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.color_setting_cancel) {
            doFinish();
        }
    }

    protected void doFinish() {
        if (onCloseClick != null)
            onCloseClick.onMyClick(null, "");
        finish();
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.inquiry)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case inquiry:
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                break;
            case close:
               doFinish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        final ColorType record = adapter.getRecord(position);
        new UpdateValueDialog(context, "색상이름 변경", "", UserSettingData.getColorName(record)) {
            @Override
            public void onApplyClick(final String newName) {
                if (newName.equals(UserSettingData.getColorName(record)))
                    return;

                new CommonServiceTask(context, "ColorSettingView") {

                    @Override
                    public void doInBackground() throws MyServiceFailureException {
                        Map<ColorType, String> customColorNames = UserSettingData.getCustomColorNames();
                        customColorNames.put(record, newName);
                        mainShopService.updateColorNames(userSettingData.getUserShopInfo(), customColorNames);      // [SILVER-359] deprecated
                        userSettingData.updateCustomColorName(customColorNames);
                    }

                    @Override
                    public void onPostExecutionUI() {
                        refresh();
                    }
                };
            }
        };
    }
}
