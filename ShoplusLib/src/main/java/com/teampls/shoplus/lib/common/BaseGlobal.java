package com.teampls.shoplus.lib.common;

/**
 * Created by Medivh on 2016-09-03.
 */
public class BaseGlobal {

    public static final int ImageWidthMax = 720;
    public static final int ImageFileSizeMax = 500; // kB
    public static final int ImageAlpha = 120;
    public static final String delimiter = ";", optionDelimiter = ":";
    public static final int RecentItemLimit = 30;
    public static final int KakaoLinkImageWidth = 760;

    public static final int RequestRefresh = 9988;
    public static final int RequestPermission = 9989;
    public final static int REQUST_MENU_REFRESH = 54271;

    public static final String tempHeader = "temp";
    public static final String Kakao_Plus_Shoplus = "http://pf.kakao.com/_xapJwC/chat";
    public static final String SHOPL_US = "https://shopl-us.com";
    public static final String SHOPL_INSTALL_GUIDE = "https://shopl-us.com/for-retails/";

    public static final String GcmToken = "GCMToken";
    public static final String KEY_SELECTED_IMAGE_PATHS = "imagePaths";
    public static final String KakaoPackage = "com.kakao.talk";
    public static final String TermsOfServiceUrl = "https://s3.ap-northeast-2.amazonaws.com/sps-res-public/terms-and-policies/terms-of-service.html";
    public static final String PrivacyPolicyUrl = "https://s3.ap-northeast-2.amazonaws.com/sps-res-public/terms-and-policies/privacy-policy.html";

    public static final String DiscountHeader = "세일_";
}
