package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2019-01-14.
 */

public enum WorkingTime {
    AllDay, Day, Night
}
