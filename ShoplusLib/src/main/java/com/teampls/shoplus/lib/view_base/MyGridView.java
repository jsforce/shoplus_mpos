package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.view.View;
import android.widget.AbsListView;
import android.widget.GridView;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.event.MyOnTask;

/**
 * Created by Medivh on 2018-08-05.
 */

public class MyGridView  implements AbsListView.OnScrollListener {
    private Context context;
    private GridView gridView;
    private BaseDBAdapter adapter;
    private boolean onNotifyDataSetChanged = false;
    private MyOnTask<Boolean> onScrollTask;

    public MyGridView(Context context, View view, int resId) {
        this.gridView = (GridView) view.findViewById(resId);
        gridView.setNumColumns(MyDevice.getColumnNum(context));
        gridView.setOnScrollListener(this);
    }

    public void setAdapter(BaseDBAdapter adapter) {
        this.adapter = adapter;
        gridView.setAdapter(adapter);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        adapter.setScrollState(scrollState);
        switch (scrollState) {
            case SCROLL_STATE_FLING:
                break;
            case SCROLL_STATE_IDLE:
                if (gridView.getFirstVisiblePosition() == 0) {
                    if (onNotifyDataSetChanged) return;
                    onNotifyDataSetChanged = true;
                    new MyDelayTask(context, 1) {
                        @Override
                        public void onFinish() {
                            adapter.notifyDataSetChanged(); // 새로 싹 그려주는게 모든 문제를 해결한다
                            onNotifyDataSetChanged = false;
                        }
                    };
                }
                break;
        }
    }

    public void setOnScroll(MyOnTask<Boolean> onScrollTask) {
        this.onScrollTask = onScrollTask;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (adapter != null)
            adapter.firstVisibleItem = firstVisibleItem;
        if (onScrollTask != null)
            onScrollTask.onTaskDone(gridView.getFirstVisiblePosition() == 0);
    }

    public GridView getThis() {
        return gridView;
    }
}
