package com.teampls.shoplus.lib.database_memory;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.enums.DBResultType;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2018-01-18.
 */

// list와 map DB로 구분함
abstract public class BaseListDB<T> extends BaseMemoryDB<T> {
    protected List<T> database = new ArrayList<>();

    // id 관리에 주의해야 한다
    // SQLite를 따라 id는 1부터 시작한다, 메모리는 0번 부터이므로 이 차이를 늘 확인해야 한다
    public BaseListDB(Context context, String dbName) {
        this.context = context;
        this.DB_NAME = dbName;
    }

    public DBResult insert(T record) {
        database.add(record);
        return new DBResult(database.size(), DBResultType.INSERTED);
    }

    public void insertAll(List<T> records) {
        for (T record : records)
            insert(record);
    }

    public DBResult update(int id, T record) {
        if (id <= 0) {
            Log.w("DEBUG_JS", String.format("[%s.update] id = 0", DB_NAME));
            return new DBResult(0, DBResultType.INSERT_FAILED);
        }
        database.set(id - 1, record);
        return new DBResult(id, DBResultType.INSERTED);
    }

    public int getId(T record) {
        return database.indexOf(record) + 1; // 없으면 -1 --> 0번 리턴
    }

    public void update(T record) {
        int id = getId(record);
        if (id > 0)
            update(id, record);
    }

    public void updateOrInsert(T record) {
        if (database.contains(record)) {
            update(record);
        } else {
            insert(record);
        }
    }

    public boolean delete(T record) {
        return database.remove(record);
    }

    // 매우 조심해서 써야 함, 특히 For문안에서 삭제 동작을 하면 안됨
    // class java.util.ConcurrentModificationException, null
    public void deleteAll(List<T> records) {
        database.removeAll(records);
    }

    public void clear() {
        database.clear();
    }

    public T getRecord(int id) {
        if (id <= 0 || database.size() < id)
            return getEmptyRecord();
        return database.get(id - 1);
    }

    public List<T> getRecords() {
        return database;
    }

    public List<T> getRecords(String columnName, String value) {
        List<T> results = new ArrayList<>();
        if (database.size() == 0)
            return results;
        try {
            Field field = database.get(0).getClass().getDeclaredField(columnName);
            field.setAccessible(true);
            for (T record : database) {
                if (value.equals(field.get(record))) // Type을 확인할 수 있다
                    results.add(record);
            }
        } catch (NoSuchFieldException e) {
            Log.e("DEBUG_JS", String.format("[BaseListDB.getRecordByKey] %s", e.getLocalizedMessage()));
        } catch (IllegalAccessException e) {
            Log.e("DEBUG_JS", String.format("[BaseListDB.getRecordByKey] %s", e.getLocalizedMessage()));
        }
        return results;
    }

    // Map의 Sorting 기능을 활용한다
    public List<T> getRecordsOrderBy(String columnName, boolean isDescending) {
        List<T> results = new ArrayList<>();
        try {
            Field field = getEmptyRecord().getClass().getDeclaredField(columnName);
            field.setAccessible(true);
            Map<T, String> sortingRecords = new HashMap<>();
            for (T record : getRecords())
                sortingRecords.put(record, field.get(record).toString());
            sortingRecords = BaseUtils.sortByValue(sortingRecords);
            results = new ArrayList<>(sortingRecords.keySet());
            if (isDescending)
                Collections.reverse(results);
            return results;
        } catch (NoSuchFieldException e) {
            Log.e("DEBUG_JS", String.format("[%s.getRecordsOrderBy] %s", DB_NAME, e.getLocalizedMessage()));
            return results;
        } catch (IllegalAccessException e) {
            Log.e("DEBUG_JS", String.format("[%s.getRecordsOrderBy] %s", DB_NAME, e.getLocalizedMessage()));
            return results;
        }
    }
}