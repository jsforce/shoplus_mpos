package com.teampls.shoplus.lib.datatypes;

import org.json.JSONObject;

/**
 * 잔금 및 매입 관련 상태 데이터 클래스
 *
 * @author lucidite
 */

public class AccountsData {
    private int onlineAmount;
    private int entrustedAmount;
    private int depositAmount;

    public AccountsData(int onlineAmount, int entrustedAmount, int depositAmount) {
        this.onlineAmount = onlineAmount;
        this.entrustedAmount = entrustedAmount;
        this.depositAmount = depositAmount;
    }

    public AccountsData(JSONObject dictData) {
        this.onlineAmount = dictData.optInt("onl", 0);
        this.entrustedAmount = dictData.optInt("ent", 0);
        this.depositAmount = dictData.optInt("dep", 0);
    }

    public int getOnlineAmount() {
        return onlineAmount;
    }

    public int getEntrustedAmount() {
        return entrustedAmount;
    }

    public int getDepositAmount() {
        return depositAmount;
    }
}
