package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

/**
 * Created by Medivh on 2018-01-18.
 */

public class UserShopInfo implements UserShopInfoProtocol {
    private String myPhoneNumber = "", subPhoneNumber = "", mainPhoneNumber = "";

    public UserShopInfo(String mainPhoneNumber, String subPhoneNumber, String myPhoneNumber) {
        this.mainPhoneNumber = mainPhoneNumber;
        this.subPhoneNumber = subPhoneNumber;
        this.myPhoneNumber = myPhoneNumber;
    }


    @Override
    public String getMainShopOrMyShopOrUserPhoneNumber() {
        if (Empty.isEmpty(mainPhoneNumber) == false)
            return mainPhoneNumber;
        else
            return getMyShopOrUserPhoneNumber();
    }

    @Override
    public String getMyShopOrUserPhoneNumber() {
        if (Empty.isEmpty(subPhoneNumber) == false)
            return subPhoneNumber;
        else
            return getUserPhoneNumber();
    }

    @Override
    public String getUserPhoneNumber() {
        return myPhoneNumber;
    }

    public String getShopPhoneNumber() {
        return subPhoneNumber;
    }

    public String getMainPhoneNumber() {
        return mainPhoneNumber;
    }
}
