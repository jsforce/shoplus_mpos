package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2016-12-05.
 */

public enum TaskState {
    beforeTasking, onTasking, afterTasking, onError;

    public boolean isFinished() {
        return (this == afterTasking);
    }

    public boolean isOnTasking() {
        return (this == onTasking);
    }

    public boolean isOnError() {
        return (this == onError);
    }

    public boolean isStarted() {
        return (this != beforeTasking);
    }

}
