package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.QueryOrderRecord;
import com.teampls.shoplus.lib.database.SlipDB;
import com.teampls.shoplus.lib.database.SlipItemDB;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.DbType;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.database_memory.MemorySlipDB;
import com.teampls.shoplus.lib.database_memory.MemorySlipItemDB;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.teampls.shoplus.lib.common.MyDevice.DeviceSize.W411;

/**
 * Created by Medivh on 2017-04-20.
 */

abstract public class BaseSlipItemDBAdapter extends BaseDBAdapter<SlipItemRecord> {
    protected SlipItemDB slipItemDB;
    protected MemorySlipItemDB mSlipItemDB;
    protected boolean doFilterSlipDBid = false, doFilterSlipKey = false,
            doFilterSlipItemType = false, doBlockCancelled = false, doShowPlusMinusSum = false,
            doFilterPhoneNumbers = false, doFilterItemIds = false, doFilterSelectedRecords = false;
    protected List<Integer> slipDBids = new ArrayList<>(), itemIds = new ArrayList<>();
    protected List<SlipItemRecord> selectedRecords = new ArrayList<>();
    protected Set<SlipItemType> slipItemTypes = new HashSet<>();
    protected String slipKey = "";
    protected List<String> noQuantityKeywords = new ArrayList<>();
    protected int targetItemId = -1;
    protected boolean unitPriceHighlighted = false; // 거래처별 단가 사용시 할인단가 인지용
    protected MyMap<Integer, ItemRecord> itemRecordMap = new MyMap<>(new ItemRecord()); // for문에서 반복적으로 fileDB 읽기회피

    protected List<String> phoneNumbers = new ArrayList<>();
    public SlipDB slipDB;
    public MemorySlipDB mSlipDB;
    public static final double textViewRatio = 0.6;
    protected DbType dbType = DbType.SQLite;

    public BaseSlipItemDBAdapter(Context context, SlipDB slipDB) {
        super(context);
        this.slipDB = slipDB;
        slipItemDB = slipDB.items;
        dbType = DbType.SQLite;
        noQuantityKeywords = globalDB.getNoQuantityKeywords();
    }

    public BaseSlipItemDBAdapter(Context context, MemorySlipDB mSlipDB) {
        super(context);
        this.mSlipDB = mSlipDB;
        this.mSlipItemDB = mSlipDB.items;
        dbType = DbType.Memory;
        noQuantityKeywords = globalDB.getNoQuantityKeywords();
        setSortingKey(MSortingKey.Date);
    }

    public BaseSlipItemDBAdapter(Context context) {
        super(context);
        dbType = DbType.Other; // ChatbotOrderData
        noQuantityKeywords = globalDB.getNoQuantityKeywords();
    }

    public void highlightUnitPriceByBuyer(boolean value) {
        unitPriceHighlighted = value; // 거래처별 단가사용시 단가가 내려가 있으면 표시해주게
    }

    private boolean doHightlightOptionRecord = false;
    private ItemOptionRecord hightlightOptionRecord = new ItemOptionRecord();

    public void highlightOptionRecord(ItemOptionRecord itemOptionRecord) {
        doHightlightOptionRecord = true;
        this.hightlightOptionRecord = itemOptionRecord;
    }

    public void setTargetItemId(int itemId) {
        this.targetItemId = itemId;
    }

    private void resetFilters() {
        doFilterSlipKey = false;
        doFilterDateTime = false;
        doFilterSlipDBid = false;
        doFilterSlipItemType = false;
        doBlockCancelled = false;
        doFilterPhoneNumbers = false;
    }

    public void setPlusMinusSum(boolean value) {
        doShowPlusMinusSum = value;
    }

    public BaseSlipItemDBAdapter setFilterPhoneNumbers(boolean value) {
        doFilterPhoneNumbers = value;
        return this;
    }

    public BaseSlipItemDBAdapter blockCancelled(boolean value) {
        doBlockCancelled = value;
        return this;
    }

    public BaseSlipItemDBAdapter filterSlipItemRecords(List<SlipItemRecord> selectedRecords) {
        doFilterSelectedRecords = true;
        this.selectedRecords = selectedRecords;
        return this;
    }

    public BaseSlipItemDBAdapter resetSlipItemTypes() {
        doFilterSlipItemType = false;
        return this;
    }

    public BaseSlipItemDBAdapter setSlipItemTypes(List<SlipItemType> types) {
        doFilterSlipItemType = true;
        slipItemTypes = new HashSet(types);
        return this;
    }

    public BaseSlipItemDBAdapter setSlipItemType(SlipItemType itemType, boolean checked) {
        doFilterSlipItemType = true;
        if (checked) {
            slipItemTypes.add(itemType);
        } else {
            slipItemTypes.remove(itemType);
        }
        return this;
    }

    public BaseSlipItemDBAdapter setSlipItemTypes(boolean checked, SlipItemType... slipItemTypes) {
        for (SlipItemType itemType : slipItemTypes)
            setSlipItemType(itemType, checked);
        return this;
    }

    public BaseSlipItemDBAdapter setSlipItemTypes(boolean checked, List<SlipItemType> slipItemTypes) {
        for (SlipItemType itemType : slipItemTypes)
            setSlipItemType(itemType, checked);
        return this;
    }

    public boolean isDoFilterPhoneNumbers() {
        return doFilterPhoneNumbers;
    }

    public BaseSlipItemDBAdapter filterPhoneNumbers(String... counterpartPhoneNumbers) {
        doFilterPhoneNumbers = true;
        phoneNumbers.clear();
        for (String phoneNumber : counterpartPhoneNumbers)
            phoneNumbers.add(phoneNumber);
        return this;
    }

    public BaseSlipItemDBAdapter filterItemIds(int... selectedItemIds) {
        doFilterItemIds = true;
        this.itemIds.clear();
        for (int itemId : selectedItemIds)
            itemIds.add(itemId);
        return this;
    }

    public String getCounterpartPhoneNumber() {
        if (phoneNumbers.size() == 1)
            return phoneNumbers.get(0);
        else if (phoneNumbers.size() == 0)
            return "";
        else {
            Log.w("DEBUG_JS", String.format("[BaseSlipItemDBAdapter.getCounterpartPhoneNumber] %s", TextUtils.join(",", phoneNumbers)));
            return phoneNumbers.get(0);
        }
    }

    public BaseSlipItemDBAdapter filterSlipKey(String slipKey) {
        doFilterSlipKey = true;
        this.slipKey = slipKey;
        return this;
    }

    public SlipSummaryRecord getSummary() {
        return new SlipSummaryRecord(records);
    }

    public void generate(List<SlipItemRecord> sources) {
        generatedRecords.clear();
        for (SlipItemRecord source : sources) {
            if (doFilterSlipItemType)
                if (slipItemTypes.contains(source.slipItemType) == false)
                    continue;
            generatedRecords.add(source);
        }
        applyGeneratedRecords();
    }

    protected MyMap<String, UserRecord> allCounterparts = new MyMap<>(new UserRecord());
    protected MyMap<String, SlipRecord> allSlipRecords = new MyMap<>(new SlipRecord());

    @Override
    public void generate() {
        if (doSkipGenerate) return;
        allCounterparts = UserDB.getInstance(context).getMap();
        List<SlipItemRecord> slipItemRecords = new ArrayList<>();

        switch (dbType) {
            default:
                orderRecords = new QueryOrderRecord(SlipItemDB.Column.salesDateTime, false, true).toList();
                if (doFilterDateTime) {
                    slipItemRecords = slipItemDB.getRecordsBetween(SlipItemDB.Column.salesDateTime, fromDateTime.toString(), toDateTime.toString(), orderRecords);
                } else {
                    slipItemRecords = slipItemDB.getRecordsOrderBy(orderRecords);
                }
                allSlipRecords = slipDB.getMap();
                break;
            case Memory:
                if (doFilterSelectedRecords) {
                    slipItemRecords = selectedRecords;
                } else {
                    for (SlipItemRecord record : mSlipItemDB.getRecords()) { // 정렬 문제
                        if (doFilterDateTime)
                            if (BaseUtils.isBetween(record.salesDateTime, fromDateTime, toDateTime) == false)
                                continue;
                        slipItemRecords.add(record);
                    }
                }
                allSlipRecords = mSlipDB.getMap();
                break;
        }

        boolean isProvider = userSettingData.isProvider();
        MyMap<String, UserRecord> foundCounterparts = new MyMap<>(new UserRecord());
        MyMap<String, SlipRecord> foundSlipRecords = new MyMap<>(new SlipRecord());

        generatedRecords.clear();
        for (SlipItemRecord record : slipItemRecords) {

            if (doFilterSlipDBid)
                if (slipDBids.contains(record.slipKey) == false)
                    continue;

            if (doFilterItemIds)
                if (itemIds.contains(record.itemId) == false)
                    continue;

            if (doFilterSlipItemType)
                if (slipItemTypes.contains(record.slipItemType) == false)
                    continue;

            if (doFilterSlipKey)
                if (slipKey.equals(record.slipKey) == false)
                    continue;

            // 영수증 정보
            SlipRecord slipRecord = foundSlipRecords.get(record.slipKey);
            if (slipRecord.getSlipKey().ownerPhoneNumber.isEmpty()) {
                slipRecord = allSlipRecords.get(record.slipKey);
                foundSlipRecords.put(slipRecord.getSlipKey().toSID(), slipRecord);
            }

            // 거래처 정보
            UserRecord counterpartRecord = foundCounterparts.get(slipRecord.getCounterpart(isProvider));
            if (counterpartRecord.isEmpty()) {
                counterpartRecord = allCounterparts.get(slipRecord.getCounterpart(isProvider));
                foundCounterparts.put(counterpartRecord.phoneNumber, counterpartRecord);
            }

            if (doFilterPhoneNumbers || doBlockCancelled) {

                if (doFilterPhoneNumbers) {
                    if (phoneNumbers.contains(counterpartRecord.phoneNumber) == false)
                        continue;
                }

                if (doBlockCancelled) {
                    if (slipRecord.cancelled)
                        continue;
                }
            }

            record._counterpartName = counterpartRecord.getName();
            record._sortingKey = sortingKey;
            //Log.i("DEBUG_JS", String.format("[PosSlipItemDBAdapter.generate] <%d, %d> name %s, counter %s (%d)", record.slipDBid, record.consecutiveNumber, record.slipItemName, counterpartRecord.displayName, counterpartRecord.id));
            generatedRecords.add(record);
        }

        if (doSortByKey)
            Collections.sort(generatedRecords);

        if (doSetRecordWhenGenerated)
            applyGeneratedRecords();
    }

    private SlipRecord getSlipRecord(String slipKey) {
        switch (dbType) {
            case SQLite:
                return slipDB.getRecordByKey(slipKey);
            case Memory:
                return mSlipDB.getRecordByKey(slipKey);
            default:
                return new SlipRecord();
        }
    }

    public class SlipItemsViewHolder extends BaseViewHolder {
        protected TextView tvDate, tvCounterpart, tvName, tvSlipItemType, tvPrice;
        protected CheckBox cbChecked;
        protected LinearLayout container;
        public SlipItemRecord record;

        @Override
        public int getThisView() {
            return R.layout.row_slip_item;
        }

        @Override
        public void init(View view) {
            tvDate = (TextView) view.findViewById(R.id.row_slip_item_date);
            tvCounterpart = (TextView) view.findViewById(R.id.row_slip_item_counterpart);
            tvName = (TextView) view.findViewById(R.id.row_slip_item_name);
            tvSlipItemType = (TextView) view.findViewById(R.id.row_slip_item_slipItemType);
            tvPrice = (TextView) view.findViewById(R.id.row_slip_item_price);
            cbChecked = (CheckBox) view.findViewById(R.id.row_slip_item_checked);
            container = (LinearLayout) view.findViewById(R.id.row_slip_item_container);
            cbChecked.setVisibility(View.GONE);
        }

        public void update(int position) {
            if (position >= getCount() || position < 0) return; // 경우에 따라서 발생할 수 있음, 주의
            record = records.get(position);

            if (clickedPositions.contains(position)) {
                container.setBackgroundColor(ColorType.Khaki.colorInt);
            } else {
                container.setBackgroundColor(ColorType.Trans.colorInt);
            }

            SlipRecord slipRecord = getSlipRecord(record.slipKey);
            if (BasePreference.isShowWeekday(context)) {
                tvDate.setText(record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format) + String.format("\n(%s)", BaseUtils.getDayOfWeekKor(record.salesDateTime)));
            } else {
                tvDate.setText(record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format));
            }

            if (record._counterpartName.isEmpty()) {
                String counterpartPhoneNumber = slipRecord.getCounterpart(context);
                UserRecord counterpart = userDB.getRecord(counterpartPhoneNumber);
                record._counterpartName = counterpart.getName();
            }
            BaseUtils.setText(tvCounterpart, "", record._counterpartName, slipRecord.cancelled);

            String extName = record.toNameAndOptionString(" ");
            BaseUtils.setText(tvName, record.itemId > 0 ? "" : "*", extName, slipRecord.cancelled);
            if (MyDevice.isTablet(context) == false) {
                if (extName.length() <= 14) {
                    tvName.setTextSize(16);
                } else if (extName.length() <= 20) {
                    tvName.setTextSize(14);
                } else if (extName.length() <= 25) {
                    tvName.setTextSize(12);
                }
            }

            BaseUtils.setText(tvSlipItemType, "", record.slipItemType.toMultiUiName(), slipRecord.cancelled);
            if (slipRecord.cancelled)
                tvSlipItemType.setTextColor(ColorType.Black.colorInt);
            else
                tvSlipItemType.setTextColor(record.slipItemType.getColorInt());

            if (record.quantity == 1) {
                tvPrice.setText(String.format("%s%s", record.slipItemType.getSignStr(), BaseUtils.toCurrencyOnlyStr(record.unitPrice)));
            } else {
                tvPrice.setText(String.format("%d x %s%s", record.quantity, record.slipItemType.getSignStr(), BaseUtils.toCurrencyOnlyStr(record.unitPrice)));
            }
            BaseUtils.setTextCancelled(tvPrice, slipRecord.cancelled);
        }

        protected void setBackgroundColorByShiftTimes() {
            // Background color
            Pair<Double, Double> shiftTimes = BasePreference.getShiftTimes(context);
            if (shiftTimes.second >= 0) { // 2개가 지정된 경우
                if (BaseUtils.isBetweenByHm(record.salesDateTime, shiftTimes.first, shiftTimes.second)) {
                    container.setBackgroundColor(ColorType.Ivory.colorInt);
                } else {
                    container.setBackgroundColor(ColorType.Beige.colorInt);
                }
            } else if (shiftTimes.first >= 0) { // 1개가 지정된 경우
                if (record.salesDateTime.getDayOfYear() % 2 != 0) {
                    if (record.salesDateTime.getHourOfDay() < shiftTimes.first) {
                        container.setBackgroundColor(ColorType.Ivory.colorInt);
                    } else {
                        container.setBackgroundColor(ColorType.Beige.colorInt);
                    }
                } else {
                    if (record.salesDateTime.getHourOfDay() < shiftTimes.first) {
                        container.setBackgroundColor(ColorType.Beige.colorInt);
                    } else {
                        container.setBackgroundColor(ColorType.Ivory.colorInt);
                    }
                }
            } else {
                container.setBackgroundColor(ColorType.Ivory.colorInt);
            }
        }
    }

    public class SlipFullViewHolder extends BaseViewHolder {
        private TextView tvType, tvName, tvUnitPrice, tvQuantity, tvSum;

        @Override
        public int getThisView() {
            return R.layout.row_slip_item_fullview;
        }

        @Override
        public void init(View view) {
            tvType = (TextView) view.findViewById(R.id.row_slip_type);
            tvName = (TextView) view.findViewById(R.id.row_slip_name);
            tvUnitPrice = (TextView) view.findViewById(R.id.row_slip_unitPrice);
            tvQuantity = (TextView) view.findViewById(R.id.row_slip_quantity);
            tvSum = (TextView) view.findViewById(R.id.row_slip_sum);
            MyView.setTextViewByDeviceSizeScale(context, 1.0, tvType, tvName, tvUnitPrice, tvQuantity, tvSum);
        }

        @Override
        public void update(int position) {
            if (position >= getCount() || position < 0) return;
            String delimiter = ":";
            SlipItemRecord record = records.get(position);
            tvType.setText(record.slipItemType.toMultiUiName());
            tvType.setTextColor(record.slipItemType.getColorInt());
            String nameString = (record.name.isEmpty() ? "(미입력)" : record.name);
            if (record.itemId <= 0 && record.slipItemType.isItemType())
                nameString = "*" + nameString;
            String colorString = (record.color == ColorType.Default) || (record.getColorName().isEmpty()) ? "" : String.format(" %s %s", delimiter, record.getColorName());
            String sizeString = (record.size == SizeType.Default) ? "" : String.format(" %s %s", delimiter, record.size.uiName);
            tvName.setText(nameString + colorString + sizeString);
            tvName.setTextColor(record.itemId == targetItemId ? ColorType.Blue.colorInt : ColorType.Black.colorInt);
            tvUnitPrice.setText(record.slipItemType.getSignStr() + BaseUtils.toCurrencyStr(record.unitPrice).replace("원", ""));
            tvQuantity.setText(record.getQuantityStr(context));
            tvSum.setText(record.slipItemType.getSign() == 0 ? "" : record.slipItemType.getSignStr() + BaseUtils.toCurrencyStr(record.unitPrice * record.quantity).replace("원", ""));

            // 수량 표시 제거
            if (noQuantityKeywords.contains(record.name)) {
                tvType.setText("");
                tvUnitPrice.setText("");
                tvQuantity.setText("");
            }
            if (SlipItemType.getNoQuantityTypes().contains(record.slipItemType)) {
                tvUnitPrice.setText("");
                tvQuantity.setText("");
            }

        }
    }

    public class SlipSharingViewHolder extends BaseViewHolder {
        private TextView tvType, tvName, tvUnitPrice, tvQuantity, tvSum, tvSerial;

        @Override
        public int getThisView() {
            return R.layout.row_slip_item_creation;
        }

        @Override
        public void init(View view) {
            tvType = (TextView) view.findViewById(R.id.row_slip_type);
            tvName = (TextView) view.findViewById(R.id.row_slip_name);
            tvUnitPrice = (TextView) view.findViewById(R.id.row_slip_unitPrice);
            tvQuantity = (TextView) view.findViewById(R.id.row_slip_quantity);
            tvSum = (TextView) view.findViewById(R.id.row_slip_sum);
            tvSerial = (TextView) view.findViewById(R.id.row_slip_serial);
            MyView.setTextViewByDeviceSize(context, tvType, tvName, tvUnitPrice, tvQuantity, tvSum);
        }

        @Override
        public void update(int position) {
            if (position >= getCount() || position < 0) return;
            String delimiter = ":";
            SlipItemRecord record = records.get(position);
            if (MyDevice.isLessThan(context, W411)) {
                tvType.setText(record.slipItemType.toMultiUiName());
            } else {
                tvType.setText(record.slipItemType.uiName);
            }
            tvType.setTextColor(record.slipItemType.getColorInt());
            tvSerial.setText(String.format("%d", record.serial + 1));
            String nameString = record.getPosName(context);
            String colorString = (record.color == ColorType.Default) || (record.getColorName().isEmpty()) ? "" : String.format(" %s %s", delimiter, record.getColorName());
            String sizeString = (record.size == SizeType.Default) ? "" : String.format(" %s %s", delimiter, record.size.uiName);
            tvName.setText(nameString + colorString + sizeString);
            tvUnitPrice.setText(BaseUtils.toCurrencyStr(record.unitPrice).replace("원", ""));
            tvQuantity.setText(record.getQuantityStr(context));
            String sumString = record.slipItemType.getSignStr() + BaseUtils.toCurrencyStr(record.unitPrice * record.quantity).replace("원", "");
            tvSum.setText(record.slipItemType.getSign() == 0 ? "" : sumString.replace("--", ""));

            // 수량 표시 제거
            if (noQuantityKeywords.contains(record.name)) {
                tvType.setText("");
                tvUnitPrice.setText("");
                tvQuantity.setText("");
            }
            if (SlipItemType.getNoQuantityTypes().contains(record.slipItemType)) {
                tvUnitPrice.setText("");
                tvQuantity.setText("");
            }

            if (doHightlightOptionRecord) // 특정 옵션을 강조 {
                tvName.setTextColor(record.toOption().equals(hightlightOptionRecord) ? ColorType.Blue.colorInt : ColorType.Black.colorInt);
        }
    }

    public class SlipCreationViewHolder extends BaseViewHolder {
        protected TextView tvType, tvName, tvUnitPrice, tvQuantity, tvSum, tvSerial, tvQR, tvDate;
        private LinearLayout priceContainer, container;

        @Override
        public int getThisView() {
            return R.layout.row_slip_item_creation;
        }

        @Override
        public void init(View view) {
            tvType = (TextView) view.findViewById(R.id.row_slip_type);
            tvName = (TextView) view.findViewById(R.id.row_slip_name);
            tvName.setTypeface(Typeface.DEFAULT_BOLD);
            tvUnitPrice = (TextView) view.findViewById(R.id.row_slip_unitPrice);
            tvQuantity = (TextView) view.findViewById(R.id.row_slip_quantity);
            tvSum = (TextView) view.findViewById(R.id.row_slip_sum);
            tvSerial = (TextView) view.findViewById(R.id.row_slip_serial);
            tvQR = findTextViewById(context, view, R.id.row_slip_QR);
            tvDate = findTextViewById(context, view, R.id.row_slip_createdDate);
            priceContainer = (LinearLayout) view.findViewById(R.id.row_slip_priceContainer);
            container = view.findViewById(R.id.row_slip_container);
            MyView.setTextViewByDeviceSize(context, tvType, tvName, tvUnitPrice, tvQuantity, tvSum, tvSerial);
        }

        @Override
        public void update(int position) {
            if (position >= getCount() || position < 0) return;
            String delimiter = ":";
            SlipItemRecord record = records.get(position);
            tvType.setText(record.slipItemType.toMultiUiName());
            tvType.setTextColor(record.slipItemType.getColorInt());
            container.setBackgroundColor(clickedPositions.contains(position) ? ColorType.Khaki.colorInt : ColorType.Trans.colorInt);
            tvSerial.setText(String.format("%d", record.serial + 1));
            String nameString = (record.name.isEmpty() ? "(미입력)" : record.name);
            String colorString = (record.color == ColorType.Default) || (record.getColorName().isEmpty()) ? "" : String.format(" %s %s", delimiter, record.getColorName());
            String sizeString = (record.size == SizeType.Default) ? "" : String.format(" %s %s", delimiter, record.size.uiName);
            tvName.setText(nameString + colorString + sizeString);
            tvUnitPrice.setText(BaseUtils.toCurrencyStr(record.unitPrice).replace("원", ""));
            tvQuantity.setText(record.getQuantityStr(context));
            String sumString = record.slipItemType.getSignStr() + BaseUtils.toCurrencyStr(record.unitPrice * record.quantity).replace("원", "");
            tvSum.setText(record.slipItemType.getSign() == 0 ? "" : sumString.replace("--", ""));
            tvQR.setVisibility(record.isItemTraceAttached() ? View.VISIBLE : View.GONE);

            if (SlipItemType.getNoQuantityTypes().contains(record.slipItemType)) {

            }

            // 수량 표시 제거
            if (noQuantityKeywords.contains(record.name)) { // 택배비, 부가세
                tvType.setText("");
                tvUnitPrice.setText("");
                tvQuantity.setText("");
            }
            if (SlipItemType.getNoQuantityTypes().contains(record.slipItemType)) {
                tvUnitPrice.setText("");
                tvQuantity.setText("");
            }

            // 거래처별 단가 사용시 모르고 낮은 가격이 적용되는 것을 인지시킴
            if (record.itemId > 0 && unitPriceHighlighted) {
                ItemRecord itemRecord = itemRecordMap.get(record.itemId);
                if (itemRecord.itemId == 0) {
                    itemRecord = ItemDB.getInstance(context).getRecordBy(record.itemId); // mItemDB에서 찾아야할 수도 있음, itemDB에 없는 경우?
                    if (itemRecord.itemId > 0)
                        itemRecordMap.put(itemRecord.itemId, itemRecord);
                }
                tvUnitPrice.setTextColor(record.unitPrice < itemRecord.unitPrice ? ColorType.Blue.colorInt : ColorType.Black.colorInt);
            }
        }
    }

    public class QrPosSlipCreationNewViewHolder extends BaseViewHolder {
        protected TextView tvNumber, tvSlipItemType, tvName, tvUnitPrice, tvShopSerial, tvCount;
        protected LinearLayout container;

        @Override
        public int getThisView() {
            return R.layout.row_qr_slip;
        }

        @Override
        public void init(View view) {
            tvNumber = findTextViewById(context, view, R.id.row_qr_slip_number);
            tvSlipItemType = findTextViewById(context, view, R.id.row_qr_slip_type);
            tvName = findTextViewById(context, view, R.id.row_qr_slip_name);
            tvShopSerial = findTextViewById(context, view, R.id.row_qr_slip_shopSerial);
            tvUnitPrice = findTextViewById(context, view, R.id.row_qr_slip_unitPrice);
            tvCount = findTextViewById(context, view, R.id.row_qr_slip_count);
            container = (LinearLayout) view.findViewById(R.id.row_qr_slip_container);
        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0) return;
            SlipItemRecord slipItemRecord = records.get(position);
            tvName.setText(String.format("%s/%s/%s", slipItemRecord.name, slipItemRecord.color.getUiName(), slipItemRecord.size.uiName));
            tvSlipItemType.setText(slipItemRecord.slipItemType.toMultiUiName());
            tvSlipItemType.setTextColor(slipItemRecord.slipItemType.getColorInt());
            tvNumber.setText(String.format("%2d", position + 1));
            tvUnitPrice.setText(String.format("%s", BaseUtils.toCurrencyOnlyStr(slipItemRecord.unitPrice)));
            tvCount.setText(slipItemRecord.getQuantityStr(context));
            ItemDB.getInstance(context).searchRecord(context, slipItemRecord.itemId, new MyOnTask<ItemRecord>() {
                @Override
                public void onTaskDone(ItemRecord itemRecord) {
                    tvShopSerial.setText(itemRecord.getSerialNumber());
                }
            });
        }
    }

    public class QrPosSlipCreationPendingsViewHolder extends QrPosSlipCreationNewViewHolder {

        @Override
        public void update(int position) {
            super.update(position);
            SlipItemRecord slipItemRecord = records.get(position);
            tvCount.setText(String.format("%s (%d)", slipItemRecord.getQuantityStr(context), slipItemRecord.traceIds.size()));
            container.setBackgroundColor(slipItemRecord.quantity == slipItemRecord.traceIds.size() ?
                    ColorType.Trans.colorInt : ColorType.LightGrey.colorInt);
        }
    }
}
