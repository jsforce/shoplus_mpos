package com.teampls.shoplus.lib.common;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Medivh on 2016-11-22.
 */

abstract public class MyQueueTask<T> {
    private boolean onSending = false;
    private Queue queue;
    private String methodName = "";

    public MyQueueTask(List<T> records) {
        this(records, true, "");
    }

    public MyQueueTask(List<T> records, String methodName) {
        this(records, true, methodName);
    }

    public MyQueueTask(List<T> records, boolean doSend, String methodName) {
        this.queue = new LinkedList(records);
        if (records == null) {
            Log.e("DEBUG_JS", String.format("[MyQueueTask] records = null"));
            return;
        }

        if (records.isEmpty()) {
            Log.e("DEBUG_JS", String.format("[MyQueueTask] records = Empty"));
            return;
        }

        this.methodName = methodName;

        if (doSend)
            start();
    }

    public void start() {
        doNextTask();
    }

    public void doNextTask() {
        try {
            if (queue.peek() == null) {
                onSending = false;
                onFinish();
                return;
            }
            onSending = true;
            onPolling((T) queue.poll());
        } catch (Exception e) {
            onError(e);
        }
    }

    public void onFinish() {
    }

    public abstract void onPolling(T record);

    public void onError(Exception e) {
        Log.e("DEBUG_JS", String.format("[MyQueueTask.onError] %s, %s", methodName, e.getLocalizedMessage()));
    }

}