package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.view.MyView;

import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 * Created by Medivh on 2019-07-05.
 */

abstract public class SetPeriodDialog extends BaseDialog {
    private TimePicker startPicker, endPicker;
    private TextView tvStartTitle, tvEndTitle;
    private DateTime startDateTime = DateTime.now(), endDateTime = DateTime.now();
    private int limitDay = 30, periodDaysLimit = 30;

    public SetPeriodDialog(Context context, DateTime startDateTime, DateTime endDateTime, int limitDay, int periodDaysLimit) {
        super(context);
        setDialogWidth(0.95, 0.8);
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.limitDay = limitDay;
        this.periodDaysLimit = periodDaysLimit;

        tvStartTitle = findTextViewById(R.id.dialog_set_period_start_title);
        tvEndTitle = findTextViewById(R.id.dialog_set_period_end_title);
        startPicker = findViewById(R.id.dialog_set_period_start_picker);
        endPicker = findViewById(R.id.dialog_set_period_end_picker);

        startPicker.setCurrentHour(startDateTime.getHourOfDay());
        startPicker.setCurrentMinute(startDateTime.getMinuteOfHour());
        endPicker.setCurrentHour(endDateTime.getHourOfDay());
        endPicker.setCurrentMinute(endDateTime.getMinuteOfHour());

        MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_set_period_title);

        setOnClick(R.id.dialog_set_period_start, R.id.dialog_set_period_end,
                R.id.dialog_set_period_apply, R.id.dialog_set_period_close, R.id.dialog_set_period_title_close);

        refreshTitles();

        show();
    }


    private void refreshTitles() {
        tvStartTitle.setText(String.format("시작 : %s", BaseUtils.toStringWithWeekDay(startDateTime, BaseUtils.YMD_Kor_Format)));
        tvEndTitle.setText(String.format("종료 : %s", BaseUtils.toStringWithWeekDay(endDateTime, BaseUtils.YMD_Kor_Format)));
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_set_period;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.dialog_set_period_start) {
            new PickDateDialog(context, "날짜선택", startDateTime, false, limitDay) {
                @Override
                public void onApplyClick(DateTime finalDateTime) {
                    startDateTime = finalDateTime;
                    refreshTitles();
                }
            };

        } else if (view.getId() == R.id.dialog_set_period_end) {
            new PickDateDialog(context, "날짜선택", endDateTime, false, limitDay) {
                @Override
                public void onApplyClick(DateTime finalDateTime) {
                    endDateTime = finalDateTime;
                    refreshTitles();
                }
            };


        } else if (view.getId() == R.id.dialog_set_period_apply) {
            startDateTime = new DateTime(startDateTime.getYear(), startDateTime.getMonthOfYear(), startDateTime.getDayOfMonth(), startPicker.getCurrentHour(), startPicker.getCurrentMinute());
            endDateTime = new DateTime(endDateTime.getYear(), endDateTime.getMonthOfYear(), endDateTime.getDayOfMonth(), endPicker.getCurrentHour(), endPicker.getCurrentMinute());

            // 정합성 체크
            if (startDateTime.isAfter(endDateTime)) {
                MyUI.toastSHORT(context, String.format("시작일은 종료일보다 빨라야 합니다"));
                return;
            }

            Duration duration = new Duration(startDateTime, endDateTime);
            if (duration.getStandardDays() >= periodDaysLimit) {
                MyUI.toastSHORT(context, String.format("최대 기간은 %d일 까지 입니다", periodDaysLimit));
                return;
            }

            onApplyClick(startDateTime, endDateTime);
            dismiss();

        } else if (view.getId() == R.id.dialog_set_period_close || view.getId() == R.id.dialog_set_period_title_close) {
            dismiss();

        }

    }

    abstract public void onApplyClick(DateTime startDateTime, DateTime endDateTime);
}
