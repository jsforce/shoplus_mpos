package com.teampls.shoplus.lib.awsservice.apigateway;

import android.support.annotation.NonNull;

import com.teampls.shoplus.lib.datatypes.ItemStockKey;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author lucidite
 */
public class APIGatewayDataParser {
    /**
     * itemid-color-size: count 맵 데이터를 parse한다.
     *
     * @param json
     * @return
     * @throws JSONException
     */
    @NonNull
    public static Map<ItemStockKey, Integer> parseItemStock(JSONObject json) throws JSONException {
        if (json == null) { return new HashMap<>(); }

        Map<ItemStockKey, Integer> result = new HashMap<>();
        Iterator<String> iter = json.keys();
        while(iter.hasNext()) {
            String itemStockKeyStr = iter.next();
            ItemStockKey itemOption = new ItemStockKey(itemStockKeyStr);
            int count = json.getInt(itemStockKeyStr);
            result.put(itemOption, count);
        }
        return result;
    }

    @NonNull
    public static JSONObject encodeItemStock(Map<ItemStockKey, Integer> counts) throws JSONException {
        JSONObject result = new JSONObject();
        for (Map.Entry<ItemStockKey, Integer> count: counts.entrySet()) {
            result.put(count.getKey().toAPIParamStr(), count.getValue().intValue());
        }
        return result;
    }
}
