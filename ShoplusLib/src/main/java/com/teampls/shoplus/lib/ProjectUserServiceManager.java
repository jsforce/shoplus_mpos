package com.teampls.shoplus.lib;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.ProjectUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectUserService;

/**
 * @author lucidite
 */
public class ProjectUserServiceManager {
    public static ProjectUserServiceProtocol defaultService(Context context) {
        return new ProjectUserService();
    }
}
