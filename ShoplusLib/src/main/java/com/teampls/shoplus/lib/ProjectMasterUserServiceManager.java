package com.teampls.shoplus.lib;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.ProjectMasterUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectMasterUserService;

/**
 * @author lucidite
 */
public class ProjectMasterUserServiceManager {
    public static ProjectMasterUserServiceProtocol defaultService(Context context) {
        return new ProjectMasterUserService();
    }
}
