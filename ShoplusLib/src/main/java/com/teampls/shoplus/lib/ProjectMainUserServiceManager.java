package com.teampls.shoplus.lib;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.ProjectMainUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectMainUserService;

/**
 * @author lucidite
 */
public class ProjectMainUserServiceManager {
    public static ProjectMainUserServiceProtocol defaultService(Context context) {
        return new ProjectMainUserService();
    }
}
