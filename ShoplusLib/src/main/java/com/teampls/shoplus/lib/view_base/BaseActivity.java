package com.teampls.shoplus.lib.view_base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_map.GlobalBoolean;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.MyActionbar;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Medivh on 2016-10-24.
 */
abstract public class BaseActivity extends FragmentActivity implements View.OnClickListener {
    protected Context context = BaseActivity.this;
    protected MyDB myDB;
    protected KeyValueDB keyValueDB;
    protected MyActionbar myActionBar;
    protected UserDB userDB;
    protected GlobalDB globalDB;
    protected UserSettingData userSettingData;
    private Set<View> resizedTexts = new HashSet<>();

    protected void checkUserSettingData(final MyOnTask onTask) {
        if (userSettingData.isProtocolNull()) {
            Log.w("DEBUG_JS", String.format("[%s] userSettingData == null", getClass().getSimpleName()));
            userSettingData.refresh(context,"checkUserSettingData", new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    if (onTask != null)
                        onTask.onTaskDone("");
                }
            });
        } else {
            if (onTask != null)
                onTask.onTaskDone("");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("DEBUG_JS", String.format("==%s==", getClass().getSimpleName()));
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        myActionBar = new MyActionbar();
        myDB = MyDB.getInstance(context);
        globalDB = GlobalDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
        keyValueDB = KeyValueDB.getInstance(context);
        userDB = UserDB.getInstance(context);
        setContentView(getThisView());
        myActionBar.initialize(context, "", this);
        setBackground(MyApp.getThemeColors(context)); // actionbar 생성 후 색 입히기 가능
        if (MyApp.get(context) == MyApp.Elicium)
            myActionBar.hideLauncher();

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE) && BasePreference.landScapeOrientationEnabled.get(context)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            myActionBar.getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(MyApp.getThemeColors(context).first)));
        }
    }

    public void setBackground(Pair<String, String> hexs) {
        if (myActionBar == null) return;
        if (hexs.first.isEmpty() == false)
            myActionBar.setBackground(hexs.first);
    }

    public TextView findTextViewById(int viewId) {
        TextView result = (TextView) findViewById(viewId);
        resizeTextView(result);
        return result;
    }

    public EditText findEditTextById(int viewId, String text) {
        EditText result = findViewById(viewId);
        result.setText(text);
        result.setSelection(text.length());
        resizeTextView(result);
        return result;
    }

    private void resizeTextView(TextView textView) {
        if (resizedTexts.contains(textView) == false) {
            MyView.setTextViewByDeviceSize(context, textView);
            resizedTexts.add(textView);
        }
    }

    public TextView findTextViewById(int viewId, String text) {
        TextView result = (TextView) findViewById(viewId);
        result.setText(text);
        resizeTextView(result);
        return result;
    }

    public Button findButtonById(int viewId) {
        Button result = (Button) findViewById(viewId);
        setOnClick(viewId);
        resizeTextView(result);
        return result;
    }

    public CheckBox findCheckBoxById(int viewId) {
        CheckBox result = (CheckBox) findViewById(viewId);
        setOnClick(viewId);
        resizeTextView(result);
        return result;
    }

    public abstract int getThisView();

    @Override
    public void onClick(View view) {
        if (view.getTag() instanceof MyMenu)
            onMyMenuSelected(((MyMenu) view.getTag()));
    }

    public void setHeaderBackground(int containerResId) {
        findViewById(containerResId).setBackgroundColor(ColorType.toInt(MyApp.getThemeColors(context).first));
    }

    public void doFinishForResult() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    abstract public void onMyMenuCreate();

    abstract protected void onMyMenuSelected(MyMenu myMenu);

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (BaseAppWatcher.checkInstance(this) == false) return true;
        onMyMenuCreate();
        return true;
    }

    public View setOnClick(int viewId) {
        View view = findViewById(viewId);
        view.setOnClickListener(this);
        if (view instanceof TextView && resizedTexts.contains(view) == false) {
            MyView.setTextViewByDeviceSize(context, (TextView) view);
            resizedTexts.add(view);
        }
        return view;
    }

    public void setOnClick(int... resIds) {
        for (int resId : resIds) {
            setOnClick(resId);
        }
    }

    public ViewGroup getView() {
        return (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
    }

    public void setVisibility(boolean visible, int... resIds) {
        setVisibility(visible ? View.VISIBLE : View.GONE, resIds);
    }

    public void setVisibility(boolean visible, View... views) {
        setVisibility(visible ? View.VISIBLE : View.GONE, views);
    }

    public void setVisibility(int visibility, int... resIds) {
        for (int resId : resIds)
            findViewById(resId).setVisibility(visibility);
    }

    public void setVisibility(int visibility, View... views) {
        for (View view : views)
            view.setVisibility(visibility);
    }

    public <T extends View> T setView(int viewId) {
        return (T) findViewById(viewId);
    }

    protected void doFinish() {
    }


}

/*

 */