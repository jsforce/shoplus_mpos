package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.teampls.shoplus.lib.enums.DBResultType;
import com.teampls.shoplus.lib.enums.TransType;

/**
 * Created by Medivh on 2017-10-18.
 */

public class TransReceiptDB extends BaseDB<TransReceiptRecord> {
    private static TransReceiptDB instance = null;
    private static int Ver = 1;

    public enum Column {
        _id, slipKey, receipt, transType;
        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    public static TransReceiptDB getInstance(Context context) {
        if (instance == null)
            instance = new TransReceiptDB(context);
        return instance;
    }

    private TransReceiptDB(Context context) {
        super(context, "TransReceiptDB", Ver, Column.toStrs());
    }

    @Override
    protected int getId(TransReceiptRecord record) {
        return record.id;
    }

    @Override
    protected TransReceiptRecord getEmptyRecord() {
        return new TransReceiptRecord();
    }

    @Override
    protected TransReceiptRecord createRecord(Cursor cursor) {
        TransType transType;
        try {
            transType = TransType.valueOf(cursor.getString(Column.transType.ordinal()));
        }  catch (IllegalArgumentException e) {
            transType = TransType.receipt;
        }
        return new TransReceiptRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.slipKey.ordinal()),
                cursor.getInt(Column.receipt.ordinal()),
                transType
        );
    }

    public void toLogCat(String callLocation) {
        for (TransReceiptRecord record : getRecords())
            record.toLogCat(callLocation);
    }

    @Override
    protected ContentValues toContentvalues(TransReceiptRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.slipKey.toString(), record.slipKey);
        contentvalues.put(Column.receipt.toString(), record.receipt);
        contentvalues.put(Column.transType.toString(), record.transType.toString());
        return contentvalues;
    }

    public TransReceiptRecord getRecordByKey(String slipKey) {
        // 반드시 있다고 보장하지 않는다
        if (hasValue(Column.slipKey, slipKey)) {
            return getRecord(Column.slipKey, slipKey);
        } else {
            return new TransReceiptRecord(0, slipKey, 0, TransType.receipt);
        }
    }

    public DBResult updateOrInsert(TransReceiptRecord record) {
        if (hasValue(Column.slipKey, record.slipKey)) {
            TransReceiptRecord dbRecord = getRecordByKey(record.slipKey);
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public void deleteBy(String slipKey) {
        for (int id : getIds(Column.slipKey, slipKey))
            delete(id);
    }
}
