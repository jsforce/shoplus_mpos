package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.VerificationHandler;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.awsservice.ServiceErrorHelper;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.database.MyRecord;

/**
 * Created by Medivh on 2016-10-14.
 */
abstract public class BaseConfirm extends BaseActivity implements View.OnClickListener {
    protected Context context = BaseConfirm.this;
    protected MyDB myDB;
    protected EditText etConfirmationCode;
    private boolean isRequestNewCodeEnabled = false;
    private boolean finished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        myActionBar.hide();
        myDB = MyDB.getInstance(context);
        etConfirmationCode = (EditText) findViewById(R.id.confirmation_code);
        disableRequestNewCodeButton(30);
    }


    public int getThisView() {
        return R.layout.base_user_confirm;
    };


    private void disableRequestNewCodeButton(int disablePeriod_sec) {
        isRequestNewCodeEnabled = false;
        new MyDelayTask(context, disablePeriod_sec * 1000) {
            @Override
            public void onFinish() {
                if (finished == false) {
                    isRequestNewCodeEnabled = true;
                    MyUI.toastSHORT(context, String.format("새로운 코드를 받을 수 있습니다"));
                }
            }
        };
    }

    @Override
    protected void onStop() {
        super.onStop();
        finished = true;
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.check_confirmation_code) {
            if (Empty.isEmpty(context, etConfirmationCode, "인증코드가 입력되지 않았습니다")) return;
            onConfirmClick(etConfirmationCode.getText().toString());

        } else if (view.getId() == R.id.request_newConfirmationCode) {
            if (isRequestNewCodeEnabled == false) {
                MyUI.toastSHORT(context, String.format("인증번호 전송은 30초에 한번만 가능합니다. 전송이 가능해지면 알려드리겠습니다"));
                return;
            }
            disableRequestNewCodeButton(30);
            onRequestNewCodeClick();
        }
    }

    protected void onConfirmClick(String confirmCode) {
        boolean forcedAliasCreation = false;
        UserHelper.getPool().getUser(myDB.getUsername()).confirmSignUpInBackground(confirmCode, forcedAliasCreation, confirmationCallback);
    }

    protected void onRequestNewCodeClick() {
        UserHelper.getPool().getUser(myDB.getUsername()).resendConfirmationCodeInBackground(resendConfCodeHandler);
    }

//    @Override
//    public void onBackPressed() {
//        new MyAlertDialog(context, "입력정보 삭제", "기존 입력 정보를 모두 삭제하고 앱을 종료하시겠습니까?") {
//            @Override
//            public void yes() {
//                myDB.logOut();
//                finish();
//            }
//
//            @Override
//            public void no() {
//                finish();
//            }
//        };
//    }

    abstract public void onConfirm();
    abstract public void onExistingUserError();

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // #Sunstrider Server-related Task
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    GenericHandler confirmationCallback = new GenericHandler() {
        @Override
        public void onSuccess() {
            MyRecord record = myDB.getMyRecord();
            record.confirmed = true;
            myDB.register(record);
            MyUI.toast(context, String.format("정상 유저로 인증 되셨습니다."));
            onConfirm();
        }

        @Override
        public void onFailure(Exception e) {
            String userErrorMessage = ServiceErrorHelper.getCognitoIdErrorMessageForUser(e, true);
            if (AmazonServiceException.class.isInstance(e)) {
                AmazonServiceException awsException = (AmazonServiceException) e;
                switch (awsException.getErrorCode()) {
                    // 일반적으로 발생할 수 있는 것으로 기대되는 예외 목록
                    case "AliasExistsException":
                        new MyAlertDialog(context, "가입 유저 안내", "이미 가입하신 유저입니다.\n\n앱이 재시작되면 [가입 고객입니다] 를 선택해주세요.", "확인", null) {
                            @Override
                            public void yes() {
                                myDB.deleteDB();
                                onExistingUserError();
                            }
                        };
                        return;
                    default:
                        MyUI.toast(context, userErrorMessage);
                        return;
                }
            } else {
                MyUI.toast(context, userErrorMessage);
            }
        }
    };

    VerificationHandler resendConfCodeHandler = new VerificationHandler() {
        @Override
        public void onSuccess(CognitoUserCodeDeliveryDetails verificationCodeDeliveryMedium) {
            MyUI.toast(context, String.format("새로운 코드가 발송되었습니다. 새로 받으신 코드를 입력해 주세요."));
        }

        @Override
        public void onFailure(Exception exception) {
            String userErrorMessage = ServiceErrorHelper.getCognitoIdErrorMessageForUser(exception, true);
            MyUI.toast(context, userErrorMessage);
        }
    };

}