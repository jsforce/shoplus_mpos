package com.teampls.shoplus.lib.dialog;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.MyRecord;
import com.teampls.shoplus.lib.database_global.UserDB;

/**
 * Created by Medivh on 2017-06-26.
 */

public class LoginDialog extends BaseDialog {
    private EditText etPhoneNumber, etPassword;
    private String prevPhoneNumber = "";

    public LoginDialog(Context context) {
        super(context);
        setCancelable(false); // 취소불가 다이얼로그창
        etPhoneNumber = (EditText) findViewById(R.id.dialog_login_phoneNumber);
        etPassword = (EditText) findViewById(R.id.dialog_login_password);
        setOnClick(R.id.dialog_login_close, R.id.dialog_login_apply, R.id.dialog_login_clear_password,
                R.id.dialog_login_clear_phoneNumber, R.id.dialog_login_logout);
        prevPhoneNumber = myDB.getPrivateNumber();
        etPhoneNumber.setText(prevPhoneNumber);
        etPassword.requestFocus();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_login;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_login_logout) {
            new MyAlertDialog(context, "로그 아웃", "모든 앱에서 로그 아웃 하시겠습니까? 저장된 아이디와 비밀번호가 삭제됩니다") {
                @Override
                public void yes() {
                    dismiss();
                    myDB.logOut();
                    ((Activity) context).finishAffinity();
                }
            };

        } else if (view.getId() == R.id.dialog_login_close) {
            dismiss();
            MyUI.toastSHORT(context, String.format("앱을 종료합니다"));
            ((Activity) context).finishAffinity();

        } else if (view.getId() == R.id.dialog_login_apply) {
            if (Empty.isEmpty(context, etPhoneNumber, "전화번호가 입력되지 않았습니다")) return;
            if (Empty.isEmpty(context, etPassword, "비밀번호가 입력되지 않았습니다")) return;

            String phoneNumber = BaseUtils.toSimpleForm(etPhoneNumber.getText().toString());
            String password = BaseUtils.encode(etPassword.getText().toString(), phoneNumber);
            myDB.logOut();
            myDB.register(new MyRecord(phoneNumber, password, true));

            if (prevPhoneNumber.equals(phoneNumber) == false) {
                ItemDB.getInstance(context).deleteDBWithoutImage();
                UserDB.getInstance(context).deleteDB();
            }

            dismiss();
            MyUI.toast(context, String.format("로그인 정보를 입력했습니다. 앱을 다시 시작합니다"));

            new MyDelayTask(context, 1000) {
                @Override
                public void onFinish() {
                    MyDevice.openApp(context, context.getPackageName());
                }
            };
            ((Activity) context).finishAffinity();

        } else if (view.getId() == R.id.dialog_login_clear_password) {
            etPassword.setText("");
        } else if (view.getId() == R.id.dialog_login_clear_phoneNumber) {
            etPhoneNumber.setText("");
        }
    }
}