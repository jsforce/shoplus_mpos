package com.teampls.shoplus.lib.database_memory;

/**
 * Created by Medivh on 2018-04-30.
 */

public enum DbType {
    SQLite, Memory, Other
}
