package com.teampls.shoplus.lib.database;

import com.teampls.shoplus.lib.common.BaseUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-10-09.
 */

public class QueryAndRecord extends BaseRecord<QueryAndRecord> {
    public Enum<?> column;
    public String value;

    public QueryAndRecord(Enum column, String value) {
        this.column = column;
        this.value = value;
    }

    public QueryAndRecord(Enum column, int value) {
        this(column, Integer.toString(value));
    }

    public QueryAndRecord(Enum column, boolean value) {
        this(column, BaseUtils.toDbStr(value));
    }

}
