package com.teampls.shoplus.lib.services.chatbot.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 자동 처리되지 않은 고객 발화 데이터
 *
 * @author lucidite
 */
public class NonProcessedUtteranceData {
    private String utteranceId;
    private String buyerPhoneNumber;
    private String utteranceText;
    private DateTime processedDateTime;
    private String processingText;

    public NonProcessedUtteranceData(JSONObject dataObj) throws JSONException {
        this.utteranceId = dataObj.getString("created");
        this.buyerPhoneNumber = dataObj.getString("buyer_id");
        this.utteranceText = dataObj.getString("utterance");

        String processDateTimeStr = dataObj.optString("processed", null);
        if (processDateTimeStr != null) {
            this.processedDateTime = APIGatewayHelper.getDateTimeFromRemoteString(processDateTimeStr);
        } else {
            this.processedDateTime = null;
        }
        this.processingText = dataObj.optString("note", "");
    }

    /**
     * 발화 데이터의 ID를 조회한다.
     * [NOTE] 이 미처리 발화를 완료 처리할 때 사용한다.
     *
     * @return
     */
    public String getUtteranceId() {
        return utteranceId;
    }

    /**
     * 고객이 발화한 일시를 조회한다.
     *
     * @return
     */
    public DateTime getCreatedDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.utteranceId);
    }

    /**
     * 고객의 전화번호(ID)를 조회한다.
     *
     * @return
     */
    public String getBuyerPhoneNumber() {
        return buyerPhoneNumber;
    }

    /**
     * 고객의 발화 텍스트를 조회한다.
     *
     * @return
     */
    public String getUtteranceText() {
        return utteranceText;
    }

    /**
     * 사용자가 이 발화를 완료 처리했는지 조회한다.
     * [NOTE] 미완이슈 관리 용도
     *
     * @return
     */
    public boolean isProcessed() {
        return processedDateTime != null;
    }

    /**
     * (처리된 경우) 매장에서 처리한 내용을 조회한다.
     * [NOTE] 고객에게 전송하기 위한 용도로 쓸 수 있다 (공유).
     * [NOTE] 처리되었을 경우라 해도 빈 문자열일 수 있다 (별도 메모/회신 없이 바로 완료처리만 한 경우).
     *
     * @return
     */
    public String getProcessingText() {
        return processingText;
    }
}
