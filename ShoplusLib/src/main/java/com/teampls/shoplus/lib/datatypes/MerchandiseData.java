package com.teampls.shoplus.lib.datatypes;

import com.amazonaws.AmazonClientException;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.awsservice.s3.ImageDownloader;
import com.teampls.shoplus.lib.protocol.ImageDataProtocol;

import java.io.IOException;

/**
 * 상품 정보 데이터 클래스
 *
 * @author lucidite
 */

public class MerchandiseData {
    private int itemid;
    private String provider;
    private String name;
    private String mainImagePath;

    public MerchandiseData(int itemid, String provider, String name, String mainImagePath) {
        this.itemid = itemid;
        this.provider = provider;
        this.name = name;
        this.mainImagePath = mainImagePath;
    }

    public int getItemid() {
        return itemid;
    }

    public String getProvider() {
        return provider;
    }

    public String getName() {
        return name;
    }

    public String getMainImagePath() {
        return mainImagePath;
    }

    /**
     * 서버로부터 섬네일 이미지를 다운로드한다.
     *
     * @return
     * @throws MyServiceFailureException
     */
    public ImageDataProtocol downloadThumbnail() throws MyServiceFailureException {
        try {
            return new ImageDownloader(UserHelper.getCredentialsProvider().getCredentials(),
                    MyAWSConfigs.getCurrentConfig().getS3ItemThumbnailsBucketName()).download(this.mainImagePath);
        } catch (IOException | AmazonClientException e) {
            e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }
}
