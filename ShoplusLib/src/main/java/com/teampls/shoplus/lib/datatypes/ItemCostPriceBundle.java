package com.teampls.shoplus.lib.datatypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * 여러 아이템의 매입원가 데이터셋
 *
 * @author lucidite
 */

public class ItemCostPriceBundle extends HashMap<Integer, Integer> {
    private int staringItemId;

    public ItemCostPriceBundle() {};

    public ItemCostPriceBundle(int startingItemId, JSONArray arr) throws JSONException {
        this.staringItemId = startingItemId;
        for (int i = 0; i < arr.length(); ++i) {
            JSONObject costPriceObj = arr.getJSONObject(i);
            Integer itemid = Integer.valueOf(costPriceObj.getInt("itemid"));
            Integer costPrice = Integer.valueOf(costPriceObj.getInt("cost_price"));
            this.put(itemid, costPrice);
        }
    }

    /**
     * 현재 데이터셋의 유효 아이템 ID 범위를 반환한다. 이 값 이후의 아이템 ID에 대해서만 매입원가 값을 가지고 있다.
     * @return
     */
    public int getStaringItemId() {
        return staringItemId;
    }

    /**
     * 특정 아이템 ID가 현재 데이터셋의 유효 범위 내에 있는지 확인한다(범위 체크용 함수).
     * 이 함수의 값이 true가 아닌 itemid에 대해서는 조회 메서드들이 매입원가 데이터를 올바르게 반환할 수 없다.
     *
     * @param itemid
     * @return
     */
    public boolean isInDataRange(int itemid) {
        return itemid >= this.staringItemId;
    }

    /**
     * 특정 아이템의 매입원가를 반환한다. 아이템 ID는 현재 데이터셋의 시작 아이템 ID보다 크거나 같아야 한다(조회범위 내).
     * 데이터셋의 범위 조건을 벗어나는 경우 IllegalArgumentException 예외가 발생한다.
     *
     * NOTE. 런타임 예외를 발생시키므로 사전에 범위 체크에 유의해서 사용해야 한다.
     *
     * @param itemid
     * @return 매입원가(원 단위), 매입원가 데이터가 설정되지 않은 경우 0을 반환한다.
     * @throws IllegalArgumentException 조회하려는 아이템의 ID가 현재 데이터셋의 시작 아이템 ID보다 작은 경우
     */
    public int getCostPriceWithRangeCheck(int itemid) throws IllegalArgumentException {
        if (!this.isInDataRange(itemid)) {
            throw new IllegalArgumentException("itemid " + itemid + " is less than starting itemid " + this.staringItemId);
        }
        return this.getCostPriceWithoutRangeCheck(itemid);
    }

    /**
     * 특정 아이템의 매입원가를 반환한다. 아이템 ID는 현재 데이터셋의 시작 아이템 ID보다 크거나 같아야 한다(조회범위 내).
     * 데이터셋의 범위 조건을 벗어나는 경우 실제 서버에서 매입원가가 설정되어 있다고 하더라도 0을 반환한다.
     *
     * NOTE. 데이터셋의 범위 이전의 아이템에 대해 질의하는 경우 잘못된 데이터를 반환할 수 있으므로
     * (매입원가가 설정되었음에도 0을 반환할 수 있음) 사전에 범위 체크에 유의해서 사용해야 한다.
     *
     * @param itemid
     * @return 매입원가(원 단위), 매입원가 데이터가 설정되지 않은 경우 0을 반환한다.
     */
    public int getCostPriceWithoutRangeCheck(int itemid) {
        Integer costPrice = this.get(Integer.valueOf(itemid));
        if (costPrice != null) {
            return costPrice.intValue();
        } else {
            return 0;
        }
    }

    public int getCost(int itemId) {
        if (this.containsKey(itemId)) {
            return get(itemId);
        } else {
            return 0;
        }
    }
}
