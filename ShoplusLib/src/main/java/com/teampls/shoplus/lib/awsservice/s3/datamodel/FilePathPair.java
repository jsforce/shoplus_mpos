package com.teampls.shoplus.lib.awsservice.s3.datamodel;

import android.util.Pair;

/**
 * @author lucidite
 */

public class FilePathPair extends Pair<String, String> implements FilePathPairProtocol {
    public FilePathPair(String original, String thumbnail) {
        super(original, thumbnail);
    }

    @Override
    public String getOriginalFilePath() {
        return this.first;
    }

    @Override
    public String getThumbnailFilePath() {
        return this.second;
    }
}
