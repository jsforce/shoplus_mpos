package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

import com.teampls.shoplus.lib.database.DraftSlipRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.protocol.TransactionDraftDataProtocol;

import java.util.List;

/**
 * Created by Medivh on 2019-03-06.
 */

public class MDraftSlipDB extends BaseMapDB<String, DraftSlipRecord> {
    private static MDraftSlipDB instance;
    public MDraftSlipItemDB items;

    public static MDraftSlipDB getInstance(Context context) {
        if (instance == null)
            instance = new MDraftSlipDB(context);
        return instance;
    }

    private MDraftSlipDB(Context context) {
        super(context, "MDraftSlipDB");
        items = MDraftSlipItemDB.getInstance(context);
    }

    @Override
    protected DraftSlipRecord getEmptyRecord() {
        return new DraftSlipRecord();
    }

    @Override
    public String getKeyValue(DraftSlipRecord record) {
        return Long.toString(record.draftId);
    }

    public DraftSlipRecord getRecordByKey(long draftId) {
        return getRecordByKey(String.valueOf(draftId));
    }

    public void set(List<TransactionDraftDataProtocol> protocols) {
        clear();
        items.clear();
        for (TransactionDraftDataProtocol protocol : protocols) {
            DraftSlipRecord draftSlipRecord = new DraftSlipRecord(protocol);
            insert(draftSlipRecord);
            items.insertAll(SlipItemRecord.toList(draftSlipRecord.getSlipDataKey(context), protocol.getTransactionItems()));
        }
    }

    public void toLogCat(String location) {
        for (DraftSlipRecord record : getRecords())
            record.toLogCat(location);
    }

    public void updateOrInsertBy(TransactionDraftDataProtocol protocol) {
        DraftSlipRecord draftSlipRecord = new DraftSlipRecord(protocol);
        updateOrInsert(draftSlipRecord);
        items.deleteBy(draftSlipRecord.getSlipDataKey(context).toSID());
        items.insertAll(SlipItemRecord.toList(draftSlipRecord.getSlipDataKey(context), protocol.getTransactionItems()));
    }
}
