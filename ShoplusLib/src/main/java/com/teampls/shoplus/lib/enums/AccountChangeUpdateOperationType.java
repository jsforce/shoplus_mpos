package com.teampls.shoplus.lib.enums;

/**
 * 거래기록에 의한 잔금 변경 항목의 Clear/Restore 처리
 *
 * @author lucidite
 */

public enum AccountChangeUpdateOperationType {
    Clearance("clear"),
    Restoration("restore");

    private String remoteStr;

    AccountChangeUpdateOperationType(String remoteStr) {
        this.remoteStr = remoteStr;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }
}
