package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2017-02-14.
 */

public class MyImageDialog extends BaseDialog {
    private MyOnTask onTask;

    public MyImageDialog(final Context context, String name, String message, Bitmap bitmap, MyOnTask onTask) {
        super(context);
        setDialogWidth(0.6, 0.5);
        this.onTask = onTask;
        TextView textView = findTextViewById(R.id.dialog_image_title, name);
        MyView.setTextViewByDeviceSizeScale(context, 1.0, textView);
        int paddingWidth = MyDevice.toPixel(context, 10) * 2; // padding = 10dp
        ImageView imageView = (ImageView) findViewById(R.id.dialog_image_imageView);
        imageView.getLayoutParams().height = MyGraphics.getHeight((int) (MyDevice.getWidth(context) * 0.5) - paddingWidth, bitmap.getWidth(), bitmap.getHeight());
        imageView.setImageBitmap(bitmap);
        if (onTask != null)
            setVisibility(View.VISIBLE, R.id.dialog_image_link);

        if (TextUtils.isEmpty(message) == false) {
            setVisibility(View.VISIBLE, R.id.dialog_image_message);
            findTextViewById(R.id.dialog_image_message, message);
        }

        setOnClick(R.id.dialog_image_link, R.id.dialog_image_close);
        MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_image_title,  R.id.dialog_image_message);
        show();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_image;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_image_close) {
            dismiss();
        } else if (view.getId() == R.id.dialog_image_link) {
            if (onTask != null)
                onTask.onTaskDone("");
            dismiss();
        }
    }

}
