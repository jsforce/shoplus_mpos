package com.teampls.shoplus.lib.view_base;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.database.MyDB;

/**
 * Created by Medivh on 2016-10-14.
 */
abstract public class BaseUserTypeSetting extends BaseActivity {
    protected MyDB myDB = MyDB.getInstance(context);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("DEBUG_JS", String.format("==%s==", getClass().getSimpleName()));
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        myActionBar.hide();
        setOnClick(R.id.user_type_setting_provider, R.id.user_type_setting_employee,
                R.id.user_type_setting_buyer);
    }

    @Override
    public int getThisView() {
        return R.layout.base_user_type_setting;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        if (view.getId() == R.id.user_type_setting_provider) {
            onProviderClick(view);

        } else if (view.getId() ==  R.id.user_type_setting_employee) {
            onEmployeeClick(view);

        } else if (view.getId() == R.id.user_type_setting_buyer) {
            onBuyerClick(view);
        }
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    public abstract void onProviderClick(View view);

    public abstract  void onBuyerClick(View view);

    public abstract void onEmployeeClick(View view);

}