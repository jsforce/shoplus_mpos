package com.teampls.shoplus.lib.awsservice.config;

import com.amazonaws.regions.Regions;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;

/**
 * 개발서버 설정
 *
 * @author lucidite
 */

public class DevAWSConfigs extends MyAWSConfigs {
    private final String USER_POOL_ID = "ap-northeast-2_UTbWH8e9r";
    private final String USER_POOL_CLIENT_ID = "7j1spiinegslf1qnpp3h7vjjbi";
    private final String USER_POOL_CLIENT_SECRET = "abge3j5r0nm5h65cf45mbjp4afolallm9q6qual09r2l0uljoh6";
    private final String IDENTITY_POOL_ID = "ap-northeast-2:5bbe72c7-bcb5-4336-a86b-fe2a0b476d5d";
    private final String APIGATEWAY_INVOKE_URL = "https://a1eip6kv3l.execute-api.ap-northeast-2.amazonaws.com/dev";
    private String S3_SLIPS_BUCKET_NAME = "project-dalaran-slips-dev";
    private String S3_ITEMS_BUCKET_NAME = "project-stormwind-items-dev";
    private String S3_RECEIPTS_BUCKET_NAME = "project-darnassus-receipts-dev";

    @Override
    public String getUserPoolId() {
        return USER_POOL_ID;
    }

    @Override
    public String getUserPoolClientId() {
        return USER_POOL_CLIENT_ID;
    }

    @Override
    public String getUserPoolClientSecret() {
        return USER_POOL_CLIENT_SECRET;
    }

    @Override
    public String getIdentityPoolId() {
        return IDENTITY_POOL_ID;
    }

    @Override
    public String getApigatewayInvokeUrl() {
        return APIGATEWAY_INVOKE_URL;
    }

    @Override
    public String getS3SlipsBucketName() {
        return S3_SLIPS_BUCKET_NAME;
    }

    @Override
    public String getS3ItemsBucketName() {
        return S3_ITEMS_BUCKET_NAME;
    }

    @Override
    public String getS3ReceiptsBucketName() {
        return S3_RECEIPTS_BUCKET_NAME;
    }
}
