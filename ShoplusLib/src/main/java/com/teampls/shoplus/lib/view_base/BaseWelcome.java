package com.teampls.shoplus.lib.view_base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.awsservice.handler.BaseAuthenticationHandler;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyCountDownTimer;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.enums.LoginState;
import com.teampls.shoplus.lib.view.MyProgressBarTime;

/**
 * Created by Medivh on 2017-03-10.
 */

abstract public class BaseWelcome extends Activity {
    protected Context context = BaseWelcome.this;
    private int thisView = R.layout.base_welcome;
    protected MyDB myDB;
    protected int DefaultDelayMills = 1000;
    private boolean doFinishActivity = false;
    protected boolean doClearActivityStacks = false;


    // 권한 OK 전까지는 앱 디렉토리가 생성되지 않았으므로 global 계열의 파일을 불러오면 에러가 발생할 수 있다
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("DEBUG_JS", String.format("== %s : Welcome ==", MyDevice.getAppLabel(context)));
        checkIntent();
        checkFragments();

        setContentView(thisView);
        getActionBar().hide(); // styles에 AppTheme 확인 (각 앱에는 없어야 함)
        ((TextView) findViewById(R.id.welcome_version)).setText("Version :" + MyDevice.getAppVersionName(this));
        ((ImageView) findViewById(R.id.welcome_cover)).setImageDrawable(getResources().getDrawable(getCoverImageResId()));

        myDB = MyDB.getInstance(context);
        myDB.open();
        if (MyDevice.networkConnected(context)) {
            BaseAppWatcher.setNetworkConnected(true);
            if (manipulateDBs()) return;
            init();
            onCreateFinish();
        } else {
            BaseAppWatcher.setNetworkConnected(false);
            onNetworkDisconnected();
            MyUI.toastSHORT(context, String.format("네트워크에 연결되지 않음"));
        }

        // GlobalDB 계열을 읽으려면 file 권한이 필요
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE) && BasePreference.landScapeOrientationEnabled.get(context))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    protected void checkIntent() {
    }

    protected void checkFragments() {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("DEBUG_JS", String.format("[BaseWelcome.onActivityResult] %d, %d, %s", requestCode, resultCode, data.toString()));
        finish();
    }

    protected void onNetworkDisconnected() {
        new MyAlertDialog(context, "네트워크 연결 이상", "네트워크에 연결되어 있지 않습니다", "확   인", null) {
            @Override
            public void yes() {
                finish();
            }
        };
    }

    abstract public int getCoverImageResId(); // 주의 : 원본 이미지의 크기가 720?을 넘어가면 표시되지 않는다, 이유는 모르겠다

    public String getAppDir() {
        return String.format("/%s", MyApp.get(context).toString());
    }

    protected void init() {
        // 권한이 필요한 기능이 있으면 안된다.
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        boolean isProduction = MyAWSConfigs.isProduction();
        UserHelper.init(context);
        if (isProduction == false)
            MyUI.toastSHORT(context, "개발용 서버로 연결합니다");
        BaseAppWatcher.init(context, isProduction, getAppDir());
        if (BaseAppWatcher.isServerChanged)
            onServerChange();
        MyDevice.setFontScale(context, BasePreference.getFontScale(context));
    }

    protected void onServerChange() {
    }

    protected boolean manipulateDBs() {
        return false;
    }

    protected void onCreateFinish() {
        if (DefaultDelayMills > 0)
            new MyProgressBarTime((ProgressBar) findViewById(R.id.welcome_progressBar), DefaultDelayMills).start();
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                selectNextActivity();
            }
        };
        new MyCountDownTimer(100) {
            @Override
            public void onTick(long millsLeft) {
                if (elapsedTimeMs >= DefaultDelayMills) {
                    if (doFinishActivity) {
                        cancel();
                        finish();
                    }
                }
            }
        }.start();
    }

    abstract public void startAppWatcher();

    protected void selectNextActivity() {
        BaseAppWatcher.setValidInstance(true);

        if (myDB.confirmed()) {
            startAppWatcher();
            BaseAppWatcher.setLoginState(context, LoginState.onTrying);
            UserHelper.getPool().getUser(myDB.getUserPoolNumber()).signOut();
            UserHelper.getPool().getUser(myDB.getUserPoolNumber()).getSessionInBackground(new MyAuthenticationHandler(context));

            if (BaseAppWatcher.checkAppPermission(context) == false) {
                MyDevice.requestPermission(context, MyDevice.UserPermission.WRITE_STORAGE);
                return;
            }

            new MyDelayTask(context, DefaultDelayMills) {
                @Override
                public void onFinish() {
                    if (doClearActivityStacks)
                        startActivity(BaseUtils.getIntentWithClearStacks(context, getMainViewPager()));
                    else
                        startActivity(BaseUtils.getIntent(context, getMainViewPager())); // 살아는 있지만 빈 activity가 올 수 있나?
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    doFinishActivity = true;
                }
            };

        } else if (myDB.registered()) {
            new MyDelayTask(context, DefaultDelayMills) {
                @Override
                public void onFinish() {
                    startActivity(BaseUtils.getIntent(context, getUserConfirm()));
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    doFinishActivity = true;
                }
            };

        } else {
            new MyDelayTask(context, DefaultDelayMills) {
                @Override
                public void onFinish() {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        startActivity(BaseUtils.getIntent(context, getUserIsRegistered()));
                    } else {
                        // 마시멜로우 이상 버전
                        if (MyDevice.hasAllPermissions(context)) {
                            startActivity(BaseUtils.getIntent(context, getUserIsRegistered()));
                        } else {
                            startActivity(BaseUtils.getIntent(context, getUserReqPermissions()));
                        }
                    }
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    doFinishActivity = true;
                }
            };
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            MyDevice.restartApp(context, "앱을 재시작합니다");
        } else {
            MyUI.toastSHORT(context, String.format("앱을 종료합니다"));
            finishAffinity();
        }
    }

    abstract public Class<?> getUserConfirm();

    abstract public Class<?> getUserIsRegistered();

    abstract public Class<?> getMainViewPager();

    abstract public Class<?> getUserReqPermissions();

    class MyAuthenticationHandler extends BaseAuthenticationHandler {

        public MyAuthenticationHandler(Context context) {
            super(context);
        }

        @Override
        public void onNotAuthorized() {
        }

        @Override
        public void onTaskDone(Object result) {
        }
    }

}
