package com.teampls.shoplus.lib.database;

import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-06-10.
 */

public class OrderItemRecord implements Comparable<OrderItemRecord> {
    public List<SlipItemRecord> slipItems = new ArrayList<>(); // 실제 주문건들을 저장
    public int quantity = 0, unitPrice = 0, itemId = 0, finalOrder = 0, stock = 0;
    public String name = "";
    public SizeType size = SizeType.Default;
    public ColorType color = ColorType.Default;
    public DateTime salesDateTime = Empty.dateTime;

    public OrderItemRecord() {

    }

    public OrderItemRecord(ItemRecord itemRecord, ItemOptionRecord optionRecord, int orderCount) {
        this.name = itemRecord.name;
        this.size = optionRecord.size;
        this.color = optionRecord.color;
        this.unitPrice = itemRecord.unitPrice;
        this.itemId = itemRecord.itemId;
        this.finalOrder = orderCount;
    }

    public ItemStockKey toStockKey() {
        return new ItemStockKey(itemId, color, size);
    }

    public ItemOptionRecord toOptionRecord() {
        ItemOptionRecord result = new ItemOptionRecord(itemId, color, size);
        result.stock = stock;
        return result;
    }

    public OrderItemRecord(SlipItemRecord slipItem, int stock) {
        slipItems.clear();
        slipItems.add(slipItem);

        itemId = slipItem.itemId;  // itemId, size, color가 같아야 한다 (필수 요건)
        size = slipItem.size;
        color = slipItem.color;

        unitPrice = slipItem.unitPrice; // itemId가 같으므로 이 정보도 같아야 한다
        name = slipItem.name;
        quantity = slipItem.quantity;
        this.stock = stock;
    }

    public boolean isBundleOf(SlipItemRecord slipItemRecord) {
        if (itemId == 0 && slipItemRecord.itemId == 0) {
            // 둘 다 수동 입력인 경우는 itemId 대신 이름으로 구분한다.
            if (name.equals(slipItemRecord.name) == false) return false;
        } else {
            if (itemId != slipItemRecord.itemId) return false;
        }
        if (size != slipItemRecord.size) return false;
        if (color != slipItemRecord.color) return false;
        return true;
    }

    public void addSlipItem(SlipItemRecord slipItem) {
        slipItems.add(slipItem);
        quantity = quantity + slipItem.quantity;
        if (slipItem.salesDateTime.isAfter(salesDateTime))
            salesDateTime = slipItem.salesDateTime;
    }

    public void addAll(List<SlipItemRecord> slipItemRecords) {
        slipItems.addAll(slipItemRecords);
    }

    public OrderItemRecord(List<SlipItemRecord> slipItems, int stock) {
        this(slipItems.get(0), stock);
        int index = 0;
        for (SlipItemRecord slipItem : slipItems) {
            if (slipItem.itemId != itemId)
                Log.e("DEBUG_JS", String.format("[OrderItemRecord.OrderItemRecord] itemId not same (%d, %d)", slipItem.itemId, itemId));
            if (index == 0) {
                index++;
                continue;
            } else {
                addSlipItem(slipItem);
                index++;
            }
        }
    }

    public String toOptionString(String delimiter, boolean doHideDefault) {
        if (slipItems.isEmpty()) return "";
        return slipItems.get(0).toOptionString(delimiter, doHideDefault);
    }

    public String toNameOptionString(String delimiter) {
        if (slipItems.isEmpty()) return "";
        return slipItems.get(0).toNameAndOptionString(delimiter);
    }


    @Override
    public int compareTo(@NonNull OrderItemRecord another) {
        return name.compareTo(another.name);
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] %d, %s, %s (%s), %s, %d x %d, stock %d, order %d",
                location, itemId, name, color.getOriginalUiName(), color.getUiName(), size.uiName, quantity, unitPrice, stock, finalOrder));
        for (SlipItemRecord slipItemRecord : slipItems)
            slipItemRecord.toLogCat(location);
    }
}
