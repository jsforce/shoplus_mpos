package com.teampls.shoplus.lib.datatypes;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 매장 정보 데이터 클래스
 *
 * @author lucidite
 */

public class MyShopInformationData implements JSONDataWrapper {
    private String shopName = "";
    private List<String> shopPhoneNumbers = new ArrayList<>();
    private String buildingAddress = "";
    private String detailedAddress = "";
    private List<String> accountNumbers = new ArrayList<>();
    private String notice = "";

    public MyShopInformationData() {};

    public MyShopInformationData clone() {
        MyShopInformationData result = new MyShopInformationData();
        result.shopName = shopName;
        for (String shopPhoneNumber : shopPhoneNumbers)
            result.shopPhoneNumbers.add(shopPhoneNumber);
        result.buildingAddress = buildingAddress;
        result.detailedAddress = detailedAddress;
        for (String accountNumber : accountNumbers)
            result.accountNumbers.add(accountNumber);
        result.notice = notice;
        return result;
    }
    
    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %s, phones: %s, %s %s, %s, %s", callLocation,
                shopName, shopPhoneNumbers, buildingAddress, detailedAddress, accountNumbers, notice));
    }

    public MyShopInformationData(String shopName, List<String> shopPhoneNumbers, String buildingAddress, String detailedAddress, List<String> accountNumbers, String notice) {
        this.shopName = shopName;
        this.shopPhoneNumbers = shopPhoneNumbers;
        this.buildingAddress = buildingAddress;
        this.detailedAddress = detailedAddress;
        this.accountNumbers = accountNumbers;
        this.notice = notice;
    }

    public MyShopInformationData(JSONObject dataObj) {
        this.shopName = dataObj.optString("name", "");

        this.shopPhoneNumbers = new ArrayList<>();
        JSONArray phoneArr = dataObj.optJSONArray("phones");
        if (phoneArr != null) {
            for (int i = 0; i < phoneArr.length(); ++i) {
                this.shopPhoneNumbers.add(phoneArr.optString(i, ""));
            }
        }

        this.buildingAddress = dataObj.optString("bld_addr", "");
        this.detailedAddress = dataObj.optString("dtl_addr", "");

        this.accountNumbers = new ArrayList<>();
        JSONArray accountArr = dataObj.optJSONArray("accounts");
        if (accountArr != null) {
            for (int i = 0; i < accountArr.length(); ++i) {
                this.accountNumbers.add(accountArr.optString(i, ""));
            }
        }
        this.notice = dataObj.optString("notice", "");
    }

    @Override
    public JSONObject getObject() throws JSONException {
        JSONObject dataObj = new JSONObject();
        if (!this.shopName.isEmpty()) {
            dataObj.put("name", this.shopName);
        }
        if (!this.shopPhoneNumbers.isEmpty()) {
            JSONArray phoneArr = new JSONArray();
            for (String phoneNumber : this.shopPhoneNumbers) {
                if (!phoneNumber.isEmpty()) {
                    phoneArr.put(phoneNumber);
                }
            }
            dataObj.put("phones", phoneArr);
        }
        if (!this.buildingAddress.isEmpty()) {
            dataObj.put("bld_addr", this.buildingAddress);
        }
        if (!this.detailedAddress.isEmpty()) {
            dataObj.put("dtl_addr", this.detailedAddress);
        }
        if (!this.accountNumbers.isEmpty()) {
            JSONArray accountArr = new JSONArray();
            for (String accountNumber : this.accountNumbers) {
                if (!accountNumber.isEmpty()) {
                    accountArr.put(accountNumber);
                }
            }
            dataObj.put("accounts", accountArr);
        }
        if (!this.notice.isEmpty()) {
            dataObj.put("notice", this.notice);
        }
        return dataObj;
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("MyShopInformationData instance does not have a JSON array");
    }

    public String getShopName() {
        return shopName;
    }

    public String getShopPhoneNumber(int index) {
        if (shopPhoneNumbers.size() > index) {
            return shopPhoneNumbers.get(index);
        } else {
            return "";
        }
    }

    public String getAccountNumber(int index) {
        if (accountNumbers.size() > index) {
            return accountNumbers.get(index);
        } else {
            return "";
        }
    }

    public List<String> get3Accounts() {
        List<String> results = new ArrayList<>();
        results.add(getAccountNumber(0));
        results.add(getAccountNumber(1));
        results.add(getAccountNumber(2));
        return results;
    }

    public String getBuildingAddress() {
        return buildingAddress;
    }

    public String getDetailedAddress() {
        return detailedAddress;
    }

    public String getNotice() {
        return notice;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public void setShopPhoneNumbers(List<String> shopPhoneNumbers) {
        this.shopPhoneNumbers = shopPhoneNumbers;
    }

    public void updatePhoneNumber(int index, String phoneNumber) {
        if (shopPhoneNumbers.size() > index) {
            shopPhoneNumbers.set(index, phoneNumber);
        } else {
            for (int i = shopPhoneNumbers.size(); i <= index; i++) {
                if (i == index)
                    shopPhoneNumbers.add(phoneNumber);
                else
                    shopPhoneNumbers.add("");
            }
        }
    }

    public void updateAccountNumber(int index, String accountNumber) {
        if (accountNumbers.size() > index) {
            accountNumbers.set(index, accountNumber);
        } else {
            for (int i = accountNumbers.size(); i <= index; i++) {
                if (i == index)
                    accountNumbers.add(accountNumber);
                else
                    accountNumbers.add("");
            }
        }
    }

    public void setBuildingAddress(String buildingAddress) {
        this.buildingAddress = buildingAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }

    public void setAccountNumbers(List<String> accountNumbers) {
        this.accountNumbers = accountNumbers;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }
}
