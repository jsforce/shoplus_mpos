package com.teampls.shoplus.lib.enums;

import com.teampls.shoplus.lib.common.BaseUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 상품(아이템) 카테고리 타입
 *
 * @author lucidite
 */

public enum ItemCategoryType {
    ALL(null, "전체"),
    NONE(null, ItemCategorySet.ETC, "미지정"),
    // major
    T_SHIRT("t-shirt", ItemCategorySet.Cloth, "티셔츠", "T", "티", "반팔", "긴팔", "MTM", "나시", "폴라"),
    SWEATER("sweater", ItemCategorySet.Cloth, "니트/스웨터", "캐시"),
    BLOUSE("blouse", ItemCategorySet.Cloth, "블라우스", "BL"),
    SHIRT("shirt", ItemCategorySet.Cloth, "남방/셔츠", "NB"),
    CARDIGAN("cardigan", ItemCategorySet.Cloth, "가디건", "CD", "GD"),
    JACKET("jacket", ItemCategorySet.Cloth, "자켓", "JK"),
    VEST("vest", ItemCategorySet.Cloth, "조끼", "베스트"),
    SKIRT("skirt", ItemCategorySet.Cloth, "스커트", "SK", "스컷"),
    PANTS("pants", ItemCategorySet.Cloth, "긴바지", "바지", "SL", "팬츠", "슬랙", "슬랙스", "PT"),
    SHORTS("shorts", ItemCategorySet.Cloth, "반바지"),
    JEAN("jean", ItemCategorySet.Cloth, "진/청바지"),
    DRESS("dress", ItemCategorySet.Cloth, "원피스/점프슈트", "OPS"),

    // 여기서부터는 minor
    TWO_PIECE("2-piece", ItemCategorySet.Cloth, "투피스"),
    THREE_PIECE("3-piece", "쓰리피스"),
    SUIT("suit", "정장"),
    COAT("coat", ItemCategorySet.Cloth, "코트", "바바리", "CT", "TC", "트렌치"),
    FIELD_JACKET("f-jacket", ItemCategorySet.Cloth, "야상"),
    WIND_STOPPER("wind-stop", ItemCategorySet.Cloth, "바람막이/점퍼", "JP"),
    PADDING("padding", ItemCategorySet.Cloth, "패딩"),
    UNDERWEAR("underwear", ItemCategorySet.ETC, "속옷"),
    SWIMSUIT("swimsuit", ItemCategorySet.ETC, "수영복"),
    SCARF("scarf", ItemCategorySet.ETC, "스카프"),
    BAG("bag", ItemCategorySet.ETC, "가방", "클러치"),
    WALLET("wallet", ItemCategorySet.ETC, "지갑"),
    ACCESSORY("acc", ItemCategorySet.ETC, "악세사리", "ACC"),
    MISCELLANEOUS("misc", ItemCategorySet.ETC, "기타"),

    // 신발
    SHOES("shoes", ItemCategorySet.Shoes, "신발일반"),
    RUNNING("running", ItemCategorySet.Shoes, "운동화"),
    SANDAL("sandal", ItemCategorySet.Shoes, "샌들/슬링백"),
    SLIPPER("slipper", ItemCategorySet.Shoes, "슬리퍼/뮬"),
    FLAT("flat", ItemCategorySet.Shoes, "플랫"),
    SLIPON("slipon", ItemCategorySet.Shoes, "슬립온"),
    BOOTS("boots", ItemCategorySet.Shoes, "부츠"),
    PUMPS("pumps", ItemCategorySet.Shoes, "힐/펌프스");

    private String remoteStr;
    private String uiStr;
    private List<String> abbreviations = new ArrayList<>();
    private ItemCategorySet categorySet = ItemCategorySet.None;

    ItemCategoryType(String remoteStr, String uiStr) {
        this(remoteStr, ItemCategorySet.None, uiStr);
    }

    ItemCategoryType(String remoteStr, ItemCategorySet categorySet, String uiStr, String... abbreviation) {
        this.remoteStr = remoteStr;
        this.categorySet = categorySet;
        this.uiStr = uiStr;
        abbreviations = BaseUtils.toList(abbreviation);
        if (uiStr.contains("/")) {
            abbreviations.addAll(0, BaseUtils.toList(uiStr.split("/")));
        } else {
            abbreviations.add(0, uiStr);
        }
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }

    public String toUIStr() {
        return this.uiStr;
    }

    public static ItemCategoryType fromRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty()) {
            return ItemCategoryType.NONE;
        }
        for (ItemCategoryType category : ItemCategoryType.values()) {
            String compareTo = category.toRemoteStr();
            if (compareTo == null) {
                continue;
            }
            if (compareTo.equals(remoteStr)) {
                return category;
            }
        }
        return ItemCategoryType.NONE;
    }

    public static List<ItemCategoryType> getTypes() {
        List<ItemCategoryType> results = new ArrayList<>();
        for (ItemCategoryType itemCategoryType : values()) {
            if (itemCategoryType == ALL)
                continue;
            results.add(itemCategoryType);
        }
        return results;
    }

    public static int getCount(ItemCategorySet categorySet) {
        int result = 0;
        for (ItemCategoryType categoryType : values())
            if (categoryType.categorySet == categorySet)
                result++;
        return result;
    }

    public static ItemCategoryType getType(String name) {
        name = name.toUpperCase().trim();
        if (name.isEmpty())
            return ItemCategoryType.NONE;

        // 특수 케이스
        if (name.endsWith("CT"))  // "T" 때문에 티로 분류될 위험이 있음
            return ItemCategoryType.COAT;

        if (name.endsWith("PT"))
            return ItemCategoryType.PANTS;

        for (ItemCategoryType itemCategoryType : values()) {
            for (String abbreviation : itemCategoryType.abbreviations) {
                if (name.endsWith(abbreviation))
                    return itemCategoryType;
            }
        }
        return ItemCategoryType.NONE;
    }

    public ItemCategorySet getSet() {
        return categorySet;
    }

}
