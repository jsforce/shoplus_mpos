package com.teampls.shoplus.lib.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-04-21.
 */

public enum AccountLogType {
    CashPaid("현장결제", ColorType.Black.colorInt),
    Online("온라인", AccountType.OnlineAccount.colorInt), Entrust("대납", AccountType.EntrustedAccount.colorInt),
    OnlinePaid("온라인완료", ColorType.Black.colorInt), EntrustPaid("대납완료", ColorType.Black.colorInt),
    DepositPaid("매입 뺐음", ColorType.Black.colorInt), Deposit("매입", AccountType.DepositAccount.colorInt)
    ;
    public String uiStr;
    public int colorInt = 0;

    AccountLogType(String uiStr, int colorInt) {
        this.uiStr = uiStr;
        this.colorInt = colorInt;
    }

    public String getKey() {
        return "payment."+this.toString();
    }

    public static List<AccountLogType> getPendings(List<AccountLogType> accountLogTypes) {
        List<AccountLogType> results = new ArrayList<>();
        List<AccountLogType> pendingTypes = getPendingTypes();
        for (AccountLogType accountLogType : accountLogTypes) {
            if (pendingTypes.contains(accountLogType))
                results.add(accountLogType);
        }
        return results;
    }

    public static List<AccountLogType> getPendingTypes() {
        List<AccountLogType> results = new ArrayList<>();
        results.add(Online);
        results.add(Entrust);
        results.add(Deposit);
        return results;
    }
}
