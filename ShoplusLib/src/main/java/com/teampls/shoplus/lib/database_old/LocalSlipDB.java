package com.teampls.shoplus.lib.database_old;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.enums.DBResultType;
import com.teampls.shoplus.lib.enums.TransType;

/**
 * Created by Medivh on 2017-10-18.
 */

public class LocalSlipDB extends BaseDB<LocalSlipRecord> {
    private static LocalSlipDB instance = null;
    private static int Ver = 3;

    public enum Column {
        _id, slipKey, memo, receivable, transType;
        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    public static LocalSlipDB getInstance(Context context) {
        if (instance == null)
            instance = new LocalSlipDB(context);
        return instance;
    }

    private LocalSlipDB(Context context) {
        super(context, "LocalSlipDB", Ver, Column.toStrs());
    }

    @Override
    protected int getId(LocalSlipRecord record) {
        return record.id;
    }

    @Override
    protected LocalSlipRecord getEmptyRecord() {
        return new LocalSlipRecord();
    }

    @Override
    protected LocalSlipRecord createRecord(Cursor cursor) {
        TransType transType;
        try {
            transType = TransType.valueOf(cursor.getString(Column.transType.ordinal()));
        }  catch (IllegalArgumentException e) {
            transType = TransType.receipt;
        }
        return new LocalSlipRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.slipKey.ordinal()),
                cursor.getString(Column.memo.ordinal()),
                cursor.getInt(Column.receivable.ordinal()),
                transType
        );
    }

    public void toLogCat(String callLocation) {
        for (LocalSlipRecord record : getRecords())
            record.toLogCat(callLocation);
    }

    @Override
    protected ContentValues toContentvalues(LocalSlipRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.slipKey.toString(), record.slipKey);
        contentvalues.put(Column.memo.toString(), record.memo);
        contentvalues.put(Column.receivable.toString(), record.receivable);
        contentvalues.put(Column.transType.toString(), record.transType.toString());
        return contentvalues;
    }

    public LocalSlipRecord getRecordByKey(String slipKey) {
        // 반드시 있다고 보장하지 않는다
        if (hasValue(Column.slipKey, slipKey)) {
            return getRecord(Column.slipKey, slipKey);
        } else {
            return new LocalSlipRecord(0, slipKey, "", 0, TransType.receipt);
        }
    }

    public DBResult updateOrInsert(LocalSlipRecord record) {
        if (hasValue(Column.slipKey, record.slipKey)) {
            LocalSlipRecord dbRecord = getRecordByKey(record.slipKey);
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public void deleteBy(String slipKey) {
        for (int id : getIds(Column.slipKey, slipKey))
            delete(id);
    }
}
