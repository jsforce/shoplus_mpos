package com.teampls.shoplus.lib.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 사용자 설정 중, 사용자 타입 설정: 사용자 설정 및 미송/투데이 서비스 호출 파라미터에 적용
 * UserRelationType과 중복성이나, remote string 및 NOT_DEFINED의 허용 문제를 고려하여 우선 추가 타입을 정의
 */

public enum UserConfigurationType {
    NOT_DEFINED("","미확인"),
    PROVIDER("provider", "도매"),
    BUYER("buyer", "소매");

    private String remoteStr;
    public String uiStr;
    UserConfigurationType(String remoteStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
    }

    public String toRemoteStr() {
        if (this == NOT_DEFINED)
            return null;
        else
            return this.remoteStr;
    }

    public static UserConfigurationType fromRemoteStr(String remoteStr) {
        if (remoteStr == null) {
            return NOT_DEFINED;
        }
        for (UserConfigurationType type: UserConfigurationType.values()) {
            if (remoteStr.equals(type.remoteStr)) {
                return type;
            }
        }
        return NOT_DEFINED;
    }

    /**
     * 서비스 GET 호출 시의 파라미터
     *
     * @return
     */
    public Map<String, String> buildQueryParams() {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("usertype", this.remoteStr);
        return queryParams;
    }
}
