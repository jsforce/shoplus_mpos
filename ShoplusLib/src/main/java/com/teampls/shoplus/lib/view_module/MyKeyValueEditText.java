package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;

import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.event.MyOnTask;

/**
 * Created by Medivh on 2018-02-23.
 */

public class MyKeyValueEditText {
    private EditText editText;
    private KeyValueDB keyValueDB;
    private MyTextWatcher myTextWatcher;
    private MyOnTask<String> onTask;
    private String key = "";
    private View myView;
    private final String originalValue;

    public MyKeyValueEditText(Context context, View view, int resId, String key, String defaultValue) {
        this.key = key;
        this.myView = view;
        keyValueDB = KeyValueDB.getInstance(context);
        myTextWatcher = new MyTextWatcher();
        originalValue = keyValueDB.getValue(key, defaultValue);
        editText = (EditText) view.findViewById(resId);
        editText.setText(originalValue);
        editText.setSelection(editText.getText().toString().length());
        editText.addTextChangedListener(myTextWatcher);
    }

    public boolean isSameAsOriginal() {
        return originalValue.equals(getValue());
    }

    public void setAfterTextChanged(boolean doEventOnSet, MyOnTask<String> onTask) {
        this.onTask = onTask;
        if (doEventOnSet)
            editText.setText(editText.getText().toString());
    }

    public void setOnClearClick(int resId) {
        if (myView == null) return;
        myView.findViewById(resId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("");
                editText.requestFocus();
            }
        });
    }

    public EditText getEditText() {
        return editText;
    }

    public String getValue() {
        return editText.getText().toString();
    }

    public void saveValue() {
        keyValueDB.put(key, editText.getText().toString());
    }

    class MyTextWatcher extends BaseTextWatcher {

        @Override
        public void afterTextChanged(Editable editable) {
            if (enabled == false) return;
            enabled = false;
            if (onTask != null)
                onTask.onTaskDone(editable.toString()); // 여기서 다시 edittext에 글을 써 무한 루프가 생기지 않도록 방지
            enabled = true;
        }
    }
}
