package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-01-26.
 */

public class MySubContainers<T> extends ArrayList<MySubContainer> {
    private MySubContainer currentContainer;
    private View myView;
    private Button btPrev, btNext;
    protected boolean isValidInput = true;
    private MyOnTask checkInputVality;
    protected Context context;

    public MySubContainers(Context context, View view) {
        this.context = context;
        this.myView = view;
    }

    public void add(T module, int resId, boolean valid) {
        MySubContainer container = new MySubContainer(module, myView, resId, valid);
        add(container);
        if (currentContainer == null && valid) {
            currentContainer = container;
            container.setVisible(true);
        } else {
            container.setVisible(false);
        }
    }

    public T getCurrentModule() {
        if (currentContainer == null)
            return null;
        else
            return (T) currentContainer.module;
    }

    public List<MySubContainer> getValids() {
        List<MySubContainer> results = new ArrayList<>();
        for (MySubContainer record : this)
            if (record.valid)
                results.add(record);
        return results;
    }

    public void setValid(T module, boolean isValid) {
        for (MySubContainer record : this)
            if (record.getModule() == module)
                record.setValid(isValid);
    }

    public void setInputValidity(boolean value) {
        this.isValidInput = value;
    }

    public boolean isValidInput() {
        return isValidInput;
    }

    public void setNextClick(int resId, final MyOnClick<Boolean> onNextClick) {
        if (myView == null)
            return;
        btNext = (Button) myView.findViewById(resId);
        MyView.setTextViewByDeviceSize(context, btNext);
        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<MySubContainer> valids = getValids();
                if (valids.size() == 0) {
                    Log.w("DEBUG_JS", String.format("[MySubContainers.onClick] valids.size == 0"));
                    return;
                }

                if (checkInputVality != null)
                    checkInputVality.onTaskDone(getCurrentModule());

                if (isValidInput == false) {
                    return;
                }

                if (isLastContainer()) {
                    if (onNextClick != null)
                        onNextClick.onMyClick(null, true);
                } else {
                    int index = 0;
                    for (MySubContainer record : valids) {
                        if (currentContainer == record) {
                            currentContainer = valids.get(index + 1);
                            break;
                        }
                        index++;
                    }
                    refresh();
                    if (onNextClick != null)
                        onNextClick.onMyClick(null, false);
                }
            }
        });
        refresh();
    }

    public void setPrevClick(int resId, final MyOnClick<Boolean> onPrevClick) {
        btPrev = (Button) myView.findViewById(resId);
        MyView.setTextViewByDeviceSize(context, btPrev);
        btPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getValids().size() == 0) return;

                if (checkInputVality != null)
                    checkInputVality.onTaskDone(getCurrentModule());

                if (isValidInput == false) {
                    return;
                }

                if (isFirstContainer()) {
                    if (onPrevClick != null)
                        onPrevClick.onMyClick(view, true);
                } else {
                    int index = 0;
                    for (MySubContainer record : getValids()) {
                        if (currentContainer == record) {
                            currentContainer = getValids().get(index - 1);
                            break;
                        }
                        index++;
                    }
                    refresh();
                    if (onPrevClick != null)
                        onPrevClick.onMyClick(null, false);
                }
            }
        });
        refresh();
    }

    public boolean isFirstContainer() {
        if (currentContainer == null || getValids().size() <= 0)
            return false;
        return currentContainer == getValids().get(0);
    }

    public boolean isLastContainer() {
        if (currentContainer == null || getValids().size() <= 0)
            return false;
        return currentContainer == getValids().get(getValids().size()-1);
    }

    public void refresh() {
        for (MySubContainer record : this)
            record.setVisible(record == currentContainer);
        if (btPrev != null)
            btPrev.setText(isFirstContainer() ? "닫  기" : "이  전");
        if (btNext != null)
            btNext.setText(isLastContainer() ? "완  료" : "다   음");
    }

    public void setCurrentModule(T module) {
        for (MySubContainer record : this)
            if (record.getModule() == module) {
                currentContainer = record;
                break;
            }
    }

    public void toLogCat(String callLocation) {
        List<T> validModules = new ArrayList<>();
        for (MySubContainer subContainer : getValids())
            validModules.add((T) subContainer.module);
        Log.i("DEBUG_JS", String.format("[%s.%s] %s, %s", myView.getClass().getSimpleName(), callLocation, currentContainer.getModule(), validModules));
    }

    public void setCheckInputValidity(MyOnTask onTask) {
        this.checkInputVality = onTask;
    }

}
