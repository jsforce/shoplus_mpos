package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import com.teampls.shoplus.lib.awsservice.apigateway.protocol.APIInputModelInterface;
import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.enums.AutoMessageRequestTimeConfigurationType;
import com.teampls.shoplus.lib.enums.SlipGenerationType;
import com.teampls.shoplus.lib.protocol.PaymentTermsProtocol;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 서버로부터 받은 Slip 데이터를 처리하거나 서버로 Slip 데이터를 업로드하기 위한 데이터 클래스
 *
 * @author lucidite
 */

public class SlipJSONData implements SlipDataProtocol, APIInputModelInterface, JSONDataWrapper {
    private JSONObject data;

    public SlipJSONData(JSONObject data) {
        this.data = data;
    }

    public SlipJSONData(String response) throws JSONException {
        this.data = new JSONObject(response);
    }

    /**
     * Slip/Transaction 인스턴스를 생성한다
     * 클라이언트에서 Slip/Transaction을 생성하여 업로드하기 위한 용도로, cancellation 파라미터는 받지 않는다.
     * (클라이언트에서 취소된 Slip/Transaction을 생성할 수는 없다.)
     *
     * @param type
     * @param ownerPhoneNumber
     * @param counterpartPhoneNumber
     * @param createdDatetime
     * @param salesDate
     * @param imagePaths
     * @param items
     * @param paymentTerms
     * @param demandForUnpayment [SILVER-496] 거래 전 잔금에 대한 청구금액 (이 거래의 총 청구액은 이 값 + 거래에 따른 온라인/대납 청구액이다)
     * @param linkedChatbotOrderId [ORGR-16] 챗봇 주문으로부터 생성되는 거래기록일 경우 주문 ID를 전달 (이외에는 빈 문자열 또는 null을 전달)
     * @return
     * @throws JSONException
     */
    public static SlipJSONData build(SlipGenerationType type, String ownerPhoneNumber, String counterpartPhoneNumber,
                                     DateTime createdDatetime, DateTime salesDate, List<String> imagePaths,
                                     List<SlipItemDataProtocol> items, PaymentTermsProtocol paymentTerms, int demandForUnpayment,
                                     String linkedChatbotOrderId) throws JSONException {
        JSONObject data = new JSONObject();
        String slipTypeStr = type.toRemoteStr();
        if (slipTypeStr != null && !slipTypeStr.isEmpty()) {
            data.put("st", slipTypeStr);
        }
        data.put("uo", ownerPhoneNumber);
        data.put("cr", APIGatewayHelper.getRemoteDateTimeString(createdDatetime));
        if (isExist(counterpartPhoneNumber)) {
            data.put("uc", counterpartPhoneNumber);
        }

        // 판매일 정보가 null이거나 판매일과 생성일의 날짜 정보가 같은 경우 필드 자체를 생성하지 않는다(default).
        if ((salesDate != null) && (!isSameDate(salesDate, createdDatetime))) {
            data.put("sd", APIGatewayHelper.getRemoteDateString(salesDate));
        }

        JSONArray imagePathArr = new JSONArray();
        if (imagePaths != null) {
            for (String imagePath : imagePaths) {
                imagePathArr.put(imagePath);
            }
        }
        if (imagePathArr.length() > 0) {
            data.put("img", imagePathArr);
        }

        if (items != null && !items.isEmpty()) {
            data.put("its", SlipItemJSONData.toJSONArray(items));
        }

        if (paymentTerms != null) {
            PaymentTermsJSONData paymentData = new PaymentTermsJSONData(paymentTerms);
            data.put("tp", paymentData.getObject());
        }

        // [SILVER-496] 거래 전 잔금에 대한 청구금액 설정
        if (demandForUnpayment != 0) {
            data.put("du", demandForUnpayment);
        }

        // [ORGR-16] 챗봇 주문 완료 처리의 경우
        if (linkedChatbotOrderId != null && !linkedChatbotOrderId.isEmpty()) {
            data.put("oid", linkedChatbotOrderId);
        }

        return new SlipJSONData(data);
    }

    @Override
    public SlipDataKey getKey() {
        return new SlipDataKey(this.getOwnerPhoneNumber(), this.getCreatedDatetime());
    }

    @Override
    public SlipGenerationType getSlipType() {
        return SlipGenerationType.fromRemoteStr(this.data.optString("st"), SlipGenerationType.MANUAL);
    }

    @Override
    public String getOwnerPhoneNumber() {
        return this.data.optString("uo");
    }

    @Override
    public String getCounterpartPhoneNumber() {
        return this.data.optString("uc");
    }

    @Override
    public DateTime getCreatedDatetime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.data.optString("cr"));
    }

    @Override
    public List<String> getImagePaths() {
        List<String> result = new ArrayList<>();
        JSONArray imagePaths = this.data.optJSONArray("img");
        if (imagePaths != null) {
            for (int i = 0; i < imagePaths.length(); ++i) {
                result.add(imagePaths.optString(i, ""));
            }
        }
        return result;
    }

    @Override
    public List<SlipItemDataProtocol> getItems() {
        try {
            List<SlipItemDataProtocol> result = new ArrayList<>();
            JSONArray itemsArr = this.data.getJSONArray("its");
            for (int i = 0; i < itemsArr.length(); ++i) {
                result.add(new SlipItemJSONData(itemsArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();                    // 발생해서는 안 되는 에러임: 확인 필요
            return new ArrayList<>();                 // Fallback: 에러 대신 빈 맵을 반환한다.
        }
    }

    @Override
    public DateTime getSalesDate() {
        try {
            return APIGatewayHelper.getDateFromRemoteString(data.getString("sd"));
        } catch (JSONException e) {
            return this.getCreatedDatetime();       // Fallback: 판매일자가 설정되지 않은 경우 생성일시를 반환한다.
        }
    }

    @Override
    public PaymentTermsProtocol getPaymentTerms() {
        JSONObject termsOfPayment = this.data.optJSONObject("tp");
        return new PaymentTermsJSONData(termsOfPayment);            // 내부적으로 null 처리
    }

    @Override
    public Boolean isCompleted() {
        return this.data.optBoolean("cp");
    }

    @Override
    public Boolean isCancelled() {
        return this.data.has("cl");     // cl(=cancellation) 필드에 값이 존재하면 취소된 것임
    }

    @Override
    public Boolean isDeletionScheduled() {
        return this.data.has("dlt");    // dlt(=deleted) 필드에 값이 존재하면 삭제된 것임
    }

    @Override
    public DateTime getCancelledDatetime() {
        JSONObject cancellationData = this.data.optJSONObject("cl");
        if (cancellationData == null) {
            return APIGatewayHelper.fallbackDatetime;
        }
        String cancellationTimeStr = cancellationData.optString("d", "");
        if (cancellationTimeStr.isEmpty()) {
            return APIGatewayHelper.fallbackDatetime;
        }
        return APIGatewayHelper.getDateTimeFromRDSString(cancellationTimeStr);
    }

    @Override
    public String getComposerPhoneNumber() {
        return this.data.optString("emp", this.getOwnerPhoneNumber());
    }

    @Override
    public String getLinkedChatbotOrderId() {
        return this.data.optString("oid", "");
    }

    @Override
    public int getDemandForUnpayment() {
        return this.data.optInt("du", 0);
    }

    @Override
    public String toInputModelString() {
        return data.toString();
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return this.data;
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("SlipJSONData instance does not have a JSON array");
    }

    public void setAsTransaction() throws JSONException {
        this.data.put("type", "transaction");
    }

    public static List<SlipDataProtocol> build(String response) throws JSONException {
        List<SlipDataProtocol> result = new ArrayList<>();
        JSONArray dataArr = new JSONArray(response);
        for (int i = 0; i < dataArr.length(); ++i) {
            result.add(new SlipJSONData(dataArr.getJSONObject(i)));
        }
        return result;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // Utility methods

    private static Boolean isExist(String value) {
        return (value != null) && (!value.isEmpty());
    }

    /**
     * 두 DateTime의 날짜 정보가 같은지 체크한다. 시간 정보는 비교하지 않는다.
     *
     * @param d1
     * @param d2
     * @return
     */
    private static Boolean isSameDate(DateTime d1, DateTime d2) {
        return (d1.getYear() == d2.getYear()) && (d1.getMonthOfYear() == d2.getMonthOfYear()) && (d1.getDayOfMonth() == d2.getDayOfMonth());
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // 자동 메시지 서비스 관련

    public void setAutoMessageConfiguration(AutoMessageRequestTimeConfigurationType config) throws JSONException {
        if (config == null || config.equals(AutoMessageRequestTimeConfigurationType.DO_NOT_SEND)) {
            this.data.remove("km");
        } else {
            this.data.put("km", config.getCommandValue());
        }
    }
}
