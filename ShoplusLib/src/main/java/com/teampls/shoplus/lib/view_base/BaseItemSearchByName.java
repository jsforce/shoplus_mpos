package com.teampls.shoplus.lib.view_base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseItemDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyKeyboard;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.ItemDataBundle;
import com.teampls.shoplus.lib.datatypes.SpecificItemDataBundle;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnClickPair;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2017-06-20.
 */

public class BaseItemSearchByName extends BaseActivity implements AdapterView.OnItemClickListener, AbsListView.OnScrollListener {
    protected BaseItemDBAdapter adapter;
    protected ListView listView;
    protected EditText etName;
    public static String KEY_SHOW_ALL = "<전체보기>", KEY_SEARCH = "",
            KEY_Sorting = "itemSearch.byName.sorting", KEY_IgnoreSpace = "itemSearch.byName.ignoreSpace";
    public List<ItemRecord> fullRecords = new ArrayList<>();
    protected static MyOnClick<ItemRecord> onItemClick;
    protected static MyOnClickPair<String, List<ItemRecord>> onShowSelectedClick;
    protected MyKeyboard myKeyboard;
    protected TextView tvMore;
    protected Set<String> keywords = new HashSet<>();
    protected TaskState onSearching = TaskState.beforeTasking;
    protected MyTextWatcher myTextWatcher;
    protected ItemDB itemDB;
    protected static boolean doFinishOnItemClick = false, searchMoreEnabled = true;
    protected MyRadioGroup<ItemSearchType> rgSearchType;
    protected static ItemSearchType itemSearchType = ItemSearchType.ByName;

    public static void startActivity(Context context, ItemSearchType itemSearchType, String itemSearchTypeKey,
                                     boolean searchMoreEnabled, boolean doFinishOnItemClick, MyOnClick<ItemRecord> onItemClick, MyOnClickPair<String, List<ItemRecord>> onShowSelectedClick) {
        BaseItemSearchByName.itemSearchType = itemSearchType;
        BaseItemSearchByName.KEY_SEARCH = itemSearchTypeKey; // update 용
        BaseItemSearchByName.onItemClick = onItemClick;
        BaseItemSearchByName.searchMoreEnabled = searchMoreEnabled;
        BaseItemSearchByName.onShowSelectedClick = onShowSelectedClick;
        BaseItemSearchByName.doFinishOnItemClick = doFinishOnItemClick;
        ((Activity) context).startActivityForResult(new Intent(context, BaseItemSearchByName.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        rgSearchType = new MyRadioGroup<>(context, getView());
        rgSearchType.add(R.id.item_search_by_name_name, ItemSearchType.ByName)
                .add(R.id.item_search_by_name_number, ItemSearchType.BySerialNum);
        rgSearchType.setChecked(itemSearchType);

        myActionBar.hide();
        myTextWatcher = new MyTextWatcher();
        setItemDB(); // setAdapter보다 앞에 있어야 함
        setAdapter();

        tvMore = findTextViewById(R.id.item_search_by_name_more);
        etName = (EditText) findViewById(R.id.item_search_by_name_edittext);
        etName.addTextChangedListener(myTextWatcher);

        listView = (ListView) findViewById(R.id.item_search_by_name_listview);
        listView.setOnItemClickListener(this);
        listView.setOnScrollListener(this);
        listView.setAdapter(adapter);

        setOnClick(R.id.item_search_by_name_clear, R.id.item_search_by_name_close,
                R.id.item_search_by_name_showAll, R.id.item_search_by_name_setting,
                R.id.item_search_by_name_more);

        myKeyboard = new MyKeyboard(context, getView(), R.id.item_search_by_name_container, R.id.item_search_by_name_edittext);

        refreshFullRecords();

        MyView.setTextViewByDeviceSize(context, getView(), R.id.item_search_by_name_name,
                R.id.item_search_by_name_number,
                R.id.item_search_by_name_edittext);
        MyView.setImageViewSizeByDeviceSizeScale(context, getView(), 0.5,
                R.id.item_search_by_name_showAll, R.id.item_search_by_name_close, R.id.item_search_by_name_clear);

        rgSearchType.setOnClickListener(false, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (rgSearchType.getCheckedItem()) {
                    case ByName:
                        if (MyDevice.getAndroidVersion() < 27)
                            etName.setHint("이름 일부분 입력");
                        etName.setText("");
                        etName.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                    case BySerialNum:
                        if (MyDevice.getAndroidVersion() < 27)
                            etName.setHint("상품번호 6자리 (올해것은 뒷4자리만)");
                        etName.setText("");
                        etName.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
                        MyUI.toastSHORT(context, String.format("연도 2자리 상품번호 4자리를 입력해주세요 (예) 180023"));
                        break;
                }
                if (KEY_SEARCH.isEmpty() == false)
                    keyValueDB.put(KEY_SEARCH, rgSearchType.getCheckedItem().toString());
                myKeyboard.show();
            }
        });
        rgSearchType.click();

        if (BaseAppWatcher.isNetworkConnected() == false) {
            tvMore.setEnabled(false);
            findTextViewById(R.id.item_search_by_name_moreGuide, "오프라인에서는 '검색'은 사용하실 수 없습니다.");
        }

        if (searchMoreEnabled == false) {
            tvMore.setVisibility(View.GONE);
            setVisibility(View.GONE, R.id.item_search_by_name_moreGuide);
        }

        if (onShowSelectedClick == null)
            findViewById(R.id.item_search_by_name_showAll).setVisibility(View.GONE);
    }

    protected void setAdapter() {
        adapter = new BaseItemDBAdapter(context) {
            @Override
            public BaseViewHolder getViewHolder() {
                return new ItemSearchByNameViewHolder();
            }
        };
        adapter.addFirstRecord(KEY_SHOW_ALL);
        adapter.setOrderRecord(ItemDB.Column.name, false);
    }

    protected void setItemDB() {
        itemDB = ItemDB.getInstance(context);
    }

    @Override
    public int getThisView() {
        return R.layout.base_item_search_by_name;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.item_search_by_name_more) {
            String keyword = etName.getText().toString().trim();
            if (keyword.length() < 2) {
                MyUI.toastSHORT(context, String.format("2자 이상이 필요합니다"));
                return;
            }
            onMoreClick(keyword);

        } else if (view.getId() == R.id.item_search_by_name_close) {
            MyDevice.hideKeyboard(context, etName);
            doFinishForResult();

        } else if (view.getId() == R.id.item_search_by_name_clear) {
            etName.setText("");

        } else if (view.getId() == R.id.item_search_by_name_showAll) {
            myKeyboard.hide();
            doFinishForResult();
            List<ItemRecord> selectedRecords = adapter.getRecords();
            selectedRecords.remove(0); // first record
            if (onShowSelectedClick != null)
                onShowSelectedClick.onMyClick(view, etName.getText().toString(), selectedRecords);

        } else if (view.getId() == R.id.item_search_by_name_setting) {
            new SettingDialog(context).show();
        }
    }

    class SettingDialog extends MyButtonsDialog {

        public SettingDialog(final Context context) {
            super(context, "이름검색 설정", "");
            MSortingKey key = MSortingKey.valueOf(keyValueDB, KEY_Sorting, MSortingKey.ID);
            addRadioButtons("가나다순", MSortingKey.Name, "최신순", MSortingKey.ID, key, new MyOnTask<MSortingKey>() {
                @Override
                public void onTaskDone(MSortingKey result) {
                    myTextWatcher.enabled = false;
                    etName.setText("");
                    myTextWatcher.enabled = true;
                    keyValueDB.put(KEY_Sorting, result.toString());
                    refreshFullRecords();
                }
            });

            addCheckBox("이름내 공란무시 (시범기능)", KEY_IgnoreSpace, true, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    if (result)
                        MyUI.toastSHORT(context, String.format("공란을 무시합니다. 앱이 느려지면 꺼주세요"));
                    myTextWatcher.afterTextChanged(etName.getText());
                }
            });
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
        ItemRecord record = adapter.getRecord(position); // refresh에 의해 삭제된 아이템일 수 있다

        if (record.name.equals(KEY_SHOW_ALL)) {
            if (onItemClick != null)
                onItemClick.onMyClick(view, record);
            myKeyboard.hide();
            doFinishForResult(); // 무조건 끝내야
            return;
        }

        if (record.itemId <= 0) {
            MyUI.toastSHORT(context, String.format("상품 정보에 오류가 발견되었습니다. 새로고침을 해주세요"));
            return;
        }

        if (itemDB.has(record.itemId) == false) {
            downloadItem(record.itemId, new MyOnTask<ItemRecord>() {
                @Override
                public void onTaskDone(ItemRecord downloadedRecord) {
                    if (onItemClick != null)
                        onItemClick.onMyClick(view, downloadedRecord);
                    if (doFinishOnItemClick) {
                        myKeyboard.hide();
                        doFinishForResult();
                    }
                }
            });
        } else {
            if (onItemClick != null)
                onItemClick.onMyClick(view, record);
            if (doFinishOnItemClick) {
                myKeyboard.hide();
                doFinishForResult();
            }
        }
    }

    private void downloadItem(final int itemId, final MyOnTask<ItemRecord> onTask) {
        new CommonServiceTask(context, "", "정보 다운 중...") {
            ItemRecord itemRecord = new ItemRecord();

            @Override
            public void doInBackground() throws MyServiceFailureException {
                SpecificItemDataBundle bundle = itemService.getSpecificItem(userSettingData.getUserShopInfo(), itemId);
                itemDB.updateOrInsertWithOptionSync(new ItemDataBundle(bundle));
                itemRecord = new ItemRecord(bundle.getItem());
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone(itemRecord);
            }
        };
    }

    public void onMoreClick(final String keyword) {
        switch (rgSearchType.getCheckedItem()) {
            case ByName:
                if (keywords.contains(keyword)) {
                    new MyAlertDialog(context, "이미 검색한 키워드", String.format("[%s] 키워드는 이미 검색했습니다. 한번 더 하시겠습니까?", keyword)) {
                        @Override
                        public void yes() {
                            downloadItemsBy(keyword);
                        }

                        public void no() {
                            tvMore.setVisibility(View.GONE);
                        }
                    };
                } else {
                    keywords.add(keyword);
                    downloadItemsBy(keyword);
                }
                break;

            case BySerialNum:
                String serialStr = BaseUtils.toItemSerialStr(etName.getText().toString());
                if (serialStr.isEmpty()) {
                    MyUI.toastSHORT(context, String.format("올바른 형식이 아닙니다"));
                    return;
                } else {
                    itemDB.searchRecord(context, serialStr, new MyOnTask<ItemRecord>() {
                        @Override
                        public void onTaskDone(ItemRecord itemRecord) {
                            if (itemRecord.itemId == 0) {
                                MyUI.toastSHORT(context, String.format("찾지못함"));
                            } else {
                                itemDB.updateOrInsert(itemRecord);
                                MyUI.toastSHORT(context, String.format("%s 찾음", itemRecord.name));
                                refreshFullRecords();
                            }
                        }
                    });
                }
                break;
        }
    }

    private void downloadItemsBy(final String keyword) {
        if (onSearching.isOnTasking()) {
            MyUI.toastSHORT(context, String.format("검색 중입니다"));
            return;
        }
        onSearching = TaskState.onTasking;

        new CommonServiceTask(context, "downloadItemsBy", String.format("[%s] 검색중..", keyword)) {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                ItemDataBundle itemDataBundle = itemService.searchItems(userSettingData.getUserShopInfo(), keyword);
                itemDB.updateOrInsert(itemDataBundle);
                if (itemDataBundle.getItems().size() == 0) {
                    MyUI.toastSHORT(context, String.format("발견되지 않음"));
                } else {
                    MyUI.toastSHORT(context, String.format("%d건 발견됨", itemDataBundle.getItems().size()));
                }
            }

            @Override
            public void onPostExecutionUI() {
                onSearching = TaskState.afterTasking;
                refreshFullRecords();
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                onSearching = TaskState.onError;
            }
        };
    }

    protected void refreshFullRecords() {
        adapter.resetRecords();
        adapter.blockEmptyName();
        adapter.setSortingKey(MSortingKey.valueOf(keyValueDB, KEY_Sorting, MSortingKey.ID));
        adapter.generate(false);
        fullRecords = adapter.getGeneratedRecords();

        //   setAsRecentRecords();
        myTextWatcher.afterTextChanged(etName.getText());
    }

    private void setAsRecentRecords() {
        List<ItemRecord> recentRecords = itemDB.getRecentRecords();
        recentRecords = cleanUpEmptyAndDuplicate(recentRecords);
        if (recentRecords.size() >= 1)
            adapter.setRecords(recentRecords);
    }

    private List<ItemRecord> cleanUpEmptyAndDuplicate(List<ItemRecord> records) {
        List<ItemRecord> results = new ArrayList<>();
        List<Integer> itemIds = new ArrayList<>();
        for (ItemRecord record : records) {
            if (record.name.isEmpty())
                continue;
            if (itemIds.contains(record.itemId))
                continue;
            results.add(record);
            itemIds.add(record.itemId);
        }
        return results;
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onMyMenuCreate() {
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {
    }


    // fullRecords에서 복사해서 쓰는 방식
    class MyTextWatcher extends BaseTextWatcher {

        @Override
        public void afterTextChanged(Editable s) {
            if (enabled == false)
                return;
            enabled = false;

            String searchKeyword = s.toString().toLowerCase().trim();
            boolean doIgnoreSpace = keyValueDB.getBool(KEY_IgnoreSpace, true);
            List<ItemRecord> results = new ArrayList<>();
            int count = 0;
            ItemSearchType searchType = rgSearchType.getCheckedItem();

            for (ItemRecord record : fullRecords) {
                if (searchKeyword.isEmpty()) {
                    results.add(record);
                } else if (adapter.isFirstRecordAdded() && count == 0) {
                    results.add(record);
                } else {
                    switch (searchType) {
                        case ByName:
                            String recordName = record.name.toLowerCase().trim();
                            if (doIgnoreSpace) {
                                if (recordName.replace(" ", "").contains(searchKeyword.replace(" ", "")))
                                    results.add(record);
                            } else {
                                if (recordName.contains(searchKeyword))
                                    results.add(record);
                            }
                            break;
                        case BySerialNum:
                            if (record.serialNum.replace("-", "").contains(searchKeyword.replace("-", "")))
                                results.add(record);
                            break;
                    }
                }
                count++;
            }
            adapter.setRecords(results);
            refresh();
            enabled = true;
        }
    }
}
