package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.datatypes.TransactionNoteData;
import com.teampls.shoplus.lib.dialog.MyMultilinesDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_base.BaseActivity;

import java.util.List;

/**
 * Created by Medivh on 2019-04-11.
 */

public class TransNoteView extends BaseActivity implements AdapterView.OnItemClickListener {
    private ExpandableHeightListView lvNotes;
    private TransNoteAdapter adapter;
    private static String transactionId = "";
    private int noteCount = Integer.MAX_VALUE;
    private static MyOnTask<List<String>> onAddSlipClick;

    public static void startActivity(Context context, String transactionId, MyOnTask<List<String>> onAddSlipClick) {
        TransNoteView.onAddSlipClick = onAddSlipClick;
        TransNoteView.transactionId = transactionId;
        context.startActivity(new Intent(context, TransNoteView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;

        adapter = new TransNoteAdapter(context);
        lvNotes = findViewById(R.id.trans_note_listview);
        lvNotes.setExpanded(true);
        lvNotes.setOnItemClickListener(this);
        lvNotes.setAdapter(adapter);

        setOnClick(R.id.trans_note_add, R.id.trans_note_add_button,
                R.id.trans_note_cancel);

        downloadNotes(transactionId);
    }

    @Override
    public int getThisView() {
        return R.layout.activity_trans_note;
    }

    private void downloadNotes(final String transactionId) {
        new CommonServiceTask(context, "TransNoteView", "메모 다운 중...") {
            private List<TransactionNoteData> notes;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                notes = transactionService.getTransactionNotes(userSettingData.getUserShopInfo(), transactionId);
                noteCount = notes.size();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.setRecords(notes);
                refresh();
            }
        };
    }

    private void addNotes(final String transactionId, final String note) {
        new CommonServiceTask(context, "TransNoteView", "메모 추가 중...") {
            private List<TransactionNoteData> notes;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                notes = transactionService.postTransactionNote(userSettingData.getUserShopInfo(), transactionId, note);
                noteCount = notes.size();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.setRecords(notes);
                refresh();
            }
        };
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.trans_note_add || view.getId() == R.id.trans_note_add_button) {
            int limit = 2;
            if (userSettingData.isActivated(ShoplusServiceType.POS))
                limit = Integer.MAX_VALUE;
            if (noteCount >= limit) {
                MyUI.toastSHORT(context, String.format("최대 개수입니다"));
                return;
            }
            new UpdateValueDialog(context, "새 메모 입력", "", true) {
                @Override
                public void onApplyClick(String newValue) {
                    if (newValue.isEmpty() == false)
                        addNotes(transactionId, newValue);
                }
            };
        } else if (view.getId() == R.id.trans_note_cancel) {
            finish();
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case refresh:
                downloadNotes(transactionId);
                break;
            case close:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TransactionNoteData record = adapter.getRecord(position);
        MyMultilinesDialog myMultilinesDialog = new MyMultilinesDialog(context, record.getCreatedDateTime().toString(BaseUtils.MDHm_Kor_Format), record.getNoteText());
        myMultilinesDialog.show();
        if (onAddSlipClick != null) {
            myMultilinesDialog.setOnFunctionClick("영수증에 추가", new MyOnTask<List<String>>() {
                @Override
                public void onTaskDone(List<String> result) {
                    onAddSlipClick.onTaskDone(result);
                    finish();
                }
            });
        }
    }

    class TransNoteAdapter extends BaseDBAdapter<TransactionNoteData> {

        public TransNoteAdapter(Context context) {
            super(context);
        }

        @Override
        public void generate() {

        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new BaseViewHolder() {
                private TextView tvDate, tvNote;

                @Override
                public int getThisView() {
                    return R.layout.row_trans_notes;
                }

                @Override
                public void init(View view) {
                    tvDate = findTextViewById(context, view, R.id.row_trans_notes_date);
                    tvNote = findTextViewById(context, view, R.id.row_trans_notes_note);
                }

                @Override
                public void update(int position) {
                    TransactionNoteData record = adapter.getRecord(position);
                    tvDate.setText(BaseUtils.toStringWithWeekDay(record.getCreatedDateTime(), BaseUtils.MDHm_Twolines_Format));
                    tvNote.setText(record.getNoteText());
                }
            };
        }
    }
}
