package com.teampls.shoplus.lib.common;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.hardware.Camera;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.message.template.ButtonObject;
import com.kakao.message.template.ContentObject;
import com.kakao.message.template.FeedTemplate;
import com.kakao.message.template.LinkObject;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;
import com.kakao.network.storage.ImageUploadResponse;
import com.teampls.shoplus.lib.database.MyQuery;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.datatypes.NewArrivalsLinkExecutionParamsData;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.event.MyOnTask;

import org.joda.time.DateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;
import static android.content.Context.WINDOW_SERVICE;
import static android.media.ToneGenerator.TONE_CDMA_HIGH_S_X4;
import static com.kakao.util.helper.Utility.getPackageInfo;
import static com.teampls.shoplus.lib.common.BaseGlobal.KakaoPackage;
import static com.teampls.shoplus.lib.common.MyDevice.DeviceSize.W360;
import static com.teampls.shoplus.lib.common.MyDevice.DeviceSize.W400;

/**
 * Created by Medivh on 2016-09-05.
 */
public class MyDevice {
    public static final String rootDir = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String pictures = rootDir + "/" + "Pictures";
    public static final String teamplDir = rootDir + "/" + "TEAMPL";
    public static final String tempPNG = teamplDir + "/temp.png";
    public static final String tempJPG = teamplDir + "/temp.jpg";

    public static final String cameraDir = rootDir + "/DCIM/Camera";
    public static final String kakaoDir = rootDir + "/Pictures/KakaoTalk";
    public static final String beautyPlusDir = rootDir + "/BeautyPlus";
    public static final String xperiaCameraDir = rootDir + "/DCIM/100ANDRO";
    public static final String htcCameraDir = rootDir + "/DCIM/100MEDIA";
    private static final String cameraImageBucketId = getBucketId(cameraDir);
    private static final String xperiaCameraImageBucketId = getBucketId(xperiaCameraDir);
    private static final String htcCameraImageBucketId = getBucketId(htcCameraDir);
    private static final String kakaoImageBucketId = getBucketId(kakaoDir);
    private static final String beautyPlusImageBucketId = getBucketId(beautyPlusDir);
    public static final String CONNECTION_CONFIRM_CLIENT_URL = "http://clients3.google.com/generate_204";

    private static final Uri URI = Uri.parse("content://com.google.android.gsf.gservices");
    private static final String ID_KEY = "android_id";
    private static String marketVersion = "";
    private static String modifiedContactId = "";

    public static void copyToClipboard(Context context, String title, String message) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(title, message);
        clipboard.setPrimaryClip(clip);
        MyUI.toastSHORT(context, String.format("클립보드에 복사했습니다"));
    }

    public static void restartApp(final Context context, String message) {
        if (message.isEmpty() == false)
            MyUI.toast(context, message);
        new MyDelayTask(context, 1000) {
            @Override
            public void onFinish() {
                MyDevice.openApp(context, context.getPackageName());
            }
        };

        if (context instanceof Activity)
            ((Activity) context).finishAffinity();
        else if (context instanceof PreferenceActivity)
            ((PreferenceActivity) context).finishAffinity();
    }

    public static void beep(int volume, int duration) {
        ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_ALARM, volume);
        toneGenerator.startTone(TONE_CDMA_HIGH_S_X4, duration);
    }

    public static void beepAlertAsync(Context context) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                toneGenerator.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
            }
        };
    }

    public static float getFontScale(Context context) {
        return context.getResources().getConfiguration().fontScale;
    }

    public static boolean isLargeFont(Context context) {
        return getFontScale(context) >= 1.3;
    }


    public static void changeAppIcon(Context context, String aliasName, boolean toSecurityMode) {
        PackageManager pm = context.getPackageManager();
        if (toSecurityMode) {
            pm.setComponentEnabledSetting(
                    new ComponentName(context, aliasName),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
            pm.setComponentEnabledSetting(((Activity) context).getComponentName(),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
            Log.w("DEBUG_JS", String.format("[MyDevice.changeAppIcon] change to Security Mode"));
        } else {
            pm.setComponentEnabledSetting(((Activity) context).getComponentName(),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
            pm.setComponentEnabledSetting(
                    new ComponentName(context, aliasName),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
        }

    }

    public static void openWeb(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }

    public static String getKeyHash(final Context context) {
        PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
        if (packageInfo == null)
            return null;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return Base64.encodeToString(md.digest(), Base64.NO_WRAP);
            } catch (NoSuchAlgorithmException e) {
                Log.i("DEBUG_JS", String.format("[MyDevice.getKeyHash] %s, %s", signature, e));
            }
        }
        return null;
    }

    public enum DeviceSize {
        W320(320), W360(360), W383(383), W400(400), W411(411), W600(600), W720(720), W800(800), W1000(1000);
        public int size = 0;

        DeviceSize(int size) {
            this.size = size;
        }
    }

    public static int getColumnNum(Context context) {
        if (isLandscapeScreen(context)) {
            return isLessThan(context, W400) ? 5 : 6;
        } else {
            return isLessThan(context, W400) ? 3 : 4;
        }
    }

    public static int getOrderingColumnNum(Context context) {
        if (isLandscapeScreen(context)) {
            return isLessThan(context, W400) ? 4 : 5;
        } else {
            return isLessThan(context, W400) ? 2 : 3;
        }
    }

    public static DeviceSize getDeviceSize(Context context) {
        int myDeviceWidthDp = 0;
        if (isLandscapeScreen(context)) {
            myDeviceWidthDp = getHeightDP(context);
        } else {
            myDeviceWidthDp = getWidthDP(context);
        }
        for (DeviceSize deviceSize : DeviceSize.values()) {
            if (myDeviceWidthDp <= deviceSize.size) { // 가장 근접한 것 중에서 큰 것
                return deviceSize;
            }
        }
        return W360;
    }

    public static boolean isLessThan(Context context, DeviceSize refSize) {
        if (isLandscapeScreen(context)) {
            return (getHeightDP(context) <= refSize.size);
        } else {
            return (getWidthDP(context) <= refSize.size);
        }
    }

    public static boolean isTablet(Context context) {
        return (getWidthDP(context) >= DeviceSize.W600.size);
    }

    public static boolean isRunning(Context context, String packageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcessInfo : activityManager.getRunningAppProcesses()) {
            if (appProcessInfo.processName.contains(packageName))
                return true;
        }
        return false;
    }

    public static void openApp(Context context, String packageName) {
        openActivity(context, packageName, "");
    }

    public static void openActivity(Context context, String packageName, String activityName) {
        if (MyDevice.hasPackage(context, packageName)) {
            Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
            if (!TextUtils.isEmpty(activityName) && isRunning(context, packageName)) {
                try {
                    Intent activeIntent = new Intent(activityName);
                    activeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    context.startActivity(activeIntent); // 실제 없는 경우 null이 아니라 여기서 No Activity found  에러가 발생
                } catch (Exception e) {
                    Log.e("DEBUG_JS", String.format("[MyDevice.openActivity] %s", e.getLocalizedMessage()));
                    context.startActivity(intent);
                }
            } else {
                //  Log.i("DEBUG_JS", String.format("[MyDevice.openActivity] acitivityName = %s, isRunning? %s", TextUtils.isEmpty(activityName), isRunning(context, packageName)));
                context.startActivity(intent);
            }
        } else {
            MyDevice.openPlayStore(context, packageName);
        }
    }

    public static void callPhone(Context context, String phoneNumber) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT) {
            MyUI.toastSHORT(context, String.format("유심칩이 없습니다"));
            return;
        }

        if (hasPermission(context, UserPermission.CALL_PHONE)) {
            try {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                context.startActivity(intent);
            } catch (SecurityException e) {

            }
        } else {
            requestPermission(context, UserPermission.CALL_PHONE);
            MyUI.toastSHORT(context, String.format("전화 걸기 권한이 필요합니다"));
        }
    }

    public static void sendKakaoNewArravals(final Context context, final String message, Bitmap bitmap, final String linkUrl,
                                            final String mainImagePath, final boolean doShowAppLink, final MyOnTask onTask) {
        MyUI.toast(context, String.format("[친구탭]에서 10명까지 선택해주세요"));

        final String filePath = createTempFilePath(BaseAppWatcher.appDir, "jpg");
        MyGraphics.toFile(bitmap, filePath);
        KakaoLinkService.getInstance().uploadImage(context, false, new File(filePath), new ResponseCallback<ImageUploadResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Log.e("DEBUG_JS", String.format("[KakaoLinkService.uploadImage] %s", errorResult.toString()));
            }

            @Override
            public void onSuccess(ImageUploadResponse result) {
                MyDevice.deleteFiles(context, filePath);
                String execParams = new NewArrivalsLinkExecutionParamsData(linkUrl, mainImagePath).getExecutionParamsStr();
                ContentObject contentObject = ContentObject.newBuilder(message, result.getOriginal().getUrl(), getLinkObject(linkUrl)).build();
                ButtonObject leftButton = new ButtonObject("웹에서 보기", getLinkObject(linkUrl));

                FeedTemplate params;
                if (doShowAppLink) {
                    ButtonObject rightButton = new ButtonObject("앱에서 보기", getLinkObject(linkUrl, execParams));
                    params = FeedTemplate.newBuilder(contentObject).addButton(leftButton).addButton(rightButton).build();
                } else {
                    params = FeedTemplate.newBuilder(contentObject).addButton(leftButton).build();
                }
                KakaoLinkService.getInstance().sendDefault(context, params, new ResponseCallback<KakaoLinkResponse>() {
                            @Override
                            public void onFailure(ErrorResult errorResult) {
                                Log.e("DEBUG_JS", String.format("[KakaoLinkService.send] %s", errorResult.toString()));
                            }

                            @Override
                            public void onSuccess(KakaoLinkResponse result) {
                                if (onTask != null)
                                    onTask.onTaskDone("");
                            }
                        }
                );
            }
        });
    }

    public static void sendKakaoPendings(final Context context, Bitmap bitmap, final String linkUrl) {
        final String filePath = createTempFilePath(BaseAppWatcher.appDir, "jpg");
        MyGraphics.toFile(bitmap, filePath);
        KakaoLinkService.getInstance().uploadImage(context, false, new File(filePath), new ResponseCallback<ImageUploadResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Log.e("DEBUG_JS", String.format("[KakaoLinkService.uploadImage] %s", errorResult.toString()));
            }

            @Override
            public void onSuccess(ImageUploadResponse result) {
                MyDevice.deleteFiles(context, filePath);
                ContentObject contentObject = ContentObject.newBuilder("미완료 거래 내역을 보내드립니다", result.getOriginal().getUrl(), getLinkObject(linkUrl)).build();
                //LinkObject appLink = LinkObject.newBuilder().setAndroidExecutionParams(BaseGlobal.SHOPLUS_DALARAN_GOOGLE).setIosExecutionParams(BaseGlobal.SHOPLUS_DALARAN_APPLE).build();
                ButtonObject leftButton = new ButtonObject("웹에서 보기", getLinkObject(linkUrl));
                ButtonObject rightButton = new ButtonObject("더 알아보기", getLinkObject(BaseGlobal.SHOPL_US));
                FeedTemplate params = FeedTemplate.newBuilder(contentObject).addButton(leftButton).addButton(rightButton).build();

                KakaoLinkService.getInstance().sendDefault(context, params, new ResponseCallback<KakaoLinkResponse>() {
                            @Override
                            public void onFailure(ErrorResult errorResult) {
                                Log.e("DEBUG_JS", String.format("[KakaoLinkService.send] %s", errorResult.toString()));
                            }

                            @Override
                            public void onSuccess(KakaoLinkResponse result) {

                            }
                        }
                );
            }
        });

    }

    private static LinkObject getLinkObject(String url) {
        return LinkObject.newBuilder().setWebUrl(url).setMobileWebUrl(url).setAndroidExecutionParams(null).setIosExecutionParams(null).build();
    }

    private static LinkObject getLinkObject(String url, String params) {
        return LinkObject.newBuilder().setAndroidExecutionParams(params).setIosExecutionParams(params).build();
    }


    private static File toImageFile(Bitmap bitmap) {
        // 최소 200 px * 200 px 필요
        if (bitmap.getWidth() < 200 || bitmap.getHeight() < 200) {
            Log.w("DEBUG_JS", String.format("[MyDevice.sendKakaoSlip] invalid bitmap size %d x %d", bitmap.getWidth(), bitmap.getHeight()));
            if (bitmap.getWidth() > bitmap.getHeight()) {
                bitmap = MyGraphics.resizeByHeight(bitmap, 200);
            } else {
                bitmap = MyGraphics.resizeByWidth(bitmap, 200);
            }
        }

        // 사각형으로 만들고 빈공간은 흰색으로
        if (bitmap.getWidth() > bitmap.getHeight()) {
            Bitmap rectBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            rectBitmap.eraseColor(ColorType.White.colorInt);
            Canvas canvas = new Canvas(rectBitmap);
            Rect rect = new Rect(0, 0, bitmap.getWidth(), Math.min(bitmap.getWidth(), bitmap.getHeight()));
            canvas.drawBitmap(bitmap, rect, rect, null);
            bitmap = rectBitmap;
        }

        final String filePath = createTempFilePath(BaseAppWatcher.appDir, "jpg");
        MyGraphics.toFile(bitmap, filePath);
        File imageFile = new File(filePath);
        // file size는 2M 이하
        if (imageFile.length() > 2e6) {
            Log.w("DEBUG_JS", String.format("[MyDevice.sendKakaoSlip] too big %dkB", imageFile.length() / 1000));
            bitmap = MyGraphics.resizeBySampling(bitmap, 2);
            toImageFile(bitmap);
            //sendKakaoSlip(context, bitmap, linkUrl);
            return new File("");
        }
        return imageFile;
    }

    private static void sendKakaoImage(final Context context, final File imageFile, final String title, final String rightButtonTitle, final String rightButtonLink) {
        KakaoLinkService.getInstance().uploadImage(context, false, imageFile, new ResponseCallback<ImageUploadResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Log.e("DEBUG_JS", String.format("[KakaoLinkService.uploadImage] %s", errorResult.toString()));
            }

            @Override
            public void onSuccess(ImageUploadResponse result) {
                MyDevice.deleteFiles(context, imageFile.getPath());
                String imageLinkUrl = result.getOriginal().getUrl();
                ContentObject contentObject = ContentObject.newBuilder(title, imageLinkUrl, getLinkObject(imageLinkUrl)).build();
                ButtonObject leftButton = new ButtonObject("웹으로 보기", getLinkObject(imageLinkUrl));
                ButtonObject rightButton = new ButtonObject(rightButtonTitle, getLinkObject(rightButtonLink));
                FeedTemplate params;
                if (rightButtonLink.isEmpty()) {
                    params = FeedTemplate.newBuilder(contentObject).addButton(leftButton).build();
                } else {
                    params = FeedTemplate.newBuilder(contentObject).addButton(leftButton).addButton(rightButton).build();
                }

                KakaoLinkService.getInstance().sendDefault(context, params, new ResponseCallback<KakaoLinkResponse>() {
                            @Override
                            public void onFailure(ErrorResult errorResult) {
                                Log.e("DEBUG_JS", String.format("[KakaoLinkService.send] %s", errorResult.toString()));
                            }

                            @Override
                            public void onSuccess(KakaoLinkResponse result) {

                            }
                        }
                );
            }
        });

    }

    public static void sendKakaoReceipt(final Context context, Bitmap bitmap) {
        final File imageFile = toImageFile(bitmap);
        if (imageFile.getPath().isEmpty()) {
            MyUI.toastSHORT(context, String.format("이미지 변환을 하지 못했습니다."));
            return;
        }
        sendKakaoImage(context, imageFile, "영수증을 보내드립니다", "더 알아보기", "https://wp.me/P8qpsT-uZ");
    }

    // 이미지는 20일간만 유효
    public static void sendKakaoSlip(final Context context, SlipFullRecord slipFullRecord, Bitmap bitmap, final String linkUrl) {
        final File imageFile = toImageFile(bitmap);
        if (imageFile.getPath().isEmpty()) {
            MyUI.toastSHORT(context, String.format("이미지 변환을 하지 못했습니다."));
            return;
        }

        boolean isImageLink = (linkUrl.equals(BaseGlobal.SHOPL_INSTALL_GUIDE) == false);
        String rightButtonTitle = isImageLink? "상품사진 보기" : "앱으로 보기";

        if (BaseUtils.isValidPhoneNumber(slipFullRecord.record.counterpartPhoneNumber) || isImageLink) {
            sendKakaoImage(context, imageFile, "금일 거래 내역을 보내드립니다.", rightButtonTitle, linkUrl);
        } else {
            sendKakaoImage(context, imageFile, "금일 거래 내역을 보내드립니다.", "", "");
        }
    }

    public static void sendKakaoAccount(final Context context, Bitmap bitmap) {
        final File imageFile = toImageFile(bitmap);
        if (imageFile.getPath().isEmpty()) {
            MyUI.toastSHORT(context, String.format("이미지 변환을 하지 못했습니다."));
            return;
        }
        sendKakaoImage(context, imageFile, "금일 입금 내역을 보내드립니다\n(유효 20일, 이후는 샵플미송에서)", "더 알아보기", BaseGlobal.SHOPL_US);
    }

    public static boolean hasKakao(final Context context) {
        try {
            Context kakaoContext = context.createPackageContext(KakaoPackage, Context.CONTEXT_IGNORE_SECURITY);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            MyUI.toastSHORT(context, String.format("카카오톡이 설치되어 있지 않습니다"));
            return false;
        }
    }

    public static void sendKakao(final Context context, final Bitmap bitmap) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                try {
                    Context kakaoContext = context.createPackageContext(KakaoPackage, Context.CONTEXT_IGNORE_SECURITY);
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_STREAM, MyGraphics.toUri(context, bitmap));
                    intent.setPackage(BaseGlobal.KakaoPackage);
                    context.startActivity(intent);
                } catch (PackageManager.NameNotFoundException e) {
                    MyUI.toastSHORT(context, String.format("카카오톡이 설치되어 있지 않습니다"));
                }
            }
        };
    }

    public static void sendKakao(final Context context, final String PROVIDER, final String filePath) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                try {
                    Context kakaoContext = context.createPackageContext(KakaoPackage, Context.CONTEXT_IGNORE_SECURITY);
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    if (filePath.contains(".pdf")) {
                        intent.setType("application/pdf");
                        //   intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(filePath)));
                    } else if (filePath.contains(".txt") || filePath.contains(".csv")) { // 그냥 "text" 타입으로 전달하면 kakao로 전달이 안됨
                        intent.setType("application/vnd.ms-excel");
                    }
                    Uri uri = FileProvider.getUriForFile(context, PROVIDER, new File(filePath)); // 새로운 보안 정책 때문에
                    intent.putExtra(Intent.EXTRA_STREAM, uri); // Uri.fromFile(new File(filePath)));
                    intent.setPackage(KakaoPackage);
                    context.startActivity(intent);
                } catch (PackageManager.NameNotFoundException e) {
                    MyUI.toastSHORT(context, String.format("카카오톡이 설치되어 있지 않습니다"));
                }
            }
        };
    }

    public static void shareImageFile(Context context, String filePath) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/jpeg");
        Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", new File(filePath));
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        context.startActivity(Intent.createChooser(intent, "Share Image"));
    }

    public static void shareFile(final Context context, final String filePath) {
        String path = filePath;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(getFileType(path));
        Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", new File(filePath));
        intent.putExtra(Intent.EXTRA_STREAM, uri); // Uri.parse(path));
        context.startActivity(Intent.createChooser(intent, "Share File"));
    }

    public static void shareText(final Context context, String title, final String message) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        context.startActivity(Intent.createChooser(intent, "안내보내기"));
    }

    private static String getFileType(String filePath) {
        // "application/msword","application/vnd.ms-powerpoint","
        String result = "text/plain";
        if (filePath.contains(".") == false)
            return result;
        String extension = filePath.substring(filePath.lastIndexOf(".")).replace(".", "");
        if (BaseUtils.toList("xls", "xlsx", "csv").contains(extension)) {
            return "application/vnd.ms-excel|application/x-msexcel|application/excel|application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        } else if (BaseUtils.toList("jpg", "jpeg", "png").contains(extension)) {
            return "image/jpeg";
        } else if (BaseUtils.toList("pdf").contains(extension)) {
            return "application/pdf";
        } else if (BaseUtils.toList("zip").contains(extension)) {
            return "application/zip";
        } else {
            return result;
        }
    }

    public static void sendInstagram(final Context context, final Bitmap bitmap) {
        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo("com.instagram.android", 0);
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, MyGraphics.toUri(context, bitmap));
            intent.setPackage("com.instagram.android");
            context.startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            MyUI.toastSHORT(context, String.format("인스타그램이 설치되어 있지 않습니다"));
        }
    }

    public static void sendWechat(final Context context, final Bitmap bitmap) {
        new MyAsyncTask(context, "sendWechat", "위챗 실행중...") {
            @Override
            public void doInBackground() {
                try {
                    Context wechatContext = context.createPackageContext("com.tencent.mm", Context.CONTEXT_IGNORE_SECURITY);
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_STREAM, MyGraphics.toUri(context, bitmap));
                    intent.setPackage("com.tencent.mm");
                    context.startActivity(intent);
                } catch (PackageManager.NameNotFoundException e) {
                    MyUI.toastSHORT(context, String.format("위챗이 설치되어 있지 않습니다"));
                }
            }
        };
    }

    public static void sendWechat(final Context context, final String title, final String message) {
        new MyAsyncTask(context, "sendWechat", "위챗 실행중...") {
            @Override
            public void doInBackground() {
                try {
                    Context wechatContext = context.createPackageContext("com.tencent.mm", Context.CONTEXT_IGNORE_SECURITY);
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, title);
                    intent.putExtra(Intent.EXTRA_TEXT, message);
                    intent.setPackage("com.tencent.mm");
                    context.startActivity(intent);
                } catch (PackageManager.NameNotFoundException e) {
                    MyUI.toastSHORT(context, String.format("위챗이 설치되어 있지 않습니다"));
                }
            }
        };
    }

    public static void openAppSetting(final Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        ((Activity) context).startActivityForResult(intent, BaseGlobal.RequestPermission);
    }

    public static void openBluetooth(Context context) {
        Intent intent = new Intent();
        intent.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
        context.startActivity(intent);
    }

    public static List<BluetoothDevice> getBluetoothDevices() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            return new ArrayList<BluetoothDevice>(0);
        } else {
            return new ArrayList(bluetoothAdapter.getBondedDevices());
        }
    }

    public static boolean isBluetoothEnabled() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            return false;
        } else {
            return bluetoothAdapter.isEnabled();
        }
    }

    public static void openPlayStore(Context context, String packageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    public static boolean hasPackage(Context context, String packageName) {
        try {
            Context otherContext = context.createPackageContext(packageName, Context.CONTEXT_IGNORE_SECURITY);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String getPermissionString(Context context) {
        List<String> results = new ArrayList<>();
        if (MyDevice.hasPermission(context, UserPermission.CAMERA))
            results.add("카메라");
        if (MyDevice.hasPermission(context, UserPermission.READ_CONTACTS))
            results.add("주소록");
        return TextUtils.join(", ", results);
    }

    public static void activateCamera(Context context, String saveFilePath, String PROVIDER, int REQUEST_KEY) {
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.CAMERA) == false) {
            MyUI.toastSHORT(context, String.format("카메라 권한이 없습니다"));
            MyDevice.requestPermission(context, MyDevice.UserPermission.CAMERA);
            return;
        }
        Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri uri = FileProvider.getUriForFile(context, PROVIDER, new File(saveFilePath));
        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        ((Activity) context).startActivityForResult(imageIntent, REQUEST_KEY);
    }

    public static String getAppLabel(Context context) {
        try {
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName().toString(), 0);
            return context.getPackageManager().getApplicationLabel(ai).toString();
        } catch (final PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    public static boolean isOnline() {
        CheckConnect cc = new CheckConnect(CONNECTION_CONFIRM_CLIENT_URL);
        cc.start();
        try {
            cc.join();
            return cc.isSuccess();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static class CheckConnect extends Thread {
        private boolean success;
        private String host;
        private int timeOutMs = 5000;
        // url이 유효하지 않으면 2.3초 정도 소요
        // url이 유효해도 dns 설정이 잘못이면 15초 정도 소요
        // timeout은 웹 페이지 응답 시간만 조절한다
        //http://nomore7.tistory.com/entry/HttpURLConnection-setTimeout-%EC%A0%81%EC%9A%A9-%EC%95%88%EB%90%A0%EB%95%8C-%EC%95%88%EB%90%9C%EB%8B%A4%EA%B3%A0-%EC%B0%A9%EA%B0%81-%ED%95%A0-%EB%95%8C

        public CheckConnect(String host) {
            this.host = host;
        }

        @Override
        public void run() {
            HttpURLConnection conn = null;
            try {
                conn = (HttpURLConnection) new URL(host).openConnection();
                conn.setRequestProperty("User-Agent", "Android");
                conn.setConnectTimeout(timeOutMs);
                conn.connect();
                int responseCode = conn.getResponseCode();
                if (responseCode == 204) success = true;
                else success = false;
            } catch (Exception e) {
                e.printStackTrace();
                success = false;
            }
            if (conn != null) {
                conn.disconnect();
            }
        }

        public boolean isSuccess() {
            return success;
        }
    }

    public static boolean isKakaoInstalled(Context context) {
        try {
            Context kakaoContext = context.createPackageContext(BaseGlobal.KakaoPackage, Context.CONTEXT_IGNORE_SECURITY);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean isMobileDataEnabled(Context context) {
        boolean result = false; // Assume disabled
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            // get the setting for "mobile data"
            result = (Boolean) method.invoke(cm);
        } catch (Exception e) {
            result = false;
            // TODO do whatever error handling you want here
        }
        return result;
    }

    public static String getModelName() {
        return Build.MODEL;
    }

    public static void showMemoryUsage(Context context, String location) {
        Pair<Integer, Integer> ramUsage = MyDevice.getRamUsageMB(context);
        Pair<Integer, Integer> sdUsage = MyDevice.getSdUsageMB();
        Runtime runtime = Runtime.getRuntime();  // unit byte
        Log.i("DEBUG_JS", String.format("[%s] Ram %d/%d MB (%d per), SD %.1f/%.1f GB (%d per), " +
                        "Heap %d/%d MB",
                location, ramUsage.first, ramUsage.second, ramUsage.first * 100 / ramUsage.second,
                sdUsage.first / 1000.0, sdUsage.second / 1000.0, sdUsage.first * 100 / sdUsage.second,
                runtime.freeMemory() / 0x100000, runtime.maxMemory() / 0x100000
        ));
    }

    public static Pair<Integer, Integer> getRamUsageMB(Context context) {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        return Pair.create((int) mi.availMem / 0x100000, (int) mi.totalMem / 0x100000);
    }

    public static Pair<Integer, Integer> getSdUsageMB() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return Pair.create((int) (stat.getAvailableBytes() / 0x100000), (int) (stat.getTotalBytes() / 0x100000));
    }

    public static void insertContact(Context context, String name, String phoneNumber, int REQUEST_KEY) {
        if (hasPermission(context, UserPermission.READ_CONTACTS) == false) {
            MyUI.toastSHORT(context, String.format("주소록 권한이 필요합니다"));
            requestPermission(context, UserPermission.READ_CONTACTS);
            return;
        }
        Intent intent = new Intent(Intent.ACTION_INSERT, ContactsContract.Contacts.CONTENT_URI);
        if (name.isEmpty() == false) {
            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
            intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
        }

        if (phoneNumber.isEmpty() == false) {
            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, phoneNumber);
        }
        intent.putExtra("finishActivityOnSaveCompleted", true);
        ((Activity) context).startActivityForResult(intent, REQUEST_KEY);
    }

    public static void modifyContact(Context context, String contactId, int REQUEST_KEY) {
        if (hasPermission(context, UserPermission.READ_CONTACTS) == false) {
            MyUI.toastSHORT(context, String.format("주소록 권한이 없습니다"));
            requestPermission(context, UserPermission.READ_CONTACTS);
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, contactId);
        intent.setData(uri);
        intent.putExtra("finishActivityOnSaveCompleted", true);
        ((Activity) context).startActivityForResult(intent, REQUEST_KEY);
        modifiedContactId = contactId;
    }

    public static UserRecord getUserRecord(Context context, String contactId) {
        UserRecord result = new UserRecord();
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, contactId);
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            Log.e("DEBUG_JS", String.format("[MyDevice.getUserRecord] data == null"));
            return result;
        }

        try {
            if (cursor.moveToFirst()) {
                result = MyDevice.getUserRecord(context, cursor);
            }
        } finally {
            cursor.close();
        }
        return result;
    }

    public static String getModifiedContactId() {
        return modifiedContactId;
    }

    public static UserRecord getUserRecord(Context context, final Intent data) {
        UserRecord result = new UserRecord();
        if (data == null) {
            Log.e("DEBUG_JS", String.format("[MyDevice.getUserRecord] data == null"));
            return result;
        }
        Cursor cursor = context.getContentResolver().query(data.getData(), null, null, null, null);
        if (cursor == null) return result;
        try {
            if (cursor.moveToFirst()) {
                result = MyDevice.getUserRecord(context, cursor);
            }
        } finally {
            cursor.close();
        }
        return result;
    }

    public static void addContactDirectly(Context context, String displayName, String phoneNumber) {
        if (hasPermission(context, UserPermission.READ_CONTACTS) == false) {
            requestPermission(context, UserPermission.READ_CONTACTS);
            MyUI.toastSHORT(context, String.format("구글 주소록에도 저장을 하고자 합니다"));
            return;
        }

        if (hasPermission(context, UserPermission.WRITE_CONTACTS) == false) {
            requestPermission(context, UserPermission.WRITE_CONTACTS);
            MyUI.toastSHORT(context, String.format("저장권한이 필요합니다"));
            return;
        }

        ArrayList<ContentProviderOperation> operationList = new ArrayList<ContentProviderOperation>();
        operationList.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        operationList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Nickname.NAME, displayName)
                .build());

        operationList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .build());
        try {
            ContentProviderResult[] results = context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, operationList);
            Log.w("DEBUG_JS", String.format("[MyDevice.addContactDirectly] %s, %s", displayName, phoneNumber));
        } catch (Exception e) {
            Log.e("DEBUG_JS", String.format("[MyDevice.addContactDirectly] %s", e.getLocalizedMessage()));
        }
    }

    public static int getWidth(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        if (isLandscapeScreen(context)) {
            return Math.max(dm.widthPixels, dm.heightPixels);
        } else {
            return dm.widthPixels;
        }
    }

    public static int getWidthDP(Context context) {
        return toDP(context, getWidth(context));
    }

    public static boolean isLandscapeScreen(Context context) {
        final int screenOrientation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (screenOrientation) {
            // portrait
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                return false;
            // landscape
            case Surface.ROTATION_90:
            default:
                return true;
        }
    }

    public static int getHeight(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        if (isLandscapeScreen(context)) {
            return Math.min(dm.widthPixels, dm.heightPixels);
        } else {
            return dm.heightPixels;
        }
    }

    public static int getHeightDP(Context context) {
        return toDP(context, getHeight(context));
    }


    static class SentBoardcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(context, "전송 했습니다", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(context, "전송하지 못했습니다", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(context, "서비스 지역이 아닙니다", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(context, "네트워크가 연결되어 있지 않습니다", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(context, "PDU Null", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    //static class

    public static void sendMMS(Context context, String phoneNumber, String message) {

    }


    public static void sendDM(Context context, String phoneNumber) {
        PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_SENT_ACTION"), 0);
        short destinationPort = 80;
        //byte[] data = ItemDB.getInstance(context).getImageRecord(1).image;
        byte[] data = "�스�입�다".getBytes();
        SmsManager.getDefault().sendDataMessage(phoneNumber, null, destinationPort, data, sentIntent, null);
        Log.e("DEBUG_JS", String.format("[%s] sending...", MyDevice.class.getSimpleName()));
    }


    public static boolean hasPhoneNumber(Context context) {
        return !Empty.isEmpty(MyDevice.getMyPhoneNumber(context));
    }

    public enum UserPermission {CAMERA, PHONE, WRITE_STORAGE, READ_STORAGE, READ_CONTACTS, WRITE_CONTACTS, CALL_PHONE}

    public static boolean hasPermission(final Context context, final UserPermission userPermission) {
        boolean result = false;
        switch (userPermission) {
            case PHONE:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED);
                break;
            case CAMERA:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
                break;
            case WRITE_STORAGE:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
                break;
            case READ_STORAGE:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
                break;
            case READ_CONTACTS:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);
                break;
            case WRITE_CONTACTS:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED);
                break;
            case CALL_PHONE:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED);
                break;
        }
        // Log.i("DEBUG_JS", String.format("[MyDevice.hasPermission] %s : %s ", userPermission.toDbString(), Boolean.toDbString(result)));
        return result;
    }

    public static void explainAndRequestPermission(final Context context, final UserPermission userPermission) {
        switch (userPermission) {
            case PHONE:
                new MyAlertDialog(context, "기기 전화번호 확인", "미확인 이용자의 부정 사용을 방지하기 위해 기기 전화 번호를 확인하고자 합니다.\n\n" +
                        "동의하시면 다음 창에서 [허용]을 눌러주세요", "권한 요청 이유를 확인했습니다", null) {
                    @Override
                    public void yes() {
                        requestPermission(context, userPermission);
                    }
                };
                break;
            case WRITE_STORAGE:
                new MyAlertDialog(context, "파일 저장공간 사용", "앱 관련 파일을 저장할 폴더를 생성하고자 합니다.\n\n" +
                        "동의하시면 다음 창에서 [허용]을 눌러주세요", "권한 요청 이유를 확인했습니다", null) {
                    @Override
                    public void yes() {
                        requestPermission(context, userPermission);
                    }
                };
                break;
            case READ_STORAGE:
                new MyAlertDialog(context, "이미지 업로드 허용", "이미지를 업로드 하기 위해 읽어오려합니다.\n\n" +
                        "동의하시면 다음 창에서 [허용]을 눌러주세요", "권한 요청 이유를 확인했습니다", null) {
                    @Override
                    public void yes() {
                        requestPermission(context, userPermission);
                    }
                };
                break;
            case CAMERA:
                new MyAlertDialog(context, "카메라 사용", "이미지를 찍기 위해서 카메라를 사용하고자 합니다.\n\n" +
                        "동의하시면 다음 창에서 [허용]을 눌러주세요", "권한 요청 이유를 확인했습니다", null) {
                    @Override
                    public void yes() {
                        requestPermission(context, userPermission);
                    }
                };
                break;
//            case SMS:
//                new MyAlertDialog(context, "SMS 전송 허가", "문자를 보내시려면 다음 창에서 [허용]을 눌러주세요",
//                        "권한 요청 이유를 확인했습니다", null) {
//                    @Override
//                    public void yes() {
//                        requestPermission(context, userPermission);
//                    }
//                };
//                break;
        }
        return;
    }

    public static String[] getAllPermissionStr() {
        return new String[]{Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.CALL_PHONE
        };
    }

    public static List<UserPermission> getAllPermissions() {
        List<UserPermission> results = new ArrayList<>();
        results.add(UserPermission.PHONE);
        results.add(UserPermission.WRITE_STORAGE);
        results.add(UserPermission.CAMERA);
        results.add(UserPermission.READ_CONTACTS);
        results.add(UserPermission.CALL_PHONE);
        return results;
    }

    public static boolean hasAllPermissions(Context context) {
        for (UserPermission userPermission : getAllPermissions()) {
            if (userPermission == UserPermission.CALL_PHONE)
                continue; // 전화기가 없는 경우 (태블릿, 시뮬레이터) 문제 발생
            if (hasPermission(context, userPermission) == false) {
                Log.e("DEBUG_JS", String.format("[MyDevice.hasAllPermissions] %s not allowed", userPermission.toString()));
                return false;
            }
        }
        return true;
    }

    public static void requestPermission(final Context context, final UserPermission userPermission) {
        switch (userPermission) {
            case PHONE:
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, userPermission.ordinal());
                break;
            case WRITE_STORAGE:
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, userPermission.ordinal());
                break;
            case READ_STORAGE:
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, userPermission.ordinal());
                break;
            case CAMERA:
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, userPermission.ordinal());
                break;
//            case SMS:
//                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.SEND_SMS}, userPermission.ordinal());
//                break;
            case READ_CONTACTS:
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_CONTACTS}, userPermission.ordinal());
                break;

            case WRITE_CONTACTS:
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_CONTACTS}, userPermission.ordinal());
                break;
            case CALL_PHONE:
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, userPermission.ordinal());
                break;
        }
        return;
    }

    public static String getMyPhoneNumber(Context context) {
        try {
            if (hasPermission(context, UserPermission.CALL_PHONE) == false) {
                MyUI.toastSHORT(context, String.format("전화번호 가져오기 권한이 없습니다"));
                return "";
            }
            if (context == null)
                return "";
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager == null)
                return "";
            String phoneNumber = manager.getLine1Number();
            if (phoneNumber == null) {
                return "";
            } else {
                return BaseUtils.toSimpleForm(phoneNumber);
            }
        } catch (Exception e) {
            return "";
        }
    }

    public static void createFolders(Context context, String folderPath) {
        folderPath = folderPath + "/readme.txt";
        createFolderIfNotExist(new File(folderPath));
        refresh(context, new File(folderPath));
    }

    public static List<String> getCameraImages(Context context) {
        String[] projection = {MediaStore.Images.Media.DATA};
        String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
        String[] selectionArgs = {cameraImageBucketId};
        String orderBy = null; //MediaStore.Images.Media._ID;
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection, selection, selectionArgs, orderBy);
        ArrayList<String> result = new ArrayList<String>(); //xperiaCameraDir
        if (cursor.moveToFirst()) {
            int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                String data = cursor.getString(dataColumn);
                result.add(data);
            } while (cursor.moveToNext());
        }

        String[] selectionArgsXperia = {xperiaCameraImageBucketId};
        cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection, selection, selectionArgsXperia, orderBy);
        if (cursor.moveToFirst()) {
            int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                String data = cursor.getString(dataColumn);
                result.add(data);
            } while (cursor.moveToNext());
        }

        String[] selectionArgsHTC = {htcCameraImageBucketId};
        cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection, selection, selectionArgsHTC, orderBy);
        if (cursor.moveToFirst()) {
            int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                String data = cursor.getString(dataColumn);
                result.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return result;
    }

    public static ArrayList<String> getDirImages(Context context, String buckedId) {
        String[] projection = {MediaStore.Images.Media.DATA};
        String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
        String[] selectionArgs = {buckedId};
        String orderBy = null; //MediaStore.Images.Media._ID;
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection, selection, selectionArgs, orderBy);
        ArrayList<String> result = new ArrayList<String>(); //xperiaCameraDir
        if (cursor.moveToFirst()) {
            int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                String data = cursor.getString(dataColumn);
                result.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    public static ArrayList<String> getAllImages(Context context, int Limit) {
        Uri images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String sortingOption = MediaStore.Images.Media.DATE_ADDED;
        String[] projection = {MediaStore.Images.Media._ID, sortingOption,
                MediaStore.Images.Media.DATA};
        String orderBy = String.format("%s DESC LIMIT %d", sortingOption, Limit);
        Cursor cursor = context.getContentResolver().query(images, projection, null, null, orderBy);
        ArrayList<String> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                String filePath = cursor.getString(dataColumn);

                //     Log.i("DEBUG_JS", String.format("[MyDevice.getAllImages] %s", filePath));
                result.add(filePath);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    public static void getGalleryImage(Context context, String title, int code) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ((Activity) context).startActivityForResult(Intent.createChooser(intent, title), code);
    }

    public static String getBucketId(String path) {
        return String.valueOf(path.toLowerCase().hashCode());
    }

    public static String getPath(Context context, Uri uri, boolean galleryOnly) {
        if (uri == null) {
            Log.e("DEBUG_JS", String.format("[BaseUtils.getPath] uri = null"));
            return "";
        }

        Cursor cursor = null;
        int column_index = 0;
        try {
            if (galleryOnly) {
                String[] projection = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                cursor = context.getContentResolver().query(uri, null, null, null, null);
                cursor.moveToNext();
                column_index = cursor.getColumnIndex("_data");
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    public static void createFolderIfNotExist(File folder) {
        try {
            if (folder.exists() == false) {
                boolean success = folder.mkdirs();
                Thread.sleep(1);   // to avoid an error: open failed: EBUSY (Device or resource busy)
                if (success) {
                    Log.i("DEBUG_JS", String.format("[%s.createFolderIfNotExist] %s is created", MyDevice.class.getSimpleName(), folder.toString()));
                } else {
                    Log.e("DEBUG_JS", String.format("[%s.createFolderIfNotExist] %s is not created", MyDevice.class.getSimpleName(), folder.toString()));
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static boolean googleConnected(String packageName) {
        // check StrictMode.ThreadPolicy policy
        HttpURLConnection connection = null;
        try {
            URL mUrl = new URL("https://play.google.com/store/apps/details?id=" + packageName);
            connection = (HttpURLConnection) mUrl.openConnection();
            if (connection == null) {
                Log.e("DEBUG_JS", String.format("[%s.getMarketVersion] connection = null", MyDevice.class.getSimpleName()));
                return false;
            }

            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11");
            connection.setConnectTimeout(5000);
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            int code = connection.getResponseCode();
            connection.disconnect();

            if (code != HttpURLConnection.HTTP_OK)
                Log.e("DEBUG_JS", String.format("[%s] code = %d", MyDevice.class.getSimpleName(), code));
            return (code == HttpURLConnection.HTTP_OK);
        } catch (Exception ex) {
            Log.e("DEBUG_JS", String.format("[%s.getMarketVersion] google sends no response", MyDevice.class.getSimpleName()));
            return false;
        }
    }

    public static String getAppVersionName(Context context) {
        PackageInfo info = null;
        try {
            info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[MyDevice.getAppVersionName] %s", e.getMessage()));
        }
        return info.versionName;
    }

    public static int getAndroidVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static int getAppVersionCode(Context context) {
        PackageInfo info = null;
        try {
            info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[MyDevice.getAppVersionCode] %s", e.getMessage()));
        }
        return info.versionCode;
    }

    public static String getAndroidId(Context context) {
        String[] params = {ID_KEY};
        Cursor c = context.getContentResolver().query(URI, null, null, params, null);
        if (!c.moveToFirst() || c.getColumnCount() < 2)
            return "";

        try {
            Log.i("DEBUG_JS", "getAndroidId() : " + Long.toHexString(Long.parseLong(c.getString(1))));
            return Long.toHexString(Long.parseLong(c.getString(1)));
        } catch (NumberFormatException e) {
            return "";
        }
    }

    public static boolean hasFreeMemoryForImage() {
        return (getFreeStorageMB() >= 3);
    }

    public static long getFreeStorageMB() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((long) stat.getBlockSize() * (long) stat.getAvailableBlocks()) / 1048576;
        // stat.getAvailableBlocksLong() stat.getAvailableBytes()
    }

    public static Camera.Size getCameraPixels() {
        Camera camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        final Camera.Parameters params = camera.getParameters();
        Camera.Size size = params.getPictureSize();
        //Log.i("DEBUG_JS",String.format("size = %d x %d", size.width, size.height));
        camera.release();
        return size;
    }

    public static long getAvailableRamMB(Context context) {
        ActivityManager actManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        return memInfo.availMem / 1048576;
    }

    public static void showKeyboard(final Context context, final View view) {
        showKeyboard(context, view, 300);
    }

    public static void showKeyboard(final Context context, final View view, int delay) {
        new MyDelayTask(context, delay) {
            @Override
            public void onFinish() {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (MyDevice.getAndroidVersion() >= 28) {
                    view.requestFocus();
                    imm.toggleSoftInputFromWindow(view.getWindowToken(), 0, 0);
                } else {
                    imm.showSoftInput(view, 0);
                }
            }
        };
    }

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static String getRegId(Context context) {
        String regId = PreferenceManager.getDefaultSharedPreferences(context).getString(BaseGlobal.GcmToken, "");
        if (regId.isEmpty())
            Log.e("DEBUG_JS", String.format("[MyDevice.getRegId] regId = empty"));
        return regId;
    }

    public static boolean hasSDCard() {
        return (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED));
    }

    public static void refresh(Context context, File file) {
        new MediaUpdater(context, file, null).refresh();
    }

    public static void refresh(Context context, File file, MyOnTask onFinishEvent) {
        if (MyDevice.hasPermission(context, UserPermission.WRITE_STORAGE)) {
            new MediaUpdater(context, file, onFinishEvent).refresh();
        }
    }

    public static boolean googleConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            try {
                URL url = new URL("http://www.google.com/");
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setRequestProperty("User-Agent", "test");
                urlc.setRequestProperty("Connection", "reset");
                urlc.setConnectTimeout(1000); // mTimeout is in seconds
                urlc.connect();
                if (urlc.getResponseCode() == 200) {
                    return true;
                } else {
                    return false;
                }
            } catch (IOException e) {
                Log.i("warning", "Error checking internet connection", e);
                return false;
            }
        } else {
            if (activeNetwork == null) {
                Log.i("DEBUG_JS", String.format("[%s] activeNetwork = null", MyDevice.class.getSimpleName()));
            } else if (activeNetwork.isConnected() == false) {
                Log.i("DEBUG_JS", String.format("[%s] activeNetwork.isConnected = false", MyDevice.class.getSimpleName()));
            }

        }
        return false;
    }

    public static boolean networkConnected(Context context) {
        return (wifiConnected(context) || mobileConnected(context));
    }

    public static boolean wifiConnected(Context context) {
        ConnectivityManager connect = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connect.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
    }

    public static boolean mobileConnected(Context context) {
        ConnectivityManager connect = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connect.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
    }

    public static boolean isNetworkUseEnabled(Context context) {
        LocationManager myLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return myLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static boolean isGPSUseEnabled(Context context) {
        LocationManager myLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return myLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }

    public static int toPixel(Context context, int dp) {
        if (dp < 0)
            return dp;
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int) px;
    }

    public static int toDP(Context context, int px) {
        if (px <= 0)
            return px;
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return (int) dp;
    }

    private static float toSP(Context context, float px) {
        if (px <= 0)
            return 0;
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float toOriginalSP(Context context, float px) {
        float scale = MyDevice.getFontScale(context);
        float sp = toSP(context, px);
        if (scale == 0)
            return sp;
        else
            return sp / scale;
    }

    @Deprecated
    private static void limitFontScale(Context context, float scaleLimit) {
        Configuration configuration = context.getResources().getConfiguration();
        if (configuration.fontScale > scaleLimit)
            configuration.fontScale = scaleLimit;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        context.getResources().updateConfiguration(configuration, metrics);
    }

    public static void setFontScale(Context context, float scale) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.fontScale = scale;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        context.getResources().updateConfiguration(configuration, metrics);
    }

    public static int getXInDpi(Context context) {
        return (int) context.getResources().getDisplayMetrics().xdpi;
    }

    public static int getYInDpi(Context context) {
        return (int) context.getResources().getDisplayMetrics().ydpi;
    }

    public static int getXInPx(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getYInPx(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static int getXInDp(Context context) {
        return (int) (getXInPx(context) / getDensity(context));
    }

    public static int getYInDp(Context context) {
        return (int) (getYInPx(context) / getDensity(context));
    }

    public static float getDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static int getDensityDpi(Context context) {
        return context.getResources().getDisplayMetrics().densityDpi;
    }


    // Hap<phoneNumber, displayName>
    public static HashMap<String, String> getMyAddressBook(Context context, MyOnAsync<UserRecord> rowCallback) {
        if (MyDevice.hasPermission(context, UserPermission.READ_CONTACTS) == false) {
            Log.w("DEBUG_JS", String.format("[MyDevice.getMyAddressBook] hasPermission: READ_CONTACTS == false"));
            return new HashMap<String, String>(0);
        }

        HashMap<String, String> result = new HashMap<>();
        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.HAS_PHONE_NUMBER};
        String[] selectionArgs = null;
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

        Cursor cursor = context.getContentResolver().query(uri, projection, null, selectionArgs, sortOrder);
        if (cursor == null) return result;
        if (cursor.getCount() == 0) {
            cursor.close();
            return result;
        }

        int idColumn = cursor.getColumnIndex(ContactsContract.Contacts._ID);
        int nameColumn = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int hasNumber = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);

        while (cursor.moveToNext()) {
            if (BaseUtils.toBoolean(cursor.getInt(hasNumber))) {
                List<Pair<Integer, String>> typeAndNumbers = getPhoneNumbers(context, cursor.getString(idColumn));
                String mainNumber = getMainPhoneNumber(typeAndNumbers);
                result.put(mainNumber, cursor.getString(nameColumn)); // <phoneNumber, displayName>
                String address = getAddress(context, cursor.getString(idColumn));

                UserRecord userRecord = new UserRecord();
                userRecord.phoneNumber = mainNumber;
                userRecord.name = cursor.getString(nameColumn);
                userRecord.location = address;
                //    result.username = cursor.getString(idColumn);

                // addressbookDB
                if (rowCallback != null)
                    rowCallback.onSuccess(userRecord);
                //  Log.i("DEBUG_JS", String.format("[MyDevice.getMyAddressBook] %s - %s", cursor.getString(nameColumn), mainNumber));
            }
        }

        cursor.close();
        return result;
    }

    public static UserRecord getUserRecord(Context context, Cursor cursor) {
        UserRecord result = new UserRecord();
        int idColumn = cursor.getColumnIndex(ContactsContract.Contacts._ID);
        int nameColumn = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int hasNumber = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
        if (BaseUtils.toBoolean(cursor.getInt(hasNumber))) {
            List<Pair<Integer, String>> typeAndNumbers = getPhoneNumbers(context, cursor.getString(idColumn));
            String address = getAddress(context, cursor.getString(idColumn));
            //   result.username = cursor.getString(idColumn);
            result.phoneNumber = getMainPhoneNumber(typeAndNumbers);
            result.name = cursor.getString(nameColumn);
            result.location = address;

        }
        return result;
    }

    private static String getMainPhoneNumber(List<Pair<Integer, String>> typeAndNumbers) {
        String result = "";
        if (typeAndNumbers.size() <= 0)
            return result;
        else if (typeAndNumbers.size() == 1)
            return typeAndNumbers.get(0).second;

        for (Pair<Integer, String> typeAndNumber : typeAndNumbers) {
            if (typeAndNumber.first == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                if (BaseUtils.isValidPhoneNumber(typeAndNumber.second))
                    return typeAndNumber.second;    // mobile + 01*-****-****  형식
        }

        // mobile + 핸드폰 형식이 없을때
        for (Pair<Integer, String> typeAndNumber : typeAndNumbers) {
            if (BaseUtils.isValidPhoneNumber(typeAndNumber.second))
                return typeAndNumber.second;    // mobile + 01*-****-****  형식
        }

        // 핸드폰 형식이 없을때
        return typeAndNumbers.get(0).second;
    }

    private static List<Pair<Integer, String>> getPhoneNumbers(Context context, String id) {
        List<Pair<Integer, String>> results = new ArrayList<>();
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.NUMBER};
        String selection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?";
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, new String[]{id}, null);
        if (cursor == null) return results;
        if (cursor.getCount() == 0) {
            cursor.close();
            return results;
        }
        int typeColumn = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
        int numberColumn = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        while (cursor.moveToNext()) {
            String phoneNumber = cursor.getString(numberColumn);
            phoneNumber = BaseUtils.toSimpleForm(phoneNumber);
            // Log.i("DEBUG_JS", String.format("[MyDevice.getPhoneNumbers] Type %d, number %s", cursor.getInt(typeColumn), phoneNumber));
            results.add(Pair.create(cursor.getInt(typeColumn), phoneNumber));
        }
        cursor.close();
        return results;
    }

    private static String getAddress(Context context, String id) {
        String result = "";
        Uri uri = ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI;
        String selection = ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID + "=?";
        Cursor cursor = context.getContentResolver().query(uri, null, selection, new String[]{id}, null);
        if (cursor == null) return result;
        if (cursor.getCount() == 0) {
            cursor.close();
            return result;
        }
        int streetColumn = cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET);
        int cityColumn = cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY);
        int countryColumn = cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY);
        while (cursor.moveToNext()) {
            result = String.format("%s %s %s", cursor.getString(countryColumn), cursor.getString(cityColumn), cursor.getString(streetColumn));
            result = result.replace("null", "").trim();
        }
        cursor.close();
        return result;
    }

    public static String getFileNameOnly(String filePath, boolean doRemoveExt) {
        File file = new File(filePath);
        if (file.exists()) {
            String fileName = file.toString().substring(file.toString().lastIndexOf("/") + 1);
            if (doRemoveExt) {
                return fileName.substring(0, fileName.lastIndexOf("."));
            } else {
                return fileName;
            }
        } else {
            Log.e("DEBUG_JS", String.format("[MyDevice.getFilenameOnly] %s not exist", filePath));
            return "";
        }
    }

    public static String createTempImageFilePath() {
        return createTempFilePath(BaseAppWatcher.appDir, "jpg");
    }

    public static String getTempTextFilePath() {
        return createTempFilePath(BaseAppWatcher.appDir, "txt");
    }

    public static File openOrCreate(String filePath) {
        File file = new File(filePath);
        if (file.exists() == false) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                Log.e("DEBUG_JS", String.format("[MyDevice.openOrCreate] %s", e.getLocalizedMessage()));
            }
        }
        return file;
    }

    public static String createTempFilePath(String dirPath, String ext) {
        File dir = new File(dirPath);
        if (dir.exists() == false) {
            dir.mkdirs();
            Log.e("DEBUG_JS", String.format("[MyDevice.createTempFilePath] %s not exists.. newly created", dirPath));
        }
        return String.format("%s/%s%s.%s", dirPath, BaseGlobal.tempHeader, DateTime.now().toString(BaseUtils.File_Format), ext.toLowerCase());
    }

    public static File createImageFile() {
        return new File(createCurrentTimeFilePath(BaseAppWatcher.appDir, "camera", "jpg"));
    }


    public static String createCurrentTimeFilePath(String dirPath, String prefix, String ext) {
        File dir = new File(dirPath);
        if (dir.exists() == false) {
            dir.mkdirs();
            Log.e("DEBUG_JS", String.format("[MyDevice.getSaveFilePath] %s not exists.. newly created", dirPath));
        }
        return String.format("%s/%s_%s.%s", dirPath, prefix, DateTime.now().toString(BaseUtils.File_Format), ext.toLowerCase());
    }

    public static void deleteFiles(Context context, String... filePaths) {
        List<String> results = new ArrayList<>();
        for (String path : filePaths)
            results.add(path);
        deleteFiles(context, results);
    }

    public static void deleteFiles(Context context, List<String> filePaths) {
        if (MyDevice.hasPermission(context, UserPermission.WRITE_STORAGE) == false) return;
        List<File> files = new ArrayList<>();
        for (String filePath : filePaths) {
            File file = new File(filePath);
            if (file.exists())
                files.add(file);
        }
        deleteFileFromMediaStore(context, files);
    }

    public static int deleteTempFiles(Context context, String dirPath) {
        if (MyDevice.hasPermission(context, UserPermission.WRITE_STORAGE) == false) return 0;
        if (new File(dirPath).exists() == false) return 0;

        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return (filename.contains("temp"));
            }
        };

        List<File> files = new ArrayList<>();
        int result = 0;
        for (File file : new File(dirPath).listFiles(filter)) {
            if (file.isFile()) {
                files.add(file);
                result++;
            }
        }
        deleteFileFromMediaStore(context, files);
        return result;
    }

    public static void deleteFileFromMediaStore(Context context, List<File> files) {
        if (files.isEmpty()) return;
        final ContentResolver contentResolver = context.getContentResolver();
        List<String> canonicalPaths = new ArrayList<>();
        List<String> simpleNames = new ArrayList<>();

        for (File file : files) {
            try {
                canonicalPaths.add(file.getCanonicalPath());
            } catch (IOException e) {
                canonicalPaths.add(file.getAbsolutePath());
            }
            simpleNames.add(String.format("%s (%dkB)", getFileNameOnly(file.toString(), false), file.length() / 1000));
        }
        int result = 0;
        final Uri uri = MediaStore.Files.getContentUri("external");
        if (canonicalPaths.size() == 1)
            result = contentResolver.delete(uri, MediaStore.Files.FileColumns.DATA + " = ?", BaseUtils.toStringArray(canonicalPaths));
        else {
            String inClause = String.format(" IN (%s)", TextUtils.join(",", MyQuery.createQuestionArray(canonicalPaths)));
            result = contentResolver.delete(uri, MediaStore.Files.FileColumns.DATA + inClause, BaseUtils.toStringArray(canonicalPaths));
        }
        if (result == 0) {
            Log.e("DEBUG_JS", String.format("[MyDevice.deleteFileFromMediaStore] fail to delete %s", canonicalPaths));
            for (String filePath : canonicalPaths)
                contentResolver.delete(uri, MediaStore.Files.FileColumns.DATA + " = ?", new String[]{filePath});
        }
        for (File file : files) {
            file.delete(); // txt나 csv 같은 경우는 실제로 안 지워지는 경우가 있음
            refresh(context, file);
        }
        Log.w("DEBUG_JS", String.format("[MyDevice.deleteFiles] %s", TextUtils.join(",", simpleNames)));
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            return contentUri.getPath();
        }
    }

    public static void copy(File source, File target) throws IOException {
        InputStream in = new FileInputStream(source);
        OutputStream out = new FileOutputStream(target);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

}


/*
    public static void sendSMS(Context context, String phoneNumber, String message) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + phoneNumber));
        sendIntent.putExtra("sms_body", message);
        context.startActivity(sendIntent); // lauch SMS app
    }

//    public static void sendSMSDirectly(Context context, String phoneNumber, String message) {
//        if (hasPermission(context, UserPermission.SMS)) {
//            PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_SENT_ACTION"), 0);
//            PendingIntent deliveredIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_DELIVERED_ACTION"), 0);
//            SentBoardcastReceiver sentReceiver = new SentBoardcastReceiver();
//            context.registerReceiver(sentReceiver, new IntentFilter("SMS_SENT_ACTION"));
//            SmsManager mSmsManager = SmsManager.getDefault();
//            mSmsManager.sendTextMessage(phoneNumber, null, message, sentIntent, deliveredIntent);
//            context.unregisterReceiver(sentReceiver);
//        } else {
//            requestPermission(context, UserPermission.SMS);
//            MyUI.toast(context, String.format("SMS 권한이 필요합니다"));
//        }
//    }

    static class DeliveredBoardcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(context, "SMS 수신 성공", Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(context, "SMS 수신 실패", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
 */
