package com.teampls.shoplus.lib.database_app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.database.ItemDB;

/**
 * Created by Medivh on 2017-06-04.
 */

public class SilvermoonItemDB extends ItemDB {

    private static SilvermoonItemDB instance = null;

    public static SilvermoonItemDB getInstance(Context context) {
        try {
            Context silvermoonContext = context.createPackageContext(MyApp.Silvermoon.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            if (instance == null)
                instance = new SilvermoonItemDB(silvermoonContext);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[SilvermoonItemDB.getInstance] not found in %s", context.getPackageName()));
            instance = getInstanceMPos(context);
        }
        return instance;
    }

    public static SilvermoonItemDB getInstanceMPos(Context context) {
        try {
            Context warspear = context.createPackageContext(MyApp.Warspear.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            if (instance == null)
                instance = new SilvermoonItemDB(warspear);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[SilvermoonItemDB.getInstance] not found in %s", context.getPackageName()));
        }
        return instance;
    }

    private SilvermoonItemDB(Context silvermoonContext) {
        super(silvermoonContext);
        this.options = SilvermoonItemOptionDB.getInstance(silvermoonContext);
    }


    public static boolean exists(Context context) {
        try {
            Context silvermoonContext = context.createPackageContext(MyApp.Silvermoon.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            try {
                Context warspear = context.createPackageContext(MyApp.Warspear.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
                return true;
            } catch (PackageManager.NameNotFoundException e2) {
                return false;
            }
        }
    }
}
