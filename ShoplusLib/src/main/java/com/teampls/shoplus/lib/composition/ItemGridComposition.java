package com.teampls.shoplus.lib.composition;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseItemDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_map.KeyValueBoolean;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.datatypes.ItemDataBundle;
import com.teampls.shoplus.lib.datatypes.ItemSearchKey;
import com.teampls.shoplus.lib.dialog.CategoryDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.MyCheckableListDialog;
import com.teampls.shoplus.lib.dialog.MyCheckableListDialogOld;
import com.teampls.shoplus.lib.dialog.SortingOrderDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.enums.ItemSortingKey;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnClickPair;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseItemSearchByName;
import com.teampls.shoplus.lib.view_base.BaseItemsActivity;
import com.teampls.shoplus.lib.view_base.BaseItemsFrag;
import com.teampls.shoplus.lib.view_base.BaseModuleView;
import com.teampls.shoplus.lib.view_module.BaseFloatingButtons;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Medivh on 2017-11-17.
 */

public class ItemGridComposition implements AbsListView.OnScrollListener, View.OnClickListener {
    public static int thisView = R.layout.base_items;
    public GridView gridView;
    protected Context context;
    public BaseItemDBAdapter adapter;
    protected String title = "", KEY_Sorting = "items.sorting.key",
            KEY_FILTER_STATE = "items.filter.state", KEY_FILTER_NAME = "items.filter.name";
    public LinearLayout filterContainer;
    public TextView tvFilterCategory, tvFilterName, tvSorting, tvNotice, tvFilterProvider, tvFilterLocation,
            tvFilterState, tvCellSetting; //, tvLastSelectedItem;
    protected final KeyValueBoolean doShowName = new KeyValueBoolean("items.name", true),
            doShowPrice = new KeyValueBoolean("items.price", true), doShowSerial = new KeyValueBoolean("items.serial", false),
            doShowState = new KeyValueBoolean("items.state", true), doShowRetailPrice = new KeyValueBoolean("items.retailPrice", false);
    public ItemDB itemDB;
    protected KeyValueDB keyValueDB;
    public ItemsFloatingButtons floatingButtons;
    protected com.teampls.shoplus.lib.datatypes.ItemSearchKey itemSearchKey;
    protected BaseItemsFrag parentFrag = null;
    protected BaseModuleView parentModuleView = null;
    protected BaseItemsActivity parentActivity = null;
    private MyOnClick onBackButtonClick;
    public MyEmptyGuide myEmptyGuide;
    protected UserSettingData userSettingData;
    private List<ItemSortingKey> sortingKeys = new ArrayList<>();
    private Set<String> providerPhoneNumbers = new HashSet<>();
    protected boolean searchMoreEnabled = true, doFinishItemSearchByName = false;

    public ItemGridComposition(BaseItemsActivity parentActivity) {
        this.context = parentActivity;
        itemDB = ItemDB.getInstance(context);
        keyValueDB = KeyValueDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
        this.parentActivity = parentActivity;
    }

    public ItemGridComposition(Context context, BaseModuleView parentModuleView) {
        this.context = context;
        itemDB = ItemDB.getInstance(context);
        keyValueDB = KeyValueDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
        this.parentModuleView = parentModuleView;
    }

    public ItemGridComposition(Context context, BaseItemsFrag parentFrag) {
        this.context = context;
        itemDB = ItemDB.getInstance(context);
        keyValueDB = KeyValueDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
        this.parentFrag = parentFrag;
    }

    public void onCreateView() {
        initUiComponents(parentActivity.getView());
        gridView.setOnItemClickListener(parentActivity);
    }

    public void onCreateView(View view) {
        initUiComponents(view);
        if (parentModuleView != null)
            gridView.setOnItemClickListener(parentModuleView);
        if (parentFrag != null)
            gridView.setOnItemClickListener(parentFrag);
    }

    public ItemGridComposition setSortingKey(String key) {
        KEY_Sorting = key; // 한 앱에서도 view마다 sorting 설정을 할 수 있다
        return this;
    }

    public ItemGridComposition setItemStateKey(String key) {
        KEY_FILTER_STATE = key; // 한 앱에서도 view마다 sorting 설정을 할 수 있다
        return this;
    }

    public ItemGridComposition setFilterNameKey(String key) {
        KEY_FILTER_NAME = key;
        return this;
    }

    public List<ItemStateType> getItemStates() {
        if (keyValueDB.getValue(KEY_FILTER_STATE).isEmpty()) {
            return BaseUtils.toList(ItemStateType.values());
        } else {
            return BaseUtils.toList(ItemStateType.valueOf(keyValueDB.getValue(KEY_FILTER_STATE)));
        }
    }

    public ItemGridComposition setFinishItemSearchByName(boolean value) {
        doFinishItemSearchByName = value;
        return this;
    }

    private void initUiComponents(View view) {
        gridView = view.findViewById(R.id.items_gridview);
        gridView.setOnScrollListener(this);
        gridView.setAdapter(adapter);
        gridView.setNumColumns(MyDevice.getColumnNum(context));
        gridView.setHorizontalSpacing(1);
        gridView.setVerticalSpacing(1);

        filterContainer = view.findViewById(R.id.items_filterContainer);
        tvFilterCategory = findTextViewById(view, R.id.items_filter_category);
        tvFilterName = findTextViewById(view, R.id.items_filter_name);
        tvSorting = findTextViewById(view, R.id.items_sorting);
        tvFilterState = findTextViewById(view, R.id.items_filter_state);
        tvCellSetting = findTextViewById(view, R.id.items_cell_setting);
        tvNotice = findTextViewById(view, R.id.items_notice);
        tvFilterProvider = findTextViewById(view, R.id.items_filter_provider);
        tvFilterLocation = findTextViewById(view, R.id.items_filter_location);
    //    tvLastSelectedItem = findTextViewById(view, R.id.items_lastSelected);

        floatingButtons = new ItemsFloatingButtons(view);
        floatingButtons.hide();

        myEmptyGuide = new MyEmptyGuide(context, view, adapter, view.findViewById(R.id.items_gridview),
                R.id.items_empty_container, R.id.items_empty_message, R.id.items_empty_contactUs);

        setOnClick(view, R.id.items_filter_provider, R.id.items_filter_location,
                R.id.items_filter_category, R.id.items_filter_name, R.id.items_sorting,
                R.id.items_filter_state, R.id.items_cell_setting, R.id.items_lastSelected);

        MyView.setIconSizeByDeviceSize(context, floatingButtons.getButtons());

        onCreate();
    }

    private ItemRecord lastSelectedItemRecord = new ItemRecord();

//    public void setLastSelectedItem(ItemRecord itemRecord) {
//        this.lastSelectedItemRecord = itemRecord;
//        if (lastSelectedItemRecord.itemId > 0) {
//            tvLastSelectedItem.setVisibility(View.VISIBLE);
//            tvLastSelectedItem.setText(lastSelectedItemRecord.getUiName());
//        } else {
//            tvLastSelectedItem.setVisibility(View.GONE);
//        }
//    }

    private TextView findTextViewById(View view, int resId) {
        TextView result = (TextView) view.findViewById(resId);
        resizeViews(result);
        return result;
    }


    private Set<View> resizedViews = new HashSet<>();

    private void resizeViews(TextView view) {
        if (resizedViews.contains(view) == false) {
            MyView.setTextViewByDeviceSize(context, view);
            resizedViews.add(view);
        }
    }

    private void onCreate() {
        setSortingKeys(ItemSortingKey.values());
        setCellShowing();
        setOrderingBySortingKey();
        refreshFilterHeaders();
        resetSearchKey();
    }

    public void setSearchMores(boolean value) {
        searchMoreEnabled = value;
        if (searchMoreEnabled) {
            floatingButtons.setMoreButtonVisibility(View.VISIBLE);
        } else {
            floatingButtons.setMoreButtonVisibility(View.GONE);
        }
    }

    public void setOnBackButtonClick(MyOnClick onBackButtonClick) {
        floatingButtons.setBackButtonVisibility(View.VISIBLE);
        this.onBackButtonClick = onBackButtonClick;
    }

    public void setNotice(int visibility, String message) {
        tvNotice.setVisibility(visibility);
        tvNotice.setText(message);
    }

    public ItemGridComposition setCellShowing() {
        adapter.resetShowing();
        if (doShowName.get(context))
            adapter.showName();
        if (doShowPrice.get(context))
            adapter.showPrice();
        if (doShowSerial.get(context))
            adapter.showSerial();
        if (doShowState.get(context))
            adapter.showState();

        // userSetting에서 불러오면 에러가 난다. 보완할 부분
        if (BasePreference.retailEnabled.getValue(context) && doShowRetailPrice.get(context)) // 켜져있는 상태에서
            adapter.showRetailPrice();

        switch (getSortingKey()) {
            default:
                break;
            case ItemId:
                adapter.showDate();
                break;
            case Name:
                adapter.showChar();
                break;
            case RecentVisited:
                break;
            case StockCount:
                //adapter.showDate();
                break;
        }
        return this;
    }

    public void refreshFilterHeaders() {
        if (adapter.getFilterStates().size() == 1) {
            tvFilterState.setText(adapter.getFilterStates().get(0).toUIStr());
            tvFilterState.setTextColor(ColorType.Blue.colorInt);
        } else if (adapter.getFilterStates().size() != 1) {
            tvFilterState.setText("모든상태");
            tvFilterState.setTextColor(ColorType.Black.colorInt);
        }

        switch (ItemSearchType.getFrom(keyValueDB, KEY_FILTER_NAME)) {
            case ByName:
                tvFilterName.setText("이름");
                tvFilterName.setTextColor(ColorType.Black.colorInt);
                break;
            case BySerialNum:
                tvFilterName.setText("번호");
                tvFilterName.setTextColor(ColorType.Blue.colorInt);
                break;
        }

        tvFilterCategory.setText("분류");
        tvFilterCategory.setTextColor(ColorType.Black.colorInt);
        if (getAdapter().isCategoryFiltered()) {
            tvFilterCategory.setText(getAdapter().getFilterCategory().toUIStr());
            tvFilterCategory.setTextColor(ColorType.Blue.colorInt);
        }

        if (providerPhoneNumbers.isEmpty()) {
            providerPhoneNumbers = new HashSet<>(itemDB.getStrings(ItemDB.Column.providerPhoneNumber));
            providerPhoneNumbers.remove("");
        }

        if (providerPhoneNumbers.size() >= 2) {
            tvFilterProvider.setText(String.format("거래처 (%d)", providerPhoneNumbers.size()));
        } else {
            tvFilterProvider.setText("거래처");
        }

        tvFilterProvider.setTextColor(ColorType.Black.colorInt);
        if (getAdapter().isProvidersFiltered()) {
            if (getAdapter().getFilterProviders().size() == 1) {
                UserRecord provider = UserDB.getInstance(context).getRecord(getAdapter().getFilterProviders().get(0));
                tvFilterProvider.setText(provider.getName());
                tvFilterProvider.setTextColor(ColorType.Blue.colorInt);
            }
        }

        ItemSortingKey key = getSortingKey();
        tvSorting.setText(key.uiStr);
        switch (key) {
            default:
                tvSorting.setTextColor(ColorType.Blue.colorInt);
                break;
            case ItemId:
                tvSorting.setTextColor(ColorType.Black.colorInt);
                break;
        }
    }

    private void resetSortingKeyIfSkipGeneration() {
        switch (getSortingKey()) {
            case ItemId:
            case Name:
                // 정상적인 Ordering
                break;
            case RecentVisited:
            case StockCount:
                getAdapter().resetOrdering();
                keyValueDB.put(KEY_Sorting, ItemSortingKey.ItemId.toString());
                break;
        }
    }

    protected ItemSortingKey getSortingKey() {
        return ItemSortingKey.valueOfWithDefault(keyValueDB.getValue(KEY_Sorting, ItemSortingKey.ItemId.toString()));
    }

    protected void resetSearchKey() {
        ItemRecord record = itemDB.getDownloadedFirstItem().clone();
        record.createdDateTime = BaseUtils.toMonth(record.createdDateTime.plusMonths(1));
        // 4월 12일 경우 5월 1일로 지정, 그래야 실제 함수에 5월 1일이 들어가서 4월 1일까지 검색을 한다.
        itemSearchKey = new ItemSearchKey(record);
    }

    public void setOnClick(View view, int... resIds) {
        for (int resId : resIds) {
            View myView = view.findViewById(resId);
            myView.setOnClickListener(this);
            if (myView instanceof TextView)
                resizeViews((TextView) myView);
        }
    }

    protected void setVisibility(View view, int visibility, int... resIds) {
        for (int resId : resIds) {
            view.findViewById(resId).setVisibility(visibility);
        }
    }

    public void setAdapter(BaseItemDBAdapter adapter) {
        this.adapter = adapter;
    }

    public void setOrderingBySortingKey() {
        getAdapter().setSortingKey(getSortingKey().mSortingKey);
        switch (getSortingKey()) {
            // 실제 Ordering이 수행됨
            default:
                getAdapter().enableCountMap(false);
                break;
            case StockCount:
                getAdapter().enableCountMap(true).setCountMap(itemDB.getStockMap()); // memoryDB는?
                break;
            case RecentVisited:
                getAdapter().resetFiltering().resetOrdering().enableCountMap(false).setRecords(itemDB.getRecentRecords());
                break;
        }
    }

    public BaseItemDBAdapter getAdapter() {
        return adapter;
    }

    protected void refreshParent() {
        if (parentFrag != null)
            parentFrag.refresh();
        if (parentModuleView != null)
            parentModuleView.refresh();
        if (parentActivity != null)
            parentActivity.refresh();
    }

    public void onRefresh() {
        floatingButtons.refresh();
        myEmptyGuide.refresh();
        refreshFilterHeaders();
    }

    private void onItemClickParent(int position) {
        if (parentFrag != null)
            parentFrag.onItemClick(null, null, position, 0);
        if (parentModuleView != null)
            parentModuleView.onItemClick(null, null, position, 0);
        if (parentActivity != null)
            parentActivity.onItemClick(null, null, position, 0);
    }

    public void setSortingKeys(ItemSortingKey... keys) {
        sortingKeys.clear();
        for (ItemSortingKey sortingKey : keys)
            sortingKeys.add(sortingKey);
    }

    @Override
    public void onClick(View view) {
        if (view == null) return;
        if (view.getId() == R.id.items_sorting) {
            onSortingClick();
        } else if (view.getId() == R.id.items_filter_category) {
            onFilterCategoryClick();
        } else if (view.getId() == R.id.items_filter_name) {
            onFilterNameClick();
        } else if (view.getId() == R.id.items_filter_provider) {
            onFilterProviderClick();
        } else if (view.getId() == R.id.items_filter_location) {
            onFilterLocationClick();
        } else if (view.getId() == R.id.items_filter_state) {
            onFilterStateClick();
        } else if (view.getId() == R.id.items_cell_setting) {
            onCellSettingClick();
        } else if (view.getId() == R.id.items_lastSelected) {
            if (lastSelectedItemRecord.itemId > 0)
                selectItem(lastSelectedItemRecord);
        }
    }

    public void selectItem(ItemRecord itemRecord) {
        int position = 0;
        boolean found = false;
        for (ItemRecord record : adapter.getRecords()) {
            if (record.itemId == itemRecord.itemId) {
                found = true;
                onItemClickParent(position);
                break;
            }
            position++;
        }
        if (found == false)
            MyUI.toastSHORT(context, String.format("현재 리스트에는 없습니다"));
    }

    private void onSortingClick() {
        final ItemSortingKey prevSortingKey = getSortingKey();

        new SortingOrderDialog(context, "다음 순서로 보기", prevSortingKey, sortingKeys) {
            @Override
            public void onItemClicked(ItemSortingKey sortingKey) {
                keyValueDB.put(KEY_Sorting, sortingKey.toString());
                if (prevSortingKey == ItemSortingKey.RecentVisited) // 최근것 보기에서 탈출
                    getAdapter().resetRecords();
                switch (sortingKey) {
                    default:
                        adapter.enableCountMap(false);
                        break;
                    case StockCount:
                        adapter.autoSetCountMap();
                        break;
                }
                resetSearchKey();
                refreshFilterHeaders();
                setCellShowing();
                setOrderingBySortingKey();
                refreshParent();
            }
        };
    }

    private void onFilterProviderClick() {
        final String KEY_allProvider = "0000", VALUE_allProvider = " [모든 거래처] ";
        List<String> providerPhoneNumbers = itemDB.getStrings(ItemDB.Column.providerPhoneNumber);
        Map<String, String> providers = UserDB.getInstance(context).getNameMap(providerPhoneNumbers);
        providers.put(KEY_allProvider, VALUE_allProvider);

        String currentProviderName = VALUE_allProvider;
        if (getAdapter().isProvidersFiltered()) {
            final List<String> selectedProviders = getAdapter().getFilterProviders();
            if (selectedProviders.size() == 1) {
                currentProviderName = providers.get(selectedProviders.get(0));
            } else if (selectedProviders.size() >= 2) {
                currentProviderName = providers.get(selectedProviders.get(0));
                Log.e("DEBUG_JS", String.format("[ItemGridComposition.filterProvider] multiple proviers : %s", selectedProviders));
            }
        }

        final Map<String, String> sortedProviders = BaseUtils.sortByValue(providers);
        new MyCheckableListDialogOld(context, "거래처 선택", "", new ArrayList(sortedProviders.values()), currentProviderName) {
            @Override
            protected void onItemClick(int position, String selectedName) {
                String providerPhoneNumber = new ArrayList<String>(sortedProviders.keySet()).get(position); // 같은 이름이 있을 수 있으므로, 순서로 구분해야 함

                // 거래처와 카테고리는 AND 조건으로 묶는다
                if (getAdapter().isCategoryFiltered()) {
                    ItemCategoryType category = getAdapter().getFilterCategory();
                    getAdapter().resetFiltering().filterCategory(category);
                } else {
                    getAdapter().resetFiltering();
                }

                if (providerPhoneNumber.equals(KEY_allProvider) == false)
                    getAdapter().filterProviders(BaseUtils.toList(providerPhoneNumber));

                resetSortingKeyIfSkipGeneration();
                refreshFilterHeaders();
                resetSearchKey();
                refreshParent();
                dismiss();
            }
        };
    }

    protected void onFilterLocationClick() {

    }

    protected void onFilterStateClick() {

        new MyCheckableListDialog<ItemStateType>(context, "상품 상태별 보기", BaseUtils.toList(ItemStateType.values()),
                adapter.getFilterStates().size() == 1 ? adapter.getFilterStates().get(0) : null, "모든상태") {
            @Override
            public void onItemClicked(@Nullable ItemStateType state) {
                if (state == null) {
                    adapter.filterStates(ItemStateType.values());
                    keyValueDB.put(KEY_FILTER_STATE, "");
                } else {
                    adapter.filterStates(state);
                    keyValueDB.put(KEY_FILTER_STATE, state.toString());
                }
                refreshFilterHeaders();
                refreshParent();
            }

            protected String getUiString(ItemStateType record) {
                return record.toUIStr();
            }
        };
    }

    protected void onCellSettingClick() {
        new CellSettingDialog(context).show();
        //displayContainer.setVisibility(displayContainer.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }

    class CellSettingDialog extends MyButtonsDialog {

        public CellSettingDialog(Context context) {
            super(context, "표시 설정", "상품 이미지 위에 표시할 내용을 설정합니다");
            setTopMargin(0);
            addCheckBox("이름 표시", doShowName, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    onSettingClick();
                }
            });

            addCheckBox("가격 표시", doShowPrice, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    onSettingClick();
                }
            });

            addCheckBox("번호 표시", doShowSerial, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    onSettingClick();
                }
            });

            addCheckBox("상태 표시", doShowState, new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    onSettingClick();
                }
            });

            if (userSettingData.getProtocol().isRetailOn()) {
                addCheckBox("소매가를 표시", doShowRetailPrice, new MyOnTask<Boolean>() {
                    @Override
                    public void onTaskDone(Boolean result) {
                        onSettingClick();
                    }
                });
            }
        }

        private void onSettingClick() {
            setCellShowing();
            refreshParent();
        }
    }

    protected void onFilterCategoryClick() {
        // 카테고리는 adapter의 카테고리 필터링을 이용한다
        new CategoryDialog(context, "분류별 상품보기", itemDB.getRecords(), true, true, true) {
            @Override
            public void onItemClicked(ItemCategoryType category) {
                // 거래처와 카테고리는 AND 조건으로 묶는다
                if (getAdapter().isProvidersFiltered()) {
                    List<String> providers = getAdapter().getFilterProviders();
                    getAdapter().resetFiltering().filterProviders(providers);
                } else {
                    getAdapter().resetFiltering();
                }

                if (category != ItemCategoryType.ALL)
                    getAdapter().filterCategory(category);

                resetSearchKey();
                resetSortingKeyIfSkipGeneration();
                refreshFilterHeaders();
                refreshParent();
            }
        };
    }

    protected void onFilterNameClick() {
        // 이름은 String인 이름으로 필터링을 하지 않고 itemId로 필터링을 한다
        ItemSearchType itemSearchType = ItemSearchType.getFrom(keyValueDB, KEY_FILTER_NAME);
        BaseItemSearchByName.startActivity(context, itemSearchType, KEY_FILTER_NAME,
                searchMoreEnabled, doFinishItemSearchByName, new MyOnClick<ItemRecord>() {
                    @Override
                    public void onMyClick(View view, ItemRecord record) {
                        // single item clicked ... 필터를 사용하지 않고 바로 찾는 아이템을 보여준다
                        resetSearchKey();
                        resetSortingKeyIfSkipGeneration();
                        refreshFilterHeaders();

                        if (record.name.equals(BaseItemSearchByName.KEY_SHOW_ALL)) {
                            getAdapter().resetFiltering();
                            refreshFilterHeaders();
                            refreshParent();
                        } else {
                            boolean found = false;
                            int position = 0;
                            for (ItemRecord adapterRecord : adapter.getRecords()) {
                                if (adapterRecord.itemId == record.itemId) {
                                    onItemClickParent(position);
                                    found = true;
                                    break;
                                }
                                position++;
                            }

                            // 기존에 없었던 아이템 (검색으로 새로 찾은 아이템)
                            if (found == false) {
                                adapter.addRecord(record);
                                onItemClickParent(adapter.getCount() - 1);
                            }
                        }
                    }
                }, new MyOnClickPair<String, List<ItemRecord>>() {
                    @Override
                    public void onMyClick(View view, String keyword, List<ItemRecord> records) {
                        // multiple items selected ...  필터링을 한다
                        resetSearchKey();
                        List<Integer> itemIds = new ArrayList<>();
                        for (ItemRecord record : records)
                            itemIds.add(record.itemId);
                        getAdapter().resetFiltering().filterItemIds(itemIds);
                        resetSortingKeyIfSkipGeneration();
                        refreshFilterHeaders();
                        if (keyword.isEmpty()) {
                            tvFilterName.setText("이름(클릭)");
                            tvFilterName.setTextColor(ColorType.Black.colorInt);
                        } else {
                            tvFilterName.setText(keyword);
                            tvFilterName.setTextColor(ColorType.Blue.colorInt);
                        }
                        refreshParent();
                    }
                });
    }

    private boolean onNotifyDataSetChanged = false;

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        adapter.setScrollState(scrollState);
        switch (scrollState) {
            case SCROLL_STATE_FLING:
                break;
            case SCROLL_STATE_IDLE:
                if (gridView.getFirstVisiblePosition() == 0) {
                    if (onNotifyDataSetChanged) return;
                    onNotifyDataSetChanged = true;
                    new MyDelayTask(context, 1) {
                        @Override
                        public void onFinish() {
                            adapter.notifyDataSetChanged(); // 새로 싹 그려주는게 모든 문제를 해결한다
                            onNotifyDataSetChanged = false;
                        }
                    };
                }
                break;
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        adapter.firstVisibleItem = firstVisibleItem;
        if (floatingButtons != null) {
            if (gridView.getFirstVisiblePosition() == 0) {
                floatingButtons.setMoveTo0Visibility(View.GONE);
            } else {
                floatingButtons.setMoveTo0Visibility(View.VISIBLE);
            }
        }
    }


    private MyOnClick onShowHistoryClick;

    public void setOnShowHistoryClick(MyOnClick onClick) {
        this.onShowHistoryClick = onClick;
        floatingButtons.setShowHistory(View.VISIBLE);
    }

    public class ItemsFloatingButtons extends BaseFloatingButtons {
        protected ImageView ivBack, ivResetFilter, ivLoadMore, ivMoveToPosition0, ivShowHistory;
        protected boolean ivBackVisible = true, ivResetFilterVisible = true, ivLoadMoreVisible = true,
                ivMoveToPosition0Visible = true, ivShowHistoryVisible = false;

        public ItemsFloatingButtons(View view) {
            super(view);
            add(R.id.items_reset_filter, R.id.items_more, R.id.items_back, R.id.items_to_position0, R.id.items_show_history);
            ivBack = view.findViewById(R.id.items_back);
            ivResetFilter = view.findViewById(R.id.items_reset_filter);
            ivLoadMore = view.findViewById(R.id.items_more);
            ivMoveToPosition0 = view.findViewById(R.id.items_to_position0);
            ivMoveToPosition0.setImageAlpha(150);
            ivBack.setVisibility(View.GONE);
            ivShowHistory = view.findViewById(R.id.items_show_history);
            ivShowHistory.setImageAlpha(200);
            MyView.setImageViewSizeByDeviceSize(context, ivBack, ivResetFilter, ivLoadMore, ivMoveToPosition0, ivShowHistory);
            hide();
        }

        public void setVisibility(int visibility) {
            view.findViewById(R.id.items_floatingContainer).setVisibility(visibility);
        }

        public void setShowHistory(int visibility) {
            ivShowHistory.setVisibility(visibility);
            ivShowHistoryVisible = (visibility == View.VISIBLE);
        }

        public void setBackButtonVisibility(int visibility) {
            ivBack.setVisibility(visibility);
            ivBackVisible = (visibility == View.VISIBLE);
        }

        public void setMoreButtonVisibility(int visibility) {
            ivLoadMore.setVisibility(visibility);
            ivLoadMoreVisible = (visibility == View.VISIBLE);
        }

        public void setResetFilterVisibility(int visibility) {
            ivResetFilter.setVisibility(visibility);
            ivResetFilterVisible = (visibility == View.VISIBLE);
        }

        public void setMoveTo0Visibility(int visibility) {
            ivMoveToPosition0.setVisibility(visibility);
            ivMoveToPosition0Visible = (visibility == View.VISIBLE);
        }

        protected void onResetFilterClick() {
            resetSearchKey();
            getAdapter().resetFiltering();
            resetSortingKeyIfSkipGeneration();
            refreshFilterHeaders();
            refreshParent();
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.items_reset_filter) {
                onResetFilterClick();

            } else if (view.getId() == R.id.items_show_history) {
                if (onShowHistoryClick != null)
                    onShowHistoryClick.onMyClick(view, "");

            } else if (view.getId() == R.id.items_to_position0) {
                gridView.post(new Runnable() {
                    @Override
                    public void run() {
                        gridView.smoothScrollToPosition(0);
                    }
                });
                ivMoveToPosition0.setVisibility(View.GONE);

            } else if (view.getId() == R.id.items_more) {
                if (getAdapter().isCategoryFiltered()) {
                    DateTime monthAgoDate = BaseUtils.toMonth(itemSearchKey.getDate().minusMonths(1));
                    String title = String.format("%s까지 조회", monthAgoDate.toString(BaseUtils.YMD_Kor_Format));
                    String message = String.format("[%s] 상품을 더 보시겠습니까?\n\n버튼을 누를때 마다 한달씩 검색 기간이 늘어납니다.",
                            getAdapter().getFilterCategory().toUIStr());
                    new MyAlertDialog(context, title, message) {
                        @Override
                        public void yes() {
                            downCategory(getAdapter().getFilterCategory());
                        }
                    };
                }
            } else if (view.getId() == R.id.items_back) {
                if (onBackButtonClick != null)
                    onBackButtonClick.onMyClick(view, "");
            }
        }

        private void downCategory(final ItemCategoryType itemCategoryType) {
            new CommonServiceTask(context, "BaseItemsFrag") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    Pair<ItemDataBundle, com.teampls.shoplus.lib.datatypes.ItemSearchKey> pair =
                            itemService.getItemsOfCategory(userSettingData.getUserShopInfo(), itemCategoryType, itemSearchKey);
                    updateItemDB(pair.first);
                    itemSearchKey = pair.second;
                    MyUI.toastSHORT(context, String.format("%d건이 발견됐습니다", pair.first.getItems().size()));
                }

                @Override
                public void onPostExecutionUI() {
                    refreshParent();
                }
            };
        }

        protected void updateItemDB(ItemDataBundle bundle) {
            itemDB.updateOrInsert(bundle);
        }

        public void refresh() {
            if (getAdapter().isCategoryFiltered() || getAdapter().isItemIdsFiltered() || getAdapter().isProvidersFiltered()) {
                if (ivResetFilterVisible)
                    ivResetFilter.setVisibility(View.VISIBLE);
                if (ivLoadMoreVisible)
                    ivLoadMore.setVisibility(View.VISIBLE);
            } else {
                ivResetFilter.setVisibility(View.GONE);
                ivLoadMore.setVisibility(View.GONE);
            }

//            if (getAdapter().isCategoryFiltered()) {
//                if (ivLoadMoreVisible)
//                    ivLoadMore.setVisibility(View.VISIBLE);
//            } else {
//                ivLoadMore.setVisibility(View.GONE);
//            }

            if (gridView.getFirstVisiblePosition() == 0) {
                ivMoveToPosition0.setVisibility(View.GONE);
            } else {
                if (ivLoadMoreVisible)
                    ivMoveToPosition0.setVisibility(View.VISIBLE);
            }

        }
    }
}

/*
//
//        switch (getSortingKey()) {
//            // 실제 Ordering이 수행됨
//            default:
//            case ItemId:
//                if (getAdapter().isSkipGeneration())
//                    getAdapter().resetRecords();
//                getAdapter().resetOrdering();
//                break;
//            case Name:
//                if (getAdapter().isSkipGeneration())
//                    getAdapter().resetRecords();
//                getAdapter().setOrderRecord(ItemDB.Column.name, false, false);
//                break;
//
//            // 직접 Ordering을 하고 그 결과를 바로 적용함
//            case RecentVisited:
//                getAdapter().resetFiltering().resetOrdering().setRecords(itemDB.getRecentRecords());
//                break;
//            case StockCount:
//                Map<Integer, Integer> stockCounts = new HashMap<>();
//                List<ItemRecord> records = getAdapter().getRecords();
//                if (records.isEmpty()) // 최초 시작시는 없을 수 있다
//                    records = itemDB.getRecords();
//                for (ItemRecord itemRecord : records) {
//                    int stockCount = itemDB.options.getPositiveStockSum(itemRecord.itemId);
//                    stockCounts.put(itemRecord.itemId, stockCount);
//                }
//                stockCounts = BaseUtils.sortByValue(stockCounts);
//                List<Integer> sortedItemIds = new ArrayList(stockCounts.keySet());
//                Collections.reverse(sortedItemIds);
//                getAdapter().resetFiltering().resetOrdering().setRecords(itemDB.getRecordsBy(sortedItemIds));
//                break;
//        }
 */