package com.teampls.shoplus.lib.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

/**
 * Created by Medivh on 2016-10-04.
 */
abstract public class MyCountDownTimer  {
    private long mMillisInFuture = Long.MAX_VALUE;
    private final long mCountdownInterval;
    private long mStopTimeInFuture;
    private boolean mCancelled = false;
    protected int count = 0, elapsedTimeMs = 0;
    protected ProgressDialog progressDialog;
    public boolean onTasking = false;

    public MyCountDownTimer(long interval_ms, long max_ms) {
        mCountdownInterval = interval_ms;
        mMillisInFuture = max_ms;
        onPreTick();
    }

    public MyCountDownTimer(long interval_ms) {
        this(interval_ms, Long.MAX_VALUE);
    }

    public final void cancel() {
        mHandler.removeMessages(MSG);
        mCancelled = true;
        onTasking = false;
        if (progressDialog != null)
            progressDialog.cancel();
    }

    public MyCountDownTimer enableProgressDialog(Context context, String message) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(message);
        progressDialog.show();
        return this;
    }

    public synchronized final MyCountDownTimer start() {
        if (mMillisInFuture <= 0) {
            onTimeOut();
            return this;
        }
        if (onTasking) {
            onTimeOut();
            return this;
        }

        onTasking = true;
        mStopTimeInFuture = SystemClock.elapsedRealtime() + mMillisInFuture;
        mHandler.sendMessage(mHandler.obtainMessage(MSG));
        mCancelled = false;
        //    Log.i("DEBUG_JS", String.format("[%s.initBase] ", getClass().getSimpleName()));
        return this;
    }

    public void onPreTick() {

    }

    public abstract void onTick(long millsLeft);

    public void onTimeOut() {
        if (progressDialog != null)
            progressDialog.cancel();
        onTasking = false;
    };

    private static final int MSG = 1;

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            synchronized (MyCountDownTimer.this) {
                final long millisLeft = mStopTimeInFuture - SystemClock.elapsedRealtime();

                if (millisLeft <= 0) {
                    onTimeOut();
                } else if (millisLeft < mCountdownInterval) {
                    // no tick, just delay until done
                    sendMessageDelayed(obtainMessage(MSG), millisLeft);
                } else {
                    long lastTickStart = SystemClock.elapsedRealtime();
                    elapsedTimeMs = (int) mCountdownInterval*count;
                    onTick(millisLeft);
                    count ++;

                    // take into account user_offline's onTickEvent taking time to execute
                    long delay = lastTickStart + mCountdownInterval - SystemClock.elapsedRealtime();

                    // special case: user_offline's onTickEvent took more than interval to
                    // complete, skip to next interval
                    while (delay < 0) delay += mCountdownInterval;

                    if (!mCancelled) {
                        sendMessageDelayed(obtainMessage(MSG), delay);
                    }
                }
            }
        }
    };

}

