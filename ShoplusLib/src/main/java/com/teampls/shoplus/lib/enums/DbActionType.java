package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2017-01-17.
 */

public enum DbActionType {
    DEFAULT, INSERT, UPDATE, DELETE, MULTIPLE_UPDATE, READ, SPLIT; // CRUD (create, read, update, delete)
    public int count = 1;
}
