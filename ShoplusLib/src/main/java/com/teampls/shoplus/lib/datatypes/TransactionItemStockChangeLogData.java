package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.enums.SlipItemType;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 거래항목에 따른 재고수량 변동 로그
 *
 * @author lucidite
 */
public class TransactionItemStockChangeLogData implements ItemStockChangeLogProtocol {
    private String logId;
    private DateTime updated;
    private String transactionId;
    private SlipItemType transactionItemType;
    private int quantity;
    private boolean cancelled;

    public TransactionItemStockChangeLogData(JSONObject dataObj) throws JSONException {
        if (dataObj == null) {
            this.logId = "";
            this.updated = APIGatewayHelper.fallbackDatetime;
            this.transactionId = "";
            this.transactionItemType = SlipItemType.SALES;
            this.quantity = 0;
            this.cancelled = false;
        } else {
            this.logId = dataObj.getString("log_id");
            this.updated = APIGatewayHelper.getDateTimeFromRemoteString(dataObj.getString("updated"));
            this.transactionId = dataObj.getString("tr_id");
            this.transactionItemType = SlipItemType.getFromRemoteTypeString(dataObj.getString("tr_type"));
            this.quantity = dataObj.optInt("quantity", 0);
            this.cancelled = dataObj.optBoolean("cancelled", false);
        }
    }

    private int getStockSign() {
        switch (this.transactionItemType) {
            case SALES: return -1;
            case EXCHANGE_SALES: return -1;

            case RETURN: return 1;
            case EXCHANGE_RETURN: return 1;
            case DEPOSIT: return 1;

            case ADVANCE_CLEAR: return -1;
            case PREORDER_CLEAR: return -1;

            case CONSIGN: return -1;
            case SAMPLE: return -1;

            case CONSIGN_RETURN: return 1;
            case SAMPLE_RETURN: return 1;
            default: return 0;
        }
    }

    @Override
    public String getLogId() {
        return this.logId;
    }

    @Override
    public DateTime getUpdatedDateTime() {
        return this.updated;
    }

    @Override
    public int getValue() {
        int baseQuantity = (this.cancelled) ? -this.quantity : quantity;
        return this.getStockSign() * baseQuantity;
    }

    public String getTransactionId() {
        return this.transactionId;
    }

    public SlipItemType getTransactionItemType() {
        return this.transactionItemType;
    }

    public boolean isCancelled() {
        return this.cancelled;
    }
}
