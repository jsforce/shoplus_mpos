package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.view.MyView;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Medivh on 2016-10-31.
 */
abstract public class BaseViewHolder implements View.OnClickListener {
    public int position = 0;
    public String title = "";
    protected ColorType selectedColor = ColorType.Khaki;
    private Set<View> resizedViews = new HashSet<>();

    public abstract int getThisView();

    public abstract void init(View view);

    public abstract void update(int position);

    public void refresh() {
    }

    public void refreshAdapter() {

    }

    public void setColorSet(ColorType selectedColor) {
        this.selectedColor = selectedColor;
    }

    public void onClick(View view) {

    }

    public void setYPos(int y) {

    }

    public String getTitle() {
        return title;
    }

    public View setOnClick(View view, int viewId) {
        View v = view.findViewById(viewId);
        v.setOnClickListener(this);
        return v;
    }

    public void setOnClick(View view, int... resIds) {
        for (int resId : resIds) {
            setOnClick(view, resId);
        }
    }

    public void setEnabled(View view, Boolean enabled, int... resIds) {
        for (int resId : resIds) {
            View v = view.findViewById(resId);
            v.setEnabled(enabled);
        }

    }

    public void setVisibility(View view, int visibility, int... resIds) {
        for (int resId : resIds) {
            View v = view.findViewById(resId);
            v.setVisibility(visibility);
        }
    }

    public void setVisibility(int visibility, View... views) {
        for (View v : views)
            v.setVisibility(visibility);
    }

    public TextView findTextViewById(Context context, View view, int resId) {
        TextView textView = (TextView) view.findViewById(resId);
        if (resizedViews.contains(textView) == false) {
            MyView.setTextViewByDeviceSize(context, textView);
            resizedViews.add(textView);
        }
        return textView;
    }

    public TextView findTextViewById(Context context, View view, int resId, int fontSize) {
        TextView textView = view.findViewById(resId);
        if (resizedViews.contains(textView) == false) {
            MyView.setTextViewByDeviceSize(context, textView, fontSize);
            resizedViews.add(textView);
        }
        return textView;
    }

    public TextView findTextViewById(Context context, View view, int resId, String text) {
        TextView textView = (TextView) view.findViewById(resId);
        textView.setText(text);
        if (resizedViews.contains(textView) == false) {
            MyView.setTextViewByDeviceSize(context, textView);
            resizedViews.add(textView);
        }
        return textView;
    }

    public <T extends View> T setView(View view, int resId) {
        return (T) view.findViewById(resId); // 편하긴 하지만 오류의 가능성이...
    }
}
