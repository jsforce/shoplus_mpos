package com.teampls.shoplus.lib.view_module;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-06-16.
 */

public class MyTextGroup<T> implements View.OnClickListener {
    private Context context;
    private View view;
    private View.OnClickListener onClickListener;
    private List<TextView> textViews = new ArrayList<>();
    private TextView clickedTextView;
    private ProgressDialog progressDialog;
    private boolean doShowProgress = false;

    public MyTextGroup(Context context, View view) {
        this.context = context;
        this.view = view;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("처리중...");
    }

    public void showProgress() {
        this.doShowProgress = true;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onClick(View v) {
        if (v instanceof TextView == false)
            return;
        clickedTextView = (TextView) v;
        if (onClickListener != null) {
            if (doShowProgress)
                MyUI.show(context, progressDialog);
            onClickListener.onClick(clickedTextView);
        } else {
            onProgressFinish();
        }
    }

    public void onProgressFinish() {
        if (progressDialog != null)
            if (doShowProgress)
                progressDialog.dismiss();
    }

    public T getClickedItem() {
        if (clickedTextView == null) {
            if (textViews.size() == 0) {
                return null;
            } else {
                return (T) textViews.get(0).getTag();
            }
        } else {
            return (T) clickedTextView.getTag();
        }
    }

    public MyTextGroup add(int resId, T tag) {
        return add((TextView) view.findViewById(resId), tag);
    }

    public MyTextGroup add(TextView textView, T tag) {
        textView.setOnClickListener(this);
        textView.setTag(tag);
        MyView.setTextViewByDeviceSize(context, textView);
        textViews.add(textView);
        return this;
    }
}
