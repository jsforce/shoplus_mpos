package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.enums.MessengerType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * 사용자의 메신저 링크 설정 데이터 클래스
 *
 * @author lucidite
 */
public class MessengerLinkData {
    private MessengerType type;
    private String value;

    public MessengerLinkData() {
        type = MessengerType.NOT_DEFINED;
        value = "";
    }

    public MessengerLinkData(MessengerType type, String value) { // not URL
        this.type = type;
        this.value = value;
    }

    public MessengerType getType() {
        return type;
    }

    public static final String kakaoHeader = "http://qr.kakao.com/talk/";
    public static final String plusHeader = "http://pf.kakao.com/";
    public static final String weChatHeader = "https://u.wechat.com/";

    /**
     * QR코드 인쇄 등에 사용하기 위한 연결 URL 문자열을 반환한다.
     * @return
     */
    public String getURLStr() {
        if (this.value.isEmpty())
            return "";
        switch (this.type) {
            case KAKAOTALK: return kakaoHeader + this.value;
            case PLUSFRIEND: return plusHeader + this.value;
            case WECHAT: return  weChatHeader+ this.value;
            default: return "";
        }
    }

    /**
     * QR코드 인쇄 등에 사용하기 위한 연결 URL 문자열을 반환한다.
     * 플러스친구일 경우 채팅으로 바로 연결할 수 있다.
     * @return
     */
    public String getChatUrlStr() {
        switch (this.type) {
            case KAKAOTALK: return this.getURLStr();
            case PLUSFRIEND: return "http://pf.kakao.com/" + this.value + "/chat";
            case WECHAT: return this.getURLStr();
            default: return "";
        }
    }

    /**
     * 앱에서 메신저를 열기 위한 링크 URL 문자열을 반환한다.
     * [NOTE] 플러스친구 스키마 테스트 필요 (Android에서 동작 확인되지 않음)
     * @return
     */
    public String getAppLinkStr() {
        switch (this.type) {
            case KAKAOTALK: return this.getURLStr();
            case PLUSFRIEND: return "kakaoplus://plusfriend/home/" + this.value;
            case WECHAT: return this.getURLStr();
            default: return "";
        }
    }

    @Override
    public int hashCode() {
        return this.type.hashCode() * 31 + this.value.hashCode();
    }

    public static Set<MessengerLinkData> parse(JSONObject linksObj) throws JSONException {
        Set<MessengerLinkData> result = new HashSet<>();
        if (linksObj == null) {
            return result;
        }
        Iterator<String> iter = linksObj.keys();

        while (iter.hasNext()) {
            String messengerTypeStr = iter.next();
            MessengerType type = MessengerType.ofRemoteStr(messengerTypeStr);
            String link = linksObj.getString(messengerTypeStr);
            result.add(new MessengerLinkData(type, link));
        }
        return result;
    }
}
