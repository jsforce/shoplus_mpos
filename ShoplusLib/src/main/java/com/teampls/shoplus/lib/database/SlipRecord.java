package com.teampls.shoplus.lib.database;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.enums.AccountLogType;
import com.teampls.shoplus.lib.enums.SlipGenerationType;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.protocol.TransactionDraftDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-09-02.
 */
public class SlipRecord implements Comparable<SlipRecord> {
    public int id = 0, onlinePayment = 0, entrustPayment = 0;
    public String ownerPhoneNumber = "", counterpartPhoneNumber = "", composerPhoneNumber = "", // 매장, 거래처, 실작성자
            onlinePaidDateStr = "", entrustPaidDateStr = "";
    public DateTime createdDateTime = Empty.dateTime, salesDateTime = Empty.dateTime, cancelledDateTime = Empty.dateTime;
    public boolean confirmed = false, onlinePaid = false, entrustPaid = false, completed = false, cancelled = false;
    public SlipGenerationType generation = SlipGenerationType.NONE;
    public boolean mOnDeleting = false; // t삭제 예정인 slip 표시
    public MSortingKey sortingKey = MSortingKey.Date;
    public int bill = 0;

    public SlipRecord(SlipDataProtocol protocol) {
        this.ownerPhoneNumber = protocol.getOwnerPhoneNumber();
        this.counterpartPhoneNumber = protocol.getCounterpartPhoneNumber();
        this.composerPhoneNumber = protocol.getComposerPhoneNumber();
        this.createdDateTime = protocol.getCreatedDatetime();
        this.salesDateTime = protocol.getSalesDate();
        this.onlinePayment = protocol.getPaymentTerms().getOnlinePayment();
        this.onlinePaid = protocol.getPaymentTerms().isOnlinePaymentCleared();
        this.onlinePaidDateStr = protocol.getPaymentTerms().getOnlinePaymentClearedDate();
        this.entrustPayment = protocol.getPaymentTerms().getEntrustedPayment();
        this.entrustPaid = protocol.getPaymentTerms().isEntrustedPaymentCleared();
        this.entrustPaidDateStr = protocol.getPaymentTerms().getEntrustedPaymentClearedDate();
        this.confirmed = true;
        this.generation = protocol.getSlipType();
        this.completed = protocol.isCompleted();
        this.cancelled = protocol.isCancelled();
        this.cancelledDateTime = protocol.getCancelledDatetime();
        this.mOnDeleting = protocol.isDeletionScheduled();
        this.bill = protocol.getDemandForUnpayment();
    }

    private String getProviderNumber(Context context) {
        return UserSettingData.getInstance(context).getProviderRecord().phoneNumber;
    }

    public String getTransactionId() {
        return APIGatewayHelper.getRemoteDateTimeString(salesDateTime);
    }

    public SlipRecord(Context context, TransactionDraftDataProtocol protocol) {
        this.ownerPhoneNumber = getProviderNumber(context);
        this.counterpartPhoneNumber = protocol.getRecipient();
        // this.composerPhoneNumber 는 draft 단계에서는 필요없음 (myDB.getPrivatePhoneNumber임)
        this.createdDateTime = protocol.getCreatedDatetime();
        if (protocol.getUpdatedDatetime().isAfter(protocol.getCreatedDatetime())) {
            this.salesDateTime = protocol.getUpdatedDatetime();
        } else {
            this.salesDateTime = createdDateTime;
        }
        this.confirmed = false;
    }

    public List<String> getPaymentStrings(boolean pendingOnly) {
        List<String> results = new ArrayList<>();
        List<AccountLogType> payments = pendingOnly ? AccountLogType.getPendings(getPaymentTypes()) : getPaymentTypes();
        for (AccountLogType payment : payments)
            results.add(payment.uiStr);
        return results;
    }

    public String getLogId() {
        return APIGatewayHelper.getRemoteDateTimeString(createdDateTime);
    }

    public List<AccountLogType> getPaymentTypes() {
        List<AccountLogType> results = new ArrayList<>();
        if (onlinePayment == 0 && entrustPayment == 0) {
            results.add(AccountLogType.CashPaid);
        } else {
            if (onlinePayment != 0) {
                if (onlinePaid) {
                    results.add(AccountLogType.OnlinePaid);
                } else {
                    results.add(AccountLogType.Online);
                }
            }
            if (entrustPayment != 0) {
                if (entrustPaid) {
                    results.add(AccountLogType.EntrustPaid);
                } else {
                    results.add(AccountLogType.Entrust);
                }
            }
        }
        return results;
    }

    public boolean isPaid() {
        if (onlinePayment != 0 && !onlinePaid)
            return false;
        if (entrustPayment != 0 && !entrustPaid)
            return false;
        return true;
    }

    public SlipDataKey getSlipKey() {
        return new SlipDataKey(ownerPhoneNumber, createdDateTime);
    }

    public SlipRecord() {
    }

    public SlipRecord(int id, String ownerPhoneNumber, String counterpartPhoneNumber,
                      DateTime createdDateTime, DateTime salesDateTime, boolean confirmed,
                      int onlinePayment, boolean onlinePaid, String onlinePaidDateStr,
                      int entrustPayment, boolean entrustPaid, String entrustPaidDateStr,
                      SlipGenerationType generation, boolean completed, boolean cancelled,
                      DateTime cancelledDateTime, boolean mOnDeleting, int bill) {
        this.id = id;
        this.ownerPhoneNumber = ownerPhoneNumber;
        this.counterpartPhoneNumber = counterpartPhoneNumber;
        this.createdDateTime = createdDateTime;
        this.salesDateTime = salesDateTime;
        this.confirmed = confirmed;
        this.onlinePayment = onlinePayment;
        this.onlinePaid = onlinePaid;
        this.onlinePaidDateStr = onlinePaidDateStr;
        this.entrustPayment = entrustPayment;
        this.entrustPaid = entrustPaid;
        this.entrustPaidDateStr = entrustPaidDateStr;
        this.generation = generation;
        this.completed = completed;
        this.cancelled = cancelled;
        this.cancelledDateTime = cancelledDateTime;
        this.mOnDeleting = mOnDeleting;
        this.bill = bill;
    }

    public SlipRecord(String ownerPhoneNumber, String counterpartPhoneNumber) {
        this.ownerPhoneNumber = ownerPhoneNumber;
        this.counterpartPhoneNumber = counterpartPhoneNumber;
        this.createdDateTime = DateTime.now();
        this.salesDateTime = createdDateTime;
    }

    public SlipRecord(String ownerPhoneNumber, String counterpartPhoneNumber, DateTime createdDateTime) {
        this.ownerPhoneNumber = ownerPhoneNumber;
        this.counterpartPhoneNumber = counterpartPhoneNumber;
        this.createdDateTime = createdDateTime;
        this.salesDateTime = createdDateTime;
    }

    public boolean isSame(SlipRecord record) {
        return BaseRecord.isSame(this, record);
    }


    public boolean isMine(Context context) {
        return (ownerPhoneNumber.equals(getProviderNumber(context)));
    }

    public String getCounterpart(Context context) {
        if (isMine(context)) {
            return counterpartPhoneNumber;
        } else {
            return ownerPhoneNumber;
        }
    }

    public String getCounterpart(boolean isProvider) {
        if (isProvider) {
            return counterpartPhoneNumber;
        } else {
            return ownerPhoneNumber;
        }
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] <%d> %s, counter %s, created %s, sales %s, confirmed? %s, online %d (%s, %s), " +
                        "entrust %d (%s, %s), generation %s, completed %s, cancelled %s (%s), claimed %d",
                location, id, ownerPhoneNumber, counterpartPhoneNumber,
                createdDateTime.toString(BaseUtils.fullFormat), salesDateTime.toString(BaseUtils.fullFormat), confirmed,
                onlinePayment, onlinePaid, onlinePaidDateStr, entrustPayment, entrustPaid, entrustPaidDateStr, generation.toString(),
                completed, cancelled, cancelledDateTime.toString(BaseUtils.fullFormat), bill));
    }

    public void check() {
        if (id == 0)
            Log.w("DEBUG_JS", String.format("[SlipRecord.check] id == 0"));
    }

    @Override
    public int compareTo(@NonNull SlipRecord slipRecord) {
        switch (sortingKey) {
            default:
            case Date:
                return -salesDateTime.compareTo(slipRecord.salesDateTime);
        }
    }

    public List<SlipRecord> toList() {
        List<SlipRecord> results = new ArrayList<>();
        results.add(this);
        return results;
    }
}
