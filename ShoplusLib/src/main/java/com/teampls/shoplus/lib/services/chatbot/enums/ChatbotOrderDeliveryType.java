package com.teampls.shoplus.lib.services.chatbot.enums;

/**
 * 주문 배송 유형
 *
 * @author lucidite
 */
public enum ChatbotOrderDeliveryType {
    POST("p", "택배"),
    VISIT("v", "직접방문"),
    ENTRUSTED("e", "사입삼촌"),
    MISC("m", "기타");            // Fallback Type

    private String remoteStr;
    private String uiStr;

    ChatbotOrderDeliveryType(String remoteStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
    }

    public String getRemoteStr() {
        return remoteStr;
    }

    public String getUiStr() {
        return uiStr;
    }

    public static ChatbotOrderDeliveryType fromRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty()) {
            return MISC;
        }
        for (ChatbotOrderDeliveryType type: ChatbotOrderDeliveryType.values()) {
            if (type.remoteStr.equals(remoteStr)) {
                return type;
            }
        }
        return MISC;        // Fallback
    }
}
