package com.teampls.shoplus.lib.database;

import android.support.annotation.NonNull;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-08-27.
 */
// 관련 class : AccountsData
public class AccountRecord implements Comparable<AccountRecord> {
    public int id = 0;
    public String counterpartPhoneNumber = "", name = ""; // name : 중복이나 sorting을 위해
    public int online = 0, entrust = 0, deposit = 0;
    //
    public MSortingKey sortingKey;

    public AccountRecord() {
    }

    public AccountRecord(int id, String counterpartPhoneNumber, int online, int entrust, int deposit, String name) {
        this.id = id;
        this.counterpartPhoneNumber = counterpartPhoneNumber;
        this.online = online;
        this.entrust = entrust;
        this.deposit = deposit;
        this.name = name;
    }

    public AccountRecord(String counterpartPhoneNumber, AccountsData data, String name) {
        this.counterpartPhoneNumber = counterpartPhoneNumber;
        this.online = data.getOnlineAmount();
        this.entrust = data.getEntrustedAmount();
        this.deposit = data.getDepositAmount();
        this.name = name;
    }


    public AccountRecord(String counterpartPhoneNumber, AccountLogRecord logRecord, String name) {
        this.counterpartPhoneNumber = counterpartPhoneNumber;
        this.name = name;
        switch (logRecord.accountType) {
            case DepositAccount:
                break;
            case OnlineAccount:
                online = logRecord.account;
                entrust = logRecord.receivable - online;
                break;
            case EntrustedAccount:
                entrust = logRecord.account;
                online = logRecord.receivable - entrust;
                break;
        }
    }

    public AccountRecord clone() {
        return new AccountRecord(id, counterpartPhoneNumber, online, entrust, deposit, name);
    }

    public void update(AccountType accountType, AccountChangeLogDataProtocol protocol, boolean isCrossing) {
        switch (accountType) {
            case DepositAccount:
                this.deposit = protocol.getCurrentAmount();
                break;
            case EntrustedAccount:
                if (isCrossing)
                    this.online = protocol.getCurrentAmount();
                else
                    this.entrust = protocol.getCurrentAmount();
                break;
            case OnlineAccount:
                if (isCrossing)
                    this.entrust = protocol.getCurrentAmount();
                else
                    this.online = protocol.getCurrentAmount();
                break;
        }
    }

    public boolean isSame(AccountRecord record) {
        if (counterpartPhoneNumber.equals(record.counterpartPhoneNumber) == false) return false;
        if (online != record.online) return false;
        if (entrust != record.entrust) return false;
        if (deposit != record.deposit) return false;
        if (name.equals(record.name) == false) return false;
        return true;
    }

    public List<String> toStringList(String phoneNumber, boolean doShowReceivable) {
        List<String> results = new ArrayList<>();
        results.add(String.format("연락처 : %s", BaseUtils.toPhoneFormat(phoneNumber)));
        if (doShowReceivable) {
            results.add(String.format("잔금 : %s", BaseUtils.toCurrencyStr(getReceivable())));
        } else {
            results.add(String.format("온라인 잔금 : %s", BaseUtils.toCurrencyStr(online)));
            results.add(String.format("대  납 잔금 : %s", BaseUtils.toCurrencyStr(entrust)));
        }
        results.add(String.format("매입금 : %s", BaseUtils.toCurrencyStr(deposit)));
        return results;
    }

    public int getReceivable() {
        return online + entrust;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] %s, online %d, entrust %d, deposit %d, name %s ", location, counterpartPhoneNumber, online, entrust, deposit, name));
    }

    @Override
    public int compareTo(@NonNull AccountRecord record) {
        switch (sortingKey) {
            case Name:
                return name.compareTo(record.name);
            case Value:
                if (getReceivable() == record.getReceivable())
                    return 0;
                return -((Integer) getReceivable()).compareTo(record.getReceivable());
            case SecondValue:
                if (deposit == record.deposit)
                    return 0;
                return -((Integer) deposit).compareTo(record.deposit);
            default:
                return counterpartPhoneNumber.compareTo(record.counterpartPhoneNumber);
        }
    }
}
