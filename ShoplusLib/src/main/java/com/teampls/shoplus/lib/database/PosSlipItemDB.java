package com.teampls.shoplus.lib.database;

import android.content.Context;

/**
 * Created by Medivh on 2017-05-25.
 */

public class PosSlipItemDB extends SlipItemDB {
    private static PosSlipItemDB instance = null;

    public static PosSlipItemDB getInstance(Context context) {
        if (instance == null)
            instance = new PosSlipItemDB(context);
        return instance;
    }

    protected PosSlipItemDB(Context context) {
        super(context, "PosSlipItemDB", Ver, Column.toStrs());
    }

    protected PosSlipItemDB(Context context, String DBName, int DBversion, String[] columns) {
        super(context, DBName, DBversion, columns);
    }
}
