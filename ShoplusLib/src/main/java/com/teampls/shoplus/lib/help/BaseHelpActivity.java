package com.teampls.shoplus.lib.help;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.event.MyOnTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-01-03.
 */

abstract public class BaseHelpActivity extends Activity {
    // Base 0 ~ 99, My 100 ~
    public static final int COMMOM_LIMIT = 99, NOTIF_BATTERY = 0;
    private Context context = BaseHelpActivity.this;
    protected List<HelpRecord> records = new ArrayList<>(0);
    protected int recordIndex = 0;
    protected TextView tvTitle, tvDescription;
    protected ImageView image;
    protected static MyOnTask onFinish;
    protected static int helpCode = 0;

    abstract protected void createRecords(int helpCode);

    protected void add(String title, String description, int imageRes) {
        records.add(new HelpRecord(title, description, imageRes));
    }
    
    protected void getCommonRecords(int helpCode) {
        records.clear();
        switch (helpCode) {
            case NOTIF_BATTERY:
                // 자주 사용되지 않는 앱이 Doze 모드에 걸릴 수 있는데 우리 앱도 가능성이 높다
       //         records.addInteger(new HelpRecord("배터리 최적화로 이동", "배터리 최적화 상태면 알림을 못 받을 수 있습니다. 이동 후 '모든앱'을 클릭하고 앱을 찾아주세요", R.drawable.trouble_notif1));
     //           records.addInteger(new HelpRecord("설정변경 (최적화하지 않음)", "그림과 같이 변경해주세요. 다음을 클릭하시면 최적화 페이지로 이동하겠습니다", R.drawable.trouble_notif2));
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (BaseAppWatcher.checkInstance(this) == false) return;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_help);
        getActionBar().hide();
        tvTitle = (TextView) findViewById(R.id.help_title);
        tvDescription = (TextView) findViewById(R.id.help_description);
        image = (ImageView) findViewById(R.id.help_image);
        createRecords(helpCode);
        showHelpPage(recordIndex);
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE) && BasePreference.landScapeOrientationEnabled.get(context))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    private void showHelpPage(int index) {
        if (index < 0 || index > records.size() -1) return;
        tvTitle.setText(String.format("[%d/%d] %s", index+1, records.size(), records.get(index).title));
        tvDescription.setText(records.get(index).description);
        image.setImageResource(records.get(index).resId);
    }

    public void onPrevClick(View view) {
        if (recordIndex == 0) {
            finish();
            return;
        }
        recordIndex = recordIndex-1;
        showHelpPage(recordIndex);
    }

    public void onNextClick(View view) {
        if (recordIndex == records.size()-1) {
            if (onFinish != null)
                onFinish.onTaskDone(null);
            finish();
            return;
        }
        recordIndex = Math.min(records.size()-1, recordIndex+1);
        showHelpPage(recordIndex);
    }
}
