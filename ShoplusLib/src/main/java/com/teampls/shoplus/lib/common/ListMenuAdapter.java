package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2019-05-23.
 */

public class ListMenuAdapter extends BaseDBAdapter<ListMenuRecord> {

    public ListMenuAdapter(Context context) {
        super(context);
    }

    @Override
    public void generate() {

    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new BaseViewHolder() {
            private ImageView ivIcon;
            private TextView tvTitle, tvDescription;

            @Override
            public int getThisView() {
                return R.layout.row_list_menu;
            }

            @Override
            public void init(View view) {
                ivIcon = view.findViewById(R.id.row_list_menu_imageView);
                tvTitle = findTextViewById(context, view, R.id.row_list_menu_title);
                tvDescription = findTextViewById(context, view, R.id.row_list_menu_description);
                MyView.setImageViewSizeByDeviceSize(context, ivIcon);
            }

            @Override
            public void update(int position) {
                ListMenuRecord record = records.get(position);
                if (record.imageResId != 0)
                    ivIcon.setImageDrawable(context.getResources().getDrawable(record.imageResId));
                tvTitle.setText(record.title);
                tvDescription.setText(record.description);
            }
        };
    }
}
