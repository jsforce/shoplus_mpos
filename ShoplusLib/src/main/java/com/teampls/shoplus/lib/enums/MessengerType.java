package com.teampls.shoplus.lib.enums;

/**
 * 설정에 포함된 사용자의 메신저 연결 정보 유형
 *
 * @author lucidite
 */
public enum MessengerType {
    NOT_DEFINED("","", ""),
    KAKAOTALK("KT","카톡", "KakaoTalk"),
    PLUSFRIEND("PF","플친", "PlusFriend"),
    WECHAT("WC","위챗","WeChat");

    private String remoteStr;
    public String uiStr = "", engStr = "";

    MessengerType(String remoteStr, String uiStr, String engStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
        this.engStr = engStr;
    }

    public static MessengerType ofRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty()) {
            return  NOT_DEFINED;
        }
        for (MessengerType type: MessengerType.values()) {
            if (type.remoteStr.equals(remoteStr)) {
                return type;
            }
        }
        return  NOT_DEFINED;
    }

    public static MessengerType valueOfDefault(String name) {
        try {
            return valueOf(name);
        } catch (Exception e) {
            return NOT_DEFINED;
        }
    }



    public String toRemoteStr() {
        return this.remoteStr;
    }
}
