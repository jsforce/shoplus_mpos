package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.enums.ContactBuyerType;

/**
 * (거래처의) 연락처 정보 데이터 프로토콜
 *
 * @author lucidite
 */

public interface ContactDataProtocol {

    /**
     * [SILVER-441] 거래처 번호를 반환한다.
     * 전화번호로 등록한 거래처의 경우 전화번호, 임시거래처인 경우 임시거래처 번호이다.
     * (숫자 문자열이어야 하며, 거래처의 ID로 사용되는 유일한 값이어야 한다.)
     *
     * @return
     */
    String getContactNumber();

    /**
     * 거래처 상호 문자열을 반환한다.
     * @return
     */
    String getName();

    /**
     * [SILVER-423, SILVER-424] 도매/소매 거래처 유형 (기본값 = 도매)
     * @return
     */
    ContactBuyerType getBuyerType();

    /**
     * [SILVER-441] 최종 업데이트된 시간,
     * - SILVER-441 적용 전 거래처에서는 0을 반환한다.
     * - Debug 및 참고용으로 현재 사용자 용도로 고려하지 않고 있다. (업데이트를 정상 다운로드하는지 디버깅하기 위함)
     * - "서버 기준"의 타임스탬프로 업데이트 다운로드의 기준이다. (로컬에서 생성 또는 변경할 수 없는 Read-only 정보임)
     *
     * @return
     */
    Long getUpdatedTimeStamp();

    /**
     * [SILVER-441] 삭제된 거래처 여부를 반환한다.
     * - 사용 중인 거래처인 경우 False, 다른 거래처에 통합된 경우 True를 반환한다.
     * - SILVER-441에서 업데이트 기반 관리 방식을 채택하면서 merge된 거래처를 업데이트에서 확인해 주어야 한다.
     *
     * @return
     */
    boolean isDeleted();

    /**
     * 거래처 지역/위치 문자열
     * @return
     */
    String getLocation();

    /**
     * [SILVER-544] 가격차별화 여부를 반환한다(고객별 가격 설정 여부).
     *
     * @return
     */
    boolean isPriceDiscriminated();
}
