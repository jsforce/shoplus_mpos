package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.CheckableStringAdapter;
import com.teampls.shoplus.lib.view.MyView;

import java.util.List;

/**
 * Created by Medivh on 2018-03-03.
 */

abstract public class MyCheckableListDialogOld extends BaseDialog  implements AdapterView.OnItemClickListener {
    private ListView listView;
    private CheckableStringAdapter adapter;

    public MyCheckableListDialogOld(Context context, String title, String message, List<String> records, String currentRecord) {
        super(context);
        setDialogWidth(0.9, 0.6);
        MyView.setImageViewSizeByDeviceSize(context, (ImageView) findViewById(R.id.dialog_list_close));

        findTextViewById(R.id.dialog_list_title, title);
        findTextViewById(R.id.dialog_list_message, message);
        if (message.isEmpty())
            setVisibility(View.GONE, R.id.dialog_list_message);

        int position = 0, selectedPosition = 0;
        for (String record : records) {
            if (record.equals(currentRecord)) {
                selectedPosition = position;
                break;
            }
            position++;
        }

        adapter = new CheckableStringAdapter(context);
        adapter.setRecords(records);
        adapter.setClickedPosition(selectedPosition);

        listView = (ListView) findViewById(R.id.dialog_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);

        setOnClick(R.id.dialog_list_close);
        show();

    }

    @Override
    public int getThisView() {
        return R.layout.dialog_list;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_list_close) {
            dismiss();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        onItemClick(position, adapter.getRecord(position));
    }

    protected abstract void onItemClick(int position, String selectedRecord);
}
