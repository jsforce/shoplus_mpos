package com.teampls.shoplus.lib.database;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.database_old.LocalSlipDB;
import com.teampls.shoplus.lib.database_old.LocalSlipRecord;
import com.teampls.shoplus.lib.database_old.OldPosSlipDB;
import com.teampls.shoplus.lib.database_old.OldSlipRecord;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-05-22.
 */

public class PosSlipDB extends SlipDB {
    private static PosSlipDB instance = null;
    public PosAndSourceLinkDB posAndSourceLink; // 원래 관리중은 SlipDB의 items

    public static PosSlipDB getInstance(Context context) {
        if (instance == null)
            instance = new PosSlipDB(context);
        return instance;
    }

    protected PosSlipDB(Context context, String dbName) {
        super(context, dbName, Ver, Column.toStrs());
    }

    protected PosSlipDB(Context context) {
        super(context, "PosSlipDB7", Ver, Column.toStrs()); // QrSlipDB 이름도 챙겨야 함
        items = PosSlipItemDB.getInstance(context);
        posAndSourceLink = PosAndSourceLinkDB.getInstance(context);
        // 미송발송처럼 미완료 리스트를 조작해서 생겨난 item이 원래 어디랑 연결되어야 하는지를 기록하는 DB
        // 외부 SlipItem의 항목을 가지고 있다
        if (KeyValueDB.getInstance(context).getBool(OldPosSlipDB.getInstance(context).DB_NAME) == false)
            recoverFromOldDB();
    }

    protected PosSlipDB(Context context, String DBName, int DBversion, String[] columns) {
        super(context, DBName, DBversion, columns);
    }

    public void recoverFromOldDB() {
        for (OldSlipRecord oldSlipRecord : OldPosSlipDB.getInstance(context).getRecords()) {
            LocalSlipRecord localSlipRecord = LocalSlipDB.getInstance(context).getRecordByKey(oldSlipRecord.getSlipKey().toSID());
            SlipRecord newSlipRecord = oldSlipRecord.toSlipRecord(localSlipRecord.receivable);
            updateOrInsert(newSlipRecord);
        }
        KeyValueDB.getInstance(context).put(OldPosSlipDB.getInstance(context).DB_NAME, true);
    }

    public void recoverClaimedReceivableFromOldDB() {
        for (SlipRecord slipRecord : getRecords()) {
            if (slipRecord.bill == 0) {
                LocalSlipRecord localSlipRecord = LocalSlipDB.getInstance(context).getRecordByKey(slipRecord.getSlipKey().toSID());
                slipRecord.bill = localSlipRecord.receivable;
                update(slipRecord.id, slipRecord);
            }
        }
    }

    @Override
    public void deleteDB() {
        if (posAndSourceLink != null) posAndSourceLink.deleteDB();
        super.deleteDB();
    }

    @Override
    public void clear() {
        if (posAndSourceLink != null) posAndSourceLink.clear();
        super.clear();
    }

    public DBResult updateOrInsertItemFromSource(SlipRecord record, SlipItemRecord sourceSlipItemRecord) {
        // source로 부터 slipItemRecord를 만들어 추가
        // sourceSlipItemRecord : 미송앱에서는 현재 챙길상품, 카탈로그에서는 미송영수증 상품
        if (posAndSourceLink.hasSource(sourceSlipItemRecord.id)) { // id 가지고만 비교하므로 주의 (중간에 삭제 등으로 변경되는 경우)
            // source가 변경된 시간 저장 (지금은 무시해도 됨)
            PosAndSourceLinkRecord existingSourceSlipItemRecord = posAndSourceLink.getRecordBySource(sourceSlipItemRecord.id);
            existingSourceSlipItemRecord.updateDateTime = DateTime.now();
            posAndSourceLink.update(existingSourceSlipItemRecord.id, existingSourceSlipItemRecord);

            // item의 type만 수정
            SlipItemRecord posSlipItemRecord = items.getRecord(existingSourceSlipItemRecord.posSlipItemDbId);
            posSlipItemRecord.slipItemType = sourceSlipItemRecord.slipItemType;
            return items.update(posSlipItemRecord.id, posSlipItemRecord);
        } else {
            // insertAll
            SlipItemRecord posSlipItemRecord = sourceSlipItemRecord.clone();
            posSlipItemRecord.slipKey = record.getSlipKey().toSID();
            posSlipItemRecord.serial = items.getNextSerial(posSlipItemRecord.slipKey);    //   sourceSlipItemRecord.slipKey -- 새로 입력하는 레코드의 serial을 source에서 찾는다?
            DBResult result = items.insert(posSlipItemRecord);
            posAndSourceLink.insert(new PosAndSourceLinkRecord(result.id, sourceSlipItemRecord.id, record.counterpartPhoneNumber));
            // (pos쪽 id, source쪽 id, 거래처 id)
            return result;
        }
    }

    public void deleteItem(int posSlipItemDbId) {
        items.deleteAndRefresh(posSlipItemDbId);
        if (posAndSourceLink.has(posSlipItemDbId))
            posAndSourceLink.deleteBy(posSlipItemDbId);
    }

    @Override
    public void deleteFromDBs(SlipDataKey slipKey) {
        if (has(slipKey) == false) {
            Log.w("DEBUG_JS", String.format("[SlipDB.deleteFromDBs] %s not found", slipKey.toSID()));
            return;
        }
        deleteLink(slipKey);
        super.deleteFromDBs(slipKey);
    }

    public void deleteLink(SlipDataKey slipKey) {
        for (SlipItemRecord posSlipItemRecord : items.getRecordsBy(slipKey.toSID()))
            posAndSourceLink.deleteBy(posSlipItemRecord.id);
    }

    public void checkLinkIntegrity() {
        posAndSourceLink.checkIntegrity(items);
    }

    public void mergeItems(String counterpartPhoneNumber, SlipFullRecord fullRecord) {
        SlipRecord currentSlipRecord = getUnconfirmedRecord(counterpartPhoneNumber);
        if (currentSlipRecord.id <= 0) {
            Log.e("DEBUG_JS", String.format("[PosSlipDB.mergeItems] %s has NOT unconfirmed record"));
            insert(fullRecord);
            return;
        } else {
            SlipFullRecord dbFullRecord = getFullRecord(currentSlipRecord.id);
            dbFullRecord.items.addAll(fullRecord.items);
            int serial = 0;
            for (SlipItemRecord itemRecord : dbFullRecord.items) {
                itemRecord.slipKey = dbFullRecord.record.getSlipKey().toSID(); // slip key 일치
                itemRecord.serial = serial;
                serial++;
            }
            updateOrInsertFast(dbFullRecord);
        }
    }

    public void refreshConfirmeds(List<SlipDataProtocol> protocols) {
        // delete all confirmed records
        List<String> slipKeys = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();
        for (SlipRecord record : getRecords(Column.confirmed, true)) {
            ids.add(record.id);
            slipKeys.add(record.getSlipKey().toSID());
        }
        deleteAll(Column._id, ids);

        items.deleteAll(SlipItemDB.Column.slipKey, slipKeys);
        serviceLinks.deleteAll(ServiceLinkDB.Column.key, slipKeys);

        beginTransaction();
        items.beginTransaction();
        for (SlipDataProtocol protocol : protocols) {
            if (protocol.isCancelled() == false)
                insert(new SlipFullRecord(protocol));
        }
        items.endTransaction();
        endTransaction();
    }

}
