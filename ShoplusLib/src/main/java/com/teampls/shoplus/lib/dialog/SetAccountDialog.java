package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MAccountDB;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.enums.MemberPermission;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2018-11-11.
 */
// AddCountDialog 와 자매품
public class SetAccountDialog extends BaseDialog {
    private MyCurrencyEditText editText;
    private AccountRecord accountRecord = new AccountRecord();
    private MyOnTask onTask;
    private AccountType accountType = AccountType.OnlineAccount;

    public SetAccountDialog(Context context, UserRecord counterpart, AccountType accountType, final MyOnTask onTask) {
        super(context);
        this.accountType = accountType;
        setDialogWidth(0.9, 0.7);
        this.onTask = onTask;
        accountRecord = MAccountDB.getInstance(context).getRecordByKey(counterpart.phoneNumber);
        findTextViewById(R.id.dialog_set_account_title, getTitle(" 수정"));
        findTextViewById(R.id.dialog_set_account_message, String.format("%s : %s", getTitle(""), BaseUtils.toCurrencyStr(getAccountValue())));
        editText = new MyCurrencyEditText(context, getView(), R.id.dialog_set_account_value, R.id.dialog_set_account_clear_value);
        setOnClick(R.id.dialog_set_account_byValue, R.id.dialog_set_account_toZero, R.id.dialog_set_account_cancel);
        show();
        MyDevice.showKeyboard(context, editText.getEditText());
    }

    private String getTitle(String suffix) {
        switch (accountType) {
            default:
            case EntrustedAccount:
            case OnlineAccount:
                return "잔금" + suffix;
            case DepositAccount:
                return "매입금" + suffix;
        }
    }

    private int getAccountValue() {
        switch (accountType) {
            default:
            case EntrustedAccount:
            case OnlineAccount:
                return accountRecord.getReceivable();
            case DepositAccount:
                return accountRecord.deposit;
        }
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_set_account;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_set_account_byValue) {
            new MyAlertDialog(context, getTitle(" 수정"), String.format("%s로 수정하시겠습니까?", BaseUtils.toCurrencyStr(editText.getValue()))) {
                @Override
                public void yes() {
                    updateAccountLog(accountType, editText.getValue(), onTask);
                }
            };

        } else if (view.getId() == R.id.dialog_set_account_toZero) {
            new MyAlertDialog(context, getTitle(" 수정"), String.format("0원으로 수정하시겠습니까?")) {
                @Override
                public void yes() {
                    updateAccountLog(accountType, 0, onTask);
                }
            };

        } else if (view.getId() == R.id.dialog_set_account_cancel) {
            MyDevice.hideKeyboard(context, editText.getEditText());
            dismiss();
        }
    }

    private void updateAccountLog(final AccountType accountType, final int targetValue, final MyOnTask onTask) {

        // 권한체크
        switch (accountType) {
            default:
                if (MemberPermission.ChangeOnline.hasPermission(userSettingData) == false) {
                    MemberPermission.ChangeOnline.toast(context);
                    return;
                }

                if (MemberPermission.ChangeEntrusted.hasPermission(userSettingData) == false) {
                    MemberPermission.ChangeEntrusted.toast(context);
                    return;
                }
                break;
            case DepositAccount:
                if (MemberPermission.ChangeDeposit.hasPermission(userSettingData) == false) {
                    MemberPermission.ChangeDeposit.toast(context);
                    return;
                }
                break;
        }

        new CommonServiceTask(context, "SetAccountLogDialog", "수정중....") {
            boolean changed = false;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                // to zero
                AccountsData data = new AccountsData(0, 0, 0);

                // 해당 계정으로 0으로 초기화
                switch (accountType) {
                    default:
                        if (accountRecord.online != 0) {
                            data = accountService.changeAccountAmount(userSettingData.getUserShopInfo(),
                                    accountRecord.counterpartPhoneNumber, AccountType.OnlineAccount, -accountRecord.online, DateTime.now());
                            changed = true;
                        }

                        if (accountRecord.entrust != 0) {
                            data = accountService.changeAccountAmount(userSettingData.getUserShopInfo(),
                                    accountRecord.counterpartPhoneNumber, AccountType.EntrustedAccount, -accountRecord.entrust, DateTime.now());
                            changed = true;
                        }

                        break;

                    case DepositAccount:
                        if (accountRecord.deposit != 0) {
                            data = accountService.changeAccountAmount(userSettingData.getUserShopInfo(),
                                    accountRecord.counterpartPhoneNumber, AccountType.DepositAccount, -accountRecord.deposit, DateTime.now());
                            changed = true;
                        }
                        break;
                }

                // 목표값으로 변경 (잔금은 online 계정으로)
                if (targetValue != 0) {
                    data = accountService.changeAccountAmount(userSettingData.getUserShopInfo(),
                            accountRecord.counterpartPhoneNumber, accountType, targetValue, DateTime.now());
                    changed = true;
                }


                if (changed)
                    MAccountDB.getInstance(context).update(new AccountRecord(accountRecord.counterpartPhoneNumber, data, accountRecord.name));
            }

            @Override
            public void onPostExecutionUI() {
                dismiss();
                MyDevice.hideKeyboard(context, editText.getEditText());
                MyUI.toastSHORT(context, String.format("수정 완료"));
                if (onTask != null)
                    onTask.onTaskDone("");
            }

        };
    }
}