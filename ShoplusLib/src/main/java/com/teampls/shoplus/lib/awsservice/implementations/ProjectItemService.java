package com.teampls.shoplus.lib.awsservice.implementations;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;

import com.amazonaws.AmazonClientException;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ProjectItemServiceProtocol;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.ItemJSONData;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.SlipItemJSONData;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.awsservice.s3.ImageDownloader;
import com.teampls.shoplus.lib.awsservice.s3.ImageUploadType;
import com.teampls.shoplus.lib.awsservice.s3.ImageUploader;
import com.teampls.shoplus.lib.datatypes.ItemCostPriceBundle;
import com.teampls.shoplus.lib.datatypes.ItemDataBundle;
import com.teampls.shoplus.lib.datatypes.ItemOptionData;
import com.teampls.shoplus.lib.datatypes.ItemSearchKey;
import com.teampls.shoplus.lib.datatypes.ItemStockChangeLogBundleData;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.datatypes.MerchandiseData;
import com.teampls.shoplus.lib.datatypes.SimpleItemData;
import com.teampls.shoplus.lib.datatypes.SpecificItemDataBundle;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.protocol.ImageDataProtocol;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;
import com.teampls.shoplus.lib.protocol.SimpleItemDataProtocol;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 아이템 관련 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectItemService implements ProjectItemServiceProtocol {

    private String getMyItemsResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items");
    }

    private String getAnItemResourcePath(String phoneNumber, int itemid) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", String.valueOf(itemid));
    }

    private String getAnItemResourcePath(String phoneNumber, String itemSerialNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", itemSerialNumber);
    }

    private String getMonthlyItemListResourcePath(String phoneNumber, DateTime month) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "list", "period", APIGatewayHelper.getRemoteMonthString(month));
    }

    private String getMyItemsSearchResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "search");
    }

    private String getMyItemCategoryResourcePath(String phoneNumber, ItemCategoryType category) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "categories", category.toRemoteStr());
    }

    private String getItemStockResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock");
    }

    private String getAnItemStockOptionResourcePath(String phoneNumber, ItemStockKey itemStockKey) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock", itemStockKey.toAPIParamStr());
    }

    private String getAnItemStockOptionStateResourcePath(String phoneNumber, ItemStockKey itemStockKey) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock", itemStockKey.toAPIParamStr(), "state");
    }

    private String getMyStockAdjustmentResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock", "adjust");
    }

    private String getCostPriceOfAnItemResourcePath(String phoneNumber, int itemid) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", String.valueOf(itemid), "cost-price");
    }

    private String getItemCostPriceResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "cost-price");
    }

    private String getAMerchandiseResourcePath(String phoneNumber, String provider, int itemid) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "merchandise", provider, String.valueOf(itemid));
    }

    private String getItemBookmarksResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "bookmarks");
    }

    private String getAnItemInItemBookmarksResourcePath(String phoneNumber, int itemid) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "bookmarks", String.valueOf(itemid));
    }

    private String getPhoneNumberForItems(UserShopInfoProtocol userShop) {
        return userShop.getMainShopOrMyShopOrUserPhoneNumber();     // Items 정보에서는 메인 샵 정보를 사용한다.
    }

    @Override
    public List<ItemDataProtocol> makeItems(UserShopInfoProtocol userShop, List<String> mainImageLocalPaths) throws MyServiceFailureException {
        try {
            String resource = this.getMyItemsResourcePath(getPhoneNumberForItems(userShop));

            // 요청 데이터는 이미지 주소의 목록이다.
            JSONArray requestBody = new JSONArray();
            ImageUploader uploader = new ImageUploader(
                    UserHelper.getCredentialsProvider().getCredentials(), getPhoneNumberForItems(userShop),
                    MyAWSConfigs.getCurrentConfig().getS3ItemsBucketName()
            );
            List<String> remoteImagePaths = uploader.upload(
                    ImageUploadType.ITEM_MAIN_IMAGE, DateTime.now(), mainImageLocalPaths
            );

            for (String remoteImagePath: remoteImagePaths) {
                // 향후 확장 가능성을 고려하여, 이미지 주소 외의 필드도 처리할 수 있도록 JSON 객체의 목록으로 처리하여 업로드한다.
                JSONObject itemDataDict = new JSONObject();
                itemDataDict.put("main_image", remoteImagePath);
                requestBody.put(itemDataDict);
            }

            // 응답 데이터는 Item Data의 JSON 배열 타입 문자열이다.
            String response = APIGatewayClient.requestPOST(resource, requestBody.toString(), UserHelper.getIdentityToken());
            JSONArray dataArr = new JSONArray(response);
            List<ItemDataProtocol> result = new ArrayList<>();
            for (int i = 0; i < dataArr.length(); ++i) {
                result.add(new ItemJSONData(dataArr.getJSONObject(i)));
            }
            return result;
        } catch (AmazonClientException | JSONException | MyServiceFailureException e) {
            e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public ItemDataProtocol makeItemWithoutImage(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getMyItemsResourcePath(getPhoneNumberForItems(userShop));

            // 요청 데이터는 이미지 주소의 목록이다.
            JSONArray requestBody = new JSONArray().put(
                    new JSONObject().put("main_image", "no-image.jpg")
            );

            // 응답 데이터는 Item Data의 JSON 배열 타입 문자열이다. 길이는 1이어야 한다.
            String response = APIGatewayClient.requestPOST(resource, requestBody.toString(), UserHelper.getIdentityToken());
            JSONArray dataArr = new JSONArray(response);
            return new ItemJSONData(dataArr.getJSONObject(0));
        } catch (AmazonClientException | JSONException | MyServiceFailureException e) {
            e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public ItemDataProtocol updateItem(UserShopInfoProtocol userShop, ItemDataProtocol original, ItemDataProtocol updated) throws MyServiceFailureException {
        try {
            JSONObject diff = this.diffItems(original, updated);
            if (diff == null) {
                Log.i("DEBUG_JS", String.format("[ProjectItemService.updateItem] diff == null"));
                return original;        // ID가 일치하지 않거나, 비교하는 필드에 변경점이 없는 경우
            }

            String resource = this.getAnItemResourcePath(getPhoneNumberForItems(userShop), original.getItemId());
            String response = APIGatewayClient.requestPUT(resource, diff.toString(), UserHelper.getIdentityToken());
            JSONObject dataObj = new JSONObject(response);
            return new ItemJSONData(dataObj);
        } catch (JSONException | MyServiceFailureException e) {
            e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void deleteItem(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException {
        try {
            String resource = this.getAnItemResourcePath(getPhoneNumberForItems(userShop), itemid);
            APIGatewayClient.requestDELETE(resource, null, UserHelper.getIdentityToken());
        } catch (AmazonClientException | MyServiceFailureException e) {
            e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public Map<ItemStockKey, Integer> getItemStock(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getItemStockResourcePath(getPhoneNumberForItems(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            JSONObject dataObj = new JSONObject(response);

            Map<ItemStockKey, Integer> result = new HashMap<>();
            Iterator<String> iter = dataObj.keys();
            while (iter.hasNext()) {
                String itemIdStr = iter.next();
                Integer itemid = Integer.valueOf(itemIdStr);
                JSONArray stockArr = dataObj.getJSONArray(itemIdStr);
                for (int i = 0; i < stockArr.length(); ++i) {
                    JSONObject stockObj = stockArr.getJSONObject(i);
                    ColorType color = ColorType.ofRemoteStr(stockObj.optString("c", ""));
                    SizeType size = SizeType.ofRemoteStr(stockObj.optString("s", ""));

                    ItemStockKey option = new ItemStockKey(itemid.intValue(), color, size);
                    int remains = stockObj.optInt("r", 0);

                    result.put(option, Integer.valueOf(remains));
                }
            }
            return result;
        } catch (JSONException | MyServiceFailureException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void updateItemStock(UserShopInfoProtocol userShop, Map<ItemStockKey, Integer> remains) throws MyServiceFailureException {
        try {
            String resource = this.getItemStockResourcePath(getPhoneNumberForItems(userShop));

            JSONObject requestBody = new JSONObject();
            JSONArray dataArr = new JSONArray();
            for (Map.Entry<ItemStockKey, Integer> r: remains.entrySet()) {
                JSONObject remainsObj = new JSONObject();
                remainsObj.put("id", r.getKey().getItemId());
                remainsObj.put("color", r.getKey().getColorOption().toRemoteStr());
                remainsObj.put("size", r.getKey().getSizeOption().toRemoteStr());
                remainsObj.put("remains", r.getValue().intValue());
                dataArr.put(remainsObj);
            }
            requestBody.put("stock", dataArr);

            Log.i("DEBUG_TAG",requestBody.toString());

            // TODO 현재는 응답에 별다른 데이터가 포함되어 있지 않다. 최신 재고 데이터와 같은 것을 바로 받아와야 하는가?
            APIGatewayClient.requestPOST(resource, requestBody.toString(), UserHelper.getIdentityToken());
        } catch (JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void deleteItemStock(UserShopInfoProtocol userShop, ItemStockKey optionToBeDeleted) throws MyServiceFailureException {
        String resource = this.getAnItemStockOptionResourcePath(getPhoneNumberForItems(userShop), optionToBeDeleted);
        APIGatewayClient.requestDELETE(resource, null, UserHelper.getIdentityToken());
    }

    @Override
    public void updateItemOptionState(UserShopInfoProtocol userShop, ItemStockKey itemOption, boolean isSoldOut) throws MyServiceFailureException {
        try {
            String resource = this.getAnItemStockOptionStateResourcePath(getPhoneNumberForItems(userShop), itemOption);
            JSONObject requestBody = new JSONObject().put("soldout", isSoldOut);
            String result = APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
            Log.i("DEBUG_JS", String.format("[ProjectItemService.updateItemOptionState] request %s", requestBody.toString()));
            Log.i("DEBUG_JS", String.format("[ProjectItemService.updateItemOptionState] result %s", result));
        } catch (AmazonClientException | JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public Map<ItemStockKey, ItemOptionData> adjustStock(UserShopInfoProtocol userShop, List<SlipItemDataProtocol> transactionItems, Boolean isCancellation, int firstItemId) throws MyServiceFailureException {
        try {
            String resource = this.getMyStockAdjustmentResourcePath(getPhoneNumberForItems(userShop));
            JSONObject requestBody = new JSONObject();
            JSONArray transactionItemArray = new JSONArray();
            for (SlipItemDataProtocol item: transactionItems) {
                transactionItemArray.put(new SlipItemJSONData(item).getObject());
            }
            requestBody.put("transaction_items", transactionItemArray);
            requestBody.put("check_from", firstItemId);
            if (isCancellation) {
                requestBody.put("adjustment", "cancellation");
            }

            String response = APIGatewayClient.requestPOST(resource, requestBody.toString(), UserHelper.getIdentityToken());

            JSONArray stockArr = new JSONArray(response);
            return ItemOptionData.buildStockFromStockDataArr(stockArr);
        } catch (AmazonClientException | JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    private ItemDataBundle getItemsWithStock(UserShopInfoProtocol userShop, Map<String, String> queryParams) throws MyServiceFailureException {
        try {
            String resource = this.getMyItemsResourcePath(getPhoneNumberForItems(userShop));
            String response = APIGatewayClient.requestGET(resource, queryParams, UserHelper.getIdentityToken());
            JSONObject dataObj = new JSONObject(response);
            return new ItemDataBundle(dataObj);
        } catch (AmazonClientException | JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public ItemDataBundle getItemsWithStock(UserShopInfoProtocol userShop, Boolean isMPOS, Boolean isExcludingSoldout) throws MyServiceFailureException {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("mpos", isMPOS.toString());
        queryParams.put("excsoldout", isExcludingSoldout.toString());
        return this.getItemsWithStock(userShop, queryParams);
    }

    @Override
    public ItemDataBundle getRangedItemsWithStock(UserShopInfoProtocol userShop, int startItemId, int endItemId, Boolean isExcludingSoldout) throws MyServiceFailureException {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("from", String.valueOf(startItemId));
        queryParams.put("to", String.valueOf(endItemId));
        queryParams.put("excsoldout", isExcludingSoldout.toString());
        return this.getItemsWithStock(userShop, queryParams);
    }

    @Override
    public List<SimpleItemDataProtocol> getMontlyItemList(UserShopInfoProtocol userShop, DateTime month) throws MyServiceFailureException {
        return this.getMontlyItemList(userShop, month, null);
    }

    @Override
    public List<SimpleItemDataProtocol> getMontlySoldoutItemList(UserShopInfoProtocol userShop, DateTime month) throws MyServiceFailureException {
        return this.getMontlyItemList(userShop, month, ItemStateType.SOLDOUT);
    }

    private List<SimpleItemDataProtocol> getMontlyItemList(UserShopInfoProtocol userShop, DateTime month, ItemStateType state) throws MyServiceFailureException {
        try {
            String resource = this.getMonthlyItemListResourcePath(getPhoneNumberForItems(userShop), month);
            Map<String, String> params = new HashMap<>();
            if (state != null) {
                params.put("state", state.toRemoteStr());
            }
            String response = APIGatewayClient.requestGET(resource, params, UserHelper.getIdentityToken());
            JSONArray dataArr = new JSONObject(response).getJSONArray("item-list");
            List<SimpleItemDataProtocol> result = new ArrayList<>();
            for (int i = 0; i < dataArr.length(); ++i) {
                result.add(new SimpleItemData(dataArr.getJSONObject(i)));
            }
            return result;
        } catch (AmazonClientException | JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public SpecificItemDataBundle getSpecificItem(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException {
        try {
            String resource = this.getAnItemResourcePath(getPhoneNumberForItems(userShop), itemid);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            return new SpecificItemDataBundle(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public SpecificItemDataBundle getSpecificItem(UserShopInfoProtocol userShop, String itemSerialNumber) throws MyServiceFailureException {
        try {
            String resource = this.getAnItemResourcePath(getPhoneNumberForItems(userShop), itemSerialNumber);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            return new SpecificItemDataBundle(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public ItemDataBundle searchItems(UserShopInfoProtocol userShop, String itemNameSearchWord) throws MyServiceFailureException {
        String response = "";
        try {
            String percentEncodedSearchWord = Uri.encode(itemNameSearchWord);
            String resource = this.getMyItemsSearchResourcePath(getPhoneNumberForItems(userShop));
            Map<String, String> queryParams = new HashMap<>();
            queryParams.put("name", percentEncodedSearchWord);

            response = APIGatewayClient.requestGET(resource, queryParams, UserHelper.getIdentityToken());
            JSONObject dataObj = new JSONObject(response);
            return new ItemDataBundle(dataObj);
        } catch (AmazonClientException | JSONException e) {
            // e.printStackTrace();        // DEBUG
           // Log.e("DEBUG_JS", String.format("[ProjectCommonService.searchItems] %s", response));
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public Pair<ItemDataBundle, ItemSearchKey> getItemsOfCategory(UserShopInfoProtocol userShop, ItemCategoryType category, ItemSearchKey searchKey) throws MyServiceFailureException {
        try {
            String resource = this.getMyItemCategoryResourcePath(getPhoneNumberForItems(userShop), category);
            //   Log.i("DEBUG_JS", String.format("[ProjectCommonService.getItemsOfCategory] %s", resource));
            //  Log.i("DEBUG_JS", String.format("[ProjectCommonService.getItemsOfCategory] %s", searchKey.getQueryParams()));
            String response = APIGatewayClient.requestGET(resource, searchKey.getQueryParams(), UserHelper.getIdentityToken());
            JSONObject dataObj = new JSONObject(response);
            JSONObject searchKeyObj = dataObj.getJSONObject("search_key");
            return new Pair<>(new ItemDataBundle(dataObj), new ItemSearchKey(searchKeyObj));
        } catch (AmazonClientException | JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public ImageDataProtocol downloadItemImage(String remoteImagePath) throws MyServiceFailureException {
        return this.downloadItemImageFrom(MyAWSConfigs.getCurrentConfig().getS3ItemsBucketName(), remoteImagePath);
    }

    @Override
    public ImageDataProtocol downloadItemThumbnail(String remoteImagePath) throws MyServiceFailureException {
        return this.downloadItemImageFrom(MyAWSConfigs.getCurrentConfig().getS3ItemThumbnailsBucketName(), remoteImagePath);
    }

    @Override
    public ImageDataProtocol downloadMainImage(ItemDataProtocol item) throws MyServiceFailureException {
        return this.downloadItemImage(item.getMainImagePath());
    }

    @Override
    public ImageDataProtocol downloadMainThumbnail(ItemDataProtocol item) throws MyServiceFailureException {
        return this.downloadItemThumbnail(item.getMainImagePath());
    }

    @Override
    public MerchandiseData getMerchandiseData(UserShopInfoProtocol userShop, String provider, int itemid) throws MyServiceFailureException {
        try {
            String resource = this.getAMerchandiseResourcePath(getPhoneNumberForItems(userShop), provider, itemid);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            JSONObject merchandiseObj = new JSONObject(response);
            String merchandiseName = merchandiseObj.optString("item_name", "");
            String mainImagePath = merchandiseObj.optString("main_image", "no-image.jpg");
            return new MerchandiseData(itemid, provider, merchandiseName, mainImagePath);
        } catch (JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public ImageDataProtocol downloadSlipImage(String remoteImagePath) throws MyServiceFailureException {
        try {
            return new ImageDownloader(UserHelper.getCredentialsProvider().getCredentials(),
                    MyAWSConfigs.getCurrentConfig().getS3SlipsBucketName()).download(remoteImagePath);
        } catch (IOException | AmazonClientException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Nullable
    @Override
    public Integer updateCostPrice(UserShopInfoProtocol userShop, int itemid, int costPrice, @Nullable Integer marginAdditionRateInPercent) throws MyServiceFailureException {
        try {
            String resource = this.getCostPriceOfAnItemResourcePath(getPhoneNumberForItems(userShop), itemid);
            JSONObject requestBody = new JSONObject();
            requestBody.put("value", costPrice);
            if (marginAdditionRateInPercent != null) {
                requestBody.put("mar", marginAdditionRateInPercent.intValue());
            }
            String response = APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
            JSONObject resultObj = new JSONObject(response);
            if (resultObj.opt("updated_price").equals(JSONObject.NULL)) {
                return null;
            } else {
                int updatedPrice = resultObj.optInt("updated_price");
                return (updatedPrice != 0) ? Integer.valueOf(updatedPrice) : null;
            }
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public int getCostPriceOfAnItem(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException {
        try {
            String resource = this.getCostPriceOfAnItemResourcePath(getPhoneNumberForItems(userShop), itemid);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            JSONObject result = new JSONObject(response);
            return result.optInt("cost_price", 0);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public ItemCostPriceBundle getCostPriceOfItems(UserShopInfoProtocol userShop, int fromItemId, int toItemId) throws MyServiceFailureException {
        try {
            String resource = this.getItemCostPriceResourcePath(getPhoneNumberForItems(userShop));
            Map<String, String> params = new HashMap<>();
            params.put("from", String.valueOf(fromItemId));
            params.put("to", String.valueOf(toItemId));

            String response = APIGatewayClient.requestGET(resource, params, UserHelper.getIdentityToken());
            return new ItemCostPriceBundle(fromItemId, new JSONArray(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    private String getItemCloneResourcePath(String phoneNumber, String sourcePhoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "clone", sourcePhoneNumber);
    }

    @Override
    public int cloneItems(UserShopInfoProtocol userShop, String sourcePhoneNumber) throws MyServiceFailureException {
        try {
            String resource = this.getItemCloneResourcePath(getPhoneNumberForItems(userShop), sourcePhoneNumber);
            String response = APIGatewayClient.requestPOST(resource, null,UserHelper.getIdentityToken());
            return new JSONObject(response).getInt("count");
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    // [SILVER-231/SILVER-403] Item Bookmark 기능

    @Override
    public Pair<List<Integer>, SpecificItemDataBundle> postBookmarkAndGetItemData(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException {
        return this.postBookmark(userShop, itemid, true);
    }

    @Override
    public List<Integer> postBookmark(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException {
        return this.postBookmark(userShop, itemid, false).first;
    }

    private Pair<List<Integer>, SpecificItemDataBundle> postBookmark(UserShopInfoProtocol userShop, int itemid, boolean returnItem) throws MyServiceFailureException {
        try {
            String resource = this.getItemBookmarksResourcePath(getPhoneNumberForItems(userShop));
            JSONObject requestBody = new JSONObject()
                    .put("itemid", itemid)
                    .put("return_item", returnItem);

            Log.i("DEBUG_JS", String.format("[ProjectItemService.postBookmark] request : %s", requestBody.toString()));
            String response = APIGatewayClient.requestPOST(resource, requestBody.toString(), UserHelper.getIdentityToken());
            Log.i("DEBUG_JS", String.format("[ProjectItemService.postBookmark] response : %s", response));

            JSONObject result = new JSONObject(response);
            JSONArray bookmarkArr = result.getJSONArray("bookmarks");

            List<Integer> bookmarks = new ArrayList<>();
            for (int i = 0; i < bookmarkArr.length(); ++i) {
                bookmarks.add(Integer.valueOf(bookmarkArr.getInt(i)));
            }

            if (returnItem) {
                JSONObject itemDataObj = result.getJSONObject("item_data");
                SpecificItemDataBundle itemData = new SpecificItemDataBundle(itemDataObj);
                return new Pair<>(bookmarks, itemData);
            } else {
                return new Pair<>(bookmarks, null);
            }
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public void updateBookmarks(UserShopInfoProtocol userShop, List<Integer> bookmarks) throws MyServiceFailureException {
        try {
            String resource = this.getItemBookmarksResourcePath(getPhoneNumberForItems(userShop));
            JSONArray bookmarkArr = new JSONArray();
            for (Integer bookmarkItemId: bookmarks) {
                bookmarkArr.put(bookmarkItemId.intValue());
            }
            JSONObject requestBody = new JSONObject().put("bookmarks", bookmarkArr);
            APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<Integer> deleteBookmark(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException {
        try {
            String resource = this.getAnItemInItemBookmarksResourcePath(getPhoneNumberForItems(userShop), itemid);
            String response = APIGatewayClient.requestDELETE(resource, null, UserHelper.getIdentityToken());

            List<Integer> bookmarks = new ArrayList<>();
            JSONArray bookmarkArr = new JSONObject(response).getJSONArray("bookmarks");
            for (int i = 0; i < bookmarkArr.length(); ++i) {
                bookmarks.add(Integer.valueOf(bookmarkArr.getInt(i)));
            }
            return bookmarks;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 두 아이템을 비교한다. 두 아이템 간에 ID가 일치하지 않거나(비교 불가), 차이가 없는 경우 null을 반환한다.
     * 비교하는 필드는 카테고리, 이름, 중국어 이름, 설명, 가격, 소매가격, 노트 필드이다.
     *
     * @param original
     * @param updated
     * @return
     * @throws JSONException
     */
    private JSONObject diffItems(ItemDataProtocol original, ItemDataProtocol updated) throws JSONException {
        if (original.getItemId() != updated.getItemId()) {
            return null;
        }
        JSONObject result = new JSONObject();

        // 카테고리 / 이름 / 중국어 이름 / 설명 / 가격 / 소매가격 / 노트 (7개 필드 비교)
        // [STORM-158] Chinese Name 추가
        if (original.getCategory() != updated.getCategory()) {
            result.put("category", updated.getCategory().toRemoteStr());
        }
        if (!original.getItemName().equals(updated.getItemName())) {
            result.put("item_name", updated.getItemName());
        }
        if (!original.getChineseName().equals(updated.getChineseName())) {
            result.put("cn_name", updated.getChineseName());            // [STORM-158]
        }
        if (!original.getItemDescription().equals(updated.getItemDescription())) {
            result.put("description", updated.getItemDescription());
        }
        if (original.getPrice() != updated.getPrice()) {
            result.put("price", updated.getPrice());
        }
        if (original.getRetailPrice() != updated.getRetailPrice()) {
            result.put("retail", updated.getRetailPrice());
        }
        if (!original.getPrivateNote().equals(updated.getPrivateNote())) {
            result.put("note", updated.getPrivateNote());
        }

        // 위의 네 필드에 대해 변화가 없는 경우 null을 반환한다.
        return (result.length() == 0) ? null : result;
    }

    private ImageDataProtocol downloadItemImageFrom(String bucket, String remoteImagePath) throws MyServiceFailureException {
        try {
            return new ImageDownloader(UserHelper.getCredentialsProvider().getCredentials(), bucket).download(remoteImagePath);
        } catch (IOException | AmazonClientException e) {
            e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    private String getAnItemMainImageResourcePath(String phoneNumber, int itemid) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", String.valueOf(itemid), "main-image");
    }

    @Override
    public String changeMainImage(UserShopInfoProtocol userShop, int itemid, String mainImageLocalPath) throws MyServiceFailureException {
        try {
            String resource = this.getAnItemMainImageResourcePath(getPhoneNumberForItems(userShop), itemid);

            ImageUploader uploader = new ImageUploader(
                    UserHelper.getCredentialsProvider().getCredentials(), getPhoneNumberForItems(userShop),
                    MyAWSConfigs.getCurrentConfig().getS3ItemsBucketName()
            );
            String remoteImagePath = uploader.upload(
                    ImageUploadType.ITEM_MAIN_IMAGE, DateTime.now(), mainImageLocalPath
            );

            // 요청 데이터는 이미지 주소의 목록이다.
            JSONObject requestBody = new JSONObject().put("new_image", remoteImagePath);

            // 응답 데이터는 Item Data의 JSON 배열 타입 문자열이다.
            APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
            return remoteImagePath;
        } catch (AmazonClientException | JSONException | MyServiceFailureException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    private String getStockChangeLogOfADayResource(String phoneNumber, ItemStockKey itemOption, DateTime specificDay) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock", itemOption.toAPIParamStr(), "changes", APIGatewayHelper.getRemoteDateString(specificDay));
    }

    private String getRecentStockChangeLogResource(String phoneNumber, ItemStockKey itemOption) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", "stock", itemOption.toAPIParamStr(), "changes", "recent");
    }

    @Override
    public ItemStockChangeLogBundleData getItemStockChangeLogData(UserShopInfoProtocol userShop, ItemStockKey itemOption, DateTime specificDay) throws MyServiceFailureException {
        String resource = this.getStockChangeLogOfADayResource(getPhoneNumberForItems(userShop), itemOption, specificDay);
        return this.getItemStockChangeLogData(resource);
    }

    @Override
    public ItemStockChangeLogBundleData getRecentItemStockChangeLogData(UserShopInfoProtocol userShop, ItemStockKey itemOption) throws MyServiceFailureException {
        String resource = this.getRecentStockChangeLogResource(getPhoneNumberForItems(userShop), itemOption);
        return this.getItemStockChangeLogData(resource);
    }

    private ItemStockChangeLogBundleData getItemStockChangeLogData(String resource) throws MyServiceFailureException {
        try {
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            JSONObject logDataObj = new JSONObject(response);
            return new ItemStockChangeLogBundleData(logDataObj);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }
}
