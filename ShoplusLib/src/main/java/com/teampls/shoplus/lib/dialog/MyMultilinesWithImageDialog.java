package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.teampls.shoplus.R;

import java.util.List;

/**
 * Created by Medivh on 2017-08-24.
 */

public class MyMultilinesWithImageDialog extends BaseDialog {

    public MyMultilinesWithImageDialog(Context context, String title, List<String> lines, Bitmap bitmap) {
        super(context);
        setDialogWidth(0.9, 0.6);
        findTextViewById(R.id.dialog_multilines_image_title, title);
        findTextViewById(R.id.dialog_multilines_image_textview, TextUtils.join("\n", lines));
        setOnClick(R.id.dialog_multilines_image_close);
        ImageView imageView = (ImageView) findViewById(R.id.dialog_multilines_image_imageView);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_multilines_image;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_multilines_image_close) {
            dismiss();
        }
    }
}
