package com.teampls.shoplus.lib.protocol;

/**
 * 서비스 API 메서드에 사용자 관련 정보를 전달하기 위한 데이터 프로토콜
 *
 * @author lucidite
 */

public interface UserShopInfoProtocol {
    /**
     * 메인 샵 ID(PhoneNumber) 또는 내 샵 ID(PhoneNumber) 또는 내 ID(PhoneNumber)를 반환한다.
     * [NOTE] (별도설정) 메인 샵 ID - (별도설정) 샵 ID - 로그인 ID 순으로 가장 먼저 발견되는 ID를 반환한다.
     *
     * @return
     */
    String getMainShopOrMyShopOrUserPhoneNumber();

    /**
     * 내 샵 ID(PhoneNumber) 또는 내 ID(PhoneNumber)를 반환한다. 메인 샵이 설정되어 있는 경우에도 이를 반환하지 '않는다'.
     * [NOTE] (별도설정) 샵 ID - 로그인 ID 순으로 가장 먼저 발견되는 ID를 반환한다.
     *
     * @return
     */
    String getMyShopOrUserPhoneNumber();

    /**
     * 로그인한 ID(PhoneNumber)를 반환한다. 기기에 메인 샵이나 내 샵이 설정되어 있는 경우에도 이를 반환하지 '않는다'.
     * @return
     */
    String getUserPhoneNumber();
}