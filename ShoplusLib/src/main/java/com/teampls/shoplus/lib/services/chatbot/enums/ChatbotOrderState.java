package com.teampls.shoplus.lib.services.chatbot.enums;

import com.teampls.shoplus.lib.enums.ColorType;

/**
 * 챗봇을 통한 주문의 상태
 *
 * @author lucidite
 */
public enum ChatbotOrderState {
    REQUESTED("r", "주문중", ColorType.Blue),
    IN_PROGRESS("p", "처리중", ColorType.Purple),
    FINISHED("f", "완료", ColorType.Black),
    CANCELLED("c", "고객취소", ColorType.DarkGray),
    REJECTED("j", "매장취소", ColorType.DarkGray);

    private String remoteStr;
    private String uiStr;
    public ColorType color;

    ChatbotOrderState(String remoteStr, String uiStr, ColorType colorType) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
        this.color = colorType;
    }

    public String getRemoteStr() {
        return remoteStr;
    }

    public String getUiStr() {
        return uiStr;
    }

    public static ChatbotOrderState fromRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty()) {
            return REQUESTED;
        }
        String checkStr = remoteStr.toLowerCase();
        for (ChatbotOrderState state: ChatbotOrderState.values()) {
            if (state.remoteStr.equals(checkStr)) {
                return state;
            }
        }
        return REQUESTED;    // Fallback
    }

    /**
     * 주문 상태가 고객 또는 매장에 의해 취소된 상태인지 확인한다.
     *
     * @return 고객취소 또는 매장취소인 경우 true, 이외의 경우에는 false
     */
    public boolean isCancelled() {
        switch (this) {
            case CANCELLED: return true;
            case REJECTED: return true;
            default: return false;
        }
    }

    /**
     * 매장에서 (주문을 수락하고) 신규거래를 만들거나 주문을 거절(취소)할 수 있는 상태인지 확인한다.
     * [NOTE] 주문중 또는 처리중인 경우에만 신규거래로 만들거나 주문을 거절할 수 있다.
     *
     * @return 주문을 수락 또는 거절할 수 있는 상태인 경우 true, 이외의 경우에는 false
     */
    public boolean canMakeTransactionOrBeRejected() {
        switch (this) {
            case REQUESTED: return true;
            case IN_PROGRESS: return true;
            default: return false;
        }
    }
}
