package com.teampls.shoplus.lib.database_map;

import android.content.Context;

import com.teampls.shoplus.lib.database_map.KeyValueDB;

/**
 * Created by Medivh on 2018-11-19.
 */

public class KeyValueInt {
    public String key = "";
    public int defaultValue = 0;

    public KeyValueInt(String key, int defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    public void put(Context context, int value) {
        KeyValueDB.getInstance(context).put(key, value);
    }

    public int get(Context context) {
        return KeyValueDB.getInstance(context).getInt(key, defaultValue);
    }

}
