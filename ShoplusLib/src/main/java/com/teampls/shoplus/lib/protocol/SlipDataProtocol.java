package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.enums.SlipGenerationType;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Slip Data 프로토콜. 서버로부터 수신한 데이터를 거의 raw에 가깝께 wrapped하여 처리하기 위한 프로토콜.
 * TODO 필요한 경우 클라이언트에 적합한 데이터 구조로 adapting하기 위한 별도 프로토콜을 추가한다.
 *
 * @author lucidite
 */

public interface SlipDataProtocol {
    SlipDataKey getKey();
    SlipGenerationType getSlipType();
    String getOwnerPhoneNumber();
    String getCounterpartPhoneNumber();
    DateTime getCreatedDatetime();
    List<String> getImagePaths();
    List<SlipItemDataProtocol> getItems();
    DateTime getSalesDate();
    PaymentTermsProtocol getPaymentTerms();
    Boolean isCompleted();

    /**
     * Slip 또는 Transaction이 취소되었는지를 반환한다.
     * @return
     */
    Boolean isCancelled();

    /**
     * Transaction이 삭제 예약되었는지를 반환한다.
     */
    Boolean isDeletionScheduled();

    /**
     * Slip 또는 Transaction이 취소된 경우 취소된 시간을 반환한다.
     * 취소되지 않은 경우의 반환값은 undefined이다(null을 반환하지 않으려면 fallback datetime 값을 반환).
     * @return
     */
    DateTime getCancelledDatetime();

    /**
     * [SILVER-267] 실제 작성자(composer, employee)의 전화번호를 조회한다.
     *
     * @return
     */
    String getComposerPhoneNumber();

    /**
     * [ORGR-16] (챗봇 주문으로부터 생성한 거래기록의 경우) 연계된 주문 ID를 조회한다.
     * @return
     */
    String getLinkedChatbotOrderId();

    /**
     * [SILVER-496] 거래 전 잔금에 대한 청구금액을 조회한다.
     * - 거래기록을 작성할 때 거래 전 잔금을 조회한 후 0에서 최대 잔금 사이의 금액을 청구할 수 있다.
     * - 총 청구금액은 거래에서 발생한 잔금(청구금액) + 이 청구금액(Demand for Unpayment)이 된다.
     *
     * @return
     */
    int getDemandForUnpayment();
}
