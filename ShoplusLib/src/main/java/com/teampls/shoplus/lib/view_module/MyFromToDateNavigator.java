package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.view.MyView;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2019-05-25.
 */

public class MyFromToDateNavigator implements View.OnClickListener {
    private TextView tvFrom, tvTo;
    private DateTime fromDate, toDate;

    public MyFromToDateNavigator(Context context, View view) {
        tvFrom = view.findViewById(R.id.simple_timeset_today);
        tvTo = view.findViewById(R.id.simple_timeset_week);
        MyView.setTextViewByDeviceSize(context, tvFrom, tvTo);
        tvFrom.setOnClickListener(this);
        tvTo.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

    }
}
