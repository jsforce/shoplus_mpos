package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.widget.CompoundButtonCompat;
import android.view.View;
import android.widget.CheckBox;

import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-02-18.
 */

public class MyCheckBoxs<T> implements View.OnClickListener {
    public List<CheckBox> checkBoxs = new ArrayList<>();
    private View view;
    private double scale;
    private T defaultT;
    private MyOnClick<List<T>> myOnClick;
    private Context context;

    public MyCheckBoxs(Context context, View view) {
        this(context, view, 1.0);
    }

    public MyCheckBoxs(Context context, View view, double scale) {
        this.context = context;
        this.view = view;
        this.scale = scale;
    }

    public void setDefault(T defaultT) {
        this.defaultT = defaultT;
    }

    public MyCheckBoxs add(int resId, boolean checked, T... tags) {
        return add((CheckBox) view.findViewById(resId), checked, tags);
    }


    public void clear() {
        checkBoxs.clear();
    }

    public MyCheckBoxs add(CheckBox checkBox, T tag) {
        return add(checkBox, false, tag);
    }

    public void setTextAsEnumName() {
        for (CheckBox checkBox : checkBoxs) {
            List<T> tags = (List<T>) checkBox.getTag();
            if (tags.size() >= 1)
                checkBox.setText(tags.get(0).toString());
        }
    }

    public MyCheckBoxs add(CheckBox checkBox, boolean checked, T... tags) {
        List<T> results = new ArrayList<>();
        for (T tag : tags)
            results.add(tag);
        checkBox.setTag(results);
        checkBox.setScaleX((float) scale);
        checkBox.setScaleY((float) scale);
        checkBox.setChecked(checked);
        checkBox.setOnClickListener(this);
        checkBoxs.add(checkBox);
        MyView.setTextViewByDeviceSize(context, checkBox);
        if (checkBox instanceof CheckBox && MyDevice.getAndroidVersion() <= 21)
            CompoundButtonCompat.setButtonTintList(checkBox, ColorStateList.valueOf(ColorType.CheckBoxColor.colorInt));
        return this;
    }

    public MyCheckBoxs addStringWithTitle(int resId, String tag, boolean checked) {
        CheckBox checkBox = (CheckBox) view.findViewById(resId);
        checkBox.setOnClickListener(this);
        List<String> tags = new ArrayList<>();
        tags.add(tag);
        checkBox.setText(tag);
        checkBox.setTag(tags);
        checkBox.setChecked(checked);
        checkBoxs.add(checkBox);
        return this;
    }

    public MyCheckBoxs addStringWithTitle(CheckBox checkBox, String tag, boolean checked) {
        checkBox.setOnClickListener(this);
        List<String> tags = new ArrayList<>();
        tags.add(tag);
        checkBox.setText(tag);
        checkBox.setTag(tags);
        checkBox.setChecked(checked);
        checkBoxs.add(checkBox);
        return this;
    }

    public void setOnClick(MyOnClick<List<T>> myOnClick) {
        this.myOnClick = myOnClick;
    }

    public List<T> getCheckedItems() {
        List<T> results = new ArrayList<>(0);
        for (CheckBox checkBox : checkBoxs)
            if (checkBox.isChecked()) {
                List<T> tags = (List<T>) checkBox.getTag();
                for (T tag : tags)
                    results.add(tag);
            }
        if (results.size() == 0 && defaultT != null)
            results.add(defaultT);
        return results;
    }

    @Override
    public void onClick(View view) {
        CheckBox checkBox = (CheckBox) view;
        if (myOnClick != null)
            myOnClick.onMyClick(view, (List<T>) checkBox.getTag());
    }

    public void setFontSizeByDeviceSize(Context context) {
        for (CheckBox checkBox : checkBoxs)
            MyView.setTextViewByDeviceSize(context, checkBox);
    }
}
