package com.teampls.shoplus.lib.view_base;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseItemDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseItemOptionDBAdapter;
import com.teampls.shoplus.lib.adapter.MyStringAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.SelectedItemRecord;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.datatypes.ItemDataBundle;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.BaseFloatingButtons;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Medivh on 2017-06-17.
 */
// silvermoon과 ... dalaran에서 silvermoon 아이템 가져올때 사용 (발주서)
abstract public class BaseItemAndOptionSearch extends BaseActivity {
    protected ItemDB itemDB;
    protected EditText etName, etShadowNumber;
    protected ListView lvItems, lvOptions;
    protected TextView tvSelectedItem, tvLastSelectedItem;
    protected ImageView imageView, ivClear, ivClose;
    protected BaseItemDBAdapter itemAdapter;
    protected BaseItemOptionDBAdapter optionAdapter;
    protected MyStringAdapter historyAdapter;
    protected ItemRecord selectedItem = new ItemRecord();
    protected ItemRecord lastSelectedItem = new ItemRecord();
    protected ItemOptionRecord selectedOption = new ItemOptionRecord(); // 선택된 내용 표시
    protected static MyOnClick<SelectedItemRecord> onApplyClicked;
    protected Button btCancel, btOpenFullView, btApply, btAddFast, btShowHistory;
    protected MyTextWatcher myTextWatcher;
    protected FloatingButtons floatingButtons;
    protected static boolean searchMoreEnabled = true;
    protected static ItemSearchType itemSearchType = ItemSearchType.ByName;
    protected static String buyerPhoneNumber = ""; // 고객별 가격 조회용

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        myActionBar.hide();
        floatingButtons = new FloatingButtons(getView(), searchMoreEnabled ? View.VISIBLE : View.GONE);

        setItemDB();
        setItemAdapter();
        setOptionAdapter();
        historyAdapter = new MyStringAdapter(context);

        etName = (EditText) findViewById(R.id.item_and_option_search_edittext);
        etName.setText("");
        switch (itemSearchType) {
            case ByName:
                if (MyDevice.getAndroidVersion() <= 26)
                    etName.setHint("이름 일부분 입력");
                etName.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            case BySerialNum:
                if (MyDevice.getAndroidVersion() <= 26)
                    etName.setHint("상품번호 6자리 (올해것은 뒷4자리만)");
                etName.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
                break;
        }
        lvItems = findViewById(R.id.item_and_option_search_items);
        lvItems.setAdapter(itemAdapter);
        lvOptions = findViewById(R.id.item_and_option_search_options);
        lvOptions.setAdapter(optionAdapter);

        tvSelectedItem = findTextViewById(R.id.item_and_option_search_selected);
        tvLastSelectedItem = findTextViewById(R.id.item_and_option_search_lastSelected);
        imageView = findViewById(R.id.item_and_option_search_imageView);
        imageView.setImageBitmap(Empty.bitmap);
        btCancel = findViewById(R.id.item_and_option_search_cancel);
        btApply = findViewById(R.id.item_and_option_search_apply);
        btShowHistory = findViewById(R.id.item_and_option_search_showHistory);
        btShowHistory.setVisibility(View.GONE);
        btAddFast = findViewById(R.id.item_and_option_search_addFast);
        MyView.setBlueColorFilter(btAddFast);
        btOpenFullView = findViewById(R.id.item_and_option_search_openFullView);
        ivClear = findViewById(R.id.item_and_option_search_clear);
        ivClose = findViewById(R.id.item_and_option_search_close);
        etShadowNumber = findViewById(R.id.item_and_option_number_edittext);

        myTextWatcher = new MyTextWatcher();
        etName.addTextChangedListener(myTextWatcher);

        setOnClick(R.id.item_and_option_search_apply, R.id.item_and_option_search_clear,
                R.id.item_and_option_search_addFast, R.id.item_and_option_search_lastSelected,
                R.id.item_and_option_search_cancel, R.id.item_and_option_search_openFullView,
                R.id.item_and_option_search_close, R.id.item_and_option_search_selectAll,
                R.id.item_and_option_search_showHistory);

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvLastSelectedItem.setVisibility(View.GONE);
                itemAdapter.onItemClick(view, position);
                selectedItem = itemAdapter.getRecord(position);
                onMyItemClick(itemAdapter.getRecord(position));
            }
        });

        lvOptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                optionAdapter.onItemClick(view, position);
                selectedOption = optionAdapter.getRecord(position);
                onMyOptionClick(optionAdapter.getRecord(position));
            }
        });

        lvOptions.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                optionAdapter.onItemClick(view, position);
                selectedOption = optionAdapter.getRecord(position);
                onMyOptionLongClick(optionAdapter.getRecord(position));
                return true;
            }
        });

        refreshSelected();
        MyDevice.showKeyboard(context, etName);
        setBottomButtons();

        MyView.setTextViewByDeviceSize(context, etName);
        MyView.setImageViewSizeByDeviceSize(context, imageView);

        // error check
        if (buyerPhoneNumber.isEmpty())
            Log.w("DEBUG_JS", String.format("[%s.onCreate] buyerPhoneNumber is Empty", getClass().getSimpleName()));
    }

    abstract protected void setItemDB();

    abstract protected void setItemAdapter();

    abstract protected void setOptionAdapter();

    abstract protected void onMyItemClick(ItemRecord itemRecord);

    // 보통 전체를 override해서 쓴다
    protected void onMyOptionClick(ItemOptionRecord itemOptionRecord) {
        selectedOption = itemOptionRecord;
        refreshSelected();
    }

    protected void onMyOptionLongClick(ItemOptionRecord itemOptionRecord) {
        onMyOptionClick(itemOptionRecord);
    }

    protected void refreshSelected() {
        tvSelectedItem.setText(String.format("%s%s%s", selectedItem.name,
                selectedOption.toString(" / ").isEmpty() ? "" : " / " + selectedOption.toString(" / "),
                optionAdapter.clickedPositions.size() <= 1 ? "" : String.format(" (+%d)", optionAdapter.clickedPositions.size() - 1)
        ));
    }

    @Override
    public int getThisView() {
        return R.layout.base_item_and_option_search;
    }

    public void refresh() {
        if (itemAdapter != null) {
            itemAdapter.notifyDataSetChanged();
        }
        if (optionAdapter != null) {
            optionAdapter.notifyDataSetChanged();
        }
    }

    public void doFinish() {
        MyDevice.hideKeyboard(context, etName);
        doFinishForResult();
    }

    private void onLastSelectedClick() {
        tvLastSelectedItem.setVisibility(View.GONE);
        // item
        if (lastSelectedItem.itemId > 0)
            selectItem(lastSelectedItem);
        // option은 의미가 없음 ... 개수가 0이므로 어차피 선택의 과정이 필요함
    }

    protected void selectItem(ItemRecord itemRecord) {
        tvLastSelectedItem.setVisibility(View.GONE);
        etName.setText(itemRecord.getUiName());
        etName.setSelection(etName.getText().length());
        int position = 0;
        boolean found = false;
        for (ItemRecord record : itemAdapter.getRecords()) {
            if (record.itemId == itemRecord.itemId) {
                found = true;
                itemAdapter.setClickedPosition(position);
                break;
            }
            position++;
        }
        if (found) {
            itemAdapter.notifyDataSetChanged();
            onMyItemClick(itemRecord);
        } else {
            MyUI.toastSHORT(context, String.format("현재 리스트에는 없습니다"));
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.item_and_option_search_clear) {
            imageView.setImageBitmap(Empty.bitmap);
            selectedItem = new ItemRecord();
            selectedOption = new ItemOptionRecord();
            itemAdapter.clearClicked();
            optionAdapter.clear();
            optionAdapter.notifyDataSetChanged();
            refreshSelected();
            setBottomButtons();
            etName.setText("");
            etName.requestFocus();
            if (lastSelectedItem.itemId > 0) {
                tvLastSelectedItem.setVisibility(View.VISIBLE);
                tvLastSelectedItem.setText(lastSelectedItem.getUiName());
            }

        } else if (view.getId() == R.id.item_and_option_search_showHistory) {
            onShowHistoryClick();

        } else if (view.getId() == R.id.item_and_option_search_lastSelected) {
            onLastSelectedClick();

        } else if (view.getId() == R.id.item_and_option_search_cancel) {
            doFinish();
        } else if (view.getId() == R.id.item_and_option_search_close) {
            doFinish();
        } else if (view.getId() == R.id.item_and_option_search_apply) {
            onApplyClick(selectedItem);
        } else if (view.getId() == R.id.item_and_option_search_openFullView) {
            onModifyClick(selectedItem);
        } else if (view.getId() == R.id.item_and_option_search_addFast) {
            onAddFastClick(selectedItem);

        } else if (view.getId() == R.id.item_and_option_search_selectAll) {
            onAddAllOptionsClick(selectedItem);
        }
    }

    abstract protected void onModifyClick(ItemRecord selectedItem);

    abstract protected void onApplyClick(ItemRecord selectedItem);

    abstract protected void onAddFastClick(ItemRecord selectedItem);

    abstract protected void onAddAllOptionsClick(ItemRecord selectedItem);

    protected void onShowHistoryClick() {

    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }


    class MyTextWatcher extends BaseTextWatcher {
        public boolean activated = true;
        private List<ItemRecord> fullRecords = new ArrayList<>();

        public MyTextWatcher() {
            initFullRecords();
        }

        public void initFullRecords() {
            fullRecords = itemDB.getRecordsOrderBy(ItemDB.Column.itemId, true); // 최신순 정렬
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (activated == false)
                return;
            List<ItemRecord> results = new ArrayList<>();
            String keyword = "";
            switch (itemSearchType) {
                case ByName:
                    keyword = s.toString().toLowerCase().trim();
                    break;
                case BySerialNum:
                    keyword = s.toString().replace("-", "").trim();
                    break;
            }

            for (ItemRecord record : fullRecords) {
                switch (itemSearchType) {
                    case ByName:
                        if (record.name.isEmpty() && record.getUiName().contains(keyword))
                            results.add(record);
                        else if (record.name.toLowerCase().contains(keyword))
                            results.add(record);
                        break;
                    case BySerialNum:
                        if (record.serialNum.replace("-", "").contains(keyword))
                            results.add(record);
                        break;
                }
            }
            itemAdapter.setRecords(results);
            itemAdapter.notifyDataSetChanged();
            setBottomButtons();
        }
    }

    public void setBottomButtons() {
    }

    public class FloatingButtons extends BaseFloatingButtons {
        protected TaskState onSearching = TaskState.beforeTasking;

        public FloatingButtons(View view, int visibility) {
            super(view);
            switch (visibility) {
                case View.VISIBLE:
                    add(R.id.item_and_option_search_more);
                    break;
                default:
                    setVisibility(View.GONE, R.id.item_and_option_search_more);
                    break;
            }
            hide();
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.item_and_option_search_more) {
                switch (itemSearchType) {
                    case ByName:
                        String keyword = etName.getText().toString().trim();
                        if (keyword.length() < 2) {
                            MyUI.toastSHORT(context, String.format("2자 이상이 필요합니다"));
                            return;
                        }
                        searchItemsBy(keyword);
                        break;
                    case BySerialNum:
                        final String serialNum = BaseUtils.toItemSerialStr(etName.getText().toString().trim());
                        if (serialNum.isEmpty()) {
                            MyUI.toastSHORT(context, String.format("올바른 형식이 아닙니다. 예)180001"));
                            return;
                        }
                        itemDB.searchRecord(context, serialNum, new MyOnTask<ItemRecord>() {
                            @Override
                            public void onTaskDone(ItemRecord itemRecord) {
                                if (itemRecord.itemId == 0)
                                    return;
                                myTextWatcher.initFullRecords();
                                etName.setText(serialNum);
                                onMyItemClick(itemRecord);
                            }
                        });
                        break;
                }

            }
        }

        private void searchItemsBy(final String keyword) {
            if (onSearching.isOnTasking()) {
                MyUI.toastSHORT(context, String.format("검색 중입니다"));
                return;
            }

            onSearching = TaskState.onTasking;
            final ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(String.format("[%s] 검색중..", keyword));
            progressDialog.show();

            new CommonServiceTask(context, "searchItemsBy") {
                List<ItemRecord> retrievedItemRecords = new ArrayList<>();

                @Override
                public void doInBackground() throws MyServiceFailureException {
                    retrievedItemRecords = new ArrayList<>();
                    ItemDataBundle itemDataBundle = itemService.searchItems(userSettingData.getUserShopInfo(), keyword);

                    // items
                    for (ItemDataProtocol item : itemDataBundle.getItems()) {
                        ItemRecord record = new ItemRecord(item);
                        itemDB.updateOrInsert(record);
                        retrievedItemRecords.add(record);
                        // images는 adapter에서 알아서 잘 가져올 것이다
                    }

                    // options (+stock)
                    for (ItemStockKey key : itemDataBundle.getStock().keySet()) {
                        ItemOptionRecord record = new ItemOptionRecord(key, itemDataBundle.getStock().get(key));
                        itemDB.options.updateOrInsert(record);
                    }

                    if (itemDataBundle.getItems().size() == 0) {
                        MyUI.toastSHORT(context, String.format("발견되지 않음"));
                    } else {
                        MyUI.toastSHORT(context, String.format("%d건 발견됨", itemDataBundle.getItems().size()));
                    }
                }

                @Override
                public void onPostExecutionUI() {
                    onSearching = TaskState.afterTasking;
                    progressDialog.dismiss();
                    if (retrievedItemRecords.size() >= 1) {
                        myTextWatcher.initFullRecords();
                        etName.setText(keyword);
                    }
                }

                @Override
                public void catchException(MyServiceFailureException e) {
                    super.catchException(e);
                    onSearching = TaskState.onError;
                    progressDialog.dismiss();
                }
            };
        }

    }

}
