package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_map.GlobalBoolean;
import com.teampls.shoplus.lib.datatypes.MyTriple;
import com.teampls.shoplus.lib.enums.BalanceChangeMethodType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

abstract public class BasePreference extends PreferenceActivity {
    protected static String KEY_Exist = "exist";
    public static String KEY_Vibration = "vibration";
    public static String KEY_Sounding = "isSounding";
    public static String KEY_Messaging = "isNotifyingMessage";
    public static String KEY_PreferenceInitailized = "preference.initialized";
    protected static String noExist = "-999";
    protected PreferenceScreen screen;
    protected PreferenceCategory catergory;
    protected Context context = BasePreference.this;
    protected static String KEY_ShiftTime = "shiftTime";
    protected Pair<Double, Double> shiftTimes;
    public static String KEY_DOWNLOAD_TO_CAMERA_FOLDER = "downloadDir";
    protected static String KEY_ShowCancelled = "showCancelled";
    protected static String KEY_ShowWeekday = "showWeekday";
    protected boolean doShowCancelled = true, doShowWeekday = false;
    protected GlobalDB globalDB;
    protected static String KEY_disableReturns = "disable.returns";
    protected static String KEY_ImageViewRatio = "image.view.ratio";
    protected double imageViewRatio = 1.2;
    protected static float defaultImageViewRatio = 1.2f, fontScale = 1.0f;
    protected UserSettingData userSettingData;
    private static String KEY_fontScaleLimit = "font.scale.limit";
    protected static String KEY_fontScale = "font.scale";
    protected static String KEY_ReceivableManagementType = "receivable.management.type";
    protected boolean doRefresh = false, doRestart = false, doRefreshMenu = false;
    protected BalanceChangeMethodType receivableManagementType = BalanceChangeMethodType.ALL;
    public static String KEY_QR_Enabled = "qr.enabled";
    public static PreferenceBoolean isOneDigitBasicUnit = new PreferenceBoolean("is.zero.basic.unit", false),
            priceByBuyer = new PreferenceBoolean("use.pricing", false);
    public static PreferenceBoolean retailEnabled = new PreferenceBoolean("silver.preference.retail", false); // userSetting 값을 사용
    public static PreferenceBoolean doShowHm = new PreferenceBoolean("show.hourAndMinute", true);

    // Global Preference
    public static final GlobalBoolean landScapeOrientationEnabled = new GlobalBoolean("global.pref.screen.landscape", false);

    @Override
    public void onBuildHeaders(List<Header> target) {
        super.onBuildHeaders(target);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalDB = GlobalDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
        screen = getPreferenceManager().createPreferenceScreen(this);
        catergory = new PreferenceCategory(this);
        catergory.setTitle("유저 셋팅");
        screen.addPreference(catergory);
        createPreference();
        this.setPreferenceScreen(screen);
        doShowCancelled = isShowCancelled(context);
        doShowHm.updatePrevValue(context);
        shiftTimes = getShiftTimes(context);
        fontScale = getFontScale(context);

        // 전체 계정에 적용되는 정보
        isOneDigitBasicUnit.setValue(context, userSettingData.isOneDigitBasicUnit());
        isOneDigitBasicUnit.updatePrevValue(context);

        receivableManagementType = userSettingData.getReceivableManagementType();
        setEnum(context, KEY_ReceivableManagementType, receivableManagementType);

        retailEnabled.updatePrevValue(context);
        doRefresh = false;
    }

    @Override
    public void onBackPressed() {
        if (doShowCancelled != isShowCancelled(context))
            onShowCancelledChanged();

        if (shiftTimes.first.doubleValue() != getShiftTimes(context).first.doubleValue() || shiftTimes.second.doubleValue() != getShiftTimes(context).second.doubleValue()) {
            onShiftTimeChanged();
        }

        if (fontScale != getFontScale(context)) {
            onFontScaleChanged();
            MyUI.toastSHORT(context, String.format("앱 프로세스를 종료하고 시작하면 글자 크기가 변경됩니다"));
        }

        if (isOneDigitBasicUnit.isChanged(context)) {
            // 서버와 로컬의 정보를 변경해야 한다
            userSettingData.setBasicUnit(isOneDigitBasicUnit.getValue(context), new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    MyUI.toastSHORT(context, String.format("단가 단위를 변경했습니다"));
                }
            });
        }

        if (retailEnabled.isChanged(context)) {
            if (userSettingData.isProtocolNull() == false) {
                new CommonServiceTask(context, "SilverPreference") {
                    @Override
                    public void doInBackground() throws MyServiceFailureException {
                        UserSettingDataProtocol protocol = mainShopService.updateRetailPriceMode(userSettingData.getUserShopInfo(), retailEnabled.getValue(context));
                        userSettingData.set(protocol);
                        MyUI.toastSHORT(context, userSettingData.getProtocol().isRetailOn() ? "도소매가를 구분합니다" : "도소매가를 구분하지 않습니다");
                    }
                };
            }
        }

        // 설정된 정보를 global 정보에도 저장
        boolean changed = updateGlobalPreference(landScapeOrientationEnabled);
        if (changed)
            doRestart = true;

        if (doRestart) {
            MyDevice.restartApp(context, "중요 설정이 변경되어 앱을 재시작합니다");
        } else {
            super.onBackPressed();
        }
    }

    public boolean updateGlobalPreference(GlobalBoolean globalBoolean) {
        boolean prevValue = globalBoolean.get(context);
        boolean updatedValue = getBoolean(context, globalBoolean.key, globalBoolean.defaultValue);
        if (prevValue != updatedValue) {
            globalBoolean.put(context, updatedValue);
            return true;
        } else {
            return false;
        }
    }

    public static BalanceChangeMethodType getReceivableManagementType(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String managementType = preferences.getString(KEY_ReceivableManagementType, BalanceChangeMethodType.ALL.toString());
        return BalanceChangeMethodType.valueOf(managementType);
    }

    protected void createFontScale() {
        List<String> scales = Arrays.asList("1.0", "1.1", "1.2", "1.3");
        addList(KEY_fontScale, "글자 크기 조정", "앱에서 표시되는 글자 크기를 조절합니다", scales, 0);
    }

    public static float getFontScale(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return Float.valueOf(preferences.getString(KEY_fontScale, "1.0f"));
    }

    public static double getImageViewRatio(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return BaseUtils.toFloat(preferences.getString(KEY_ImageViewRatio, ""), defaultImageViewRatio);
    }

    public void onShowCancelledChanged() {
    }

    public void onShiftTimeChanged() {
    }

    public void onFontScaleChanged() {
        MyDevice.setFontScale(context, getFontScale(context));
    }

    abstract protected void createPreference();

    protected static void initBase(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_ShowCancelled, true);
        editor.commit();
    }

    public static void setBoolean(Context context, String key, boolean value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void setBoolean(Context context, GlobalBoolean globalBoolean, boolean value) {
        globalBoolean.put(context, value);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(globalBoolean.key, value);
        editor.commit();
    }

    public static int getInteger(Context context, String key, int defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return BaseUtils.toInt(preferences.getString(key, Integer.toString(defaultValue)));
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, defaultValue);
    }

    public static boolean getBoolean(Context context, PreferenceBoolean preferenceBoolean) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(preferenceBoolean.key, preferenceBoolean.defaultValue);
    }

    protected void setEnum(Context context, String key, Enum<?> value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value.toString());
        editor.commit();
    }

    protected void createNotifications() {
        CheckBoxPreference vibrating = new CheckBoxPreference(this);
        vibrating.setTitle("알림도착시 진동발생");
        vibrating.setKey(KEY_Vibration);
        catergory.addPreference(vibrating);

        CheckBoxPreference sounding = new CheckBoxPreference(this);
        sounding.setTitle("알림도착시 소리내기");
        sounding.setKey(KEY_Sounding);
        catergory.addPreference(sounding);

        CheckBoxPreference messaging = new CheckBoxPreference(this);
        messaging.setTitle("메세지 도착시 알림");
        messaging.setKey(KEY_Messaging);
        catergory.addPreference(messaging);
    }

    protected void createImageDownload(boolean defaultValue) {
        CheckBoxPreference downloadDir = new CheckBoxPreference(this);
        downloadDir.setTitle("카메라 폴더에 저장");
        downloadDir.setKey(KEY_DOWNLOAD_TO_CAMERA_FOLDER);
        downloadDir.setDefaultValue(defaultValue);
        catergory.addPreference(downloadDir);
    }

    public void createShowCancelled() {
        addCheckBox("취소기록 보기", "줄이 그어진 취소건을 봅니다", KEY_ShowCancelled, true);
    }

    public void createShowWeekDay() {
        addCheckBox("요일 표시", "날짜와 요일을 같이 표시합니다", KEY_ShowWeekday, false);
    }

    public static boolean isShowCancelled(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(KEY_ShowCancelled, true);
    }

    public static boolean isReturnsDisabled(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(KEY_disableReturns, false);
    }


    public void createShiftTimes() {
        MultiSelectListPreference shiftTime = new MultiSelectListPreference(this);
        CharSequence[] entries = {"0시", "1시", "2시", "3시", "4시", "5시", "6시", "7시", "8시", "9시", "10시", "11시", "12시",
                "13시", "14시", "15시", "16시", "17시", "18시", "19시", "20시", "21시", "22시", "23시", "24시"}; // 표시용
        CharSequence[] values = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
                "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"}; //  실제값
        shiftTime.setTitle("근무 시작 시간");
        shiftTime.setSummary("시작 시간 기준으로 색이 변경됩니다");
        shiftTime.setKey(KEY_ShiftTime);
        shiftTime.setDialogTitle("시작시간 (밤낮 교대시 2개)");
        shiftTime.setEntries(entries);
        shiftTime.setEntryValues(values); // values); //limitEntryValues);
        //shiftTime.setDefaultValue("-1");
        catergory.addPreference(shiftTime);
    }

    public void addIntegerList(String key, String title, String summary, List<Integer> entries, int defaultIndex) {
        List<String> strEntries = BaseUtils.toStringList(entries);
        addList(key, title, summary, strEntries, strEntries, defaultIndex);
    }

    public void addList(String key, String title, String summary, List<String> entries, int defaultIndex) {
        addList(key, title, summary, entries, entries, defaultIndex);
    }

    public void addList(String key, String title, String summary, List<String> entries, List<String> entryValues, int defaultIndex) {
        ListPreference preference = new ListPreference(this);
        preference.setKey(key);
        preference.setTitle(title);
        preference.setSummary(summary);
        preference.setDialogTitle(title);
        preference.setEntries(entries.toArray(new CharSequence[entries.size()]));
        preference.setEntryValues(entryValues.toArray(new CharSequence[entries.size()]));
        preference.setDefaultValue(entryValues.get(defaultIndex));
        catergory.addPreference(preference);
    }

//    public static void initBase(Context context, boolean sound) {
//        if (exist(context) == false) {
//            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//            SharedPreferences.Editor editor = preferences.edit();
//            editor.putString(KEY_Exist, "exist");
//            editor.putBoolean(KEY_Vibration, true);
//            editor.putBoolean(KEY_Sounding, sound);
//            editor.putBoolean(KEY_Messaging, true);
//            editor.commit();
//        }
//    }

    public static boolean exist(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        //	Log.i("DEBUG_JS",String.format("[MyPreference] string = %s",preferences.getString(KEY_Soldout_Period, "999")));
        return (!preferences.getString(KEY_Exist, noExist).equals(noExist));
    }

    public static boolean isDownloadToCameraFolder(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(KEY_DOWNLOAD_TO_CAMERA_FOLDER, true);
    }

    public static boolean isShowWeekday(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(KEY_ShowWeekday, false);
    }

    public static Pair<Double, Double> getShiftTimes(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> defaultValue = new HashSet<>();
        List<String> shiftTimeStrs = new ArrayList<>(preferences.getStringSet(KEY_ShiftTime, defaultValue));
        List<Double> shiftTimes = new ArrayList<>();
        for (String shiftTimeStr : shiftTimeStrs) {
            double time = BaseUtils.toDouble(shiftTimeStr, -1.0);
            if (time >= 0)
                shiftTimes.add(time);
        }
        if (shiftTimes.size() == 0) {
            return Pair.create(-1.0, -1.0);

        } else if (shiftTimes.size() == 1) {
            return Pair.create(shiftTimes.get(0), -1.0);

        } else {
            return Pair.create(Collections.min(shiftTimes), Collections.max(shiftTimes));
        }
    }

    public static void putShiftTimes(Context context, MyTriple<Double, Double, Boolean> timeSetting) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        List<String> shiftTimeStr = new ArrayList<>();
        shiftTimeStr.add(Integer.toString((int) Math.floor(timeSetting.first)));
        if (timeSetting.third)
            shiftTimeStr.add(Integer.toString((int) Math.floor(timeSetting.second)));
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet(KEY_ShiftTime, new TreeSet<String>(shiftTimeStr));
        editor.commit();
    }

    protected CheckBoxPreference addCheckBox(String title, String summary, PreferenceBoolean preferenceBoolean) {
        return addCheckBox(title, summary, preferenceBoolean.key, preferenceBoolean.defaultValue);
    }

    protected CheckBoxPreference addCheckBox(String title, String summary, GlobalBoolean preferenceBoolean) {
        return addCheckBox(title, summary, preferenceBoolean.key, preferenceBoolean.defaultValue);
    }

    protected CheckBoxPreference addCheckBox(String title, String summary, String Key, boolean defaultValue) {
        CheckBoxPreference cbPreference = new CheckBoxPreference(this);
        cbPreference.setTitle(title);
        cbPreference.setSummary(summary);
        cbPreference.setKey(Key);
        cbPreference.setDefaultValue(defaultValue);
        catergory.addPreference(cbPreference);
        return cbPreference;
    }

    public static boolean isQREnabled(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(KEY_QR_Enabled, false);
    }

    protected void checkUserSettingData(final MyOnTask onTask) {
        if (userSettingData.isProtocolNull()) {
            Log.w("DEBUG_JS", String.format("[BasePreference] userSettingData == null"));
            MyUI.toastSHORT(context, String.format("내 설정 정보를 새로 가져옵니다"));
            userSettingData.refresh(context, "checkUserSettingDat", new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    if (onTask != null)
                        onTask.onTaskDone("");
                }
            });
        } else {
            if (onTask != null)
                onTask.onTaskDone("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUserSettingData(null);
    }
}
