package com.teampls.shoplus.lib.database_memory;

import com.teampls.shoplus.lib.enums.UserLevelType;

/**
 * Created by Medivh on 2018-02-03.
 */

public class ShopMemberRecord {
    public String shopPhoneNumber = "", name = "", phoneNumber = "";
    public UserLevelType userLevel = UserLevelType.SHOP;

    public ShopMemberRecord(String shopPhoneNumber, String phoneNumber, UserLevelType userLevel, String name) {
        this.shopPhoneNumber = shopPhoneNumber;
        this.phoneNumber = phoneNumber;
        this.userLevel = userLevel;
        this.name = name;
    }

    public ShopMemberRecord(String shopPhoneNumber) {
        this.shopPhoneNumber = shopPhoneNumber;
    }


}
