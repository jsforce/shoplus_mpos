package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.DraftSlipItemDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.composition.BaseCreationComposition;
import com.teampls.shoplus.lib.database.DraftSlipRecord;
import com.teampls.shoplus.lib.database.MemoRecord;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MDraftSlipDB;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.protocol.TransactionDraftDataProtocol;
import com.teampls.shoplus.lib.view_base.BaseActivity;

import java.util.List;

/**
 * Created by Medivh on 2017-02-06.
 */

public class DraftSlipCreationView extends BaseActivity implements AdapterView.OnItemClickListener {
    private MDraftSlipDB draftSlipDB;
    public DraftSlipItemDBAdapter adapter;
    private static long draftId = 0;
    private TextView tvSummary, tvMemo;

    private ExpandableHeightListView lvSlipItems;
    private LinearLayout plusMinusSumContainer, plusContainer, minusContainer;
    private TextView tvPlusQuantitySum, tvPlusAmountSum, tvMinusQuantitySum, tvMinusAmountSum;

    // ItemCartView 종료 후 생성 과정과 동일
    public static void startActivity(Context context, long draftId) {
        DraftSlipCreationView.draftId = draftId;
        ((Activity) context).startActivityForResult(new Intent(context, DraftSlipCreationView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        doFinishForResult();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.hide();
        draftSlipDB = draftSlipDB.getInstance(context);
        adapter = new DraftSlipItemDBAdapter(context);
        lvSlipItems = findViewById(R.id.slip_creation_listview);
        lvSlipItems.setExpanded(true);
        lvSlipItems.setOnItemClickListener(this);
        lvSlipItems.setAdapter(adapter);
        DraftSlipRecord draftSlipRecord = draftSlipDB.getRecordByKey(draftId);
        UserRecord counterpart = userDB.getRecord(draftSlipRecord.recipientPhoneNumber);
        findTextViewById(R.id.slip_creation_counterpart, String.format("%s", counterpart.getName()));
        tvSummary = findTextViewById(R.id.slip_creation_summary);
        tvMemo = (TextView) findViewById(R.id.slip_creation_memo);

        // 항목추가, 완료처리, 시간변경은 공유에서 다루지 않는다
        setVisibility(View.GONE, R.id.slip_creation_addContainer, R.id.slip_creation_apply, R.id.slip_creation_date_container,
                R.id.slip_creation_addSlipitem);

        ((TextView) findViewById(R.id.slip_creation_function)).setText("작성중으로");
        setOnClick(R.id.slip_creation_cancel, R.id.slip_creation_delete, R.id.slip_creation_function,
                R.id.slip_creation_memo, R.id.slip_creation_setting);

        MyView.setTextViewByDeviceSize(context, getView(), R.id.slip_creation_table_unitprice,
                R.id.slip_creation_table_quantity, R.id.slip_creation_table_sum);
        plusMinusSumContainer = findViewById(R.id.slip_creation_plusminus_container);
        plusMinusSumContainer.setVisibility(View.GONE);
        plusContainer = findViewById(R.id.slip_creation_plus_container);
        minusContainer = findViewById(R.id.slip_creation_minus_container);
        MyView.setTextViewByDeviceSize(context, plusContainer);
        MyView.setTextViewByDeviceSize(context, minusContainer);
        tvPlusQuantitySum = findViewById(R.id.slip_creation_plus_quantity_sum);
        tvPlusAmountSum = findViewById(R.id.slip_creation_plus_amount_sum);
        tvMinusQuantitySum = findViewById(R.id.slip_creation_minus_quantity_sum);
        tvMinusAmountSum = findViewById(R.id.slip_creation_minus_amount_sum);

        refresh();
    }
    @Override
    public int getThisView() {
        return R.layout.base_slip_creation_main;
    }

    private void updateDraft(final List<SlipItemRecord> items, final String newMemo) {
        new CommonServiceTask(context, "DraftSlipCreationView") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                // version을 올려줘야 문제가 안 생김
                DraftSlipRecord draftSlipRecord = draftSlipDB.getRecordByKey(draftId);
                TransactionDraftDataProtocol protocol = transactionService.updateDraft(userSettingData.getUserShopInfo(), draftId, draftSlipRecord.versionId, SlipItemRecord.toProtocolList(items),newMemo);
                draftSlipDB.updateOrInsertBy(protocol);
            }

            @Override
            public void onPostExecutionUI() {
                MyUI.toastSHORT(context, String.format("수정사항 적용"));
                refresh();
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                MyUI.toastSHORT(context, String.format("에러가 발생했습니다. 다른 분이 수정했을 수 있습니다. 최신 데이터인지 확인해 주세요"));
            }
        };
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DraftSlipRecord draftSlipRecord = draftSlipDB.getRecordByKey(draftId);
        DraftSlipItemAddition.startActivityUpdate(context, draftSlipRecord.toSlipRecord(context),  adapter.getRecord(position), new MyOnClick<List<SlipItemRecord>>() {
            // update
            @Override
            public void onMyClick(View view, final List<SlipItemRecord> items) {
                updateDraft(items, "");
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.slip_creation_delete) {
            new MyAlertDialog(context, "공유기록 삭제", "공유 중인 거래 기록을 삭제하시겠습니까?") {
                @Override
                public void yes() {
                    deleteDraft(false);
                }
            };

        } else if (view.getId() == R.id.slip_creation_cancel) {
            doFinishForResult();

        } else if (view.getId() == R.id.slip_creation_function) {
            new MyAlertDialog(context, "작성중으로 전환", "내가 완료하기 위해 작성 중으로 전환 하시겠습니까?") {
                @Override
                public void yes() {
                    deleteDraft(true);
                }
            };

        } else if (view.getId() == R.id.slip_creation_memo || view.getId() == R.id.slip_creation_setting) {
            // 계속 추가되는 컨셉이라 이전 메모를 불러오지 않는다
            new UpdateValueDialog(context, "메모 추가", "", "") {
                @Override
                public void onApplyClick(String newValue) {
                    DraftSlipRecord draftSlipRecord = draftSlipDB.getRecordByKey(draftId);
                    List<SlipItemRecord> items = draftSlipDB.items.getRecordsBy(draftSlipRecord.getSlipDataKey(context).toSID());
                    updateDraft(items, newValue);
                }
            };
        }
    }

    private void deleteDraft(final boolean doMoveToPosSlip) {
        new CommonServiceTask(context, "DraftSlipCreationView") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                DraftSlipRecord draftSlipRecord = draftSlipDB.getRecordByKey(draftId);
                TransactionDraftDataProtocol protocol = transactionService.deleteDraft(userSettingData.getUserShopInfo(), draftId, draftSlipRecord.versionId);
                draftSlipDB.delete(draftSlipRecord);
                if (doMoveToPosSlip) {
                    PosSlipDB posSlipDB = PosSlipDB.getInstance(context);
                    SlipFullRecord slipFullRecord = new SlipFullRecord(context, protocol);
                    // 저장은 POS SlipDB에 한다는 점을 주의
                    if (posSlipDB.hasUnconfirmedRecord(slipFullRecord.record.counterpartPhoneNumber)) {
                        posSlipDB.mergeItems(slipFullRecord.record.counterpartPhoneNumber, slipFullRecord);
                        MyUI.toastSHORT(context, String.format("%s님 작성중 영수증에 추가합니다.", userDB.getRecord(slipFullRecord.record.counterpartPhoneNumber).getName()));
                    } else {
                        posSlipDB.insert(slipFullRecord);
                    }
                    //posSlipDB.locals.updateOrInsert(new LocalSlipRecord(slipFullRecord.record, MemoRecord.toUiString(draftSlipRecord.memos)));
                }
                MyUI.toastSHORT(context, String.format("[거래작성]에서 확인하실 수 있습니다."));
            }

            @Override
            public void onPostExecutionUI() {
                doFinishForResult();
            }
        };
    }

    @Override
    public void onMyMenuCreate() {
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case BaseGlobal.RequestRefresh:
                refresh();
                break;
        }
    }

    public void refresh() {
        DraftSlipRecord draftSlipRecord = draftSlipDB.getRecordByKey(draftId);
        List<SlipItemRecord> items = draftSlipDB.items.getRecordsBy(draftSlipRecord.getSlipDataKey(context).toSID());
        SlipSummaryRecord slipSummaryRecord = new SlipSummaryRecord(items);
        String summary = String.format("%s건 %s", slipSummaryRecord.count, BaseUtils.toCurrencyStr(slipSummaryRecord.amount));
        tvSummary.setText(summary);
        if (draftSlipRecord.memos.size() == 0) {
            tvMemo.setText("(클릭) 메모입력");
        } else {
            tvMemo.setText(MemoRecord.toUiString(draftSlipRecord.memos));
        }
        adapter.setRecords(items);
        adapter.notifyDataSetChanged();

        BaseCreationComposition.setPlusMinusSum(context, slipSummaryRecord, plusMinusSumContainer, plusContainer, tvPlusQuantitySum, tvPlusAmountSum,
                minusContainer, tvMinusQuantitySum, tvMinusAmountSum);
    }
}