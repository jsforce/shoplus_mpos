package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotBuyerRegistrationRequestData;

import java.util.List;

/**
 * Created by Medivh on 2019-05-10.
 */

public class MRequestedBuyers extends BaseListDB<ChatbotBuyerRegistrationRequestData> {
    private static MRequestedBuyers instance;

    public static MRequestedBuyers getInstance(Context context) {
        if (instance == null)
            instance = new MRequestedBuyers(context);
        return instance;
    }

    private MRequestedBuyers(Context context) {
        super(context, "MRequestedBuyers");
    }

    public void refresh(List<ChatbotBuyerRegistrationRequestData> records) {
        database = records;
    }

    @Override
    protected ChatbotBuyerRegistrationRequestData getEmptyRecord() {
        return new ChatbotBuyerRegistrationRequestData();
    }
}
