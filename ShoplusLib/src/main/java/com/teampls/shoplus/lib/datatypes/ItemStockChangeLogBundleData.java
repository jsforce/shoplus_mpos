package com.teampls.shoplus.lib.datatypes;

import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 재고변동 로그 묶음 데이터
 *
 * @author lucidite
 */
public class ItemStockChangeLogBundleData {
    private List<TransactionItemStockChangeLogData> transactionItemLogs;
    private List<ManualItemStockChangeLogData> stockUpdateLogs;
    private List<ArrivalItemStockChangeLogData> arrivalLogs;

    public ItemStockChangeLogBundleData(JSONObject dataObj) throws JSONException {
        JSONArray transactionItemLogArr = dataObj.optJSONArray("tr_logs");
        JSONArray stockUpdateLogArr = dataObj.optJSONArray("stock_logs");
        JSONArray arrivalLogArr = dataObj.optJSONArray("arrival_logs");

        this.transactionItemLogs = new ArrayList<>();
        this.stockUpdateLogs = new ArrayList<>();
        this.arrivalLogs = new ArrayList<>();

        for (int i = 0; i < transactionItemLogArr.length(); ++i) {
            this.transactionItemLogs.add(new TransactionItemStockChangeLogData(transactionItemLogArr.getJSONObject(i)));
        }
        for (int i = 0; i < stockUpdateLogArr.length(); ++i) {
            this.stockUpdateLogs.add(new ManualItemStockChangeLogData(stockUpdateLogArr.getJSONObject(i)));
        }
        for (int i = 0; i < arrivalLogArr.length(); ++i) {
            this.arrivalLogs.add(new ArrivalItemStockChangeLogData(arrivalLogArr.getJSONObject(i)));
        }
    }

    /**
     * 시간순으로 정렬된 로그 목록을 반환한다.
     *
     * @return
     */
    public List<ItemStockChangeLogProtocol> getSortedLogs() {
        List<ItemStockChangeLogProtocol> result = new ArrayList<>();
        result.addAll(this.transactionItemLogs);
        result.addAll(this.stockUpdateLogs);
        result.addAll(this.arrivalLogs);

        Collections.sort(result, new ItemStockChangeLogComparator());
        return result;
    }
}
