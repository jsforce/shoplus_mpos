package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.database.MyQuery;
import com.teampls.shoplus.lib.database_global.ImageRecord;
import com.teampls.shoplus.lib.enums.DBResultType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-12-18.
 */

public class LocalImageDB extends BaseDB<ImageRecord> {
    protected static int sizeLimit = 1000;

    public enum Column {
        _id, itemId(MyQuery.INT), remotePath, localPath, image(MyQuery.BLOB),
        imageId(MyQuery.INT);

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }

        public static String[] toStrNoImage() {
            List<String> results = new ArrayList<>();
            for (Column column : Column.values()) {
                if (column.toString().equals("image"))
                    continue;
                results.add(column.toString());
            }
            return results.toArray(new String[0]);
        }

        private String attribution = MyQuery.TEXT_TYPE;
        Column(){};
        Column(String attribution) {
            this.attribution = attribution;
        }

        public static String[] getAttributions() {
            String[] attributions = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++)
                attributions[i] = Column.values()[i].attribution;
            return attributions;
        }
    }

    protected LocalImageDB(Context context, String DBname, int DBversion, String[] columns) {
        super(context, DBname,DBversion, columns);
        columnAttrs = Column.getAttributions();
        cleanUpBySize(sizeLimit);
    }

    public DBResult insert(ImageRecord record) {
        if (isOpen() == false) open();
        if (record.image.length/1000 >= 2000) { // 2MB
            Log.e("DEBUG_JS", String.format("[ImageDB.insertAll] blob %dkB >= 2000kB", (int) (record.image.length/1000)));
            return new DBResult(0, DBResultType.INSERT_FAILED);
        }
        return super.insert(record);
    }

    @Override
    protected int getId(ImageRecord record) {
        return record.id;
    }

    @Override
    protected ImageRecord getEmptyRecord() {
        return new ImageRecord();
    }


    @Override
    protected ImageRecord createRecord(Cursor cursor) {
        return  new ImageRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getInt(Column.itemId.ordinal()),
                cursor.getInt(Column.imageId.ordinal()),
                cursor.getString(Column.localPath.ordinal()),
                cursor.getString(Column.remotePath.ordinal()),
                cursor.getBlob(Column.image.ordinal()));
    }

    @Override
    protected ContentValues toContentvalues(ImageRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.itemId.toString(), record.itemId);
        contentvalues.put(Column.imageId.toString(), record.imageId);
        contentvalues.put(Column.remotePath.toString(), record.remotePath);
        contentvalues.put(Column.localPath.toString(), record.localPath);
        contentvalues.put(Column.image.toString(), record.image);
        return contentvalues;
    }

    public boolean has(String remotePath) {
        return (getStrings(Column.remotePath).contains(remotePath)); // image를 가져오는 것을 방지
    }

    public ImageRecord getRecordBy(String remotePath) {
        return getRecord(getId(Column.remotePath, remotePath));
    }

    public void deleteBy(String remotePath) {
        delete(getRecordBy(remotePath).id);
    }

    public DBResult updateOrInsertByRemotePath(ImageRecord record) {
        if (this.hasValue(Column.remotePath, record.remotePath)) {
            ImageRecord dbRecord = getRecordBy(record.remotePath); // key
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return  new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public ImageRecord getNoImageRecord() {
        Cursor cursor = getCursor(Column.toStrNoImage(), null, null, null, null, null, "");
        if (cursor == null)
            return new ImageRecord();
        ImageRecord record = getNoImageRecord(cursor);
        cursor.close();
        return record;
    }

    public List<ImageRecord> getNoImageRecords() {
        return  getNoImageRecords(getCursor());
    }

    private List<ImageRecord> getNoImageRecords(Cursor cursor) {
        List<ImageRecord> records = new ArrayList<>(0);
        if (cursor == null) return records;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return records;
        }
        cursor.moveToFirst();
        do {
            records.add(getNoImageRecord(cursor));
        } while (cursor.moveToNext());
       cursor.close();
        return records;
    }

    private ImageRecord getNoImageRecord(Cursor cursor) {
        ImageRecord result = new ImageRecord();
        if (cursor == null) {
            Log.e("DEBUG_JS", String.format("[ImageDB.getNoImageRecord] cursor == null"));
            return result;
        }

        if (cursor.getCount() <= 0) {
            return result;
        }
        int id = cursor.getInt(Column._id.ordinal());
        if (id == 0) {
            return result;
        }

        return new ImageRecord(id,
                cursor.getInt(Column.itemId.ordinal()),
                cursor.getInt(Column.imageId.ordinal()),
                cursor.getString(Column.localPath.ordinal()),
                cursor.getString(Column.remotePath.ordinal()),
                Empty.bytes
        );
    }

}
