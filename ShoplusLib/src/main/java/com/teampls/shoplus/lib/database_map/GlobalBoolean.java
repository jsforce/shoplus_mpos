package com.teampls.shoplus.lib.database_map;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.database_global.GlobalDB;

/**
 * Created by Medivh on 2018-11-19.
 */

public class GlobalBoolean {
    public String key = "";
    public boolean defaultValue = false;

    public GlobalBoolean(String key) {
        this(key, false);
    }

    public GlobalBoolean(String key, boolean defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    public void put(Context context, boolean value) {
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE))
            GlobalDB.getInstance(context).put(key, value);
        else
            Log.e("DEBUG_JS", String.format("[GlobalBoolean.put] WRITE_STORAGE NOT Permitted"));
    }

    public boolean get(Context context) {
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE))
            return GlobalDB.getInstance(context).getBool(key, defaultValue);
        else {
            Log.e("DEBUG_JS", String.format("[GlobalBoolean.put] WRITE_STORAGE NOT Permitted"));
            return defaultValue;
        }
    }


}
