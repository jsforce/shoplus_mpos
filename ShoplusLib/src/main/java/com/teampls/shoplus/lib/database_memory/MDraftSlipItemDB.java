package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

/**
 * Created by Medivh on 2019-03-06.
 */

public class MDraftSlipItemDB extends MemorySlipItemDB {
    private static MDraftSlipItemDB instance;

    public static MDraftSlipItemDB getInstance(Context context) {
        if (instance == null)
            instance = new MDraftSlipItemDB(context);
        return instance;
    }

    private MDraftSlipItemDB(Context context) {
        super(context, "MDraftSlipItemDB");
    }
}
