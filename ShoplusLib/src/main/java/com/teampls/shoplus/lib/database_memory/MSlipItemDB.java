package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

/**
 * Created by Medivh on 2018-04-18.
 */

public class MSlipItemDB extends MemorySlipItemDB {
    private static MSlipItemDB instance;

    public static MSlipItemDB getInstance(Context context) {
        if (instance == null)
            instance = new MSlipItemDB(context);
        return instance;
    }

    private MSlipItemDB(Context context) {
        super(context, "MSlipItemDB");
    }

}
