package com.teampls.shoplus.lib.protocol;

import org.joda.time.DateTime;

import java.util.List;

/**
 * 거래 초안(공유용) 데이터 클래스 프로토콜
 *
 * @author lucidite
 */

public interface TransactionDraftDataProtocol {
    long getDraftId();
    long getVersionId();
    DateTime getCreatedDatetime();
    DateTime getUpdatedDatetime();
    String getRecipient();
    List<SlipItemDataProtocol> getTransactionItems();
    List<TransactionDraftMemoDataProtocol> getMemoList();
}
