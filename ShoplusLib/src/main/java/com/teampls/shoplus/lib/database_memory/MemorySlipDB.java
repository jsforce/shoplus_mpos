package com.teampls.shoplus.lib.database_memory;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-03-31.
 */

public class MemorySlipDB extends BaseMapDB<String, SlipRecord> {
    public MemorySlipItemDB items;

    protected MemorySlipDB(Context context, String dbName) {
        super(context, dbName);
    }

    @Override
    public String getKeyValue(SlipRecord record) {
        return record.getSlipKey().toSID();
    }


    @Override
    protected SlipRecord getEmptyRecord() {
        return new SlipRecord();
    }

    @Override
    public void toLogCat(String callLocation) {
        for (SlipRecord record : getRecords()) {
            Log.i("DEBUG_JS", String.format("======================="));
            record.toLogCat(callLocation);
            for (SlipItemRecord itemRecord : items.getRecordsBy(record.getSlipKey().toSID()))
                itemRecord.toLogCat(callLocation);
        }
    }

    public void set(List<SlipDataProtocol> protocols, boolean doIncludeCancelled) {
        clear();
        for (SlipDataProtocol protocol : protocols) {
            SlipFullRecord fullRecord = new SlipFullRecord(protocol);
            if (doIncludeCancelled == false)
                if (fullRecord.isCancelled())
                    continue;
            insert(fullRecord.record);
            items.insertAll(fullRecord.items);
        }
    }

    public void refresh(List<SlipDataProtocol> protocols, DateTime from, DateTime to) {
        // delete
        List<SlipRecord> betweenRecords = new ArrayList<>();
        List<SlipItemRecord> betweenItemRecords = new ArrayList<>();
        for (SlipFullRecord fullRecord : getFullRecords()) {
            if (BaseUtils.isBetween(fullRecord.record.createdDateTime, from, to)) {
                betweenRecords.add(fullRecord.record);
                betweenItemRecords.addAll(fullRecord.items);
            }
        }
        deleteAll(betweenRecords);
        items.deleteAll(betweenItemRecords);

        // insertAll
        for (SlipDataProtocol protocol : protocols) {
            SlipFullRecord fullRecord = new SlipFullRecord(protocol);
            insert(fullRecord.record);
            items.insertAll(fullRecord.items);
        }
    }

    public List<SlipFullRecord> getFullRecordsBetween(DateTime from, DateTime to, boolean doExcludeCancelled) {
        List<SlipFullRecord> fullRecords = new ArrayList<>();
        for (SlipFullRecord fullRecord : getFullRecords()) {
            if (doExcludeCancelled)
                if (fullRecord.record.cancelled)
                    continue;
            if (BaseUtils.isBetween(fullRecord.record.createdDateTime, from, to))
                fullRecords.add(fullRecord);
        }
        return fullRecords;
    }

    public List<SlipFullRecord> getFullRecordsBy(List<SlipRecord> records, boolean doExcludeCancelled) {
        List<SlipFullRecord> results = new ArrayList<>();
        for (SlipRecord record : records) {
            if (doExcludeCancelled)
                if (record.cancelled)
                    continue;
            results.add(new SlipFullRecord(record, items.getRecordsBy(record.getSlipKey().toSID())));
            ;
        }
        return results;

    }

    public List<SlipRecord> getRecordsBetween(DateTime from, DateTime to, boolean doExcludeCancelled) {
        List<SlipRecord> records = new ArrayList<>();
        for (SlipRecord record : getRecords()) {
            if (doExcludeCancelled)
                if (record.cancelled)
                    continue;
            if (BaseUtils.isBetween(record.createdDateTime, from, to))
                records.add(record);
        }
        return records;
    }

    private void insert(List<SlipDataProtocol> protocols) {
        for (SlipDataProtocol protocol : protocols) {
            SlipFullRecord fullRecord = new SlipFullRecord(protocol);
            insert(fullRecord.record);
            items.insertAll(fullRecord.items);
        }
    }

    private void deleteBetween(DateTime from, DateTime to) {
        List<SlipRecord> betweenRecords = new ArrayList<>();
        List<SlipItemRecord> betweenItemRecords = new ArrayList<>();
        for (SlipFullRecord fullRecord : getFullRecords()) {
            if (BaseUtils.isBetween(fullRecord.record.createdDateTime, from, to)) {
                betweenRecords.add(fullRecord.record);
                betweenItemRecords.addAll(fullRecord.items);
            }
        }
        deleteAll(betweenRecords);
        items.deleteAll(betweenItemRecords);
    }

    public void refreshDay(List<SlipDataProtocol> protocols, DateTime day) {
        day = BaseUtils.toDay(day);
        deleteBetween(day, day.plusDays(1).minusMillis(1));
        insert(protocols);
    }

    public void refreshMonth(List<SlipDataProtocol> protocols, DateTime month) {
        month = BaseUtils.toMonth(month);
        deleteBetween(month, month.plusMonths(1).minusMillis(1));
        insert(protocols);
    }

    public void refreshBefore(List<SlipDataProtocol> protocols, DateTime month) {
        month = BaseUtils.toMonth(month);
        deleteBetween(Empty.dateTime, month.minusMillis(1));
        insert(protocols);
    }

    public void insert(SlipFullRecord fullRecord) {
        insert(fullRecord.record);
        items.insertAll(fullRecord.items);
    }

    public void insertPendings(SlipFullRecord fullRecord) {
        insert(fullRecord.record);
        items.insertPendings(fullRecord.items);
    }


    public void updateOrInsert(List<SlipFullRecord> slipFullRecords, boolean doExcludeCancelled) {
        for (SlipFullRecord fullRecord : slipFullRecords) {
            if (doExcludeCancelled)
                if (fullRecord.isCancelled())
                    continue;
            updateOrInsert(fullRecord.record);
            items.updateOrInsert(fullRecord.record.getSlipKey().toSID(), fullRecord.items);
        }
    }

    public void updateOrInsert(SlipFullRecord slipFullRecord) {
        updateOrInsert(slipFullRecord.record);
        items.updateOrInsert(slipFullRecord.record.getSlipKey().toSID(), slipFullRecord.items);
    }

    public void insert(List<SlipFullRecord> slipFullRecords, DateTime fromDate, DateTime toDate, boolean doExcludeCancelled) {
        for (SlipFullRecord fullRecord : slipFullRecords) {
            if (doExcludeCancelled)
                if (fullRecord.isCancelled())
                    continue;
            if (fullRecord.record.createdDateTime.isBefore(fromDate))
                continue;
            if (fullRecord.record.createdDateTime.isAfter(toDate))
                continue;
            insert(fullRecord.record);
            items.insertAll(fullRecord.items);
        }
    }

    public SlipFullRecord getFullRecord(String slipKey) {
        return new SlipFullRecord(getRecordByKey(slipKey), items.getRecordsBy(slipKey));
    }

    public List<SlipFullRecord> getFullRecords() {
        List<SlipFullRecord> results = new ArrayList<>();
        for (SlipRecord record : getRecords()) {
            results.add(new SlipFullRecord(record, items.getRecordsBy(record.getSlipKey().toSID())));
        }
        return results;
    }

    public void clear() {
        items.clear();
        super.clear();
    }

    public void deleteByKey(String slipKey) {
        items.deleteBy(slipKey);
        super.deleteByKey(slipKey);
    }

    public boolean has(String key) {
        return databaseMap.containsKey(key);
    }

    public MyMap<String, SlipRecord> getMap() {
        return databaseMap;
    }

}
