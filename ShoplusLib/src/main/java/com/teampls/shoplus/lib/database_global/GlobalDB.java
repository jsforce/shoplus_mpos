package com.teampls.shoplus.lib.database_global;

/**
 * Created by Medivh on 2016-04-12.
 */

import android.content.Context;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_map.GlobalBoolean;
import com.teampls.shoplus.lib.database_map.GlobalDBDouble;
import com.teampls.shoplus.lib.database_map.GlobalDBInt;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.datatypes.MessengerLinkData;
import com.teampls.shoplus.lib.enums.MessengerType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.enums.StockManagementType;
import com.teampls.shoplus.lib.enums.MemberPermission;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class GlobalDB extends KeyValueDB {
    private static GlobalDB instance = null;
    private static int Ver = 1;

    private static final String KEY_STOCK_MANAGEMENT_TYPE = "globalDB.stockManagementType";

    // User Setting
    public static final String LOCAL_KEY_MY_LOCATION = "globalDB.my.location"; // 현재 내 위치
    public static final String KEY_PublisherPhoneNumber = "globalDB.publisher.phoneNumber";

    // Receipt 영수증 화면
    public static String KEY_RECEIPT_EnlargeCounterpart = "globalDB.receipt.enlargeCounterpart";
    public static String KEY_RECEIPT_ShowPreorderSums = "globalDB.receipt.showPreorderSums";
    public static String KEY_RECEIPT_ShowQuantitySum = "globalDB.receipt.showQuantitySum";
    public static String KEY_RECEIPT_UseHangulQuantity = "globalDB.receipt.useHangulQuantity";
    public static String KEY_RECEIPT_ShowNotifTalk = "globalDB.receipt.showNotifTalk";
    public static String KEY_RECEIPT_ShowDeposit = "globalDB.receipt.showBalance";
    public static GlobalBoolean doRequestBillAutomatically = new GlobalBoolean("globalDB.autoAdd.receivable");
    public static GlobalBoolean doShowReceivables = new GlobalBoolean("globalDB.receipt.showReceivables", true);
    public static GlobalBoolean doShowPlusMinusSum = new GlobalBoolean("globalDB.receipt.showPlusMinusSum", true);
    public static GlobalBoolean doExcludeKeywordsFromQuantity = new GlobalBoolean("globalDB.receipt.excludeKeywordsFromQuantity", true);
    public static GlobalBoolean doShowReceivablesToMemo = new GlobalBoolean("globalDB.receipt.showReceivablesToMemo");

    // 명함 화면
    public static final GlobalBoolean doEnlargePhoneNumberNameCard = new GlobalBoolean("globalDB.namecard.largePhoneNumber", true);
    public static final GlobalBoolean doAddMemoSpaceNameCard = new GlobalBoolean("globalDB.namecard.addMemoSpace");

    // 영수증 관련
    public static String[] keywordsNoQuantity = {SlipItemType.DeliveryName, SlipItemType.VALUE_ADDED_TAX.uiName};
    public static GlobalDBInt myDeliveryCost = new GlobalDBInt("myshop.delivery", 3000);

    // Printer
    private final String KEY_PRINTER_PRODUCTNAME = "globalDB.printer.current.product.name";
    public static final String KEY_PRINTER_SERIAL = "globalDB.printer.serial";
    public static final String KEY_PRINTER_SEPARATOR = "globalDB.printer.separator";
//    public static final String KEY_PRINTER_DEPTS = "globalDB.printer.depts";
    public static final String KEY_PRINTER_MEMO = "globalDB.printer.memo";
    public static final String KEY_PRINTER_QR_Kakao = "globalDB.printer.qr.kakao";
    public static final String KEY_PRINTER_QR_Wechat = "globalDB.printer.qr.wechat";
    public static final String KEY_PRINTER_QR_Image = "globalDB.printer.qr.image";
    public static final String KEY_PRINTER_QR_Kakao_URL = "globalDB.printer.qr.kakao.url";
    public static final String KEY_PRINTER_QR_Wechat_URL = "globalDB.printer.qr.wechat.url";
    public static final String KEY_PRINTER_QR_PlusFriend_URL = "globalDB.printer.qr.plusfriend.url";
    public static final String KEY_PRINTER_QR_Image_Path = "globalDB.printer.qr.image.filepath";
    private static final String KEY_PRINTER_QR_Size = "globalDB.printer.qrSize";
    public static final String KEY_PRINTER_Selected = "globalDB.printer.selected";
    public static final String KEY_PRINTER_Address_Selected = "globalDB.printer.address.selected";

    // Time
    public static final String KEY_IS_CUSTOM_DATE = "globalDB.isCustomDate";
    public static final String KEY_CUSTOM_DATE = "globalDB.customDate";

    // WorkingTime
    public static final GlobalBoolean IsDayAndNightWorking = new GlobalBoolean("globalDB.isDayAndNight.working", false);
    public static final GlobalDBDouble DayStartTime = new GlobalDBDouble("globalDB.day.startTime", 0);
    public static final GlobalDBDouble NightStartTime = new GlobalDBDouble("globalDB.night.startTime", 24);

    public enum QRSize {small, large}

    private GlobalDB(Context context) {
        super(context, "GlobalDB", Ver, Column.toStrs());
    }

    public static GlobalDB getInstance(Context context) {
        if (instance == null)
            instance = new GlobalDB(context);
        return instance;
    }

    public List<String> getNoQuantityKeywords() {
        List<String> noQuantityKeywords = new ArrayList<>();
        if (GlobalDB.doExcludeKeywordsFromQuantity.get(context))
            noQuantityKeywords = BaseUtils.toList(GlobalDB.keywordsNoQuantity);
        return noQuantityKeywords;
    }


    public DBHelper getDBHelper() {
        return new DBHelper(context, getTeamPlPath());
    }

    public String getMessengerValue(MessengerType type) {
        switch (type) {
            case KAKAOTALK:
                return getValue(GlobalDB.KEY_PRINTER_QR_Kakao_URL).replace(MessengerLinkData.kakaoHeader, "");
            case WECHAT:
                return getValue(GlobalDB.KEY_PRINTER_QR_Wechat_URL).replace(MessengerLinkData.weChatHeader,"");
            case PLUSFRIEND:
                return getValue(GlobalDB.KEY_PRINTER_QR_PlusFriend_URL).replace(MessengerLinkData.plusHeader,"").replace("?from=qr","");
        }
        return "";
    }

    public void setMessengerUrl(MessengerType type, String url) {
        switch (type) {
            case KAKAOTALK:
                put(GlobalDB.KEY_PRINTER_QR_Kakao_URL, url);
                put(GlobalDB.KEY_PRINTER_QR_PlusFriend_URL, "");
                break;
            case WECHAT:
                put(GlobalDB.KEY_PRINTER_QR_Wechat_URL, url);
                break;
            case PLUSFRIEND:
                put(GlobalDB.KEY_PRINTER_QR_Kakao_URL, "");
                put(GlobalDB.KEY_PRINTER_QR_PlusFriend_URL, url);
                break;
            default:
                break;
        }
    }

    public DateTime getCreatedDateTime(UserSettingData userSettingData) {
        DateTime createdDateTime = DateTime.now();

        if (MemberPermission.SetTransactionDate.hasPermission(userSettingData) == false)
            return createdDateTime.plusHours(userSettingData.getProtocol("createDateTime").getTimeShift());

        if (getBool(GlobalDB.KEY_IS_CUSTOM_DATE) == false)
            return createdDateTime.plusHours(userSettingData.getProtocol("createDateTime").getTimeShift());

        createdDateTime = BaseUtils.toDateTime(getValue(GlobalDB.KEY_CUSTOM_DATE));
        createdDateTime = BaseUtils.createCustomDateTime(createdDateTime, createdDateTime.getHourOfDay());
        return createdDateTime;
    }

    public QRSize getQRSize() {
        return QRSize.valueOf(getValue(KEY_PRINTER_QR_Size, QRSize.large.toString()));
    }

    public void setQRSize(QRSize qrSize) {
        put(KEY_PRINTER_QR_Size, qrSize.toString());
    }

    @Deprecated
    private void setCurrentProductName(String currentProductName) {
        put(KEY_PRINTER_PRODUCTNAME, currentProductName);
    }

    @Deprecated
    private String getCurrentProductName() {
        return getValue(KEY_PRINTER_PRODUCTNAME);
    }

    public StockManagementType getStockManagementType() {
        return StockManagementType.valueOf(getValue(KEY_STOCK_MANAGEMENT_TYPE, StockManagementType.manual.toString()));
    }

    public void setStockManagementType(StockManagementType stockManagementType) {
        put(KEY_STOCK_MANAGEMENT_TYPE, stockManagementType.toString());
    }

}
