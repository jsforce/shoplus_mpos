package com.teampls.shoplus.lib.database_memory;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.common.Empty;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Medivh on 2018-04-22.
 */

abstract public class BaseMemoryDB<T> {
    protected String DB_NAME = "";
    public Context context;

    abstract protected T getEmptyRecord();

    public void toLogCat(String callLocation){
        Log.w("DEBUG_JS", String.format("[BaseMemoryDB.toLogCat] %s, abstract class", callLocation));
    };

}
