package com.teampls.shoplus.lib.database_memory;

import com.teampls.shoplus.lib.database_map.KeyValueDB;

/**
 * Created by Medivh on 2018-05-01.
 */

public enum MSortingKey {
  Date, Name, Type, Value, SecondValue, CounterpartName, ID, Count, MultiField, Scheduled;

  public static MSortingKey valueOf(KeyValueDB keyValueDB, String key, MSortingKey defaultValue) {
      try {
          return MSortingKey.valueOf(keyValueDB.getValue(key, defaultValue.toString()));
      } catch (IllegalArgumentException e) {
          return defaultValue;
      }
  }

}
