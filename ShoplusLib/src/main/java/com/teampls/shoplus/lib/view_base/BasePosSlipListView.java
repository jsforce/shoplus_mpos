package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseSlipDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.PosAndSourceLinkDB;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.view.DraftSlipListView;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import com.teampls.shoplus.lib.view_module.MyCheckBox;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Medivh on 2017-05-22.
 */


// onResume 마다 무조건 updateOrInsert
abstract public class BasePosSlipListView extends BaseActivity implements AdapterView.OnItemClickListener {
    protected BaseSlipDBAdapter adapter;
    protected ListView lvSlips;
    protected int periodLimit = 3; // days
    protected PosSlipDB posSlipDB;
    protected MyEmptyGuide myEmptyGuide;
    protected MyCheckBox cb7Days;
    private final String KEY_Show7Days = "slipListView.show.7Days";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        lvSlips = (ListView) findViewById(R.id.slip_main_slips);
        setSlipDB();
        setAdapters();
        lvSlips.setAdapter(adapter);
        lvSlips.setOnItemClickListener(this);
        myEmptyGuide = new MyEmptyGuide(context, getView(), adapter, lvSlips,
                R.id.slip_main_empty_container, R.id.slip_main_empty_message, R.id.slip_main_empty_contactUs);
        setEmptyGuide();
        setOnClick(R.id.slip_main_close, R.id.slip_main_new);

        if (keyValueDB.getBool(KEY_Show7Days))
            periodLimit = 7;

        cb7Days = new MyCheckBox(keyValueDB, getView(), R.id.slip_main_7days, KEY_Show7Days, false);
        cb7Days.setOnClick(false, new MyOnClick<Boolean>() {
            @Override
            public void onMyClick(View view, Boolean checked) {
                if (checked) {
                    periodLimit = 7;
                    downloadRecentSlips();
                    MyUI.toastSHORT(context, String.format("앱이 느려질 수 있습니다"));
                } else {
                    periodLimit = 3;
                    refreshSlipDB();
                    refresh();
                }
            }
        });

        MyView.setTextViewByDeviceSize(context, getView(), R.id.slip_main_7days, R.id.slip_main_notice);
    }

    public void setEmptyGuide() {
        myEmptyGuide.setMessage("거래 기록이 없습니다.\n아래 [새거래] 버튼을 눌러 주세요");
        myEmptyGuide.setContactUsButton("새거래", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onApplyClick();
            }
        });
    }

    abstract public void setAdapters();

    public void setSlipDB() {
        posSlipDB = PosSlipDB.getInstance(context);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshSlipDB();
        refresh();
    }

    protected void refreshSlipDB() {
        DateTime fromDateTime = DateTime.now().minusDays(periodLimit);
        for (SlipRecord posSlipRecord : posSlipDB.getRecords()) {
            if (posSlipRecord.cancelled) {
                posSlipDB.deleteFromDBs(posSlipRecord.getSlipKey());
            } else if (posSlipRecord.createdDateTime.isBefore(fromDateTime)) {
                if (posSlipRecord.confirmed) {//confirmed 되지 않은 것은 남겨두자.
                    posSlipDB.deleteFromDBs(posSlipRecord.getSlipKey());
                }
            }
        }
    }

    @Override
    public int getThisView() {
        return R.layout.base_slip_main;
    }

    public void refresh() {

        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                adapter.generate(false);
            }

            @Override
            public void onPostExecutionUI() {
                adapter.applyGeneratedRecords();
                adapter.notifyDataSetChanged();
                myEmptyGuide.refresh();
            }
        };
    }

    protected void downloadBuyers() {
        new CommonServiceTask(context, "downloadRequestedBuyers", "거래처 정보 다운로드 중...") {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                userDB.update(workingShopService.getContactUpdates(userSettingData.getUserShopInfo(), userDB.getLatestTimeStamp()));
            }

            @Override
            public void onPostExecutionUI() {
                refresh();
            }

        };

    }

    ;

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.slip_main_close) {
            doFinishForResult();
        } else if (view.getId() == R.id.slip_main_new) {
            onApplyClick();
        }
    }

    public void onApplyClick() {
        Log.e("DEBUG_JS", String.format("[BasePosSlipListView.onApplyClick] Abstract"));
    }

    protected abstract void cancelSlipManually(SlipDataKey slipDataKey, boolean doCopy);

    public class CancelSlipDialog extends BaseDialog {
        private EditText etKey;

        public CancelSlipDialog(Context context) {
            super(context);
            setDialogWidth(0.95, 0.6);
            etKey = (EditText) findViewById(R.id.dialog_cancel_slip_key);
            etKey.addTextChangedListener(new MyTextWatcher());
            MyDevice.showKeyboard(context, etKey);
            setOnClick(R.id.dialog_cancel_slip_cancel, R.id.dialog_cancel_slip_copyAndCancel,
                    R.id.dialog_cancel_slip_clear, R.id.dialog_cancel_slip_close);
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_cancel_slip;
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.dialog_cancel_slip_copyAndCancel) {
                onCancelSlip(true);
            } else if (view.getId() == R.id.dialog_cancel_slip_cancel) {
                new MyAlertDialog(context, "복사본 없이 삭제", "바로 취소를 하시면 복사본을 남기지 않습니다. 취소 하시겠습니까?") {
                    @Override
                    public void yes() {
                        onCancelSlip(false);
                    }
                };

            } else if (view.getId() == R.id.dialog_cancel_slip_clear) {
                etKey.setText("");
            } else if (view.getId() == R.id.dialog_cancel_slip_close) {
                MyDevice.hideKeyboard(context, etKey);
                dismiss();
            }
        }

        private void onCancelSlip(boolean doCopy) {
            String number = etKey.getText().toString().replace("-", "");
            if (number.length() < 15) {
                MyUI.toastSHORT(context, String.format("증빙번호는 총 15자리 입니다"));
                return;
            }

            final DateTime createdDateTime;
            try {
                createdDateTime = BaseUtils.KEY_Format.parseDateTime(etKey.getText().toString());
            } catch (IllegalArgumentException e) {
                MyUI.toastSHORT(context, String.format("증빙번호 형식이 올바르지 않습니다"));
                return;
            } catch (Exception e) {
                MyUI.toastSHORT(context, String.format("증빙번호 형식이 올바르지 않습니다"));
                return;
            }

            SlipDataKey slipDataKey = new SlipDataKey(userSettingData.getProviderRecord().phoneNumber, createdDateTime);
            cancelSlipManually(slipDataKey, doCopy);
            MyDevice.hideKeyboard(context, etKey);
            dismiss();
        }

        class MyTextWatcher extends BaseTextWatcher {
            public boolean activated = true;

            @Override
            public void afterTextChanged(Editable s) {
                if (activated == false) return;
                activated = false;
                String number = s.toString().replace("-", "");
                if (number.length() <= 6) {
                    //
                } else if (number.length() <= 10) {
                    etKey.setText(String.format("%s-%s", number.substring(0, 6), number.substring(6, number.length())));
                } else if (number.length() <= 15) {
                    etKey.setText(String.format("%s-%s-%s", number.substring(0, 6), number.substring(6, 10), number.substring(10, number.length())));
                } else {
                    etKey.setText(String.format("%s-%s-%s", number.substring(0, 6), number.substring(6, 10), number.substring(10, 15)));
                }
                etKey.setSelection(etKey.getText().toString().length());
                activated = true;
                return;
            }
        }
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case refresh:
                new RefreshClickDialog(context).show();
                break;
            case close:
                doFinishForResult();
                break;
            case cancelSlip:
                new CancelSlipDialog(context).show();
                break;
            case openDrafts:
                DraftSlipListView.startActivity(context);
                break;
        }
    }

    class RefreshClickDialog extends MyButtonsDialog {

        public RefreshClickDialog(final Context context) {
            super(context, "영수증 정보 새로 고침", "최근 발행한 영수증 목록을 가져옵니다. 작성중인 영수증은 어떻게 할까요?");
            addButton("작성중 보존 (추천)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    downloadRecentSlips();
                }
            });

            addButton("작성중 삭제 (주의)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new MyAlertDialog(context, "[주의] 작성중 삭제", "작성중인 모든 영수증은 삭제하시겠습니까?") {
                        @Override
                        public void yes() {
                            dismiss();
                            posSlipDB.deleteAll(PosSlipDB.Column.confirmed, false);
                            downloadRecentSlips();
                        }
                    };
                }
            });

            addButton("긴급 복구 (문의후사용)", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    posSlipDB.recoverFromOldDB();
                    downloadRecentSlips();
                }
            });
        }
    }

    protected void downloadRecentSlips() {
        new CommonServiceTask(context, "downloadRecentSlips", "거래 기록 다운로드 중...") {
            boolean isNewBuyerFound = false;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                posSlipDB.checkLinkIntegrity();

                List<SlipDataProtocol> protocols = transactionService.getRecentTransactions(userSettingData.getUserShopInfo());
                posSlipDB.refreshConfirmeds(protocols);
                posSlipDB.recoverClaimedReceivableFromOldDB(); // 잔금 추가한 내용은 서버에 없을 수 있음

                // check if new buyer is found
                for (SlipDataProtocol protocol : protocols)
                    if (userDB.has(protocol.getCounterpartPhoneNumber()) == false) {
                        isNewBuyerFound = true;
                        break;
                    }
            }

            @Override
            public void onPostExecutionUI() {
                if (isNewBuyerFound) {
                    MyUI.toastSHORT(context, String.format("새 거래처 발견"));
                    downloadBuyers();
                } else {
                    refresh();
                }
            }

        };
    }


}