package com.teampls.shoplus.lib.common;

import android.content.Context;

import com.teampls.shoplus.lib.ProjectAccountServiceManager;
import com.teampls.shoplus.lib.ProjectAdminServiceManager;
import com.teampls.shoplus.lib.ProjectItemServiceManager;
import com.teampls.shoplus.lib.ProjectMainUserServiceManager;
import com.teampls.shoplus.lib.ProjectUserServiceManager;
import com.teampls.shoplus.lib.awsservice.ProjectAccountServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectAdminServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectItemServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectMainUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectTransactionServiceProtocol;
import com.teampls.shoplus.lib.awsservice.ProjectUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectAccountService;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectAdminService;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectItemService;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectMainUserService;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectTransactionService;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectUserService;
import com.teampls.shoplus.lib.awsservice.implementations.PublicAPIService;

/**
 * Created by Medivh on 2017-05-02.
 */

abstract public class CommonServiceTask extends BaseServiceTask {

    public CommonServiceTask(Context context, String location) {
        super(context, location);
    }

    public CommonServiceTask(Context context, String location, String progressMessage) {
        super(context, location, progressMessage);
    }

    @Override
    public void setService() {
    }
}