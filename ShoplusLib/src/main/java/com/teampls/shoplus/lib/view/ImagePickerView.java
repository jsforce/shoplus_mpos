package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.ImagePickerAdapter;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_map.KeyValueInt;
import com.teampls.shoplus.lib.database_map.KeyValueString;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.yongchun.library.adapter.ImageFolderAdapter;
import com.yongchun.library.model.LocalMedia;
import com.yongchun.library.model.LocalMediaFolder;
import com.yongchun.library.utils.GridSpacingItemDecoration;
import com.yongchun.library.utils.LocalMediaLoader;
import com.yongchun.library.utils.ScreenUtils;
import com.yongchun.library.view.ImageCropActivity;
import com.yongchun.library.view.ImagePreviewActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Medivh on 2017-12-11.
 */

public class ImagePickerView extends AppCompatActivity implements View.OnClickListener,
        ImagePickerAdapter.OnImageSelectChangedListener {
    public static ImagePickerView instance;
    public final static int REQUEST_CAMERA = 67;
    public final static int MODE_MULTIPLE = 1;
    public final static int MODE_SINGLE = 2;
    public final static String BUNDLE_CAMERA_PATH = "CameraPath";
    private Context context;
    private static boolean isShow = false, enableCrop = false, showCamera = true;
    private static int maxSelectNum = 100;
    private TextView tvFolderName, tvSelectedCount, tvMessage;
    private LinearLayout header;
    private ImageFolderWindow folderWindow;
    private String cameraPath = "", selectedFolderName = "전 체";
    private ImagePickerAdapter adapter;
    private RecyclerView recyclerView;
    private LocalMediaLoader localMediaLoader; // 이게 라이브러리
    private List<LocalMediaFolder> imageFolders;
    private static MyOnClick<List<String>> onApplyClick;
    private int selectMode = MODE_MULTIPLE;
    private static boolean doFinishOnSelection = true, enableNoSelection = false, selectionOrderEnabled = false;
    private static String message = "";

    public static void start(Context context, int maxSelectNum, boolean doFinishOnSelection, MyOnClick<List<String>> onApplyClick) {
        start(context, false,maxSelectNum, doFinishOnSelection, false, onApplyClick);
    }

    public static void start(Context context, boolean selectionOrderEnabled, int maxSelectNum, boolean doFinish,
                             boolean enableNoSelection, MyOnClick<List<String>> onApplyClick) {
        setMessage("");
        ImagePickerView.selectionOrderEnabled = selectionOrderEnabled;
        ImagePickerView.maxSelectNum = maxSelectNum;
        ImagePickerView.onApplyClick = onApplyClick;
        ImagePickerView.doFinishOnSelection = doFinish;
        ImagePickerView.enableNoSelection = enableNoSelection;
        context.startActivity(new Intent(context, ImagePickerView.class));
    }

    public static void setMessage(String message) {
        ImagePickerView.message = message;
    }

    private int getThisView() {
        return R.layout.activity_image_picker;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_CAMERA_PATH, cameraPath);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        setContentView(getThisView());
        if (savedInstanceState != null)
            cameraPath = savedInstanceState.getString(BUNDLE_CAMERA_PATH);
        context = this;
        instance = this;
        if (selectMode == MODE_MULTIPLE)
            enableCrop = false;

        adapter = new ImagePickerAdapter(this, maxSelectNum, selectMode, showCamera, false);
        adapter.enableOrder(selectionOrderEnabled);
        adapter.setOnImageSelectChangedListener(this);

        folderWindow = new ImageFolderWindow(this);
        folderWindow.setOnItemClickListener(new ImageFolderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String folderName, List<LocalMedia> images) {
                folderWindow.dismiss();
                adapter.bindImages(images);
                adapter.notifyDataSetChanged();
                selectedFolderName = folderName;
                refreshViews();
            }
        });

        header = (LinearLayout) findViewById(R.id.image_picker_header);
        tvFolderName = (TextView) findViewById(R.id.image_picker_folder_name);
        tvSelectedCount = (TextView) findViewById(R.id.image_picker_selectedCount);
        tvMessage = (TextView) findViewById(R.id.image_picker_message);
        tvMessage.setText(message);
        tvMessage.setVisibility(message.isEmpty() ? View.GONE : View.VISIBLE);
        findViewById(R.id.image_picker_noselection).setVisibility(enableNoSelection ? View.VISIBLE : View.GONE);

        // GridView
        int columnCount = MyDevice.getColumnNum(context);
        recyclerView = (RecyclerView) findViewById(R.id.image_picker_recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(columnCount , ScreenUtils.dip2px(this, 2), false));
        recyclerView.setLayoutManager(new GridLayoutManager(this, columnCount));
        recyclerView.setAdapter(adapter);

        setOnClick(R.id.image_picker_folder_name, R.id.image_picker_apply,
                R.id.image_picker_close, R.id.image_picker_noselection);

        localMediaLoader = new LocalMediaLoader(this, LocalMediaLoader.TYPE_IMAGE);
        localMediaLoader.loadAllImage(new LocalMediaLoader.LocalMediaLoadListener() {
            @Override
            public void loadComplete(List<LocalMediaFolder> folders) {
                imageFolders = folders;
                folderWindow.bindFolder(folders);
                adapter.bindImages(folders.get(0).getImages());
                refreshViews();
            }
        });

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE) && BasePreference.landScapeOrientationEnabled.get(context))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public void setOnClick(int... resIds) {
        for (int resId : resIds)
            findViewById(resId).setOnClickListener(this);
    }

    private void refreshViews() {
        if (selectedFolderName.equals("All Images") || selectedFolderName.isEmpty())
            selectedFolderName = "전  체";
        tvFolderName.setText(String.format("%s (%d)", selectedFolderName, adapter.getItemCount() - 1)); // 1개는 카메라
        tvSelectedCount.setText(String.format("%d/%d", adapter.getSelectedImages().size(),
                Math.min(adapter.getItemCount() - 1, maxSelectNum)));
    }

    @Override
    public void onClick(final View view) {
        if (view.getId() == R.id.image_picker_folder_name) {
            if (folderWindow.isShowing()) {
                folderWindow.dismiss();
            } else {
                folderWindow.showAsDropDown(header);
            }

        } else if (view.getId() == R.id.image_picker_apply) {
            if (enableNoSelection == false && adapter.getSelectedImages().size() == 0) {
                MyUI.toastSHORT(context, String.format("선택된 이미지가 없습니다"));
                return;
            }

            if (adapter.getSelectedImages().size() == 0) {
                new MyAlertDialog(context, "선택된 이미지 없음", "이미지를 선택하지 않으셨습니다. 계속 진행하시겠습니까?") {
                    @Override
                    public void yes() {
                        if (onApplyClick != null)
                            onApplyClick.onMyClick(view,  new ArrayList<String>());
                        if (doFinishOnSelection)
                            doFinish();
                    }
                };
                return;
            }

            if (onApplyClick != null) {
                List<String> selectedImagePaths = new ArrayList<>();
                for (LocalMedia localMedia : adapter.getSelectedImages())
                    selectedImagePaths.add(localMedia.getPath());
                onApplyClick.onMyClick(view, selectedImagePaths);
            }

            if (doFinishOnSelection)
                doFinish();

        } else if (view.getId() == R.id.image_picker_close) {
            doFinish();

        } else if (view.getId() == R.id.image_picker_noselection) {
            if (onApplyClick != null)
                onApplyClick.onMyClick(view,  new ArrayList<String>());
            if (doFinishOnSelection)
                doFinish();
        }
    }

    protected void doFinish() {
        finish();
    }

    // ==== ImageListAdapter.OnImageSelectChangedListener ===
    @Override
    public void onChange(List<LocalMedia> selectImages) { // onItemClickListener() 와 동일
        refreshViews();
    }

    @Override
    public void onTakePhoto() {
        startCamera();
    }

    @Override
    public void onPictureClick(LocalMedia media, int position) {
//        if (enableCrop) {
//            startCrop(media.getPath());
//        } else {
//            onSelectDone(media.getPath());
//        }
    }
    // ==== ImageListAdapter.OnImageSelectChangedListener ===

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;
        // on take photo success
        switch (requestCode) {
            case REQUEST_CAMERA:
                onCameraFinished();
                break;
            case ImageCropActivity.REQUEST_CROP:
                String path = data.getStringExtra(ImageCropActivity.OUTPUT_PATH);
                //        onSelectDone(path);
                break;
            case ImagePreviewActivity.REQUEST_PREVIEW:
                boolean isDone = data.getBooleanExtra(ImagePreviewActivity.OUTPUT_ISDONE, false);
                List<LocalMedia> images = (List<LocalMedia>) data.getSerializableExtra(ImagePreviewActivity.OUTPUT_LIST);
                if (isDone) {
                    //                  onSelectDone(images);
                } else {
                    adapter.bindSelectImages(images);
                }
                break;
        }
    }

    /**
     * start to camera、preview、crop
     */
    public void startCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File cameraFile = MyDevice.createImageFile();
            cameraPath = cameraFile.getAbsolutePath();
            Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", cameraFile); //Uri.fromFile(cameraFile)
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(cameraIntent, REQUEST_CAMERA);
        }
    }

    public void startPreview(List<LocalMedia> previewImages, int position) {
        ImagePreviewActivity.startPreview(this, previewImages, adapter.getSelectedImages(), maxSelectNum, position);
    }

    public void onCameraFinished() {
        // 전체로 전환
        // 여기는 uri로 처리하면 또 안됨 --- 일단 이렇게 두고 나중에 수정하자
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(cameraPath))));
        imageFolders.get(0).getImages().add(0, new LocalMedia(cameraPath, new Date().getTime(), 0));
        adapter.bindImages(imageFolders.get(0).getImages());
        selectedFolderName = "";
        adapter.notifyDataSetChanged();
        MyUI.toastSHORT(context, String.format("사진 이미지 추가 (사진이 많으면 약간 시간 소요)"));
    }

    public void startCrop(String path) {
        ImageCropActivity.startCrop(this, path);
    }


}
