package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.DraftSlipDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.DraftSlipRecord;
import com.teampls.shoplus.lib.database_memory.MDraftSlipDB;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.protocol.TransactionDraftDataProtocol;
import com.teampls.shoplus.lib.view_base.BaseActivity;

import java.util.List;

/**
 * Created by Medivh on 2017-08-23.
 */

public class DraftSlipListView extends BaseActivity implements AdapterView.OnItemClickListener {
    protected DraftSlipDBAdapter adapter;
    protected ListView lvDraftSlips;
    protected MDraftSlipDB draftSlipDB;
    private TaskState draftDownloading = TaskState.beforeTasking;

    public static void startActivity(Context context ) {
       context.startActivity(new Intent(context, DraftSlipListView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.setTitle("공유 거래기록");
        draftSlipDB = MDraftSlipDB.getInstance(context);
        lvDraftSlips = (ListView) findViewById(R.id.slip_main_slips);
        adapter = new DraftSlipDBAdapter(context);
        lvDraftSlips.setAdapter(adapter);
        lvDraftSlips.setOnItemClickListener(this);
        setOnClick(R.id.slip_main_close, R.id.slip_main_new);
        setVisibility(View.GONE, R.id.slip_main_new,R.id.slip_main_bottom_container);
        downloadDraftSlips();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        DraftSlipRecord draftSlipRecord = adapter.getRecord(position);
        DraftSlipCreationView.startActivity(context, draftSlipRecord.draftId);
    }

    @Override
    public int getThisView() {
        return R.layout.base_slip_main;
    }

    public void refresh() {
        new MyAsyncTask(context, "DraftSlipListView", "정리중...") {
            @Override
            public void doInBackground() {
                adapter.generate();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
            }
        };
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.slip_main_close)
            finish();
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.help)
                .add(MyMenu.refresh)
                .add(MyMenu.close);
    }

    private void downloadDraftSlips() {
        if (draftDownloading.isOnTasking()) {
            MyUI.toastSHORT(context, String.format("다운로드 중입니다"));
            return;
        }
        draftDownloading = TaskState.onTasking;

        new CommonServiceTask(context, "DraftSlipListView", "기록 다운중....") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<TransactionDraftDataProtocol> protocols = transactionService.getDrafts(userSettingData.getUserShopInfo());
                draftSlipDB.set(protocols);
            }

            @Override
            public void onPostExecutionUI() {
                draftDownloading = TaskState.afterTasking;
                refresh();
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                draftDownloading = TaskState.onError;
            }
        };
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case help:
                MyDevice.openWeb(context, "https://shopl-us.com/2017/09/15/%EC%9E%91%EC%84%B1%EC%A4%91-%EC%98%81%EC%88%98%EC%A6%9D-%EA%B3%B5%EC%9C%A0%ED%95%98%EA%B8%B0/");
                break;
            case close:
                finish();
                break;
            case refresh:
                downloadDraftSlips();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BaseGlobal.RequestRefresh:
                refresh();
                break;
        }
    }
}
