package com.teampls.shoplus.lib.awsservice;

import android.util.Log;

import com.amazonaws.regions.Regions;
import com.teampls.shoplus.lib.awsservice.config.DevAWSConfigs;
import com.teampls.shoplus.lib.awsservice.config.ProdAWSConfigs;
import com.teampls.shoplus.lib.view.MyInfoView;

/**
 * AWS 설정 관련 상수 컨테이너 클래스
 *
 * @author lucidite
 */
abstract public class MyAWSConfigs {

    /// SERVER SWITCH //////////////////////////
    private static Boolean isProduction = true;
    ////////////////////////////////////////////

    public static Boolean isMaster = true;

    public static Boolean isProduction() {
        MyInfoView mv;
        return isProduction;
    }

    private static MyAWSConfigs currentConfig = null;

    public static MyAWSConfigs getCurrentConfig() {
        if (MyAWSConfigs.currentConfig == null) {
            MyAWSConfigs.currentConfig = isProduction ? new ProdAWSConfigs() : new DevAWSConfigs();
        }
        return MyAWSConfigs.currentConfig;
    }

    // COMMON CONFIGURATIONS
    private static final Regions USER_POOL_REGION = Regions.AP_NORTHEAST_2;
    private static final Regions IDENTITY_POOL_REGION = Regions.AP_NORTHEAST_2;
    private static final String PUBLIC_API_ROOT_URL = "https://72miou2vd7.execute-api.ap-northeast-2.amazonaws.com/prod/sps";
    private static final String PUBLIC_API_KEY = "mT0nB6yCXf5rzFGYXN32n6go4MQW7KTJ9mucJ2OZ";
    private static final Regions S3_BUCKET_REGIONS = Regions.AP_NORTHEAST_2;

    public static final int REMOTE_ITEM_PRICE_UNIT = 1000;     // 원격에서는 1천원을 1 단위로 저장/전송한다.

    abstract public String getUserPoolClientId();
    abstract public String getUserPoolId();
    abstract public String getUserPoolClientSecret();
    public Regions getUserPoolRegion() { return MyAWSConfigs.USER_POOL_REGION; }
    abstract public String getIdentityPoolId();
    public Regions getIdentityPoolRegion() { return MyAWSConfigs.IDENTITY_POOL_REGION; }

    abstract public String getApigatewayInvokeUrl();
    public String getApigatewayApiKey() { return null; }
    public String getPublicApiRootUrl() { return MyAWSConfigs.PUBLIC_API_ROOT_URL; }
    public String getPublicApiKey() { return MyAWSConfigs.PUBLIC_API_KEY; }
    public Regions getS3BucketRegion() { return MyAWSConfigs.S3_BUCKET_REGIONS; }

    abstract public String getS3SlipsBucketName();
    abstract public String getS3ItemsBucketName();
    public String getS3ItemThumbnailsBucketName() {
        return this.getS3ItemsBucketName() + "-resize";
    }
    abstract public String getS3ReceiptsBucketName();

    public static String getResourcePath(String resourceRelativePath) {
        return currentConfig.getApigatewayInvokeUrl() + resourceRelativePath;
    }

    public static String getResourcePath(String...resourceRelativePath) {
        String result = currentConfig.getApigatewayInvokeUrl();
        for (String path: resourceRelativePath) {
            result += ("/" + path);
        }
        return result;
    }
}
