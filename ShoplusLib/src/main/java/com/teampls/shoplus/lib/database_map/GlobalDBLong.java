package com.teampls.shoplus.lib.database_map;

import android.content.Context;

import com.teampls.shoplus.lib.database_global.GlobalDB;

/**
 * Created by Medivh on 2018-11-19.
 */

public class GlobalDBLong extends BaseDBValue<Long> {

    public GlobalDBLong(String key, Long defaultValue) {
        super(key, defaultValue);
    }

    @Override
    public KeyValueDB getDB(Context context) {
        return GlobalDB.getInstance(context);
    }

    @Override
    public void put(Context context,Long value) {
        getDB(context).put(key, value);
    }

    @Override
    public Long get(Context context) {
        return getDB(context).getLong(key, defaultValue);
    }
}
