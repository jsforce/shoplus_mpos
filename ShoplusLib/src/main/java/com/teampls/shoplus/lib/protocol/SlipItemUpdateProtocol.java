package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.datatypes.SlipDataKey;

import org.joda.time.DateTime;

/**
 * Slip 아이템 업데이트 로그 기록 데이터 프로토콜
 *
 * @author lucidite
 */

public interface SlipItemUpdateProtocol {
    SlipDataKey getReferenceSlipKey();
    DateTime getUpdatedDatetime();
    DateTime getCreatedDatetime();
    String getCounterpart();
    SlipItemDataProtocol getSlipItem();
}
