package com.teampls.shoplus.lib.view_base;

import android.os.Bundle;
import android.view.View;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2016-10-14.
 */
abstract public class BaseIsRegistered extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        myActionBar.hide();
        MyView.setTextViewByDeviceSize(context, getView(), R.id.userClass_title,
                R.id.userClass_register, R.id.userClass_login);
    }

    @Override
    public int getThisView() {
        return R.layout.base_is_registered;
    }

    public abstract void onNewUserClick(View view);

    public abstract  void onExistingUserClick(View view);

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

}