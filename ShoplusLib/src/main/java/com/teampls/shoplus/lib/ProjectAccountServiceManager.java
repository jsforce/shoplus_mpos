package com.teampls.shoplus.lib;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.ProjectAccountServiceProtocol;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectAccountService;

/**
 * @author lucidite
 */
public class ProjectAccountServiceManager {
    public static ProjectAccountServiceProtocol defaultService(Context context) {
        return new ProjectAccountService();
    }
}
