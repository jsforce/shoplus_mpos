package com.teampls.shoplus.lib.event;

/**
 * Created by Medivh on 2016-03-17.
 */
abstract public class MyOnYesNo {
    protected Object sender;

    public MyOnYesNo(Object sender) {
        this.sender = sender;
    }

    public MyOnYesNo() {

    }

    abstract public void yes();
    public void no() {};
}
