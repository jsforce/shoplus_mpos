package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import com.teampls.shoplus.lib.adapter.BaseItemDBAdapter;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-10-29.
 */
abstract public class BaseItemsView extends BaseActivity implements AdapterView.OnItemClickListener, AbsListView.OnScrollListener {
    protected GridView gridView;
    protected Context context = BaseItemsView.this;
    public BaseItemDBAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        myActionBar.hide();
        setItemDB();
        setAdaptor();
        setGridView();
        gridView.setOnItemClickListener(this);
        gridView.setNumColumns(MyDevice.getColumnNum(context));
        gridView.setOnScrollListener(this);
        gridView.setAdapter(adapter);
        setUiComponents();
    }

    abstract protected void setGridView();
    abstract public int getThisView();
    abstract protected void setAdaptor();
    protected void setUiComponents() {};
    protected void setItemDB() {};
    protected void refreshHeader() {};

    protected List<Integer> clickedItemIds = new ArrayList();

    public void refresh() {
        if (adapter == null) return;
        clickedItemIds = adapter.getClickedItemIds(); // filtering이나 sorting에 의해 click 정보가 달라질 수 있음
        new MyAsyncTask(context, "BaseItemsView") {
            @Override
            public void doInBackground() {
                adapter.generate();
            }

            @Override
            public void onPostExecutionUI() {
                adapter.resetClickedPosition(clickedItemIds);
                adapter.notifyDataSetChanged();
                refreshHeader();
            }
        };
    }

    @Override
    public void onMyMenuCreate() {
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        adapter.setScrollState(scrollState);
        switch (scrollState) {
            case SCROLL_STATE_FLING:
                break;
            case SCROLL_STATE_IDLE:
                break;
        }
    }

    private int prevFirstVisibleItem = 0;

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }
}
