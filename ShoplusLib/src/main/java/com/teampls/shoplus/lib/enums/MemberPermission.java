package com.teampls.shoplus.lib.enums;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.datatypes.ProjectDalaranAuthData;
import com.teampls.shoplus.lib.datatypes.ProjectSilvermoonAuthData;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;

import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 * Created by Medivh on 2018-01-28.
 */

public enum MemberPermission {
    ChangeStockValue("재고수량 변경", UserLevelType.EMPLOYEE, true),
    ChangeOnline("온라인 잔금 변경", UserLevelType.EMPLOYEE, true),
    ChangeEntrusted("대납(현금) 잔금 변경", UserLevelType.EMPLOYEE, true),
    ChangeDeposit("매입금 수동조정", UserLevelType.EMPLOYEE, true),
    SetTransactionDate("영수증 날짜 변경", UserLevelType.EMPLOYEE, true),
    CancelTransactionAfter1Day("1일경과 거래취소", UserLevelType.EMPLOYEE, true),
    MergeCounterpart("거래처 통합", UserLevelType.MASTER, false),
    DeleteNotFound("못찾음 재고 삭제", UserLevelType.MASTER, false);
    // 서버에 설정이 안된 것은 일단 막는다. 안되서

    public String uiStr = "";
    private UserLevelType defaultUserLevel = UserLevelType.SHOP;
    public boolean implemented = true;

    MemberPermission(String uiStr, UserLevelType userLevelType, boolean implemented) {
        this.uiStr = uiStr;
        this.defaultUserLevel = userLevelType;
        this.implemented = implemented;
    }

    public UserLevelType getDefaultUserLevel() {
        return defaultUserLevel;
    }

    public void toast(Context context) {
        MyUI.toast(context, getToastMessage());
    }

    public String getToastMessage() {
        return String.format("%s 권한이 필요합니다. 사장님께 문의해주세요", uiStr);
    }

    public static boolean canCancelTransaction(Context context, DateTime dateTime) {
        Duration duration = new Duration(dateTime, DateTime.now());
        if (duration.getStandardHours() <= 24)
            return true;
        else
            return MemberPermission.CancelTransactionAfter1Day.hasPermission(UserSettingData.getInstance(context));
    }

    public boolean hasPermission(UserSettingData userSettingData) {
        if (userSettingData.isProtocolNull())
            return false;

        UserSettingDataProtocol protocol = userSettingData.getProtocol();
        if (userSettingData.isBuyer())
            return true; // Buyer 상태에서는 permission 체크를 하지 않음

        if (protocol.doesShopHaveMasterUser() == false)
            return true; // 그 매장에 사장님 권한이 없으면 모든 권한이 열린다 (주의하자)

        switch (this) {
            case ChangeOnline:
                return protocol.hasDalaranOnlineBalanceChangePermission();
            case ChangeEntrusted:
                return protocol.hasDalaranEntrustedBalanceChangePermission();
            case ChangeDeposit:
                return protocol.hasDalaranDepositChangePermission();
            case ChangeStockValue:
                return userSettingData.getUserLevelType().isAllowed(protocol.getSilvermoonAuthData().getStockValueUpdatePermission());
            case SetTransactionDate:
                return userSettingData.getUserLevelType().isAllowed(protocol.getSilvermoonAuthData().getTransactionDateSettingPermission());
            case CancelTransactionAfter1Day:
                return userSettingData.getUserLevelType().isAllowed(protocol.getSilvermoonAuthData().getCancelTransactionAfter1DayPermission());
            default:
                Log.e("DEBUG_JS", String.format("[MemberPermission.hasPermission] %s not implemented", this.toString()));
            case MergeCounterpart:
            case DeleteNotFound:
                return userSettingData.getUserLevelType().isAllowed(this.getDefaultUserLevel());
        }
    }

    public UserLevelType getMemberPermission(UserSettingData userSettingData) {
        if (userSettingData.isProtocolNull())
            return this.getDefaultUserLevel();

        UserSettingDataProtocol protocol = userSettingData.getProtocol();
        switch (this) {
            case ChangeOnline:
                return protocol.getDalaranAuthData().getOnlineBalanceChangePermission();
            case ChangeEntrusted:
                return protocol.getDalaranAuthData().getEntrustedBalanceChangePermission();
            case ChangeDeposit:
                return protocol.getDalaranAuthData().getDepositChangePermission();
            case ChangeStockValue:
                return protocol.getSilvermoonAuthData().getStockValueUpdatePermission();
            case SetTransactionDate:
                return protocol.getSilvermoonAuthData().getTransactionDateSettingPermission();
            case CancelTransactionAfter1Day:
                return protocol.getSilvermoonAuthData().getCancelTransactionAfter1DayPermission();
            default:
                Log.e("DEBUG_JS", String.format("[MemberPermission.getMemberPermission] %s not implemented", this.toString()));
            case MergeCounterpart:
            case DeleteNotFound:
                return this.getDefaultUserLevel();
        }

    }

    public void updateMemberPermission(Context context, final UserSettingData userSettingData, final UserLevelType userLevelType, final MyOnTask onTask) {
        if (userSettingData.isProtocolNull())
            return;

        new CommonServiceTask(context, "updateMemberPermission") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                UserSettingDataProtocol protocol = userSettingData.getProtocol();
                ProjectDalaranAuthData dalaranData = protocol.getDalaranAuthData();
                ProjectSilvermoonAuthData silvermoonData = protocol.getSilvermoonAuthData();
                switch (MemberPermission.this) {
                    case ChangeOnline:
                        dalaranData.setOnlineBalanceChangePermission(userLevelType);
                        protocol = mainShopService.updateDalaranAuth(userSettingData.getUserShopInfo(), dalaranData);
                        break;
                    case ChangeEntrusted:
                        dalaranData.setEntrustedBalanceChangePermission(userLevelType);
                        protocol = mainShopService.updateDalaranAuth(userSettingData.getUserShopInfo(), dalaranData);
                        break;
                    case ChangeDeposit:
                        dalaranData.setDepositChangePermission(userLevelType);
                        protocol = mainShopService.updateDalaranAuth(userSettingData.getUserShopInfo(), dalaranData);
                        break;
                    case ChangeStockValue:
                        silvermoonData.setStockValueUpdatePermission(userLevelType);
                        protocol = mainShopService.updateSilvermoonAuth(userSettingData.getUserShopInfo(), silvermoonData);
                        break;
                    case SetTransactionDate:
                        silvermoonData.setTransactionDateSettingPermission(userLevelType);
                        protocol = mainShopService.updateSilvermoonAuth(userSettingData.getUserShopInfo(), silvermoonData);
                        break;
                    case CancelTransactionAfter1Day:
                        silvermoonData.setCancelTransactionAfter1DayPermission(userLevelType);
                        protocol = mainShopService.updateSilvermoonAuth(userSettingData.getUserShopInfo(), silvermoonData);
                        break;
                    default:
                        Log.e("DEBUG_JS", String.format("[UserSettingData.updateMemberPermission] %s not implemented", MemberPermission.this.toString()));
                    case MergeCounterpart:
                    case DeleteNotFound:
                        break;
                }
                userSettingData.set(protocol);
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone(true);
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                if (onTask != null)
                    onTask.onTaskDone(false);
            }
        };

    }

}
