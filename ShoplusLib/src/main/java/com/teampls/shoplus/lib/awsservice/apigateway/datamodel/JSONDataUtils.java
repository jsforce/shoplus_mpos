package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

/**
 * @author lucidite
 */

public class JSONDataUtils {
    public static boolean exists(String str) {
        return (str != null) && !str.isEmpty();
    }
}
