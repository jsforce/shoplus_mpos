package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.database.AccountLogRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MAccountLogDB;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.enums.AccountChangeOperationType;
import com.teampls.shoplus.lib.enums.AccountLogType;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2017-10-27.
 */

abstract public class BaseAccountLogDBAdapter extends BaseDBAdapter<AccountLogRecord> {
    protected MAccountLogDB accountLogDB;
    protected boolean doBlockCleared = false, doBlockCancelled = false, doFilterPhoneNumbers = false,
            doFilterAccountLogType = false, doShowAsSummary = false, doFilterAccountType = false,
            doShowMemo = false, doFilterOperationType = false, doSumReceivable = false, doBlockMergedUser = true; // 합쳐진 거래처들 (잔여)
    protected List<String> phoneNumbers = new ArrayList<>();
    protected Set<AccountLogType> accountLogSummaryTypes = new HashSet<>();
    protected List<AccountType> accountTypes = new ArrayList<>();
    protected List<AccountChangeOperationType> operationTypes = new ArrayList<>();

    public BaseAccountLogDBAdapter(Context context, MAccountLogDB accountLogDB) {
        super(context);
        this.accountLogDB = accountLogDB;
        setDefaultOrdering();
    }

    public void setDefaultOrdering() {
    }

    public BaseAccountLogDBAdapter resetFilters() {
        doFilterPhoneNumbers = false;
        doBlockCancelled = false;
        doFilterAccountLogType = false;
        doShowAsSummary = false;
        doBlockCleared = false;
        doBlockMergedUser = true;
        return this;
    }

    public void blockMergedUser(boolean value) {
        doBlockMergedUser = value;
    }

    public boolean isDoFilterPhoneNumbers() {
        return doFilterPhoneNumbers;
    }

    public List<String> getFilterPhoneNumbers() {
        return phoneNumbers;
    }

    public BaseAccountLogDBAdapter setFilterPhoneNumbers(boolean value) {
        doFilterPhoneNumbers = value;
        return this;
    }

    public void setSumReceivable(boolean value) {
        this.doSumReceivable = value;
    }

    public BaseAccountLogDBAdapter filterOperationType(AccountChangeOperationType... operationTypes) {
        doFilterOperationType = true;
        this.operationTypes.clear();
        for (AccountChangeOperationType operationType : operationTypes)
            this.operationTypes.add(operationType);
        return this;
    }

    public BaseAccountLogDBAdapter setAccountLogType(AccountLogType accountLogType, boolean checked) {
        doFilterAccountLogType = true;
        if (checked) {
            accountLogSummaryTypes.add(accountLogType);
        } else {
            accountLogSummaryTypes.remove(accountLogType);
        }
        return this;
    }

    public BaseAccountLogDBAdapter filterAccountType(AccountType... accountTypes) {
        doFilterAccountType = true;
        this.accountTypes.clear();
        for (AccountType accountType : accountTypes)
            this.accountTypes.add(accountType);
        return this;
    }

    public BaseAccountLogDBAdapter showMemo(boolean value) {
        doShowMemo = value;
        return this;
    }

    private boolean doBlockTransfer = false;

    public void blockOperationTransfer(boolean value) {
        doBlockTransfer = value;
    }

    public BaseAccountLogDBAdapter resetAccountType() {
        doFilterAccountType = false;
        this.accountTypes.clear();
        return this;
    }


    public BaseAccountLogDBAdapter filterPhoneNumbers(String... counterPhoneNumbers) {
        doFilterPhoneNumbers = true;
        phoneNumbers.clear();
        for (String phoneNumber : counterPhoneNumbers)
            phoneNumbers.add(phoneNumber);
        return this;
    }

    public BaseAccountLogDBAdapter blockCancelled(boolean value) {
        doBlockCancelled = value;
        return this;
    }

    public BaseAccountLogDBAdapter doShowCleared(boolean value) {
        doBlockCleared = !value;
        return this;
    }

    public BaseAccountLogDBAdapter doShowAsSummary(boolean value) {
        doShowAsSummary = value;
        return this;
    }

    @Override
    public void generate() {
        if (doSkipGenerate) return;
        boolean isProvider = userSettingData.isProvider();
        generatedRecords.clear();
        int prevOnlineAccount = 0, prevEntrustAccount = 0;

        List<String> counterparts = userDB.getStrings(UserDB.Column.phoneNumber); // UserDB가 업데이트 중일때는 문제가 됨, 불완전 정보

        // 이건 누적합산때문에 특수하게  미리 sorting을 해야 함
        for (AccountLogRecord record : accountLogDB.getRecordsOrderByKey()) { // 오래된 순서대로 가져와서 누적값을 더해나감
            if (doFilterPhoneNumbers) {
                if (isProvider) {
                    if (phoneNumbers.contains(record.buyer) == false)
                        continue;
                } else {
                    if (phoneNumbers.contains(record.provider) == false)
                        continue;
                }
            }

            if (doFilterDateTime)
                if (BaseUtils.isBetween(record.getCreatedDateTime(), fromDateTime, toDateTime) == false)
                    continue;

            if (doBlockCancelled)
                if (record.cancelled)
                    continue;

            if (doBlockCleared)
                if (record.cleared)
                    continue;

            if (doShowAsSummary) {
                switch (record.operation) {
                    default:
                        continue;
                    case Transaction:
                        break;
                }
            }

            if (doFilterAccountLogType)
                if (accountLogSummaryTypes.contains(record.getAccountLogSummaryType()) == false)
                    continue;

            if (doFilterAccountType)
                if (accountTypes.contains(record.accountType) == false)
                    continue;

            if (doFilterOperationType)
                if (operationTypes.contains(record.operation) == false)
                    continue;

            if (doBlockMergedUser)
                if (counterparts.contains(record.getCounterpart(context)) == false)
                    continue;

            if (doSumReceivable) {
                switch (record.accountType) {
                    case OnlineAccount:
                        record.receivable = record.account + prevEntrustAccount; // 현재 online 총액 + 최근 대납 총액
                        prevOnlineAccount = record.account;
                        break;
                    case EntrustedAccount:
                        record.receivable = record.account + prevOnlineAccount; // 현재 대납 총액 + 최근 온라인 총액
                        prevEntrustAccount = record.account;
                        break;
                }
            }

            // 순서주의, 전달 숨김은 목록에서는 빼야 하지만 최근 대납, 최근 온라인은 기록해야 한다.
            if (doBlockTransfer)
                if (record.operation == AccountChangeOperationType.Transfer)
                    continue;

            record.sortingKey = sortingKey;
            generatedRecords.add(record);
        }

        if (sortingKey != MSortingKey.ID)
            Collections.sort(generatedRecords);
        Collections.reverse(generatedRecords); // 이전 기록 합산 때문에 시간 순차로 계산후 역정렬
        if (doSetRecordWhenGenerated)
            applyGeneratedRecords();
    }


    public class ReviewHolder extends BaseViewHolder {
        protected TextView tvDate, tvTime, tvPlusMemo, tvMinus,
                tvPlus, tvMinusMemo, tvAmount, tvAmountMemo;
        protected AccountLogRecord record;

        @Override
        public int getThisView() {
            return R.layout.row_account_review;
        }

        @Override
        public void init(View view) {
            tvDate = (TextView) view.findViewById(R.id.row_account_review_date);
            tvTime = (TextView) view.findViewById(R.id.row_account_review_time);
            tvPlus = (TextView) view.findViewById(R.id.row_account_review_plus);
            tvPlusMemo = (TextView) view.findViewById(R.id.row_account_review_plus_memo);
            tvMinus = (TextView) view.findViewById(R.id.row_account_review_minus);
            tvMinusMemo = (TextView) view.findViewById(R.id.row_account_review_minus_memo);
            tvAmount = (TextView) view.findViewById(R.id.row_account_review_amount);
            tvAmountMemo = (TextView) view.findViewById(R.id.row_account_review_amount_memo);
            MyView.setTextViewByDeviceSize(context, tvDate, tvTime, tvPlus, tvPlusMemo, tvMinus, tvMinusMemo, tvAmount, tvAmountMemo);
        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0) // UI에 뿌리는 도중에 변경될 가능성이 있을때
                return;
            record = records.get(position);
            tvDate.setText(record.createdDateTime.toString(BaseUtils.MD_FixedSize_Format));
            tvTime.setText(record.createdDateTime.toString(BaseUtils.Hm_Format));
            if (record.variation >= 0) {
                tvPlus.setVisibility(View.VISIBLE);
                tvPlus.setText(BaseUtils.toCurrencyOnlyStr(record.variation));
                tvPlusMemo.setText(record.operation.uiStr);
                tvPlusMemo.setVisibility(doShowMemo ? View.VISIBLE : View.GONE);
                tvMinus.setVisibility(View.GONE);
                tvMinusMemo.setVisibility(View.GONE);
            } else {
                tvPlus.setVisibility(View.GONE);
                tvPlusMemo.setVisibility(View.GONE);
                tvMinus.setVisibility(View.VISIBLE);
                tvMinus.setText(BaseUtils.toCurrencyOnlyStr(record.variation));
                tvMinusMemo.setText(record.operation.uiStr);
                tvMinusMemo.setVisibility(doShowMemo ? View.VISIBLE : View.GONE);
            }

            if (accountTypes.size() == 1) { // 딱 1개가 아니면 무조건 합산을
                tvAmount.setText(BaseUtils.toCurrencyOnlyStr(record.account));
            } else { // 0, 2, 3 해당
                tvAmount.setText(BaseUtils.toCurrencyOnlyStr(record.receivable));
            }

            if (doShowMemo) {
                tvAmountMemo.setText(record.accountType.uiStr);
                tvAmountMemo.setVisibility(View.VISIBLE);
            } else {
                tvAmountMemo.setVisibility(View.GONE);
            }

        }
    }

    public class LogViewHolder extends BaseViewHolder {
        protected TextView tvDate, tvCounterpart, tvVariation, tvAccountType,
                tvOperation, tvAccount;
        protected AccountLogRecord record;
        protected LinearLayout container;
        private float accountTypeFontSize = 16;

        @Override
        public int getThisView() {
            return R.layout.row_account_log;
        }

        @Override
        public void init(View view) {
            tvDate = (TextView) view.findViewById(R.id.row_account_log_date);
            tvCounterpart = (TextView) view.findViewById(R.id.row_account_log_counterpart);
            tvOperation = (TextView) view.findViewById(R.id.row_account_log_operation);
            tvVariation = (TextView) view.findViewById(R.id.row_account_log_variation);
            tvAccountType = (TextView) view.findViewById(R.id.row_account_log_accountType);
            tvAccount = (TextView) view.findViewById(R.id.row_account_log_account);
            container = (LinearLayout) view.findViewById(R.id.row_account_log_container);
            accountTypeFontSize = MyDevice.toOriginalSP(context, tvAccountType.getTextSize());
            MyView.setTextViewByDeviceSize(context, tvDate, tvCounterpart, tvOperation, tvVariation, tvAccountType, tvAccount);
        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0) return;

            record = records.get(position);
            if (clickedPositions.contains(position)) {
                container.setBackgroundColor(ColorType.Khaki.colorInt);
            } else {
                Pair<Double, Double> shiftTimes = BasePreference.getShiftTimes(context);
                if (shiftTimes.first >= 0) {
                    if (BaseUtils.isBetweenByHm(record.createdDateTime, shiftTimes.first, shiftTimes.second)) {
                        container.setBackgroundColor(ColorType.Ivory.colorInt);
                    } else {
                        container.setBackgroundColor(ColorType.Beige.colorInt);
                    }
                } else {
                    container.setBackgroundColor(ColorType.Ivory.colorInt);
                }
            }

            // Date
            tvDate.setVisibility(View.VISIBLE);
            tvDate.setTextColor(ColorType.Black.colorInt);
            if (BasePreference.isShowWeekday(context)) {
                tvDate.setText(record.createdDateTime.toString(BaseUtils.MD_FixedSize_Format) + String.format("\n(%s)", BaseUtils.getDayOfWeekKor(record.createdDateTime)));
            } else {
                tvDate.setText(record.createdDateTime.toString(BaseUtils.MD_FixedSize_Format));
            }

            if (position > 0) {
                AccountLogRecord prevRecord = records.get(position - 1);
                if (record.createdDateTime.getDayOfYear() == prevRecord.createdDateTime.getDayOfYear()) {
                    if (BasePreference.doShowHm.getValue(context)) {
                        tvDate.setText(record.createdDateTime.toString(BaseUtils.Hm_Format));
                        tvDate.setTextColor(ColorType.DarkGray.colorInt);
                    } else {
                        tvDate.setVisibility(View.INVISIBLE);
                    }
                }
            }

            switch (record.operation) {
                default:
                    tvOperation.setVisibility(View.VISIBLE);
                    tvOperation.setText(record.operation.uiStr);
                    break;
                case Transaction:
                    if (record.cleared) {
                        tvOperation.setVisibility(View.VISIBLE);
                        tvOperation.setText(record.updatedDateTime.toString(BaseUtils.MD_FixedSize_Format));
                    } else {
                        tvOperation.setVisibility(View.GONE);
                    }
                    break;
            }

            switch (record.accountType) {
                case DepositAccount:
                    tvAccount.setVisibility(View.VISIBLE);
                    tvAccount.setText(BaseUtils.toCurrencyOnlyStr(record.account));
                    break;
                case EntrustedAccount:
                case OnlineAccount:
                    tvAccount.setVisibility(View.GONE);
                    break;
            }

            String counterpartPhoneNumber = record.getCounterpart(context);
            if (userDB.has(counterpartPhoneNumber)) {
                UserRecord counterpart = userDB.getRecord(counterpartPhoneNumber);
                BaseUtils.setText(tvCounterpart, "", counterpart.name, record.operation == AccountChangeOperationType.TransactionCancellation);
            } else {
                BaseUtils.setText(tvCounterpart, "", counterpartPhoneNumber, record.operation == AccountChangeOperationType.TransactionCancellation);
            }
            BaseUtils.setText(tvVariation, record.variation == 0, "", BaseUtils.toCurrencyOnlyStr(record.variation), record.operation == AccountChangeOperationType.TransactionCancellation);

            // doShowAsSummary .. 무조건 True
            tvAccountType.setText(record.getAccountLogSummaryType().uiStr); // clear 됐는지 표시
            tvAccountType.setTextColor(record.cancelled? ColorType.DarkGray.colorInt : record.getAccountLogSummaryType().colorInt);
            if (record.cleared) {
                tvAccountType.setBackgroundResource(R.drawable.background_round_gray_button);
                tvAccountType.setTextSize(accountTypeFontSize - 3);
                MyView.setPadding(context, tvAccountType, 3, 0, 3, 0);
            } else {
                tvAccountType.setBackgroundResource(0);
                tvAccountType.setTextSize(accountTypeFontSize);
                MyView.setPadding(context, tvAccountType, 0, 0, 0, 0);
            }

            BaseUtils.setTextCancelled(tvDate, record.cancelled);
            BaseUtils.setTextCancelled(tvCounterpart, record.cancelled);
            BaseUtils.setTextCancelled(tvAccountType, record.cancelled);
            BaseUtils.setTextCancelled(tvVariation, record.cancelled);
        }

    }

}
