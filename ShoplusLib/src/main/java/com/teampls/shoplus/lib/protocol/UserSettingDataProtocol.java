package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.datatypes.ExternalAPIData;
import com.teampls.shoplus.lib.datatypes.MessengerLinkData;
import com.teampls.shoplus.lib.datatypes.MyShopInformationData;
import com.teampls.shoplus.lib.datatypes.ProjectDalaranAuthData;
import com.teampls.shoplus.lib.datatypes.ProjectSilvermoonAuthData;
import com.teampls.shoplus.lib.datatypes.ServiceActivationData;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.enums.BalanceChangeMethodType;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.enums.UserLevelType;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 사용자 설정 정보 프로토콜
 *
 * @author lucidite
 */

public interface UserSettingDataProtocol {
    UserLevelType getUserLevel();
    UserConfigurationType getUserType();
    Map<ColorType, String> getColorNames();
    List<String> getItemTraceLocations();
    Boolean isRawPriceInputMode();

    /**
     * [SILVER-423] 소매 가격 기능을 사용하도록 설정되었는지를 반환한다.
     * @return
     */
    Boolean isRetailOn();

    Boolean showNegativeStockValue();
    int getTimeShift();
    String getMainShop();
    ProjectDalaranAuthData getDalaranAuthData();
    ProjectSilvermoonAuthData getSilvermoonAuthData();
    int getDefaultMarginAdditionRate();
    BalanceChangeMethodType getBalanceChangeMethod();
    MyShopInformationData getShopInformation();
    UserSpecificSettingProtocol getUserSpecificSetting();
    List<String> getAllowedShops();
    Map<ShoplusServiceType, ServiceActivationData> getServiceActivationData();
    Set<MessengerLinkData> getMessengerLinkData();

    /**
     * 근무 중인 매장이 (매장 계정으로) 사장 계정을 가지는지 확인한다.
     * 종업원이 설정되지 않은 단독 매장 계정인지를 확인하기 위한 용도로 사용한다.
     *
     * @return
     */
    Boolean doesShopHaveMasterUser();

    //////////////////////////////////////////////////////////////////////
    // Utility methods - Auth Check
    /**
     * [샵플미송] 로그인한 계정이 온라인 잔금관리 권한이 있는지를 확인한다.
     * @return
     */
    Boolean hasDalaranOnlineBalanceChangePermission();

    /**
     * [샵플미송] 로그인한 계정이 대납(현금) 잔금관리 권한이 있는지를 확인한다.
     * @return
     */
    Boolean hasDalaranEntrustedBalanceChangePermission();

    /**
     * [샵플미송] 로그인한 계정이 매입금 관리 권한이 있는지를 확인한다.
     * @return
     */
    Boolean hasDalaranDepositChangePermission();

    /**
     * [STORM-143] 챗봇 사용자의 경우 연결된 플러스친구 주소를 반환한다.
     * [NOTE] 챗봇 미사용자의 경우 또는 정상 연결이 되어 있지 않은 경우 빈 문자열을 반환한다.
     * @return
     */
    String getChatbotLinkedPlusFriend();

    /**
     * 외부 API 활용을 위한 정보를 반환한다.
     * [STORM-158] 현재 파파고를 이용한 상품명 번역 API에 필요한 정보를 포함하고 있음
     *
     * @return
     */
    ExternalAPIData getExternalAPIData();

    /**
     * 특정 기능들의 전역 차단 설정 정보를 반환한다.
     * @return
     */
    FunctionBlockConfigurationDataProtocol getFunctionBlockConfigurations();
}
