package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.content.Intent;
import android.view.View;

/**
 * Created by Medivh on 2019-05-23.
 */

public class ListMenuRecord {
    public int imageResId = 0;
    public String title = "", description = "";
    private View.OnClickListener onClickListener;

    public ListMenuRecord(int imageResId, String title, String description, View.OnClickListener onClickListener) {
        this.imageResId = imageResId;
        this.title = title;
        this.description = description;
        this.onClickListener = onClickListener;
    }

    public void onItemClick(Context context) {
        if (context != null && onClickListener != null)
            onClickListener.onClick(new View(context));
    }

}
