package com.teampls.shoplus.lib.database_global;

import android.content.Context;

public class ThumbImageDB extends ImageDB {
    private static ThumbImageDB instance = null;

    public static ThumbImageDB getInstance(Context context) {
        if (instance == null)
            instance = new ThumbImageDB(context);
        return instance;
    }

    private ThumbImageDB(Context context) {
        super(context, "ThumbImageDB", Ver, Column.toStrs());
        columnAttrs = Column.getAttributions();
    }

}
