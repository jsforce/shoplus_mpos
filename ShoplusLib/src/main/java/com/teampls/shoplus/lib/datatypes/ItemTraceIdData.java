package com.teampls.shoplus.lib.datatypes;

import android.util.Log;

import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemTraceBundleVersionType;
import com.teampls.shoplus.lib.enums.SizeType;

import org.joda.time.DateTime;

/**
 * 제고추적 ID의 encoding/decoding 데이터 클래스
 *
 * @author lucidite
 */

public class ItemTraceIdData {
    private ItemTraceBundleVersionType bundleVersion;

    private ItemStockKey itemOption;
    private String serialNumber;
    private boolean isValid = true;

    public ItemTraceIdData() {
        bundleVersion = ItemTraceBundleVersionType.SINGLE;
        itemOption = new ItemStockKey(0, ColorType.Default, SizeType.Default);
        serialNumber = "";
        isValid = false;
    };

    public ItemTraceIdData(ItemTraceBundleVersionType bundleVersion, ItemStockKey itemOption, String serialNumber) {
        this.bundleVersion = bundleVersion;
        this.itemOption = itemOption;
        this.serialNumber = serialNumber;
    }

    public ItemTraceIdData(ItemTraceBundleVersionType bundleVersion, ItemStockKey itemOption, DateTime timestamp, int serialCount) {
        this.bundleVersion = bundleVersion;
        this.itemOption = itemOption;
        this.serialNumber = String.format("%08X%04X", timestamp.getMillis() / 1000, serialCount);
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %s (%d), %d, %s, %s, %s", callLocation, bundleVersion.getVersionStr(), getBundleCount(), itemOption.getItemId(),
                itemOption.getColorOption().getUiName(), itemOption.getSizeOption().uiName, serialNumber));
    }

    public ItemTraceIdData(String traceId) {
        try {
            this.bundleVersion = ItemTraceBundleVersionType.fromVersionStr(traceId.substring(0, 1));
            String itemIdStr = traceId.substring(1, 7);
            int itemId = Integer.valueOf(itemIdStr, 16).intValue();
            String colorStr = traceId.substring(7, 13).toLowerCase();
            this.serialNumber = traceId.substring(13, 25);
            String sizeStr = traceId.substring(25).toLowerCase();

            ColorType color = ColorType.ofRemoteStr(colorStr);
            SizeType size = SizeType.ofRemoteStr(sizeStr);
            this.itemOption = new ItemStockKey(itemId, color, size);
            isValid = true;
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            Log.e("DEBUG_TAG", "[ERROR] Item Trace ID 오류: " + traceId);
            this.bundleVersion = ItemTraceBundleVersionType.SINGLE;
            this.itemOption = new ItemStockKey(0, ColorType.Default, SizeType.Default);
            this.serialNumber = "";
            isValid = false;
        }
    }

    public boolean isValid() {
        return isValid;
    }

    public ItemTraceBundleVersionType getBundleVersion() {
        return bundleVersion;
    }

    public int getBundleCount() {
        return bundleVersion.getCount();
    }

    public ItemStockKey getItemOption() {
        return itemOption;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public DateTime getCreatedDateTime() {
        try {
            String timestampStr = this.serialNumber.substring(0, 8);
            long timestamp = Long.valueOf(timestampStr, 16);
            return new DateTime(timestamp);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            return new DateTime(0);
        }
    }

    public String encode() {
        String result = this.bundleVersion.getVersionStr();
        result += String.format("%06X", this.itemOption.getItemId());
        if (this.itemOption.getColorOption() == ColorType.Default) {
            result += "CLNONE";
        } else {
            result += this.itemOption.getColorOption().toRemoteStr().toUpperCase();
        }
        result += this.serialNumber;
        if (this.itemOption.getSizeOption() != SizeType.Default) {
            result += this.itemOption.getSizeOption().toRemoteStr().toUpperCase();
        }
        return result;
    }

    public static ItemTraceIdData convert(int itemid, String traceDetailsStr) throws IllegalArgumentException {
        String[] traceDetailsElems = traceDetailsStr.split(ItemTraceBundleVersionType.SEPARATOR);
        ItemTraceBundleVersionType bundleVersion;
        if (traceDetailsElems.length == 1) {
            bundleVersion = ItemTraceBundleVersionType.SINGLE;
        } else if (traceDetailsElems.length == 2) {
            bundleVersion = ItemTraceBundleVersionType.fromTraceDetailsPostfixStr(traceDetailsElems[1]);
        } else {
            throw new IllegalArgumentException(traceDetailsStr);
        }

        String[] itemOptionKeyElems = traceDetailsElems[0].split("-", 3);
        if (itemOptionKeyElems.length == 3) {
            ColorType color = ColorType.ofRemoteStr(itemOptionKeyElems[0]);
            SizeType size = SizeType.ofRemoteStr(itemOptionKeyElems[1]);
            ItemStockKey itemOption = new ItemStockKey(itemid, color, size);
            return new ItemTraceIdData(bundleVersion, itemOption, itemOptionKeyElems[2]);
        } else {
            Log.e("DEBUG_TAG", "[ERROR] tc_id 값에 오류가 있습니다: " + traceDetailsStr);
            throw new IllegalArgumentException(traceDetailsStr);
        }
    }
}
