package com.teampls.shoplus.lib.awsservice.s3;

/**
 * 상품 이미지 업로드 분류 타입
 *
 * @author lucidite
 */
public enum ImageUploadType {
    ITEM_MAIN_IMAGE("i"),
    ITEM_SUB_IMAGE("s"),
    RECEIPT(""),            // [NOTE] 영수증 사진 (no sub-path)
    SLIP(""),               // [NOTE] 미송 증빙사진 (no sub-path)
    MISC("m");

    private String baseDir;

    ImageUploadType(String baseDir) {
        this.baseDir = baseDir;
    }

    public String getBasePath() {
        return this.baseDir.isEmpty() ? "" : (this.baseDir + "/");
    }
}
