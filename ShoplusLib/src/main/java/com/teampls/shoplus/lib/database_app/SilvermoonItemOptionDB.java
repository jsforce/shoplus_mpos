package com.teampls.shoplus.lib.database_app;

import android.content.Context;

import com.teampls.shoplus.lib.database.ItemOptionDB;

/**
 * Created by Medivh on 2017-06-16.
 */

public class SilvermoonItemOptionDB extends ItemOptionDB {

    private static SilvermoonItemOptionDB instance = null;

    public static SilvermoonItemOptionDB getInstance(Context silvermoonContext) {
        if (instance == null)
            instance = new SilvermoonItemOptionDB(silvermoonContext);
        return instance;
    }

    private SilvermoonItemOptionDB(Context context) {
        super(context);
    }
}
