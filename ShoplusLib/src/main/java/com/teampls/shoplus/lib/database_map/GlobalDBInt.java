package com.teampls.shoplus.lib.database_map;

import android.content.Context;

import com.teampls.shoplus.lib.database_global.GlobalDB;

/**
 * Created by Medivh on 2018-11-19.
 */

public class GlobalDBInt extends BaseDBValue<Integer> {

    public GlobalDBInt(String key, int defaultValue) {
        super(key, defaultValue);
    }

    @Override
    public KeyValueDB getDB(Context context) {
        return GlobalDB.getInstance(context);
    }

    @Override
    public void put(Context context, Integer value) {
        getDB(context).put(key, value);
    }

    @Override
    public Integer get(Context context) {
        return getDB(context).getInt(key, defaultValue);
    }
}
