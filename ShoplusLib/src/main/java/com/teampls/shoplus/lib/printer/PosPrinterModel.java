package com.teampls.shoplus.lib.printer;

import android.util.Log;

import com.bxl.config.editor.BXLConfigLoader;

/**
 * Created by Medivh on 2018-04-07.
 */

public enum PosPrinterModel {
    Default, R200, R210, R215, R200II, R310, Q300, R410;

    public static PosPrinterModel get(String productName) {
        if (productName.equals(BXLConfigLoader.PRODUCT_NAME_SPP_R200II)) {
            return R200II;
//        } else if (productName.equals(MyR200Printer.PRODUCT_NAME_SPP_R200)) {
//            return R200;
        } else if (productName.equals(BXLConfigLoader.PRODUCT_NAME_SPP_R210)) {
            return R210;
        } else if (productName.equals(BXLConfigLoader.PRODUCT_NAME_SPP_R215)) {
            return R215;
        } else if (productName.equals(BXLConfigLoader.PRODUCT_NAME_SPP_R310)) {
            return R310;
        } else if (productName.equals(BXLConfigLoader.PRODUCT_NAME_SRP_Q300)) {
            return Q300;
        } else if (productName.equals(BXLConfigLoader.PRODUCT_NAME_SPP_R410)) {
            return R410;
        } else {
            return R310;
        }
    }

    public static String toProductName(String deviceName) {
        String productName = "";
        //   Log.i("DEBUG_JS", String.format("[MyPosPrinter.toProductName] productName : %s", device.getUiName()));

        if ((deviceName.indexOf("SPP-R200II") >= 0)) {
            if (deviceName.length() > 10) {
                if (deviceName.substring(10, 11).equals("I")) {
                    productName = BXLConfigLoader.PRODUCT_NAME_SPP_R200III;
                } else {
                    productName = BXLConfigLoader.PRODUCT_NAME_SPP_R200II;
                }
            }

        } else if ((deviceName.indexOf("SPP-R210") >= 0)) {
            productName = BXLConfigLoader.PRODUCT_NAME_SPP_R210;
        } else if ((deviceName.indexOf("SPP-R215") >= 0)) {
            productName = BXLConfigLoader.PRODUCT_NAME_SPP_R215;
        } else if ((deviceName.indexOf("SPP-R310") >= 0)) {
            productName = BXLConfigLoader.PRODUCT_NAME_SPP_R310;
        } else if ((deviceName.indexOf("SPP-R300") >= 0)) {
            productName = BXLConfigLoader.PRODUCT_NAME_SPP_R300;
        } else if ((deviceName.indexOf("SPP-R400") >= 0)) {
            productName = BXLConfigLoader.PRODUCT_NAME_SPP_R400;
        } else if ((deviceName.indexOf("SRP-Q300") >= 0)) {
            productName = BXLConfigLoader.PRODUCT_NAME_SRP_Q300;
        } else if ((deviceName.indexOf("SPP-R410") >= 0)) {
            productName = BXLConfigLoader.PRODUCT_NAME_SPP_R410;
        } else {
            // User-Defined
//            if (deviceName.endsWith("R200")) {
//                productName = MyR200Printer.PRODUCT_NAME_SPP_R200;

             if (deviceName.endsWith("R210")) {
                    productName = BXLConfigLoader.PRODUCT_NAME_SPP_R210;

            } else if (deviceName.endsWith("R200II")) {
                productName = BXLConfigLoader.PRODUCT_NAME_SPP_R200II;

            } else if (deviceName.endsWith("R200III")) {
                productName = BXLConfigLoader.PRODUCT_NAME_SPP_R200III;

            } else if (deviceName.endsWith("R310")) {
                productName = BXLConfigLoader.PRODUCT_NAME_SPP_R310;

            } else if (deviceName.endsWith("Q300")) {
                productName = BXLConfigLoader.PRODUCT_NAME_SRP_Q300;

            } else if (deviceName.endsWith("R410")) {
                productName = BXLConfigLoader.PRODUCT_NAME_SPP_R410;

            } else {
                Log.w("DEBUG_JS", String.format("[MyPosPrinter.toProductName] %s not found : default R310", deviceName));
                productName = BXLConfigLoader.PRODUCT_NAME_SPP_R310;
            }
        }
        return productName;
    }

    public String getLine() {
        String string = "";
        switch (this) {
            case Default:
            case R310:
                string = "................................................";
                break;
            case R210:
            case R200II:
            case R215:
                string = "................................";
                break;
            case Q300:
                string = "..........................................";
                break;
            case R410:
                string = "--------------------------------------------------------------------";
                break;
        }
        return string;
    }

    public String getMemoLine() {
        String string = "";
        switch (this) {
            case Default:
            case R310:
                string = "..memo..........................................";
                break;
            case R210:
            case R200II:
            case R215:
                string = "..memo..........................";
                break;
            case Q300:
                string = "..memo....................................";
                break;
            case R410:
                string = "--memo--------------------------------------------------------------";
                break;
        }
        return string;
    }

    public MyPosPrinter.EscSeq getNormalFont() {
        switch (this) {
            default:
                return MyPosPrinter.EscSeq.FontNormal;
            case R410:
                return MyPosPrinter.EscSeq.FontNormal;
        }
    }
}
