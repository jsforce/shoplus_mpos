package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import com.squareup.timessquare.CalendarPickerView;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyUI;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Medivh on 2017-04-07.
 */

abstract public class PickPeriodDialog extends BaseDialog implements CalendarPickerView.OnDateSelectedListener, CalendarPickerView.OnInvalidDateSelectedListener {
    private CalendarPickerView calendarView;
    private TextView tvFrom, tvTo;
    protected DateTime selectedFromDate = BaseUtils.createDay(), selectedToDate = BaseUtils.createDay().plusDays(1).minusMillis(1);
    private int rangeDaysLimit = 0, periodDaysLimit = Integer.MAX_VALUE;

    public PickPeriodDialog(Context context, String title, int limitDays) {
        this(context, title, DateTime.now(), limitDays, -1);
    }

    public PickPeriodDialog(Context context, String title, DateTime selectedDay, int rangeDaysLimit, int periodDaysLimit) {
     this(context, title, selectedDay, rangeDaysLimit, periodDaysLimit, Pair.create(0, 0));
    }

    public PickPeriodDialog(Context context, String title, DateTime selectedDay, int rangeDaysLimit, int periodDaysLimit, Pair<Integer, Integer> startHm) {
        super(context);
        findTextViewById(R.id.dialog_pick_period_title, title);
        this.rangeDaysLimit = rangeDaysLimit;
        if (periodDaysLimit >= 0)
            this.periodDaysLimit = periodDaysLimit;
        selectedDay = BaseUtils.toDay(selectedDay);
        DateTime nowDateTime = BaseUtils.createDay();
        DateTime fromDateTime = nowDateTime.minusDays(rangeDaysLimit);
        DateTime toDateTime = nowDateTime.plusYears(1);
        if (BaseUtils.isBetween(selectedDay, fromDateTime, toDateTime) == false)
            selectedDay = BaseUtils.createDay();
        calendarView = (CalendarPickerView) findViewById(R.id.dialog_pick_period_calendar);
        calendarView.init(fromDateTime.toDate(), toDateTime.toDate())
                .withSelectedDate(selectedDay.toDate())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
        calendarView.setOnDateSelectedListener(this);
        calendarView.setOnInvalidDateSelectedListener(this);
        tvFrom = findTextViewById(R.id.dialog_pick_period_fromTextView, "");
        tvTo = findTextViewById(R.id.dialog_pick_period_toTextView, "");
        setOnClick(R.id.dialog_pick_period_cancel, R.id.dialog_pick_period_apply, R.id.dialog_pick_period_fromTime_plus,
                R.id.dialog_pick_period_fromTime_minus, R.id.dialog_pick_period_toTime_plus, R.id.dialog_pick_period_toTime_minus);

        selectedFromDate = selectedDay.plusHours(startHm.first).plusMinutes(startHm.second);
        selectedToDate = selectedFromDate.plusDays(1).minusMillis(1);
        setFromAndToDate();
        show();
    }

    public void highlightDays(List<DateTime> days) {
        if (calendarView != null) {
            List<Date> dates = new ArrayList<>();
            for (DateTime day : days)
                dates.add(day.toDate());
            calendarView.highlightDates(dates);
        }
    }

    @Override
    public void onDateSelected(Date date) {
        setFromAndToDate();
    }

    @Override
    public void onDateUnselected(Date date) {
        setFromAndToDate();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_pick_period;
    }

    protected ArrayList<Date> getHolidays() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        String dateInString = "21-04-2015";
        Date date = null;
        try {
            date = sdf.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayList<Date> holidays = new ArrayList<>();
        holidays.add(date);
        return holidays;
    }

    private void setFromAndToDate() {
        List<Date> selectedDates = calendarView.getSelectedDates(); // 중간 날짜도 모두 선택되는 방식
        if (selectedDates.size() == 0) {
            // now
        } else {
            // 날짜만 변경 (시, 분은 유지)
            selectedFromDate = new DateTime(selectedDates.get(0)).plusMinutes(selectedFromDate.getMinuteOfDay());
            DateTime toDate;
            if (selectedDates.size() == 1)
                toDate = new DateTime(selectedDates.get(0)).plusDays(1).minusMillis(1); // 당일 맨 마지막 시간
            else if (selectedDates.size() >= 2)
                toDate = new DateTime(selectedDates.get(selectedDates.size()-1)).plusMinutes(selectedToDate.getMinuteOfDay()); // 기존에 지정된 시분으로
            else
                toDate = DateTime.now();

            Duration duration = new Duration(selectedFromDate, toDate);
            if (duration.getStandardDays() <= periodDaysLimit) {
                selectedToDate = toDate;
                refreshText();
            } else {
                // 주의 calendarView.setDate를 하면 무한 순환한다
                MyUI.toastSHORT(context, String.format("%d일 이내에서 선택해주세요", periodDaysLimit));
            }
        }
    }

    protected void refreshText() {
        BaseUtils.setText(tvFrom, Empty.isEmpty(selectedFromDate), "", String.format("시작: %s", selectedFromDate.toString(BaseUtils.MDHm_Kor_Format)), false);
        BaseUtils.setText(tvTo, Empty.isEmpty(selectedToDate), "", String.format("종료: %s", selectedToDate.toString(BaseUtils.MDHm_Kor_Format)), false);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_pick_period_cancel) {
            dismiss();

        } else if (view.getId() == R.id.dialog_pick_period_apply) {
            onApplyClick(selectedFromDate, selectedToDate);
            dismiss();

        } else if (view.getId() == R.id.dialog_pick_period_fromTime_plus) {
            int hour = selectedFromDate.getHourOfDay();
            hour = hour + 1;
            if (hour > 23)
                hour = 0;
            selectedFromDate = setHour(selectedFromDate, hour);
            refreshText();

        } else if (view.getId() == R.id.dialog_pick_period_fromTime_minus) {
            int hour = selectedFromDate.getHourOfDay();
            hour = hour - 1;
            if (hour < 0)
                hour = 23;
            selectedFromDate = setHour(selectedFromDate, hour);
            refreshText();

        } else if (view.getId() == R.id.dialog_pick_period_toTime_plus) {
            int hour = selectedToDate.getHourOfDay();
            hour = hour + 1;
            if (hour > 23)
                hour = 0;
            selectedToDate = setHour(selectedToDate, hour);
            refreshText();

        } else if (view.getId() == R.id.dialog_pick_period_toTime_minus) {
            int hour = selectedToDate.getHourOfDay();
            hour = hour - 1;
            if (hour < 0)
                hour = 23;
            selectedToDate = setHour(selectedToDate, hour);
            refreshText();

        }
    }

    private DateTime setHour(DateTime dateTime, int hour) {
        return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), hour, dateTime.getMinuteOfHour(), dateTime.getSecondOfMinute());
    }

    abstract public void onApplyClick(DateTime fromDateTime, DateTime toDateTime);

    @Override
    public void onInvalidDateSelected(Date date) {
        MyUI.toastSHORT(context, String.format("%d일 이내만 선택이 가능합니다", rangeDaysLimit));
    }
}
