package com.teampls.shoplus.lib.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;


public class AppLinkDialog extends Dialog implements View.OnClickListener {
    private int thisView = R.layout.dialog_applink;
    private Context context;
    private RadioGroup rgTelecomMedia;
    private final int KAKAO = 0;
    private String androidLink = "", appleLink = "";

    public AppLinkDialog(Context context, String androidLink, String appleLink) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(thisView);

        this.context = context;
        this.androidLink = androidLink;
        this.appleLink = appleLink;

        ((ImageButton) findViewById(R.id.dialog_applink_android)).setOnClickListener(this);
        ((ImageButton) findViewById(R.id.dialog_applink_apple)).setOnClickListener(this);
        rgTelecomMedia = (RadioGroup) findViewById(R.id.dialog_applink_radiogroup);
    }

    @Override
    public void onClick(View view) {
        int media = 0;
        if (rgTelecomMedia.getCheckedRadioButtonId() == R.id.dialog_applink_rgKakao) {
            media = KAKAO;
        }
        if (view.getId() == R.id.dialog_applink_android) {
            switch (media) {
                case KAKAO:
                    throughKakao(androidLink);
                    break;
            }
            dismiss();
        } else if (view.getId() == R.id.dialog_applink_apple) {
            switch (media) {
                case KAKAO:
                    throughKakao(appleLink);
                    break;
            }
            dismiss();
        }
    }

    private void throughKakao(String link) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, link);
        intent.setPackage(BaseGlobal.KakaoPackage);
        try {
            Context kakaoContext = context.createPackageContext(BaseGlobal.KakaoPackage, Context.CONTEXT_IGNORE_SECURITY);
            context.startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            MyUI.toastSHORT(context, String.format("카카오톡이 설치되어 있지 않습니다"));
        }
    }

}
