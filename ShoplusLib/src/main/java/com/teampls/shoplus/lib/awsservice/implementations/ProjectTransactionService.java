package com.teampls.shoplus.lib.awsservice.implementations;

import android.support.annotation.Nullable;
import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ProjectTransactionServiceProtocol;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.PaymentTermsJSONData;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.SlipItemJSONData;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.SlipJSONData;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.TransactionDraftJSONData;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.datatypes.MyTriple;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.datatypes.TransactionLookupData;
import com.teampls.shoplus.lib.datatypes.TransactionNoteData;
import com.teampls.shoplus.lib.enums.AutoMessageRequestTimeConfigurationType;
import com.teampls.shoplus.lib.enums.SlipGenerationType;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.protocol.TransactionDraftDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 거래기록 관련 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectTransactionService implements ProjectTransactionServiceProtocol {

    private String getTransactionsResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions");
    }

    private String getATransactionResourcePath(String phoneNumber, String transactionId) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", transactionId);
    }

    private String getRecentTransactionsResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", "period", "recent");
    }

    private String getMonthlyCounterpartSpecificTransactionsResourcePath(String phoneNumber, String counterpartPhoneNumber, DateTime month) {
        String periodStr = (month == null) ? "base" : APIGatewayHelper.getRemoteMonthString(month);
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", "period", periodStr, "counterpart-specific", counterpartPhoneNumber);
    }

    private String getTransactionSearchWithAnItemResourcePath(String phoneNumber, int itemid) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", "lookup", String.valueOf(itemid));
    }

    private String getMonthlyTransactionSearchWithAnItemResourcePath(String phoneNumber, int itemid, DateTime month) {
        String periodStr = (month == null) ? "base" : APIGatewayHelper.getRemoteMonthString(month);
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", "lookup", String.valueOf(itemid), periodStr);
    }

    private String getATransactionCancellationResourcePath(SlipDataKey slipDataKey) {
        return MyAWSConfigs.getResourcePath(slipDataKey.ownerPhoneNumber, "transactions",
                APIGatewayHelper.getRemoteDateTimeString(slipDataKey.createdDateTime), "cancellation");
    }

    private String getTransactionDraftsResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", "drafts");
    }

    private String getATransactionDraftResourcePath(String phoneNumber, long draftId) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", "drafts", String.valueOf(draftId));
    }

    private String getABuyerSpecificPriceResourcePath(String phoneNumber, int itemid, String buyer) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "items", String.valueOf(itemid), "buyer-prices", buyer);
    }

    private String getNotesOfATransactionResourcePath(String phoneNumber, String transactionId) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", transactionId, "notes");
    }

    @Deprecated
    private String getCounterpartSpecificTransactionsResourcePath(String phoneNumber, String counterpartPhoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "sales-slips", "counterpart-specific", counterpartPhoneNumber);
    }

    private String getPhoneNumberForTransactions(UserShopInfoProtocol userShop) {
        return userShop.getMyShopOrUserPhoneNumber();     // Transactions 정보에서는 메인 샵 정보를 사용하지 않는다.
    }

    @Override
    public MyTriple<AccountsData, SlipDataProtocol, String> postTransaction(
            UserShopInfoProtocol userShop, String counterpart, DateTime createdDatetime, List<SlipItemDataProtocol> transactionItems,
            int onlinePayment, int entrustedPayment, int demandForUnpayment, AutoMessageRequestTimeConfigurationType autoMessageRequestTimeConfig, String linkedChatbotOrderId)
            throws MyServiceFailureException {
        try {
            String resource = this.getTransactionsResourcePath(getPhoneNumberForTransactions(userShop));
            PaymentTermsJSONData paymentData = new PaymentTermsJSONData(onlinePayment, entrustedPayment, null, null);
            SlipJSONData transaction = SlipJSONData.build(
                    SlipGenerationType.NONE, getPhoneNumberForTransactions(userShop), counterpart, createdDatetime, null, new ArrayList<String>(),
                    transactionItems, paymentData, demandForUnpayment, linkedChatbotOrderId);
            transaction.setAsTransaction();
            transaction.setAutoMessageConfiguration(autoMessageRequestTimeConfig);  // 자동 메시지 송신 설정
            String response = APIGatewayClient.requestPOST(resource, transaction.toInputModelString(), UserHelper.getIdentityToken());

            JSONObject resultObj = new JSONObject(response);
            String itemsLink = resultObj.optString("tr-item-link", "");
            JSONObject accountsObj = resultObj.optJSONObject("accounts");
            if (accountsObj == null) {
                accountsObj = new JSONObject();     // fallback (일반적으로 발생해서는 안 되는 케이스)
            }
            return MyTriple.create(new AccountsData(accountsObj), (SlipDataProtocol) transaction, itemsLink);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    private String getPeriodSpecificTransactionsResourcePath(String phoneNumber, String period) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "transactions", "period", period);
    }

    @Override
    public List<SlipDataProtocol> getTransactions(UserShopInfoProtocol userShop, DateTime dateTimeFrom, DateTime dateTimeTo, Boolean isBuyer) throws MyServiceFailureException {
        String startDateTimeStr = APIGatewayHelper.getRemoteDateTimeString(dateTimeFrom);
        String endDateTimeStr = APIGatewayHelper.getRemoteDateTimeString(dateTimeTo);
        String resource = this.getPeriodSpecificTransactionsResourcePath(getPhoneNumberForTransactions(userShop), startDateTimeStr + "-" + endDateTimeStr);
        return this.getTransactionsFromRemote(resource, isBuyer);
    }

    @Override
    public SlipDataProtocol getATransaction(UserShopInfoProtocol userShop, String transactionId, Boolean isBuyer) throws MyServiceFailureException {
        try {
            String resource = this.getATransactionResourcePath(getPhoneNumberForTransactions(userShop), transactionId);
            Map<String, String> queryParams = new HashMap<>();
            if (isBuyer) {
                queryParams.put("buyer", "true");
            }
            String response = APIGatewayClient.requestGET(resource, queryParams, UserHelper.getIdentityToken());
            return new SlipJSONData(response);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<SlipDataProtocol> getRecentTransactions(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getRecentTransactionsResourcePath(getPhoneNumberForTransactions(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            return SlipJSONData.build(response);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<TransactionLookupData> searchTransactionDataOfItem(UserShopInfoProtocol userShop, int itemid) throws MyServiceFailureException {
        String resource = this.getTransactionSearchWithAnItemResourcePath(getPhoneNumberForTransactions(userShop), itemid);
        return this.searchTransactionDataOfItem(resource);
    }

    @Override
    public List<TransactionLookupData> searchMonthlyTransactionDataOfItem(UserShopInfoProtocol userShop, int itemid, DateTime month) throws MyServiceFailureException {
        String resource = this.getMonthlyTransactionSearchWithAnItemResourcePath(getPhoneNumberForTransactions(userShop), itemid, month);
        return this.searchTransactionDataOfItem(resource);
    }

    private List<TransactionLookupData> searchTransactionDataOfItem(String resource) throws MyServiceFailureException {
        try {
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            JSONArray lookupArr = new JSONObject(response).getJSONArray("lookup");
            List<TransactionLookupData> result = new ArrayList<>();
            for (int i = 0; i < lookupArr.length(); ++i) {
                // 특정 데이터 포맷 오류가 있는 경우 출력하고 무시할 것인가(로그성), 아니면 포맷 예외를 발생시킬 것인가?
                // 일반적으로는 발생하지 말아야 하는 경우이나, 프로토콜 상에서의 제약조건을 잘못 설정할 우려가 있음
                try {
                    result.add(new TransactionLookupData(lookupArr.getJSONObject(i)));
                } catch (JSONException e) {
                    Log.i("DEBUG_JS", "[ProjectExodarService.searchTransactionDataOfItem] Data Format Error");
                }
            }
            Collections.reverse(result);        // 원본 데이터는 시간의 순방향으로 정렬되어 있다. iOS API 층위의 반환과 align을 맞추기 위해 reversed를 반환
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public SlipDataProtocol cancelTransaction(SlipDataKey slipDataKey) throws MyServiceFailureException {
        try {
            String resource = this.getATransactionCancellationResourcePath(slipDataKey);
            String response = APIGatewayClient.requestPOST(resource, null, UserHelper.getIdentityToken());
            return new SlipJSONData(response);
        } catch (AmazonClientException | JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public TransactionDraftDataProtocol postDraft(UserShopInfoProtocol userShop, String recipient, List<SlipItemDataProtocol> transactionItems, String memo) throws MyServiceFailureException {
        try {
            String resource = this.getTransactionDraftsResourcePath(getPhoneNumberForTransactions(userShop));
            JSONObject requestBody = this.getDraftDataObj(0, recipient, transactionItems, memo);
            String response = APIGatewayClient.requestPOST(resource, requestBody.toString(), UserHelper.getIdentityToken());
            return new TransactionDraftJSONData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<TransactionDraftDataProtocol> getDrafts(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        try {
            String resource = this.getTransactionDraftsResourcePath(getPhoneNumberForTransactions(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            List<TransactionDraftDataProtocol> result = new ArrayList<>();
            JSONArray draftArr = new JSONArray(response);
            for (int i = 0; i < draftArr.length(); ++i) {
                result.add(new TransactionDraftJSONData(draftArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public TransactionDraftDataProtocol updateDraft(UserShopInfoProtocol userShop, long draftId, long versionId, List<SlipItemDataProtocol> transactionItems, String memo) throws MyServiceFailureException {
        try {
            String resource = this.getATransactionDraftResourcePath(getPhoneNumberForTransactions(userShop), draftId);
            JSONObject requestBody = this.getDraftDataObj(versionId, null, transactionItems, memo);

            String response = APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
            return new TransactionDraftJSONData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public TransactionDraftDataProtocol deleteDraft(UserShopInfoProtocol userShop, long draftId, long versionId) throws MyServiceFailureException {
        String response = "";
        try {
            String resource = this.getATransactionDraftResourcePath(getPhoneNumberForTransactions(userShop), draftId);
            Map<String, String> queryParams = new HashMap<>();
            if (versionId > 0) {
                queryParams.put("version", String.valueOf(versionId));
            }
            response = APIGatewayClient.requestDELETE(resource, queryParams, UserHelper.getIdentityToken());
            return new TransactionDraftJSONData(new JSONObject(response));
        } catch (JSONException e) {
            Log.e("DEBUG_JS", String.format("[ProjectTransactionService.deleteDraft] response : %s", response));
            throw new MyServiceFailureException(e);
        }
    }

    @Nullable
    @Override
    public Integer getBuyerSpecificPrice(UserShopInfoProtocol userShop, int itemid, String buyer) throws MyServiceFailureException {
        try {
            String resource = this.getABuyerSpecificPriceResourcePath(getPhoneNumberForTransactions(userShop), itemid, buyer);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            JSONObject result = new JSONObject(response);
            if (result.has("price")) {
                return Integer.valueOf(result.getInt("price"));
            } else {
                return null;
            }
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    private JSONObject getDraftDataObj(long versionId, String recipient, List<SlipItemDataProtocol> transactionItems, String memo) throws JSONException {
        JSONObject draftDataObj = new JSONObject();
        JSONArray draftItemArr = new JSONArray();
        for (SlipItemDataProtocol transactionItem : transactionItems) {
            SlipItemJSONData trItem = new SlipItemJSONData(transactionItem);
            draftItemArr.put(trItem.getObject());
        }
        draftDataObj.put("draft_items", draftItemArr);

        if (versionId > 0) {
            draftDataObj.put("version", versionId);
        }

        if (recipient != null) {
            draftDataObj.put("recipient", recipient);
        }
        if (memo != null && !memo.isEmpty()) {
            draftDataObj.put("memo", memo);
        }
        return draftDataObj;
    }

    /**
     * @author JsForce
     */
//    @Override
//    @Deprecated
//    public List<SlipDataProtocol> downloadCounterpartSpecificTransactions(UserShopInfoProtocol userShop, String counterpartPhoneNumber, Boolean isBuyer) throws MyServiceFailureException {
//        String resource = this.getCounterpartSpecificTransactionsResourcePath(getPhoneNumberForTransactions(userShop), counterpartPhoneNumber);
//        return this.getTransactionsFromRemote(resource, isBuyer);
//    }

    @Override
    public List<SlipDataProtocol> getMontlyCounterpartSpecificTransactions(UserShopInfoProtocol userShop, String counterpartPhoneNumber, DateTime month, Boolean isBuyer) throws MyServiceFailureException {
        String resource = this.getMonthlyCounterpartSpecificTransactionsResourcePath(getPhoneNumberForTransactions(userShop), counterpartPhoneNumber, month);
        return this.getTransactionsFromRemote(resource, isBuyer);
    }

    @Override
    public List<SlipDataProtocol> getOldCounterpartSpecificTransactions(UserShopInfoProtocol userShop, String counterpartPhoneNumber, Boolean isBuyer) throws MyServiceFailureException {
        String resource = this.getMonthlyCounterpartSpecificTransactionsResourcePath(getPhoneNumberForTransactions(userShop), counterpartPhoneNumber, null);
        return this.getTransactionsFromRemote(resource, isBuyer);
    }

    private List<SlipDataProtocol> getTransactionsFromRemote(String resource, Boolean isBuyer) throws MyServiceFailureException {
        try {
            String response = APIGatewayClient.requestGET(resource, this.getTransactionQueryStringParams(isBuyer), UserHelper.getIdentityToken());
            return SlipJSONData.build(response);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    private Map<String, String> getTransactionQueryStringParams(Boolean isBuyer) {
        Map<String, String> params = new HashMap<>();
        params.put("type", "transaction");
        params.put("buyer", isBuyer.toString());
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [SILVER-449] Transaction Note Services

    @Override
    public List<TransactionNoteData> getTransactionNotes(UserShopInfoProtocol userShop, String transactionId) throws MyServiceFailureException {
        try {
            String resource = this.getNotesOfATransactionResourcePath(getPhoneNumberForTransactions(userShop), transactionId);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            return TransactionNoteData.buildFromResponse(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public List<TransactionNoteData> postTransactionNote(UserShopInfoProtocol userShop, String transactionId, String noteText) throws MyServiceFailureException {
        try {
            JSONObject requestBodyObj = new JSONObject().put("note", noteText);
            String resource = this.getNotesOfATransactionResourcePath(getPhoneNumberForTransactions(userShop), transactionId);
            String response = APIGatewayClient.requestPOST(resource, requestBodyObj.toString(), UserHelper.getIdentityToken());
            return TransactionNoteData.buildFromResponse(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }
}
