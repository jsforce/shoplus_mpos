package com.teampls.shoplus.lib;

import android.content.Context;

import com.teampls.shoplus.lib.awsservice.ProjectSpecificUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.implementations.ProjectSpecificUserService;

/**
 * @author lucidite
 */
public class ProjectSpecificUserServiceManager {
    public static ProjectSpecificUserServiceProtocol defaultService(Context context) {
        return new ProjectSpecificUserService();
    }
}
