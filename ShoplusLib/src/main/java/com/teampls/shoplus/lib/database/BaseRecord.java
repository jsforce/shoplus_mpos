package com.teampls.shoplus.lib.database;

import android.text.TextUtils;
import android.util.Log;

import com.teampls.shoplus.lib.datatypes.MyShopInformationData;

import org.joda.time.DateTime;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Medivh on 2017-12-04.
 */

public class BaseRecord<T> {

    public List<T> toList() {
        List<T> results = new ArrayList<>();
        results.add((T) this);
        return results;
    }

    public static boolean isSame(Object object1, Object object2) {
        if (object1.getClass() != object2.getClass()) {
            Log.w("DEBUG_JS", String.format("[BaseRecord.isSame] different class %s, %s", object1.getClass().getSimpleName(), object2.getClass().getSimpleName()));
            return false;
        }

        boolean doShowLogout = false; //(object1 instanceof MyShopInformationData);

        try {
            for (Field field : object1.getClass().getDeclaredFields()) {
                if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] field %s", field.getName()));
                field.setAccessible(true);
                if (field.getName().equals("id") || field.getName().startsWith("_"))
                    continue;

                Object value1 = field.get(object1);
                Object value2 = field.get(object2);
                if (value1 == null || value2 == null)
                    return false;

                if (field.getType().isAssignableFrom(String.class)) {
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] string %s, %s", value1, value2));
                    if (value1.equals(value2) == false) return false;

                } else if (field.getType().isAssignableFrom(int.class)) {
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] int %d, %d", field.getInt(object1), field.getInt(object2)));
                    if (field.getInt(object1) != field.getInt(object2)) return false;

                } else if (field.getType().isAssignableFrom(boolean.class)) {
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] boolean %s, %s", field.getBoolean(object1), field.getBoolean(object2)));
                    if (field.getBoolean(object1) != field.getBoolean(object2)) return false;

                } else if (field.getType().isAssignableFrom(DateTime.class)) { // OK
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] dateTime %s, %s", (DateTime) value1, (DateTime) value2));
                    if (((DateTime) value1).isEqual((DateTime) value2) == false) return false;

                } else if (field.getType().isAssignableFrom(long.class)) {
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] long %d, %d", field.getLong(object1), field.getLong(object2)));
                    if (field.getLong(object1) != field.getLong(object2)) return false;

                } else if (field.getType().isAssignableFrom(float.class)) {
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] float %f, %f", field.getFloat(object1), field.getFloat(object2)));
                    if (field.getFloat(object1) != field.getFloat(object2)) return false;

                } else if (field.getType().isAssignableFrom(double.class)) {
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] double %f, %f", field.getDouble(object1), field.getDouble(object2)));
                    if (field.getDouble(object1) != field.getDouble(object2)) return false;

                } else if (field.getType().isAssignableFrom(List.class)) {
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] list %s, %s", (List<String>) value1, (List<String>) value2));
                    List<String> list1 = (List<String>) value1;
                    List<String> list2 = (List<String>) value2;
                    if (list1.size() != list2.size())
                        return false;

                    for (int index = 0; index < list1.size(); index++)
                        if (list1.get(index).equals(list2.get(index)) == false)
                            return false;

                } else if (field.getType().isAssignableFrom(Set.class)) {
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] set %s, %s", (Set<String>) value1, (Set<String>) value2));
                    //Log.w("DEBUG_JS", String.format("[BaseRecord.isSame] SET found !"));
                    Set<String> list1 = (Set<String>) value1;
                    Set<String> list2 = (Set<String>) value2;
                    if (list1.size() != list2.size())
                        return false;

                    for (String item1 : list1)
                        if (list2.contains(item1) == false)
                            return false;

                } else {
                    if (doShowLogout) Log.i("DEBUG_JS", String.format("[BaseRecord.isSame] ELSE %s, %s", value1, value2));
                    if (value1.toString().equals(value2.toString()) == false) return false;

                }
            }
            if (doShowLogout) Log.w("DEBUG_JS", String.format("[BaseRecord.isSame] %s ==> SAME", object1.getClass().getSimpleName()));
            return true;

        } catch (IllegalAccessException e) {
            Log.e("DEBUG_JS", String.format("[BaseRecord.isSame] %s", e.getLocalizedMessage()));
            return false;
        }
    }

}
