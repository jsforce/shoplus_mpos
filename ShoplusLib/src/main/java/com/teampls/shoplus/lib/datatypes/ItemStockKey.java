package com.teampls.shoplus.lib.datatypes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SizeType;

/**
 * ItemStockDataProtocol에서 아이템 및 옵션을 식별하기 위한 Key 데이터
 *
 * @author lucidite
 */

public class ItemStockKey implements Comparable<ItemStockKey> {
    private int itemId;
    private ColorType colorOption;
    private SizeType sizeOption;

    public ItemStockKey() {}

    public ItemStockKey(int itemId, ColorType colorOption, SizeType sizeOption) {
        this.itemId = itemId;
        this.colorOption = colorOption;
        this.sizeOption = sizeOption;
    }

    public ItemStockKey(String apiParamStr) throws IllegalArgumentException {
        String[] elems = apiParamStr.split("-", 3);
        if (elems.length == 3) {
            String itemIdStr = elems[0];
            try {
                this.itemId = Integer.valueOf(itemIdStr).intValue();
                this.colorOption = ColorType.ofRemoteStr(elems[1]);
                this.sizeOption = SizeType.ofRemoteStr(elems[2]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException(apiParamStr);
            }
        } else {
            throw new IllegalArgumentException(apiParamStr);
        }
    }

    @Nullable
    public static ItemStockKey build(int itemId, String optionStringWithDelimiter) {
        String[] optionStrArr = optionStringWithDelimiter.split("-", 2);
        if (optionStrArr.length == 2) {
            ColorType color = ColorType.ofRemoteStr(optionStrArr[0].toLowerCase());
            SizeType size = SizeType.ofRemoteStr(optionStrArr[1].toLowerCase());
            return new ItemStockKey(itemId, color, size);
        } else {
            Log.e("DEBUG_TAG", "옵션 문자열 파싱 오류 (포맷 불일치): " + optionStringWithDelimiter);
            return null;
        }
    }

    public ItemOptionRecord toOptionRecord() {
        return new ItemOptionRecord(this, new ItemOptionData());
    }

    public int getItemId() {
        return itemId;
    }

    public ColorType getColorOption() {
        return colorOption;
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %d, %s, %s", callLocation, itemId, colorOption.getUiName(), sizeOption.uiName ));
        ItemOptionRecord i;
    }

    public String toString(String delimiter) {
        return String.format("%s%s%s", colorOption.getUiName(), delimiter, sizeOption.uiName);
    }

    public String toKeyString() {
        return String.format("%d%s%s", itemId, colorOption.toString(), sizeOption.toString());
    }

    public SizeType getSizeOption() {
        return sizeOption;
    }

    public String getOptionStringWithDelimiter() {
        return this.colorOption.toRemoteStr() + "-" + this.sizeOption.toRemoteStr();
    }

    public String toAPIParamStr() {
        return this.itemId + "-" + colorOption.toRemoteStr() + "-" + sizeOption.toRemoteStr();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemStockKey)) return false;

        ItemStockKey that = (ItemStockKey) o;
        if (itemId != that.itemId) return false;
        if (sizeOption != that.sizeOption) return false;
        return colorOption == that.colorOption;
    }

    @Override
    public int hashCode() {
        int result = itemId;
        result = 31 * result + colorOption.hashCode();
        result = 31 * result + sizeOption.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ItemStockKey{itemid=" + itemId +  ", (" + sizeOption.toString() + ", " + colorOption.toString() + ")}";
    }

    @Override
    public int compareTo(@NonNull ItemStockKey record) {
        final int BEFORE    =-1;
        final int EQUAL     = 0;
        final int AFTER     = 1;

        if (this.colorOption.ordinal() < record.colorOption.ordinal()) {
            return BEFORE;

        } else if (this.colorOption.ordinal() > record.colorOption.ordinal()) {
            return AFTER;

        } else {
            if (this.sizeOption.ordinal() < record.sizeOption.ordinal()) {
                return BEFORE;

            } else if (this.sizeOption.ordinal() > record.sizeOption.ordinal()) {
                return AFTER;

            } else {
                return EQUAL;
            }
        }
    }
}
