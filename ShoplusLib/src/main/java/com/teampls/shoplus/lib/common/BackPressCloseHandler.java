package com.teampls.shoplus.lib.common;

import android.app.Activity;
import android.widget.Toast;

public class BackPressCloseHandler {
	private long backKeyPressedTime = 0;
    private Toast toast;
    private Activity activity;
    private int totalDoubleClickCount = 0;
 
    public BackPressCloseHandler(Activity context) {
        this.activity = context;
    }
 
    public boolean doubleClicked() {
        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            toast = Toast.makeText(activity,"\'뒤로\'버튼을 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT);
            toast.show();
            return false;
        } else if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
            totalDoubleClickCount++;
            toast.cancel();
            return true;
        } else {
        	return false;
        }
    }

    public int getTotalDoubleClickCount() {
        return totalDoubleClickCount;
    }

    public void resetTotalDoubleClickCount() {
        totalDoubleClickCount = 0;
    }
 
}