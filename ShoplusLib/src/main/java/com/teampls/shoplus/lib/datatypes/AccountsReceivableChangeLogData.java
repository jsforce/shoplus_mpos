package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 온라인 + 대납 변경이력 데이터 (도매용)
 *
 * @author lucidite
 */

public class AccountsReceivableChangeLogData {
    private List<AccountChangeLogDataProtocol> onlineChanges;
    private List<AccountChangeLogDataProtocol> entrustedChanges;

    public static Comparator<AccountChangeLogDataProtocol> comparator = new Comparator<AccountChangeLogDataProtocol>() {
        @Override
        public int compare(AccountChangeLogDataProtocol o1, AccountChangeLogDataProtocol o2) {
            return o2.getLogId().compareTo(o1.getLogId());
        }
    };

    public AccountsReceivableChangeLogData(JSONObject data) throws JSONException {
        JSONArray onlineChangesArr = data.getJSONArray("online");
        JSONArray entrustedChangesArr = data.getJSONArray("entrusted");

        this.onlineChanges = new ArrayList<>();
        this.entrustedChanges = new ArrayList<>();

        for (int i = 0; i < onlineChangesArr.length(); ++i) {
            this.onlineChanges.add(new AccountChangeLogData(onlineChangesArr.getJSONObject(i)));
        }
        for (int i = 0; i < entrustedChangesArr.length(); ++i) {
            this.entrustedChanges.add(new AccountChangeLogData(entrustedChangesArr.getJSONObject(i)));
        }
        Collections.sort(this.onlineChanges, AccountsReceivableChangeLogData.comparator);
        Collections.sort(this.entrustedChanges, AccountsReceivableChangeLogData.comparator);
    }

    public List<AccountChangeLogDataProtocol> getOnlineChanges() {
        return onlineChanges;
    }

    public List<AccountChangeLogDataProtocol> getEntrustedChanges() {
        return entrustedChanges;
    }

}
