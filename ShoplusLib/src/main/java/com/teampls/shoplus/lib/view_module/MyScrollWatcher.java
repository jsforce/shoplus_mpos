package com.teampls.shoplus.lib.view_module;

import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;

/**
 * Created by Medivh on 2017-11-13.
 */

public class MyScrollWatcher implements AbsListView.OnScrollListener {
    private BaseDBAdapter adapter;
    private boolean lastItemVisible = false, firstItemVisible = false;
    private View onLastView;
    // 주의 맨위가 first, 맨 아래가 last임

    public MyScrollWatcher(BaseDBAdapter adapter) {
        this.adapter = adapter;
    }

    public void setViewOnLastVisible(View onLastView) {
        this.onLastView = onLastView;
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        adapter.setScrollState(scrollState);
        switch (scrollState) {
            case SCROLL_STATE_FLING:
                break;
            case SCROLL_STATE_IDLE:
                if (firstItemVisible)
                    onFirstItemVisible();
                if (lastItemVisible)
                    onLastItemVisible();
                else
                    onLastItemInvisible();
                // 한번에 다 보일 수 있음에 주의
                break;
            case SCROLL_STATE_TOUCH_SCROLL:
                break;
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        firstItemVisible = (totalItemCount > 0) && (firstVisibleItem == 0);
        lastItemVisible = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount);
        if (firstItemVisible || lastItemVisible) {
            adapter.setScrollState(SCROLL_STATE_IDLE);
        } else {
            onLastItemInvisible();
            onScrolling();
        }
    }

    public void onFirstItemVisible() {

    }

    public void onScrolling() {

    }

    public void onLastItemVisible() {
        if (onLastView != null)
            onLastView.setVisibility(View.VISIBLE);

    }

    public void onLastItemInvisible() {
        if (onLastView != null)
            onLastView.setVisibility(View.GONE);
    }


    public void check(final ListView listView) {
        if (listView.getFirstVisiblePosition() == 0)
            onFirstItemVisible();
        if (listView.getLastVisiblePosition() == adapter.getCount() - 1) {
            onLastItemVisible();
        } else {
            onLastItemInvisible();
        }
    }

}
