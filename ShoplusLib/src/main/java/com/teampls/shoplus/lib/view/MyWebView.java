package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;

import java.net.URISyntaxException;

/**
 * Created by Medivh on 2016-10-14.
 */
public class MyWebView extends Activity {
    protected WebView webView;
    protected static Context context;
    protected static String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        setContentView(R.layout.base_web);
        getActionBar().hide();
        webView = (WebView) findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClientClass());
        webView.getSettings().setJavaScriptEnabled(true);
        if (url.isEmpty() == false)
            webView.loadUrl(url);
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.WRITE_STORAGE) && BasePreference.landScapeOrientationEnabled.get(context))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public static void start(Context context, String url) {
        MyWebView.context = context;
        MyWebView.url = url;
        ((Activity) context).startActivity(BaseUtils.getIntent(context, MyWebView.class));
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) { // 새 API는 21부터 가
            if (url != null && url.startsWith("intent://")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Intent existPackage = getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                    if (existPackage != null) {
                        startActivity(intent);
                    } else {
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                        marketIntent.setData(Uri.parse("market://details?id="+intent.getPackage()));
                        startActivity(marketIntent);
                    }
                    return true;
                }catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (url != null && url.startsWith("market://")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    if (intent != null) {
                        startActivity(intent);
                    }
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
            view.loadUrl(url);
            return false;
        }
    }



}
