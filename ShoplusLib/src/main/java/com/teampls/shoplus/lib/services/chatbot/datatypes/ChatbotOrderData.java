package com.teampls.shoplus.lib.services.chatbot.datatypes;

import android.support.annotation.NonNull;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.SlipItemJSONData;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderDeliveryType;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderState;
import com.teampls.shoplus.lib.services.chatbot.enums.OrderRejectReason;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotOrderDataProtocol;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author lucidite
 */
public class ChatbotOrderData implements ChatbotOrderDataProtocol, Comparable<ChatbotOrderData> {
    private String orderId;
    private ChatbotOrderState state;
    private String shopPhoneNumber;
    private String buyerPhoneNumber;
    private List<SlipItemDataProtocol> orderItems;
    private ChatbotOrderDeliveryType deliveryType;
    private String address;
    private String memo;
    private String linkedTransactionId;
    private OrderRejectReason rejectReason;
    private MSortingKey sortingKey = MSortingKey.Date;

    public ChatbotOrderData(){}

    public ChatbotOrderData(ChatbotOrderDataProtocol protocol) {
        this.orderId = protocol.getOrderId();
        this.state = protocol.getState();
        this.shopPhoneNumber = protocol.getShopPhoneNumber();
        this.buyerPhoneNumber = protocol.getBuyerPhoneNumber();
        this.orderItems = protocol.getOrderItems();
        this.deliveryType = protocol.getDeliveryType();
        this.address = protocol.getAddress();
        this.memo = protocol.getMemo();
        this.linkedTransactionId = protocol.getLinkedTransactionId();
        this.rejectReason = protocol.getRejectionReason();
    }

    public ChatbotOrderData(JSONObject obj) throws JSONException {
        this.orderId = obj.getString("order_id");
        this.state = ChatbotOrderState.fromRemoteStr(obj.optString("order_state", ""));
        this.shopPhoneNumber = obj.getString("shop_id");
        this.buyerPhoneNumber = obj.optString("buyer"); // obj.getString("buyer"); 우선은 되게 만드려고

        this.orderItems = new ArrayList<>();
        JSONArray orderItemArr = obj.getJSONArray("order_items");
        for (int i = 0; i < orderItemArr.length(); ++i) {
            this.orderItems.add(new SlipItemJSONData(orderItemArr.getJSONObject(i)));
        }
        this.deliveryType = ChatbotOrderDeliveryType.fromRemoteStr(obj.optString("delivery"));
        this.address = obj.optString("address", "");
        this.memo = obj.optString("memo", "");
        this.linkedTransactionId = obj.optString("tr_id", "");
        this.rejectReason = OrderRejectReason.fromRemoteStr(obj.optString("rej_reason", ""));
    }

    @Override
    public String getOrderId() {
        return orderId;
    }

    @Override
    public DateTime getCreatedDateTime() {
        return APIGatewayHelper.getDateTimeFromRemoteString(this.orderId);
    }

    @Override
    public ChatbotOrderState getState() {
        return state;
    }

    public void setState(ChatbotOrderState state) {
        this.state = state;
    }

    @Override
    public String getShopPhoneNumber() {
        return shopPhoneNumber;
    }

    @Override
    public String getBuyerPhoneNumber() {
        return buyerPhoneNumber;
    }

    @Override
    public List<SlipItemDataProtocol> getOrderItems() {
        return orderItems;
    }

    @Override
    public ChatbotOrderDeliveryType getDeliveryType() {
        return deliveryType;
    }

    public List<SlipItemRecord> getEditingItems(List<SlipItemRecord> localItems) {
        List<SlipItemRecord> results = new ArrayList<>();
        int serial = 0;
        for (SlipItemDataProtocol protocol : orderItems) {
            results.add(new SlipItemRecord(orderId, serial, getCreatedDateTime(), protocol));
            serial++;
        }

        if (localItems.isEmpty())
            return results;
        else
            return localItems;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public String getMemo() {
        return memo;
    }

    @Override
    public String getLinkedTransactionId() {
        return linkedTransactionId;
    }

    @Override
    public OrderRejectReason getRejectionReason() {
        return rejectReason;
    }

    @Override
    public int compareTo(@NonNull ChatbotOrderData o) {
        switch (sortingKey) {
            default:
            case Date:
                return -getCreatedDateTime().compareTo(o.getCreatedDateTime());
        }
    }
}
