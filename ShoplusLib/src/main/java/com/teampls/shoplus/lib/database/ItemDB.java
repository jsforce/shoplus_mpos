package com.teampls.shoplus.lib.database;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseKey;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_global.ImageDB;
import com.teampls.shoplus.lib.database_global.ImageRecord;
import com.teampls.shoplus.lib.database_global.ThumbImageDB;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.datatypes.ItemDataBundle;
import com.teampls.shoplus.lib.datatypes.NewArrivalMessageKey;
import com.teampls.shoplus.lib.datatypes.SpecificItemDataBundle;
import com.teampls.shoplus.lib.enums.DBResultType;
import com.teampls.shoplus.lib.enums.ImageSizeType;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.ItemDataProtocol;
import com.teampls.shoplus.lib.protocol.NewArrivalsDataProtocol;
import com.teampls.shoplus.lib.protocol.NewArrivalsItemDataProtocol;
import com.teampls.shoplus.lib.view.MainProgressBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ItemDB extends BaseDB<ItemRecord> {
    private static ItemDB instance = null;
    protected static int Ver = 1;
    public ImageDB images = null;
    public ThumbImageDB thumbImages = null;
    public ItemOptionDB options = null;
    public LocalItemDB locals = null;
    private ItemRecord downloadFirstItem = new ItemRecord();
    private static final String Cache_KEY_NotFountInServer = "itemDB.not.found.in.server";
    private String KEY_BookMarks = "itemDB.bookmarks";

    public enum Column {
        _id, itemId, providerPhoneNumber, name, chineseName, description, createdDateTime, modifiedDateTime,
        pickedDateTime, mainImagePath, price, retailPrice, note, serialNum, category, state;

        public static String[] toStrs() {
            return BaseUtils.toStrs(Column.values());
        }
    }

    protected ItemDB(Context context, String DBname, String DBtable, int DBversion, String[] columns) {
        super(context, DBname, DBtable, DBversion, columns);
    }

    public static ItemDB getInstance(Context context) {
        if (instance == null) {
            instance = new ItemDB(context);
        }
        return instance;
    }

    protected ItemDB(Context context, String DBName, int DBversion, String[] columns) {
        super(context, DBName, DBversion, columns);
    }

    @Override
    protected int getId(ItemRecord record) {
        return record.id;
    }

    protected ItemDB(Context context) {
        super(context, "ItemDB7", Ver, Column.toStrs());
        this.images = ImageDB.getInstance(context);
        this.thumbImages = ThumbImageDB.getInstance(context);
        this.options = ItemOptionDB.getInstance(context);
        this.locals = LocalItemDB.getInstance(context);
        //     this.subImagePaths = SubImagePathDB.getInstance(context);
    }

    // ItemDB를 다른 앱에서 호출할 경우 일부 context는 문제가 된다
    public Context getUiContext() {
        if (MyUI.isActiveActivity(context, ""))
            return context;
        else if (MyUI.isActiveActivity(BaseAppWatcher.getCurrentActivity(), ""))
            return BaseAppWatcher.getCurrentActivity();
        else {
            Log.e("DEBUG_JS", String.format("[ItemDB.getUiContext] context == null"));
            return null;
        }
    }

    @Override
    public void deleteDB() {
        super.deleteDB();
        options.deleteDB();
        locals.deleteDB();
    }

    public void clear() {
        options.clear();
        locals.clear();
        super.clear();
    }

    public void deleteDBWithoutImage() {
        super.deleteDB();
        options.deleteDB();
        locals.deleteDB();
    }

    @Override
    public int delete(int id) {
        final ItemRecord record = getRecord(id);
        options.deleteBy(record.itemId);
        locals.deleteBy(record.itemId);
        return super.delete(id);
    }

    public int deleteBy(int itemId) {
        ItemRecord record = getRecordBy(itemId);
        return delete(record.id);
    }

    public void insertWithImage(ItemRecord record, String localImagePath, Bitmap bitmap, ImageSizeType sizeType) {
        insert(record);
        //       subImagePaths.insertAll(new ImagePathRecord(record.itemId, record.mainImagePath, isMain));
        switch (sizeType) {
            case ORIGINAL:
                images.insert(new ImageRecord(record.itemId, localImagePath, record.mainImagePath, MyGraphics.toByte(bitmap)));
                break;
            case THUMB:
                thumbImages.insert(new ImageRecord(record.itemId, localImagePath, record.mainImagePath, MyGraphics.toByte(bitmap)));
                break;
        }
    }

    public void changeMainImage(ItemRecord record, String localPath, String remotePath, Bitmap fullBitmap) {
        // ItemDB
        record.mainImagePath = remotePath;
        updateOrInsert(record);

        // ImageDB
        ImageRecord imageRecord = images.getRecordBy(record.itemId);
        imageRecord.itemId = record.itemId;
        imageRecord.remotePath = remotePath;
        imageRecord.localPath = localPath;
        imageRecord.setImage(fullBitmap);
        images.updateOrInsert(imageRecord);

        // ThumbImageDB
        ImageRecord thumbImageRecord = thumbImages.getRecordBy(record.itemId);
        if (thumbImageRecord.id > 0)
            thumbImages.deleteBy(record.itemId); // thumb은 서버로 부터 받도록 지운다
    }


    public DBResult updateOrInsert(ItemRecord record) {
        ItemRecord dbRecord = getRecordBy(record.itemId); // key
        if (dbRecord.id > 0) {
            if (record.isSame(dbRecord) == false) {
                // 이미지가 교체되었으면 해당 이미지는 삭제해야 한다.
                if (dbRecord.mainImagePath.equals(record.mainImagePath) == false) {
                    if (images != null)
                        images.deleteBy(dbRecord.itemId);
                    if (thumbImages != null)
                        thumbImages.deleteBy(dbRecord.itemId);
                }
                //             subImagePaths.updateOrInsert(new ImagePathRecord(record.itemId, record.mainImagePath, true));
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            //    subImagePaths.updateOrInsert(new ImagePathRecord(record.itemId, record.mainImagePath, true));
            // 이론적으로는 insert지만 예외가 있다. itemDB가 버전업을 하게 되면 item은 없지만 path는 있는 상황이 발생한다
            return insert(record);
        }
    }

    public void deleteInvalids(List<Integer> validItemIds) {
        if (validItemIds.size() == 0) return;

        final List<Integer> invalidItemIds = getIntegers(Column.itemId);
        invalidItemIds.removeAll(validItemIds);
        deleteAll(Column.itemId, invalidItemIds);

        new MyAsyncTask(getUiContext(), "deleteInvalids") {
            @Override
            public void doInBackground() {
                options.deleteAll(ItemOptionDB.Column.itemId, invalidItemIds);
                locals.deleteAll(LocalItemDB.Column.itemId, invalidItemIds);
            }
        };
    }

    public MyMap<Integer, Integer> getStockMap() {
        return options.getStockMapByItem();
    }

    public MyMap<Integer, ItemRecord> getItemMap() {
        MyMap<Integer, ItemRecord> results = new MyMap<>(new ItemRecord());
        for (ItemRecord record : getRecords())
            results.put(record.itemId, record);
        return results;
    }

    public int getItemId(int id) {
        return this.getInt(Column.itemId, id);
    }

    public int getId(int itemId) {
        return getId(ItemDB.Column.itemId, itemId);
    }

    public ItemRecord getLatestRecord() {
        return getRecordBy(BaseUtils.getMax(getItemIds()));
    }

    public List<Integer> getItemIds() {
        return this.getIntegers(Column.itemId);
    }

    public List<ItemRecord> getRecordsBy(List<Integer> itemIds) {
        ArrayList<ItemRecord> records = new ArrayList<ItemRecord>(0);
        if (itemIds.size() <= 0) return records;
        for (int itemId : itemIds) {
            ItemRecord record = getRecordBy(itemId);
            if (record.id > 0) // found
                records.add(record);
        }
        return records;
    }

    public List<ItemRecord> getRecordsByState(ItemStateType... states) {
        return getRecordsByState(BaseUtils.toList(states));
    }

    public List<ItemRecord> getRecordsByState(List<ItemStateType> states) {
        List<ItemRecord> results = new ArrayList<ItemRecord>(0);
        for (ItemRecord record : getRecords())
            if (states.contains(record.state))
                results.add(record);
        return results;
    }

    @Override
    protected ItemRecord getEmptyRecord() {
        return new ItemRecord();
    }

    @Override
    protected ItemRecord createRecord(Cursor cursor) {
        return new ItemRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getInt(Column.itemId.ordinal()),
                cursor.getString(Column.providerPhoneNumber.ordinal()),
                cursor.getString(Column.name.ordinal()),
                cursor.getString(Column.chineseName.ordinal()),
                cursor.getString(Column.description.ordinal()),
                BaseUtils.toDateTime(cursor.getString(Column.createdDateTime.ordinal())),
                BaseUtils.toDateTime(cursor.getString(Column.modifiedDateTime.ordinal())),
                BaseUtils.toDateTime(cursor.getString(Column.pickedDateTime.ordinal())),
                cursor.getString(Column.mainImagePath.ordinal()),
                cursor.getInt(Column.price.ordinal()),
                cursor.getInt(Column.retailPrice.ordinal()),
                cursor.getString(Column.note.ordinal()),
                cursor.getString(Column.serialNum.ordinal()),
                ItemCategoryType.valueOf(cursor.getString(Column.category.ordinal())),
                ItemStateType.valueOf(cursor.getString(Column.state.ordinal()))
        );
    }

    @Override
    protected ContentValues toContentvalues(ItemRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.itemId.name(), record.itemId);
        contentvalues.put(Column.providerPhoneNumber.name(), record.providerPhoneNumber);
        contentvalues.put(Column.name.name(), record.name);
        contentvalues.put(Column.chineseName.name(), record.chineseName);
        contentvalues.put(Column.description.name(), record.description);
        contentvalues.put(Column.createdDateTime.name(), record.createdDateTime.toString());
        contentvalues.put(Column.modifiedDateTime.name(), record.modifiedDateTime.toString());
        contentvalues.put(Column.pickedDateTime.name(), record.pickedDateTime.toString());
        contentvalues.put(Column.mainImagePath.name(), record.mainImagePath);
        contentvalues.put(Column.price.name(), record.unitPrice);
        contentvalues.put(Column.retailPrice.name(), record.retailPrice);
        contentvalues.put(Column.note.name(), record.privateNote);
        contentvalues.put(Column.serialNum.name(), record.serialNum);
        contentvalues.put(Column.category.name(), record.category.toString());
        contentvalues.put(Column.state.name(), record.state.toString());
        return contentvalues;
    }

    public ItemRecord getRecordBy(int itemId) {
        return getRecord(Column.itemId, Integer.toString(itemId));
    }

    public void clearCache() {
        setItemIds(Cache_KEY_NotFountInServer, new ArrayList<Integer>());
    }

    synchronized public void searchRecord(Context context, final int itemId, final MyOnTask<ItemRecord> onTask) {
        ItemRecord result = getRecordBy(itemId);
        if (result.id > 0) {
            if (onTask != null)
                onTask.onTaskDone(result);
            return;
        }

        if (getItemIds(Cache_KEY_NotFountInServer).contains(itemId)) {
            if (onTask != null)
                onTask.onTaskDone(result);
            return;
        }

        // Not found in DB
        new CommonServiceTask(getUiContext(), "searchRecord", "상품 다운로드 중...") {
            private ItemRecord itemRecord = new ItemRecord();

            @Override
            public void doInBackground() throws MyServiceFailureException {
                SpecificItemDataBundle bundle = itemService.getSpecificItem(UserSettingData.getInstance(getUiContext()).getUserShopInfo(), itemId);
                updateOrInsertWithOptionSync(new ItemDataBundle(bundle));
                itemRecord = new ItemRecord(bundle.getItem());
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone(itemRecord);
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                // Not found in Server
                List<Integer> itemIds = new ArrayList<>();
                itemIds.add(itemId);
                addItemIds(Cache_KEY_NotFountInServer, itemIds, 1000, true);
                Log.e("DEBUG_JS", String.format("[ItemDB.catchException] %d, %s", itemId, e.getLocalizedMessage()));
                if (onTask != null)
                    onTask.onTaskDone(itemRecord);
            }
        };
    }

    synchronized public void searchRecord(Context context, final String serialNum, final MyOnTask<ItemRecord> onTask) {
        ItemRecord result = getRecord(Column.serialNum, serialNum);
        if (result.id > 0) {
            if (onTask != null)
                onTask.onTaskDone(result);
        }

        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(String.format("[%s] 검색중..", serialNum));
        progressDialog.show();

        new CommonServiceTask(getUiContext(), "searchRecord") {
            private ItemRecord itemRecord = new ItemRecord();

            @Override
            public void doInBackground() throws MyServiceFailureException {
                SpecificItemDataBundle bundle = itemService.getSpecificItem(UserSettingData.getInstance(context).getUserShopInfo(), serialNum);
                if (bundle.getItem().getItemId() >= 1) {
                    updateOrInsertWithOptionSync(new ItemDataBundle(bundle));
                    itemRecord = new ItemRecord(bundle.getItem());
                }
            }

            @Override
            public void onPostExecutionUI() {
                progressDialog.dismiss();
                if (onTask != null)
                    onTask.onTaskDone(itemRecord);
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                if (e.getLocalizedMessage().contains("[404]Not Found")) {
                    MyUI.toastSHORT(context, String.format("없는 번호입니다"));
                } else {
                    super.catchException(e);
                }
                progressDialog.dismiss();
            }
        };
    }

    public boolean has(int itemId) {
        return hasValue(Column.itemId, itemId);
    }

    public void toLogCat(String location) {
        for (ItemRecord record : getRecords())
            record.toLogCat(location);
    }

    public void updateOrInsert(NewArrivalMessageKey messageKey, NewArrivalsDataProtocol protocol) {
        for (NewArrivalsItemDataProtocol itemBundle : protocol.getContents())
            updateOrInsert(new ItemRecord(messageKey, itemBundle));
    }

    public void setDownloadFirstItem() {
        downloadFirstItem = getRecordBy(BaseUtils.getMin(getIntegers(Column.itemId)));
    }

    public ItemRecord getDownloadedFirstItem() {
        if (downloadFirstItem.itemId != 0)
            return downloadFirstItem;
        else {
            setDownloadFirstItem();
            if (downloadFirstItem.itemId == 0) {
                return new ItemRecord();
            } else {
                return downloadFirstItem;
            }

        }
    }

    private List<Integer> bookmarks = new ArrayList<>();

    public List<Integer> getBookmarks() {
        if (bookmarks == null || bookmarks.isEmpty())
            bookmarks = KeyValueDB.getInstance(context).getIntegers(KEY_BookMarks); // for문 안에서 fileDB 조회를 막기 위해
        return bookmarks;
    }

    public void setBookmarks(List<Integer> itemIds) {
        bookmarks = itemIds;
        KeyValueDB.getInstance(context).put(KEY_BookMarks, itemIds);
    }

    @Deprecated
    private Pair<Integer, Integer> getMinMaxItemIds() {
        List<Integer> itemIds = getIntegers(Column.itemId);
        if (itemIds.size() >= 1) {
            return new Pair(Collections.min(itemIds), Collections.max(itemIds));
        } else {
            return new Pair(0, 0);
        }
    }

    public void updateOrInsert(final ItemDataBundle itemBundle) {
        refresh(itemBundle, false, false, null);
    }

    public void updateOrInsertWithOptionSync(final ItemDataBundle itemBundle) {
        refresh(itemBundle, false, true, null); // option 정보도 sync로 업데이트 한다
    }

    public void refresh(ItemDataBundle itemBundle) {
        refresh(itemBundle, true, false, null);
    }

    public void refresh(ItemDataBundle itemBundle, MyOnTask onTask) {
        refresh(itemBundle, true, false, onTask);
    }

    private void refresh(final ItemDataBundle itemBundle, final boolean doDeleteInvalids, final boolean doUpdateOptionSync, MyOnTask onTask) {
        if (itemBundle.getItems() == null || itemBundle.getItems().size() == 0) {
            if (doDeleteInvalids)
                clear();
            return;
        }

        setBookmarks(itemBundle.getBookmarks());
        // 상품 정보
        List<Integer> validIds = new ArrayList<>();
        for (ItemDataProtocol item : itemBundle.getItems()) {
            validIds.add(item.getItemId());
            ItemRecord record = new ItemRecord(item);
            updateOrInsert(record);
            if (onTask != null)
                onTask.onTaskDone(record);
        }
        if (doDeleteInvalids)
            deleteInvalids(validIds);

        if (doUpdateOptionSync) {
            options.refreshFast(itemBundle.getStock(), doDeleteInvalids, null);
            locals.updateOrInsert(itemBundle.getCostPrice()); // invalids 삭제는 refreshDay(items..)에서
        } else {
            // 옵션, 재고
            new MyAsyncTask(getUiContext()) {
                @Override
                public void doInBackground() {
                    options.refreshFast(itemBundle.getStock(), doDeleteInvalids, null);
                }
            };
            // 원가
            new MyAsyncTask(getUiContext()) {
                @Override
                public void doInBackground() {
                    locals.updateOrInsert(itemBundle.getCostPrice()); // invalids 삭제는 refreshDay(items..)에서
                }
            };
        }
    }

    public void refreshWithProgress(final ItemDataBundle itemBundle, final MainProgressBar mainProgressBar, final String taskId) {
        mainProgressBar.start(taskId, "아이템 업데이트 중", itemBundle.getItems().size());
        refresh(itemBundle, true, false, new MyOnTask() {
            @Override
            public void onTaskDone(Object result) {
                mainProgressBar.next(taskId);
            }
        });
    }

    public static ArrayList<ItemRecord> sortByShowingTime(ArrayList<ItemRecord> records) {
        for (int i = 0; i < records.size(); i++) {
            int index = i;
            for (int j = i + 1; j < records.size(); j++) {
                if (records.get(j).pickedDateTime.isAfter(records.get(index).pickedDateTime))
                    index = j;
            }
            ItemRecord latestRecord = records.get(index);
            records.set(index, records.get(i));
            records.set(i, latestRecord);
        }
        return records;
    }

    public void toLogCatSimple(String location) {
        for (ItemRecord itemRecord : getRecords())
            itemRecord.toLogCatSimple(location);
    }

    public void addItemIds(String key, List<Integer> itemIds, int limit, boolean asHashSet) {
        KeyValueDB.getInstance(context).addIntegers(key, itemIds, limit);
        if (asHashSet)
            KeyValueDB.getInstance(context).setIntegersAsHashSet(key);
    }

    public void setItemIds(String key, List<Integer> itemIds) {
        KeyValueDB.getInstance(context).put(key, ""); // clear
        KeyValueDB.getInstance(context).addIntegers(key, itemIds, -1);
    }

    public List<Integer> getItemIds(String key) {
        return KeyValueDB.getInstance(context).getIntegers(key);
    }

    public void addRecent(int itemId, int limit) {
        String recentItemsStr = KeyValueDB.getInstance(context).getValue(BaseKey.recentItems, "0");
        List<Integer> itemIds = new ArrayList<>();
        for (String recentItemStr : recentItemsStr.split(multiItemDelimiter)) {
            int recentItemId = BaseUtils.toInt(recentItemStr);
            if (recentItemId > 0 && recentItemId != itemId) // 동일한 것은 제외 (과거 기록에서 삭제하는 효과)
                itemIds.add(recentItemId);
        }
        itemIds.add(itemId); // 추가
        Collections.reverse(itemIds); // 최근 순서로
        if (itemIds.size() > limit)
            itemIds = itemIds.subList(0, limit);  // 최대 개수 지정
        Collections.reverse(itemIds); // 오래된 순서로
        String result = TextUtils.join(multiItemDelimiter, itemIds);
        //Log.i("DEBUG_JS", String.format("[ItemDB.addRecent] %s", result));
        KeyValueDB.getInstance(context).put(BaseKey.recentItems, result);
    }

    public List<ItemRecord> getRecentRecords() {
        String recentItemIdsStr = KeyValueDB.getInstance(context).getValue(BaseKey.recentItems, "0"); // 오래된 순서로
        //  Log.i("DEBUG_JS", String.format("[ItemDB.getRecentRecords] %s", recentItemIdsStr));
        List<Integer> itemIds = new ArrayList<>();
        for (String recentItemStr : recentItemIdsStr.split(multiItemDelimiter)) {
            int recentItemId = BaseUtils.toInt(recentItemStr);
            if (recentItemId > 0)
                itemIds.add(recentItemId);
        }
        Collections.reverse(itemIds); // 최근 순서로
        return getRecordsBy(itemIds);
    }
}

