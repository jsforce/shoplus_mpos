package com.teampls.shoplus.lib.datatypes;

import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

/**
 * (추가) 이미지 경로 정보를 저장하기 위한 데이터 클래스.
 * 이미지 ID와 이미지 경로(key)의 매핑 데이터를 갖는다.
 *
 * @author lucidite
 */

public class ItemImagePaths extends TreeMap<Integer, String> {

    public ItemImagePaths() {
        super();
    }

    public ItemImagePaths(JSONObject obj) throws JSONException {
        super();
        Iterator<String> keys = obj.keys();
        while (keys.hasNext()) {
            String imageIdStr = keys.next();
            this.put(Integer.valueOf(imageIdStr), obj.getString(imageIdStr));
        }
    }

    public void set(int imageId, String imagePath) {
        this.put(Integer.valueOf(imageId), imagePath);
    }

    public List<Pair<Integer, String>> getValues() {
        List<Pair<Integer, String>> result = new ArrayList<>();
        for (Entry<Integer, String> value : this.entrySet()) {
            result.add(new Pair<>(value.getKey(), value.getValue()));
        }
        return result;
    }
}
