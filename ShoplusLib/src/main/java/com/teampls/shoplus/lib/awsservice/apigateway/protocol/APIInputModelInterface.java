package com.teampls.shoplus.lib.awsservice.apigateway.protocol;

/**
 * @author lucidite
 */
public interface APIInputModelInterface {
    String toInputModelString();
}
