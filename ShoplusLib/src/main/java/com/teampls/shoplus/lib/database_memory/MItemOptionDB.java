package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.ItemOptionDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.datatypes.ItemOptionData;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2018-06-29.
 */

public class MItemOptionDB extends BaseMapDB<String, ItemOptionRecord> {
    private static MItemOptionDB instance;
    public MyListMap<Integer, ItemOptionRecord> itemIdOptionsMap = new MyListMap<>();
    private ItemOptionDB files;
    public MyMap<ItemOptionRecord, Integer> optionTraceStockMap = new MyMap<>(0);

    public static MItemOptionDB getInstance(Context context) {
        if (instance == null)
            instance = new MItemOptionDB(context);
        return instance;
    }

    private MItemOptionDB(Context context) {
        super(context, "ItemOptionDB");
        files = ItemOptionDB.getInstance(context);
    }

    @Override
    protected ItemOptionRecord getEmptyRecord() {
        return new ItemOptionRecord();
    }

    @Override
    public String getKeyValue(ItemOptionRecord record) {
        return record.key;
    }

    public void deleteByItemId(int itemId) {
        List<ItemOptionRecord> records = getRecordsBy(itemId);
        deleteAll(records);
    }
    
    public void addRecordIfNotExists(Map<ItemStockKey, ItemOptionData> s3StockMap, boolean doSaveToFile) {
        List<ItemOptionRecord> newOptionRecords = new ArrayList<>();
        for (ItemStockKey stockKey : s3StockMap.keySet()) {
            ItemOptionRecord record = new ItemOptionRecord(stockKey, s3StockMap.get(stockKey));
            if (hasKey(record.key) == false) {
                insert(record);
                newOptionRecords.add(record);
                itemIdOptionsMap.add(record.itemId, record); // 리스트에 추가
            }
        }
        if (doSaveToFile)
            save(newOptionRecords);
    }

    public MyMap<Integer, Integer> getStockMap() {
        MyMap<Integer, Integer> results = new MyMap<>(0);
        for (ItemOptionRecord optionRecord : getRecords())
            results.addInteger(optionRecord.itemId, optionRecord.getPositiveStock());
        return results;
    }

    public MyMap<Integer, Integer> getTraceStockMap() {
        MyMap<Integer, Integer> results = new MyMap<>(0);
        for (ItemOptionRecord optionRecord : optionTraceStockMap.keySet())
            results.addInteger(optionRecord.itemId, optionTraceStockMap.get(optionRecord));
        return results;
    }

    // ItemTraceDB 대신 사용중
    public void updateTraceStock(Map<ItemStockKey, ItemOptionData> s3StockMap) {
        for (ItemStockKey stockKey : s3StockMap.keySet())
            optionTraceStockMap.put(stockKey.toOptionRecord(), s3StockMap.get(stockKey).getStock());
    }

    public void set(int itemId, List<ItemOptionRecord> records, boolean doSaveToFile) {
        deleteAll(getRecordsBy(itemId));
        for (ItemOptionRecord optionRecord : records)
            insert(optionRecord);
        itemIdOptionsMap.put(itemId, records); // 새로
        if (doSaveToFile)
            save(records);
    }

    synchronized private void save(final List<ItemOptionRecord> optionRecords) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                files.beginTransaction();
                for (ItemOptionRecord optionRecord : optionRecords)
                    files.updateOrInsert(optionRecord);
                files.endTransaction();
            }
        };
    }

    public List<ItemOptionRecord> getRecordsBy(int itemId) {
        List<ItemOptionRecord> results = new ArrayList<>();
        for (ItemOptionRecord record : getRecords())
            if (record.itemId == itemId)
                results.add(record);
        return results;
    }


    public List<ItemOptionRecord> getRecordsBy(List<Integer> itemIds) {
        List<ItemOptionRecord> results = new ArrayList<>();
        for (ItemOptionRecord record : getRecords()) {
            if (itemIds.contains(record.itemId)) {
                results.add(record);
            }
        }
        return results;
    }

    public int getTraceSum(int itemId) {
        int result = 0;
        if (itemIdOptionsMap.containsKey(itemId)) {
            for (ItemOptionRecord record : itemIdOptionsMap.get(itemId)) {
                result = result + optionTraceStockMap.get(record);
            }
        } else {
            for (ItemOptionRecord record : getRecordsBy(itemId))
                result = result + optionTraceStockMap.get(record);
        }
        return result;
    }

    // Stock은 수동재고, Holdings는 QR 재고
    public int getStockSum(int itemId) {
        int result = 0;
        for (ItemOptionRecord record : getRecordsBy(itemId))
            result = result + record.stock;
        return result;
    }

    public int getTraceSum() {
        int result = 0;
        for (ItemOptionRecord record : getRecords()) {
            result = result + optionTraceStockMap.get(record);
        }
        return result;
    }

    public int getTraceSum(List<ItemRecord> itemRecords) {
        int result = 0;
        for (ItemRecord itemRecord : itemRecords) {
            for (ItemOptionRecord optionRecord : getRecordsBy(itemRecord.itemId))
                result = result + optionTraceStockMap.get(optionRecord);
        }
        return result;
    }


    public void toLogCat(String location) {
        for (ItemOptionRecord record : getRecords())
            record.toLogCat(location);
    }


}

/*

    private String getTraceMapFilePath() {
        return String.format("%s/traceMap.map", BaseAppWatcher.appDir);
    }

    public void saveTraceCountMap() {
        File file = MyDevice.openOrCreate(getTraceMapFilePath());
        FileOutputStream fileStream = null;
        ObjectOutputStream objectStream = null;
        try {
            fileStream = new FileOutputStream(file);
            objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(traceCountMap);
        } catch (IOException e) {
            Log.e("DEBUG_JS", String.format("[MItemOptionDB.saveTraceCount] %s", e.getLocalizedMessage()));
        } catch (Exception e) {
            Log.e("DEBUG_JS", String.format("[MItemOptionDB.saveTraceCount] %s", e.getLocalizedMessage()));
        } finally {
            try {
                if (fileStream != null) {
                    fileStream.flush();
                    fileStream.close();
                }
                if (objectStream != null) {
                    objectStream.flush();
                    objectStream.close();
                }
            } catch (IOException e) {
                Log.e("DEBUG_JS", String.format("[MItemOptionDB.saveTraceCount] %s", e.getLocalizedMessage()));
            }
        }
    }

    public void readTraceCountMpa() {
        File file = new File(getTraceMapFilePath());
        FileInputStream fileStream = null;
        ObjectInputStream objectStream = null;
        try {
            fileStream = new FileInputStream(file);
            objectStream = new ObjectInputStream(fileStream);
            traceCountMap = (MyMap<ItemOptionRecord, Integer>) objectStream.readObject();
        } catch (FileNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[MItemOptionDB.readTraceCount] %s", e.getLocalizedMessage()));
        } catch (IOException e) {
            Log.e("DEBUG_JS", String.format("[MItemOptionDB.readTraceCount] %s", e.getLocalizedMessage()));
        } catch (Exception e) {
            Log.e("DEBUG_JS", String.format("[MItemOptionDB.readTraceCount] %s", e.getLocalizedMessage()));
        } finally {
            try {
                if (fileStream != null)
                    fileStream.close();
                if (objectStream != null)
                    objectStream.close();
            } catch (IOException e) {
                Log.e("DEBUG_JS", String.format("[MItemOptionDB.saveTraceCount] %s", e.getLocalizedMessage()));
            }
        }
    }
 */
