package com.teampls.shoplus.lib.database_map;

import android.content.Context;

import com.teampls.shoplus.lib.database_global.GlobalDB;

/**
 * Created by Medivh on 2018-11-19.
 */

public class GlobalDBString extends BaseDBValue<String> {

    public GlobalDBString(String key, String defaultValue) {
        super(key, defaultValue);
    }

    @Override
    public KeyValueDB getDB(Context context) {
        return GlobalDB.getInstance(context);
    }

    @Override
    public void put(Context context, String value) {
        getDB(context).put(key, value);
    }

    @Override
    public String get(Context context) {
        return getDB(context).getValue(key, defaultValue);
    }

}
