package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

/**
 * Created by Medivh on 2018-04-17.
 */

public class MTransDB extends MemorySlipDB {
    private static MTransDB instance;

    public static MTransDB getInstance(Context context) {
        if (instance == null)
            instance = new MTransDB(context);
        return instance;
    }

    private MTransDB(Context context) {
        super(context, "MTransDB");
        items = MTransItemDB.getInstance(context);
    }

}
