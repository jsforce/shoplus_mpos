package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.teampls.shoplus.lib.ProjectSpecificUserServiceManager;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ProjectSpecificUserServiceProtocol;
import com.teampls.shoplus.lib.database.MyDB;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.datatypes.MessengerLinkData;
import com.teampls.shoplus.lib.datatypes.MyShopInformationData;
import com.teampls.shoplus.lib.datatypes.ServiceActivationData;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.datatypes.UserShopInfo;
import com.teampls.shoplus.lib.enums.BalanceChangeMethodType;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.MemberPermission;
import com.teampls.shoplus.lib.enums.MessengerType;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.enums.UserLevelType;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.protocol.UserSpecificSettingProtocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2018-01-28.
 */

public class UserSettingData {
    private static UserSettingData instance;
    private static UserSettingDataProtocol userSettingDataProtocol;
    private Context context;
    private UserSettingDB settingDB;
    private static Map<ColorType, String> customColorNames = new HashMap<>();
    public static boolean isUserTypeChanged = false, isUserLevelChanged = false, isWorkingShopChanged = false,
            isHasMasterChanged = false, isReceivableManagementChanged = false;
    private String shopNumberForMaster = "";
    private String testerNumber = "01037860747";

    public static UserSettingData getInstance(Context context) {
        if (instance == null)
            instance = new UserSettingData(context);
        return instance;
    }

    public static boolean isInstanceValid() {
        return (instance != null);
    }

    private UserSettingData(Context context) {
        this.context = context;
        settingDB = UserSettingDB.getInstance(context);
    }

    public UserSettingDataProtocol getProtocol() {
        return getProtocol("self");
    }

    public UserSettingDataProtocol getProtocol(String location) {
        if (userSettingDataProtocol == null) {
            refresh(context, location, null);
            return Empty.userSettingData; // 우선은 값을 기본값을 주고 다운받는 방식
        } else
            return userSettingDataProtocol;
    }

    public void autoRefresh() {
        if (userSettingDataProtocol == null)
            refresh(context, "autoRefresh", null);
    }

    public void autoRefresh(MyOnTask onTask) {
        if (userSettingDataProtocol == null)
            refresh(context, "autoRefresh", onTask);
        else
            onTask.onTaskDone("");
    }

    public MessengerType getActiveMessenger() {
        MessengerType result = MessengerType.KAKAOTALK;
        if (userSettingDataProtocol == null)
            return result;
        for (MessengerLinkData data : userSettingDataProtocol.getMessengerLinkData()) {
            if (data.getURLStr().isEmpty() == false) {
                switch (data.getType()) {
                    default:
                        continue;
                    case KAKAOTALK:
                        return MessengerType.KAKAOTALK;
                    case PLUSFRIEND:
                        return MessengerType.PLUSFRIEND;
                }
            }
        }
        return result;
    }

    public MyMap<MessengerType, MessengerLinkData> getMessengerLinkMap() {
        MyMap<MessengerType, MessengerLinkData> localLinkData = new MyMap<>(new MessengerLinkData());
        localLinkData.put(MessengerType.KAKAOTALK, new MessengerLinkData(MessengerType.KAKAOTALK, GlobalDB.getInstance(context).getMessengerValue(MessengerType.KAKAOTALK)));
        localLinkData.put(MessengerType.WECHAT, new MessengerLinkData(MessengerType.WECHAT, GlobalDB.getInstance(context).getMessengerValue(MessengerType.WECHAT)));
        localLinkData.put(MessengerType.PLUSFRIEND, new MessengerLinkData(MessengerType.PLUSFRIEND, GlobalDB.getInstance(context).getMessengerValue(MessengerType.PLUSFRIEND)));

        if (userSettingDataProtocol == null)
            return localLinkData;

        MyMap<MessengerType, MessengerLinkData> serverLinkData = new MyMap<>(new MessengerLinkData());
        for (MessengerLinkData data : userSettingDataProtocol.getMessengerLinkData()) // 빈 set일 수 있음
            serverLinkData.put(data.getType(), data);

        MyMap<MessengerType, MessengerLinkData> results = new MyMap<>(new MessengerLinkData());
        for (MessengerType type : localLinkData.keySet()) {// 3개 타입의 보장을 위해
            MessengerLinkData localData = localLinkData.get(type);
            MessengerLinkData serverData = serverLinkData.get(type); //  빈 정보일 수 있음
            if (serverData.getType() == MessengerType.NOT_DEFINED)
                serverData = new MessengerLinkData(type, "");
            if (serverData.getURLStr().isEmpty() && localData.getURLStr().isEmpty() == false) {
                results.put(type, localData);
                continue;
            }
            results.put(type, serverData);
        }
        return results;
    }

    public void updateMessenger(final MessengerType messengerType, final String url, final MyOnTask onTask) {
        new CommonServiceTask(context, "updateMessengerLink", "") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                workingShopService.updateMessengerLink(getUserShopInfo(), messengerType, url);
                globalDB.setMessengerUrl(messengerType, url);
                UserSettingDataProtocol userSettingDataProtocol = userService.getUserSetting(myDB.getPrivateNumber());
                set(userSettingDataProtocol); // 리턴값이 없어서 강제로 다시 업데이트 하자
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    public boolean isProtocolNull() {
        return userSettingDataProtocol == null;
    }

    public void set(UserSettingDataProtocol protocol) {
        this.userSettingDataProtocol = protocol;

        // 전체 set 중 실제 custom만 추출
        customColorNames.clear();
        for (ColorType colorType : protocol.getColorNames().keySet()) { // 새 이름으로 지정한 set이 내려온다
            customColorNames.put(colorType, protocol.getColorNames().get(colorType));
            // 단, 이제 우리가 이름을 변경하면 안됨, 레드가 맘에 들어 쓰고 있던 고객인데 빨강으로 변경시 적용되어버림.
        }

        //  user type : 미확인, 도매, 소매
        isUserTypeChanged = (userSettingDataProtocol.getUserType() != settingDB.getUserConfigurationType());
        if (isUserTypeChanged)
            Log.w("DEBUG_JS", String.format("[UserSettingData.setOnStarting] %s -> %s", settingDB.getUserConfigurationType(), userSettingDataProtocol.getUserType()));

        // user level : 매장, 직원, 관리자, 사장님
        isUserLevelChanged = (userSettingDataProtocol.getUserLevel() != settingDB.getUserLevel());
        if (isUserLevelChanged)
            Log.w("DEBUG_JS", String.format("[UserSettingData.setOnStarting] %s -> %s", settingDB.getUserLevel(), userSettingDataProtocol.getUserLevel()));

        isHasMasterChanged = (userSettingDataProtocol.doesShopHaveMasterUser() != settingDB.hasMaster());
        if (isHasMasterChanged)
            Log.w("DEBUG_JS", String.format("[UserSettingData.set] hasMaster? %s -> %s", settingDB.hasMaster(), userSettingDataProtocol.doesShopHaveMasterUser()));

        isReceivableManagementChanged = (userSettingDataProtocol.getBalanceChangeMethod() != settingDB.getReceivableManagementType());
        saveToFileDB(userSettingDataProtocol);
    }

    private void saveToFileDB(final UserSettingDataProtocol userSetting) {
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                settingDB.setUserConfigurationType(userSetting.getUserType());
                settingDB.setUserLevel(userSetting.getUserLevel());
                settingDB.setHasMaster(userSetting.doesShopHaveMasterUser());
                settingDB.setReceivableManagementType(userSetting.getBalanceChangeMethod());

                // Preference와 연동
                BasePreference.retailEnabled.setValue(context, userSetting.isRetailOn());
                BasePreference.isOneDigitBasicUnit.setValue(context, userSetting.isRawPriceInputMode());
//                for (MessengerLinkData linkData : userSetting.getMessengerLinkData()) // 당분간은 관리하지 않음
//                    GlobalDB.getInstance(context).setMessengerUrl(linkData.getType(), linkData.getURLStr());
            }
        };
    }

    public void setOnStarting(UserSettingDataProtocol protocol) {
        set(protocol);

        // 근무지 변경 : Local DB 삭제
     //   Log.w("DEBUG_JS", String.format("[UserSettingData.update] working shop : before %s after %s", settingDB.getWorkingShopPhoneNumber(), getUserShopInfo("setOnStarting").getShopPhoneNumber()));
        if (settingDB.getWorkingShopPhoneNumber().isEmpty() == false) {
            isWorkingShopChanged = (getUserShopInfo("setOnStarting").getShopPhoneNumber().equals(settingDB.getWorkingShopPhoneNumber()) == false);
            if (isWorkingShopChanged)
                Log.w("DEBUG_JS", String.format("[UserSettingData.update] working shop : %s --> %s", settingDB.getWorkingShopPhoneNumber(), getUserShopInfo("setOnStarting").getShopPhoneNumber()));
        }
        settingDB.setWorkingShopPhoneNumber(getUserShopInfo().getShopPhoneNumber());

        // 매장폰이고 shopname이 없는 경우 myDB에 입력된 nickname을 shopname으로 등록해두자
        if (isShopPhoneState() && getWorkingShop().getShopName().isEmpty()) {
            MyShopInformationData shopData = getWorkingShop();
            String myDBNickName = MyDB.getInstance(context).getPrivateName();
            if (myDBNickName.isEmpty() == false) {
                shopData.setShopName(myDBNickName);
                Log.w("DEBUG_JS", String.format("[UserSettingData.setOnStarting] set shop name : %s from myDB", myDBNickName));
                updateWorkingShopData(shopData, null);
            }
        }
    }

    public void updateWorkingShopData(final MyShopInformationData shopData, final MyOnTask onTask) {
        new CommonServiceTask(context, "MyInfoView") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                UserSettingDataProtocol protocol = workingShopService.updateMyShopInformation(getUserShopInfo(), shopData);
                set(protocol);
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    public int getDefaultCostMarginPercent() {
        if (userSettingDataProtocol == null)
            return 0;
        else
            return userSettingDataProtocol.getDefaultMarginAdditionRate();
    }

    public MyShopInformationData getWorkingShop() {
        if (userSettingDataProtocol == null)
            return new MyShopInformationData();
        else
            return userSettingDataProtocol.getShopInformation();
    }

    public MyShopInformationData cloneShopData() {
        if (userSettingDataProtocol == null)
            return new MyShopInformationData();
        else
            return userSettingDataProtocol.getShopInformation().clone();
    }

    public void update(final MyOnTask onTask) {
        new MyAsyncTask(context, "update") {
            @Override
            public void doInBackground() {
                try {
                    ProjectSpecificUserServiceProtocol userService = ProjectSpecificUserServiceManager.defaultService(context);
                    UserSettingDataProtocol userSettingDataProtocol = userService.getUserSetting(MyDB.getInstance(context).getPrivateNumber());
                    set(userSettingDataProtocol);
                } catch (MyServiceFailureException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }



    public void init(final Context context, String location, final MyOnAsync onTask) {
        download(context, location, "", new MyOnTask() {
            @Override
            public void onTaskDone(Object result) {
                if (onTask != null) {
                    if ((Boolean) result) {
                        onTask.onSuccess("");
                    } else {
                        onTask.onError(new Exception(""));
                    }
                }
            }
        });
    }

    public void refresh(final Context context, String location, final MyOnTask onTask) {
        download(context, location, "내 정보 복구 중...", onTask);
    }

    private boolean onDownloading = false;

    private void download(final Context context, String location, final String message, final MyOnTask onTask) {
        if (onDownloading) {
        //    Log.w("DEBUG_JS", String.format("[UserSettingData.download] %s onTasking....", location));
            return;
        }
        onDownloading = true;

        new CommonServiceTask(context, "downloadUserSetting", message) {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                UserSettingDataProtocol userSettingDataProtocol = userService.getUserSetting(myDB.getPrivateNumber());
                setOnStarting(userSettingDataProtocol);
            }

            @Override
            public void onPostExecutionUI() {
                onDownloading = false;
                if (message.isEmpty() == false)
                    MyUI.toastSHORT(context, String.format("복구완료"));
                if (onTask != null)
                    onTask.onTaskDone(true);
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                onDownloading = false;
                if (message.isEmpty() == false)
                    MyUI.toastSHORT(context, String.format("복구실패"));
                if (onTask != null)
                    onTask.onTaskDone(false);
            }
        };
    }

    private UserShopInfo getUserShopInfo(String callLocation) {
        UserShopInfo defaultInfo = new UserShopInfo("", "", MyDB.getInstance(context).getPrivateNumber());
        if (userSettingDataProtocol == null || userSettingDataProtocol.getUserSpecificSetting() == null) {
            MyUI.toastSHORT(context, String.format("기본 정보 재 확인중...."));
            refresh(context, callLocation, null);
            Log.e("DEBUG_JS", String.format("[UserSettingData.getUserShopInfo] userSetting == null, %s", callLocation));
            return defaultInfo;
        } else if (isBuyer()) {
            return defaultInfo; // buyer는 main shop 개념을 적용하지 않는다
        } else {
            String shopNumberForMaster = settingDB.getValue(UserSettingDB.KEY_ShopNumberForMaster);
            if (getMyRecord().phoneNumber.equals(testerNumber) && shopNumberForMaster.isEmpty() == false)
                userSettingDataProtocol.getUserSpecificSetting().setShop(shopNumberForMaster);
            return new UserShopInfo(userSettingDataProtocol.getMainShop(), userSettingDataProtocol.getUserSpecificSetting().getShop(), MyDB.getInstance(context).getPrivateNumber());
        }
    }

    public UserShopInfo getUserShopInfo() {
        return getUserShopInfo("");
    }

    public List<String> getAllowedShops() {
        List<String> results = new ArrayList<>();
        if (userSettingDataProtocol == null)
            return results;
        if (userSettingDataProtocol.getAllowedShops() == null)
            return results;
        return userSettingDataProtocol.getAllowedShops();
    }

    public void setWorkingShopPhoneNumberForMaster(String phoneNumber) {
        settingDB.put(UserSettingDB.KEY_ShopNumberForMaster, phoneNumber);
    }

    public void updateWorkingShopPhoneNumber(final String workingShopPhoneNumber, final MyOnTask onTask) {
        if (userSettingDataProtocol == null) {
            Log.e("DEBUG_JS", String.format("[GlobalDB.updateWorkingShopPhoneNumber] userSetting == null"));
            return;
        }
        new CommonServiceTask(context, "subPhoneNumber") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                UserSpecificSettingProtocol newSetting = userSettingDataProtocol.getUserSpecificSetting();
                newSetting.setShop(workingShopPhoneNumber);
                userSettingDataProtocol = userService.updateUserSpecificSetting(myDB.getPrivateNumber(), newSetting);
            }

            @Override
            public void onPostExecutionUI() {
                setWorkingShopPhoneNumberForMaster("");
                if (onTask != null)
                    onTask.onTaskDone("");
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                userSettingDataProtocol.getUserSpecificSetting().setShop("");
                if (e.getLocalizedMessage().contains("Access Denied")) {
                    MyUI.toast(context, String.format("%s 매장의 근무자로 등록이 안되어 있습니다. 등록을 확인 후 입력해주세요",
                            BaseUtils.toPhoneFormat(workingShopPhoneNumber)));
                } else {
                    super.catchException(e);
                }
            }
        };
    }

    public List<String> getLocations() {
        if (userSettingDataProtocol == null) {
            return new ArrayList<>();
        } else
            return userSettingDataProtocol.getItemTraceLocations();
    }

    public int getMaxBookmarkCount() {
        return 100;
    }

    public void updateLocations(final List<String> locations, final MyOnTask onTask) {
        new CommonServiceTask(context, "updateLocations") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                userSettingDataProtocol = mainShopService.updateItemTraceLocation(getUserShopInfo("updateLocations"), locations);
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    public int getBasicUnit() {
        return getProtocol("userSettingData").isRawPriceInputMode() ? 1 : 1000;
    }

    public boolean isOneDigitBasicUnit() {
        return getProtocol("userSettingData").isRawPriceInputMode();
    }

    public void setBasicUnit(final boolean isOneDigitUnit, final MyOnTask<Integer> onTask) {
        new CommonServiceTask(context, "setBasicUnit") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                userSettingDataProtocol = mainShopService.updateRawPriceInputMode(getUserShopInfo("setBasicUnit"), isOneDigitUnit);
                BasePreference.isOneDigitBasicUnit.setValue(context, isOneDigitUnit);
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone(getBasicUnit());
            }
        };
    }

    public void setReceivableManagementType(final BalanceChangeMethodType type, final MyOnTask onTask) {
        new CommonServiceTask(context, "setReceivableManagementType") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                userSettingDataProtocol = mainShopService.updateBalanceChangeMethod(getUserShopInfo(), type);
                settingDB.setReceivableManagementType(type);
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    public UserLevelType getUserLevelType() { // 앱간 공유가 필요없으므로 저장할 필요도 없다
        if (userSettingDataProtocol == null)
            return settingDB.getUserLevel();
        else
            return userSettingDataProtocol.getUserLevel();
    }

    public boolean isBuyerOrMasterOrNoMaster() {
        return (isBuyer() || isMasterOrNoMaster());
    }

    public boolean isMaster() {
        if (userSettingDataProtocol == null)
            return settingDB.getUserLevel() == UserLevelType.MASTER;
        return (userSettingDataProtocol.getUserLevel() == UserLevelType.MASTER);
    }

    public boolean isMasterOrNoMaster() {
        checkAbnormalUserLevel();
        return (!hasMaster() || isMaster());
    }
    // 서로 NOT의 관계

    public boolean hasMasterAndNotMaster() {
        checkAbnormalUserLevel();
        return (hasMaster() && !isMaster());
    }

    private boolean checkAbnormalUserLevel() {
        if (hasMaster()) {
            return false;
        } else {
            switch (getUserLevelType()) {
                case MANAGER:
                case EMPLOYEE:
                    Log.e("DEBUG_JS", String.format("[UserSettingData.checkAbnormalUserLevel] No Master shop, but Emploly/Manager found.."));
                    return true; // 사장이 없는 샵에서 매니저나 종업원이 있을 수 없다
                default:
                    return false;
            }
        }
    }

    public boolean hasMaster() {
        if (userSettingDataProtocol == null)
            return settingDB.hasMaster();
        return userSettingDataProtocol.doesShopHaveMasterUser();
    }

    public static boolean hasCustomColor(ColorType color) {
        return customColorNames.containsKey(color);
    }

    public static String getColorName(ColorType color) {
        if (hasCustomColor(color)) {
            return customColorNames.get(color);
        } else {
            return color.getOriginalUiName();
        }
    }

    public static Map<ColorType, String> getCustomColorNames() {
        return customColorNames;
    }


    public void updateCustomColorName(Map<ColorType, String> newNames) {
        for (ColorType color : newNames.keySet())
            customColorNames.put(color, newNames.get(color));
    }

    // userSetting 이전에 호출가능함
    public UserConfigurationType getUserConfigType() {
        if (userSettingDataProtocol == null) {
            return settingDB.getUserConfigurationType();
        } else if (userSettingDataProtocol.getUserType() == null) {
            return UserConfigurationType.NOT_DEFINED;
        } else {
            return userSettingDataProtocol.getUserType();
        }
    }

    public BalanceChangeMethodType getReceivableManagementType() {
        if (userSettingDataProtocol == null) {
            return settingDB.getReceivableManagementType();
        } else {
            return userSettingDataProtocol.getBalanceChangeMethod();
        }
    }

    public boolean isProvider() {
        return (getUserConfigType() == UserConfigurationType.PROVIDER);
    }

    public boolean isBuyer() {
        return (getUserConfigType() == UserConfigurationType.BUYER);
    }

    // 발행자 정보, getUserShopInfo().getMyShopOrUserPhoneNumber에 이름만 붙임
    public UserRecord getProviderRecord() {
        UserRecord result = new UserRecord();
        if (userSettingDataProtocol == null) {
            result.phoneNumber = settingDB.getWorkingShopPhoneNumber();
            if (result.phoneNumber.isEmpty())
                result.phoneNumber = MyDB.getInstance(context).getPrivateNumber();
            return result;
        }

        String myShopOrUserPhoneNumber = getUserShopInfo().getMyShopOrUserPhoneNumber();
        if (isBuyer()) { // Shop 정보를 무시한다 (이름, 번호)
            result = getMyRecord();
            if (result.phoneNumber.equals(myShopOrUserPhoneNumber) == false) // 소매가 도매로 매장 번호를 셋팅했고 그 번호가 남아있는 경우
                Log.e("DEBUG_JS", String.format("[UserSettingData.getProviderRecord] result %s != myShopOr.. %s", result.phoneNumber, myShopOrUserPhoneNumber));
            return result;
        }

        result.phoneNumber = myShopOrUserPhoneNumber;
        result.name = getWorkingShop().getShopName();

        if (isShopPhoneState()) {  // 매장폰이라 shop name이 없을 수 있다
            if (result.name.isEmpty())
                result.name = MyDB.getInstance(context).getPrivateName();
            if (result.name.isEmpty()) // 새로 설치해서 myDB에도 없을 수 있다
                result.name = "이름을 입력해주세요";
        }
        return result;
    }

    // 사실상 myDB 정보인데 nickname이 없을 경우 저장된 shop name을 대신 가져온다
    public UserRecord getMyRecord() {
        UserRecord result = MyDB.getInstance(context).getMyRecord().toUserRecord();
        if (userSettingDataProtocol == null)
            return result;

        if (result.name.isEmpty() && (userSettingDataProtocol.getShopInformation().getShopName().isEmpty() == false))
            result.name = userSettingDataProtocol.getShopInformation().getShopName();

        return result;
    }

    public boolean isShopPhoneState() {
        return getUserShopInfo("isShopPhoneState").getShopPhoneNumber().isEmpty();
    }

    public void updateUserConfig(final UserConfigurationType userConfigType, final MyOnTask onTask) {
        new CommonServiceTask(context, "updateUserConfig") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                if (userConfigType == UserConfigurationType.NOT_DEFINED) {
                    MyUI.toastSHORT(context, String.format("잘못된 유저 정보입니다 %s", userConfigType.toString()));
                } else {
                    userSettingDataProtocol = mainShopService.updateUserType(getUserShopInfo(), userConfigType);
                    settingDB.setUserConfigurationType(userSettingDataProtocol.getUserType());
                }
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    public void toLogCatMemberPermission(String callLocation) {
        if (userSettingDataProtocol == null) {
            Log.e("DEBUG_JS", String.format("[UserSettingData.MemberPermission] userSetting == null"));
            return;
        }
        for (MemberPermission memberPermission : MemberPermission.values())
            Log.i("DEBUG_JS", String.format("[%s] %s, %s, has? %s", callLocation, memberPermission.toString(),
                    memberPermission.getMemberPermission(this), memberPermission.hasPermission(this)));
    }

    public boolean hasMainShop() {
        if (userSettingDataProtocol == null)
            return false;
        else
            return (TextUtils.isEmpty(getUserShopInfo().getMainPhoneNumber()) == false);
    }

    public boolean hasWorkingShopNotMe() {
        if (userSettingDataProtocol == null)
            return false;
        String shopNumber = getUserShopInfo().getShopPhoneNumber();
        String myNumber = getUserShopInfo().getUserPhoneNumber(); // myDB에서 추출하는 것이라 empty일 수는 없다
        if (TextUtils.isEmpty(shopNumber) || TextUtils.isEmpty(myNumber))
            return false;
        if (shopNumber.equals(myNumber))
            return false;
        return true;
    }

    public boolean isActivated(ShoplusServiceType serviceType) {
        if (userSettingDataProtocol == null) {
            Log.e("DEBUG_JS", String.format("[UserSettingData.isActivation] null"));
            return false;
        }

        Map<ShoplusServiceType, ServiceActivationData> activatedAuths = userSettingDataProtocol.getServiceActivationData(); // 활성화 권한들
        for (ShoplusServiceType compatibleAuth : serviceType.getCompatibles()) {
            if (activatedAuths.containsKey(compatibleAuth)) { // 조회한 권한이 활성화 내에 있는지 확인
                ServiceActivationData data = activatedAuths.get(compatibleAuth);
                if (data.isCurrentlyActivated())
                    return true;
            }
        }
        return false;
    }

    public List<ShoplusServiceType> getActivations() {
        List<ShoplusServiceType> results = new ArrayList<>();
        if (userSettingDataProtocol == null) {
            Log.e("DEBUG_JS", String.format("[UserSettingData.getActivattions] null"));
            return results;
        }

        for (ShoplusServiceType type : ShoplusServiceType.values()) {
            if (type == ShoplusServiceType.NONE)
                continue;
            if (userSettingDataProtocol.getServiceActivationData().get(type) == null)
                continue;
            if (userSettingDataProtocol.getServiceActivationData().get(type).isCurrentlyActivated()) {
                results.add(type);
                //   Log.i("DEBUG_JS", String.format("[UserSettingData.getActivations] %s", type.uiStr));
            }
        }
        return results;
    }

    public boolean hasExpireDateTime() {
        List<ShoplusServiceType> results = new ArrayList<>();
        if (userSettingDataProtocol == null) {
            Log.e("DEBUG_JS", String.format("[UserSettingData.getActivattions] null"));
            return false;
        }

        for (ShoplusServiceType type : ShoplusServiceType.values()) {
            if (type == ShoplusServiceType.NONE)
                continue;
            if (userSettingDataProtocol.getServiceActivationData().get(type) == null)
                continue;
            if (userSettingDataProtocol.getServiceActivationData().get(type).getExpiration() != null) {
                return true;
                //   Log.i("DEBUG_JS", String.format("[UserSettingData.getActivations] %s", type.uiStr));
            }
        }
        return false;
    }

    static class UserSettingDB extends KeyValueDB {
        private static UserSettingDB instance;

        private static String KEY_USER_SETTING_CONFIG = "userSetting.config";
        private static String KEY_USER_LEVEL = "user.level";
        private static String KEY_WORKING_SHOP = "user.working.shop";
        private static String KEY_HAS_MASTER = "user.has.master";
        private static String KEY_ReceivableManagementType = "userSetting.receivable.management.type";
        private static String KEY_ShopNumberForMaster = "userSetting.shopNumbeForMaster";

        public static UserSettingDB getInstance(Context context) {
            if (instance == null)
                instance = new UserSettingDB(context);
            return instance;
        }

        private UserSettingDB(Context context) {
            super(context, "UserSettingDB", 1);
        }

        public boolean hasMaster() {
            return getBool(KEY_HAS_MASTER);
        }

        public void setHasMaster(boolean value) {
            put(KEY_HAS_MASTER, value);
        }

        public UserConfigurationType getUserConfigurationType() {
            return UserConfigurationType.valueOf(getValue(KEY_USER_SETTING_CONFIG, UserConfigurationType.NOT_DEFINED.toString()));
        }

        public void setUserConfigurationType(UserConfigurationType userConfigurationType) {
            put(KEY_USER_SETTING_CONFIG, userConfigurationType.toString());
        }

        public UserLevelType getUserLevel() {
            return UserLevelType.valueOf(getValue(KEY_USER_LEVEL, UserLevelType.SHOP.toString()));
        }

        public void setUserLevel(UserLevelType userLevel) {
            put(KEY_USER_LEVEL, userLevel.toString());
        }

        public String getWorkingShopPhoneNumber() {
            return getValue(KEY_WORKING_SHOP);
        }

        public void setWorkingShopPhoneNumber(String workingShop) {
            put(KEY_WORKING_SHOP, workingShop);
        }

        public void setReceivableManagementType(BalanceChangeMethodType type) {
            put(KEY_ReceivableManagementType, type.toString());
        }

        public BalanceChangeMethodType getReceivableManagementType() {
            return BalanceChangeMethodType.valueOf(getValue(KEY_ReceivableManagementType, BalanceChangeMethodType.ALL.toString()));
        }
    }
}

/*
//    public boolean hasMemberPermission(MemberPermission memberPermission) {
//        if (userSetting == null)
//            return false;
//
//        if (isBuyer()) {
//            return true; // Buyer 상태에서는 permission 체크를 하지 않음
//        } else {
//            //Log.i("DEBUG_JS", String.format("[UserSettingData.hasMemberPermission] %s, permitted %s, my %s", memberPermission.toString(), getMemberPermission(memberPermission), getUserLevelType()));
//            switch (memberPermission) {
//                case ChangeOnline:
//                    return userSetting.hasDalaranOnlineBalanceChangePermission();
//                case ChangeDeposit:
//                    return userSetting.hasDalaranDepositChangePermission();
//                case ChangeStockValue:
//                    return getUserLevelType().isAllowed(userSetting.getSilvermoonAuthData().getStockValueUpdatePermission());
//                case SetTransactionDate:
//                    return getUserLevelType().isAllowed(userSetting.getSilvermoonAuthData().getTransactionDateSettingPermission());
//                default:
//                    return getUserLevelType().isAllowed(memberPermission.getDefaultUserLevel());
//            }
//        }
//    }

//    public UserLevelType getMemberPermission(MemberPermission memberPermission) {
//        if (userSetting == null)
//            return memberPermission.getDefaultUserLevel();
//        switch (memberPermission) {
//            case ChangeOnline:
//                return userSetting.getDalaranAuthData().getOnlineBalanceChangePermission();
//            case ChangeDeposit:
//                return userSetting.getDalaranAuthData().getDepositChangePermission();
//            case ChangeStockValue:
//                return userSetting.getSilvermoonAuthData().getStockValueUpdatePermission();
//            case SetTransactionDate:
//                return userSetting.getSilvermoonAuthData().getTransactionDateSettingPermission();
//            default:
//                Log.e("DEBUG_JS", String.format("[UserSettingData.getMemberPermission] %s not implemented", memberPermission.toString()));
//                return memberPermission.getDefaultUserLevel();
//        }
//    }


//    public void updateMemberPermission(final MemberPermission memberPermission, final UserLevelType userLevelType, final MyOnTask onTask) {
//        if (userSetting == null) {
//            Log.e("DEBUG_JS", String.format("[UserSettingData.updateMemberPermission] userSetting == null"));
//            return;
//        }
//
//        new CommonServiceTask(context, "updateMemberPermission") {
//            @Override
//            public void doInBackground() throws MyServiceFailureException {
//                ProjectDalaranAuthData dalaranData = userSetting.getDalaranAuthData();
//                ProjectSilvermoonAuthData silvermoonData = userSetting.getSilvermoonAuthData();
//                switch (memberPermission) {
//                    case ChangeOnline:
//                        dalaranData.setOnlineBalanceChangePermission(userLevelType);
//                        userSetting = mainShopService.updateDalaranAuth(getUserShopInfo(), dalaranData);
//                        break;
//                    case ChangeDeposit:
//                        dalaranData = userSetting.getDalaranAuthData();
//                        dalaranData.setDepositChangePermission(userLevelType);
//                        userSetting = mainShopService.updateDalaranAuth(getUserShopInfo(), dalaranData);
//                        break;
//                    case ChangeStockValue:
//                        silvermoonData.setStockValueUpdatePermission(userLevelType);
//                        userSetting = mainShopService.updateSilvermoonAuth(getUserShopInfo(), silvermoonData);
//                        break;
//                    case SetTransactionDate:
//                        silvermoonData.setTransactionDateSettingPermission(userLevelType);
//                        userSetting = mainShopService.updateSilvermoonAuth(getUserShopInfo(), silvermoonData);
//                        break;
//                    case CancelTransactionAfter1Day:
//                        silvermoonData.setCancelTransactionAfter1DayPermission(userLevelType);
//                        userSetting = mainShopService.updateSilvermoonAuth(getUserShopInfo(), silvermoonData);
//                        break;
//                    default:
//                        Log.e("DEBUG_JS", String.format("[UserSettingData.updateMemberPermission] %s not implemented", memberPermission.toString()));
//                        break;
//                }
//
//            }
//
//            @Override
//            public void onPostExecutionUI() {
//                if (onTask != null)
//                    onTask.onTaskDone(true);
//            }
//
//            @Override
//            public void catchException(MyServiceFailureException e) {
//                super.catchException(e);
//                if (onTask != null)
//                    onTask.onTaskDone(false);
//            }
//        };
//    }

 */