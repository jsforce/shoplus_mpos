package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;

import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_map.KeyValueDB;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2018-03-14.
 */

public class BaseExcel {
    // SHEET https://poi.apache.org/apidocs/org/apache/poi/ss/usermodel/Sheet.html
    // ROW : https://poi.apache.org/apidocs/org/apache/poi/ss/usermodel/Row.html
    // CELL : https://poi.apache.org/apidocs/org/apache/poi/ss/usermodel/Cell.html
    protected Context context;
    protected Workbook workbook;
    protected Map<String, Sheet> sheetMap = new HashMap<>();
    protected KeyValueDB keyValueDB;
    protected UserDB userDB;

    public BaseExcel(Context context) {
        this.context = context;
        workbook = new HSSFWorkbook();
        keyValueDB = KeyValueDB.getInstance(context);
        userDB = UserDB.getInstance(context);
    }

    public Sheet addSheet(String title) {
        if (sheetMap.containsKey(title) == false)
            sheetMap.put(title, workbook.createSheet(title));
        return getSheet(title);
    }

    public Sheet getSheet(String sheetTitle) {
        if (sheetMap.containsKey(sheetTitle) == false)
            addSheet(sheetTitle);
        return sheetMap.get(sheetTitle);
    }

    public void addFirstRow(Sheet sheet, ExcelCell excelCell) {
        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue(excelCell.value);
    }

    public void addEmptyRow(Sheet sheet) {
        int rowNum = sheet.getLastRowNum();
        Row row = sheet.createRow(rowNum + 1);
        Cell cell = row.createCell(0);
        cell.setCellValue(" ");
    }

    public void addRow(Sheet sheet, List<ExcelCell> cells) {
        int rowNum = sheet.getLastRowNum();
        Row row = sheet.createRow(rowNum + 1);
        int cellNum = 0;
        for (ExcelCell excelCell : cells) {
            Cell cell = row.createCell(cellNum);
            if (excelCell.isInteger)
                cell.setCellValue(BaseUtils.toDouble(excelCell.value, 0));
            else // String
                cell.setCellValue(excelCell.value);
            if (excelCell.cellStyle != null)
                cell.setCellStyle(excelCell.cellStyle);
            cellNum++;
        }
    }

    public void addRow(Sheet sheet, Workbook workbook, String... strings) {
        List<ExcelCell> cells = new ArrayList<>();
        for (String string : strings)
            cells.add(new ExcelCell(workbook, string));
        addRow(sheet, cells);
    }

    public void addRow(Sheet sheet, Workbook workbook, String title, int value, String memo) {
        List<ExcelCell> cells = new ArrayList<>();
        cells.add(new ExcelCell(workbook, title));
        cells.add(new ExcelCell(workbook, value));
        cells.add(new ExcelCell(workbook, memo));
        addRow(sheet, cells);
    }

    public void addRow(Sheet sheet, Workbook workbook, String title, int... integers) {
        List<ExcelCell> cells = new ArrayList<>();
        cells.add(new ExcelCell(workbook, title));
        for (Integer integer : integers)
            cells.add(new ExcelCell(workbook, integer));
        addRow(sheet, cells);
    }

    public String saveAsFile() {
        String fileName = MyDevice.createTempFilePath(BaseAppWatcher.appDir, "xls");
        File file = new File(fileName);
        MyFiles.createFileIfNotExist(file);
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            MyDevice.refresh(context, file);
        } catch (FileNotFoundException e) {
            fileName = "";
            Log.e("DEBUG_JS", String.format("[BaseExcel.saveAsFile] %s not found", file.toString()));

        } catch (IOException e) {
            fileName = "";
            MyUI.toastSHORT(context, String.format("파일을 만들지 못했습니다"));

        } finally {
            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (IOException e) {
                fileName = "";
                Log.e("DEBUG_JS", String.format("[BaseExcel.saveAsFile] %s", e.getLocalizedMessage()));
            }
            return fileName;
        }
    }

    protected void setColumnWidth(Sheet sheet, int columnNum, int widthInChar) {
        for (int num = 0; num < columnNum; num++)
            sheet.setColumnWidth(num, widthInChar * 256);
    }

    protected void autoSizeColumnWidth(Sheet sheet, List<ExcelCell> excelCells) {
        int position = 0;
        for (ExcelCell excelCell : excelCells) {
            int currentCharacters = sheet.getColumnWidth(position) / 256;
            int stringCharacters = excelCell.isInteger ? excelCell.value.length() : Hangul.getLengthInChar(excelCell.value);
            if (stringCharacters <= 8)
                stringCharacters = stringCharacters + 2; // 너무 작으면 답답해 보이므로
            if (stringCharacters > currentCharacters)
                sheet.setColumnWidth(position, stringCharacters * 256);
            position++;
        }
    }

    public class ExcelCell {
        public String value = "";
        private CellStyle cellStyle;
        private boolean isInteger = false;

        public ExcelCell(Workbook workbook, String value) {
            this.value = value;
        }

        public ExcelCell(Workbook workbook, int value) {
            this.value = Integer.toString(value);
            isInteger = true;
        }

        // 라이브러리 문제로 일단은 쓰지 않는다. 모든 Class가 포함된 Lib는 30M를 더 먹는다
//        private ExcelCell (Workbook workbook, String string, int... gravities) {
//            cellStyle = workbook.createCellStyle();
//            this.string = string;
//            for (int gravity : gravities)
//                setGravity(gravity);
//        }

        public void setGravity(int gravity) {
            if (gravity == Gravity.CENTER_HORIZONTAL) {
                cellStyle.setAlignment(HorizontalAlignment.CENTER);
            } else if (gravity == Gravity.RIGHT) {
                cellStyle.setAlignment(HorizontalAlignment.RIGHT);
            }
        }
    }
}