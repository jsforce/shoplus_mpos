package com.teampls.shoplus.lib.awsservice;

import com.teampls.shoplus.lib.datatypes.ProjectDalaranAuthData;
import com.teampls.shoplus.lib.datatypes.ProjectSilvermoonAuthData;
import com.teampls.shoplus.lib.enums.BalanceChangeMethodType;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

import java.util.List;
import java.util.Map;

/**
 * @author lucidite
 */

public interface ProjectMainUserServiceProtocol {
    /**
     * 사용자 설정 중 사용자 타입(도매/소매)를 업데이트한다.
     *
     * @param userShop
     * @param userType
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateUserType(UserShopInfoProtocol userShop, UserConfigurationType userType) throws MyServiceFailureException;


    /**
     * 사용자 설정 중 사용자 정의 옵션 색상명 테이블을 업데이트한다.
     *
     * @param userShop
     * @param colorNames
     * @return
     * @throws MyServiceFailureException
     * @deprecated 색상명 사용자 정의 방식 변경 SILVER-359
     */
    @Deprecated
    UserSettingDataProtocol updateColorNames(UserShopInfoProtocol userShop, Map<ColorType, String> colorNames) throws MyServiceFailureException;

    /**
     * [SILVER-359] 표준색상의 이름을 사용자 정의한다.
     * [NOTE] 사용자 정의 색상의 이름은 변경할 수 없다 (추가만 가능).
     *
     * @param userShop
     * @param presetColor   변경하려는 색상
     * @param colorNameAlias    변경하려는 색상 이름
     * @return  변경 후의 색상명 사용자 정의 데이터. 로컬의 사용자 설정 중 색상명 정의 데이터를 이 데이터로 갱신해야 한다.
     * @throws MyServiceFailureException
     */
    Map<ColorType, String> renamePresetColor(UserShopInfoProtocol userShop, ColorType presetColor, String colorNameAlias) throws MyServiceFailureException;

    /**
     * [SILVER-359] 신규 사용자 색상 이름을 추가한다.
     * [NOTE] 사용자 정의 색상의 이름은 변경할 수 없다 (추가만 가능).
     * [NOTE] 서버에서 가장 마지막에 사용한 사용자 색상 슬롯의 다음 슬롯에 자동으로 입력한다. (race condition 위험 최소화)
     *
     * @param userShop
     * @param userColorName 추가하려는 사용자 색상 이름
     * @return 변경 후의 색상명 사용자 정의 데이터. 로컬의 사용자 설정 중 색상명 정의 데이터를 이 데이터로 갱신해야 한다.
     * @throws MyServiceFailureException    빈 문자열, 중복된 사용자 정의 색상명, 개수 한도(100) 초과 등
     */
    Map<ColorType, String> addUserColorName(UserShopInfoProtocol userShop, String userColorName) throws MyServiceFailureException;

    /**
     * 사용자 설정 중 (재고앱 용도의) 아이템 추적 위치 목록을 업데이트한다.
     *
     * @param userShop
     * @param locations
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateItemTraceLocation(UserShopInfoProtocol userShop, List<String> locations) throws MyServiceFailureException;

    /**
     * 원 단위의 가격 시스템을 사용할지 업데이트한다. 천원 단위 또는 원 단위를 사용할 수 있다.
     * raw price input mode가 true이면 원 단위의 가격 입력 모드를 사용한다.
     *
     * @param userShop
     * @param isRawPriceInputMode
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateRawPriceInputMode(UserShopInfoProtocol userShop, Boolean isRawPriceInputMode) throws MyServiceFailureException;

    /**
     * [SILVER-423] 소매 가격 기능을 사용할지 업데이트한다.
     *
     * @param userShop
     * @param isRetailPriceModeOn
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateRetailPriceMode(UserShopInfoProtocol userShop, Boolean isRetailPriceModeOn) throws MyServiceFailureException;

    /**
     * 매장 운영시간에 따른 날짜 이동을 위한 거래일시 자동이동 설정값을 업데이트한다.
     * 0으로 설정하면 해당 기능을 disable할 수 있다.
     *
     * @param userShop
     * @param timeShift
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateTimeShift(UserShopInfoProtocol userShop, int timeShift) throws MyServiceFailureException;

    /**
     * 카탈로그앱 권한을 설정한다. (마스터 권한이 필요하다.)
     *
     * @param userShop
     * @param silvermoonAuth
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateSilvermoonAuth(UserShopInfoProtocol userShop, ProjectSilvermoonAuthData silvermoonAuth) throws MyServiceFailureException;

    /**
     * 미송앱 권한을 설정한다. (마스터 권한이 필요하다.)
     *
     * @param userShop
     * @param dalaranAuth
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateDalaranAuth(UserShopInfoProtocol userShop, ProjectDalaranAuthData dalaranAuth) throws MyServiceFailureException;

    /**
     * 기본 마진 가산 비율을 설정한다. (마스터 권한이 필요하다.)
     * 0으로 설정하면 가산 비율 설정을 무효화한다.
     *
     * @param userShop
     * @param defaultMarginAdditionRateInPercent
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateDefaultMarginAdditionRate(UserShopInfoProtocol userShop, int defaultMarginAdditionRateInPercent) throws MyServiceFailureException;

    /**
     * 잔금 업데이트 방식을 업데이트한다.
     *
     * @param userShop
     * @param method
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateBalanceChangeMethod(UserShopInfoProtocol userShop, BalanceChangeMethodType method) throws MyServiceFailureException;
}
