package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2018-01-22.
 */

public class CheckableStringAdapter extends BaseDBAdapter<String> {

    public CheckableStringAdapter(Context context) {
        super(context);
    }

    @Override
    public void generate() {

    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new BaseViewHolder() {
            private TextView textView;
            private RadioButton radioButton;
            private LinearLayout container;

            @Override
            public int getThisView() {
                return R.layout.row_string_checkable;
            }

            @Override
            public void init(View view) {
                textView = (TextView) view.findViewById(R.id.row_string_checkable_textview);
                radioButton = (RadioButton) view.findViewById(R.id.row_string_checkable_checked);
                container = view.findViewById(R.id.row_string_checkable_container);
                MyView.setTextViewByDeviceSize(context, textView);
            }

            @Override
            public void update(int position) {
                String record= records.get(position);
                textView.setText(record);
                if (clickedPositions.contains(position)) {
                    radioButton.setChecked(true);
                    container.setBackgroundColor(ColorType.Beige.colorInt);
                } else {
                    radioButton.setChecked(false);
                    container.setBackgroundColor(ColorType.Trans.colorInt);
                }
            }
        };
    }
}