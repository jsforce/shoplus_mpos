package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.RadioButton;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;

import static com.teampls.shoplus.lib.enums.UserConfigurationType.NOT_DEFINED;

/**
 * Created by Medivh on 2017-05-26.
 */

abstract public class UserConfigTypeDialog extends BaseDialog {
    private RadioButton rbProvider, rbBuyer;

    public UserConfigTypeDialog(Context context) {
        super(context);
        setDialogWidth(0.9, 0.6);
        setCancelable(false);
        rbProvider = (RadioButton) findViewById(R.id.dialog_slipuser_type_publisher);
        rbBuyer = (RadioButton) findViewById(R.id.dialog_slipuser_type_receiver);
        setOnClick(R.id.dialog_user_type_apply);
        switch (userSettingData.getUserConfigType()) {
            case NOT_DEFINED:
                rbBuyer.setChecked(false);
                rbProvider.setChecked(false);
                break;
            case BUYER:
                rbBuyer.setChecked(true);
                break;
            case PROVIDER:
                rbProvider.setChecked(true);
                break;
        }
        show();

        MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_slipuser_type_title,
                R.id.dialog_slipuser_type_message,
                R.id.dialog_slipuser_type_publisher, R.id.dialog_slipuser_type_receiver,
                R.id.dialog_user_type_apply);
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_user_config_type;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_user_type_apply) {
            if (rbProvider.isChecked()) {
                userSettingData.updateUserConfig(UserConfigurationType.PROVIDER, new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        onApplyClick(UserConfigurationType.PROVIDER);
                    }
                });
                dismiss();

            } else if (rbBuyer.isChecked()) {
                // 근무 매장을 없앤다. 매장폰 상태로 돌려놓는다
                userSettingData.updateWorkingShopPhoneNumber("", new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        userSettingData.updateUserConfig(UserConfigurationType.BUYER, new MyOnTask() {
                            @Override
                            public void onTaskDone(Object result) {
                                onApplyClick(UserConfigurationType.BUYER);
                            }
                        });
                    }
                });
                dismiss();
            } else {
                MyUI.toastSHORT(context, String.format("선택 후 적용을 눌러주세요"));
                return;
            }
        }
    }

    abstract public void onApplyClick(UserConfigurationType userConfigurationType);

}