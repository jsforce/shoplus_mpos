package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.enums.MemberPermission;
import com.teampls.shoplus.lib.enums.UserLevelType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 카탈로그앱 관련 권한설정 데이터
 *
 * @author lucidite
 */

public class ProjectSilvermoonAuthData implements JSONDataWrapper {
    private UserLevelType stockValueUpdatePermission;
    private UserLevelType transactionDateSettingPermission;
    private UserLevelType cancelTransactionAfter1DayPermission;

    public ProjectSilvermoonAuthData(){}

    public ProjectSilvermoonAuthData(JSONObject dataObj) {
        if (dataObj == null) {
            this.stockValueUpdatePermission = MemberPermission.ChangeStockValue.getDefaultUserLevel();
            this.transactionDateSettingPermission = MemberPermission.SetTransactionDate.getDefaultUserLevel();
            this.cancelTransactionAfter1DayPermission = MemberPermission.CancelTransactionAfter1Day.getDefaultUserLevel();
        } else {
            String stockValueUpdateStr = dataObj.optString("update_stock", "");
            String transactionDateSettingStr = dataObj.optString("set_tr_date", "");
            String cancelTransactionAfter1DayStr = dataObj.optString("cancel_tr_1d", "");
            this.stockValueUpdatePermission = UserLevelType.exactTypeFromRemoteStr(stockValueUpdateStr);
            this.transactionDateSettingPermission = UserLevelType.exactTypeFromRemoteStr(transactionDateSettingStr);
            this.cancelTransactionAfter1DayPermission = UserLevelType.exactTypeFromRemoteStr(cancelTransactionAfter1DayStr);
            this.checkNullValue();
        }
    }

    public ProjectSilvermoonAuthData(UserLevelType stockValueUpdatePermission, UserLevelType transactionDateSettingPermission, UserLevelType cancelTransactionAfter1DayPermission) {
        this.stockValueUpdatePermission = stockValueUpdatePermission;
        this.transactionDateSettingPermission = transactionDateSettingPermission;
        this.cancelTransactionAfter1DayPermission = cancelTransactionAfter1DayPermission;
        this.checkNullValue();
    }

    private void checkNullValue() {
        if (this.stockValueUpdatePermission == null) {
            this.stockValueUpdatePermission = MemberPermission.ChangeStockValue.getDefaultUserLevel();
        }
        if (this.transactionDateSettingPermission == null) {
            this.transactionDateSettingPermission = MemberPermission.SetTransactionDate.getDefaultUserLevel();
        }
        if (this.cancelTransactionAfter1DayPermission == null) {
            this.cancelTransactionAfter1DayPermission = MemberPermission.CancelTransactionAfter1Day.getDefaultUserLevel();
        }
    }

    public UserLevelType getStockValueUpdatePermission() {
        return stockValueUpdatePermission;
    }

    public UserLevelType getTransactionDateSettingPermission() {
        return transactionDateSettingPermission;
    }

    public UserLevelType getCancelTransactionAfter1DayPermission() {
        return cancelTransactionAfter1DayPermission;
    }

    public void setStockValueUpdatePermission(UserLevelType stockValueUpdatePermission) {
        this.stockValueUpdatePermission = stockValueUpdatePermission;
        this.checkNullValue();
    }

    public void setTransactionDateSettingPermission(UserLevelType transactionDateSettingPermission) {
        this.transactionDateSettingPermission = transactionDateSettingPermission;
        this.checkNullValue();
    }

    public void setCancelTransactionAfter1DayPermission(UserLevelType cancelTransactionAfter1DayPermission) {
        this.cancelTransactionAfter1DayPermission = cancelTransactionAfter1DayPermission;
        this.checkNullValue();
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return new JSONObject()
                 .put("update_stock", this.stockValueUpdatePermission.toRemoteStr())
                 .put("set_tr_date", this.transactionDateSettingPermission.toRemoteStr())
                 .put("cancel_tr_1d", this.cancelTransactionAfter1DayPermission.toRemoteStr());
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("ProjectSilvermoonAuthData instance does not have a JSON array");
    }
}
