package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.datatypes.MyTriple;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2019-01-17.
 */

public class WorkingTimeDialog extends BaseDialog {
    private TimePicker dayPicker, nightPicker;
    private RadioButton rbWorkingByDay, rbWorkingByDayAndNight;
    private LinearLayout nightContainer;
    private TextView tvStartTitle;
    private MyOnTask onTask;
    private CheckBox cbColorEnabled;

    public WorkingTimeDialog(final Context context, MyOnTask onTask) {
        super(context);
        this.onTask = onTask;
        setDialogWidth(0.95, 0.8);

        tvStartTitle = findTextViewById(R.id.dialog_time_settings_day_title);
        rbWorkingByDay = findViewById(R.id.dialog_time_settings_byDay);
        rbWorkingByDayAndNight = findViewById(R.id.dialog_time_settings_byDayAndNight);
        nightContainer = findViewById(R.id.dialog_time_settings_night_container);
        dayPicker = findViewById(R.id.dialog_time_settings_day_picker);
        nightPicker = findViewById(R.id.dialog_time_settings_night_picker);
        cbColorEnabled = findViewById(R.id.dialog_time_settings_colorEnabled);

        MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_time_settings_title, R.id.dialog_time_settings_workingStyle,
                R.id.dialog_time_settings_night_title, R.id.dialog_time_settings_colorEnabled);

        setOnClick(R.id.dialog_time_settings_byDay, R.id.dialog_time_settings_byDayAndNight,
                R.id.dialog_time_settings_apply, R.id.dialog_time_settings_close);

        MyTriple<Double, Double, Boolean> timeSettings = BaseUtils.getTimeSetting(context);
        dayPicker.setCurrentHour(BaseUtils.toHourAndMin(timeSettings.first).first);
        dayPicker.setCurrentMinute(BaseUtils.toHourAndMin(timeSettings.first).second);
        nightPicker.setCurrentHour(BaseUtils.toHourAndMin(timeSettings.second).first);
        nightPicker.setCurrentMinute(BaseUtils.toHourAndMin(timeSettings.second).second);

        if (timeSettings.third == false) {
            rbWorkingByDay.setChecked(true);
            nightContainer.setVisibility(View.GONE);
            tvStartTitle.setText("시작");
        } else {
            rbWorkingByDayAndNight.setChecked(true);
            tvStartTitle.setText("주간");
            nightContainer.setVisibility(View.VISIBLE);
        }
        show();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_time_settings;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_time_settings_byDay) {
            nightContainer.setVisibility(View.GONE);
            tvStartTitle.setText("시작");
        } else if (view.getId() == R.id.dialog_time_settings_byDayAndNight) {
            tvStartTitle.setText("주간");
            nightContainer.setVisibility(View.VISIBLE);
        } else if (view.getId() == R.id.dialog_time_settings_apply) {
            onApplyClick();
        } else if (view.getId() == R.id.dialog_time_settings_close) {
            MyUI.toastSHORT(context, String.format("설정을 저장하지 않습니다"));
            dismiss();
        }
    }

    private void onApplyClick() {
        int dayHour = dayPicker.getCurrentHour();
        int dayMinute = dayPicker.getCurrentMinute();
        int nightHour = nightPicker.getCurrentHour(); // AM, PM 개념이 적용됨 ex) PM 08 ---> 20
        int nightMinute = nightPicker.getCurrentMinute();
        boolean isDayAndNight = rbWorkingByDayAndNight.isChecked();

        if (isDayAndNight) { // 주야구별
            if (BaseUtils.toDoubleHour(dayHour, dayMinute) > BaseUtils.toDoubleHour(nightHour, nightMinute)) {
                MyUI.toastSHORT(context, String.format("주간 시간 %d시 %d분은 야간 시간 %d시 %d분 보다 빨라야 합니다", dayHour, dayMinute, nightHour, nightMinute));
                return;
            }
        }

        GlobalDB.DayStartTime.put(context, BaseUtils.toDoubleHour(dayHour, dayMinute));
        if (isDayAndNight)
            GlobalDB.NightStartTime.put(context, BaseUtils.toDoubleHour(nightHour, nightMinute));
        GlobalDB.IsDayAndNightWorking.put(context, isDayAndNight);

        if (cbColorEnabled.isChecked())
            BasePreference.putShiftTimes(context, BaseUtils.getTimeSetting(context));

        MyUI.toastSHORT(context, String.format("시간 설정을 완료했습니다"));
        dismiss();
        if (onTask != null)
            onTask.onTaskDone("");
    }

}
