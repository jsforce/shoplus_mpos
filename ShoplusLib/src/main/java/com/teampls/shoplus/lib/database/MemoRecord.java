package com.teampls.shoplus.lib.database;

import android.text.TextUtils;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.protocol.TransactionDraftMemoDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-08-22.
 */

public class MemoRecord {
    public DateTime createdDateTime = Empty.dateTime;
    public String message = "";
    private static String lineDelimiter = ",", itemDelimiter = "~";

    public MemoRecord(TransactionDraftMemoDataProtocol protocol) {
        this.createdDateTime = protocol.getCreatedDateTime();
        this.message = protocol.getMessage();
    }

    public String toString() {
        return String.format("%s%s%s", createdDateTime.toString(),itemDelimiter,message);
    }

    public String toUiString() {
        return String.format("%s %s", createdDateTime.toString(BaseUtils.Hm_Format), message);
    }

    public MemoRecord(String memoLine) {
        String[] splits = memoLine.split(itemDelimiter);
        if (splits.length >= 1)
            this.createdDateTime = BaseUtils.toDateTime(splits[0]);
        if (splits.length >= 2)
            this.message = splits[1];
    }

    // 2017-01-01 12:30:59.214T+09:00~수정했어요,2017-01-02 04:24:12.456T+09:00~알았어요
    public static String toDbString(List<MemoRecord> memoRecords) {
        if (memoRecords.isEmpty())
            return "";
        List<String> strings = new ArrayList<>();
        for (MemoRecord memoRecord : memoRecords)
            strings.add(memoRecord.toString());
        return TextUtils.join(lineDelimiter, strings);
    }

    public static String toUiString(List<MemoRecord> memoRecords) {
        if (memoRecords.size() == 0)
            return "";
        List<String> strings = new ArrayList<>();
        for (MemoRecord memoRecord : memoRecords)
            strings.add(memoRecord.toUiString());
        return TextUtils.join("\n", strings);
    }

    public static List<MemoRecord> toList(String memoLines) {
        List<MemoRecord> results = new ArrayList<>();
        if (memoLines.isEmpty())
            return results;
        for (String memoLine : memoLines.split(lineDelimiter))
            results.add(new MemoRecord(memoLine));
        return results;
    }

}
