package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.squareup.timessquare.CalendarPickerView;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.view.MyNumberPlusMinus;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Medivh on 2017-04-07.
 */

abstract public class PickDateDialog extends BaseDialog implements CalendarPickerView.OnDateSelectedListener, CalendarPickerView.OnInvalidDateSelectedListener {
    private CalendarPickerView calendarView;
    private DateTime selectedDate = DateTime.now(), today = Empty.dateTime;
    private TextView tvSelectedDate;
    private MyNumberPlusMinus hourPlusMinus;
    private boolean doApplyHms = false;

    public PickDateDialog(Context context, String title,  boolean doApplyHms, int limitDays) {
        this(context, title, BaseUtils.createDay(), doApplyHms, limitDays);
    }

    public void highlightDays(List<DateTime> days) {
        if (calendarView != null) {
            List<Date> dates = new ArrayList<>();
            for (DateTime day : days)
                dates.add(day.toDate());
            calendarView.highlightDates(dates);
        }
    }

    public PickDateDialog(Context context, String title, DateTime selectedDate,  boolean doApplyHms, int limitDays) {
        super(context);
        findTextViewById(R.id.dialog_pick_date_title, title);
        this.doApplyHms = doApplyHms;
        today = BaseUtils.createDay();
        tvSelectedDate = findTextViewById(R.id.dialog_pick_date_selected, selectedDate.toString(BaseUtils.YMD_Kor_Format));
        calendarView = findViewById(R.id.dialog_pick_date_calendar);
        if (limitDays < 0)
            limitDays = 3650;

        if (BaseUtils.isBetween(selectedDate, today.minusDays(limitDays), today.plusYears(1)) == false)
            selectedDate = today;

        calendarView.init(today.minusDays(limitDays).toDate(), today.plusYears(1).toDate())
                .withSelectedDate(selectedDate.toDate())
                .inMode(CalendarPickerView.SelectionMode.SINGLE);
        calendarView.setOnDateSelectedListener(this);
        calendarView.setOnInvalidDateSelectedListener(this);

        hourPlusMinus = new MyNumberPlusMinus(getView(), 0);
        hourPlusMinus.setPlusMinusButtons(R.id.dialog_pick_date_plus, R.id.dialog_pick_date_minus);
        hourPlusMinus.setEditView(R.id.dialog_pick_date_hour);

        if (MyDevice.getAppVersionCode(context) >= 28) {
            int currentPosition = (int) new Duration(today.minusDays(limitDays), selectedDate).getStandardDays() / 30;
            calendarView.smoothScrollToPosition(currentPosition);
        }

        if (doApplyHms == false)
            setVisibility(View.GONE, R.id.dialog_pick_date_hour_container);

        setOnClick(R.id.dialog_pick_date_cancel, R.id.dialog_pick_date_apply);
        show();
    }

    @Override
    public void onDateSelected(Date date) {
        selectedDate = new DateTime(date);
        tvSelectedDate.setText(selectedDate.toString(BaseUtils.YMD_Kor_Format));
    }

    @Override
    public void onDateUnselected(Date date) {
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_pick_date;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_pick_date_cancel) {
            dismiss();

        } else if (view.getId() == R.id.dialog_pick_date_apply) {
            int hour =  hourPlusMinus.getValue();
            if (hour >= 24)
                hour = hour % 24;

            if (doApplyHms)
                onApplyClick(BaseUtils.createCustomDateTime(selectedDate, hour));
            else
                onApplyClick(BaseUtils.toDay(selectedDate));

            dismiss();
        }

    }


    abstract public void onApplyClick(DateTime finalDateTime);

    @Override
    public void onInvalidDateSelected(Date date) {
        MyUI.toastSHORT(context, "선택 가능할 날짜를 벗어났습니다.");
    }
}
