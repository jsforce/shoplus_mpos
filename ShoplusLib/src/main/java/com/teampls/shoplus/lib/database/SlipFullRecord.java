package com.teampls.shoplus.lib.database;

import android.content.Context;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database_global.SlipImagePathRecord;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.enums.SlipGenerationType;
import com.teampls.shoplus.lib.protocol.PaymentTermsProtocol;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.protocol.TransactionDraftDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-09-12.
 */
public class SlipFullRecord implements SlipDataProtocol {
    public SlipRecord record = new SlipRecord();
    public List<SlipItemRecord> items = new ArrayList<>();
    public List<SlipImagePathRecord> imagePaths = new ArrayList<>();
    public String serviceLink = "", orderId = "";

    public SlipFullRecord() {
    }

    public SlipFullRecord(SlipRecord record) {
        this.record = record;
    }

    public SlipFullRecord(SlipRecord record, List<SlipItemRecord> items) {
        this.record = record;
        this.items = items;
    }

    public SlipFullRecord(SlipDataProtocol protocol) {
        this.record = new SlipRecord(protocol);
        imagePaths.clear();
        for (String imagePath : protocol.getImagePaths())
            imagePaths.add(new SlipImagePathRecord(record.getSlipKey().toSID(), imagePath));
        items.clear();
        int serial = 0;
        for (SlipItemDataProtocol item : protocol.getItems()) {
            items.add(new SlipItemRecord(record.getSlipKey().toSID(), serial, record.salesDateTime, item));
            serial++;
        }
    }

    public SlipFullRecord(Context context, TransactionDraftDataProtocol protocol) {
        this.record = new SlipRecord(context, protocol);
        items.clear();
        int serial = 0;
        for (SlipItemDataProtocol item : protocol.getTransactionItems()) {
            items.add(new SlipItemRecord(record.getSlipKey().toSID(), serial, record.salesDateTime, item));
            serial++;
        }
    }

    @Override
    public SlipDataKey getKey() {
        return record.getSlipKey();
    }

    @Override
    public SlipGenerationType getSlipType() {
        return record.generation;
    }

    @Override
    public String getOwnerPhoneNumber() {
        return record.ownerPhoneNumber;
    }


    @Override
    public String getCounterpartPhoneNumber() {
        return record.counterpartPhoneNumber;
    }


    @Override
    public DateTime getCreatedDatetime() {
        return record.createdDateTime;
    }


    @Override
    public List<String> getImagePaths() {
        List<String> results = new ArrayList<>();
        for (SlipImagePathRecord record : imagePaths) {
            results.add(record.imagePath);
        }
        return results;
    }

    @Override
    public List<SlipItemDataProtocol> getItems() {
        List<SlipItemDataProtocol> results = new ArrayList<>();
        for (SlipItemRecord record : items)
            results.add(record);
        return results;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] Slip <%d> own %s, %s, %s, canceled? %s, completed? %s", location,
                this.record.id, this.record.ownerPhoneNumber, this.record.createdDateTime.toString(BaseUtils.fullFormat),
                this.record.counterpartPhoneNumber, this.record.cancelled, this.record.completed));
        for (SlipItemRecord itemRecord : this.items)
            itemRecord.toLogCat(location);
        for (SlipImagePathRecord imagePathRecord : this.imagePaths)
            imagePathRecord.toLogCat(location);

    }

    public SlipItemRecord getNewItemRecord() {
        return new SlipItemRecord(getKey().toSID(), items.size(), getSalesDate());
    }


    @Override
    public DateTime getSalesDate() {
        return record.salesDateTime;
    }

    @Override
    public PaymentTermsProtocol getPaymentTerms() {
        return new PaymentTermsProtocol() {
            @Override
            public int getOnlinePayment() {
                return record.onlinePayment;
            }

            @Override
            public int getEntrustedPayment() {
                return record.entrustPayment;
            }

            @Override
            public boolean isOnlinePaymentCleared() {
                return record.onlinePaid;
            }

            @Override
            public boolean isEntrustedPaymentCleared() {
                return record.entrustPaid;
            }

            @Override
            public String getOnlinePaymentClearedDate() {
                return record.onlinePaidDateStr;
            }

            @Override
            public String getEntrustedPaymentClearedDate() {
                return record.entrustPaidDateStr;
            }

            @Override
            public boolean isPaymentCleared() {
                return false;
            }
        };
    }

    @Override
    public Boolean isCompleted() {
        return record.completed;
    }

    @Override
    public Boolean isCancelled() {
        return record.cancelled;
    }

    @Override
    public Boolean isDeletionScheduled() {
        return false;
    }

    @Override
    public DateTime getCancelledDatetime() {
        return record.cancelledDateTime;
    }

    @Override
    public String getComposerPhoneNumber() {
        return null;
    }

    @Override
    public String getLinkedChatbotOrderId() {
        return orderId;
    }

    @Override
    public int getDemandForUnpayment() {
        return record.bill;
    }

    public static SlipFullRecord copy(SlipDataProtocol protocol ) {
        SlipFullRecord result = new SlipFullRecord();
        result.record = new SlipRecord(protocol.getOwnerPhoneNumber(), protocol.getCounterpartPhoneNumber()); // 시간은 새로 셋팅됨
        result.record.confirmed = false;
        int serial = 0;
        for (SlipItemDataProtocol item : protocol.getItems()) {  // 10개 필드
            SlipItemRecord newItemRecord = new SlipItemRecord(result.record); // slipKey, salesDateTime
            newItemRecord.itemId = item.getItemId();
            newItemRecord.name = item.getItemName();
            newItemRecord.unitPrice = item.getUnitPrice();
            newItemRecord.color = item.getColor();
            newItemRecord.customColorName = item.getCustomColorName();
            if (newItemRecord.customColorName == null)
                newItemRecord.customColorName = "";
            newItemRecord.size = item.getSize();
            newItemRecord.quantity = item.getQuantity();
            newItemRecord.slipItemType = item.getItemType();
            newItemRecord.serial = serial;
            result.items.add(newItemRecord);
            serial++;
        }
        return result;
    }

    public SlipFullRecord copy() {
        SlipFullRecord result = new SlipFullRecord();
        result.record = new SlipRecord(record.ownerPhoneNumber, record.counterpartPhoneNumber); // 시간은 새로 셋팅됨
        result.record.confirmed = false;
        for (SlipItemRecord item : items) {  // 10개 필드
            SlipItemRecord newItemRecord = new SlipItemRecord(result.record); // slipKey, salesDateTime
            newItemRecord.itemId = item.itemId;
            newItemRecord.name = item.name;
            newItemRecord.unitPrice = item.unitPrice;
            newItemRecord.color = item.color;
            newItemRecord.customColorName = item.getCustomColorName();
            if (newItemRecord.customColorName == null)
                newItemRecord.customColorName = "";
            newItemRecord.size = item.size;
            newItemRecord.quantity = item.quantity;
            newItemRecord.slipItemType = item.slipItemType;
            newItemRecord.serial = item.serial;
            result.items.add(newItemRecord);
        }
        return result;
    }

    public static List<SlipFullRecord> toList(List<SlipDataProtocol> protocols) {
        List<SlipFullRecord> results = new ArrayList<>();
        for (SlipDataProtocol protocol : protocols)
            results.add(new SlipFullRecord(protocol));
        return results;
    }

}
