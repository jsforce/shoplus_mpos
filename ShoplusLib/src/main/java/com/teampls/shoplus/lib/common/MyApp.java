package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.DrawableRes;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.enums.ColorType;

/**
 * Created by Medivh on 2017-09-26.
 */

public enum MyApp {
    None,
    Stormwind2("샵플 신상알림", "#FA5050", "#FB6A6A", "http://wp.me/P8qpsT-2r", R.drawable.icon_stormwind, 1185745763),
    Elicium("혜양 신상알림", "#16977a", "#37a78e", "", R.drawable.icon_elicium, 0),
    IronForge("샵플 재고추적", "#778DB0", "#8B9EBC", "http://wp.me/P8qpsT-2v", R.drawable.icon_ironforge,1214147189),
    Exodar("샵플 거래내역", "#0565AA", "#297CB7", "http://wp.me/P8qpsT-1P", R.drawable.icon_exodar,1204911522),
    Silvermoon("샵플 카탈로그", "#eba210", "#eeb034", "http://wp.me/P8qpsT-1h", R.drawable.icon_silvermoon, 1188238639),
    Dalaran("샵플 미송/잔금", "#2B7E51", "#4B916B", "http://wp.me/P8qpsT-1j", R.drawable.icon_dalaran, 1176882332),
    Sevenstars("샵플 시작하기", "#6B7979", "#818d8d", BaseGlobal.SHOPL_US, R.drawable.icon_sevenstars,1289162244),
    Shattrath("샵플 POS", "#ff9d34", "#ff9d34", "http://wp.me/P8qpsT-eY", R.drawable.icon_shattrath, 1265655323),
    Thunderbluff("샵플 신상DP", "#FA5050", "#FB6A6A", "https://wp.me/P8qpsT-vP", R.drawable.icon_thunderbluff, 1310432415),
    Darnassus("샵플 간편장부", "#3F518F", "#3F518F", "https://wp.me/P8qpsT-uZ", R.drawable.icon_darnassus, 1308419652),
    Warspear("샵플 mPOS", "#eba210", "#eeb034", "https://wp.me/P8qpsT-1h", R.drawable.icon_warspear, 1368311485),
    Orgrimmar2("샵플 자동상담", "#91735E", "#a38873", "https://wp.me/P8qpsT-1y", R.drawable.icon_orgrimmar, 1299076098), // 별도 페이지 아직 없음
    Undercity,  Twomoons, Stormshield;

    /*
    샵플시작(sevenstars): #AFB6BB - portrait
    카탈로그(silvermoon): #FD9A09 - portrait
    투데이(exodar): #226BAC - portrait
    미송잔금(dalaran): #284D32 - portrait
    POS(shattrath): #DD691B - landscape* (기존 아이콘이 있는 것도 괜찮을 것 같습니다)
    간편장부(darnassus): #3F518F - portrait
    재고추적(ironforge): #899BCD - portrait
    신상알림(stormwind): #B02620 - portrait
    신상DP(thunderbluff): #CD3E47
     */

    private String mainColorHex = "", subColorHex = "";
    private String shoplusPackage = "com.teampls.shoplus", webLink = BaseGlobal.SHOPL_US;
    public String name = "", mainViewPagerName = "", androidLink = "", appleLink = "";
    public int iconId = 0;

    MyApp() {
    }

    MyApp(String name, String mainColorHex, String subColorHex, String webLink, @DrawableRes int iconId, int apppleAppId) {
        this.name = name;
        this.mainColorHex = mainColorHex;
        this.subColorHex = subColorHex;
        this.webLink = webLink;
        this.appleLink = String.format("https://itunes.apple.com/kr/app/id%d", apppleAppId);
        this.iconId = iconId;
        this.mainViewPagerName = String.format("%s.%s.view.MainViewPager", shoplusPackage, this.toString().toLowerCase());
        this.androidLink = String.format("https://play.google.com/store/apps/details?id=%s&hl=ko", getPackageName());
    }

    public static MyApp get(String packageName) {
        for (MyApp app : values()) {
            if (packageName.contains(app.toString().toLowerCase())) {
                return app;
            }
        }
        Log.e("DEBUG_JS", String.format("[MyApp.get] %s not found", packageName));
        return None;
    }

    public static MyApp get(Context context) {
        return get(context.getApplicationInfo().packageName.toLowerCase());
    }

    public Pair<String, String> getThemeColors() {
        return new Pair(mainColorHex, subColorHex);
    }

    public static Pair<String, String> getThemeColors(Context context) {
        MyApp myApp = get(context);
        return myApp.getThemeColors();
    }

    public String getPackageName() {
        return shoplusPackage + "." + this.toString().toLowerCase();
    }

    public Context getContext(Context context) {
        try {
            Context appContext = context.createPackageContext(getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            return appContext;
        } catch (PackageManager.NameNotFoundException e) {
            return context;
        }
    }

    public static boolean has(Context context, MyApp myApp) {
        try {
            context.createPackageContext(myApp.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public boolean installed(Context context) {
        try {
            context.createPackageContext(getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void open(Context context) {
        MyDevice.openApp(context, getPackageName());
    }

    public void openActivity(Context context, String activityName) {
        MyDevice.openActivity(context, getPackageName(), activityName);
    }

    public void openGuide(Context context) {
        MyDevice.openWeb(context, webLink);
    }

    public String getKeyCurrentActivity() {
        return String.format("%s.CurrentActivity", this.toString());
    }

}
