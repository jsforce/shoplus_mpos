package com.teampls.shoplus.lib.database_memory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-02-03.
 */

public class SubShopRecord {
    public String phoneNumber = "", name = "";
    public List<ShopMemberRecord> members = new ArrayList<>();

    public SubShopRecord() {};
}
