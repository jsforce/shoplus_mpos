package com.teampls.shoplus.lib.common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.datatypes.ExternalAPIData;
import com.teampls.shoplus.lib.datatypes.MessengerLinkData;
import com.teampls.shoplus.lib.datatypes.MyShopInformationData;
import com.teampls.shoplus.lib.datatypes.ProjectDalaranAuthData;
import com.teampls.shoplus.lib.datatypes.ProjectSilvermoonAuthData;
import com.teampls.shoplus.lib.datatypes.ServiceActivationData;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.enums.BalanceChangeMethodType;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.FunctionBlockConfigurationType;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.enums.UserLevelType;
import com.teampls.shoplus.lib.protocol.FunctionBlockConfigurationDataProtocol;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.protocol.UserSpecificSettingProtocol;
import com.teampls.shoplus.lib.view_base.BaseFragment;

public class Empty {
	public static Bitmap bitmap = Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888);
	public static String string = "";
	public static int[] ints = new int[0];
	public static String[] strings = new String[0];
	public static int integer = 0; // problematic...
	public static DateTime dateTime = new DateTime(1900, 1, 1, 0, 0, 0);
	public static String phoneNumber = "00000000000";
	public static byte[] bytes = new byte[0];
	public static HashSet<String> sets = new HashSet<>(0);
    public static final String remoteImagePath = "no-image.jpg";
    public static ArrayList<String> stringList = new ArrayList<>();

    public static boolean isEmptyImage(String remotePath) {
        return remotePath.equals(remoteImagePath);
    }

	public static boolean isEmpty(Context context, TextView textview, String errorMessage) {
		if (textview.getText().toString().trim().isEmpty()) {
			MyUI.toast(context,  errorMessage);
			return true;
		} else {
			return false;
		}
	}
	
	public static String[] strings(int size) {
		return new String[size];
	}
	
	public static Calendar calendar() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(1900, 1 ,1);
		return calendar;
	}

	public static int[] ints(int size) {
		return new int[size];
	}
	
	public static boolean isEmpty(Bitmap bitmap) {
		if (bitmap == null)
			return true;
		
		if(android.os.Build.VERSION.SDK_INT >= 12) {
			return isEmptyNew(bitmap);
		} else {
			return (bitmap.getRowBytes() * bitmap.getHeight() <= 1);
		}
	}
	
	public static boolean isEmpty(TextView textView) {
		return Empty.isEmpty(textView.getText().toString().trim()); 
	}
	
	public static boolean isEmpty(int value) {
		return (value == Empty.integer);
	}
	
	@TargetApi(12)
	private static boolean isEmptyNew(Bitmap bitmap) {
		return (bitmap.getByteCount() == Empty.bitmap.getByteCount());
	}
	
	public static boolean isEmpty(DateTime dateTime) {
		return dateTime.isEqual(Empty.dateTime);
	}
	
	public static boolean isEmpty(String string) {
        if (string == null || string.length() == 0 || string.equals("null")) // 고민이 되는 코드
            return true;
        else
            return false;
	}
	
	public static boolean isEmpty(byte[] bytes) {
		if (bytes == null) return true;
		return (bytes.length <= 0);
	}

    public static class EmptyFragment extends BaseFragment {
        private int tabIndex = 0;
        private EmptyFragment instance;

        public EmptyFragment getInstance(int tabIndex) {
            if (instance == null)
                new EmptyFragment();
            this.tabIndex = tabIndex;
            return instance;
        }

        @Override
        public void refresh() {

        }

        @Override
        public String getTitle() {
            return "임시탭1";
        }

        @Override
        public int getTabIndex() {
            return tabIndex;
        }

        @Override
        public void onClick(View view) {

        }
    }

    public static class EmptyFragment2 extends BaseFragment {
        private int tabIndex = 0;
        private EmptyFragment instance;

        public EmptyFragment getInstance(int tabIndex) {
            if (instance == null)
                new EmptyFragment();
            this.tabIndex = tabIndex;
            return instance;
        }

        @Override
        public void refresh() {

        }

        @Override
        public String getTitle() {
            return "임시탭2";
        }

        @Override
        public int getTabIndex() {
            return tabIndex;
        }

        @Override
        public void onClick(View view) {

        }
    }

    public static BaseFragment getFragment() {
        return new EmptyFragment();
    }

    public static BaseFragment getFragment2() {
        return new EmptyFragment2();
    }

    public static BaseViewHolder getViewHolder() {
        return new BaseViewHolder() {
            @Override
            public int getThisView() {
                return 0;
            }

            @Override
            public void init(View view) {

            }

            @Override
            public void update(int position) {

            }
        };
    }

    public static Cursor getCursor() {
        return new Cursor() {

            @Override
            public int getCount() {
                return 0;
            }

            @Override
            public int getPosition() {
                return 0;
            }

            @Override
            public boolean move(int i) {
                return false;
            }

            @Override
            public boolean moveToPosition(int i) {
                return false;
            }

            @Override
            public boolean moveToFirst() {
                return false;
            }

            @Override
            public boolean moveToLast() {
                return false;
            }

            @Override
            public boolean moveToNext() {
                return false;
            }

            @Override
            public boolean moveToPrevious() {
                return false;
            }

            @Override
            public boolean isFirst() {
                return false;
            }

            @Override
            public boolean isLast() {
                return false;
            }

            @Override
            public boolean isBeforeFirst() {
                return false;
            }

            @Override
            public boolean isAfterLast() {
                return false;
            }

            @Override
            public int getColumnIndex(String s) {
                return 0;
            }

            @Override
            public int getColumnIndexOrThrow(String s) throws IllegalArgumentException {
                return 0;
            }

            @Override
            public String getColumnName(int i) {
                return null;
            }

            @Override
            public String[] getColumnNames() {
                return new String[0];
            }

            @Override
            public int getColumnCount() {
                return 0;
            }

            @Override
            public byte[] getBlob(int i) {
                return new byte[0];
            }

            @Override
            public String getString(int i) {
                return "";
            }

            @Override
            public void copyStringToBuffer(int i, CharArrayBuffer charArrayBuffer) {

            }

            @Override
            public short getShort(int i) {
                return 0;
            }

            @Override
            public int getInt(int i) {
                return 0;
            }

            @Override
            public long getLong(int i) {
                return 0;
            }

            @Override
            public float getFloat(int i) {
                return 0;
            }

            @Override
            public double getDouble(int i) {
                return 0;
            }

            @Override
            public int getType(int i) {
                return 0;
            }

            @Override
            public boolean isNull(int i) {
                return false;
            }

            @Override
            public void deactivate() {

            }

            @Override
            public boolean requery() {
                return false;
            }

            @Override
            public void close() {

            }

            @Override
            public boolean isClosed() {
                return false;
            }

            @Override
            public void registerContentObserver(ContentObserver contentObserver) {

            }

            @Override
            public void unregisterContentObserver(ContentObserver contentObserver) {

            }

            @Override
            public void registerDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public void setNotificationUri(ContentResolver contentResolver, Uri uri) {

            }

            @Override
            public Uri getNotificationUri() {
                return Uri.EMPTY;
            }

            @Override
            public boolean getWantsAllOnMoveCalls() {
                return false;
            }

            @Override
            public void setExtras(Bundle bundle) {

            }

            @Override
            public Bundle getExtras() {
                return new Bundle();
            }

            @Override
            public Bundle respond(Bundle bundle) {
                return new Bundle();
            }
        };
    }

    public static UserSettingDataProtocol userSettingData = new UserSettingDataProtocol() {
        @Override
        public UserLevelType getUserLevel() {
            return UserLevelType.EMPLOYEE;
        }

        @Override
        public UserConfigurationType getUserType() {
            return UserConfigurationType.NOT_DEFINED;
        }

        @Override
        public Map<ColorType, String> getColorNames() {
            return new HashMap<>();
        }

        @Override
        public List<String> getItemTraceLocations() {
            return new ArrayList<>();
        }

        @Override
        public Boolean isRawPriceInputMode() {
            return false;
        }

        @Override
        public Boolean isRetailOn() {
            return false;
        }

        @Override
        public Boolean showNegativeStockValue() {
            return false;
        }

        @Override
        public int getTimeShift() {
            return 0;
        }

        @Override
        public String getMainShop() {
            return "";
        }

        @Override
        public ProjectDalaranAuthData getDalaranAuthData() {
            return new ProjectDalaranAuthData();
        }

        @Override
        public ProjectSilvermoonAuthData getSilvermoonAuthData() {
            return new ProjectSilvermoonAuthData();
        }

        @Override
        public int getDefaultMarginAdditionRate() {
            return 0;
        }

        @Override
        public BalanceChangeMethodType getBalanceChangeMethod() {
            return BalanceChangeMethodType.ALL;
        }

        @Override
        public MyShopInformationData getShopInformation() {
            return new MyShopInformationData();
        }

        @Override
        public UserSpecificSettingProtocol getUserSpecificSetting() {
            return new UserSpecificSettingProtocol() {
                @Override
                public String getShop() {
                    return "";
                }

                @Override
                public void setShop(String shop) {

                }
            };
        }

        @Override
        public List<String> getAllowedShops() {
            return new ArrayList<>();
        }

        @Override
        public Map<ShoplusServiceType, ServiceActivationData> getServiceActivationData() {
            return new HashMap<>();
        }

        @Override
        public Set<MessengerLinkData> getMessengerLinkData() {
            return new HashSet<>();
        }

        @Override
        public Boolean doesShopHaveMasterUser() {
            return false;
        }

        @Override
        public Boolean hasDalaranOnlineBalanceChangePermission() {
            return false;
        }

        @Override
        public Boolean hasDalaranEntrustedBalanceChangePermission() {
            return false;
        }

        @Override
        public Boolean hasDalaranDepositChangePermission() {
            return false;
        }

        @Override
        public String getChatbotLinkedPlusFriend() {
            return "";
        }

        @Override
        public ExternalAPIData getExternalAPIData() {
            return new ExternalAPIData();
        }

        @Override
        public FunctionBlockConfigurationDataProtocol getFunctionBlockConfigurations() {
            return new FunctionBlockConfigurationDataProtocol() {
                @Override
                public boolean isBlocked(FunctionBlockConfigurationType function) {
                    return false;
                }
            };
        }
    };

}
