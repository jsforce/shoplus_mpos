package com.teampls.shoplus.lib.composition;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_global.AddressbookDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2019-01-24.
 */

public class UserComposition {
    private Context context;
    private AddressbookDB addressbookDB;
    private UserDB userDB;
    private UserSettingData userSettingData;

    public UserComposition(Context context) {
        this.context = context;
        addressbookDB = AddressbookDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
        userDB = UserDB.getInstance(context);
    }

    public void syncAddressBook(final MyOnTask onTask) {
        if (MyDevice.hasPermission(context, MyDevice.UserPermission.READ_CONTACTS) == false) {
            MyDevice.requestPermission(context, MyDevice.UserPermission.READ_CONTACTS);
            return;
        }

        MyUI.toast(context, String.format("개인 주소록은 화면에 보여주는 용도로만 사용합니다"));
        new MyDelayTask(context, 4000) {
            @Override
            public void onFinish() {
                MyUI.toast(context, String.format("개인 주소록은 직원간 공유되지 않습니다. 영수증이 발행된 거래처만 공유됩니다"));
            }
        };

        new MyAsyncTask(context, "MyAddressbookView", "주소록 로딩중...") {
            private int addressbookCount = 0;

            @Override
            public void onPreExecute() {
                addressbookCount = 0;
            }

            @Override
            public void doInBackground() {
                HashMap<String, String> addressBook = MyDevice.getMyAddressBook(context, new MyOnAsync<UserRecord>() {
                    @Override
                    public void onSuccess(UserRecord userRecord) {
                        userRecord.toLogCat("load addressbook");
                        addressbookDB.updateOrInsert(userRecord);
                        addressbookCount = addressbookCount + 1;
                        setProgressMessage(String.format("주소록 로딩중...%d", addressbookCount));
                    }

                    @Override
                    public void onError(Exception e) {
                    }
                });
                addressbookDB.deleteInvalid(new ArrayList(addressBook.keySet()));  // keySets = valid phone numbers
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone("");
            }
        };
    }

    public void downloadCounterparts(final MyOnTask<List<UserRecord>> onTask) { // refresh
        new CommonServiceTask(context, "MyUsersView", "주소록 확인중...") {
            List<UserRecord> results = new ArrayList<>();
            @Override
            public void doInBackground() throws MyServiceFailureException {
                results = new ArrayList<>();
                Pair<Long, List<ContactDataProtocol>> contactData = workingShopService.getContactUpdates(
                        userSettingData.getUserShopInfo(), userDB.getLatestTimeStamp()); // 전화-이름 정보
                userDB.update(contactData);
                for (ContactDataProtocol protocol : contactData.second)
                    results.add(new UserRecord(protocol));
            }

            @Override
            public void onPostExecutionUI() {
                MyUI.toastSHORT(context, String.format("최신 연락처입니다"));
                if (onTask != null)
                    onTask.onTaskDone(results);
            }
        };
    }

    public void updateUsers(String location, final List<UserRecord> userRecords, final MyOnTask onTask) {
        new CommonServiceTask(context, location, "유저 등록중...") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                for (UserRecord userRecord  : userRecords) {
                    Map userMap = new HashMap();
                    userMap.put(userRecord.phoneNumber, userRecord);
                    workingShopService.updateContactInfo(userSettingData.getUserShopInfo(), userMap);
                    userDB.updateOrInsert(userRecord);
                }
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone(true);
            }

        };
    }

    public void uploadUser(String location, final UserRecord userRecord, final MyOnTask onTask) {
        boolean hasUser = userDB.has(userRecord.phoneNumber);
        updateUsers(location, userRecord.toList(), onTask);
        if (hasUser)
            MyUI.toastSHORT(context, String.format("고객 정보를 수정했습니다"));
        else
            MyUI.toastSHORT(context, String.format("고객 정보를 추가했습니다"));
    }

    public List<UserRecord> getAllSortedRecords(boolean doIncludeAddressRecords) {
        List<UserRecord> results = userDB.getRecords();
        List<String> phoneNumbers = userDB.getStrings(UserDB.Column.phoneNumber);
        if (doIncludeAddressRecords) {
            for (UserRecord record : addressbookDB.getRecords()) {
                if (phoneNumbers.contains(record.phoneNumber) == false)
                    results.add(record);
            }
        }

        List<UserRecord> normalRecords = new ArrayList<>();
        List<UserRecord> starRecords = new ArrayList<>();
        for (UserRecord record : results) {
            if (record.name.startsWith("*")) {
                starRecords.add(record);
            } else {
                normalRecords.add(record);
            }
        }
        Collections.sort(normalRecords);
        normalRecords.addAll(starRecords);
        return normalRecords;
    }

    public void cleanUp() {
        addressbookDB.cleanUp();
        userDB.cleanUp();
    }

    public List<UserRecord> getRecords(List<String> phoneNumbers) {
        List<UserRecord> results = new ArrayList<>();
        for (String phoneNumber : phoneNumbers) {
            UserRecord record = getRecord(phoneNumber);
            if (record.isEmpty()) {
                Log.e("DEBUG_JS", String.format("[AllUserDB.getRecordsBy] not found: %s", phoneNumber));
                continue;
            }
            results.add(record);
        }
        return results;
    }

    public UserRecord getRecord(String phoneNumber) {
        UserRecord record = userDB.getRecord(phoneNumber);
        if (record.isEmpty())
            record = addressbookDB.getRecord(phoneNumber);
        return record;
    }

    // 중복시 userDB 정보를 우선적으로
    public List<UserRecord> getAllSortedRecords() {
        return getAllSortedRecords(true);
    }

    public List<UserRecord> getUserDBRecords() {
        return getAllSortedRecords(false);
    }

}
