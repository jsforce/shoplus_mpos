package com.teampls.shoplus.lib.template;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.MyQuery;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.database.MemoRecord;
import com.teampls.shoplus.lib.enums.DBResultType;

public class TemplateDB extends BaseDB<TemplateRecord> {

    private static TemplateDB instance = null;
    private static int Ver = 1;

    public enum Column {
        _id, draftId(MyQuery.INT), versionId(MyQuery.INT), createdDateTime, updatedDateTime, recipientPhoneNumber, memos;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }

        private String attribution = MyQuery.TEXT_TYPE;

        Column() {
        }

        Column(String attribution) {
            this.attribution = attribution;
        }

        public static String[] getAttributions() {
            String[] attributions = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++)
                attributions[i] = Column.values()[i].attribution;
            return attributions;
        }
    }

    private TemplateDB(Context context) {
        super(context, "TemplateDB", Ver, Column.toStrs());
        columnAttrs = Column.getAttributions();
        //  images = ImageDB.getInstance(context);
        //    items = SlipItemDB.getInstance(context)
    }

    public static TemplateDB getInstance(Context context) {
        if (instance == null)
            instance = new TemplateDB(context);
        return instance;
    }

    protected ContentValues toContentvalues(TemplateRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.draftId.name(), record.draftId);
        contentvalues.put(Column.versionId.name(), record.versionId);
        contentvalues.put(Column.createdDateTime.name(), record.createdDateTime.toString());
        contentvalues.put(Column.updatedDateTime.name(), record.updatedDateTime.toString());
        contentvalues.put(Column.recipientPhoneNumber.name(), record.recipientPhoneNumber);
        contentvalues.put(Column.memos.name(), MemoRecord.toDbString(record.memos));
        return contentvalues;
    }

    public TemplateRecord getRecordBy(long draftId) {
        return getRecord(Column.draftId, Long.toString(draftId));
    }


    public DBResult updateOrInsert(TemplateRecord record) {
        if (hasValue(Column.draftId, record.draftId)) {
            TemplateRecord dbRecord = getRecordBy(record.draftId); // key
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    @Override
    protected int getId(TemplateRecord record) {
        return record.id;
    }

    @Override
    protected TemplateRecord getEmptyRecord() {
        return new TemplateRecord();
    }

    @Override
    protected TemplateRecord createRecord(Cursor cursor) {
        return new TemplateRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getLong(Column.draftId.ordinal()),
                cursor.getLong(Column.versionId.ordinal()),
                BaseUtils.toDateTime(cursor.getString(Column.createdDateTime.ordinal())),
                BaseUtils.toDateTime(cursor.getString(Column.updatedDateTime.ordinal())),
                cursor.getString(Column.recipientPhoneNumber.ordinal()),
                MemoRecord.toList(cursor.getString(Column.memos.ordinal())));
    }

}