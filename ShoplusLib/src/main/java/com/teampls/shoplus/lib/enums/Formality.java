package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2017-05-08.
 */

public enum Formality {
    Formal, Informal
}
