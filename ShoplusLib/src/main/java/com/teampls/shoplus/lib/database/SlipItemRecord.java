package com.teampls.shoplus.lib.database;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_global.CustomerPriceDB;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.datatypes.ItemTraceRecord;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemTraceAttachErrorCode;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2016-09-01.
 */
public class SlipItemRecord implements SlipItemDataProtocol, Comparable<SlipItemRecord> {
    public int id = 0, quantity = 0, unitPrice = 0, serial = 0, itemId = 0;
    public String name = "", slipKey = "", updatedDateStr = "";
    public String customColorName = "";
    public SlipItemType slipItemType = SlipItemType.NONE;
    public DateTime salesDateTime = Empty.dateTime;
    public SizeType size = SizeType.Default;
    public ColorType color = ColorType.Default;
    public Set<String> traceIds = new HashSet<>();
    ////////
    public MSortingKey _sortingKey = MSortingKey.Date;
    public String _counterpartName = "";
    public int _priceDelta = 0;

    // itemId, name, slipKey, quantity, unitPrice, salesDateTime은 ItemRecord (SlipRecord) 정보
    // color, size : ItemOptionRecord 정보
    // quantity, serial, updateDateStr, slipItemType : 자체 정보

    public SlipItemRecord() {
    }

    public SlipItemRecord(String slipKey, int serial, DateTime salesDateTime) {
        this.slipKey = slipKey;
        this.serial = serial;
        this.salesDateTime = salesDateTime;
    }

    public SlipItemRecord(ColorType color, SizeType size, int quantity) {
        this.color = color;
        this.customColorName = color.getUiName();
        this.size = size;
        this.quantity = quantity;
    }


    public SlipItemRecord(String slipKey, int serial, DateTime salesDateTime, SlipItemDataProtocol protocol) {
        this.slipKey = slipKey;
        this.serial = serial;
        this.salesDateTime = salesDateTime;
        this.quantity = protocol.getQuantity();
        this.unitPrice = protocol.getUnitPrice();
        this.name = protocol.getItemName().trim();
        this.slipItemType = protocol.getItemType();
        this.updatedDateStr = protocol.getUpdatedDate();
        this.itemId = protocol.getItemId();
        this.color = protocol.getColor();
        this.size = protocol.getSize();
        this.customColorName = protocol.getCustomColorName(); // Nullable
        if (customColorName == null)
            customColorName = "";
    }

    public SlipItemRecord(int id, String slipKey, int serial, String name, int unitPrice,
                          int quantity, SlipItemType slipItemType, DateTime salesDateTime,
                          String updatedDateStr, ColorType color, SizeType size, int itemId,
                          String customColorName, Set<String> traceIds) {
        this.id = id;
        this.slipKey = slipKey;
        this.serial = serial;
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.slipItemType = slipItemType;
        this.salesDateTime = salesDateTime;
        this.updatedDateStr = updatedDateStr;
        this.color = color;
        this.size = size;
        this.itemId = itemId;
        this.customColorName = customColorName;
        this.traceIds = traceIds;
        if (customColorName == null)
            this.customColorName = "";
    }

    public SlipItemRecord(String slipKey, int serial, String name, int unitPrice,
                          int quantity, SlipItemType slipItemType, DateTime salesDateTime, int itemId) {
        this.slipKey = slipKey;
        this.serial = serial;
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.slipItemType = slipItemType;
        this.salesDateTime = salesDateTime;
        this.itemId = itemId;
    }


    // itemId, name, unitPrice : ItemRecord 정보
    //  slipKey, salesDateTime : SlipRecord 정보
    // color, size : ItemOptionRecord 정보
    // quantity, serial, updateDateStr, slipItemType : 자체 정보

    public SlipItemRecord(ItemRecord itemRecord, ItemOptionRecord optionRecord) {
        this.itemId = itemRecord.itemId;
        this.name = itemRecord.name;
        this.unitPrice = itemRecord.unitPrice;
        this.color = optionRecord.color;
        if (UserSettingData.hasCustomColor(optionRecord.color))
            this.customColorName = UserSettingData.getColorName(optionRecord.color);
        this.size = optionRecord.size;
    }

    public SlipItemRecord(ItemRecord itemRecord, ItemOptionRecord optionRecord, SlipRecord slipRecord) {
        this.itemId = itemRecord.itemId;
        this.name = itemRecord.name;
        this.unitPrice = itemRecord.unitPrice;
        this.color = optionRecord.color;
        if (UserSettingData.hasCustomColor(optionRecord.color))
            this.customColorName = color.getUiName();
        this.size = optionRecord.size;

        this.slipKey = slipRecord.getSlipKey().toSID();
        this.salesDateTime = slipRecord.salesDateTime;
        // slipType과 quantity 초기화에서 에러가 많이 난다. 그렇다고 여기에 자동 초기화를?
    }

    public SlipItemRecord(ItemRecord itemRecord, ItemTraceRecord traceRecord, SlipRecord slipRecord) {
        this.itemId = itemRecord.itemId;
        this.name = itemRecord.name;
        this.unitPrice = itemRecord.unitPrice;
        this.color = traceRecord.toOptionRecord().color;
        if (UserSettingData.hasCustomColor(color))
            this.customColorName = color.getUiName();
        this.size = traceRecord.toOptionRecord().size;
        attachItemTrace(traceRecord.traceId);
        this.slipKey = slipRecord.getSlipKey().toSID();
        this.salesDateTime = slipRecord.salesDateTime;
        // slipType과 quantity 초기화에서 에러가 많이 난다. 그렇다고 여기에 자동 초기화를?
    }

    public SlipItemRecord(SlipRecord slipRecord) {
        this.slipKey = slipRecord.getSlipKey().toSID();
        this.salesDateTime = slipRecord.salesDateTime;
    }

    public boolean isSame(SlipItemRecord record) {
        // integer (8)
        if (itemId != record.itemId) return false;
        if (serial != record.serial) return false;
        if (quantity != record.quantity) return false;
        if (unitPrice != record.unitPrice) return false;
        if (slipItemType != record.slipItemType) return false;
        if (salesDateTime.isEqual(record.salesDateTime) == false) return false;
        if (color != record.color) return false;
        if (size != record.size) return false;

        // string (3)
        if (slipKey.equals(record.slipKey) == false) return false;
        if (name.equals(record.name) == false) return false;
        if (updatedDateStr.equals(record.updatedDateStr) == false) return false;
        if (customColorName.equals(record.customColorName) == false) return false;
        if (TextUtils.join(",", traceIds).equals(TextUtils.join(",", record.traceIds)) == false)
            return false;

        return true;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof SlipItemRecord)) return false;
        SlipItemRecord record = (SlipItemRecord) object;
        if (itemId != record.itemId) return false;
        if (color != record.color) return false;
        if (size != record.size) return false;
        if (slipKey.equals(record.slipKey) == false) return false; // 새로 추가
        // 수동끼리는 모두 동일, 심지어 이름이랑 금액도 같을 수 있다, 순서외는 구별이 안된다.
        return true;
    }

    @Override
    public int hashCode() {
        int result = itemId;
        result = 31 * result + color.hashCode();
        result = 31 * result + size.hashCode();
        return result;
    }

    public void toLogCat(String location) {
        if (location.startsWith("e.")) {
            Log.e("DEBUG_JS", String.format("[%s] %d, <%d>, %s, key: %s, (%d), color %s (%s), %s, %d x %d, %s, %s, updated %s",
                    location, id, itemId, name, slipKey, serial, color.getOriginalUiName(), customColorName, size.uiName, quantity, unitPrice, slipItemType.toString(), salesDateTime.toString(BaseUtils.fullFormat), updatedDateStr));
        } else {
            Log.i("DEBUG_JS", String.format("[%s] %d, <%d>, %s, key: %s, (%d), color %s (%s), %s, %d x %d, %s, %s, updated %s",
                    location, id, itemId, name, slipKey, serial, color.getOriginalUiName(), customColorName, size.uiName, quantity, unitPrice, slipItemType.toString(), salesDateTime.toString(BaseUtils.fullFormat), updatedDateStr));
        }
    }

    public void toLogCatTraceIds(String location) {
        Log.i("DEBUG_JS", String.format("[%s] <%d>, %s, %s, %s, %s, %s, %s",
                location, itemId, name, slipKey, customColorName, size.uiName, slipItemType.toString(), traceIds));
    }

    public SlipItemRecord(String name, int unitPrice, int quantity, SlipItemType slipItemType) {
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.slipItemType = slipItemType;
        this.salesDateTime = DateTime.now();
        //Log.i("DEBUG_JS", String.format("[SlipItemRecord.SlipItemRecord] %s, %d, %d, %s", slipItemName, quantity, unitPrice, slipItemType.toSimpleString()));
    }

    @Override
    public int getItemId() {
        return itemId;
    }

    @Override
    public String getItemName() {
        return name;
    }

    @Override
    public ColorType getColor() {
        return color;
    }

    @Nullable
    @Override
    public String getCustomColorName() {
        return customColorName;
    }

    @Override
    public String getColorName() {
        if (TextUtils.isEmpty(customColorName))
            return color.getOriginalUiName();
        else
            return customColorName;
    }

    @Override
    public SizeType getSize() {
        return size;
    }

    @Override
    public int getUnitPrice() {
        return unitPrice;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public SlipItemType getItemType() {
        return slipItemType;
    }

    @Override
    public String getUpdatedDate() {
        return updatedDateStr;
    }


    @Override
    public Set<String> getItemTraces() {
        return traceIds; // 서버 전송용
    }

    @Override
    public Boolean isItemTraceAttached() {
        return traceIds.size() >= 1;
    }

    @Override
    public ItemTraceAttachErrorCode attachItemTrace(String itemTrace) {
        traceIds.add(itemTrace);
        return ItemTraceAttachErrorCode.OK;
    }

    @Nullable
    @Override
    public Integer getPriceDelta() {
        return _priceDelta;
    }

    @Override
    public void setPriceDelta(int itemReferencePrice) {
        this._priceDelta = itemReferencePrice; // 아직 DB에 저장하지 못한다.
    }

    public SlipItemDataProtocol asProtocol() {
        return this;
    }

    public static List<SlipItemDataProtocol> toProtocolList(List<SlipItemRecord> slipItems) {
        List<SlipItemDataProtocol> results = new ArrayList<>();
        for (SlipItemRecord slipItem : slipItems)
            results.add(slipItem.asProtocol());
        return results;
    }

    public String toNameAndOptionString(String delimiter) {
        List<String> results = new ArrayList<>();
        results.add(name.isEmpty() ? "미입력" : name);
        if (color != ColorType.Default)
            results.add(getColorName());
        if (size != SizeType.Default)
            results.add(size.uiName);
        return TextUtils.join(delimiter, results);
    }

    public String toNameOptionQuantityString(String delimiter) {
        List<String> results = new ArrayList<>();
        results.add(name.isEmpty() ? "미입력" : name);
        if (color != ColorType.Default)
            results.add(getColorName());
        if (size != SizeType.Default)
            results.add(size.uiName);
        return String.format("%s (%d개)", TextUtils.join(delimiter, results), quantity);
    }

    public String toOptionString(String delimiter) {
        List<String> results = new ArrayList<>();
        if (color != ColorType.Default)
            results.add(getColorName());
        if (size != SizeType.Default)
            results.add(size.uiName);
        return TextUtils.join(delimiter, results);
    }

    public ItemOptionRecord toOption() {
        return new ItemOptionRecord(itemId, color, size);
    }

    public String toOptionString(String delimiter, boolean doHideDefault) {
        List<String> results = new ArrayList<>();
        if ((color == ColorType.Default) && doHideDefault) {
            // skip
        } else {
            results.add(getColorName());
        }
        if ((size == SizeType.Default && doHideDefault)) {
            // skip
        } else {
            results.add(size.uiName);
        }
        return TextUtils.join(delimiter, results);
    }

    public SlipItemRecord clone() {
        SlipItemRecord result = new SlipItemRecord();
        result.id = id;
        result.quantity = quantity;
        result.unitPrice = unitPrice;
        result.serial = serial;
        result.itemId = itemId;
        result.name = name;
        result.slipKey = slipKey;
        result.updatedDateStr = updatedDateStr;
        result.slipItemType = slipItemType;
        result.salesDateTime = salesDateTime;
        result.size = size;
        result.color = color;
        result.customColorName = customColorName;
        return result;
    }

    public String getPosName(Context context) {
        if (name.isEmpty()) {
            ItemRecord itemRecord = ItemDB.getInstance(context).getRecordBy(itemId);
            if (itemRecord.itemId == 0)
                return "(미입력)";
            if (itemRecord.category == ItemCategoryType.NONE) {
                return String.format("NO_%s", itemRecord.serialNum);
            } else {
                return String.format("%s_%s", itemRecord.category.toString(), itemRecord.serialNum);
            }
        } else {
            return name;
        }
    }

    @Override
    public int compareTo(@NonNull SlipItemRecord slipItemRecord) {
        switch (_sortingKey) {
            default:
                if (salesDateTime.isEqual(slipItemRecord.salesDateTime)) return 0;
                return salesDateTime.isAfter(slipItemRecord.salesDateTime) ? -1 : 1; // 시간이 늦을 수록 앞쪽으로
            case Name:
                return (name.compareTo(slipItemRecord.name));
            case Type:
                if (slipItemType.ordinal() == slipItemRecord.slipItemType.ordinal())
                    return 0; // 등호부분이 빠지면 violation 에러가 발생한다
                return slipItemType.ordinal() < slipItemRecord.slipItemType.ordinal() ? -1 : 1;
            case CounterpartName:
                return _counterpartName.compareTo(slipItemRecord._counterpartName);
            case MultiField: // item-color-size
                if (itemId != slipItemRecord.itemId) {
                    return -((Integer) itemId).compareTo(slipItemRecord.itemId);
                } else if (color.compareTo(slipItemRecord.color) != 0) {
                    return color.compareTo(slipItemRecord.color);
                } else {
                    return size.compareTo(slipItemRecord.size);
                }
        }
    }

    public static List<SlipItemRecord> toList(SlipDataKey slipDataKey, List<SlipItemDataProtocol> protocols) {
        List<SlipItemRecord> results = new ArrayList<>();
        int serial = 0;
        for (SlipItemDataProtocol protocol : protocols) {
            results.add(new SlipItemRecord(slipDataKey.toSID(), serial, slipDataKey.createdDateTime, protocol));
            serial++;
        }
        return results;
    }

    public List<SlipItemRecord> toList() {
        List<SlipItemRecord> results = new ArrayList<>();
        results.add(this);
        return results;
    }

    public SlipDataKey getSlipKey() {
        return new SlipDataKey(slipKey);
    }
    //    public SlipItemRecord(String slipKey, int serial, DateTime salesDateTime, SlipItemDataProtocol protocol) {


    public ItemStockKey getStockey() {
        return new ItemStockKey(itemId, color, size);
    }

    public String getQuantityStr(Context context) {
        if (GlobalDB.getInstance(context).getBool(GlobalDB.KEY_RECEIPT_UseHangulQuantity)) {
            return String.format("%d개", quantity);
        } else {
            return String.format("x %d", quantity);
        }
    }

    // for문을 돌때 문제가 되는데.... fileDB 를 쓰고 있어서
    @Deprecated
    private int getUnitPriceByPreference(Context context, String counterpart, ItemRecord itemRecord, boolean doShowToast) {
        int result = unitPrice;
        if (itemId < 0)
            return result;

        UserSettingData userSettingData = UserSettingData.getInstance(context);
        if (userSettingData.getProtocol().isRetailOn() && UserDB.getInstance(context).getRecord(counterpart).priceType == ContactBuyerType.RETAIL) {
            result = itemRecord.retailPrice == 0 ? unitPrice : itemRecord.retailPrice;
            if (doShowToast)
                MyUI.toastSHORT(context, String.format("소매가 %d 적용", BaseUtils.toCurrencyStr(result)));
        } else {
            // 도매손님
            if (BasePreference.priceByBuyer.getValue(context)) {
                result = CustomerPriceDB.getInstance(context).getPrice(userSettingData.getUserShopInfo(), counterpart, itemId, unitPrice);
                if (doShowToast && itemRecord.retailPrice > 0)
                    MyUI.toastSHORT(context, String.format("고객가 %d 적용", BaseUtils.toCurrencyStr(result)));
            }
        }
        //Log.i("DEBUG_JS", String.format("[SlipItemRecord.getUnitPriceByPreference] retailOn? %s, contactRetail? %s, customerPrice? %s, price? %d",userSettingData.getProtocol().isRetailOn(), contactRecord.priceType == ContactBuyerType.RETAIL, BasePreference.byCustomerPriceEnabled.valueOfDefault(context), result));
        return result;
    }
}
