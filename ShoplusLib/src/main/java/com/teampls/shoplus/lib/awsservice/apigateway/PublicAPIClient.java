package com.teampls.shoplus.lib.awsservice.apigateway;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class PublicAPIClient {

    public static String request(String resourceURL, String apiKey) throws MyServiceFailureException {
        try {
            URL url = new URL(resourceURL);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.addRequestProperty("x-api-key", apiKey);

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = conn.getInputStream();
                return APIGatewayHelper.getStringFromInputStream(inputStream);
            } else {
                throw new MyServiceFailureException("HttpError[" + responseCode + "]", "버전 정보를 확인할 수 없습니다.");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }
}
