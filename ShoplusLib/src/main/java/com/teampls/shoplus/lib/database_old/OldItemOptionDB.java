package com.teampls.shoplus.lib.database_old;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.QueryAndRecord;
import com.teampls.shoplus.lib.database.QueryOrderRecord;
import com.teampls.shoplus.lib.datatypes.ItemStockKey;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.DBResultType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.undercity.enums.ItemStockUpdateType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class OldItemOptionDB extends BaseDB<OldItemOptionRecord> {

    private static OldItemOptionDB instance = null;
    protected static int Ver = 4;

    // 1 :  _id, key, itemId, color, size
    // 2 : _id, key, itemId, color, size, confirmed

    public enum Column {
        _id, key, itemId, color, size, sizeOriginal, confirmed, stock, revStock;
        // sizeOriginal -- xs, s, m, l, xL 등의 순서를 보여줄때 유지시키기 위해 (오타네, Ordinal이라고 했어야 했는데)

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    protected OldItemOptionDB(Context context) {
        super(context, "ItemOptionDB", Ver, Column.toStrs());
    }

    protected OldItemOptionDB(Context context, String dbName) {
        super(context, dbName, Ver, Column.toStrs());
    }

    public static OldItemOptionDB getInstance(Context context) {
        if (instance == null)
            instance = new OldItemOptionDB(context);
        return instance;
    }

    protected ContentValues toContentvalues(OldItemOptionRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.key.toString(), record.key);
        contentvalues.put(Column.itemId.toString(), record.itemId);
        contentvalues.put(Column.color.toString(), record.color.toString());
        contentvalues.put(Column.size.toString(), record.size.toString());
        contentvalues.put(Column.sizeOriginal.toString(), record.size.ordinal());
        contentvalues.put(Column.confirmed.toString(), record.confirmed);
        contentvalues.put(Column.stock.toString(), record.stock);
        contentvalues.put(Column.revStock.toString(), record.revStock);
        return contentvalues;
    }

    @Override
    protected void toLogCat(int id, OldItemOptionRecord record, int option, String operation) {
        Log.i("DEBUG_JS", String.format("[%s] %d, %s, %s", getClass().getSimpleName(),
                record.itemId, record.color.toString(), record.size.toString()));
    }

    private boolean has(OldItemOptionRecord targetRecord, List<OldItemOptionRecord> records) {
        if (records == null)
            records = getRecordsBy(targetRecord.itemId);

        for (OldItemOptionRecord record : records) {
            if ((targetRecord.color == record.color) && (targetRecord.size == record.size)) {
                return true;
            }
        }
        return false;
    }

    public OldItemOptionRecord getRecordBy(String key) {
        return getRecord(Column.key, key);
    }

    public void deleteInvalids(List<String> validKeys) {
        List<String> invalidKeys = getStrings(Column.key);
        invalidKeys.removeAll(validKeys);
        deleteAll(Column.key, invalidKeys);
    }

    public void deleteInvalids(int itemId, List<String> validKeys) {
        List<String> invalidKeys = getStrings(Column.key, Column.itemId, itemId);
        invalidKeys.removeAll(validKeys);
        deleteAll(Column.key, invalidKeys);
    }

    public void refresh(Map<ItemStockKey, Integer> stockBundle) {
        refresh(stockBundle, null);
    }

    public void updateOptionStocks(ItemStockUpdateType stockUpdateType, Map<ItemStockKey, Integer> stockKeyMap) {
        for (ItemStockKey key : stockKeyMap.keySet()) {
            int currentStock = getRecord(key).stock;
            int updateStock = stockKeyMap.get(key);
            int stock = 0;
            switch (stockUpdateType) {
                case ORDER:
                    stock = currentStock; // 그대로
                    break;
                case ARRIVAL:
                    stock = currentStock + updateStock; // 증가
                    break;
                case BATCH:
                    stock = updateStock; // 강제 변경
                    break;
                case SALES:
                    stock = currentStock - updateStock; // 감소
                    break;
            }
            OldItemOptionRecord record = new OldItemOptionRecord(key, stock);
            updateOrInsert(record);
        }
    }

    public void refresh(Map<ItemStockKey, Integer> stockBundle, MyOnTask onTask) {
       // beginTransaction();
        final List<String> validKeys = new ArrayList<>();
        for (ItemStockKey key : stockBundle.keySet()) {
            OldItemOptionRecord record = new OldItemOptionRecord(key, stockBundle.get(key));
            validKeys.add(record.key);
            updateOrInsert(record);
            if (onTask != null)
                onTask.onTaskDone("");
        }
      //  endTransaction();
        new MyAsyncTask(context) {
            @Override
            public void doInBackground() {
                deleteInvalids(validKeys);
            }
        };
    }

    public void refreshFast(Map<ItemStockKey, Integer> stockBundle, boolean doDeleteInvalids, MyOnTask onTask) {
        final List<String> validKeys = new ArrayList<>();
        MyListMap<Integer, OldItemOptionRecord> itemOptionMap = new MyListMap<>();
        for (ItemStockKey key : stockBundle.keySet()) {
            OldItemOptionRecord record = new OldItemOptionRecord(key, stockBundle.get(key));
            itemOptionMap.add(record.itemId, record);
            validKeys.add(record.key);
        }

        for (Integer itemId : itemOptionMap.keySet()) {
            updateOrInsertFast(itemId, itemOptionMap.get(itemId));
            if (onTask != null)
                onTask.onTaskDone("");
        }

        if (doDeleteInvalids)
            deleteInvalids(validKeys);
    }

    public void updateOrInsertFast(int itemId, List<OldItemOptionRecord> records) {
        // 동일 itemId에 대해서만 검사
        // 그룹 vs 그룹 업데이트 방식 : records vs dbRecords

        List<OldItemOptionRecord> dbRecords = getRecordsBy(itemId);
        for (OldItemOptionRecord record : records) {
            // 기존 db에 있는지 검사
            boolean found = false;
            for (OldItemOptionRecord dbRecord : dbRecords) {
                // has() 대신
                if ((record.color == dbRecord.color) && (record.size == dbRecord.size)) { // itemId는 이미 동일하므로
                    found = true;
                    if (dbRecord.isSame(record) == false) {
                        update(dbRecord.id, record);
                    } else {
                        //
                    }
                }
            }
            if (found == false) {
                insert(record);
            }
        }
    }

    public DBResult updateOrInsert(OldItemOptionRecord record) {
        if (hasValue(Column.key, record.key)) {// item, color, size
            OldItemOptionRecord dbRecord = getRecordBy(record.key);
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public void update(Map<ItemStockKey, Integer> stockBundle) {
        for (ItemStockKey key : stockBundle.keySet()) {
            OldItemOptionRecord record = new OldItemOptionRecord(key, stockBundle.get(key));
            updateOrInsert(record);
        }
    }

    public List<OldItemOptionRecord> recoverDeleted(MyListMap<Integer, OldItemOptionRecord> options) {
        List<OldItemOptionRecord> results = new ArrayList<>();
        for (int itemId : options.keySet()) {
            List<OldItemOptionRecord> myOptions = getRecordsBy(itemId);
            for (OldItemOptionRecord option : options.get(itemId)) {
                if (myOptions.contains(option) == false) {
                    Log.w("DEBUG_JS", String.format("[ItemOptionDB.recoverDeleted] Not contains %s", option.toString(",")));
                    insert(option); // 삭제된 것이라는 표시가 필요한데 우선은 그냥 가자
                    results.add(option);
                }
            }
        }
        return results;
    }

    // 재고가 있는 것만 가져옴
    public void updateByPositiveStocks(Map<ItemStockKey, Integer> stockBundle) {
        for (OldItemOptionRecord record : getRecords()) {
            if (stockBundle.keySet().contains(record.toStockKey())) {
                record.stock = stockBundle.get(record.toStockKey()); // 있는 값은 그대로
            } else {
                record.stock = Math.min(0, record.stock); // 없는건 0 또는 기존 음수
            }
            update(record.id, record);
        }
    }

    public void updateFast(Map<ItemStockKey, Integer> stockBundle,MyOnTask onTask) {

        MyListMap<Integer, OldItemOptionRecord> itemOptionMap = new MyListMap<>();
        for (ItemStockKey key : stockBundle.keySet()) {
            OldItemOptionRecord record = new OldItemOptionRecord(key, stockBundle.get(key));
            List<OldItemOptionRecord> options = itemOptionMap.getList(record.itemId);
            options.add(record);
            itemOptionMap.put(record.itemId, options);
        }

        for (Integer itemId : itemOptionMap.keySet()) {
            updateOrInsertFast(itemId, itemOptionMap.getList(itemId));
            if (onTask != null)
                onTask.onTaskDone("");
        }
    }

    public boolean refresh(int itemId, ArrayList<OldItemOptionRecord> newRecords) {
       List<OldItemOptionRecord> oldRecords = getRecordsBy(itemId);

        if (newRecords.size() == 0)
            newRecords.add(new OldItemOptionRecord(itemId, ColorType.Default, SizeType.Default));

        for (OldItemOptionRecord record : newRecords) {
            if (has(record, oldRecords) == false)
                insert(record);
        }

        for (OldItemOptionRecord oldRecord : oldRecords) {
            if (has(oldRecord, newRecords) == false)
                delete(oldRecord.id);
        }
        return true;
    }

//    public void updateOrInsert(int itemId, ArrayList<OldItemOptionRecord> records) {
//        if (records.size() == 0)
//            records.addInteger(new OldItemOptionRecord(itemId, ColorType.Default, SizeType.Default));
//        ArrayList<OldItemOptionRecord> dbRecords = getRecordsBy(itemId);
//
//        for (OldItemOptionRecord record : records) {
//            if (dbRecords.contains(record)) {
//
//            } else {
//                insert(record);
//            }
//
//            if (has(record, dbRecords)) {
//                // dbRecord.isSame(record) == true (always)
//            } else {
//                insert(record);
//            }
//        }
//
//        for (OldItemOptionRecord dbRecord : dbRecords) {
//            if (has(dbRecord, records) == false)
//                delete(dbRecord.id);
//        }
//    }

    public void deleteBy(int itemId) {
        List<Integer> itemIds = new ArrayList<>();
        itemIds.add(itemId);
        deleteAll(Column.itemId, itemIds);
    }

    @Override
    protected int getId(OldItemOptionRecord record) {
        return record.id;
    }

    @Override
    protected OldItemOptionRecord getEmptyRecord() {
        return new OldItemOptionRecord();
    }

    @Override
    protected OldItemOptionRecord createRecord(Cursor cursor) {
        return new OldItemOptionRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.key.ordinal()),
                cursor.getInt(Column.itemId.ordinal()),
                ColorType.valueOfDefault(cursor.getString(Column.color.ordinal())),
                SizeType.valueOfDefault(cursor.getString(Column.size.ordinal())),
                BaseUtils.toBoolean(cursor.getInt(Column.confirmed.ordinal())),
                cursor.getInt(Column.stock.ordinal()),
                cursor.getInt(Column.revStock.ordinal())
        );
    }

    public List<OldItemOptionRecord> getRecordsBy(int itemId) {
        return getRecords(Column.itemId, itemId);
    }

    public void toLogCat(String location) {
        boolean isEmpty = true;
        for (OldItemOptionRecord record : getRecords()) {
            record.toLogCat(location);
            isEmpty = false;
        }
        if (isEmpty)
            Log.i("DEBUG_JS", String.format("[%s] %s is empty", location, DB_NAME));
    }

    public List<OldItemOptionRecord> getOnRevisionRecords(List<Integer> itemIds) {
        List<OldItemOptionRecord> results = new ArrayList<>(0);
        for (Integer itemId : itemIds)
            results.addAll(getOnRevisionRecords(itemId));
        return results;
    }

    public List<OldItemOptionRecord> getOnRevisionRecords(int itemId) {
        List<OldItemOptionRecord> results = new ArrayList<>(0);
        for (OldItemOptionRecord record : getRecordsBy(itemId))
            if (record.isOnRevision())
                results.add(record);
        return results;
    }

    public List<OldItemOptionRecord> getOnRevisionRecords() {
        List<OldItemOptionRecord> results = new ArrayList<>(0);
        for (OldItemOptionRecord record : getRecords())
            if (record.isOnRevision())
                results.add(record);
        return results;
    }

    public Map<ItemStockKey, Integer> getOnRevisionBundle(int itemId) {
        Map<ItemStockKey, Integer> results = new HashMap<>(0);
        for (OldItemOptionRecord record : getOnRevisionRecords(itemId))
            results.put(record.toStockKey(), record.revStock);
        return results;
    }

    public Map<ItemStockKey, Integer> getOnRevisionBundle(List<Integer> itemIds) {
        Map<ItemStockKey, Integer> results = new HashMap<>(0);
        for (Integer itemId : itemIds)
            results.putAll(getOnRevisionBundle(itemId));
        return results;
    }

    public Map<ItemStockKey, Integer> getOnRevisionBundle() {
        Map<ItemStockKey, Integer> results = new HashMap<>(0);
        for (OldItemOptionRecord record : getOnRevisionRecords())
            results.put(record.toStockKey(), record.revStock);
        return results;
    }

    public void confirmRevisions(List<Integer> itemIds) {
        for (Integer itemId : itemIds)
            confirmRevision(itemId);
    }

    public void confirmRevision(int itemId) {
        for (OldItemOptionRecord record : getOnRevisionRecords(itemId)) {
            record.confirmed = true;
            record.stock = record.revStock;
            update(record.id, record);
        }
    }

    public void confirmRevisions() {
        for (OldItemOptionRecord record : getOnRevisionRecords()) {
            record.confirmed = true;
            record.stock = record.revStock;
            update(record.id, record);
        }
    }

    public boolean hasOnRevision() {
        for (OldItemOptionRecord record : getRecords())
            if (record.isOnRevision()) return true;
        return false;
    }

    public boolean hasOnRevision(int itemId) {
        for (OldItemOptionRecord record : getRecordsBy(itemId))
            if (record.isOnRevision()) return true;
        return false;
    }

    public boolean hasOnRevision(List<Integer> itemIds) {
        for (int itemId : itemIds) {
            if (hasOnRevision(itemId))
                return true;
        }
        return false;
    }


    public void clearOnRevisions() {
        for (OldItemOptionRecord record : getOnRevisionRecords()) {
            if (record.confirmed == false) {
                delete(record.id);
            } else if (record.stock != record.revStock) {
                record.revStock = record.stock;
                update(record.id, record);
            }
        }
    }

    public void clearOnRevisions(int itemId) {
        for (OldItemOptionRecord record : getOnRevisionRecords(itemId)) {
            if (record.confirmed == false) {
                delete(record.id);
            } else if (record.stock != record.revStock) {
                record.revStock = record.stock;
                update(record.id, record);
            }
        }
    }

    public void clearOnRevisions(List<Integer> itemIds) {
        for (int itemId : itemIds)
            clearOnRevisions(itemId);
    }

    public int getStockSum(int itemId) {
        int result = 0;
        for (OldItemOptionRecord record : getRecordsBy(itemId))
            result = result + record.getPositiveStock();
        return result;
    }

    public int getSums(List<Integer> itemIds) {
        int result = 0;
        for (OldItemOptionRecord record : getRecordsIN(ItemDB.Column.itemId, itemIds, null))
            result = result +  record.getPositiveStock();
        return result;
    }

    public MyMap<Integer, Integer> getStockMap() {
        MyMap<Integer, Integer> results = new MyMap<>(0);
        for (OldItemOptionRecord optionRecord : getRecords())
            results.addInteger(optionRecord.itemId, optionRecord.getPositiveStock());
        return results;
    }

    public List<OldItemOptionRecord> getSortedRecords(int itemId) {
        List<QueryOrderRecord> queryOrderRecords = new ArrayList<>();
        queryOrderRecords.add(new QueryOrderRecord(Column.itemId, true, true));
        queryOrderRecords.add(new QueryOrderRecord(Column.color, false, false));
        queryOrderRecords.add(new QueryOrderRecord(Column.sizeOriginal, true, false));
        //return getRecordsINInts(Column.itemId, new ArrayList<Integer>(itemId), queryOrderRecords);
        List<QueryAndRecord> queryAndRecords = new ArrayList<>();
        queryAndRecords.add(new QueryAndRecord(Column.itemId, itemId));
        return getRecords(queryAndRecords, queryOrderRecords);
    }

    public OldItemOptionRecord getRecord(ItemStockKey itemStockKey) {
        List<QueryAndRecord> queryAndRecords = new ArrayList<>();
        queryAndRecords.add(new QueryAndRecord(Column.itemId, itemStockKey.getItemId()));
        queryAndRecords.add(new QueryAndRecord(Column.color, itemStockKey.getColorOption().toString()));
        queryAndRecords.add(new QueryAndRecord(Column.size, itemStockKey.getSizeOption().toString()));
        List<OldItemOptionRecord> results = getRecords(queryAndRecords);
        if (results.size() == 1) {
            return results.get(0);
        } else {
            if (results.size() >= 2) {
                Log.w("DEBUG_JS", String.format("[ItemOptionDB.getRecordByKey] duplicated %d, %s, %s : %s", itemStockKey.getItemId(), itemStockKey.getColorOption().toString(),
                        itemStockKey.getSizeOption().toString(), TextUtils.join(",", results)));
                return results.get(0);
            } else {
                return new OldItemOptionRecord();
            }
        }
    }

    public Map<ColorType, List<Pair<SizeType, Integer>>> toColorMap(List<OldItemOptionRecord> optionRecords) {
        Map<ColorType, List<Pair<SizeType, Integer>>> results = new HashMap<>();
        for (OldItemOptionRecord optionRecord : optionRecords) {
            if (results.containsKey(optionRecord.color) == false) {
                // new
                Pair<SizeType, Integer> pair = Pair.create(optionRecord.size, optionRecord.stock);
                List<Pair<SizeType, Integer>> list = new ArrayList<>();
                list.add(pair);
                results.put(optionRecord.color, list);
            } else {
                // exist...
                List<Pair<SizeType, Integer>> sizeTypes = results.get(optionRecord.color);
                sizeTypes.add(Pair.create(optionRecord.size, optionRecord.stock));
                results.put(optionRecord.color, sizeTypes);
            }
        }
        return results;
    }

}