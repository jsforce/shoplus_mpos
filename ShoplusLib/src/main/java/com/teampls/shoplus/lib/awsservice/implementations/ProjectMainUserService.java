package com.teampls.shoplus.lib.awsservice.implementations;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ProjectMainUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.UserSettingJSONData;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.datatypes.ProjectDalaranAuthData;
import com.teampls.shoplus.lib.datatypes.ProjectSilvermoonAuthData;
import com.teampls.shoplus.lib.datatypes.UserSettingField;
import com.teampls.shoplus.lib.enums.BalanceChangeMethodType;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * (공용) 사용자 설정 관련 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectMainUserService implements ProjectMainUserServiceProtocol {
    public String getMyUserSettingResourcePath(String phoneNumber, UserSettingField setting) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "user-setting", setting.toRemoteStr());
    }

    public String getColorNameSettingResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "user-setting", "color-names");
    }

    private String getPhoneNumberForMainUserService(UserShopInfoProtocol userShop) {
        return userShop.getMainShopOrMyShopOrUserPhoneNumber();     // Main User Service에서는 메인 샵 정보를 사용한다.
    }

    private UserSettingDataProtocol updateSetting(UserShopInfoProtocol userShop, UserSettingField field, Object valueObj) throws MyServiceFailureException {
        try {
            String resource = this.getMyUserSettingResourcePath(getPhoneNumberForMainUserService(userShop), field);
            JSONObject requestBodyObj = new JSONObject().put("value", valueObj);
            String response = APIGatewayClient.requestPUT(resource, requestBodyObj.toString(), UserHelper.getIdentityToken());
            return new UserSettingJSONData(response);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    private Map<ColorType, String> updateColorName(UserShopInfoProtocol userShop, JSONObject requestBodyObj) throws MyServiceFailureException {
        try {
            String resource = this.getColorNameSettingResourcePath(getPhoneNumberForMainUserService(userShop));
            String response = APIGatewayClient.requestPUT(resource, requestBodyObj.toString(), UserHelper.getIdentityToken());
            JSONObject colorNameObj = new JSONObject(response);

            Map<ColorType, String> result = new HashMap<>();
            Iterator<String> iter = colorNameObj.keys();

            while (iter.hasNext()) {
                String colorStr = iter.next();
                String colorName = colorNameObj.getString(colorStr);
                ColorType color = ColorType.ofRemoteStr(colorStr);
                result.put(color, colorName);
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public UserSettingDataProtocol updateUserType(UserShopInfoProtocol userShop, UserConfigurationType userType) throws MyServiceFailureException {
        return this.updateSetting(userShop, UserSettingField.USER_TYPE, userType.toRemoteStr());
    }

    @Override
    @Deprecated
    public UserSettingDataProtocol updateColorNames(UserShopInfoProtocol userShop, Map<ColorType, String> colorNames) throws MyServiceFailureException {
        Map<String, String> colorNameRemoteMap = new HashMap<>();
        for (Map.Entry<ColorType, String> colorMapping: colorNames.entrySet()) {
            colorNameRemoteMap.put(colorMapping.getKey().toRemoteStr(), colorMapping.getValue());
        }
        return this.updateSetting(userShop, UserSettingField.COLOR_NAMES, new JSONObject(colorNameRemoteMap));
    }

    @Override
    public Map<ColorType, String> renamePresetColor(UserShopInfoProtocol userShop, ColorType presetColor, String colorNameAlias) throws MyServiceFailureException {
        try {
            JSONObject params = new JSONObject()
                    .put("operation", "rename")
                    .put("color", presetColor.toRemoteStr())
                    .put("color_name", colorNameAlias);
            return this.updateColorName(userShop, params);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public Map<ColorType, String> addUserColorName(UserShopInfoProtocol userShop, String userColorName) throws MyServiceFailureException {
        try {
            JSONObject params = new JSONObject()
                    .put("operation", "addInteger")
                    .put("color_name", userColorName);
            return this.updateColorName(userShop, params);
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public UserSettingDataProtocol updateItemTraceLocation(UserShopInfoProtocol userShop, List<String> locations) throws MyServiceFailureException {
        return this.updateSetting(userShop, UserSettingField.ITEM_TRACE_LOCATIONS, new JSONArray(locations));
    }

    @Override
    public UserSettingDataProtocol updateRawPriceInputMode(UserShopInfoProtocol userShop, Boolean isRawPriceInputMode) throws MyServiceFailureException {
        return this.updateSetting(userShop, UserSettingField.RAW_PRICE_INPUT, isRawPriceInputMode);
    }

    @Override
    public UserSettingDataProtocol updateRetailPriceMode(UserShopInfoProtocol userShop, Boolean isRetailPriceModeOn) throws MyServiceFailureException {
        return this.updateSetting(userShop, UserSettingField.RETAIL_PRICE, isRetailPriceModeOn);
    }

    @Override
    public UserSettingDataProtocol updateTimeShift(UserShopInfoProtocol userShop, int timeShift) throws MyServiceFailureException {
        return this.updateSetting(userShop, UserSettingField.TIME_SHIFT, timeShift);
    }

    @Override
    public UserSettingDataProtocol updateSilvermoonAuth(UserShopInfoProtocol userShop, ProjectSilvermoonAuthData silvermoonAuth) throws MyServiceFailureException {
        try {
            return this.updateSetting(userShop, UserSettingField.PROJECT_SILVERMOON_AUTH, silvermoonAuth.getObject());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public UserSettingDataProtocol updateDalaranAuth(UserShopInfoProtocol userShop, ProjectDalaranAuthData dalaranAuth) throws MyServiceFailureException {
        try {
            return this.updateSetting(userShop, UserSettingField.PROJECT_DALARAN_AUTH, dalaranAuth.getObject());
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public UserSettingDataProtocol updateDefaultMarginAdditionRate(UserShopInfoProtocol userShop, int defaultMarginAdditionRateInPercent) throws MyServiceFailureException {
        return this.updateSetting(userShop, UserSettingField.MARGIN_ADDITION_RATE, defaultMarginAdditionRateInPercent);
    }

    @Override
    public UserSettingDataProtocol updateBalanceChangeMethod(UserShopInfoProtocol userShop, BalanceChangeMethodType method) throws MyServiceFailureException {
        return this.updateSetting(userShop, UserSettingField.BALANCE_CHANGE_METHOD, method.toRemoteStr());
    }

}
