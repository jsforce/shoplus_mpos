package com.teampls.shoplus.lib.services.chatbot.protocol;

import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderDeliveryType;
import com.teampls.shoplus.lib.services.chatbot.enums.ChatbotOrderState;
import com.teampls.shoplus.lib.services.chatbot.enums.OrderRejectReason;

import org.joda.time.DateTime;

import java.util.List;

/**
 * 챗봇을 통한 주문 데이터 프로토콜
 *
 * @author lucidite
 */
public interface ChatbotOrderDataProtocol {
    /**
     * 주문번호 (transaction ID와 동일 포맷)
     * @return
     */
    String getOrderId();
    DateTime getCreatedDateTime();
    ChatbotOrderState getState();

    /**
     * 매장의 휴대전화번호 (서비스 ID)
     * @return
     */
    String getShopPhoneNumber();

    /**
     * 주문고객의 휴대전화번호 (챗봇에 등록한 서비스 ID)
     * @return
     */
    String getBuyerPhoneNumber();

    /**
     * 주문에 포함된 주문항목
     * [NOTE] ItemType은 sales여야 하나, 주문 상태에서는 ItemType이 실질적인 의미를 가지지는 않는다.
     * (거래로 변환되면서 결제/미송/주문/위탁 등의 제한된 상태로 변경된 이후에야만 상태가 유의미해짐)
     * @return
     */
    List<SlipItemDataProtocol> getOrderItems();

    /**
     * 주문고객이 선택한 배송 방법
     * @return
     */
    ChatbotOrderDeliveryType getDeliveryType();

    /**
     * 주문고객이 기재한 주소 (택배의 경우 입력)
     * @return
     */
    String getAddress();

    /**
     * 주문고객이 기재한 메모
     * @return
     */
    String getMemo();

    /**
     * (완료된 주문의 경우) 연결된 거래내역 ID
     * [NOTE] 연결된 거래 건 조회를 위해 사용할 수 있으며, 완료된 주문이 아닌 경우 빈 문자열을 반환한다.
     * @return 연결된 거래내역 ID 또는 빈 문자열
     */
    String getLinkedTransactionId();

    /**
     * (매장취소한 주문의 경우) 매장에서 선택한 취소사유
     * [NOTE] 사전에 정의한 취소 사유 중에서 매장에서 선택하도록 처리, 매장취소하지 않은 주문은 NONE을 반환한다.
     * @return
     */
    OrderRejectReason getRejectionReason();
}
