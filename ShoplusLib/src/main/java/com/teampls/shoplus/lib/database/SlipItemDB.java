package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyListMap;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.DBResultType;
import com.teampls.shoplus.lib.enums.SizeType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.protocol.SlipItemDataProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2016-09-01.
 * 미송앱과 카탈로그앱의 사용법이 다름
 * 미송앱 : slipKey/serial로 구분, slip 생성후 slipItem 생성 순서가 고정, 다수 slip 존재
 * 카탈로그앱 : itemId/color/size로 구분, slip 생성과 slipItem 생성이 독립, 한순간에 한개의 slip만 존재
 */
public class SlipItemDB extends BaseDB<SlipItemRecord> {

    private static SlipItemDB instance = null;
    protected static int Ver = 7;

    // Ver1 (2016-09-01) : _id, slipKey, consecutiveNumber, slipItemName, unitPrice, quantity, slipItemType, dealDateTime;
    // Ver2 (2017-01-17) : _id, slipKey, itemId, slipItemName, unitPrice, quantity, slipItemType, salesDateTime
    // Ver3 (2017-03-01) :  _id, slipKey, serial, slipItemName, unitPrice, quantity, slipItemType, salesDateTime, updatedDateTime, size, color, itemId;
    // 6  9-11 ; _id, slipKey, serial, slipItemName, unitPrice, quantity, slipItemType, salesDateTime, updatedDateStr, size,  color, itemId, customColorName;

    public enum Column {
        _id, slipKey, serial, slipItemName, unitPrice, quantity, slipItemType, salesDateTime, updatedDateStr, size,
        color, itemId, customColorName, traceIds;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    protected SlipItemDB(Context context) {
        super(context, "SlipItemDB", Ver, Column.toStrs());
    }

    protected SlipItemDB(Context context, String DBName, int DBversion, String[] columns) {
        super(context, DBName, DBversion, columns);
    }

    public static SlipItemDB getInstance(Context context) {
        if (instance == null)
            instance = new SlipItemDB(context);
        return instance;
    }

    @Override
    public void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (newVersion) {
            case 7:
                Log.w("DEBUG_JS", String.format("[%s] %s upgrade %d -> %d and addInteger new %s", getClass().getSimpleName(), DB_NAME, oldVersion, newVersion, Column.traceIds.toString()));
                db.execSQL(MyQuery.addColumn(DB_TABLE, Column.traceIds.toString()));
                break;
            default:
                Log.w("DEBUG_JS", String.format("[%s] %s upgrade %d -> %d and deleteDB", getClass().getSimpleName(), DB_NAME, oldVersion, newVersion));
                db.execSQL(MyQuery.delete(DB_TABLE));
                helper.onCreate(db);
                break;
        }
        upgraded = true;
    }

    private final String traceIdDelimiter = ",";

    protected ContentValues toContentvalues(SlipItemRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.slipKey.toString(), record.slipKey);
        contentvalues.put(Column.serial.toString(), record.serial);
        contentvalues.put(Column.slipItemName.toString(), record.name);
        contentvalues.put(Column.quantity.toString(), record.quantity);
        contentvalues.put(Column.unitPrice.toString(), record.unitPrice);
        contentvalues.put(Column.slipItemType.toString(), record.slipItemType.toString());
        contentvalues.put(Column.salesDateTime.toString(), record.salesDateTime.toString());
        contentvalues.put(Column.updatedDateStr.toString(), record.updatedDateStr);
        contentvalues.put(Column.size.toString(), record.size.toString());
        contentvalues.put(Column.color.toString(), record.color.toString());
        contentvalues.put(Column.itemId.toString(), record.itemId);
        contentvalues.put(Column.customColorName.toString(), record.customColorName);
        contentvalues.put(Column.traceIds.toString(), TextUtils.join(traceIdDelimiter, record.traceIds));
        return contentvalues;
    }

    public MyListMap<String, SlipItemRecord> getListMap() {
        MyListMap<String, SlipItemRecord> results = new MyListMap<>();
        for (SlipItemRecord record : getRecords())
            results.add(record.slipKey, record);
        return results;
    }

    public boolean has(String slipKey, int serial) {
        // Log.i("DEBUG_JS", String.format("[SlipItemDB.has] ? %s : slipKey = %d, consec = %d",Boolean.toSimpleString(hasValue(Column.slipKey, record.slipKey, Column.consecutiveNumber, record.consecutiveNumber)),record.slipKey, record.consecutiveNumber));
        return hasValue(Column.serial, serial, Column.slipKey, slipKey);
    }

    public SlipItemRecord getRecord(String slipKey, int serial) {
        int id = getId(Column.serial, serial, Column.slipKey, slipKey);
        return getRecord(id);
    }

    public void updateOrInsertBySerial(List<SlipItemRecord> records) {
        for (SlipItemRecord record : records)
            updateOrInsertBySerial(record);
    }

    public void updateOrInsertWithSlipKey(String slipKey, List<SlipItemRecord> records) {
        List<SlipItemRecord> dbRecords = getRecordsBy(slipKey); // 검색 대상을 한정
        if (dbRecords.size() != records.size()) {
            deleteBy(slipKey);
            for (SlipItemRecord record : records)
                insert(record);
        } else {
            int serial = 0;
            for (SlipItemRecord record : records) {
                SlipItemRecord dbRecord = dbRecords.get(serial);
                if (dbRecord.isSame(record) == false)
                    update(dbRecord.id, record);
                serial++;
            }
        }
    }

    public DBResult updateOrInsertBySerial(SlipItemRecord record) { // (BySerial)
        if (has(record.slipKey, record.serial)) {
            SlipItemRecord dbRecord = getRecord(record.slipKey, record.serial);
            //  Log.i("DEBUG_JS", String.format("[SlipItemDB.updateOrInsertByRemotePath] %d, %s, %s", dbRecord.id, dbRecord.slipItemName, record.slipItemName));
            if (dbRecord.isSame(record) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public void toLogCat(String location, String slipKey) {
        for (SlipItemRecord record : getRecordsBy(slipKey))
            record.toLogCat(location);
    }

    public void toLogCat(int option) {
        for (SlipItemRecord record : getRecords())
            toLogCat(record.id, record, option, "SlipItemDB.show");
    }

    public void toLogCat(String location) {
        if (getSize() == 0) {
            Log.i("DEBUG_JS", String.format("[SlipItemDB.toLogCat] %s empty", DB_NAME));
            return;
        }

        for (SlipItemRecord record : getRecords())
            record.toLogCat(location);
    }

    public List<SlipItemRecord> getRecords() {
        Cursor cursor = getCursorOrderBy(Column.salesDateTime, true);
        List<SlipItemRecord> results = getRecords(cursor);
        cursor.close();
        return results;
    }

    @Override
    protected int getId(SlipItemRecord record) {
        return record.id;
    }

    @Override
    protected SlipItemRecord getEmptyRecord() {
        return new SlipItemRecord();
    }

    @Override
    protected SlipItemRecord createRecord(Cursor cursor) {

        return new SlipItemRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.slipKey.ordinal()),
                cursor.getInt(Column.serial.ordinal()),
                cursor.getString(Column.slipItemName.ordinal()),
                cursor.getInt(Column.unitPrice.ordinal()),
                cursor.getInt(Column.quantity.ordinal()),
                SlipItemType.valueOf(cursor.getString(Column.slipItemType.ordinal())),
                BaseUtils.toDateTime(cursor.getString(Column.salesDateTime.ordinal())),
                cursor.getString(Column.updatedDateStr.ordinal()),
                ColorType.valueOf(cursor.getString(Column.color.ordinal())),
                SizeType.valueOf(cursor.getString(Column.size.ordinal())),
                cursor.getInt(Column.itemId.ordinal()),
                cursor.getString(Column.customColorName.ordinal()),
                new HashSet(BaseUtils.split(cursor.getString(Column.traceIds.ordinal()), traceIdDelimiter))
        );
    }

    public List<SlipItemRecord> getRecordsBy(String slipKey) {
        return getRecords(Column.slipKey, slipKey);
    }

    public List<SlipItemRecord> getRecordsBy(int itemId) {
        return getRecords(Column.itemId, itemId);
    }

    public List<SlipItemRecord> getRecordsBy(List<SlipRecord> slipRecords) {
        List<SlipItemRecord> results = new ArrayList<>();
        for (SlipRecord slipRecord : slipRecords)
            results.addAll(getRecordsBy(slipRecord.getSlipKey().toSID()));
        return results;
    }

    public void refreshRecordsBySerial(String slipKey) {
        List<QueryAndRecord> queryAndRecords = new ArrayList<>();
        queryAndRecords.add(new QueryAndRecord(Column.slipKey, slipKey));
        List<QueryOrderRecord> queryOrderRecords = new ArrayList<>();
        queryOrderRecords.add(new QueryOrderRecord(Column.serial, true, false));
        List<SlipItemRecord> results = getRecords(queryAndRecords, queryOrderRecords);
        refreshSerial(results);
    }


    public List<SlipItemRecord> getRefreshedRecordsBy(String slipKey) {
        List<QueryAndRecord> queryAndRecords = new ArrayList<>();
        queryAndRecords.add(new QueryAndRecord(Column.slipKey, slipKey));
        List<QueryOrderRecord> queryOrderRecords = new ArrayList<>();
        queryOrderRecords.add(new QueryOrderRecord(Column.serial, true, false));
        List<SlipItemRecord> results = getRecords(queryAndRecords, queryOrderRecords);
        results = refreshSerial(results).first;
        return results;
    }

    public Map<Integer, Integer> deleteBySerial(String slipKey, int serial) {
        int id = getId(Column.serial, serial, Column.slipKey, slipKey);
        return deleteAndRefresh(id);
    }

    public Map<Integer, Integer> deleteAndRefresh(int id) {
        SlipItemRecord record = getRecord(id);
        super.delete(id);
        Map<Integer, Integer> r = refreshSerial(record.slipKey).second;
        return r;
    }

    private  Pair<List<SlipItemRecord>, Map<Integer, Integer>> refreshSerial(String slipKey) {
        List<SlipItemRecord> results = getRecords(Column.slipKey, slipKey);
        Pair<List<SlipItemRecord>, Map<Integer, Integer>> p = refreshSerial(results);
        Map<Integer, Integer> r = p.second;
        return p;
    }

    public List<SlipItemRecord> pullUp(String slipKey, int serial) {
        return moveTo(slipKey, serial, 0);
    }

    public List<SlipItemRecord> pullDown(String slipKey, int serial) {
        List<SlipItemRecord> records = getRecordsOrderBySerial(slipKey);
        if (records.size() <= 1)
            return records;
        else
            return moveTo(slipKey, serial, records.size()-1);
    }

    private List<SlipItemRecord> moveTo(String slipKey, int serial, int position) {
        List<SlipItemRecord> results = new ArrayList<>();
        SlipItemRecord interest = new SlipItemRecord();
        boolean found = false;
        for (SlipItemRecord record : getRecordsOrderBySerial(slipKey)) { // serial 순으로
            if (record.serial == serial) {
                found = true;
                interest = record;
                continue;
            }
            results.add(record);
        }
        if (found)
            results.add(position, interest);
        return refreshSerial(results).first; // 저장까지 다 해주므로
    }

    private Pair<List<SlipItemRecord>, Map<Integer, Integer>> refreshSerial(List<SlipItemRecord> records) {
        List<SlipItemRecord> results = new ArrayList<>();
        Map<Integer, Integer> beforeAndAfterSerials = new HashMap();
        int serial = 0;
        for (SlipItemRecord slipItemRecord : records) {
            beforeAndAfterSerials.put(slipItemRecord.serial, serial); // from - to -
            if (slipItemRecord.serial != serial) {
                slipItemRecord.serial = serial;
                update(slipItemRecord.id, slipItemRecord);
            }
            results.add(slipItemRecord);
            serial++;
        }
        return Pair.create(results, beforeAndAfterSerials);
    }

    public int getNextSerial(String slipKey) {
        int serial = 0;
        for (SlipItemRecord record : getRecordsOrderBySerial(slipKey)) {
            if (record.serial != serial) {
                record.serial = serial;
                update(record.id, record);
            }
            serial++;
        };
        return serial;
    }

    //public SlipSummaryRecord getSummary(String slipKey, boolean doRemoveNonpendingTypes) {
    public SlipSummaryRecord getSummary(String slipKey) {
        return new SlipSummaryRecord(getRecordsBy(slipKey));
    }

    public void deleteBy(String... slipKeys) {
        deleteAll(Column.slipKey, BaseUtils.toList(slipKeys));
    }

    public void deleteInvalids(int itemId, List<String> validKeys) {
        List<String> invalidSlipKeys = new ArrayList<>();
        for (SlipItemRecord record : getRecords(Column.itemId, itemId)) {
            if (validKeys.contains(record.slipKey) == false)
                invalidSlipKeys.add(record.slipKey);
        }
        deleteAll(Column.slipKey, invalidSlipKeys);
    }

    public void deleteInvalids(int itemId, List<DateTime> months, List<String> validKeys) {
        DateTime fromMonth = DateTime.now(), toMonth = Empty.dateTime;
        for (DateTime dateTime : months) {
            DateTime month = BaseUtils.toMonth(dateTime);
            if (fromMonth.isAfter(month))
                fromMonth = month;
            if (toMonth.isBefore(month))
                toMonth = month;
        }
        toMonth = toMonth.plusMonths(1).minusMillis(1);
        List<String> invalidSlipKeys = new ArrayList<>();
        for (SlipItemRecord record : getRecords(Column.itemId, itemId)) {
            if (BaseUtils.isBetween(record.salesDateTime, fromMonth, toMonth)) { // 동일기간내
                if (validKeys.contains(record.slipKey) == false) // 없다면
                    invalidSlipKeys.add(record.slipKey); // 사라진 것
            }
        }
        deleteAll(Column.slipKey, invalidSlipKeys);
    }

    private static boolean isSame(SlipItemRecord record1, SlipItemRecord record2) {
        if (record1.name.equals(record2.name) == false) return false;
        if (record1.quantity != record2.quantity) return false;
        if (record1.unitPrice != record2.unitPrice) return false;
        // color, size
        return true;
    }

    public static List<SlipItemDataProtocol> toProtocol(List<SlipItemRecord> records) {
        List<SlipItemDataProtocol> results = new ArrayList<>();
        for (SlipItemRecord record : records)
            results.add(record);
        return results;
    }

    public  List<SlipItemRecord> getRecordsOrderBySerial(String slipKey) {
      return getRecordsOrderBy(Column.slipKey, slipKey, Column.serial, "+0", false);
//        int indexSerial, targetSerial;
//        List<SlipItemRecord> records = getRecords(Column.slipKey, slipKey);
//        for (int i = 0; i < records.size(); i++) {
//            int index = i;
//            indexSerial = records.get(i).serial;
//            for (int j = i + 1; j < records.size(); j++) {
//                targetSerial = records.get(j).serial;
//                //Log.i("DEBUG_JS", String.format("[LocalUserDB.sortByName] index (%d) %s, target (%d) %s, compare %d", index, indexName, j, targetName, targetName.compareTo(indexName)));
//                if (targetSerial < indexSerial) { // 내림차순 : 가장 작은걸 찾아라
//                    index = j;
//                    indexSerial = targetSerial;
//                }
//            }
//            if (index != i) {
//                SlipItemRecord foreNameRecord = records.get(index);
//                records.set(index, records.get(i));
//                records.set(i, foreNameRecord);
//            }
//            //   Log.w("DEBUG_JS", String.format("[LocalUserDB.sortByName] %dth : foreName %s (%d)", i, foreNameRecord.name, index));
//        }
//        return records;
    }

    public void sortByName(String slipKey) {
        List<QueryOrderRecord> orderRecords = new QueryOrderRecord(Column.slipItemName, false, false).toList();
        orderRecords.add(new QueryOrderRecord(Column.slipItemType, false, false));
        List<SlipItemRecord> records = getRecords(new QueryAndRecord(Column.slipKey, slipKey).toList(),
                orderRecords);
        refreshSerial(records);
    }

    public List<SlipItemRecord> sortByName(List<SlipItemRecord> records) {
        String indexName, targetName;
        for (int i = 0; i < records.size(); i++) {
            int index = i;
            indexName = records.get(i).name;
            for (int j = i + 1; j < records.size(); j++) {
                targetName = records.get(j).name;
                //Log.i("DEBUG_JS", String.format("[LocalUserDB.sortByName] index (%d) %s, target (%d) %s, compare %d", index, indexName, j, targetName, targetName.compareTo(indexName)));
                if ((targetName.compareTo(indexName)) < 0) {
                    index = j;
                    indexName = targetName;
                }
            }
            if (index != i) {
                SlipItemRecord foreNameRecord = records.get(index);
                records.set(index, records.get(i));
                records.set(i, foreNameRecord);
            }
            //   Log.w("DEBUG_JS", String.format("[LocalUserDB.sortByName] %dth : foreName %s (%d)", i, foreNameRecord.name, index));
        }
        return records;
    }

    public void sortByTypes(String slipKey) {
        List<SlipItemRecord> memoRecords = new ArrayList<>(); // itemId 없고 amount = 0인 경우 한정

        List<SlipItemRecord> plusSales = new ArrayList<>(), plusAdvance = new ArrayList<>(), plusSample = new ArrayList<>(),
        plusConsign = new ArrayList<>(), plusPreorder = new ArrayList<>();

        List<SlipItemRecord> minusReturn = new ArrayList<>(), minusWithdrawal = new ArrayList<>(), minusAdvance = new ArrayList<>(),
                minusDiscount = new ArrayList<>();

        List<SlipItemRecord> exchange = new ArrayList<>(), deposit = new ArrayList<>(), advance = new ArrayList<>(),
                consign = new ArrayList<>(), sample = new ArrayList<>(), preorder = new ArrayList<>();

        for (SlipItemRecord record : getRecordsBy(slipKey)) {
            switch (record.slipItemType) {
                default:
                    memoRecords.add(record);
                    break;
                // plus
                case SALES:
                    plusSales.add(record);
                    break;
                case ADVANCE:
                    plusAdvance.add(record);
                    break;
                case SAMPLE_CLEAR:
                    plusSample.add(record);
                    break;
                case CONSIGN_CLEAR:
                    plusConsign.add(record);
                    break;
                case PREORDER_CLEAR:
                    plusPreorder.add(record);
                    break;

                // 0
                case EXCHANGE_RETURN:
                case EXCHANGE_RETURN_PENDING:
                case EXCHANGE_SALES:
                case EXCHANGE_SALES_PENDING:
                    exchange.add(record);
                    break;
                case DEPOSIT:
                    deposit.add(record);
                    break;
                case ADVANCE_CLEAR:
                case ADVANCE_OUT_OF_STOCK:
                case ADVANCE_PACKING:
                    advance.add(record);
                    break;
                case CONSIGN:
                case CONSIGN_RETURN:
                    consign.add(record);
                    break;
                case SAMPLE:
                case SAMPLE_RETURN:
                    sample.add(record);
                    break;
                case PREORDER:
                case PREORDER_PACKING:
                case PREORDER_OUT_OF_STOCK:
                case PREORDER_WITHDRAW:
                    preorder.add(record);
                    break;

                // -1
                case RETURN:
                    minusReturn.add(record);
                    break;
                case WITHDRAWAL:
                    minusWithdrawal.add(record);
                    break;
                case ADVANCE_SUBTRACTION:
                    minusAdvance.add(record);
                    break;
                case DISCOUNT:
                    minusDiscount.add(record);
                    break;
            }
        }

        // serial 번호 재부여
        List<SlipItemRecord> results = new ArrayList<>();
        results.addAll(plusSales);
        results.addAll(plusAdvance);
        results.addAll(plusSample);
        results.addAll(plusConsign);
        results.addAll(plusPreorder);

        results.addAll(exchange);
        results.addAll(deposit);
        results.addAll(advance);
        results.addAll(consign);
        results.addAll(sample);
        results.addAll(preorder);

        results.addAll(minusReturn);
        results.addAll(minusWithdrawal);
        results.addAll(minusAdvance);
        results.addAll(minusDiscount);

        results = BaseUtils.sortOnlyOption(results); // 옵션별 내림차순 정렬
        results.addAll(memoRecords);

        refreshSerial(results);
    }

}
