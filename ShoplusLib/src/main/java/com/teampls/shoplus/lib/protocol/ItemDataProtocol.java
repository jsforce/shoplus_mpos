package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemStateType;

import org.joda.time.DateTime;

/**
 * 상품 아이템 데이터 프로토콜
 *
 * @author lucidite
 */

public interface ItemDataProtocol {
    int getItemId();
    String getSerialNumber();
    String getProviderPhoneNumber();
    ItemStateType getItemState();
    ItemCategoryType getCategory();
    String getItemName();
    String getChineseName();
    String getItemDescription();
    String getMainImagePath();
    int getPrice();
    int getRetailPrice();
    String getPrivateNote();
    DateTime getCreatedDatetime();
    DateTime getModifiedDatetime();
    DateTime getPickedDatetime();
}
