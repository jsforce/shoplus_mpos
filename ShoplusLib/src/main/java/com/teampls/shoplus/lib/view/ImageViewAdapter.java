package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.database_global.ImageDB;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-11-10.
 */

public class ImageViewAdapter extends PagerAdapter implements View.OnClickListener {
    protected Context context;
    private LayoutInflater inflater;
    private int thisView = R.layout.base_imagefullview;
    private ImageDB imageDB = ImageDB.getInstance(context);
    private List<Integer> ids = new ArrayList<>();
    private int currentPosition = 0;

    public ImageViewAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public ImageViewAdapter setIds(List<Integer> ids) {
        this.ids = ids;
        return this;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    @Override
    public int getCount() {
        return ids.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = inflater.inflate(thisView, null);
        ImageView ivImageView = (ImageView) view.findViewById(R.id.imagefullview_image);
        ivImageView.setOnClickListener(this);
        ivImageView.setImageBitmap(imageDB.getRecord(ids.get(position)).getBitmap());
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.imagefullview_image) {
               ImageZoomView.start(context, imageDB.getRecord(currentPosition+1).getBitmap()); // ImageZoomView
        }
    }
}
