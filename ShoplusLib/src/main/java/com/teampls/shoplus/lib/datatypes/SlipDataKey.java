package com.teampls.shoplus.lib.datatypes;

import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;

import org.joda.time.DateTime;

/**
 * Slip Data의 Remote DB Key. Combination of the owner phone number & created datetime.
 *
 * @author lucidite
 */

public class SlipDataKey {
    public String ownerPhoneNumber = "";
    public DateTime createdDateTime = Empty.dateTime;
    private static final String delimiter = "_";

    public SlipDataKey(String ownerPhoneNumber, DateTime createdDateTime) {
        this.ownerPhoneNumber = ownerPhoneNumber;
        this.createdDateTime = createdDateTime;
    }

    public SlipDataKey() {}

    public SlipDataKey clone() {
        return new SlipDataKey(ownerPhoneNumber, createdDateTime);
    }

    public SlipDataKey(String slipKey) {
        if (slipKey.isEmpty()) {
            Log.i("DEBUG_JS", String.format("[SlipDataKey.SlipDataKey] empty slipkey"));
            return;
        }
        this.ownerPhoneNumber = slipKey.split(delimiter)[0];
        this.createdDateTime = BaseUtils.toDateTime(slipKey.split(delimiter)[1]);
    }

    public String toSID() {
        return String.format("%s%s%s", ownerPhoneNumber, SlipDataKey.delimiter, createdDateTime.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SlipDataKey that = (SlipDataKey) o;

        if (!ownerPhoneNumber.equals(that.ownerPhoneNumber)) return false;
        return createdDateTime.equals(that.createdDateTime);

    }

    @Override
    public int hashCode() {
        int result = ownerPhoneNumber.hashCode();
        result = 31 * result + createdDateTime.hashCode();
        return result;
    }
}
