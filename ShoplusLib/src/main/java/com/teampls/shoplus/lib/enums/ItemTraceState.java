package com.teampls.shoplus.lib.enums;

/**
 * 추적 중인 아이템의 재고 상태
 *
 * @author lucidite
 */

public enum ItemTraceState {
    FOUND("n", "재고"),               // 원래 normal
    WAREHOUSE("w", "창고관리"),         // SaaS 관리
    RELEASED("r", "출고완료"),              // SaaS 관리
    SOLD("s", "판매"),
    NOT_FOUND("m", "못찾음"),          // 원래 missing
    HIDDEN("h", "삭제됨"),
    DEFAULT("", "미등록");

    private String remoteStr;
    public String uiStr;

    ItemTraceState(String remoteStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
    }

    public static ItemTraceState fromRemoteStr(String remoteStr) {
        for (ItemTraceState state: ItemTraceState.values()) {
            if (state.remoteStr.equals(remoteStr)) {
                return state;
            }
        }
        return DEFAULT;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }

}
