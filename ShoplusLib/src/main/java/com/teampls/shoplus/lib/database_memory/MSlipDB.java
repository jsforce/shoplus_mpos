package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

/**
 * Created by Medivh on 2018-04-17.
 */

public class MSlipDB extends MemorySlipDB {
    private static MSlipDB instance;

    public static MSlipDB getInstance(Context context) {
        if (instance == null)
            instance = new MSlipDB(context);
        return instance;
    }

    private MSlipDB(Context context) {
        super(context, "MSlipDB");
        items = MSlipItemDB.getInstance(context);
    }

}
