package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.protocol.SimpleItemDataProtocol;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 상품목록 데이터 클래스
 *
 * @author lucidite
 */
public class SimpleItemData implements SimpleItemDataProtocol {
    private int itemid;
    private String serialNumber;
    private String provider;
    private ItemStateType itemState;
    private String itemName;
    private String mainImagePath;
    private int price;

    public SimpleItemData(JSONObject dataObj) throws JSONException {
        this.itemid = dataObj.getInt("id");
        this.serialNumber = dataObj.getString("sn");
        this.provider = dataObj.getString("provider");
        this.itemState = ItemStateType.fromRemoteStr(dataObj.getString("item_state"));
        this.itemName = dataObj.optString("item_name", "");
        this.mainImagePath = dataObj.getString("main_image");
        this.price = dataObj.optInt("price", 0);
    }

    @Override
    public int getItemId() {
        return this.itemid;
    }

    @Override
    public String getSerialNumber() {
        return this.serialNumber;
    }

    @Override
    public String getProviderPhoneNumber() {
        return this.provider;
    }

    @Override
    public ItemStateType getItemState() {
        return this.itemState;
    }

    @Override
    public String getItemName() {
        return this.itemName;
    }

    @Override
    public String getMainImagePath() {
        return this.mainImagePath;
    }

    @Override
    public int getPrice() {
        return this.price;
    }
}
