package com.teampls.shoplus.lib.services.chatbot.datatypes;

import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotBuyerRegistrationRequestDataProtocol;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author lucidite
 */
public class ChatbotBuyerRegistrationRequestData extends ChatbotBuyerBaseData implements ChatbotBuyerRegistrationRequestDataProtocol {
    private DateTime requested;

    public ChatbotBuyerRegistrationRequestData() {
        super();
        this.requested = Empty.dateTime;
    }

    public ChatbotBuyerRegistrationRequestData(JSONObject json) throws JSONException {
        super(json);
        long timestampInMilliseconds = json.getLong("reg_req") * 1000;
        this.requested = new DateTime(timestampInMilliseconds);
    }

    public ChatbotBuyerRegistrationRequestData(ChatbotBuyerRegistrationRequestDataProtocol protocol) {
        super(protocol);
        this.requested = protocol.getRequestedDateTime();
    }

    @Override
    public DateTime getRequestedDateTime() {
        return this.requested;
    }
}
