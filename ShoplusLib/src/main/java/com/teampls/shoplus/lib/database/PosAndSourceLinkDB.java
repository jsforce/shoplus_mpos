package com.teampls.shoplus.lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.enums.DBResultType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-05-22.
 */

// 다운받은 slipItem 과 영수증 상에 올라가있는 slipItem을 관리하기 위한 DB

public class PosAndSourceLinkDB extends BaseDB<PosAndSourceLinkRecord> {

    private static PosAndSourceLinkDB instance = null;
    protected static int Ver = 1;

    public static PosAndSourceLinkDB getInstance(Context context) {
        if (instance == null)
            instance = new PosAndSourceLinkDB(context);
        return instance;
    }

    private PosAndSourceLinkDB(Context context) {
        super(context, "ExternalSlipItemDB", Ver, Column.toStrs());
    }

    // 1 :  _id, posSlipKey, posSerial, updateDateTime, counterpartPhoneNumber, originalSlipKey, originalSerial;

    // slipItemDbId : posSlipItemDB의 id
    // externalSlipItemDbId : sourceSlipItemDB의 id
    public enum Column {
        _id, slipItemDbId, updateDateTime, counterpartPhoneNumber, externalSlipItemDbId;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    protected ContentValues toContentvalues(PosAndSourceLinkRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.slipItemDbId.toString(), record.posSlipItemDbId);
        contentvalues.put(Column.updateDateTime.toString(), record.updateDateTime.toString());
        contentvalues.put(Column.counterpartPhoneNumber.toString(), record.counterpartPhoneNumber);
        contentvalues.put(Column.externalSlipItemDbId.toString(), record.sourceSlipItemDbId);
        return contentvalues;
    }

    public List<PosAndSourceLinkRecord> getRecordsBy(List<SlipItemRecord> slipItemRecords) {
        List<PosAndSourceLinkRecord> results = new ArrayList<>();
        for (SlipItemRecord slipItemRecord : slipItemRecords) {
            if (has(slipItemRecord.id)) { // 외부에서 가져온 것만 정보가 있다
                results.add(getRecordBy(slipItemRecord.id));
            }
        }
        return results;
    }


    public boolean has(int posSlipItemDbId) {
        // Log.i("DEBUG_JS", String.format("[SlipItemDB.has] ? %s : slipKey = %d, consec = %d",Boolean.toSimpleString(hasValue(Column.slipKey, record.slipKey, Column.consecutiveNumber, record.consecutiveNumber)),record.slipKey, record.consecutiveNumber));
        return hasValue(Column.slipItemDbId, posSlipItemDbId);
    }

    public PosAndSourceLinkRecord getRecordBy(int posSlipItemDbId) {
        return getRecord(Column.slipItemDbId, BaseUtils.toDbStr(posSlipItemDbId));
    }

    public void deleteBy(int posSlipItemDbId) {
        PosAndSourceLinkRecord record = getRecordBy(posSlipItemDbId);
        delete(record.id);
    }

    public PosAndSourceLinkRecord getRecordBySource(int sourceSlipItemDbId) {
        return getRecord(Column.externalSlipItemDbId, BaseUtils.toDbStr(sourceSlipItemDbId));
    }

    public void deleteByExternal(int sourceSlipItemId) {
        PosAndSourceLinkRecord record = getRecordBySource(sourceSlipItemId);
        delete(record.id);
    }

    public DBResult updateOrInsert(PosAndSourceLinkRecord record) {
        if (has(record.posSlipItemDbId)) {
            PosAndSourceLinkRecord dbRecord = getRecord(record.posSlipItemDbId);
            //  Log.i("DEBUG_JS", String.format("[SlipItemDB.updateOrInsertByRemotePath] %d, %s, %s", dbRecord.id, dbRecord.slipItemName, record.slipItemName));
            if (dbRecord.isSame(record) == false) {
                return update(dbRecord.id, record);
            } else {
                return new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    public void toLogCat(String location) {
        for (PosAndSourceLinkRecord record : getRecords())
            record.toLogCat(location);
    }

    @Override
    protected int getId(PosAndSourceLinkRecord record) {
        return record.id;
    }

    @Override
    protected PosAndSourceLinkRecord getEmptyRecord() {
        return new PosAndSourceLinkRecord();
    }

    @Override
    protected PosAndSourceLinkRecord createRecord(Cursor cursor) {
        return new PosAndSourceLinkRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getInt(Column.slipItemDbId.ordinal()),
                BaseUtils.toDateTime(cursor.getString(Column.updateDateTime.ordinal())),
                cursor.getString(Column.counterpartPhoneNumber.ordinal()),
                cursor.getInt(Column.externalSlipItemDbId.ordinal())
        );
    }

    public boolean hasSource(int sourceSlipItemDbId) {
        return hasValue(Column.externalSlipItemDbId, sourceSlipItemDbId);
    }

    public void checkIntegrity(SlipItemDB posSlipItemDB) {
       List<Integer> validDbIds = posSlipItemDB.getIds();
        for (PosAndSourceLinkRecord record : getRecords())
            if (validDbIds.contains(record.posSlipItemDbId) == false) {
              //  Log.w("DEBUG_JS", String.format("[PosAndSourceLinkDB.checkIntegrity] delete ... posDBid %d, source slipItemDbId %d, counterpart %s, time %s",record.posSlipItemDbId, record.sourceSlipItemDbId, record.counterpartPhoneNumber, record.updateDateTime));
                delete(record.id);
            }
    }

}

