package com.teampls.shoplus.lib.view_module;

import android.content.Context;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.view.MyView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-02-18.
 */

public class MyRadioGroup<T> implements View.OnClickListener {
    private Context context;
    public List<RadioButton> radioButtons = new ArrayList<>();
    public RadioButton currentRadioButton;
    private View.OnClickListener onClickListener;
    private View view;
    private double scale;
    private T defaultT;
    private boolean doSaveSetting = false;
    private String settingKEY = "";
    private KeyValueDB keyValueDB;
    public boolean isFirstClick = false;

    public MyRadioGroup(Context context, View view, double scale, View.OnClickListener onClickListener) {
        this.context = context;
        this.view = view;
        this.scale = scale;
        this.onClickListener = onClickListener;
    }

    public void setEnabled(boolean value) {
        for (RadioButton button : radioButtons)
            button.setEnabled(value);
    }

    public void setAsNormalFont(Context context) {
        for (RadioButton radioButton : radioButtons)
            MyView.setAsNormalFont(context, (TextView) radioButton);
    }

    public void setKeyAndInit(Context context, String key, Enum<?> defautValue) {
        doSaveSetting = true;
        keyValueDB = KeyValueDB.getInstance(context);
        settingKEY = key;
        try {
            Enum<?> savedValue = defautValue.valueOf(defautValue.getClass(), keyValueDB.getValue(settingKEY, defautValue.toString()));
            setChecked((T) savedValue);
        } catch (IllegalArgumentException e) { // enum name을 변경했을 경우
            setChecked((T) defautValue);
            keyValueDB.put(settingKEY, defautValue.toString());
        }
    }

    public  MyRadioGroup(Context context, View view) {
        this(context, view, 1.0, null);
    }

    public void setOnClickListener(boolean doClick, View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        if (doClick) {
            isFirstClick = true;
            click();
            isFirstClick = false;
        }
    }

    public void click() {
        if (onClickListener != null)
            onClickListener.onClick(view);
    }

    public MyRadioGroup add(int resId, T tag) {
        return add((RadioButton) view.findViewById(resId), tag);
    }

    public void clear() {
        radioButtons.clear();
        // currentRadioButton
    }

    public void setText(T tag, String text) {
        for (RadioButton button : radioButtons) {
            if ((T) button.getTag() == tag)
                button.setText(text);
        }
    }

    public void setVisibility(int visibility, T... tags) {
        for (RadioButton button : radioButtons) {
            for (T tag : tags) {
                if ((T) button.getTag() == tag)
                    button.setVisibility(visibility);
            }
        }
    }

    public void setDefault(T defaultT) {
        this.defaultT = defaultT;
    }

    public MyRadioGroup add(RadioButton radioButton, T tag) {
        radioButton.setOnClickListener(this);
        radioButton.setTag(tag);
        radioButton.setScaleX((float) scale);
        radioButton.setScaleY((float) scale);
        radioButtons.add(radioButton);
        MyView.setTextViewByDeviceSize(context, radioButton);
        if (defaultT == null)
            defaultT = tag;
        return this;
    }

    @Override
    public void onClick(View v) {
        if (v instanceof RadioButton == false) return;
        currentRadioButton = (RadioButton) v;
        for (RadioButton button : radioButtons)
            button.setChecked(button == currentRadioButton);
        if (onClickListener != null)
            onClickListener.onClick(v);
        if (doSaveSetting) {
            keyValueDB.put(settingKEY, v.getTag().toString());
        }
    }

    public void setChecked(T tag) {
        for (RadioButton button : radioButtons) {
            button.setChecked(button.getTag() == tag);
            if (button.getTag() == tag) {
                currentRadioButton = button;
                break;
            }
        }
    }

    public void setChecked(T tag, T defaultTag) {
        boolean checked = false;
        for (RadioButton button : radioButtons) {
            button.setChecked(button.getTag() == tag);
            if (button.getTag() == tag) {
                currentRadioButton = button;
                break;
            }
        }
    }

    public void setChecked(int index) {
        for (int i = 0; i < radioButtons.size(); i++) {
            radioButtons.get(i).setChecked(i == index);
            if (i == index)
                currentRadioButton = radioButtons.get(i);
        }
    }

    public boolean hasCheckedItem() {
        for (RadioButton button : radioButtons) {
            if (button.isChecked())
                return true;
        }
        return false;
    }

    public T getCheckedItem() {
        for (RadioButton button : radioButtons) {
            if (button.isChecked())
                return ((T) button.getTag());
        }
        return defaultT;
    }

}
