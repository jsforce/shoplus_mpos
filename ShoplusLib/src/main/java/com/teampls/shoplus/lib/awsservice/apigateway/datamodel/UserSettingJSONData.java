package com.teampls.shoplus.lib.awsservice.apigateway.datamodel;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.datatypes.ExternalAPIData;
import com.teampls.shoplus.lib.datatypes.FunctionBlockConfigurationData;
import com.teampls.shoplus.lib.datatypes.MessengerLinkData;
import com.teampls.shoplus.lib.datatypes.MyShopInformationData;
import com.teampls.shoplus.lib.datatypes.ProjectDalaranAuthData;
import com.teampls.shoplus.lib.datatypes.ProjectSilvermoonAuthData;
import com.teampls.shoplus.lib.datatypes.ServiceActivationData;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.datatypes.UserSettingField;
import com.teampls.shoplus.lib.datatypes.UserShopInfo;
import com.teampls.shoplus.lib.enums.BalanceChangeMethodType;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.MessengerType;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.enums.UserLevelType;
import com.teampls.shoplus.lib.protocol.FunctionBlockConfigurationDataProtocol;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;
import com.teampls.shoplus.lib.protocol.UserSpecificSettingProtocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author lucidite
 */

public class UserSettingJSONData implements UserSettingDataProtocol, JSONDataWrapper {
    private JSONObject data;

    public UserSettingJSONData(JSONObject data) throws JSONException {
        if (data != null) {
            this.data = data;
        } else {
            this.data = new JSONObject();
        }
    }

    public UserSettingJSONData(String response) throws JSONException {
        this.data = new JSONObject(response);
    }

    @Override
    public UserLevelType getUserLevel() {
        String userLevelStr = this.data.optString("user_level", "");
        return UserLevelType.fromRemoteStr(userLevelStr);
    }

    @Override
    public UserConfigurationType getUserType() {
        String remoteStr = this.data.optString(UserSettingField.USER_TYPE.toRemoteStr());
        return UserConfigurationType.fromRemoteStr(remoteStr);
    }

    @Override
    public Map<ColorType, String> getColorNames() {
        Map<ColorType, String> result = new HashMap<>();
        try {
            JSONObject colorNamesObj = this.data.getJSONObject(UserSettingField.COLOR_NAMES.toRemoteStr());
            Iterator<String> iter = colorNamesObj.keys();
            while(iter.hasNext()) {
                String colorStr = iter.next();
                ColorType color = ColorType.ofRemoteStr(colorStr);
                // ColorType.fromRemoteStr 메서드 구현상 null 비교는 필요 없으나, 안전을 위해 null check
                if (color != null && color != ColorType.Default) {
                    result.put(color, colorNamesObj.getString(colorStr));
                }
            }
            return result;
        } catch (JSONException e) {
            return result;
        }
    }

    @Override
    public List<String> getItemTraceLocations() {
        List<String> result = new ArrayList<>();
        try {
            JSONArray locationArr = this.data.getJSONArray(UserSettingField.ITEM_TRACE_LOCATIONS.toRemoteStr());
            for (int i = 0; i < locationArr.length(); ++i) {
                result.add(locationArr.getString(i));
            }
            return result;
        } catch (JSONException e) {
            return result;
        }
    }

    @Override
    public Boolean isRawPriceInputMode() {
        return this.data.optBoolean(UserSettingField.RAW_PRICE_INPUT.toRemoteStr(), false);
    }

    @Override
    public Boolean isRetailOn() {
        return this.data.optBoolean(UserSettingField.RETAIL_PRICE.toRemoteStr(), false);
    }

    @Override
    public Boolean showNegativeStockValue() {
        return this.data.optBoolean(UserSettingField.SHOW_NEGATIVE_STOCK_VALUE.toRemoteStr(), true);
    }

    @Override
    public int getTimeShift() {
        return this.data.optInt(UserSettingField.TIME_SHIFT.toRemoteStr(), 0);
    }

    @Override
    public String getMainShop() {
        return this.data.optString("main_shop", "");
    }


    @Override
    public ProjectDalaranAuthData getDalaranAuthData() {
        return new ProjectDalaranAuthData(this.data.optJSONObject("dalaran_auth"));
    }

    @Override
    public ProjectSilvermoonAuthData getSilvermoonAuthData() {
        return new ProjectSilvermoonAuthData(this.data.optJSONObject("silvermoon_auth"));
    }

    @Override
    public int getDefaultMarginAdditionRate() {
        return this.data.optInt("mar", 0);
    }

    @Override
    public BalanceChangeMethodType getBalanceChangeMethod() {
        String remoteStr = this.data.optString("balance_change", "");
        return BalanceChangeMethodType.fromRemoteStr(remoteStr);
    }

    @Override
    public MyShopInformationData getShopInformation() {
        JSONObject shopInfoObj = this.data.optJSONObject("shop-info");
        if (shopInfoObj == null) {
            shopInfoObj = new JSONObject();
        }
        return new MyShopInformationData(shopInfoObj);
    }

    @Override
    public UserSpecificSettingProtocol getUserSpecificSetting() {
        return new UserSpecificSettingJSONData(this.data.optJSONObject("user-specific"));
    }

    @Override
    public List<String> getAllowedShops() {
        List<String> result = new ArrayList<>();
        JSONArray allowedShopArr = this.data.optJSONArray("allowed_shops");
        if (allowedShopArr != null) {
            for (int i = 0; i < allowedShopArr.length(); ++i) {
                JSONObject allowedShopObj = allowedShopArr.optJSONObject(i);
                if (allowedShopObj != null) {
                    String shopPhoneNumber = allowedShopObj.optString("shop", "");
                    if (!shopPhoneNumber.isEmpty()) {
                        result.add(shopPhoneNumber);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Map<ShoplusServiceType, ServiceActivationData> getServiceActivationData() {
        Map<ShoplusServiceType, ServiceActivationData> result = new HashMap<>();
        JSONObject activationObj = this.data.optJSONObject("service-activation");
        if (activationObj != null) {
            JSONArray serviceNameArr = activationObj.names();
            if (serviceNameArr == null)
                return result;

            for (int i = 0; i < serviceNameArr.length(); ++i) {
                try {
                    String serviceName = serviceNameArr.getString(i);
                    JSONObject activationData = activationObj.getJSONObject(serviceName);

                    ShoplusServiceType service = ShoplusServiceType.fromRemoteStr(serviceName);
                    ServiceActivationData activation = new ServiceActivationData(activationData);
                    result.put(service, activation);
                } catch (JSONException e) {
                    // TODO Parsing Error Logging
                    continue;
                }
            }
        }
        return result;
    }

    @Override
    public Set<MessengerLinkData> getMessengerLinkData() {
        try {
            return MessengerLinkData.parse(this.data.optJSONObject("msg"));
        } catch (JSONException e) {
            Log.e("DEBUG_TAG", "[ERROR] JSONException in UserSettingJSONData.getMessengerLinkData(): " + e.getMessage());
            return new HashSet<>();
        }
    }

    @Override
    public Boolean doesShopHaveMasterUser() {
        // 일반적으로 has_master 값은 반드시 설정되어 있으나, 안전망으로 true 설정
        // master 사용자가 없으면 앱 권한이 열릴 것이므로, fallback 케이스로 true로 설정하는 것이 안전하다.
        return this.data.optBoolean("has_master", true);
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return this.data;
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("UserSettingJSONData instance does not have a JSON array");
    }

    //////////////////////////////////////////////////////////////////////
    // Utility methods - Auth Check

    @Override
    public Boolean hasDalaranOnlineBalanceChangePermission() {
        return this.getUserLevel().getPermissionLevel() >= this.getDalaranAuthData().getOnlineBalanceChangePermission().getPermissionLevel();
    }

    @Override
    public Boolean hasDalaranEntrustedBalanceChangePermission() {
        return this.getUserLevel().getPermissionLevel() >= this.getDalaranAuthData().getEntrustedBalanceChangePermission().getPermissionLevel();
    }

    @Override
    public Boolean hasDalaranDepositChangePermission() {
        return this.getUserLevel().getPermissionLevel() >= this.getDalaranAuthData().getDepositChangePermission().getPermissionLevel();
    }

    //////////////////////////////////////////////////////////////////////
    // Chatbot

    @Override
    public String getChatbotLinkedPlusFriend() {
        return this.data.optString("chatbot_pf", "");
    }


    //////////////////////////////////////////////////////////////////////
    // External API Data

    @Override
    public ExternalAPIData getExternalAPIData() {
        return new ExternalAPIData(this.data.optJSONObject("api"));
    }

    //////////////////////////////////////////////////////////////////////
    // Function Block Configurations (from remote)

    @Override
    public FunctionBlockConfigurationDataProtocol getFunctionBlockConfigurations() {
        return new FunctionBlockConfigurationData(this.data.optJSONObject("ftn_block"));
    }
}
