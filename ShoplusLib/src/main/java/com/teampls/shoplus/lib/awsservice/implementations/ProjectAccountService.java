package com.teampls.shoplus.lib.awsservice.implementations;

import android.util.Log;

import com.teampls.shoplus.lib.awsservice.ProjectAccountServiceProtocol;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.datatypes.AccountChangeLogData;
import com.teampls.shoplus.lib.datatypes.AccountsData;
import com.teampls.shoplus.lib.datatypes.AccountsReceivableChangeLogData;
import com.teampls.shoplus.lib.enums.AccountChangeUpdateOperationType;
import com.teampls.shoplus.lib.enums.AccountType;
import com.teampls.shoplus.lib.enums.UserConfigurationType;
import com.teampls.shoplus.lib.protocol.AccountChangeLogDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;
import com.teampls.shoplus.undercity.datatypes.LogisticsWarehousingSummaryData;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 미송앱 관련 AWS 서비스 API 호출 구현체
 *
 * @author lucidite
 */

public class ProjectAccountService implements ProjectAccountServiceProtocol {

    private String getAnAccountsResourcePath(String phoneNumber, String counterpart) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "accounts", counterpart);
    }

    private String getAnAccountTypeResourcePath(String phoneNumber, String counterpart, AccountType accountType) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "accounts", counterpart, accountType.toRemoteStr());
    }

    private String getAccountsSituationPath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "accounts", "situation");
    }

    private String getAccountsReceivableHistoryResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "accounts", "history");
    }

    private String getSpecificAccountHistoryResourcePath(String phoneNumber, String counterpart, AccountType accountType) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "accounts", "history", counterpart, accountType.toRemoteStr());
    }

    private String getAnAccountChangeLogResourcePath(String phoneNumber, AccountType accountType, String logId) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "accounts", "history", "types", accountType.toRemoteStr(), logId);
    }

    private String getPhoneNumberForAccounts(UserShopInfoProtocol userShop) {
        return userShop.getMyShopOrUserPhoneNumber();     // Accounts 정보에서는 메인 샵 정보를 사용하지 않는다.
    }

    @Override
    public AccountsData getAccounts(UserShopInfoProtocol userShop, UserConfigurationType myUserType, String counterpart) throws MyServiceFailureException {
        try {
            String resource = this.getAnAccountsResourcePath(getPhoneNumberForAccounts(userShop), counterpart);
            String response = APIGatewayClient.requestGET(resource, myUserType.buildQueryParams(), UserHelper.getIdentityToken());
            return new AccountsData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public Map<String, AccountsData> getAccountsSituation(UserShopInfoProtocol userShop) throws MyServiceFailureException {
        Map<String, AccountsData> result = new HashMap<>();

        try {
            String resource = this.getAccountsSituationPath(getPhoneNumberForAccounts(userShop));
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());
            JSONArray accountsArr = new JSONArray(response);
            for (int i = 0; i < accountsArr.length(); ++i) {
                JSONObject accountsObj = accountsArr.getJSONObject(i);
                result.put(accountsObj.getString("counterpart"), new AccountsData(accountsObj));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    private AccountsData updateAccountAmount(UserShopInfoProtocol userShop, String counterpart, String operation, AccountType accountType, int amount, DateTime userSpecifiedCreatedDateTime) throws MyServiceFailureException {
        try {
            String resource = this.getAnAccountTypeResourcePath(getPhoneNumberForAccounts(userShop), counterpart, accountType);
            JSONObject requestBody = new JSONObject();
            requestBody.put("operation", operation);
            requestBody.put("amount", amount);
            if (userSpecifiedCreatedDateTime != null) {
                requestBody.put("created", APIGatewayHelper.getRemoteDateTimeString(userSpecifiedCreatedDateTime));
            }

            String response = APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
            return new AccountsData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public AccountsData changeAccountAmount(UserShopInfoProtocol userShop, String counterpart, AccountType accountType, int changeAmount, DateTime userSpecifiedCreatedDateTime) throws MyServiceFailureException {
        return this.updateAccountAmount(userShop, counterpart, "change", accountType, changeAmount, userSpecifiedCreatedDateTime);
    }

    @Override
    public AccountsData transferAccountAmount(UserShopInfoProtocol userShop, String counterpart, AccountType sourceAccountType, int transferAmount) throws MyServiceFailureException {
        return this.updateAccountAmount(userShop, counterpart, "transfer", sourceAccountType, transferAmount, null);
    }

    @Override
    public List<AccountChangeLogDataProtocol> getAccountChangeHistory(UserShopInfoProtocol userShop, UserConfigurationType myUserType, String counterpart, AccountType accountType) throws MyServiceFailureException {
        try {
            String resource = this.getSpecificAccountHistoryResourcePath(getPhoneNumberForAccounts(userShop), counterpart, accountType);
            String response = APIGatewayClient.requestGET(resource, myUserType.buildQueryParams(), UserHelper.getIdentityToken());

            List<AccountChangeLogDataProtocol> result = new ArrayList<>();
            JSONArray logArr = new JSONArray(response);
            for (int i = 0; i < logArr.length(); ++i) {
                result.add(new AccountChangeLogData(logArr.getJSONObject(i)));
            }
            Collections.sort(result, AccountsReceivableChangeLogData.comparator);
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public AccountsReceivableChangeLogData getWeeklyAccountsReceivableChangeHistory(UserShopInfoProtocol userShop, UserConfigurationType myUserType, DateTime startDateOfWeek) throws MyServiceFailureException {
        try {
            String resource = this.getAccountsReceivableHistoryResourcePath(getPhoneNumberForAccounts(userShop));
            Map<String, String> params = myUserType.buildQueryParams();
            params.put("week_from", APIGatewayHelper.getRemoteDateString(startDateOfWeek));
            String response = APIGatewayClient.requestGET(resource, params, UserHelper.getIdentityToken());
            return new AccountsReceivableChangeLogData(new JSONObject(response));
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

    @Override
    public AccountChangeLogDataProtocol updateAccountChange(UserShopInfoProtocol userShop, AccountType accountType, String changeLogId, AccountChangeUpdateOperationType operation, Boolean isCrossingAccounts) throws MyServiceFailureException {
        String response = "";
        try {
            String resource = this.getAnAccountChangeLogResourcePath(getPhoneNumberForAccounts(userShop), accountType, changeLogId);
            JSONObject requestBody = new JSONObject();
            requestBody.put("operation", operation.toRemoteStr());
            if ((operation == AccountChangeUpdateOperationType.Clearance) && isCrossingAccounts) {
                requestBody.put("crossing", true);
            }
            response = APIGatewayClient.requestPUT(resource, requestBody.toString(), UserHelper.getIdentityToken());
            return new AccountChangeLogData(new JSONObject(response));
        } catch (JSONException e) {
            Log.e("DEBUG_JS", String.format("[ProjectTransactionService.updateAccountChange] response : %s", response));
            throw new MyServiceFailureException(e);
        }
    }

    private String getMyEntrustedChangeHistoryResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "accounts", "history", "types", "entrusted");
    }

    @Override
    public List<AccountChangeLogDataProtocol> downloadEntrustedChangeHistory(UserShopInfoProtocol userShop, UserConfigurationType myUserType, DateTime from, DateTime to) throws MyServiceFailureException {
        try {
            String resource = this.getMyEntrustedChangeHistoryResourcePath(getPhoneNumberForAccounts(userShop));

            Map<String, String> querystringParams = myUserType.buildQueryParams();
            querystringParams.put("from", APIGatewayHelper.getRemoteDateTimeString(from));
            querystringParams.put("to", APIGatewayHelper.getRemoteDateTimeString(to));

            // Log.i("DEBUG_JS", String.format("[ProjectExodarService.downloadEntrustedChangeHistory] querystringParams : %s", querystringParams));

            String response = APIGatewayClient.requestGET(resource, querystringParams, UserHelper.getIdentityToken());

            List<AccountChangeLogDataProtocol> result = new ArrayList<>();
            JSONArray dataArr = new JSONArray(response);
            for (int i = 0; i < dataArr.length(); ++i) {
                result.add(new AccountChangeLogData(dataArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new MyServiceFailureException(e);
        }
    }

}
