package com.teampls.shoplus.lib.database_global;

import android.util.Log;

/**
 * Created by Medivh on 2016-09-13.
 */
public class SlipImagePathRecord {
    public int id = 0;
    public String slipKey = "", imagePath = "";

    public SlipImagePathRecord(){};

    public SlipImagePathRecord(int id, String slipKey, String imagePath) {
        this.id = id;
        this.slipKey = slipKey;
        this.imagePath = imagePath;
    }

    public SlipImagePathRecord(String slipKey, String imagePath) {
        this.slipKey = slipKey;
        this.imagePath = imagePath;
    }

    public boolean isSame(SlipImagePathRecord record) {
        if (slipKey.equals(record.slipKey) == false) return false;
        if (imagePath.equals(record.imagePath) == false) return false;
        return true;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s]  <%d> %s, %s", location, id, slipKey, imagePath));
    }
}
