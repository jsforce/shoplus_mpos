package com.teampls.shoplus.lib.awsservice;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;

/**
 * @author lucidite
 */

public interface ProjectAdminServiceProtocol {
    /**
     * 특정 사용자에게 특정 서비스의 사용 권한을 할당한다.
     *
     * @param admin
     * @param targetUser
     * @param service
     * @throws MyServiceFailureException
     */
    void activate(String admin, String targetUser, ShoplusServiceType service) throws MyServiceFailureException;
}
