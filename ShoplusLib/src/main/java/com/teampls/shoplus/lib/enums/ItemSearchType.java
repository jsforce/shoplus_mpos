package com.teampls.shoplus.lib.enums;

import com.teampls.shoplus.lib.database_map.KeyValueDB;

/**
 * Created by Medivh on 2018-07-29.
 */

public enum ItemSearchType {
    ByName, BySerialNum;

    public static ItemSearchType getFrom(KeyValueDB keyValueDB, String key) {
        try {
            return ItemSearchType.valueOf(keyValueDB.getValue(key, ItemSearchType.ByName.toString()));
        } catch (IllegalArgumentException e) {
            return ItemSearchType.ByName;
        }
    }
}
