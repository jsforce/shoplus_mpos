package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.enums.AccountChangeOperationType;

import org.joda.time.DateTime;

/**
 * 잔금 및 매입 변경 이력 데이터 클래스 프로토콜
 *
 * @author lucidite
 */

public interface AccountChangeLogDataProtocol {
    /**
     * 변경 로그 ID.
     * 거래기록에 의해 추가된 경우 거래기록의 ID와 일치하며, 그 외의 경우 생성시간 데이터
     *
     * @return
     */
    String getLogId();

    DateTime getCreatedDateTime();

    /**
     * 정렬에 사용하기 위한 데이터.
     * 거래기록에 의해 파생된 경우 CreatedDateTime이 실제 생성 시간을 정확하게 반영하지 못할 수 있다.
     * @return
     */
    int getSortKey();

    String getProvider();
    String getBuyer();

    /**
     * 계정 변경 금액
     *
     * @return
     */
    int getChangeAmount();

    /**
     * 로그 작성 시점의 계정 금액
     *
     * @return
     */
    int getCurrentAmount();

    /**
     * 정보성 데이터로, 해당 로그를 사용자가 clear 처리했는지를 표시한다.
     *
     * @return
     */
    Boolean isCleared();

    /**
     * 정보성 데이터로, 해당 로그의 거래기록이 취소되었는지를 표시한다.
     *
     * @return
     */
    Boolean isCancelled();

    /**
     * 정보성 데이터로, 해당 로그가 완료처리된 경우 대납-온라인이 교차 처리되었는지를 표시한다.
     * 대납/온라인이 아니거나 완료되지 않은 경우 반환값은 기본적으로 의미를 가지지 않으며, false를 반환하도록 한다.
     *
     * @return
     */
    Boolean isCrossingAccounts();

    /**
     * 현금흐름과 관계있는 변경 로그인지를 반환한다(결산관련).
     *
     * @return
     */
    Boolean isRelatedToCashFlow();

    /**
     * 해당 로그를 사용자가 clear 처리/restore 처리한 경우, 또는 해당 로그의 연계 거래기록을 취소한 경우 최종 업데이트 시간을 표시한다.
     * 이외의 경우에는 생성일을 반환한다.
     *
     * @return
     */
    DateTime getUpdatedDateTime();

    /**
     * 변경 연산 종류를 반환한다.
     *
     * @return
     */
    AccountChangeOperationType getOperationType();
}
