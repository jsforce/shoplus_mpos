package com.teampls.shoplus.lib.database_old;

import android.util.Log;

import com.teampls.shoplus.lib.database.BaseRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.enums.TransType;

/**
 * Created by Medivh on 2017-10-18.
 */

public class LocalSlipRecord {
    public int id = 0, receivable = 0;
    public String slipKey = "", memo = "";
    public TransType transType = TransType.receipt;

    public LocalSlipRecord() {};

    public LocalSlipRecord(int id, String slipKey, String memo, int receivable, TransType transType) {
        this.id = id;
        this.slipKey = slipKey;
        this.memo = memo;
        this.receivable = receivable;
        this.transType = transType;
    }

    public LocalSlipRecord(SlipRecord slipRecord, String memo) {
        this.slipKey = slipRecord.getSlipKey().toSID();
        this.memo = memo;
    }

    public boolean isSame(LocalSlipRecord record) {
        return BaseRecord.isSame(this, record);
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %s, %s, %d", callLocation, slipKey, memo, receivable));
    }
}
