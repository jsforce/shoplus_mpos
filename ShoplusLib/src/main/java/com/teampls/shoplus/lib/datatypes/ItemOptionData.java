package com.teampls.shoplus.lib.datatypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 상품 옵션(색상/사이즈) 상태 및 재고 데이터
 * [STORM-156] 옵션별 판매완료 상태를 추가하기 위해 신설
 *
 * @author lucidite
 */
public class ItemOptionData {
    private int stock;
    private boolean isSoldOut;

    public ItemOptionData() {
        stock = 0;
        isSoldOut = false;
    }

    public ItemOptionData(int stock, boolean isSoldOut) {
        this.stock = stock;
        this.isSoldOut = isSoldOut;
    }

    public int getStock() {
        return stock;
    }

    public boolean isSoldOut() {
        return isSoldOut;
    }

    public static Map<ItemStockKey, ItemOptionData> buildStockFromStockDataArr(JSONArray stockArr) throws JSONException  {
        Map<ItemStockKey, ItemOptionData> result = new HashMap<>();
        if (stockArr == null) {
            return result;
        }
        for (int i = 0; i < stockArr.length(); ++i) {
            JSONObject stockObj = stockArr.getJSONObject(i);
            ItemStockKey key = ItemDataBundle.buildItemStockKey(stockObj);
            int remains = stockObj.getInt("remains");
            boolean isSoldOut = (stockObj.optInt("soldout", 0) != 0);
            ItemOptionData optionData = new ItemOptionData(remains, isSoldOut) ;
            result.put(key, optionData);
        }
        return result;
    }
}
