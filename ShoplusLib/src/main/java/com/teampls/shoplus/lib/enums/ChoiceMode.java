package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2019-04-26.
 */

public enum ChoiceMode {
    Single("한개씩 처리"), Multiple("한꺼번에 처리");

    public String uiStr = "";
    ChoiceMode(String uiStr) {
        this.uiStr = uiStr;
    }

    public ChoiceMode getOtherMode() {
        switch (this) {
            default:
            case Single:
                return Multiple;
            case Multiple:
                return Single;
        }
    }

    public static ChoiceMode valueOfDefault(String name) {
        try {
            return valueOf(name);
        } catch (Exception e) {
            return Single;
        }
    }

}
