package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.MyStringAdapter;
import com.teampls.shoplus.lib.view_module.BaseTextWatcher;
import com.teampls.shoplus.lib.common.MyDevice;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-03-15.
 */
abstract public class SearchStringDialog extends BaseDialog implements AdapterView.OnItemClickListener {
    private MyStringAdapter adapter;
    private ListView listView;
    private EditText etName;
    private List<String> originalStrings = new ArrayList<>();

    public SearchStringDialog(Context context, String title, List<String> strings) {
        super(context);
        setDialogSize(R.id.dialog_string_search_container);
        for (String string : strings)
            originalStrings.add(string);
        adapter = new MyStringAdapter(context);
        adapter.setRecords(strings);
        listView = (ListView) findViewById(R.id.dialog_string_search_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        etName = (EditText) findViewById(R.id.dialog_string_search_edittext);
        etName.addTextChangedListener(new MyTextWatcher());

        findTextViewById(R.id.dialog_string_search_title, title);
        setOnClick(R.id.dialog_string_search_close, R.id.dialog_string_search_clear,
                R.id.dialog_string_search_add);
        setVisibility(View.GONE, R.id.dialog_string_search_edit_container);
        refresh();
        show();
        //  MyDevice.showKeyboard(context, etName);
    }

    private void refresh() {
        if (adapter == null) return;
        adapter.generate();
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_string_search;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_string_search_close) {
            MyDevice.hideKeyboard(context, etName);
            dismiss();

        } else if (view.getId() == R.id.dialog_string_search_clear) {
            etName.setText("");

        } else if (view.getId() == R.id.dialog_string_search_add) {
            new UpdateValueDialog(context, "직접 입력", "건물 주소를 직접 입력해주세요","") {
                @Override
                public void onApplyClick(String newValue) {
                    onItemClick(newValue);
                    doDismiss();
                }
            };
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        onItemClick(adapter.getRecord(position));
        MyDevice.hideKeyboard(context, etName);
        dismiss();
    }

    abstract public void onItemClick(String string);

    class MyTextWatcher extends BaseTextWatcher {
        public boolean doStopWorking = false;

        @Override
        public void afterTextChanged(Editable s) {
            if (doStopWorking)
                return;
            List<String> results = new ArrayList<>();
            for (String string : originalStrings)
                if (string.toLowerCase().contains(s.toString().toLowerCase()))
                    results.add(string);
            adapter.setRecords(results);
            refresh();
        }
    }

}


