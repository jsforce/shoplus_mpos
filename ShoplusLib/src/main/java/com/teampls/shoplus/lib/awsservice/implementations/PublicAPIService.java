package com.teampls.shoplus.lib.awsservice.implementations;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.apigateway.PublicAPIClient;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.AppVersion;

import org.json.JSONException;

/**
 *
 * Public 정보 관련 서비스. 버전, 공지 등의 정보를 다운로드한다.
 * @author lucidite
 */

public class PublicAPIService {
    private static String getResourcePath(String resourceRelativePath) {
        return MyAWSConfigs.getCurrentConfig().getPublicApiRootUrl() + resourceRelativePath;
    }

    /**
     * Shop+Book 앱의 버전 정보를 받아온다 (Project Dalaran).
     * 포맷이 잘못된 경우, 또는 서비스 실패가 발생한 경우(통신 문제 등) 빈 객체를 반환한다. (optional information)
     * AppVersion.isValid() 메서드를 이용하여 정보를 제대로 받았는지 확인할 수 있다.
     *
     * @return 앱의 최신 버전(latest) 및 요구 버전(required)
     */
    public AppVersion getBookAppVersion() {
        // 포맷이 잘못된 경우, 또는 서비스 실패가 발생한 경우(통신 문제 등) 빈 객체를 반환한다.
        try {
            String resource = getResourcePath("/book/version/android");
            String response = PublicAPIClient.request(resource, MyAWSConfigs.getCurrentConfig().getPublicApiKey());
            return new AppVersion(response);
        } catch (JSONException | MyServiceFailureException e) {
            e.printStackTrace();
            return new AppVersion();    // invalid info
        }
    }
}
