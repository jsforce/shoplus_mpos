package com.teampls.shoplus.lib.datatypes;

import android.app.Service;
import android.support.annotation.Nullable;
import android.util.Log;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;
import com.teampls.shoplus.lib.common.Empty;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 서비스 활성화 관련 데이터 클래스
 *
 * @author lucidite
 */

public class ServiceActivationData {
    private DateTime expired;
    private boolean demo;

    public ServiceActivationData(){
        expired = Empty.dateTime;
        demo = false;
    };

    public ServiceActivationData(JSONObject dataObj) throws JSONException {
        Object expiredObj = dataObj.opt("expired");
        if (expiredObj != null) {
            if (expiredObj.equals(JSONObject.NULL)) {
                this.expired = null;
            } else {
                String expiredDateTimeStr = dataObj.optString("expired", "");
                if (expiredDateTimeStr.isEmpty()) {
                    this.expired = null;
                } else {
                    this.expired = APIGatewayHelper.getDateTimeFromRDSString(expiredDateTimeStr);
                }
            }
        } else {
            this.expired = null;
        }
        this.demo = dataObj.optBoolean("demo", false);
    }

    /**
     * 현재 해당 서비스가 활성화되어 있는지를 반환한다.
     * 만료 기일이 정해져 있지 않거나 만료일자가 도래하지 않은 경우 참을 반환한다.
     *
     * @return
     */
    public Boolean isCurrentlyActivated() {
        if (this.expired != null) {
            return this.expired.isAfterNow();
        } else {
            return true;        // 만료일시 미지정
        }
    }

    /**
     * 만료일시를 반환한다.
     * 만료일시가 지정되지 않은 경우 NULL을 반환함에 유의한다.
     *
     * @return
     */
    @Nullable
    public DateTime getExpiration() {
        if (expired == null) {
            return Empty.dateTime;
        } else {
            return this.expired;
        }
    }

    /**
     * 데모 서비스 여부를 반환한다. 챗봇 서비스 등에서 데모 플래그 설정을 가진다.
     * @return
     */
    public boolean isDemo() {
        return demo;
    }
}
