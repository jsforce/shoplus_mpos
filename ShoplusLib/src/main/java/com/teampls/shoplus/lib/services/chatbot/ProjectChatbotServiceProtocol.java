package com.teampls.shoplus.lib.services.chatbot;

import android.util.Pair;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotSettingData;
import com.teampls.shoplus.lib.services.chatbot.datatypes.NonProcessedUtteranceData;
import com.teampls.shoplus.lib.services.chatbot.enums.OrderRejectReason;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotBuyerBaseDataProtocol;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotBuyerRegistrationRequestDataProtocol;
import com.teampls.shoplus.lib.services.chatbot.protocol.ChatbotOrderDataProtocol;

import java.util.List;

/**
 * @author lucidite
 */

public interface ProjectChatbotServiceProtocol {

    /**
     * (거래처에 포함되지 않은 고객의) 신규 거래처 신청 목록을 조회한다.
     *
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    List<ChatbotBuyerRegistrationRequestDataProtocol> getBuyerRegistrationRequests(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 특정 신규 거래처 신청 건을 수락한다.
     * [NOTE] 이름 및 도소매 여부를 설정하면 신규 거래처를 생성한 후 반환한다. (신청 내역은 삭제)
     *
     * @param userShop
     * @param request 수락할 거래처 신청 데이터
     * @param updatedBuyerName 거래처명, null 또는 빈 문자열을 전달하면 request에 있는 고객명을 그대로 사용한다.
     * @param isRetail 소매 고객으로 설정할 경우 true, 소매 기능을 사용하지 않거나 도매 고객인 경우 false를 전달한다.
     * @return 새로 생성된 거래처 정보를 반환한다. (local의 거래처 관리자 목록에 추가하여 거래처 정보를 사용하기 위한 용도)
     * @throws MyServiceFailureException
     */
    ContactDataProtocol acceptBuyerRegistrationRequests(
            UserShopInfoProtocol userShop, ChatbotBuyerRegistrationRequestDataProtocol request,
            String updatedBuyerName, boolean isRetail
    ) throws MyServiceFailureException;

    /**
     * 사용 요청 또는 사용 중인 이용고객 목록을 반환한다.
     *
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    List<ChatbotBuyerBaseDataProtocol> getAllBuyers(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 최근 주문내역을 조회한다.
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    List<ChatbotOrderDataProtocol> getRecentOrders(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 특정 주문의 상태를 처리중으로 변경한다.
     * [NOTE] 거래기록을 조회할 때 주문중(REQUEST) 상태를 처리중(IN_PROGRESS)으로 변경해야 한다.
     *        이때 변경이 실패하는 경우는 고객이 주문을 취소한 경우이며, 반환값을 통해 확인할 수 있다.
     * [NOTE] 매장에서 상태를 변경할 수 있는 케이스가 제한되어 있으며, 이외의 케이스는 다른 방식으로 변경할 수 있다.
     * - canMakeTransactionOrBeRejected가 참인 경우에는 주문을 거절(매장취소)할 수 있으나 별도로 함수를 제공한다.
     * - 거래기록으로 만들고 완료처리하는 것은 거래기록을 만들 때 주문 ID를 포함하면 자동으로 처리되므로,
     *   거래기록으로 만든 후 성공하면 local의 주문정보의 상태만 완료로 변경하면 된다.
     *
     * @param userShop
     * @param orderId
     * @return 상태가 변경되었을 경우 True, 변경 실패한 경우 False를 반환하며 변경 후의 주문데이터를 참조용으로 같이 반환한다.
     * @throws MyServiceFailureException
     */
    Pair<Boolean, ChatbotOrderDataProtocol> setOrderInProgress(UserShopInfoProtocol userShop, String orderId) throws MyServiceFailureException;

    /**
     * 특정 주문을 거절한다 (매장취소).
     * [NOTE] canMakeTransactionOrBeRejected가 참인 경우에만 매장취소할 수 있다.
     *        변경이 실패하는 경우는 고객이 주문을 취소했거나 이미 완료/취소된 경우이며, 반환값을 통해 확인할 수 있다.
     *
     * @param userShop
     * @param orderId
     * @param reason
     * @return 상태가 변경되었을 경우 True, 변경 실패한 경우 False를 반환하며 변경 후의 주문데이터를 참조용으로 같이 반환한다.
     * @throws MyServiceFailureException
     */
    Pair<Boolean, ChatbotOrderDataProtocol> rejectOrder(UserShopInfoProtocol userShop, String orderId, OrderRejectReason reason) throws MyServiceFailureException;

    /**
     * 현재 설정된 신상/매장추천 상품 목록을 조회한다.
     *
     * @param userShop
     * @return 신상/매장추천으로 설정된 상품의 ID 목록
     * @throws MyServiceFailureException
     */

    // ProjectStormwindService에 몰래 하나 복사함
    List<Integer> getNewArrivalsItemIds(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 새로운 신상/매장추천 상품 목록을 설정한다.
     *
     * @param userShop
     * @param itemids
     * @throws MyServiceFailureException
     */

    // ProjectStormwindService에 몰래 하나 복사함
    void updateNewArrivalItemIds(UserShopInfoProtocol userShop, List<Integer> itemids) throws MyServiceFailureException;

    //////////////////////////////////////////////////////////////////////
    // [ORGR-79] 자동 처리되지 않은 발화 관리 기능

    /**
     * 챗봇에서 자동 처리하지 못한 최근 발화 목록을 조회한다.
     *
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    List<NonProcessedUtteranceData> getRecentNonProcessedUtterances(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 특정 미처리 발화를 완료 처리한다.
     * [NOTE] 이미 완료 처리된 발화 데이터도 다시 완료 처리할 수 있다. (노트 업데이트)
     *
     * @param userShop
     * @param utteranceId 완료처리할 발화 ID
     * @param note 완료 처리 노트 (메모용 또는 고객 전송용), 빈 문자열을 전달하여 노트 없이 완료처리할 수 있다.
     * @return
     * @throws MyServiceFailureException
     */
    NonProcessedUtteranceData processNonProcessedUtterance(UserShopInfoProtocol userShop, String utteranceId, String note) throws MyServiceFailureException;

    //////////////////////////////////////////////////////////////////////
    // Chatbot 동작 모드 설정 관련

    /**
     * 챗봇 동작 모드 설정을 조회한다.
     *
     * @param userShop
     * @return
     * @throws MyServiceFailureException
     */
    ChatbotSettingData getChatbotSetting(UserShopInfoProtocol userShop) throws MyServiceFailureException;

    /**
     * 챗봇 동작 모드를 업데이트한다.
     *
     * @param userShop
     * @param setting
     * @return 업데이트된 챗봇 동작 모드를 반환한다. 정상적으로 업데이트된 경우 파라미터에 전달한 값과 동일해야 한다.
     * @throws MyServiceFailureException
     */
    ChatbotSettingData updateChatbotSetting(UserShopInfoProtocol userShop, ChatbotSettingData setting) throws MyServiceFailureException;
}
