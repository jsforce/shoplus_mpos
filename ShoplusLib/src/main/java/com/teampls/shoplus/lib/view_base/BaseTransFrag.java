package com.teampls.shoplus.lib.view_base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.MyDateTimeNavigator;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;

import org.joda.time.DateTime;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Medivh on 2017-12-10.
 */

abstract public class BaseTransFrag extends BaseFragment implements AdapterView.OnItemClickListener {
    protected int thisView = R.layout.base_slips;
    protected static int tabIndex = 0;
    protected String title = "거래목록";
    protected ListView listView;
    protected TextView tvSummary, tvNotice;
    protected MyDateTimeNavigator dateTimeNavigator;
    protected Set<DateTime> downloadedDateTimes = new HashSet<>();
    protected MyEmptyGuide myEmptyGuide;
    protected LinearLayout dateHeader, counterpartHeader;
    private BaseDBAdapter baseAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseAdapter = setAdapter();
    }

    abstract public BaseDBAdapter setAdapter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(thisView, container, false);
        listView = (ListView) view.findViewById(R.id.slips_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(baseAdapter);
        dateHeader = (LinearLayout) view.findViewById(R.id.slips_datetime_header);
        counterpartHeader = (LinearLayout) view.findViewById(R.id.slips_counterpart_header);
        counterpartHeader.setVisibility(View.GONE);

        tvSummary = (TextView) view.findViewById(R.id.slips_summary);
        tvNotice = (TextView) view.findViewById(R.id.slips_notice);

        MyView.setTextViewByDeviceSize(context, tvSummary, tvNotice);

        downloadedDateTimes.clear();

        setOnClick(view, R.id.slips_summaryByMonth);

        dateTimeNavigator = new MyDateTimeNavigator(context, BaseUtils.createDay(), view, R.id.slips_period_prev,
                R.id.slips_period_next);
        setDateTimeNavigator(view);

        myEmptyGuide = new MyEmptyGuide(context, view, baseAdapter, view.findViewById(R.id.slips_listview),
                R.id.slips_empty_container, R.id.slips_empty_message, R.id.slips_empty_contactUs);
        myEmptyGuide.setMessage("거래 기록이 없습니다.\n\n영수증이 발행되면\n이 페이지에 표시됩니다");
        myEmptyGuide.setContactUsButton("앱 문의하기", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
            }
        });

        setByUserConfigType();
        return view;
    }

    private void setDateTimeNavigator(View view) {
        dateTimeNavigator.setAsDayMode(view, R.id.slips_period_year, R.id.slips_period_month, R.id.slips_period_day);
        dateTimeNavigator.setOnApplyClick(new MyOnClick<DateTime>() {
            @Override
            public void onMyClick(View view, DateTime dateTime) {
                onTargetDateSelected(dateTime, null);
            }
        });
        dateTimeNavigator.setLeftCalendarButton(R.id.slips_period_calendar, new MyOnClick<DateTime>() {
            @Override
            public void onMyClick(View view, DateTime selectedDateTime) {
                onTargetDateSelected(selectedDateTime, null);
            }
        });
        dateTimeNavigator.setRightRecentButton(R.id.slips_period_recent, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRecentClick();
            }
        });
    }

    protected void setByUserConfigType() {

    }

    public void onRecentClick() {

    }

    public void clearCache() {
        downloadedDateTimes.clear();
    }


    public void onTargetDateSelected(DateTime dateTime, MyOnTask onTask) {
        dateTimeNavigator.setDateTime(dateTime);
        DateTime targetDateTime = dateTimeNavigator.getDateTime();
        if (downloadedDateTimes.contains(targetDateTime)) {
            setAdapterByDateTime(targetDateTime, targetDateTime.plusDays(1).minusMillis(1));
            refresh();
            if (onTask != null)
                onTask.onTaskDone("");
        } else {
            downloadTransactions(targetDateTime, onTask);
        }
    }

    abstract protected void setAdapterByDateTime(DateTime fromDateTime, DateTime toDateTime);

    abstract public void downloadTransactions(final DateTime fromDateTime, final DateTime toDateTime, final MyOnTask onTask);

    abstract public void downloadTransactions(final DateTime day, final MyOnTask onTask);

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public int getTabIndex() {
        return tabIndex;
    }

    public void onTargetCounterpartSelected(String phoneNumber) {
        if (baseAdapter == null) return;
        downloadCounterpartTransactions(phoneNumber);
    }

    abstract public void downloadCounterpartTransactions(final String phoneNumber);

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.slips_summaryByMonth) {
            onSummaryByMonthClick();
        }
    }

    abstract protected void onSummaryByMonthClick();

}
