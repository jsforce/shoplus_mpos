package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.text.InputType;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.view_module.MyCurrencyEditText;

/**
 * Created by Medivh on 2019-04-23.
 */

abstract public class UpdateCurrencyDialog extends BaseDialog implements View.OnKeyListener {
    private MyCurrencyEditText etCurrency;

    public UpdateCurrencyDialog(Context context, String title, String message, int number, int hint) {
        super(context);
        setDialogWidth(0.8, 0.6);
        findTextViewById(R.id.dialog_update_value_title, title);
        findTextViewById(R.id.dialog_update_value_message).setText(message);
        if (message.isEmpty())
            findTextViewById(R.id.dialog_update_value_message).setVisibility(View.GONE);
        etCurrency = new MyCurrencyEditText(context, getView(), R.id.dialog_update_value_edittext, R.id.dialog_update_value_edittext_clear);
        etCurrency.setOriginalValue(number);
        etCurrency.getEditText().setHint(Integer.toString(hint));
        etCurrency.getEditText().setGravity(Gravity.RIGHT);
        etCurrency.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
        etCurrency.getEditText().requestFocus();
        etCurrency.getEditText().setOnKeyListener(this);

        Button button = findButtonById(R.id.dialog_update_value_function);
        button.setVisibility(View.VISIBLE);
        button.setText("0으로");

        setOnClick(R.id.dialog_update_value_cancel, R.id.dialog_update_value_apply, R.id.dialog_update_value_function);
        MyDevice.showKeyboard(context, etCurrency.getEditText());
        show();

        new MyDelayTask(context, 100) {
            @Override
            public void onFinish() {
                if (etCurrency.getEditText().toString().length() > 0)
                    etCurrency.getEditText().setSelection(etCurrency.getEditText().getText().toString().length()); // 주의, etString에는 아직 글씨가 쓰여져있지 않다
            }
        };
    }

    abstract public void onApplyClick(int newValue);

    @Override
    public int getThisView() {
        return R.layout.dialog_update_value;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_ENTER:
                    onApplyClick(etCurrency.getValue());
                    MyDevice.hideKeyboard(context, etCurrency.getEditText());
                    dismiss();
                    return true;
                default:
                    break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_update_value_cancel) {
            MyDevice.hideKeyboard(context, etCurrency.getEditText());
            dismiss();
        } else if (view.getId() == R.id.dialog_update_value_apply) {
            onApplyClick(etCurrency.getValue());
            MyDevice.hideKeyboard(context, etCurrency.getEditText());
            dismiss();
        } else if (view.getId() == R.id.dialog_update_value_function) {
            onApplyClick(0);
            MyDevice.hideKeyboard(context, etCurrency.getEditText());
            dismiss();
        }
    }
}
