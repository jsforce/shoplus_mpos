package com.teampls.shoplus.lib.awsservice;

import android.util.Pair;

import com.teampls.shoplus.lib.datatypes.MyShopInformationData;
import com.teampls.shoplus.lib.datatypes.ServiceActivationResult;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.enums.MessengerType;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;
import com.teampls.shoplus.lib.protocol.UserSettingDataProtocol;
import com.teampls.shoplus.lib.protocol.UserShopInfoProtocol;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author lucidite
 */

public interface ProjectUserServiceProtocol {
    /**
     * 특정 서비스의 권한을 확인한다.
     *
     * @param userShop
     * @param service
     * @return 명시적 activated일 경우 True, 명시적 deactivated일 경우 False, 미정 상태인 경우 null을 반환한다
     * @throws MyServiceFailureException
     */
    @Deprecated
    Boolean checkServiceActivation(UserShopInfoProtocol userShop, ShoplusServiceType service) throws MyServiceFailureException;

    /**
     * 트라이얼 서비스 활성화를 요청한다. 서비스에 사전 설정된 기간만큼 서비스를 활성화한다.
     * 연락을 허락하는지를 파라미터로 받는다.
     * * 트라이얼인 경우 기존에 서비스 활성화 관련 기록이 있는 경우에는 활성화하지 않는다 (신규유저에게만 허용).
     *
     * @param userShop
     * @param service
     * @param allowContact  사용자 연락 허락 여부 (optional)
     * @param demoRequested 방문데모 요청 여부 (optional)
     * @return
     * @throws MyServiceFailureException
     */
    ServiceActivationResult requestTempActivation(UserShopInfoProtocol userShop, ShoplusServiceType service, Boolean allowContact, Boolean demoRequested) throws MyServiceFailureException;

    /**
     * 리딤코드로 서비스 활성화를 요청한다. 서비스에 사전 설정된 기간만큼 서비스를 활성화한다.
     * * 리딤코드를 사용하는 경우 기존에 리딤코드를 사용하지 않은 사용자만 활성화한다.
     *
     * @param userShop
     * @param service
     * @param shopName 가게 이름을 전달하여 설정할 수 있다. 기존에 설정된 매장명이 있는 경우 업데이트한다.
     * @return
     * @throws MyServiceFailureException
     */
    ServiceActivationResult activateWithRedeemCode(UserShopInfoProtocol userShop, ShoplusServiceType service, String redeemCode, String shopName) throws MyServiceFailureException;

    /**
     * mPOS 사용자를 등록한다. 기존에 강제 만료된 사용자는 활성화할 수 없다.
     * [NOTE] 추천인은 현재 샵플서비스를 사용하고 있는 사용자여야 한다 (만료 사용자 제외)
     * 추천인 정보가 없는 경우 안내 메시지만 전송하며, 서비스를 바로 활성화하지는 않는다.
     *
     * @param userShop
     * @param shopName   가게 이름을 전달하여 설정할 수 있다. 기존에 설정된 매장명이 있는 경우 업데이트한다. (필수)
     * @param shopInfo   가게 위치 정보(건물명, 호수)를 전달한다. (필수)
     * @param introducer 추천인 정보(추천인 ID 휴대전화번호), 없는 경우 빈 문자열 (이 경우 즉시 등록하지 않고 안내메시지 발송)
     * @return
     * @throws MyServiceFailureException
     */
    ServiceActivationResult activateMPOSService(UserShopInfoProtocol userShop, String shopName, String shopInfo, String introducer) throws MyServiceFailureException;

    /**
     * [SILVER-441] 기기에 저장된 LUT(Last Updated Timestamp) 이후에 업데이트된 연락처를 가져온다.
     * lastUpdatedTimestamp를 0 또는 null로 설정하면 전체 거래처를 가져온다.
     * <p>
     * [NOTE] 주소 등 거래처의 필수정보를 제외한 상세 정보는 받아오지 않음에 유의한다.
     * (현재는 주소가 변동된 경우에도 updated 타임스탬프가 갱신되기는 하나, 차후에 주소 변동의 경우에는 변동 기록에서 제외할 가능성도 있음)
     *
     * @param userShop
     * @param lastUpdatedTimestamp 로컬에 저장된 LUT, 전체를 다운로드할 경우에는 0
     * @return 새로운 업데이트 타임스탬프(NUT, New Updated Timestamp) 및 LUT 이후에 업데이트된 거래처 목록
     * @throws MyServiceFailureException
     */
    Pair<Long, List<ContactDataProtocol>> getContactUpdates(UserShopInfoProtocol userShop, long lastUpdatedTimestamp) throws MyServiceFailureException;

    /**
     * [SILVER-441] 특정 거래처의 정보를 받아온다.
     * getContactUpdates 메서드와 달리 주소 등의 상세 정보를 포함하여 다운로드한다.
     * - 찾는 거래처가 없는 경우 에러(Not Found)가 발생한다. (있는 거래처에 대해서 요청해야 한다)
     *
     * @param userShop
     * @param contactNumber
     * @return
     * @throws MyServiceFailureException 거래처가 없는 경우 예외 발생
     */
    ContactDataProtocol getSpecificContact(UserShopInfoProtocol userShop, String contactNumber) throws MyServiceFailureException;

    /**
     * 모르는 거래처의 기본 연락처 정보를 조회하여 내 연락처에 추가한다. 검색하고자 하는 거래처 전화번호를 파라미터로 전달한다.
     * 업데이트한 후의 내 전체 연락처 정보를 반환한다.
     * [주의] 이미 등록되어 있는 전화번호를 검색 요청하는 경우, 서버에 저장되어 있는 값으로 강제 업데이트한다.
     *
     * @param userShop
     * @param searchingPhoneNumber 검색하고자 하는 거래처 전화번호
     * @return
     * @throws MyServiceFailureException
     */
    Map<String, ContactDataProtocol> searchContactInfo(UserShopInfoProtocol userShop, Set<String> searchingPhoneNumber) throws MyServiceFailureException;

    /**
     * (거래처의) 연락처 정보를 업데이트한다. 거래처의 휴대전화 번호를 key로, 관련 정보를 value로 하는 맵 데이터를 업로드한다.
     * 기존에 등록된 휴대전화 번호의 정보는 새로운 정보로 업데이트된다.
     * 전화번호의 유효성 등 데이터의 유효성 체크는 별도로 실시하지 않는다.
     *
     * @param userShop
     * @param contactInfo
     * @throws MyServiceFailureException
     */
    void updateContactInfo(UserShopInfoProtocol userShop, Map<String, ContactDataProtocol> contactInfo) throws MyServiceFailureException;

    /**
     * 두 연락처를 하나로 합친다. 합쳐지는 연락처의 거래기록 및 미완거래 정보(최대 30일 이내)를 합치는 쪽의 연락처로 변경한다.
     * 합쳐지는 연락처 정보는 삭제된다.
     *
     * @param userShop
     * @param acquirer    합치는 연락처 (남아있음)
     * @param predecessor 합쳐지는 연락처 (데이터 이동 후 삭제됨)
     * @throws MyServiceFailureException
     */
    void mergeContacts(UserShopInfoProtocol userShop, String acquirer, String predecessor) throws MyServiceFailureException;

    /**
     * 내 가게 정보를 업데이트한다.
     *
     * @param userShop
     * @param shopInformation
     * @return
     * @throws MyServiceFailureException
     */
    UserSettingDataProtocol updateMyShopInformation(UserShopInfoProtocol userShop, MyShopInformationData shopInformation) throws MyServiceFailureException;

    /**
     * 메신저 정보를 업데이트한다. null 또는 빈 문자열을 넣으면 삭제한다.
     * [NOTE] 카카오톡과 플러스친구는 둘 중 하나만 등록될 수 있다. 하나를 등록하면 다른 하나가 삭제된다.
     *
     * @param userShop
     * @param messengerType 메신저 타입
     * @param url           (http://pf.kakao.com/_pftest, https://u.wechat.com/wechatkeytest 등), 삭제하려는 경우 null 또는 빈 문자열
     * @return 업데이트된 메신저 연결 정보
     * @throws MyServiceFailureException
     */
    void updateMessengerLink(UserShopInfoProtocol userShop, MessengerType messengerType, String url) throws MyServiceFailureException;
}
