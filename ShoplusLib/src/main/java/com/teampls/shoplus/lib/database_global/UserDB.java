package com.teampls.shoplus.lib.database_global;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.util.Pair;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.database_map.GlobalDBLong;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.lib.protocol.ContactDataProtocol;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2016-10-01.
 */
// Global 계열은 업그레이드시 구앱과 충돌문제가 있으므로 새로 만들자 -- 새이름
public class UserDB extends BaseDB<UserRecord> {
    private static int Ver = 1;
    private static UserDB instance = null;
    private static GlobalDBLong latestTimeStamp = new GlobalDBLong("userDB.timestamp", (long) -1);

    public enum Column {
        _id, name, phoneNumber, priceType, location, deleted, timestamp;

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }
    }

    public static UserDB getInstance(Context context) {
        if (instance == null)
            instance = new UserDB(context);
        return instance;
    }

    private UserDB(Context context) {
        super(context, "UserDB", Ver, Column.toStrs());
    }

    public UserDB(Context context, String DBname, int DBversion, String[] columns) {
        super(context, DBname, DBversion, columns);
    }

    @Override
    protected int getId(UserRecord record) {
        return record.id;
    }

    @Override
    protected UserRecord getEmptyRecord() {
        return new UserRecord();
    }

    @Override
    protected UserRecord createRecord(Cursor cursor) {
        return new UserRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.phoneNumber.ordinal()),
                cursor.getString(Column.name.ordinal()),
                ContactBuyerType.valueOfWithDefault(cursor.getString(Column.priceType.ordinal())),
                cursor.getString(Column.location.ordinal()),
                BaseUtils.toBoolean(cursor.getInt(Column.deleted.ordinal())),
                cursor.getLong(Column.timestamp.ordinal())
        );
    }

    @Override
    protected ContentValues toContentvalues(UserRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.phoneNumber.toString(), record.phoneNumber);
        contentvalues.put(Column.name.toString(), record.name);
        contentvalues.put(Column.priceType.toString(), record.priceType.toString());
        contentvalues.put(Column.location.toString(), record.location);
        contentvalues.put(Column.deleted.toString(), record.deleted);
        contentvalues.put(Column.timestamp.toString(), record.timeStamp);
        return contentvalues;
    }

    public DBHelper getDBHelper() {
        return new DBHelper(context, getTeamPlPath());
    }

    public boolean has(String phoneNumber) {
        return hasValue(Column.phoneNumber, phoneNumber);
    }

    public UserRecord getRecord(String phoneNumber) {
        UserRecord result = getRecord(Column.phoneNumber, phoneNumber);
        return result;
    }

    public UserRecord getRecordWithNumber(String phoneNumber) {
        UserRecord result = getRecord(Column.phoneNumber, phoneNumber);
        if (result.phoneNumber.isEmpty())
            result.phoneNumber = phoneNumber;
        return result;
    }

    @Deprecated
    public boolean isUpdateFound() {
        return false;
    }

    public MyMap<String, UserRecord> getMap() { // phoneNumber - name
        MyMap<String, UserRecord> results = new MyMap<>(new UserRecord());
        for (UserRecord userRecord : getRecords())
            results.put(userRecord.phoneNumber, userRecord);
        return results;
    }

    @Override
    public void clear() {
        super.clear();
        resetTimeStamp();
    }

    public void resetTimeStamp() {
        latestTimeStamp.put(context, (long) -1);
    }

    public Long getLatestTimeStamp() {
        return latestTimeStamp.get(context); // BaseUtils.getMaxLong(getLongs(Column.timestamp));
    }

    public void update(Map<String, ContactDataProtocol> protocolMap) {
        for (String phoneNumber : protocolMap.keySet()) {
            updateOrInsert(new UserRecord(protocolMap.get(phoneNumber)));
        }
    }

    public boolean update(Pair<Long, List<ContactDataProtocol>> protocols) {
        boolean updated = false;
        latestTimeStamp.put(context, protocols.first);
        boolean isEmpty = this.isEmpty();
        for (ContactDataProtocol protocol : protocols.second) {
            if (protocol.getContactNumber().isEmpty()) continue;
            UserRecord userRecord = new UserRecord(protocol);
            if (userRecord.isDeleted()) {
                delete(userRecord.phoneNumber);
                Log.w("DEBUG_JS", String.format("[UserDB.update] %s (%s) is deleted...", protocol.getName(), userRecord.phoneNumber));
            } else {
                DBResult result;
                if (isEmpty)
                    result = insert(userRecord);
                else
                    result = updateOrInsert(userRecord);
                switch (result.resultType) {
                    case INSERTED:
                        updated = true;
                        Log.w("DEBUG_JS", String.format("[UserDB.update] %s (%s) is inserted...", protocol.getName(), userRecord.phoneNumber));
                        break;
                    case UPDATED:
                        updated = true;
                        Log.w("DEBUG_JS", String.format("[UserDB.update] %s (%s) is updated...", protocol.getName(), userRecord.phoneNumber));
                        break;
                }
            }
        }
        return updated;
    }

    public MyMap<String, String> getNameMap() { // phoneNumber - name
        cleanUp();
        MyMap<String, String> results = new MyMap("");
        for (UserRecord record : getRecords())
            results.put(record.phoneNumber, record.getName());
        return results;
    }

    public MyMap<String, String> getNameMap(List<String> phoneNumbers) { // phoneNumber - name
        cleanUp();
        MyMap<String, String> results = new MyMap<>("");
        phoneNumbers = new ArrayList(new HashSet(phoneNumbers));
        phoneNumbers.remove("");
        for (UserRecord userRecord : getRecordsIN(Column.phoneNumber, phoneNumbers, null))
            results.put(userRecord.phoneNumber, userRecord.getName());
        return results;
    }

    public void cleanUp() {
        List<Integer> idsForDeletion = new ArrayList<>();
        for (UserRecord record : getRecords())
            if (record.isEmpty())
                idsForDeletion.add(record.id);
        deleteAll(Column._id, idsForDeletion);
    }

    public void deleteInvalid(List<String> validPhoneNumbers) {
        deleteInvalid(validPhoneNumbers, false);
    }

    public void deleteInvalid(List<String> validPhoneNumbers, boolean doKeepEmptyPhoneNumber) {
        if (doKeepEmptyPhoneNumber)
            validPhoneNumbers.add("");
        List<String> invalidsPhoneNumbers = getStrings(Column.phoneNumber);
        invalidsPhoneNumbers.removeAll(validPhoneNumbers);
        deleteAll(Column.phoneNumber, invalidsPhoneNumbers);
    }

    public void delete(String phoneNumber) {
        UserRecord record = getRecord(phoneNumber);
        if (record.id > 0)
            delete(record.id);
    }

    protected UserRecord getRecordByKey(UserRecord record) {
        return getRecord(record.phoneNumber);
    }

    public List<UserRecord> getRecordsBy(List<String> phoneNumbers) {
        return getRecordsIN(Column.phoneNumber, phoneNumbers, null);
    }

}
