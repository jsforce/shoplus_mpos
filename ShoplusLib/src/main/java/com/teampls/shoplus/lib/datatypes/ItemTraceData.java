package com.teampls.shoplus.lib.datatypes;

import android.util.Log;

import com.teampls.shoplus.lib.enums.ItemTraceState;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 아이템 추적 데이터 클래스
 *
 * @author lucidite
 */

public class ItemTraceData {
    private ItemTraceIdData traceIdData;
    private ItemTraceState state;
    private String locationOrBuyer;

    public ItemTraceData() {};

    public ItemTraceData(JSONObject data) throws JSONException {
        try {
            int itemid = data.getInt("itemid");
            String traceValue = data.getString("tc_id");
            this.traceIdData = ItemTraceIdData.convert(itemid, traceValue);
            this.state = ItemTraceState.fromRemoteStr(data.getString("tc_state"));
            this.locationOrBuyer = data.optString("tc_loc", "");
        } catch (IllegalArgumentException e) {
            throw  new JSONException(e.getMessage());
        }
    }

    /**
     * 추적 데이터의 번들 수량 정보.
     * [NOTE] 서버로부터 불러온 추적 데이터 리스트로부터 총 수량 계산을 위해 자주 추출해서 사용해야 하므로,
     *        sub 데이터인 추적 ID에 포함된 정보를 상위로 다시 전달하여 불러온다.
     *
     * @return
     */
    public int getBundleCount() {
        return this.traceIdData.getBundleCount();
    }

    public ItemTraceIdData getTraceIdData() {
        return traceIdData;
    }

    public ItemTraceState getState() {
        return state;
    }

    public String getLocationOrBuyer() {
        return locationOrBuyer;
    }
}
