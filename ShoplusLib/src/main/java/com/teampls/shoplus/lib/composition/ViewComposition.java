package com.teampls.shoplus.lib.composition;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.view.MyView;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Medivh on 2019-02-10.
 */

abstract public class ViewComposition implements View.OnClickListener {
    protected Activity context, activity;
    private Set<View> resizedTexts = new HashSet<>();
    protected GlobalDB globalDB;
    protected KeyValueDB keyValueDB;
    protected UserSettingData userSettingData;
    protected UserDB userDB;

    public ViewComposition(Activity activity) {
        this.context = activity;
        this.activity = activity;
        globalDB = GlobalDB.getInstance(activity);
        keyValueDB = KeyValueDB.getInstance(activity);
        userSettingData = UserSettingData.getInstance(activity);
        userDB = UserDB.getInstance(activity);
    }


    public TextView findTextViewById(int viewId) {
        TextView result = (TextView) findViewById(viewId);
        resizeTextView(result);
        return result;
    }

    public ViewGroup getView() {
        return (ViewGroup) ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
    }

    public void setOnClick(int... resIds) {
        for (int resId : resIds)
            setOnClick(resId);
    }

    public EditText findEditTextById(int viewId, String text) {
        EditText result = findViewById(viewId);
        result.setText(text);
        result.setSelection(text.length());
        resizeTextView(result);
        return result;
    }

    public Button findButtonById(int viewId) {
        Button result = (Button) findViewById(viewId);
        setOnClick(viewId);
        resizeTextView(result);
        return result;
    }

    private void resizeTextView(TextView textView) {
        if (resizedTexts.contains(textView) == false) {
            MyView.setTextViewByDeviceSize(context, textView);
            resizedTexts.add(textView);
        }
    }

    protected <T extends View> T findViewById(int viewId) {
        return context.findViewById(viewId);
    }

    public View setOnClick(int viewId) {
        View view = findViewById(viewId);
        view.setOnClickListener(this);
        if (view instanceof TextView && resizedTexts.contains(view) == false) {
            MyView.setTextViewByDeviceSize(context, (TextView) view);
            resizedTexts.add(view);
        }
        return view;
    }

    public void setVisibility(boolean visible, int... resIds) {
        setVisibility(visible ? View.VISIBLE : View.GONE, resIds);
    }

    public void setVisibility(boolean visible, View... views) {
        setVisibility(visible ? View.VISIBLE : View.GONE, views);
    }

    public void setVisibility(int visibility, int... resIds) {
        for (int resId : resIds)
            findViewById(resId).setVisibility(visibility);
    }

    public void setVisibility(int visibility, View... views) {
        for (View view : views)
            view.setVisibility(visibility);
    }

}
