package com.teampls.shoplus.lib.database_app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.PosSlipDB;

/**
 * Created by Medivh on 2017-06-02.
 */

public class DalaranPosSlipDB extends PosSlipDB {
    private static DalaranPosSlipDB instance = null;
    public static boolean isValid = true;

    public static DalaranPosSlipDB getInstance(Context context) {
        try {
            Context dalaranContext = context.createPackageContext(MyApp.Dalaran.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);

            PackageInfo info = context.getPackageManager().getPackageInfo(MyApp.get(context).getPackageName(), 0);
            PackageInfo dalaranInfo = dalaranContext.getPackageManager().getPackageInfo(MyApp.Dalaran.getPackageName(), 0);
            if (info.sharedUserId == null && dalaranInfo.sharedUserId == null) {
                if (instance == null)
                    instance = new DalaranPosSlipDB(dalaranContext);
                isValid = true;
            } else if (info.sharedUserId == null || dalaranInfo.sharedUserId == null) {
                MyUI.toast(context, "에러:앱 공유 ID가 한쪽만 존재합니다");
                instance = new DalaranPosSlipDB(context);
                isValid = false;
            } else if (info.sharedUserId.equals(dalaranInfo.sharedUserId) == false) {
                MyUI.toast(context, "에러:앱 공유 ID가 다릅니다");
                instance = new DalaranPosSlipDB(context);
                isValid = false;
            } else {
                if (instance == null)
                    instance = new DalaranPosSlipDB(dalaranContext);
                isValid = true;
            }
            //   Log.w("DEBUG_JS", String.format("[DalaranPosSlipDB.getInstance] %s", dalaranContext.getDatabasePath("PosSlipDB7Table")));
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("DEBUG_JS", String.format("[DalaranPosSlipDB.getInstance] not found in silvermoon"));
        }
        return instance;
    }

    private DalaranPosSlipDB(Context context) {
        super(context);
        items = DalaranPosSlipItemDB.getInstance(context);
    }

}
