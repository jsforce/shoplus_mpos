package com.teampls.shoplus.lib.awsservice.dynamodb.types;

/**
 * Failure Severity Level
 *
 * @author lucidite
 */

public enum FailureSeverity {
    CRITICAL("critical"),
    MAJOR("major"),
    MINOR("minor");

    private String remoteStr;

    FailureSeverity(String remoteStr) {
        this.remoteStr = remoteStr;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }
}
