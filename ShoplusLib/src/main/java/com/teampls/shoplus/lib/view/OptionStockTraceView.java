package com.teampls.shoplus.lib.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.datatypes.ArrivalItemStockChangeLogData;
import com.teampls.shoplus.lib.datatypes.ItemStockChangeLogBundleData;
import com.teampls.shoplus.lib.datatypes.ItemStockChangeLogProtocol;
import com.teampls.shoplus.lib.datatypes.ManualItemStockChangeLogData;
import com.teampls.shoplus.lib.datatypes.TransactionItemStockChangeLogData;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.view_base.BaseActivity;
import com.teampls.shoplus.lib.view_module.MyDateTimeNavigator;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2019-05-21.
 */

public class OptionStockTraceView extends BaseActivity implements AdapterView.OnItemClickListener {
    private static ItemOptionRecord optionRecord;
    private OptionStockTraceAdapter adapter;
    private ListView listView;
    private MyDateTimeNavigator dailyNavigator;
    private TextView tvNotice;

    public static void startActivity(Context context, ItemOptionRecord optionRecord) {
        OptionStockTraceView.optionRecord = optionRecord;
        context.startActivity(new Intent(context, OptionStockTraceView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        ItemRecord itemRecord = ItemDB.getInstance(context).getRecordBy(optionRecord.itemId);
        myActionBar.setTitle(optionRecord.toStringWithName(itemRecord.name, " / "), true);
        adapter = new OptionStockTraceAdapter(context);
        listView = findViewById(R.id.stock_trace_listview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        tvNotice = findTextViewById(R.id.stock_trace_notice);

        TextView tvTime = findTextViewById(R.id.stock_trace_time);
        TextView tvVariation = findTextViewById(R.id.stock_trace_variation);
        MyView.adjustWeight(tvTime, 90, tvVariation, 90, 20);
        MyView.setTextViewByDeviceSize(context, getView(), R.id.stock_trace_count, R.id.stock_trace_event);

        dailyNavigator = new MyDateTimeNavigator(context, DateTime.now(), getView(), MyDateTimeNavigator.MyMode.Day);
        dailyNavigator.setLimit(DateTime.now().minusDays(90), DateTime.now().plusDays(30));
        dailyNavigator.setOnApplyClick(new MyOnClick<DateTime>() {
            @Override
            public void onMyClick(View view, DateTime record) {
                onTargetDateSelected(record, null);
            }
        });
        dailyNavigator.setLeftCalendarButton(MyDateTimeNavigator.MyMode.Day, new MyOnClick<DateTime>() {
            @Override
            public void onMyClick(View view, DateTime record) {
                onTargetDateSelected(record, null);
            }
        });
        dailyNavigator.setRightRecentButton(MyDateTimeNavigator.MyMode.Day, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTargetDateSelected(BaseUtils.createDay(), null);
            }
        });

        if (MyApp.get(context) == MyApp.Silvermoon) {
            downloadStockTrace(optionRecord, dailyNavigator.getDateTime());
        } else {
            dailyNavigator.setVisibility(View.GONE); // navigator는 제공하지 않는다
            downloadStockTrace(optionRecord, null);
        }
    }

    public void onTargetDateSelected(DateTime dateTime, MyOnTask onTask) {
        dailyNavigator.setDateTime(dateTime);
        downloadStockTrace(optionRecord, dateTime);
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
    }

    private void downloadStockTrace(final ItemOptionRecord optionRecord, final DateTime dateTime) {
        new CommonServiceTask(context, "", "재고변동 기록 조회...") {
            private ItemStockChangeLogBundleData bundleData;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                if (dateTime != null) {
                    bundleData = itemService.getItemStockChangeLogData(userSettingData.getUserShopInfo(), optionRecord.toStockKey(), dateTime);
                    adapter.setRecords(bundleData.getSortedLogs());
                } else {
                    bundleData = itemService.getRecentItemStockChangeLogData(userSettingData.getUserShopInfo(), optionRecord.toStockKey());
                    adapter.setRecords(bundleData.getSortedLogs());
                }
            }

            @Override
            public void onPostExecutionUI() {
                refresh();
                if (dateTime != null) {
                    tvNotice.setText(String.format("기간 : %s ~ %s", BaseUtils.toDay(dateTime).toString(BaseUtils.MDHm_Format), BaseUtils.toDay(dateTime).plusDays(1).minusMillis(1).toString(BaseUtils.MDHm_Format)));
                } else {
                    tvNotice.setText(String.format("기간 : %s ~ %s",DateTime.now().minusDays(1).toString(BaseUtils.MDHm_Format), DateTime.now().toString(BaseUtils.MDHm_Format)));
                }
            }
        };
    }

    @Override
    public int getThisView() {
        return R.layout.activity_stock_trace;
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case close:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ItemStockChangeLogProtocol record = adapter.getRecord(position);
        if (record instanceof TransactionItemStockChangeLogData) {
            downloadTransaction(((TransactionItemStockChangeLogData) record).getTransactionId());

        } else if (record instanceof ManualItemStockChangeLogData) {
            MyUI.toastSHORT(context, String.format("직접 수량을 입력했습니다"));

        } else if (record instanceof ArrivalItemStockChangeLogData) {

        }
    }

    private void downloadTransaction(final String transactionId) {
        new CommonServiceTask(context, "", "영수증 다운중...") {
            private SlipDataProtocol protocol;

            @Override
            public void doInBackground() throws MyServiceFailureException {
                protocol = transactionService.getATransaction(userSettingData.getUserShopInfo(), transactionId, false);
            }

            @Override
            public void onPostExecutionUI() {
                SimplePosSlipSharingView.startActivity(context, new SlipFullRecord(protocol), optionRecord);
            }
        };
    }

    class OptionStockTraceAdapter extends BaseDBAdapter<ItemStockChangeLogProtocol> {

        public OptionStockTraceAdapter(Context context) {
            super(context);
        }

        @Override
        public void generate() {

        }

        @Override
        public BaseViewHolder getViewHolder() {
            return new BaseViewHolder() {
                private TextView tvTime, tvCount, tvVariation, tvEvent;


                @Override
                public int getThisView() {
                    return R.layout.row_option_stock;
                }

                @Override
                public void init(View view) {
                    tvTime = findTextViewById(context, view, R.id.row_four_column1);
                    tvCount = findTextViewById(context, view, R.id.row_four_column2);
                    tvVariation = findTextViewById(context, view, R.id.row_four_column3);
                    tvEvent = findTextViewById(context, view, R.id.row_four_column4);
                    MyView.adjustWeight(tvTime, 90, tvVariation, 90, 20);
                }

                @Override
                public void update(int position) {
                    ItemStockChangeLogProtocol record = records.get(position);
                    ItemStockChangeLogProtocol preRecord = record;
                    if (position + 1 < records.size())
                        preRecord = records.get(position + 1);
                    tvTime.setText(record.getUpdatedDateTime().toString(BaseUtils.MDHm_Format));
                    tvCount.setText(Integer.toString(record.getValue()));
                    tvVariation.setText(BaseUtils.toSignStr(record.getValue() - preRecord.getValue()));
                    tvEvent.setTextColor(ColorType.Black.colorInt);
                    String event = "";

                    if (record instanceof TransactionItemStockChangeLogData) {
                        // 변동
                        TransactionItemStockChangeLogData transRecord = (TransactionItemStockChangeLogData) record;
                        if (transRecord.isCancelled()) {
                            event = "판매취소";
                        } else {
                            event = "판매";
                        }
                        tvVariation.setText(BaseUtils.toSignString(record.getValue(), "0")); // 변동정보
                        tvCount.setText("");

                    } else if (record instanceof ManualItemStockChangeLogData) {
                        event = "수동조정";
                        tvEvent.setTextColor(ColorType.Red.colorInt);
                        tvCount.setText(String.format("%d", record.getValue()));
                        tvVariation.setText("");

                    } else if (record instanceof ArrivalItemStockChangeLogData) {
                        event = "입고";
                        tvEvent.setTextColor(ColorType.Blue.colorInt);
                        tvVariation.setText(BaseUtils.toSignString(record.getValue(), "0")); // 변동정보
                        tvCount.setText("");
                    }
                    tvEvent.setText(event);
                }
            };
        }
    }
}
