package com.teampls.shoplus.lib.enums;

/**
 * 신상알림 메시지에서 사용할 언어 설정
 *
 * @author lucidite
 */
public enum NewArrivalsLanguageType {
    NORMAL("", "", "한국어"),
    CHINESE("cn", "中", "중국어");

    private String remoteStr;
    public String uiStr;
    private String simpleStr;

    NewArrivalsLanguageType(String remoteStr, String simpleStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.simpleStr = simpleStr;
        this.uiStr = uiStr;
    }

    public String toRemoteStr() {
        return remoteStr;
    }

    public String toSimpleStr() {
        return simpleStr;
    }

    public static NewArrivalsLanguageType fromRemoteStr(String remoteStr) {
        if (remoteStr == null) {
            return NORMAL;
        }
        for (NewArrivalsLanguageType type : NewArrivalsLanguageType.values()) {
            if (remoteStr.equals(type.remoteStr)) {
                return type;
            }
        }
        return NORMAL;
    }

    public static NewArrivalsLanguageType valueOfDefault(String name) {
        try {
            return valueOf(name);
        } catch (Exception e) {
            return NORMAL;
        }
    }
}
