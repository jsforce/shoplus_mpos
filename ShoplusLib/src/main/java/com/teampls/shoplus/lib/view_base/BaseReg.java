package com.teampls.shoplus.lib.view_base;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.awsservice.ServiceErrorHelper;
import com.teampls.shoplus.lib.awsservice.cognito.CognitoUtils;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.MyRecord;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.project.ProjectCodeName;
import com.teampls.shoplus.lib.view.MyView;

import java.util.Calendar;

/**
 * Created by Medivh on 2016-10-14.
 */
abstract public class BaseReg extends BaseActivity implements View.OnClickListener {
    protected TextView tvTerms, tvPrivacy, tvGeneratePassword;
    protected EditText etNickname, etPassword, etPhoneNumber;
    private final int passwordLength = 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        myActionBar.hide();
        etNickname = (EditText) findViewById(R.id.myReg_nickname);
        etPhoneNumber = (EditText) findViewById(R.id.myReg_phonenumber);
        etPassword = (EditText) findViewById(R.id.myReg_password);
        tvGeneratePassword = (TextView) findViewById(R.id.myReg_generatePassword);

        tvTerms = (TextView) findViewById(R.id.myReg_terms);
        tvTerms.setOnClickListener(this);
        tvPrivacy = (TextView) findViewById(R.id.myReg_privacy);
        tvPrivacy.setOnClickListener(this);
        tvTerms.setText(Html.fromHtml("<u>" + tvTerms.getText().toString() + "<u>"));
        tvPrivacy.setText(Html.fromHtml("<u>" + tvPrivacy.getText().toString() + "<u>"));

        setOnClick(R.id.myReg_generatePassword, R.id.myReg_register,
                R.id.myReg_terms, R.id.myReg_privacy);

        if (MyDevice.hasPermission(context, MyDevice.UserPermission.PHONE) == false) {
            restartApp();
        } else {
            etPhoneNumber.setText(BaseUtils.toPhoneFormat(MyDevice.getMyPhoneNumber(context)));
            etPassword.setText("");
            MyView.setFontSizeByDeviceSize(context, etNickname, etPhoneNumber, etPassword,
                    tvGeneratePassword, tvTerms, tvPrivacy);
            MyView.setFontSizeByDeviceSize(context, getView(), R.id.myReg_generatePassword, R.id.myReg_register,
                    R.id.myReg_privacy_nextline, R.id.myReg_nickname_guide, R.id.myReg_privacy_or, R.id.myReg_nickname_title);

            MyView.setMarginByDeviceSize(context, etNickname, etPhoneNumber, etPassword, tvGeneratePassword);
            MyView.setMarginByDeviceSize(context, getView(),R.id.myReg_nickname_title, R.id.myReg_register,
                    R.id.myReg_phone_guide);
        }
    }

    private void restartApp() {
        new MyAlertDialog(context, "전화번호 권한 없음", "전화번호를 확인할 수 없어 앱을 재시작 합니다") {
            @Override
            public void yes() {
                myDB.clear();
                new MyDelayTask(context, 500) {
                    @Override
                    public void onFinish() {
                        MyDevice.openApp(context, context.getPackageName());
                    }
                };
                finishAffinity();
            }

            @Override
            public void no() {
                finish();
            }
        };
    }

    @Override
    public int getThisView() {
        return R.layout.base_user_reg;
    }

    public void onClick(View view) {
        if (view.getId() == R.id.myReg_register) {
            if (Empty.isEmpty(context, etNickname, "이름이 입력되지 않았습니다")) return;
            if (Empty.isEmpty(context, etPassword, "비밀번호가 입력되지 않았습니다")) return;
            if (Empty.isEmpty(context, etPhoneNumber, "전화번호가 입력되지 않았습니다")) return;
            if (etPassword.getText().toString().length() < 6) {
                MyUI.toastSHORT(context, String.format("비밀번호는 최소한 6자 이상 되어야 합니다"));
                return;
            }
            String nickname = etNickname.getText().toString().trim();
            String phoneNumber = BaseUtils.toSimpleForm(etPhoneNumber.getText().toString());
            String password = BaseUtils.encode(etPassword.getText().toString().trim(), phoneNumber);

            ///////////////////////////////////////////////////
            // [Integration] 새로운 사용자를 등록한다.
            registerNewUser(nickname, phoneNumber, password);

        } else if (view.getId() == R.id.myReg_generatePassword) {
            etPassword.setText(BaseUtils.getRandomPassword(passwordLength));
            // etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else if (view.getId() == R.id.myReg_terms) {
            MyDevice.openWeb(context, BaseGlobal.TermsOfServiceUrl);
        } else if (view.getId() == R.id.myReg_privacy) {
            MyDevice.openWeb(context, BaseGlobal.PrivacyPolicyUrl);
        }
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    protected void registerNewUser(final String nickname, final String phoneNumber, final String password) {
        final String username = CognitoUtils.getEncodedUsername(ProjectCodeName.DALARAN, Calendar.getInstance(), phoneNumber);
        CognitoUserAttributes userAttributes = new CognitoUserAttributes();
        userAttributes.addAttribute("nickname", nickname);
        userAttributes.addAttribute("phone_number", CognitoUtils.convertToCompatiblePhoneNumber(phoneNumber));
        SignUpHandler handler = new SignUpHandler() {
            @Override
            public void onSuccess(CognitoUser user, boolean signUpConfirmationState, CognitoUserCodeDeliveryDetails cognitoUserCodeDeliveryDetails) {
                Log.i("DEBUG_TAG", "[DEBUG] 회원 등록되었습니다(Confirmed=" + String.valueOf(signUpConfirmationState) + ").");
                // TODO @Sunstrider signUpConfirmationState 상태 확인 (정상인 경우 False여야 한다.)

                // 사용자 등록에 성공하면 myDB에 사용자 정보를 저장하고 activity를 전환한다.
                myDB.register(new MyRecord(username, phoneNumber, nickname, password));
                BaseAppWatcher.setNewlyRegistered(context, true); // 새 가입자라고 표시
                onRegister();
            }

            @Override
            public void onFailure(Exception exception) {
                String userErrorMessage = ServiceErrorHelper.getCognitoIdErrorMessageForUser(exception, true);
                MyUI.toast(context, userErrorMessage);
            }
        };
        UserHelper.getPool().signUp(username, password, userAttributes, null, handler);
    }

    abstract public void onRegister();

}