package com.teampls.shoplus.lib.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by Medivh on 2016-09-26.
 */
public class MyAsyncTask {
    private Context context;
    private String location = "";
    protected ProgressDialog otherProgressDialog;

    public MyAsyncTask(final Context context) {
        this(context, "");
    }

    public MyAsyncTask(Context context, final String callLocation, final String progressMessage) {
        this(context, callLocation);
        if (TextUtils.isEmpty(progressMessage) == false && MyUI.isActiveActivity(context, "")) {
            otherProgressDialog = new ProgressDialog(context);
            otherProgressDialog.setMessage(progressMessage);
            otherProgressDialog.show();
        }
    }

    protected void setProgressMessage(String message) {
        if (otherProgressDialog == null)
            return;
        MyUI.setMessage(context, otherProgressDialog, message);
    }

    private void dismissProgress() {
        if (otherProgressDialog != null)
            otherProgressDialog.dismiss();
    }

    public MyAsyncTask(Context context, final String location) {
        this.context = context;
        this.location = location;

        if (MyUI.isActivity(context, "MyAsyncTask") == false) {
            if (MyUI.isActivity(BaseAppWatcher.getCurrentActivity(), "MyAsyncTask") == false) {
                dismissProgress();
                return;
            } else {
                context = BaseAppWatcher.getCurrentActivity();
                this.context = context;
            }
        }

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                onPreExecute();
                execute();
            }
        });
    }

    public void onPreExecute() {

    }

    public void doInBackground() {
    }

    private void execute() {
        new Thread() {
            public void run() {
                try {
                    doInBackground();
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            dismissProgress();
                            onPostExecutionUI();
                        }
                    });
                } catch (final Exception e) {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            dismissProgress();
                            catchException(e);
                        }
                    });
                }
            }
        }.start();
    }

    public void onPostExecutionUI() {
    }

    public void catchException(Exception e) {
        Log.e("DEBUG_JS", String.format("[MyAsyncTask.run] %s.%s : %s", context.getClass().getSimpleName(), location, e.getLocalizedMessage()));
    }
}
