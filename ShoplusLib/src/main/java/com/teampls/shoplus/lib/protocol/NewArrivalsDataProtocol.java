package com.teampls.shoplus.lib.protocol;

import java.util.List;

/**
 * 신상알림 메시지(링크)에 해당하는 신상알림 데이터 프로토콜
 *
 * @author lucidite
 */

public interface NewArrivalsDataProtocol {
    String getMessage();
    List<NewArrivalsItemDataProtocol> getContents();
}
