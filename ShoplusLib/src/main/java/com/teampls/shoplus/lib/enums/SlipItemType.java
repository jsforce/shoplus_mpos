package com.teampls.shoplus.lib.enums;

import android.graphics.Color;
import android.util.Log;

import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database_global.GlobalDB;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.teampls.shoplus.lib.enums.SlipItemType.ExchangeType.Item;
import static com.teampls.shoplus.lib.enums.SlipItemType.ExchangeType.Money;
import static com.teampls.shoplus.lib.enums.SlipItemType.ExchangeType.MoneyAndItem;
import static com.teampls.shoplus.lib.enums.SlipItemType.FamilyType.Advance;
import static com.teampls.shoplus.lib.enums.SlipItemType.FamilyType.Consign;
import static com.teampls.shoplus.lib.enums.SlipItemType.FamilyType.Deposit;
import static com.teampls.shoplus.lib.enums.SlipItemType.FamilyType.Deprecated;
import static com.teampls.shoplus.lib.enums.SlipItemType.FamilyType.ExchangeReturn;
import static com.teampls.shoplus.lib.enums.SlipItemType.FamilyType.ExchangeSale;
import static com.teampls.shoplus.lib.enums.SlipItemType.FamilyType.Preorder;
import static com.teampls.shoplus.lib.enums.SlipItemType.FamilyType.Sales;
import static com.teampls.shoplus.lib.enums.SlipItemType.FamilyType.Sample;
import static com.teampls.shoplus.lib.enums.SlipItemType.PendingType.Done;
import static com.teampls.shoplus.lib.enums.SlipItemType.PendingType.Pending;

/**
 * 개별 Slip Item의 타입 및 서버로 전송할 문자열 정보
 *
 * @author lucidite
 */
// 항목이 추가되면 아래 UI 파일도 같이 추가해줘야 함
//  BaseSlipItemAddition
public enum SlipItemType {
    NONE("df", "미정", FamilyType.None, PendingType.None, ExchangeType.None, 0, 0),

    // [SHOPLUS-9] 전표(거래)항목 상태/종류
    // 1종: 미송앱 기록/(도매)거래기록 생성 시에 일반적으로 선택 가능한 상태.
    // 2종: 미송앱에서 1종 상태로부터 전환 가능한 상태. (도매)거래기록 생성 시에도 2종 상태를 사용한다(SHOPLUS-27).
    // *3종: (도매)거래기록 생성 시에 사용 가능한 상태. 미송앱 기록 생성 또는 상태 변경 시에 사용 불가.
    // [SHOPLUS-27] 3종 상태는 더 이상 업로드용으로 사용하지 않는다. 하방호환성 문제로 당분간은 잔류(17-05-29).
    // (참고) 2종-3종 상태 간에는 1:1 대응 관계가 있음.
    // [SHOPLUS-440] 부가세 항목 신설 (차후 확장에 사전 대응, 19-01-18, Sunstrider)

    SALES("sl", "판매", Sales, Done, MoneyAndItem, 1, -1),
    RETURN("rt", "반품", Sales, Done, MoneyAndItem, -1, 1),
    DEPOSIT("dp", "매입", Deposit, Done, Item, 0, 1), // 기존엔 Pending
    WITHDRAWAL("wd", "매입빼기", Deposit, Done, Money, -1, 0),

    ADVANCE("rv", "미송", Advance, Pending, Money, 1, 0),
    ADVANCE_OUT_OF_STOCK("ro", "미송품절", Advance, Pending, ExchangeType.None, 0, 0),     // [SHOPLUS-27] 미송과 내부적으로 동일
    ADVANCE_PACKING("rp", "미송준비", Advance, Pending, ExchangeType.None, 0, 0), // 정보성
    ADVANCE_CLEAR("rc", "미송배송", Advance, Done, Item, 0, -1),
    ADVANCE_SUBTRACTION("rd", "미송환불", Advance, Done, Money, -1, 0),

    PREORDER("od", "주문", Preorder, Pending, ExchangeType.None, 0, 0),
    PREORDER_PACKING("op", "주문포장", Preorder, Pending, ExchangeType.None, 0, 0),
    PREORDER_CLEAR("oc", "주문결제", Preorder, Done, MoneyAndItem, 1, -1),
    PREORDER_OUT_OF_STOCK("oo", "주문품절", Preorder, Done, ExchangeType.None, 0, 0),    // [SHOPLUS-27] 주문취소와 내부적으로 동일
    PREORDER_WITHDRAW("ow", "주문취소", Preorder, Done, ExchangeType.None, 0, 0),

    CONSIGN("cs", "위탁", Consign, Pending, Item, 0, -1),
    CONSIGN_CLEAR("cc", "위탁결제", Consign, Done, Money, 1, 0),
    CONSIGN_RETURN("cr", "위탁반납", Consign, Done, Item, 0, 1),

    SAMPLE("sp", "샘플", Sample, Pending, Item, 0, -1),                     // [SHOPLUS-27] 위탁과 내부적으로 동일
    SAMPLE_CLEAR("sc", "샘플결제", Sample, Done, Money, 1, 0),             // [SHOPLUS-27] 위탁결제와 내부적으로 동일
    SAMPLE_RETURN("sr", "샘플반납", Sample, Done, Item, 0, 1),            // [SHOPLUS-27] 위탁반품과 내부적으로 동일

    EXCHANGE_SALES_PENDING("esp", "보낼상품", ExchangeSale, Pending, Item, 0, 0), // 미송과 같은 속성, 보낼물건
    EXCHANGE_RETURN_PENDING("erp", "받을상품", ExchangeReturn, Pending, Item, 0, 0), // 샘플과 같은 속성, 받을물건
    EXCHANGE_SALES("es", "교환보냄", ExchangeSale, Done, Item, 0, -1),           // [SHOPLUS-27] 결제와 내부적으로 동일
    EXCHANGE_RETURN("er", "교환받음", ExchangeReturn, Done, Item, 0, 1),          // [SHOPLUS-27] 반품과 내부적으로 동일

    DISCOUNT("dc", "DC", FamilyType.None, Done, ExchangeType.None, -1, 0),          // [SILVER-208]
    VALUE_ADDED_TAX("vt", "부가세", FamilyType.None, Done, ExchangeType.None, 1, 0),   // [SILVER-440] TODO 신규추가(체크필요)
    MISC_EXPENSES("me", "비용", FamilyType.None, Done, ExchangeType.None, 1, 0),       // TODO 신규추가 (체크필요)

    @Deprecated
    POS_ADVANCE_CLEAR("prc", "미송배송", Deprecated, Done, Item, 0, -1),       // 3종
    POS_WITHDRAWAL("pwd", "매입빼기", Deprecated, Done, Money, -1, 0),
    POS_ADVANCE_SUBTRACTION("prd", "미송환불", Deprecated, Done, Money, -1, 0), // 3종
    POS_CONSIGN_RETURN("pcr", "위탁반납", Deprecated, Done, Item, 0, 1),      // 3종
    POS_PREORDER_CLEAR("poc", "주문결제", Deprecated, Done, MoneyAndItem, 1, -1),      // 3종
    POS_CONSIGN_CLEAR("pcc", "위탁결제", Deprecated, Done, Money, 1, 0),       // 3종
    POS_PREORDER_WITHDRAW("pow", "주문취소", Deprecated, Done, ExchangeType.None, 0, 0);   // 3종


    final public static String DeliveryName = "택배비";
    private String remoteTypeStr;
    public String uiName;
    private PendingType pendingType;
    public FamilyType familyType;
    private ExchangeType exchangeType;
    private int moneySign = 0, stockSign = 0;

    enum PendingType {None, Pending, Done}

    public enum FamilyType {None, Advance, Consign, Preorder, Sample, Sales, Deposit, Deprecated, ExchangeSale, ExchangeReturn}

    enum ExchangeType {None, Money, Item, MoneyAndItem}

    SlipItemType(String remoteTypeStr, String uiName, FamilyType familyType, PendingType pendingType, ExchangeType exchangeType, int moneySign, int stockSign) {
        this.remoteTypeStr = remoteTypeStr;
        this.uiName = uiName;
        this.familyType = familyType;
        this.pendingType = pendingType;
        this.exchangeType = exchangeType;
        this.moneySign = moneySign;
        this.stockSign = stockSign;
    }

    /**
     * SlipItemType을 서버로 전송할 slip item type 문자열로 변환한다.
     *
     * @return
     */
    public String toRemoteString() {
        return this.remoteTypeStr;
    }

    /**
     * 서버로부터 받은 slip item type 문자열을 SlipItemType으로 변환한다.
     *
     * @param receivedStr
     * @return
     */
    public static SlipItemType getFromRemoteTypeString(String receivedStr) {
        for (SlipItemType item : SlipItemType.values()) {
            if (item.remoteTypeStr.equals(receivedStr)) {
                return item;
            }
        }
        // fallback (backward-compatibility)
        switch (receivedStr) {
            case "sales":
                return SALES;
            case "return":
                return RETURN;
            case "deposit":
                return DEPOSIT;
            case "withdrawal":
                return WITHDRAWAL;
            case "reserve":
                return ADVANCE;
            case "reserve_clear":
                return ADVANCE_CLEAR;
            case "reserve_deposit":
                return ADVANCE_SUBTRACTION;
            case "consign":
                return CONSIGN;
            case "consign_clear":
                return CONSIGN_CLEAR;
            case "consign_return":
                return CONSIGN_RETURN;
            default:
                //    Log.e("DEBUG_JS", String.format("[SlipItemType.getFromRemoteTypeString] %s", receivedStr));
                return SALES;
        }
    }

    public String toMultiUiName() {
        String[] headers = {ADVANCE.uiName, CONSIGN.uiName, DEPOSIT.uiName, PREORDER.uiName, SAMPLE.uiName,
                "교환", "취소", "선교환", "선반품", "입고", "출고", "받을", "보낼"};
        String result = uiName;
        for (String header : headers) {
            if (uiName.startsWith(header) && (uiName.length() > header.length())) {
                result = String.format("%s\n%s", header, uiName.replace(header, ""));
                break;
            }
        }
        return result;
    }

    public static List<SlipItemType> getPendings(Set<SlipItemType> slipItemTypes, SlipItemType... moreTypes) {
        Set<SlipItemType> results = new HashSet<>();
        List<SlipItemType> moreTypeList = BaseUtils.toList(moreTypes);
        for (SlipItemType slipItemType : slipItemTypes) {
            if (slipItemType.pendingType == Pending) {
                results.add(slipItemType);
            } else if (moreTypeList.contains(slipItemType))
                results.add(slipItemType);
        }
        return new ArrayList(results);
    }

    public static List<SlipItemType> getPendings() {
        Set<SlipItemType> results = new HashSet<>();
        for (SlipItemType slipItemType : values()) {
            if (slipItemType.pendingType == Pending)
                results.add(slipItemType);
        }
        return new ArrayList(results);
    }

    public List<SlipItemType> getAvailableTypes() {
        List<SlipItemType> results = getFamilyTypes();
        switch (this) {
            case EXCHANGE_SALES_PENDING:
                results.add(ADVANCE); // 선반품을 미송으로
                break;
            case ADVANCE:
                results.add(EXCHANGE_SALES_PENDING); // reverse
                break;
            case EXCHANGE_RETURN_PENDING:
                results.add(SALES); // 소매에서 보내려고 하다가 판 경우
                break;
            case SALES:
                results.add(EXCHANGE_RETURN_PENDING); // reverse
                break;
        }
        return results;
    }

    public List<SlipItemType> getFamilyTypes() {
        List<SlipItemType> results = new ArrayList<>();
        for (SlipItemType slipItemType : values()) {
            if (this.familyType == slipItemType.familyType)
                results.add(slipItemType);
        }
        return results;
    }

    public String getSignStr() {
        switch (getSign()) {
            case -1:
                return "-";
            default:
                return "";
        }
    }

    public String getInverseSignStr() {
        switch (getSign()) {
            case -1:
                return "";
            default:
                return "-";
        }
    }

    public int getSign() {
        return moneySign;
    }

    public int getStockSign() { // 재고 변동
        return stockSign;
    }

    // 실질적으로 미완료된 거래들만 컬러로 표시 (반품예치, 미송예치, 미송, 위탁)
    // 나머지는 전표가 쓰인 순간에 완료된 거래들이다. (거래, 반품, 인출, 미송배송, 위탁완료, 위탁반품)
    public int getColorInt() {
        switch (this) {
            case ADVANCE:
            case ADVANCE_PACKING:
            case ADVANCE_OUT_OF_STOCK:
                return Color.parseColor("#FF8774");
            case PREORDER:
            case PREORDER_PACKING:
                return Color.parseColor("#FF00FF");
            case EXCHANGE_SALES_PENDING:
                return ColorType.Fuchsia.colorInt;

            case CONSIGN:
                return Color.parseColor("#749FC1");
            case SAMPLE:
                return ColorType.RoyalBlue.colorInt;

            case DEPOSIT:
                return ColorType.OliveDrab.colorInt;

            case WITHDRAWAL:
                return ColorType.RedWine.colorInt;

            case RETURN:
                //case EXCHANGE_RETURN: 검은색으로 표시
            case EXCHANGE_RETURN_PENDING:
                return ColorType.Maroon.colorInt;
            default:
                return ColorType.Black.colorInt;
        }
    }

    public boolean isPending() {
        return (pendingType == PendingType.Pending);
    }

    public boolean isDone() {
        return (pendingType == PendingType.Done);
    }

    public static SlipItemType getClearType(SlipItemType slipItemType) {
        switch (slipItemType) {
            case EXCHANGE_SALES_PENDING:
                return EXCHANGE_SALES;
            case EXCHANGE_RETURN_PENDING:
                return EXCHANGE_RETURN;
            case DEPOSIT:
                return WITHDRAWAL;
            case ADVANCE:
            case ADVANCE_OUT_OF_STOCK:
            case ADVANCE_PACKING:
                return ADVANCE_CLEAR;
            case CONSIGN:
                return CONSIGN_CLEAR;
            case SAMPLE:
                return SAMPLE_CLEAR;
            case PREORDER:
            case PREORDER_PACKING:
                return PREORDER_CLEAR;
            default:
                Log.w("DEBUG_JS", String.format("[SlipItemType.getClearType] %s is not pending type", slipItemType.toString()));
                return slipItemType;
        }
    }

    public static List<SlipItemType> getMoneyRelated(boolean all) {
        List<SlipItemType> results = new ArrayList<>();
        for (SlipItemType slipItemType : values()) {
            if (slipItemType.exchangeType == ExchangeType.Money) {
                results.add(slipItemType);
            } else if (slipItemType.exchangeType == ExchangeType.MoneyAndItem) {
                if (all)
                    results.add(slipItemType);
            }
        }
        return results;
    }

    public static List<SlipItemType> getItemRelated(boolean all) {
        List<SlipItemType> results = new ArrayList<>();
        for (SlipItemType slipItemType : values()) {
            if (slipItemType.exchangeType == ExchangeType.Item) {
                results.add(slipItemType);
            } else if (slipItemType.exchangeType == ExchangeType.MoneyAndItem) {
                if (all)
                    results.add(slipItemType);
            }
        }
        return results;
    }

    public static List<SlipItemType> getQrRelated() {
        List<SlipItemType> results = new ArrayList<>();
        results.add(SALES);
        results.add(RETURN); // 반품종류
        results.add(DEPOSIT); // 반품종류
        results.add(CONSIGN);
        results.add(CONSIGN_RETURN); // 반품종류
        results.add(SAMPLE);
        results.add(SAMPLE_RETURN); // 반품종류
        results.add(PREORDER_CLEAR);
        results.add(EXCHANGE_SALES);
        results.add(EXCHANGE_RETURN); // 반품종류
        return results;
    }

    public static List<SlipItemType> getQrPendings() {
        List<SlipItemType> results = new ArrayList<>();
        results.add(ADVANCE_CLEAR);
        results.add(CONSIGN_RETURN);
        results.add(SAMPLE_RETURN);
        results.add(PREORDER_CLEAR);
        results.add(EXCHANGE_SALES);
        results.add(EXCHANGE_RETURN);
        return results;
    }

    public static List<SlipItemType> getAll() {
        List<SlipItemType> results = new ArrayList<>();
        for (SlipItemType slipItemType : values()) {
            results.add(slipItemType);
        }
        return results;
    }

    public boolean isTradeType() {
        switch (this) {
            case MISC_EXPENSES: // 택배비 등 추가비용
            case VALUE_ADDED_TAX: // 부가세
                return false;
            default:
                return true;
        }
    }

    public boolean isNonEditableType() {
        switch (this) {
            case DISCOUNT:
            case VALUE_ADDED_TAX:
                return true;
            default:
                return false;
        }
    }

    public boolean isPriceOnlyType() {
        switch (this) {
            default:
                return false;
            case MISC_EXPENSES:
                return true;
        }
    }

    public boolean isItemType() {
        switch (this) {
            default:
                return true;
            case DISCOUNT:
            case VALUE_ADDED_TAX:
            case MISC_EXPENSES:
                return false;
        }
    }

    public boolean isExchangeTypeOrTaxOrExpense() {
        switch (this) {
            case EXCHANGE_RETURN:
            case EXCHANGE_RETURN_PENDING:
            case EXCHANGE_SALES:
            case EXCHANGE_SALES_PENDING:
            case VALUE_ADDED_TAX:
            case MISC_EXPENSES:
                return true;
            default:
                return false;
        }
    }

    public static List<SlipItemType> getNoQuantityTypes() {
        List<SlipItemType> results = new ArrayList<>();
        results.add(SlipItemType.VALUE_ADDED_TAX);
        results.add(SlipItemType.DISCOUNT);
        results.add(SlipItemType.WITHDRAWAL);
        return results;
    }

    /**
     * 실제 출고가 일어나는 종류인지 확인한다. QR코드 연동 거래기록 작성에 활용한다.
     *
     * @return
     */
    public boolean isReleasingType() {
        switch (this) {
            case SALES:
            case CONSIGN:
            case SAMPLE:
            case EXCHANGE_SALES:
            case ADVANCE_CLEAR:
            case PREORDER_CLEAR:
                return true;
            default:
                return false;
        }
    }
}

/*
        int result = 0;
        switch (this) {
            case EXCHANGE_SALES:
            case EXCHANGE_SALES_PENDING:
            case EXCHANGE_RETURN:
            case EXCHANGE_RETURN_PENDING:
                return 0;
            case SALES:
                return 1;
            case RETURN:
                return -1;
            case DEPOSIT:
                return 0;                   // SHOPLUS-27 (Sunstrider)
            case WITHDRAWAL:
            case POS_WITHDRAWAL:
                return -1;                  // SHOPLUS-27 (Sunstrider)

            case ADVANCE:
                return 1;
            case ADVANCE_PACKING:
            case ADVANCE_OUT_OF_STOCK:      // SHOPLUS-27 (Sunstrider), 내부적으로 미송과 동일
                return 0;                   // SHOPLUS-27 (Sunstrider)
            case ADVANCE_CLEAR:
            case POS_ADVANCE_CLEAR:
                return 0;                   // SHOPLUS-27 (Sunstrider)
            case ADVANCE_SUBTRACTION:
            case POS_ADVANCE_SUBTRACTION:
                return -1;                  // SHOPLUS-27 (Sunstrider)

            case CONSIGN:
            case SAMPLE:                    // SHOPLUS-27 (Sunstrider), 내부적으로 위탁과 동일
                return 0;                   // SHOPLUS-27 (Sunstrider)

            case CONSIGN_CLEAR:
            case POS_CONSIGN_CLEAR:
            case SAMPLE_CLEAR:              // SHOPLUS-27 (Sunstrider), 내부적으로 위탁완료와 동일
                return 1;                   // SHOPLUS-27 (Sunstrider)
            case CONSIGN_RETURN:
            case POS_CONSIGN_RETURN:
            case SAMPLE_RETURN:             // SHOPLUS-27 (Sunstrider), 내부적으로 위탁반품과 동일
                return 0;

            case PREORDER:
            case PREORDER_PACKING:
                return 0;                   // SHOPLUS-27 (Sunstrider)

            case PREORDER_CLEAR:
            case POS_PREORDER_CLEAR:
                return 1;                   // SHOPLUS-27 (Sunstrider)
            case PREORDER_WITHDRAW:
            case POS_PREORDER_WITHDRAW:
            case PREORDER_OUT_OF_STOCK:     // SHOPLUS-27 (Sunstrider), 내부적으로 주문취소와 동일
                return 0;
            case DISCOUNT:                  // SILVER-208
                return -1;
        }
        return result;
 */