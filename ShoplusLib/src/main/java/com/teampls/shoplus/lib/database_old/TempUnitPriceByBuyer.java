package com.teampls.shoplus.lib.database_old;

import android.content.Context;

import com.teampls.shoplus.lib.database_map.KeyValueDB;

/**
 * Created by Medivh on 2019-05-24.
 */

public class TempUnitPriceByBuyer extends KeyValueDB {
    private static TempUnitPriceByBuyer instance = null;

    public static TempUnitPriceByBuyer getInstance(Context context) {
        if (instance == null)
            instance = new TempUnitPriceByBuyer(context);
        return instance;
    }

    public TempUnitPriceByBuyer(Context context) {
        super(context, "TempUnitPriceByBuyer", 1);
        cleanUpBySize(100);
    }
}
