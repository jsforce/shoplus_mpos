package com.teampls.shoplus.lib.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.BaseRecord;
import com.teampls.shoplus.lib.datatypes.MyShopInformationData;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.dialog.SearchStringDialog;
import com.teampls.shoplus.lib.dialog.UpdateValueDialog;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.viewSetting.BaseSettingView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-04-11.
 */

public class PosSlipInfoView extends BaseSettingView {
    private TextView tvName, tvPhone1, tvPhone2, tvAccount1, tvAccount2, tvAccount3,
            tvAddress, tvLocation, tvMemo;
    private List<String> buildingAddresses = new ArrayList<>();
    private MyShopInformationData shopData;

    public static void startActivity(Context context) {
        ((Activity) context).startActivityForResult(new Intent(context, PosSlipInfoView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public int getThisView() {
        return R.layout.base_slipinfo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.setTitle("거래기록 기본정보 입력");
        myActionBar.hideRefreshTime();
        shopData = userSettingData.cloneShopData();
        buildingAddresses = MyInfoView.getBuildingAddresses();
        tvName = findViewById(R.id.slipinfo_name);
        tvPhone1 = findViewById(R.id.slipinfo_phoneNumber1);
        tvPhone2 = findViewById(R.id.slipinfo_phoneNumber2);
        tvAccount1 = findViewById(R.id.slipinfo_account1);
        tvAccount2 = findViewById(R.id.slipinfo_account2);
        tvAccount3 = findViewById(R.id.slipinfo_account3);
        tvAddress = findViewById(R.id.slipinfo_address);
        tvLocation = findViewById(R.id.slipinfo_location);
        tvMemo = findViewById(R.id.slipinfo_memo);

        LinearLayout container = findViewById(R.id.slipinfo_receipt_container);
        addCheckBoxs(container, true);

        setOnClick(R.id.slipinfo_close, R.id.slipinfo_name, R.id.slipinfo_phoneNumber1, R.id.slipinfo_phoneNumber2,
                R.id.slipinfo_account1, R.id.slipinfo_account2, R.id.slipinfo_account3, R.id.slipinfo_apply,
                R.id.slipinfo_address, R.id.slipinfo_location, R.id.slipinfo_memo);

        refreshView();
    }

    private void refreshView() {
        BaseUtils.setText(tvName, "매장명 : ", shopData.getShopName());
        BaseUtils.setText(tvPhone1, "매장번호1 : ", shopData.getShopPhoneNumber(0));
        BaseUtils.setText(tvPhone2, "매장번호2 : ", shopData.getShopPhoneNumber(1));
        BaseUtils.setText(tvAccount1, "계좌1 : ", shopData.getAccountNumber(0));
        BaseUtils.setText(tvAccount2, "계좌2 : ", shopData.getAccountNumber(1));
        BaseUtils.setText(tvAccount3, "계좌3 : ", shopData.getAccountNumber(2));
        BaseUtils.setText(tvAddress, "건물주소 : ", shopData.getBuildingAddress());
        BaseUtils.setText(tvLocation, "매장위치 : ", shopData.getDetailedAddress());
        BaseUtils.setText(tvMemo, "메모 : ", shopData.getNotice());
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.slipinfo_close) {
            doCheckAndFinish();

        } else if (view.getId() == R.id.slipinfo_name) {
            new UpdateValueDialog(context, "매장명 입력", "", shopData.getShopName()) {
                public void onApplyClick(String newStr) {
                    shopData.setShopName(newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.slipinfo_apply) {
            userSettingData.updateWorkingShopData(shopData, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    MyUI.toastSHORT(context, String.format("설정을 적용했습니다"));
                    doFinishForResult();
                }
            });

        } else if (view.getId() == R.id.slipinfo_phoneNumber1) {
            new UpdateValueDialog(context, "전화번호 입력", "", shopData.getShopPhoneNumber(0)) {
                public void onApplyClick(String newStr) {
                    shopData.updatePhoneNumber(0, newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.slipinfo_phoneNumber2) {
            new UpdateValueDialog(context, "전화번호 입력", "", shopData.getShopPhoneNumber(1)) {
                public void onApplyClick(String newStr) {
                    shopData.updatePhoneNumber(1, newStr);
                    refreshView();
                }
            };

        } else if (view.getId() == R.id.slipinfo_account1) {
            new UpdateValueDialog(context, "계좌 입력", "", shopData.getAccountNumber(0)) {
                public void onApplyClick(String newStr) {
                    shopData.updateAccountNumber(0, newStr);
                    refreshView();
                }
            };
        } else if (view.getId() == R.id.slipinfo_account2) {
            new UpdateValueDialog(context, "계좌 입력", "", shopData.getAccountNumber(1)) {
                public void onApplyClick(String newStr) {
                    shopData.updateAccountNumber(1, newStr);
                    refreshView();
                }
            };
        } else if (view.getId() == R.id.slipinfo_account3) {
            new UpdateValueDialog(context, "계좌 입력", "", shopData.getAccountNumber(2)) {
                public void onApplyClick(String newStr) {
                    shopData.updateAccountNumber(2, newStr);
                    refreshView();
                }
            };
        } else if (view.getId() == R.id.slipinfo_address) {
            new SearchStringDialog(context, "주소 입력", buildingAddresses) {
                @Override
                public void onItemClick(String string) {
                    String buildingAddress = string;
                    if (string.contains("]")) {
                        String buildingName = string.split("]")[0].replace("[", "");
                        buildingAddress = string.split("]")[1].trim();
                        if (shopData.getDetailedAddress().isEmpty()) // 건물이름 자동으로 등록
                            shopData.setDetailedAddress(buildingName);
                    }
                    shopData.setBuildingAddress(buildingAddress);
                    refreshView();
                }
            };
        } else if (view.getId() == R.id.slipinfo_location) {
            new UpdateValueDialog(context, "매장 위치 입력", "", shopData.getDetailedAddress()) {
                public void onApplyClick(String newStr) {
                    shopData.setDetailedAddress(newStr);
                    refreshView();
                }
            };
        } else if (view.getId() == R.id.slipinfo_memo) {
            new UpdateValueDialog(context, "메모 입력", "", shopData.getNotice()) {
                public void onApplyClick(String newStr) {
                    shopData.setNotice(newStr);
                    refreshView();
                }
            };
        } else {
            //
        }
    }

    private void doCheckAndFinish() {
        if (BaseRecord.isSame(shopData, userSettingData.getWorkingShop())) {
            doFinishForResult();
        } else {
            new MyAlertDialog(context, "정보 변경", "변경된 내용이 있습니다. 저장하지 않고 닫으시겠습니까?") {
                @Override
                public void yes() {
                    doFinishForResult();
                }
            };
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar.add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case close:
                doCheckAndFinish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        doCheckAndFinish();
    }
}