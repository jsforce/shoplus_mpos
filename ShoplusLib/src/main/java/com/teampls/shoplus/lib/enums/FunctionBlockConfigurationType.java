package com.teampls.shoplus.lib.enums;

/**
 * 차단 설정 가능한 기능 목록 데이터
 *
 * @author lucidite
 */
public enum FunctionBlockConfigurationType {
    NONE(""),

    /**
     * 안드로이드에서 엑셀로 거래내역을 내보낼 때 엑셀 편집을 고려한 고급 사용자용으로 내보내는 것을 허용할지 설정한다.
     */
    ANDROID_ADVANCED_EXCEL_EXPORTING("adr_adv_excel");

    private String remoteStr;

    FunctionBlockConfigurationType(String remoteStr) {
        this.remoteStr = remoteStr;
    }

    public String toRemoteStr() {
        return remoteStr;
    }

    public static FunctionBlockConfigurationType fromRemoteStr(String remoteStr) {
        if (remoteStr == null) {
            return NONE;
        }
        for (FunctionBlockConfigurationType function: FunctionBlockConfigurationType.values()) {
            if (function.remoteStr.equals(remoteStr)) {
                return function;
            }
        }
        return NONE;
    }
}
