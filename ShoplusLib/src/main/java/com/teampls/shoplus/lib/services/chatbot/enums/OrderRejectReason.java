package com.teampls.shoplus.lib.services.chatbot.enums;

/**
 * 매장에서 주문을 취소(거절)할 때 입력하는 사전 정의된 주문취소사유
 *
 * @author lucidite
 */
public enum OrderRejectReason {
    NONE("", ""),
    OUT_OF_STOCK("o", "재고부족"),
    DELIVERY_PROBLEM("d", "배송/물류문제"),
    INTERNAL_PROBLEM("i", "매장내부문제"),
    MISC("m", "기타(매장문의");

    private String remoteStr;
    private String uiStr;

    OrderRejectReason(String remoteStr, String uiStr) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
    }

    public String getRemoteStr() {
        return remoteStr;
    }

    public String getUiStr() {
        return uiStr;
    }

    public static OrderRejectReason fromRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty()) {
            return NONE;
        }
        String checkStr = remoteStr.toLowerCase();
        for (OrderRejectReason reason: OrderRejectReason.values()) {
            if (reason.remoteStr.equals(checkStr)) {
                return reason;
            }
        }
        return MISC;    // Fallback (빈 문자열이 아닌 경우)
    }
}
