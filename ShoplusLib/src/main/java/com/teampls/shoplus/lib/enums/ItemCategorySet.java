package com.teampls.shoplus.lib.enums;

import com.teampls.shoplus.lib.database_map.KeyValueDB;

/**
 * Created by Medivh on 2018-08-01.
 */

public enum ItemCategorySet {
    None(""), Cloth("의류"), Shoes("신발"), ETC("기타");

    public String uiStr = "";
    ItemCategorySet(String uiStr) {
        this.uiStr = uiStr;
    }

    public static  ItemCategorySet getFrom(KeyValueDB keyValueDB, String key) {
        try {
            return  ItemCategorySet.valueOf(keyValueDB.getValue(key));
        } catch (IllegalArgumentException e) {
            return ItemCategorySet.None;
        }
    }
}
