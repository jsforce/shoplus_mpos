package com.teampls.shoplus.lib.database_global;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.database.BaseDB;
import com.teampls.shoplus.lib.database.DBResult;
import com.teampls.shoplus.lib.database.MyQuery;
import com.teampls.shoplus.lib.enums.DBResultType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ImageDB extends BaseDB<ImageRecord> {
    private static ImageDB instance = null;
    protected static int Ver = 1;
    protected static int sizeLimit = 1000 + 100; // 1000개 + 즐겨찾기

    public static ImageDB getInstance(Context context) {
        if (instance == null)
            instance = new ImageDB(context);
        return instance;
    }

    public enum Column {
        _id, itemId(MyQuery.INT), remotePath, localPath, image(MyQuery.BLOB);

        public static String[] toStrs() {
            String[] columns = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++) {//
                columns[i] = Column.values()[i].toString();
            }
            return columns;
        }

        public static String[] toStrNoImage() {
            ArrayList<String> results = new ArrayList<>();
            for (Column column : Column.values()) {
                if (column.toString().equals("image"))
                    continue;
                results.add(column.toString());
            }
            return results.toArray(new String[0]);
        }

        private String attribution = MyQuery.TEXT_TYPE;
        Column(){};
        Column(String attribution) {
            this.attribution = attribution;
        }

        public static String[] getAttributions() {
            String[] attributions = new String[Column.values().length];
            for (int i = 0; i < Column.values().length; i++)
                attributions[i] = Column.values()[i].attribution;
            return attributions;
        }
    }

    private ImageDB(Context context) {
        super(context, "ImageDB", Ver, Column.toStrs());
        columnAttrs = Column.getAttributions();
        cleanUpBySize(sizeLimit);
    }

    protected ImageDB(Context context, String DBname, int DBversion, String[] columns) {
        super(context, DBname,DBversion, columns);
        columnAttrs = Column.getAttributions();
        cleanUpBySize(sizeLimit);
    }

    public void finish() {
        cleanUpBySize(sizeLimit);
        clearDuplications();
    }

    public DBHelper getDBHelper() {
        return new DBHelper(context, getTeamPlPath());
    }

    @Override
    public int delete(int id) {
        int itemId = getInt(Column.itemId, id);
        Log.w("DEBUG_JS", String.format("[%s.delete] [%d] %d", getClass().getSimpleName(), id, itemId));
        return super.delete(id);
    }

    public void delete(String imagePath) {
        delete(getId(Column.remotePath, imagePath));
    }

    public DBResult insert(ImageRecord record) {
        if (isOpen() == false) open();
        if (record.image.length/1000 >= 2000) { // 2MB
            Log.e("DEBUG_JS", String.format("[ImageDB.insertAll] blob %dkB >= 2000kB", record.image.length/1000));
            return new DBResult(0, DBResultType.INSERT_FAILED);
        }
        return super.insert(record);
    }

    @Override
    protected int getId(ImageRecord record) {
        return record.id;
    }

    public DBResult updateOrInsertByRemotePath(ImageRecord record) {
        if (this.hasValue(Column.remotePath, record.remotePath)) {
            ImageRecord dbRecord = getRecordBy(record.remotePath); // key
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return  new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            return insert(record);
        }
    }

    private Set<Integer> insertItemIds = new HashSet<>();

    public DBResult updateOrInsert(ImageRecord record) {
        if (this.hasValue(Column.itemId, record.itemId)) {
            if (insertItemIds.contains(record.itemId))
                Log.e("DEBUG_JS", String.format("[ImageDB.updateOrInsert] insertItemId %d found", record.itemId));
            ImageRecord dbRecord = getRecordBy(record.itemId); // key
            if (record.isSame(dbRecord) == false) {
                return update(dbRecord.id, record);
            } else {
                return  new DBResult(dbRecord.id, DBResultType.SKIPPED);
            }
        } else {
            insertItemIds.add(record.itemId);
            DBResult result= insert(record);
            insertItemIds.remove(record.itemId);
            return result;
        }
    }

    public ImageRecord getRecordBy(String imagePath) {
        return getRecord(getId(Column.remotePath, imagePath));
    }

    @Override
    protected ImageRecord getEmptyRecord() {
        return new ImageRecord();
    }

    @Override
    protected ImageRecord createRecord(Cursor cursor) {
        return new ImageRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getInt(Column.itemId.ordinal()),
                0, // imageId
                cursor.getString(Column.localPath.ordinal()),
                cursor.getString(Column.remotePath.ordinal()),
                cursor.getBlob(Column.image.ordinal()));
    }

    @Override
    protected ContentValues toContentvalues(ImageRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.itemId.toString(), record.itemId);
        contentvalues.put(Column.remotePath.toString(), record.remotePath);
        contentvalues.put(Column.localPath.toString(), record.localPath);
        contentvalues.put(Column.image.toString(), record.image);
        return contentvalues;
    }

    public ImageRecord getRecordBy(int itemId) {
        return getRecord(getId(Column.itemId,itemId));
    }

    public ImageRecord getNoImageRecordBy(int itemId) {
        return getNoImageRecord(getId(itemId));
    }

    public List<ImageRecord> getNoImageRecords() {
        ArrayList<ImageRecord> records = new ArrayList<ImageRecord>(0);
        Cursor cursor = getCursor(Column.toStrNoImage(), null, null, null, null, null, "");
        if (cursor == null) return records;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return records;
        }
        do {
            records.add(getNoImageRecord(cursor));
        } while (cursor.moveToNext());
        cursor.close();
        return records;
    }

    public ImageRecord getNoImageRecord(int id) {
        Cursor cursor = getCursor(Column.toStrNoImage(), KEY_ID + "=" + id, null, null, null, null, "");
        if (cursor == null)
            return new ImageRecord();
        ImageRecord record = getNoImageRecord(cursor);
        cursor.close();
        return record;
    }

    private ImageRecord getNoImageRecord(Cursor cursor) {
        ImageRecord result = new ImageRecord();
        if (cursor == null) {
            Log.e("DEBUG_JS", String.format("[ImageDB.getNoImageRecord] cursor == null"));
            return result;
        }

        if (cursor.getCount() <= 0) {
            return result;
        }
        int id = cursor.getInt(Column._id.ordinal());
        if (id == 0) {
            return result;
        }

        return new ImageRecord(id,
                cursor.getInt(Column.itemId.ordinal()),
                0,
                cursor.getString(Column.localPath.ordinal()),
                cursor.getString(Column.remotePath.ordinal()),
                Empty.bytes
        );
    }

    public int getId(int itemId) {
        return getId(Column.itemId, itemId);
    }

    public List<Integer> getIds(int itemId) {
        return getIds(Column.itemId, itemId);
    }

    public List<Integer> getItemIds() {
        return this.getIntegers(Column.itemId);
    }

    protected Bitmap getImageResized(int id, int widthRef) {
        byte[] image = getImage(ImageDB.Column.image, id);
        BitmapFactory.Options options = MyGraphics.getBitmapOption(image);
        int scaleFactor = MyGraphics.getScaleFactor(options.outWidth, options.outHeight, widthRef, MyGraphics.ScaleReference.width);
        Bitmap bitmap = MyGraphics.toBitmap(image, scaleFactor);
        if (bitmap.getWidth() > widthRef)
            bitmap = MyGraphics.resizeByWidth(bitmap, widthRef);
        return bitmap;
    }

    public void deleteInvalid(List<Integer> validItemIds) {
        for (int itemId : getItemIds()) {
            if (validItemIds.contains(itemId) == false)
                deleteBy(itemId);
        }
    }

    public void deleteBy(int itemId) {
        for (int id : this.getIds(Column.itemId, itemId)) {
            this.delete(id);
        }
    }

    public boolean hasImage(int itemId) {
        // if image == empty, then return false
        if (hasValue(Column.itemId, itemId) == false)
            return false;
        return (!Empty.isEmpty(getRecordBy(itemId).getBitmap()));
    }

    public boolean has(int itemId) {
        return getIntegers(Column.itemId).contains(itemId); // image를 가져오는걸 방지
    }

    public boolean has(String remotePath) {
        return hasValue(Column.remotePath, remotePath);
    }

    // 테스트 되지 않았음
    public void toFile(int itemId, String filePath) {
        List<Integer> ids = getIds(Column.itemId, itemId);
        if (ids.size() <= 0) return;
        MyGraphics.toFile(getImage(ImageDB.Column.image, ids.get(0)), filePath);
    }

    public void toLogCat(int location) {
        for (ImageRecord record : getRecords())
            toLogCat(record.id, record, location, "ImageDB.show");
    }


    public void toLogCat(String location) {
        for (ImageRecord record : getRecords())
            record.toLogCat(location);
    }
}
