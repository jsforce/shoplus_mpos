package com.teampls.shoplus.lib.database_global;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2016-10-01.
 */
public class OldAddressbookDB extends LocalUserDB {
    private static int Ver = 2;
    private static OldAddressbookDB instance = null;

    // 1 : App folder내 저장
    // 2 : common folder내 저장

    public static OldAddressbookDB getInstance(Context context) {
        if (instance == null)
            instance = new OldAddressbookDB(context);
        return instance;
    }

    private OldAddressbookDB(Context context) {
        super(context, "AddressbookDB", Ver, UserDB.Column.toStrs());
    }

    public List<LocalUserRecord> getRecordsExcept(List<String> ignorePhoneNumbers) {
        List<LocalUserRecord> results = new ArrayList<>();
        for (LocalUserRecord record : getRecords()) {
            if (ignorePhoneNumbers.contains(record.phoneNumber))
                continue;
            results.add(record);
        }
        return results;
    }

    public DBHelper getDBHelper() {
        return new DBHelper(context, getTeamPlPath());
    }

}
