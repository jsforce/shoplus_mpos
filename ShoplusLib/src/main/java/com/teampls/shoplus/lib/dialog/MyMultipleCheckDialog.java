package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.CheckableStringAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-08-08.
 */

abstract public class MyMultipleCheckDialog<T> extends BaseDialog implements AdapterView.OnItemClickListener {
    private ListView listView;
    private CheckableStringAdapter adapter;
    private List<T> records;

    public MyMultipleCheckDialog(Context context, String title, List<T> records) {
        super(context);
        setDialogWidth(0.95, 0.7);
        this.records = records;
        List<String> uiStrRecords = new ArrayList<>();
        for (T record : records)
            uiStrRecords.add(getUiString(record));

        adapter = new CheckableStringAdapter(context);
        adapter.setRecords(uiStrRecords);
        adapter.setChoiceMode(BaseDBAdapter.CHOICE_MODE_MULTIPLE);

        listView = (ListView) findViewById(R.id.dialog_listview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        findTextViewById(R.id.dialog_list_title, title);
        setOnClick(R.id.dialog_list_close);
        findViewById(R.id.dialog_list_message).setVisibility(View.GONE);
        findViewById(R.id.dialog_list_button_container).setVisibility(View.VISIBLE);
        setOnClick(R.id.dialog_list_button_cancel, R.id.dialog_list_button_apply);
        refresh();
        show();
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_list;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        adapter.onItemClick(view, position);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_list_close || view.getId() == R.id.dialog_list_button_cancel) {
            dismiss();
        } else if (view.getId() == R.id.dialog_list_button_apply) {
            List<T> selectedRecords = new ArrayList<>();
            for (int position : adapter.getClickedPositions())
                selectedRecords.add(records.get(position));
            onApplyClick(selectedRecords);
            dismiss();
        }
    }

    abstract public void onApplyClick(List<T> selectedRecords);

    protected abstract String getUiString(T record);

}
