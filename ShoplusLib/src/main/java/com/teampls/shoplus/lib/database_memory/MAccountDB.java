package com.teampls.shoplus.lib.database_memory;

import android.content.Context;
import android.text.TextUtils;

import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.database.AccountRecord;
import com.teampls.shoplus.lib.datatypes.AccountsData;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Medivh on 2018-05-11.
 */

public class MAccountDB extends BaseMapDB<String, AccountRecord> {
    private static MAccountDB instance;

    public static MAccountDB getInstance(Context context) {
        if (instance == null)
            instance = new MAccountDB(context);
        return instance;
    }

    private MAccountDB(Context context) {
        super(context, "MAccountDB");
    }

    @Override
    protected AccountRecord getEmptyRecord() {
        return new AccountRecord();
    }

    @Override
    public String getKeyValue(AccountRecord record) {
        return record.counterpartPhoneNumber;
    }

    public void set(Map<String, AccountsData> situation, MyMap<String, String> nameMap) {
        clear();
        for (String phoneNumber : situation.keySet()) {
            AccountsData data = situation.get(phoneNumber);
            String displayName = nameMap.get(phoneNumber);
            AccountRecord record = new AccountRecord(phoneNumber, data, TextUtils.isEmpty(displayName)? "" : displayName);
            insert(record);
        }
    }

    public List<AccountRecord> getRecordsOrderBy(MSortingKey sortingKey, boolean isDescending) {
        List<AccountRecord> records = getRecords();
        for (AccountRecord record : records)
            record.sortingKey = sortingKey;
        Collections.sort(records);
        if (isDescending)
            Collections.reverse(records);
        return records;
    }
}
