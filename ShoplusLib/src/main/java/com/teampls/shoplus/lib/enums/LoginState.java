package com.teampls.shoplus.lib.enums;

/**
 * Created by Medivh on 2018-01-21.
 */

public enum LoginState {
    Default, onTrying, onSuccess, onAwsError, notAuthorized, userNotFound, onError, Disconnected;
}
