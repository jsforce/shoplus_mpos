package com.teampls.shoplus.lib.enums;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * (매장에 대한) 사용자 레벨
 *
 * @author lucidite
 */

public enum UserLevelType {
    /**
     * 로그인한 사용자가 매장 번호 자신인 경우 (기본적으로 종업원과 동일한 권한을 가져야 한다)
     */
    SHOP("shop", "매장", 0),
    /**
     * 로그인한 사용자가 매장에 등록된 종업원인 경우
     */
    EMPLOYEE("employee", "직원", 0),
    /**
     * 로그인한 사용자가 매장에 등록된 지배인인 경우
     */
    MANAGER("manager", "관리자", 1),
    /**
     * 로그인한 사용자가 매장에 등록된 사장인 경우
     */
    MASTER("master", "사장님", 2);

    private String remoteStr;
    private String uiStr;
    private int permissionLevel;

    UserLevelType(String remoteStr, String uiStr, int permissionLevel) {
        this.remoteStr = remoteStr;
        this.uiStr = uiStr;
        this.permissionLevel = permissionLevel;
    }

    public String toRemoteStr() {
        return this.remoteStr;
    }

    public String toUIStr() {
        return this.uiStr;
    }

    public int getPermissionLevel() {
        return permissionLevel;
    }

    @NonNull
    public static UserLevelType fromRemoteStr(String remoteStr) {
        if (remoteStr == null) {
            return SHOP;
        }
        for (UserLevelType userLevel: UserLevelType.values()) {
            if (remoteStr.equals(userLevel.remoteStr)) {
                return userLevel;
            }
        }
        return SHOP;       // fallback
    }

    /**
     * 원격 문자열에 따른 정확한 UserLevelType을 반환한다.
     * fromRemoteStr과는 달리, 문자열이 null이거나 일치하는 타입이 없는 경우 null을 반환한다.
     *
     * @param remoteStr
     * @return
     */
    @Nullable
    public static UserLevelType exactTypeFromRemoteStr(String remoteStr) {
        if (remoteStr == null) {
            return null;
        }

        for (UserLevelType userLevel: UserLevelType.values()) {
            if (remoteStr.equals(userLevel.remoteStr)) {
                return userLevel;
            }
        }
        return null;
    }

    public boolean isAllowed(UserLevelType allowedUserLevel) {
        switch (allowedUserLevel) {
            case MASTER:
                return (this == MASTER); // 허용 권한이 마스터이면 내가 마스터이어야만 됨
            case MANAGER:
                return (this == MASTER) || (this == MANAGER);
            default:
                return true; //
        }
    }
}
