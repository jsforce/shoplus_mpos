package com.teampls.shoplus.lib.view_base;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseItemOptionDBAdapter;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyMap;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.QueryOrderRecord;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view_module.MyRadioGroup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2018-09-20.
 */

abstract public class BaseCreationView extends BaseActivity {
    protected ListView listView;
    protected BaseItemOptionDBAdapter adapter;
    protected TextView tvTotalCount;
    protected MyRadioGroup<Incrementals> incremetalsGroup;
    protected String KEY_Incremental = "BaseCreationView.incremental";
    protected static List<ItemRecord> selectedItemRecords = new ArrayList<>();
    protected static MyMap<ItemOptionRecord, Integer> selectedOptionMap = new MyMap<>(0);
    protected static MyOnTask onTask;
    protected ItemDB itemDB;

    protected enum Incrementals {
        i1, i10, i100;

        public int getValue() {
            return BaseUtils.toInt(toString().replace("i", ""), 1);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstance(this) == false) return;
        myActionBar.hide();
        setAdapter();

        listView = findViewById(R.id.creation_listview);
        listView.setAdapter(adapter);
        tvTotalCount = findTextViewById(R.id.creation_totalCount);
        setHeaderBackground(R.id.creation_headerContainer);

        setOnClick(R.id.creation_apply, R.id.creation_prev,
                R.id.creation_close, R.id.creation_to0);

        incremetalsGroup = new MyRadioGroup(context, getView(), 1.0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyValueDB.put(KEY_Incremental, view.getTag().toString());
                adapter.setIncrementalOfCreationView(((Incrementals) view.getTag()).getValue());
                adapter.notifyDataSetChanged();
            }
        });
        incremetalsGroup.add(R.id.creation_i1, Incrementals.i1)
                .add(R.id.creation_i10, Incrementals.i10).add(R.id.creation_i100, Incrementals.i100);
        Incrementals initValue = Incrementals.valueOf(keyValueDB.getValue(KEY_Incremental, Incrementals.i1.toString()));
        incremetalsGroup.setChecked(initValue);
        adapter.setIncrementalOfCreationView(initValue.getValue());
    }

    @Override
    public int getThisView() {
        return R.layout.activity_creation;
    }

    abstract protected void setAdapter();

    protected void setAdapterByItems() {
        // itemRecords
        if (selectedItemRecords.isEmpty()) {
            Set<Integer> itemIds = new HashSet<>();
            for (ItemOptionRecord optionRecord : selectedOptionMap.keySet())
                itemIds.add(optionRecord.itemId);
            selectedItemRecords = itemDB.getRecordsIN(ItemDB.Column.itemId, new ArrayList(itemIds),
                    new QueryOrderRecord(ItemDB.Column.itemId, true, false).toList());
        }
        adapter.filterItemRecords(selectedItemRecords);
    }

    protected void setAdapterByOptions() {
        // optionsRecords
        if (selectedOptionMap.isEmpty()) {
            for (ItemRecord itemRecord : selectedItemRecords)
                for (ItemOptionRecord optionRecord : itemDB.options.getRecordsBy(itemRecord.itemId))
                    selectedOptionMap.put(optionRecord, 0);
        }
        adapter.filterItemOptionRecords(new ArrayList(selectedOptionMap.keySet()));
    }

    public void refresh() {
        if (adapter == null) return;
        new MyAsyncTask(context, "refresh") {
            @Override
            public void doInBackground() {
                adapter.generate(false);
            }

            @Override
            public void onPostExecutionUI() {
                adapter.applyGeneratedRecords();
                adapter.notifyDataSetChanged();
                refreshHeader();
            }
        };
    }

    public void refreshHeader() {
        int totalCount = 0;
        for (Integer count : adapter.optionCountMap.values())
            totalCount = totalCount + count;
        tvTotalCount.setText(String.format("총 %d장", totalCount));
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.creation_apply) {
            onApplyClick();
        } else if (view.getId() == R.id.creation_prev) {
            finish();
        } else if (view.getId() == R.id.creation_close) {
            finish();
        } else if (view.getId() == R.id.creation_to0) {
            onToZeroClick();
        }
    }

    abstract protected void onApplyClick();

    protected void onToZeroClick() {
        adapter.optionCountMap.clear();
        refresh();
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

}
