package com.teampls.shoplus.lib.database;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.database.MonthlyCounterpartDB;
import com.teampls.shoplus.lib.database.MonthlyCounterpartRecord;
import com.teampls.shoplus.lib.database.QueryOrderRecord;
import com.teampls.shoplus.lib.view.MyView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Medivh on 2016-09-18.
 */
public class MonthlyCounterpartDBAdapter extends BaseDBAdapter<MonthlyCounterpartRecord> {
    protected MonthlyCounterpartDB monthlyCounterpartDB;
    private boolean doSumUp = false, doFilterMonth = false;
    private DateTime monthDateTime = Empty.dateTime;
    private boolean isPercentMode = false;
    private int sum = 0;

    public MonthlyCounterpartDBAdapter(Context context) {
        super(context);
        monthlyCounterpartDB = monthlyCounterpartDB.getInstance(context);
        orderRecords.add(new QueryOrderRecord(MonthlyCounterpartDB.Column.amount, true, true));
    }

    public void setPercentMode(boolean value) {
        this.isPercentMode = value;
    }

    public void filterMonth(DateTime monthDateTime) {
        doSumUp = false;
        doFilterMonth = true;
        this.monthDateTime = monthDateTime;
    }

    public void setSumUp(boolean value) {
        this.doSumUp = value;
    }

    @Override
    public void generate() {
        List<MonthlyCounterpartRecord> counterpartRecords = new ArrayList<>();
        if (doSumUp) {
            if (doFilterMonth)
                counterpartRecords  = monthlyCounterpartDB.getRecordsOrderBy(MonthlyCounterpartDB.Column.monthDateTime, true);
        } else {
            counterpartRecords  = monthlyCounterpartDB.getRecordsBy(MonthlyCounterpartDB.Column.monthDateTime, monthDateTime.getYear(), monthDateTime.getMonthOfYear(), orderRecords);
        }

        List<MonthlyCounterpartRecord> _records = new ArrayList<>();
        sum = 0;
        for (MonthlyCounterpartRecord record : counterpartRecords) {
            sum = record.amount + sum;
            _records.add(record);
        }
        records = _records;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new BuyerSummaryViewHolder();
    }

    class BuyerSummaryViewHolder extends BaseViewHolder {
        private TextView tvNum, tvName, tvQuantity, tvSum, tvMarginPercent;

        @Override
        public int getThisView() {
            return R.layout.row_item_summary;
        }

        @Override
        public void init(View view) {
            tvNum = (TextView) view.findViewById(R.id.row_item_summary_num);
            tvName = (TextView) view.findViewById(R.id.row_item_summary_name);
            tvQuantity = (TextView) view.findViewById(R.id.row_item_summary_quantity);
            tvSum = (TextView) view.findViewById(R.id.row_item_summary_sum);
            tvMarginPercent = (TextView) view.findViewById(R.id.row_item_summary_margin);
            tvMarginPercent.setVisibility(View.GONE);
            MyView.setTextViewByDeviceSize(context, tvNum, tvName, tvQuantity, tvSum, tvMarginPercent);
        }

        @Override
        public void update(int position) {
            if (position >= records.size() || position < 0) return;
            MonthlyCounterpartRecord record = getRecord(position);
            tvNum.setText(Integer.toString(position+1));
            tvName.setText(record.name);
            tvQuantity.setText(Integer.toString(record.quantity));
            if (isPercentMode)
                tvSum.setText(sum == 0? "" : String.format("%d%%", ((long) record.amount *100)/sum));
            else
                tvSum.setText(BaseUtils.toCurrencyOnlyStr(record.amount));
        }
    }

}
