package com.teampls.shoplus.lib.database_memory;

import android.content.Context;

import com.teampls.shoplus.lib.common.MyMap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2018-04-17.
 */

abstract public class BaseMapDB<K, V>  extends BaseMemoryDB<V> {
    protected MyMap<K, V> databaseMap;

    public BaseMapDB(Context context, String dbName) {
        this.context = context;
        this.DB_NAME = dbName;
        databaseMap = new MyMap<>(getEmptyRecord());
    }

    public boolean isEmpty() {
        return (databaseMap == null || databaseMap.size() == 0);
    }

    abstract public K getKeyValue(V record);

    public void insert(V record) {
        databaseMap.put(getKeyValue(record), record);
    }

    public void update(V record) {
        K key = getKeyValue(record);
        if (hasKey(key)) // 있을때만 업데이트
            databaseMap.put(key, record);
    }

    public void insertAll(List<V> records) {
        for (V record : records)
            insert(record);
    }

    public void updateOrInsert(V record) {
        if (hasKey(getKeyValue(record))) {
            update(record);
        } else {
            insert(record);
        }
    }

    public void updateOrInsertAll(List<V> records) {
        for (V record : records)
            updateOrInsert(record);
    }

    public void delete(V record) {
        databaseMap.remove(getKeyValue(record));
    }

    public void deleteAll(List<V> records) {
        Set<K> keys = new HashSet<>();
        for (V record : records)
            keys.add(getKeyValue(record));
        databaseMap.keySet().removeAll(keys);
    }

    public void deleteByKey(String key) {
        databaseMap.remove(key);
    }

    public void clear() {
        databaseMap.clear();
    }


    public List<V> getRecords() {
        return new ArrayList(databaseMap.values());
    }

    public V getRecordByKey(String keyValue) {
        if (databaseMap.containsKey(keyValue)) {
            return databaseMap.get(keyValue);
        } else {
            return getEmptyRecord();
        }
    }

    public boolean hasKey(K keyValue) {
        return (databaseMap.containsKey(keyValue));
    }

    public Set<K> getKeys() {
        return databaseMap.keySet();
    }
}
