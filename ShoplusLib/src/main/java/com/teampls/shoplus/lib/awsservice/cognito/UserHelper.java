/*
 *  Copyright 2013-2016 Amazon.com,
 *  Inc. or its affiliates. All Rights Reserved.
 *
 *  Licensed under the Amazon Software License (the "License").
 *  You may not use this file except in compliance with the
 *  License. A copy of the License is located at
 *
 *      http://aws.amazon.com/asl/
 *
 *  or in the "license" file accompanying this file. This file is
 *  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, express or implied. See the License
 *  for the specific language governing permissions and
 *  limitations under the License.
 */

package com.teampls.shoplus.lib.awsservice.cognito;

import android.content.Context;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.auth.CognitoCredentialsProvider;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.regions.Region;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProvider;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProviderClient;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;

import java.util.HashMap;
import java.util.Map;

public class UserHelper {
    private static UserHelper userHelper;
    private static CognitoUserPool userPool;
    private static CognitoCredentialsProvider credentialsProvider = null;

    // User details from the service
    private static CognitoUserSession currSession;

    public static void init(Context context) {
        if (userHelper != null && userPool != null) {
            return;
        }

        if (userHelper == null) {
            userHelper = new UserHelper();
        }

        if (userPool == null) {
            ClientConfiguration clientConfiguration = new ClientConfiguration();
            AmazonCognitoIdentityProvider cipClient = new AmazonCognitoIdentityProviderClient(
                    new AnonymousAWSCredentials(), clientConfiguration);
            cipClient.setRegion(Region.getRegion(MyAWSConfigs.getCurrentConfig().getUserPoolRegion()));
            userPool = new CognitoUserPool(context, MyAWSConfigs.getCurrentConfig().getUserPoolId(),
                    MyAWSConfigs.getCurrentConfig().getUserPoolClientId(), MyAWSConfigs.getCurrentConfig().getUserPoolClientSecret(), cipClient);
        }
    }

    private static String getCredentialsLoginProviderKey(String userPoolRegionName, String userPoolId) {
        return "cognito-idp." + userPoolRegionName + ".amazonaws.com/" + userPoolId;
    }

    public static void updateCredentialsLogin(Context context) {
        credentialsProvider = new CognitoCachingCredentialsProvider(context,
                MyAWSConfigs.getCurrentConfig().getIdentityPoolId(), MyAWSConfigs.getCurrentConfig().getIdentityPoolRegion());
        Map<String, String> logins = new HashMap<>();
        logins.put(
                getCredentialsLoginProviderKey(MyAWSConfigs.getCurrentConfig().getUserPoolRegion().getName(),
                        MyAWSConfigs.getCurrentConfig().getUserPoolId()),
                getIdentityToken());
        credentialsProvider.setLogins(logins);

        ClientConfiguration clientConfiguration = new ClientConfiguration();
        AmazonCognitoIdentityProvider cipClient = new AmazonCognitoIdentityProviderClient(
                credentialsProvider.getCredentials(), clientConfiguration);
        cipClient.setRegion(Region.getRegion(MyAWSConfigs.getCurrentConfig().getUserPoolRegion()));

        userPool = new CognitoUserPool(context, MyAWSConfigs.getCurrentConfig().getUserPoolId(),
                MyAWSConfigs.getCurrentConfig().getUserPoolClientId(), MyAWSConfigs.getCurrentConfig().getUserPoolClientSecret(), cipClient);
    }

    public static CognitoUserPool getPool() {
        return userPool;
    }

    public static void setCurrSession(CognitoUserSession session) {
        currSession = session;
    }

    public static CognitoUserSession getCurrSession() {
        return currSession;
    }

    public static CognitoCredentialsProvider getCredentialsProvider() {
        return credentialsProvider;
    }

    public static String getIdentityToken() {
        //Log.i("DEBUG_TAG", "TOKEN====> " + getCurrSession().getIdToken().getJWTToken());
        return getCurrSession().getIdToken().getJWTToken();
    }
}

