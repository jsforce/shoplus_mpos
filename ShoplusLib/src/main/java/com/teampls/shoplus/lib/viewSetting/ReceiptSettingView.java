package com.teampls.shoplus.lib.viewSetting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyMenu;

/**
 * Created by Medivh on 2018-10-06.
 */

public class ReceiptSettingView extends BaseSettingView {

    public static void startActivity(Context context) {
        ((Activity) context).startActivityForResult(new Intent(context, ReceiptSettingView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        doFinishForResult();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseAppWatcher.checkInstanceAndRestart(this) == false) return;
        myActionBar.setTitle("영수증 출력 설정");
        LinearLayout container = findViewById(R.id.receipts_container);
        addCheckBoxs(container, false);
        setOnClick(R.id.receipts_close);
    }



    @Override
    public int getThisView() {
        return R.layout.activity_receipts;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.receipts_close) {
            doFinishForResult();
        }
    }

    @Override
    public void onMyMenuCreate() {
        myActionBar
                .add(MyMenu.contactUs)
                .add(MyMenu.close);
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        switch (myMenu) {
            case close:
                doFinishForResult();
                break;
        }
    }
}
