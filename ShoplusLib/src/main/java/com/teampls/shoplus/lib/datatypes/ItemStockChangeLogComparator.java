package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayHelper;

import java.util.Comparator;

/**
 * @author lucidite
 */
public class ItemStockChangeLogComparator implements Comparator<ItemStockChangeLogProtocol> {
    @Override
    public int compare(ItemStockChangeLogProtocol o1, ItemStockChangeLogProtocol o2) {
        String updated1 = APIGatewayHelper.getRemoteDateTimeString(o1.getUpdatedDateTime());
        String updated2 = APIGatewayHelper.getRemoteDateTimeString(o2.getUpdatedDateTime());
        return -updated1.compareTo(updated2);
    }
}
