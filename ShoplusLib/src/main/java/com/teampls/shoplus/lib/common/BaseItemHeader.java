package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.adapter.BaseItemDBAdapter;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.lib.dialog.CategoryDialog;
import com.teampls.shoplus.lib.dialog.MyCheckableListDialog;
import com.teampls.shoplus.lib.dialog.SortingOrderDialog;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ItemCategoryType;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.enums.ItemSortingKey;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseItemSearchByName;
import com.teampls.shoplus.lib.view_base.BaseItemsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2018-08-05.
 */

abstract public class BaseItemHeader implements View.OnClickListener {
    private Context context;
    private final int viewId = R.layout.module_item_header;
    public TextView tvFilterCategory, tvFilterName, tvSorting, tvFilterState;
    private KeyValueDB keyValueDB;
    private BaseItemDBAdapter adapter;
    private String KEY_FILTER_NAME = "key.filter.name", KEY_FILTER_STATE = "key.filter.state";
    private List<ItemSortingKey> sortingKeys = new ArrayList<>();
    private ItemSortingKey selectedSortingKey = ItemSortingKey.ItemId;
    private BaseItemsActivity parentActivity;

    public BaseItemHeader(BaseItemsActivity activity, View view, BaseItemDBAdapter adapter) {
        this((Context) activity, view, adapter);
        this.parentActivity = activity;
    }

    private BaseItemHeader(Context context, View view, BaseItemDBAdapter adapter) {
        this.context = context;
        this.adapter = adapter;
        keyValueDB = KeyValueDB.getInstance(context);
        keyValueDB.put(KEY_FILTER_NAME, "");
        keyValueDB.put(KEY_FILTER_STATE, ItemStateType.DISPLAYED.toString());
        tvFilterState = (TextView) view.findViewById(R.id.item_header_state);
        tvFilterCategory = (TextView) view.findViewById(R.id.item_header_category);
        tvFilterName = (TextView) view.findViewById(R.id.item_header_name);
        tvSorting = (TextView) view.findViewById(R.id.item_header_sorting);
        MyView.setFontSizeByDeviceSize(context, tvFilterState, tvFilterCategory, tvFilterName, tvSorting);
        tvFilterState.setOnClickListener(this);
        tvFilterCategory.setOnClickListener(this);
        tvFilterName.setOnClickListener(this);
        tvSorting.setOnClickListener(this);
    }

    public void setSortingKeys(ItemSortingKey... keys) {
        sortingKeys.clear();
        for (ItemSortingKey sortingKey : keys)
            sortingKeys.add(sortingKey);
    }

    private void onNameClick() {

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.item_header_state) {
            // 주의 null이 이 밖으로 나가서는 안된다
            new MyCheckableListDialog<ItemStateType>(context, "상품 상태별 보기",BaseUtils.toList(ItemStateType.values()),
                    adapter.getFilterStates().size() == 1?  adapter.getFilterStates().get(0) : null, "모든상태") {
                @Override
                public void onItemClicked(ItemStateType selectedRecord) {
                    if (selectedRecord == null) {
                        adapter.filterStates(ItemStateType.values());
                    } else {
                        adapter.filterStates(selectedRecord);
                    }
                    refreshAdapter();
                }

                @Override
                protected String getUiString(ItemStateType record) {
                    return record.toUIStr();
                }
            };

        } else if (view.getId() == R.id.item_header_category) {
            new CategoryDialog(context, "분류별 상품보기", adapter.getRecords(), true, true, true) {
                @Override
                public void onItemClicked(ItemCategoryType category) {
                    adapter.filterCategory(category);
                    refreshAdapter();
                    onFilterCategoryClick(category);
                }
            };

        } else if (view.getId() == R.id.item_header_name) {
            ItemSearchType itemSearchType = ItemSearchType.ByName; //ItemSearchType.getFrom(keyValueDB, KEY_FILTER_NAME);
            BaseItemSearchByName.startActivity(context, itemSearchType, KEY_FILTER_NAME,
                    true, true, new MyOnClick<ItemRecord>() {
                        @Override
                        public void onMyClick(View view, ItemRecord record) {
                            // single item clicked ... 필터를 사용하지 않고 바로 찾는 아이템을 보여준다
                            adapter.resetFiltering();
                            if (record.name.equals(BaseItemSearchByName.KEY_SHOW_ALL)) {
                                refreshAdapter();
                            } else {
                                boolean found = false;
                                int position = 0;
                                for (ItemRecord adapterRecord : adapter.getRecords()) {
                                    if (adapterRecord.itemId == record.itemId) {
                                        onItemClickParent(position);
                                        found = true;
                                        onFilterNameClick(position);
                                        break;
                                    }
                                    position++;
                                }

                                // 기존에 없었던 아이템 (검색으로 새로 찾은 아이템)
                                if (found == false) {
                                    adapter.addRecord(record);
                                    onItemClickParent(adapter.getCount() - 1);
                                    onFilterNameClick(position);
                                }
                            }

                        }
                    }, null);

        } else if (view.getId() == R.id.item_header_sorting) {
            new SortingOrderDialog(context, "다음 순서로 보기", selectedSortingKey, sortingKeys) {
                @Override
                public void onItemClicked(ItemSortingKey sortingKey) {
                    if (selectedSortingKey == ItemSortingKey.RecentVisited) // 최근것 보기에서 탈출
                        adapter.resetRecords();
                    selectedSortingKey = sortingKey;
                    adapter.setSortingKey(selectedSortingKey.mSortingKey);
                    switch (selectedSortingKey) {
                        default:
                            adapter.enableCountMap(false);
                            break;
                        case StockCount:
                            adapter.autoSetCountMap();
                            break;
                    }
                    refreshAdapter();
                    setAdapterUiBySortingKey();
                    refresh();
                    dismiss();
                    onSortingClicked(sortingKey);
                }
            };

        }
    }

    public void onSortingClicked(ItemSortingKey sortingKey) {

    };


    private void setAdapterUiBySortingKey() {
        adapter.resetShowing();
        adapter.showName().showPrice().showState();
        switch (selectedSortingKey) {
            default:
                break;
            case ItemId:
                adapter.showDate();
                break;
            case Name:
                adapter.showChar();
                break;
            case RecentVisited:
                break;
            case StockCount:
                break;
        }
    }

    protected void onItemClickParent(int position) {
        if (parentActivity != null)
            parentActivity.onItemClick(null, null, position, 0);
    }

    public void onFilterCategoryClick(ItemCategoryType category) {
    };



    abstract public void refreshAdapter();

    public void onFilterNameClick(int position) {
    };

    public void refresh() {
        if (adapter.getFilterStates().size() == 1) {
            tvFilterState.setText(adapter.getFilterStates().get(0).toUIStr());
            tvFilterState.setTextColor(ColorType.Blue.colorInt);
        } else if (adapter.getFilterStates().size() > 1 || adapter.getFilterStates().size() == 0) {
            tvFilterState.setText("모든상태");
            tvFilterState.setTextColor(ColorType.Black.colorInt);
        }

        switch (ItemSearchType.getFrom(keyValueDB, KEY_FILTER_NAME)) {
            case ByName:
                tvFilterName.setText("이름");
                tvFilterName.setTextColor(ColorType.Black.colorInt);
                break;
            case BySerialNum:
                tvFilterName.setText("번호");
                tvFilterName.setTextColor(ColorType.Blue.colorInt);
                break;
        }

        tvFilterCategory.setText("분류");
        tvFilterCategory.setTextColor(ColorType.Black.colorInt);
        if (adapter.isCategoryFiltered()) {
            tvFilterCategory.setText(adapter.getFilterCategory().toUIStr());
            tvFilterCategory.setTextColor(ColorType.Blue.colorInt);
        }

        tvSorting.setText(selectedSortingKey.uiStr);
        switch (selectedSortingKey) {
            default:
                tvSorting.setTextColor(ColorType.Blue.colorInt);
                break;
            case ItemId:
                tvSorting.setTextColor(ColorType.Black.colorInt);
                break;
        }

    }

}
