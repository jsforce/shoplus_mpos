package com.teampls.shoplus.lib.datatypes;

import com.teampls.shoplus.lib.awsservice.apigateway.protocol.JSONDataWrapper;
import com.teampls.shoplus.lib.enums.MemberPermission;
import com.teampls.shoplus.lib.enums.UserLevelType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 미송앱 관련 권한설정 데이터
 *
 * @author lucidite
 */

public class ProjectDalaranAuthData implements JSONDataWrapper {
    private UserLevelType onlineBalanceChangePermission;
    private UserLevelType entrustedBalanceChangePermission;
    private UserLevelType depositChangePermission;

  //  private static UserLevelType DALARAN_DEFAULT_PERMISSION = UserLevelType.MASTER;

    public ProjectDalaranAuthData(){}

    public ProjectDalaranAuthData(JSONObject dataObj) {
        if (dataObj == null) {
            this.onlineBalanceChangePermission = MemberPermission.ChangeOnline.getDefaultUserLevel();
            this.entrustedBalanceChangePermission = MemberPermission.ChangeEntrusted.getDefaultUserLevel();
            this.depositChangePermission = MemberPermission.ChangeDeposit.getDefaultUserLevel();
        } else {
            String onlineBalanceChangeStr = dataObj.optString("change_online", "");
            String entrustedBalanceChangeStr = dataObj.optString("change_entrusted", "");
            String depositChangeStr = dataObj.optString("change_deposit", "");
            this.onlineBalanceChangePermission = UserLevelType.exactTypeFromRemoteStr(onlineBalanceChangeStr);
            this.entrustedBalanceChangePermission = UserLevelType.exactTypeFromRemoteStr(entrustedBalanceChangeStr);
            this.depositChangePermission = UserLevelType.exactTypeFromRemoteStr(depositChangeStr);
            this.checkNullValue();
        }
    }

    public ProjectDalaranAuthData(UserLevelType onlineBalanceChangePermission, UserLevelType entrustedBalanceChangePermission, UserLevelType depositChangePermission) {
        this.onlineBalanceChangePermission = onlineBalanceChangePermission;
        this.entrustedBalanceChangePermission = entrustedBalanceChangePermission;
        this.depositChangePermission = depositChangePermission;
        this.checkNullValue();
    }

    private void checkNullValue() {
        if (this.onlineBalanceChangePermission == null) {
            this.onlineBalanceChangePermission = MemberPermission.ChangeOnline.getDefaultUserLevel();
        }
        if (this.entrustedBalanceChangePermission == null) {
            this.entrustedBalanceChangePermission = MemberPermission.ChangeEntrusted.getDefaultUserLevel();
        }
        if (this.depositChangePermission == null) {
            this.depositChangePermission = MemberPermission.ChangeDeposit.getDefaultUserLevel();
        }
    }

    public UserLevelType getOnlineBalanceChangePermission() {
        return onlineBalanceChangePermission;
    }

    public UserLevelType getEntrustedBalanceChangePermission() {
        return entrustedBalanceChangePermission;
    }

    public UserLevelType getDepositChangePermission() {
        return depositChangePermission;
    }

    public void setOnlineBalanceChangePermission(UserLevelType onlineBalanceChangePermission) {
        this.onlineBalanceChangePermission = onlineBalanceChangePermission;
        this.checkNullValue();
    }

    public void setEntrustedBalanceChangePermission(UserLevelType entrustedBalanceChangePermission) {
        this.entrustedBalanceChangePermission = entrustedBalanceChangePermission;
        this.checkNullValue();
    }

    public void setDepositChangePermission(UserLevelType depositChangePermission) {
        this.depositChangePermission = depositChangePermission;
        this.checkNullValue();
    }

    @Override
    public JSONObject getObject() throws JSONException {
        return new JSONObject()
                 .put("change_online", this.onlineBalanceChangePermission.toRemoteStr())
                 .put("change_entrusted", this.entrustedBalanceChangePermission.toRemoteStr())
                 .put("change_deposit", this.depositChangePermission.toRemoteStr());
    }

    @Override
    public JSONArray getArray() throws JSONException {
        throw new JSONException("ProjectDalaranAuthData instance does not have a JSON array");
    }
}
