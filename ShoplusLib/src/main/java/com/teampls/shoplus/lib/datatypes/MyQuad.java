package com.teampls.shoplus.lib.datatypes;

/**
 * Created by Medivh on 2017-09-01.
 */

public class MyQuad<F, S, T, R> {
    public F first;
    public S second;
    public T third;
    public R fourth;

    public static <F, S, T, R> MyQuad<F, S, T, R> create(F first, S second, T third, R fourth) {
        MyQuad result = new MyQuad();
        result.first = first;
        result.second = second;
        result.third = third;
        result.fourth = fourth;
        return result;
    }

}
