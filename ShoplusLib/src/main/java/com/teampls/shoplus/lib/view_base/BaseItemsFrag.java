package com.teampls.shoplus.lib.view_base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.teampls.shoplus.lib.composition.ItemGridComposition;

/**
 * Created by Medivh on 2016-07-21.
 * PosSlipCreationView.ItemsView (silvermoon), SortedItemsView (stormwind?) 도 같은 view
 */
abstract public class BaseItemsFrag extends BaseFragment implements AdapterView.OnItemClickListener {
    protected Context context;
    protected String title = "";
    public ItemGridComposition itemGridComposition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        itemGridComposition = new ItemGridComposition(context, this);
        setAdaptor(); 
    }

    protected abstract void setAdaptor();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(itemGridComposition.thisView, container, false);
        itemGridComposition.onCreateView(view);
        setUiComponents(view);
        refresh();
        isViewCreated = true;
        return view;
    }

    protected void setUiComponents(View view) {

    }

    @Override
    public void onClick(View view) {
        if (view == null) return;
        itemGridComposition.onClick(view);
    }

}

