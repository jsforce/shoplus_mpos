package com.teampls.shoplus.lib.awsservice.s3.datamodel;

/**
 * 원본 이미지와 섬네일 이미지 경로의 세트 데이터 프로토콜. 로컬 및 원격 경로에 모두 사용한다.
 *
 * @author lucidite
 */

public interface FilePathPairProtocol {
    String getOriginalFilePath();
    String getThumbnailFilePath();
}
