package com.teampls.shoplus.lib.database_map;

import android.content.Context;

/**
 * Created by Medivh on 2018-11-19.
 */

public class KeyValueBoolean extends BaseDBValue<Boolean> {

    public KeyValueBoolean(String key) {
        this(key,false);
    }

    public KeyValueBoolean(String key, boolean defaultValue) {
        super(key, defaultValue);
    }

    public void setDefaultValue(boolean defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public KeyValueDB getDB(Context context) {
        return KeyValueDB.getInstance(context);
    }

    @Override
    public void put(Context context, Boolean value) {
        if (key.isEmpty() == false)
            getDB(context).put(key, value);
    }

    public Boolean get(Context context) {
        if (key.isEmpty())
            return false;
        return getDB(context).getBool(key, defaultValue);
    }

}
