package com.teampls.shoplus.lib.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database_global.GlobalDB;
import com.teampls.shoplus.lib.database_global.ImageDB;
import com.teampls.shoplus.lib.database_global.ImageRecord;
import com.teampls.shoplus.lib.database_global.SlipImageDB;
import com.teampls.shoplus.lib.database_global.ThumbImageDB;
import com.teampls.shoplus.lib.datatypes.MerchandiseData;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.ImageDataProtocol;

/**
 * Created by Medivh on 2017-03-16.
 */

public class MyImageLoader {
    protected static MyImageLoader instance = null;
    protected ImageDB images;
    protected ThumbImageDB thumbImages;
    protected SlipImageDB slipImages;
    protected static Context context;
    protected final int THUMB_SCALE = 4; // 1/4
    protected GlobalDB globalDB;
    protected UserSettingData userSettingData;

    public static MyImageLoader getInstance(Context context) {
        if (instance == null)
            instance = new MyImageLoader(context);
        MyImageLoader.context = context;
        return instance;
    }

    protected MyImageLoader(Context context) {
        globalDB = GlobalDB.getInstance(context);
        images = ImageDB.getInstance(context);
        thumbImages = ThumbImageDB.getInstance(context);
        //  paths = SubImagePathDB.getInstance(context);
        slipImages = SlipImageDB.getInstance(context);
        userSettingData = UserSettingData.getInstance(context);
    }

    public void retrieveSlip(final String remotePath, final MyOnTask<ImageRecord> onTask) {
        if (slipImages.has(remotePath)) {
            ImageRecord record = slipImages.getRecordBy(remotePath);
            if (onTask != null)
                onTask.onTaskDone(record);
        } else {
            if (BaseAppWatcher.isNetworkConnected() == false) {
                MyUI.toastSHORT(context, String.format("오프라인 입니다"));
                return;
            }
            new MyServiceTask(context) {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    ImageDataProtocol imageDataProtocol = itemService.downloadSlipImage(remotePath);
                    final ImageRecord record = new ImageRecord(0, "", remotePath, imageDataProtocol.getByteArray());
                    slipImages.updateOrInsertByRemotePath(record);
                    if (onTask == null) return;
                    new MyUITask(context) {
                        @Override
                        public void doInBackground() {
                            onTask.onTaskDone(record);
                        }
                    };
                }
            };
        }
    }

    public void retrieve(final ItemRecord record, final ImageView imageView) {
        retrieve(record, new MyOnTask<Bitmap>() {
            @Override
            public void onTaskDone(Bitmap result) {
                imageView.setImageBitmap(result);
            }
        });
    }

    public void retrieve(ItemRecord itemRecord) {
        retrieve(itemRecord, new MyOnTask<Bitmap>() {
            @Override
            public void onTaskDone(Bitmap result) {
                //
            }
        });
    }

    public void retrieve(final ItemRecord record, final MyOnTask<Bitmap> onTask) {

        if (record.itemId <= 0) {
            onRetrieveImage(Empty.bitmap, onTask);
            return;
        }

        // record.mainImagePath가 다른 경우를 체크
        ImageRecord imageRecord = images.getRecordBy(record.itemId);
        boolean found = (imageRecord.id > 0);
        if (found) {
            if (TextUtils.isEmpty(record.mainImagePath) == false) {
                if (record.mainImagePath.equals(imageRecord.remotePath) == false) {
                    images.deleteBy(record.itemId);
                    found = false;
                }
            }
        }

        if (found) {
            Bitmap bitmap = imageRecord.getBitmap();
            if (onTask != null)
                onTask.onTaskDone(bitmap);
        } else {
            if (BaseAppWatcher.isNetworkConnected() == false) {
                MyUI.toastSHORT(context, String.format("오프라인 입니다"));
                return;
            }
            new MyServiceTask(context) {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    ImageDataProtocol imageDataProtocol = itemService.downloadMainImage(record);
                    if (imageDataProtocol.getByteArray().length <= 0)
                        imageDataProtocol = itemService.downloadMainThumbnail(record); // 작은 이미지라도
                    if (imageDataProtocol.getByteArray().length <= 0) {
                        Log.e("DEBUG_JS", String.format("[MyImageLoader.retrieve] %d, %s, image not found", record.itemId, record.name));
                        onRetrieveImage(Empty.bitmap, onTask);
                    } else {
                        Log.w("DEBUG_JS", String.format("[MyImageLoader.download] %d", record.itemId));
                        images.updateOrInsert(new ImageRecord(record.itemId, "", record.mainImagePath, imageDataProtocol.getByteArray()));
                        onRetrieveImage(imageDataProtocol.getBitmap(), onTask);
                    }
                }

                @Override
                public void catchException(MyServiceFailureException e) {
                    Log.e("DEBUG_JS", String.format("[MyImageLoader.catchException] %d, %s, remote %s, %s", record.itemId, record.name, record.mainImagePath, e.getLocalizedMessage()));
                    onRetrieveImage(Empty.bitmap, onTask);
                }
            };
        }
    }

    public void retrieveAsync(final ItemRecord record, final MyOnTask<Bitmap> onTask) {
        new MyAsyncTask(context, "retrieveThumbAsync4") {
            private Bitmap bitmap = Empty.bitmap;

            public void doInBackground() {
                retrieve(record, onTask);
            }
        };
    }

    public void retrieveThumb(final ItemRecord record, final ImageView imageView) {
        retrieveThumb(record, new MyOnTask<Bitmap>() {
            @Override
            public void onTaskDone(Bitmap result) {
                imageView.setImageBitmap(result);
            }
        });
    }

    public void retrieveThumbAsync(final String remotePath, final ImageView imageView) {
        new MyAsyncTask(context, "retrieveThumbAsync3") {
            @Override
            public void doInBackground() {
                retrieveThumb(remotePath, new MyOnTask<Bitmap>() {
                    @Override
                    public void onTaskDone(Bitmap result) {
                        imageView.setImageBitmap(result);
                    }
                });
            }
        };
    }

    public void retrieveThumb(final String remotePath, final ImageView imageView) {
        retrieveThumb(remotePath, new MyOnTask<Bitmap>() {
            @Override
            public void onTaskDone(Bitmap result) {
                imageView.setImageBitmap(result);
            }
        });
    }

    public void retrieveThumb(final String remotePath, final MyOnTask<Bitmap> onTask) {
        if (Empty.isEmpty(remotePath)) {
            //  Log.e("DEBUG_JS", String.format("[MyImageLoader.retrieveThumb] empty remote path"));
            onRetrieveImage(Empty.bitmap, onTask);
            return;
        }

        if (thumbImages.has(remotePath)) { // 썸네일DB에 있으면
            Bitmap bitmap = thumbImages.getRecordBy(remotePath).getBitmap();
            onRetrieveImage(bitmap, onTask);
            //    Log.i("DEBUG_JS", String.format("[MyImageLoader.doInBackground] after %d, %d", key, record.itemId));
        } else {
            if (BaseAppWatcher.isNetworkConnected() == false) {
                MyUI.toastSHORT(context, String.format("오프라인 입니다"));
                return;
            }

            new MyServiceTask(context) {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    ImageDataProtocol imageDataProtocol = itemService.downloadItemImage(remotePath);
                    onRetrieveImage(imageDataProtocol.getBitmap(), onTask);
                }

                @Override
                public void catchException(MyServiceFailureException e) {
                    onRetrieveImage(Empty.bitmap, onTask);
                    //  Log.e("DEBUG_JS", String.format("[MyImageLoader.catchException] %s not found", remotePath));
                }
            };
        }
    }

    public void retrieveThumb(final ItemRecord record, final MyOnTask<Bitmap> onTask) {
        retrieveThumb(record, thumbImages, onTask);
    }


    public void retrieveThumb(final ItemRecord record, final ImageDB imageDB, final MyOnTask<Bitmap> onTask) {
        if (record.itemId <= 0) {
            //Log.w("DEBUG_JS", String.format("[MyImageLoader.retrieveThumb] itemId %d <= 0", record.itemId));
            onRetrieveImage(Empty.bitmap, onTask);
            return;
        }

        // record.mainImagePath가 다른 경우를 체크
        ImageRecord imageRecord = imageDB.getRecordBy(record.itemId);
        boolean found = (imageRecord.id > 0);
        if (found) {
            if (TextUtils.isEmpty(record.mainImagePath) == false) {
                if (record.mainImagePath.equals(imageRecord.remotePath) == false) {
                    images.deleteBy(record.itemId);
                    found = false;
                }
            }
        }

        if (found) { // 썸네일DB에 있으면
            onRetrieveImage(imageRecord.getBitmap(), onTask);
            //    Log.i("DEBUG_JS", String.format("[MyImageLoader.doInBackground] after %d, %d", key, record.itemId));
        } else {
            if (BaseAppWatcher.isNetworkConnected() == false) {
                MyUI.toastSHORT(context, String.format("오프라인 입니다"));
                return;
            }

            new MyServiceTask(context) {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    ImageDataProtocol imageDataProtocol = itemService.downloadMainThumbnail(record);
                    imageDB.updateOrInsert(new ImageRecord(record.itemId, "", record.mainImagePath, imageDataProtocol.getByteArray()));
                    onRetrieveImage(imageDataProtocol.getBitmap(), onTask);
                }

                @Override
                public void catchException(MyServiceFailureException e) {
                    if (images.has(record.itemId)) {
                        onRetrieveImage(images.getRecordBy(record.itemId).getBitmap(2), onTask);
                    } else {
                        Log.e("DEBUG_JS", String.format("[MyImageLoader.catchException] %d, %s, remote %s, %s", record.itemId, record.name, record.mainImagePath, e.getLocalizedMessage()));
                        onRetrieveImage(Empty.bitmap, onTask);
                    }
                }
            };
        }
    }

    public void retrieveThumb(final String providerPhoneNumber, final int itemId, final MyOnTask<Bitmap> onTask) {
        if (itemId <= 0) {
            onRetrieveImage(Empty.bitmap, onTask);
            return;
        }

        // mainImagePath가 다른 경우를 체크할 수 없음
        if (thumbImages.has(itemId)) { // 썸네일DB에 있으면
            Bitmap bitmap = thumbImages.getRecordBy(itemId).getBitmap();
            onRetrieveImage(bitmap, onTask);
        } else {
            new CommonServiceTask(context, "MyImageLoader") {
                @Override
                public void doInBackground() throws MyServiceFailureException {
                    MerchandiseData data = itemService.getMerchandiseData(userSettingData.getUserShopInfo(), providerPhoneNumber, itemId);
                    ImageRecord imageRecord = new ImageRecord(data, data.downloadThumbnail());
                    thumbImages.updateOrInsert(imageRecord);
                    onRetrieveImage(imageRecord.getBitmap(), onTask);
                }

                @Override
                public void catchException(MyServiceFailureException e) {
                    if (images.has(itemId)) {
                        onRetrieveImage(images.getRecordBy(itemId).getBitmap(2), onTask);
                    } else {
                        onRetrieveImage(Empty.bitmap, onTask);
                    }
                    //   Log.e("DEBUG_JS", String.format("[MyImageLoader.catchException] %d, %s, %s", itemId, providerPhoneNumber, e.getLocalizedMessage()));
                }
            };
        }
    }

    public void retrieveThumbAsync(final ItemRecord record, final ImageDB imageDB, final MyOnTask<Bitmap> onTask) {
        final ItemRecord myItemRecord = record.clone(); // 다른 record가 연달아 호출될 가능성이 있어 바로 복사 후 복사본을 전달
        new MyAsyncTask(context, "retrieveThumbAsync1") {
            public void doInBackground() {
                retrieveThumb(myItemRecord, imageDB, onTask);
            }
        };
    }

    public void retrieveThumbAsync(final ItemRecord record, final MyOnTask<Bitmap> onTask) {
        final ItemRecord myItemRecord = record.clone(); // 다른 record가 연달아 호출될 가능성이 있어 바로 복사 후 복사본을 전달
        new MyAsyncTask(context, "retrieveThumbAsync1") {
            public void doInBackground() {
                retrieveThumb(myItemRecord, onTask);
            }
        };
    }

    public void retrieveThumbAsync(final String providerPhoneNumber, final int itemId, final MyOnTask<Bitmap> onTask) {
        new MyAsyncTask(context, "retrieveThumbAsync2") {
            public void doInBackground() {
                retrieveThumb(providerPhoneNumber, itemId, onTask);
            }
        };
    }

    protected void onRetrieveImage(final Bitmap bitmap, final MyOnTask<Bitmap> onTask) {
        if (onTask == null) return;
        new MyUITask(context) {
            @Override
            public void doInBackground() {
                onTask.onTaskDone(bitmap);
            }
        };
    }

    abstract class MyServiceTask extends BaseServiceTask {

        public MyServiceTask(Context context) {
            super(context, "MyImageLoader");
        }

        @Override
        public void setService() {
        }

    }

}
