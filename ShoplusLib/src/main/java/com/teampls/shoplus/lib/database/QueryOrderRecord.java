package com.teampls.shoplus.lib.database;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medivh on 2017-10-09.
 */

public class QueryOrderRecord {
    public Enum<?> column;
    public boolean isInt = false;
    public boolean isDes = false;

    public QueryOrderRecord(){};

    public QueryOrderRecord(Enum column, boolean isInt, boolean isDes) {
        this.column = column;
        this.isInt = isInt;
        this.isDes = isDes;
    }

    public List<QueryOrderRecord> toList() {
        List<QueryOrderRecord> results = new ArrayList<>();
        results.add(this);
        return results;
    }

    public void toLogCat(String callLocation) {
        Log.i("DEBUG_JS", String.format("[%s] %s, int? %s, des? %s", callLocation, column.toString(), isInt, isDes));
    }

}
