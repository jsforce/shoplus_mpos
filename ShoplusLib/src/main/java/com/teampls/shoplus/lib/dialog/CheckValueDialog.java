package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.teampls.shoplus.R;

/**
 * Created by Medivh on 2017-04-07.
 */

abstract public class CheckValueDialog extends BaseDialog {
    private CheckBox checkBox;

    public CheckValueDialog(Context context, String title, String message, boolean value, double widthRatio) {
        super(context);
        findTextViewById(R.id.dialog_check_value_title, title);
        setDialogWidth(widthRatio, 0.6);
        checkBox = (CheckBox) findViewById(R.id.dialog_check_value_checkBox);
        checkBox.setChecked(value);
        if (message.isEmpty()) {
            setVisibility(View.GONE, R.id.dialog_check_value_message);
        } else {
            ((TextView) findViewById(R.id.dialog_check_value_message)).setText(message);
        }
        setOnClick(R.id.dialog_check_value_cancel, R.id.dialog_check_value_apply);
        show();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_check_value;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_check_value_cancel) {
            dismiss();
        } else if (view.getId() == R.id.dialog_check_value_apply) {
            onApplyClick(checkBox.isChecked());
                dismiss();
        } else {

        }
    }

    abstract public void onApplyClick(boolean newValue);
}
