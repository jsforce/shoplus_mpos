package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.composition.UserComposition;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_map.KeyValueBoolean;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.ContactBuyerType;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_module.MyCheckBox;

import org.joda.time.DateTime;

/**
 * Created by Medivh on 2017-04-07.
 */

abstract public class UpdateUserDialog extends BaseDialog {
    private UserRecord record = new UserRecord();
    private EditText etName, etPhoneNumber, etAddress;
    private DbActionType actionType = DbActionType.INSERT;
    private MyCheckBox cbAddLocal;
    private KeyValueBoolean doAddLocal = new KeyValueBoolean("userInputDialog.addLocal", false);
    private CheckBox cbIsRetailPrice;

    public UpdateUserDialog(final Context context, UserRecord record, DbActionType actionType) {
        super(context);
        this.record = record;
        this.actionType = actionType;

        setDialogWidth(0.9, 0.7);
        etName = findEditTextById(R.id.dialog_user_input_name, record.name);
        etPhoneNumber = findEditTextById(R.id.dialog_user_input_phoeNumber, BaseUtils.toPhoneFormat(record.phoneNumber));
        etAddress = findEditTextById(R.id.dialog_user_input_address, record.location);
        cbAddLocal = new MyCheckBox(context, getView(), R.id.dialog_user_input_addLocal, doAddLocal);

        switch (actionType) {
            case INSERT:
                findTextViewById(R.id.dialog_user_input_title, "신규 등록");
                break;
            case UPDATE:
                findTextViewById(R.id.dialog_user_input_title, "정보 수정");
                break;
        }

        if (record.phoneNumber.isEmpty() == false) {
            // UPDATE 가 정상이지만 INSERT에서도 들어올 수 있다
            etPhoneNumber.setFocusable(false);
            etPhoneNumber.setTextColor(ColorType.Gray.colorInt);
            setVisibility(View.INVISIBLE, R.id.dialog_user_input_phoneNumber_clear, R.id.dialog_user_input_temp_phoneNumber);
            LinearLayout phoneContainer = (LinearLayout) findViewById(R.id.dialog_user_input_phoenNumber_container);
            phoneContainer.setClickable(true);
            phoneContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String touchMessage = String.format("전화번호 변경은 새 번호로 등록 후 상단 메뉴에 있는 [거래처통합] 기능을 이용해 주세요.");
                    new MyGuideDialog(context, "번호 변경", touchMessage).show();
                }
            });
        }

        cbIsRetailPrice = findViewById(R.id.dialog_user_input_isRetailPrice);
        cbIsRetailPrice.setChecked(record.getBuyerType() == ContactBuyerType.RETAIL);
        cbIsRetailPrice.setVisibility(userSettingData.getProtocol().isRetailOn() ? View.VISIBLE : View.GONE);

        setOnClick(R.id.dialog_user_input_cancel, R.id.dialog_user_input_apply,
                R.id.dialog_user_input_phoneNumber_clear, R.id.dialog_user_input_name_clear,
                R.id.dialog_user_input_temp_phoneNumber, R.id.dialog_user_input_address_clear);
        etName.requestFocus();

        MyView.setTextViewByDeviceSize(context, getView(), R.id.dialog_user_input_phoeNumber_guide);

        MyDevice.showKeyboard(context, etName);
        show();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_user_input;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_user_input_cancel) {
            MyDevice.hideKeyboard(context, etName);
            dismiss();

        } else if (view.getId() == R.id.dialog_user_input_apply) {
            if (Empty.isEmpty(context, etName, "이름을 입력해주세요")) return;
            if (Empty.isEmpty(context, etPhoneNumber, "핸드폰 번호를 입력해주세요")) return;

            String name = etName.getText().toString().trim();
            String phoneNumber = etPhoneNumber.getText().toString().trim();
            UserDB userDB = UserDB.getInstance(context);

            switch (actionType) {
                case INSERT:
                    if (userDB.hasValue(UserDB.Column.name, name)) {
                        MyUI.toastSHORT(context, String.format("같은 이름이 있습니다"));
                        return;
                    }

                    if (userDB.has(phoneNumber)) {
                        UserRecord userRecord = userDB.getRecord(phoneNumber);
                        MyUI.toastSHORT(context, String.format("같은 전화번호가 있습니다. %s", userRecord.getName()));
                        return;
                    }
                    break;
                case UPDATE:
                    // 이름을 수정했는데 기존에 있던 이름인 경우 문제가 된다.업데이트는 뭘 수정했는지 체크를 해야 중복 검사도 할지 말지 판단함
                    break;
            }

            cbIsRetailPrice.setVisibility(userSettingData.getProtocol().isRetailOn() ? View.VISIBLE : View.GONE);

            record.name = name;
            record.phoneNumber = BaseUtils.toSimpleForm(phoneNumber);
            if (record.phoneNumber.length() >= 12) {  // not real phone number
                if (record.name.startsWith("*") == false)
                    record.name = "*" + record.name;
            }
            record.location = etAddress.getText().toString();
            record.priceType = cbIsRetailPrice.isChecked() ? ContactBuyerType.RETAIL : ContactBuyerType.WHOLESALE;

            UserComposition userComposition = new UserComposition(context);
            userComposition.uploadUser("UpdateUserDialog", record, new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    if (cbAddLocal.isChecked())
                        MyDevice.addContactDirectly(context, record.name, record.phoneNumber);
                    onUserInputFinish(record);
                }
            });
            MyDevice.hideKeyboard(context, etName);
            dismiss();

        } else if (view.getId() == R.id.dialog_user_input_phoneNumber_clear) {
            etPhoneNumber.setText("");

        } else if (view.getId() == R.id.dialog_user_input_name_clear) {
            etName.setText("");

        } else if (view.getId() == R.id.dialog_user_input_address_clear) {
            etAddress.setText("");

        } else if (view.getId() == R.id.dialog_user_input_temp_phoneNumber) {
            String tempNumber = String.format("%s", DateTime.now().toString(BaseUtils.TempUser_Format)); // 12자리
            etPhoneNumber.setText(tempNumber);
            MyUI.toast(context, String.format("[비권장] 임시 번호로 등록시 고객과 공유되지 않습니다. 가급적 실제 전화번호를 이용해주세요"));
        }
    }

    //  abstract public void onDeleteClick(UserRecord userRecord, ContactRecord contactRecord);

    abstract public void onUserInputFinish(UserRecord userRecord);

}
