package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.database.QueryOrderRecord;
import com.teampls.shoplus.lib.database.SlipDB;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.AddressbookDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MemorySlipDB;
import com.teampls.shoplus.lib.enums.AccountLogType;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.view.MyView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2017-04-20.
 */

abstract public class BaseSlipDBAdapter extends BaseDBAdapter<SlipRecord> {
    protected SlipDB slipDB;
    protected MemorySlipDB memorySlipDB;
    protected boolean doFilterPhoneNumbersMain = false, doFilterPaymentType = false;
    protected boolean doFilterConfirmation = false, doBlockCancelled = false, doSplitConfirmed = false;
    protected boolean doFilterDateTimeMain = false ;
    protected List<String> phoneNumbers = new ArrayList<>();
    protected Set<AccountLogType> accountLogTypes = new HashSet<>();
    protected boolean confirmationValue = false;
    protected DateTime fromDateTime = Empty.dateTime, toDateTime = Empty.dateTime;

    public BaseSlipDBAdapter(Context context) {
        this(context, SlipDB.getInstance(context));
        mode = Mode.FileDB;
    }

    public BaseSlipDBAdapter(Context context, SlipDB slipDB) {
        super(context);
        this.slipDB = slipDB;
        mode = Mode.FileDB;
    }

    public BaseSlipDBAdapter(Context context, MemorySlipDB memorySlipDB) {
        super(context);
        this.memorySlipDB = memorySlipDB;
        mode = Mode.MemoryDB;
    }

    public BaseSlipDBAdapter setSlipDB(SlipDB slipDB) {
        this.slipDB = slipDB;
        return this;
    }

    public BaseSlipDBAdapter filterPeriod(DateTime fromDateTime, DateTime toDateTime) {
        doFilterDateTimeMain = true;
        this.fromDateTime = fromDateTime;
        this.toDateTime = toDateTime;
        return this;
    }

    public BaseSlipDBAdapter resetFilters() {
        doFilterPhoneNumbersMain = false;
        doFilterPaymentType = false;
        doFilterConfirmation = false;
        doBlockCancelled = false;
        doSplitConfirmed = false;
        doFilterDateTimeMain = false;
        return this;
    }

    public BaseSlipDBAdapter blockCancelled(boolean value) {
        doBlockCancelled = value;
        return this;
    }

    public BaseSlipDBAdapter doSplitConfirmed(boolean value) {
        doSplitConfirmed = value;
        return this;
    }

    public BaseSlipDBAdapter filterConfirmation(boolean value) {
        doFilterConfirmation = true;
        confirmationValue = value;
        return this;
    }

    public BaseSlipDBAdapter setPaymentType(AccountLogType accountLogType, boolean checked) {
        doFilterPaymentType = true;
        if (checked) {
            accountLogTypes.add(accountLogType);
        } else {
            accountLogTypes.remove(accountLogType);
        }
        return this;
    }

    public BaseSlipDBAdapter filterPhoneNumbers(String... counterPhoneNumbers) {
        doFilterPhoneNumbersMain = true;
        phoneNumbers.clear();
        for (String phoneNumber : counterPhoneNumbers)
            phoneNumbers.add(phoneNumber);
        return this;
    }

    public BaseSlipDBAdapter resetFilterPhoneNumbers() {
        doFilterPhoneNumbersMain = false;
        return this;
    }

    public boolean isDoFilterPhoneNumbers() {
        return doFilterPhoneNumbersMain;
    }

    public void toLogCat(String callLocation) {
        for (SlipRecord record : getRecords())
            record.toLogCat(callLocation);
    }

    @Override
    public void generate() {
        if (doSkipGenerate)
            return;

        generatedRecords.clear();
        List<SlipRecord> _confirmedRecords = new ArrayList<>();
        List<SlipRecord> slipRecords = new ArrayList<>();

        switch (mode) {

            // FileDB
            default:
                if (doFilterDateTimeMain) {
                    orderRecords = new QueryOrderRecord(SlipDB.Column.createdDateTime, false, true).toList();
                    slipRecords = slipDB.getRecordsBetween(SlipDB.Column.createdDateTime, fromDateTime.toString(), toDateTime.toString(), orderRecords);
                } else if (doFilterPhoneNumbersMain) {
                    orderRecords = new QueryOrderRecord(SlipDB.Column.createdDateTime, false, true).toList();
                    if (userSettingData.isProvider()) {
                        slipRecords = slipDB.getRecordsIN(SlipDB.Column.counterpartPhoneNumber, phoneNumbers, orderRecords);
                    } else { // buyer
                        slipRecords = slipDB.getRecordsIN(SlipDB.Column.ownerPhoneNumber, phoneNumbers, orderRecords);
                    }
                } else {
                    // 주의할 것, exodar 같은데서 이리로 오게 되면 연간 자료 노출이라는 사고가 발생하게 된다
                    orderRecords = new QueryOrderRecord(SlipDB.Column.createdDateTime, false, true).toList(); // created, sales 보통은 같은데 날짜 수동변경이 가능했던 시절만 차이가 발생
                    slipRecords = slipDB.getRecordsOrderBy(orderRecords);
                }
                break;

            // MemoryDB
            case MemoryDB:
                for (SlipRecord record : memorySlipDB.getRecords()) {
                    if (doFilterDateTimeMain)
                        if (BaseUtils.isBetween(record.createdDateTime, fromDateTime, toDateTime) == false)
                            continue;
                    slipRecords.add(record);
                }
                break;
        }

        for (SlipRecord record : slipRecords) {
            if (doFilterPaymentType) {
                boolean passed = false;
                for (AccountLogType paymetType : record.getPaymentTypes()) {
                    if (accountLogTypes.contains(paymetType)) {
                        passed = true;
                        break;
                    }
                }
                if (passed == false)
                    continue;
            }

            if (doFilterConfirmation)
                if (record.confirmed != confirmationValue)
                    continue;

            if (doBlockCancelled)
                if (record.cancelled)
                    continue;

            if (doSplitConfirmed) {
                if (record.confirmed) {
                    _confirmedRecords.add(record);
                    continue;
                }
            }

            record.sortingKey = sortingKey; // Date only
            generatedRecords.add(record);
        }

        if (doSortByKey) {
            Collections.sort(this.generatedRecords);
            Collections.sort(_confirmedRecords);
        }

        if (doSplitConfirmed)
            generatedRecords.addAll(_confirmedRecords);

        // 최대한 뒤에 있어야 The content of the adapter has changed but ListView did not receive a notification 에러를 피할 수 있다
        if (doSetRecordWhenGenerated)
            applyGeneratedRecords();
    }

    private SlipSummaryRecord getSummaryRecord(SlipRecord record) {
        switch (mode) {
            default:
                return new SlipSummaryRecord(slipDB.items.getRecordsBy(record.getSlipKey().toSID()));
            case MemoryDB:
                return new SlipSummaryRecord(memorySlipDB.items.getRecordsBy(record.getSlipKey().toSID()));
        }
    }

    public class ViewHolder extends BaseViewHolder {
        protected TextView tvDate, tvCounterpart, tvSummary, tvPayment, tvSlipItemTypes;
        protected SlipRecord record;
        protected LinearLayout container, typeContainer;

        @Override
        public int getThisView() {
            return R.layout.row_slip;
        }

        @Override
        public void init(View view) {
            tvDate = findTextViewById(context, view, R.id.row_slip_date);
            tvCounterpart = findTextViewById(context, view, R.id.row_slip_counterpart);
            tvSummary = findTextViewById(context, view, R.id.row_slip_summary);
            tvSlipItemTypes = findTextViewById(context, view, R.id.row_slip_option);
            tvPayment = findTextViewById(context, view, R.id.row_slip_payment);
            container =  view.findViewById(R.id.row_slip_container);
            typeContainer = view.findViewById(R.id.row_slip_typeContainer);
        }

        public void update(int position) {
            if (position >= records.size() || position < 0) return;
            record = records.get(position);

            SlipSummaryRecord summaryRecord = getSummaryRecord(record);

            // Date
            tvDate.setVisibility(View.VISIBLE);
            tvDate.setTextColor(ColorType.Black.colorInt);
            if (BasePreference.isShowWeekday(context)) {
                tvDate.setText(record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format) + String.format("\n(%s)", BaseUtils.getDayOfWeekKor(record.salesDateTime)));
            } else {
                tvDate.setText(record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format));
            }
            if (position > 0) {
                SlipRecord prevRecord = records.get(position - 1);
                if (record.createdDateTime.getDayOfYear() == (prevRecord.createdDateTime.getDayOfYear())) {
                    if (BasePreference.doShowHm.getValue(context)) {
                        tvDate.setText(record.salesDateTime.toString(BaseUtils.Hm_Format));
                        tvDate.setTextColor(ColorType.DarkGray.colorInt);
                    } else {
                        tvDate.setVisibility(View.INVISIBLE);
                    }
                }
            }

            // Counterpart
            String counterpartPhoneNumber = record.getCounterpart(context);
            UserRecord counterpart = userDB.getRecord(counterpartPhoneNumber); // 비어 있을 수 있음
            BaseUtils.setText(tvCounterpart, "", counterpart.getNameWithRetail(), record.cancelled);
            BaseUtils.setText(tvSummary, summaryRecord.count == 0, "",
                    String.format("%d건 %s", summaryRecord.count, BaseUtils.toCurrencyStr(summaryRecord.amount)), record.cancelled);

            // Payments
            List<String> paymentStrings = record.getPaymentStrings(false);
            if (paymentStrings.size() == 0) {
                tvPayment.setVisibility(View.INVISIBLE);
            } else if (paymentStrings.size() == 1 && record.getPaymentTypes().get(0) == AccountLogType.CashPaid) { // 즉납완료는 표시할 필요 없음
                tvPayment.setVisibility(View.INVISIBLE);
            } else {
                tvPayment.setVisibility(View.VISIBLE);
                tvPayment.setText(TextUtils.join(", ", paymentStrings));
                tvPayment.setTextColor(record.cancelled ? ColorType.DarkGray.colorInt : ColorType.LightSteelBlue.colorInt);
            }

            // SlipItemTypes - color 때문에 동적으로 추가함
            // 고객의 요청에 의해 "미송배송", "매입", "부가세" 추가
            List<SlipItemType> pendingTypes = SlipItemType.getPendings(summaryRecord.slipItemTypes, SlipItemType.DEPOSIT, SlipItemType.ADVANCE_CLEAR, SlipItemType.VALUE_ADDED_TAX);
            if (pendingTypes.size() >= 1) {
                typeContainer.setVisibility(View.VISIBLE);
                typeContainer.removeAllViews();
                int count = 0;
                for (SlipItemType slipItemType : pendingTypes) {
                    count++;
                    String text = slipItemType.uiName + (count == pendingTypes.size() ? "" : ", ");
                    int colorInt = record.cancelled ? ColorType.DarkGray.colorInt : slipItemType.getColorInt();
                    if (slipItemType == SlipItemType.ADVANCE_CLEAR || slipItemType == SlipItemType.VALUE_ADDED_TAX)
                        colorInt = ColorType.DarkGray.colorInt;
                    TextView textView = MyView.createTextView(context, text, 14, 0, colorInt);
                    textView.setSingleLine(true);
                    MyView.setTextViewByDeviceSize(context, textView);
                    typeContainer.addView(textView);
                }
            } else {
                typeContainer.setVisibility(View.GONE);
            }

            setBackgroundColorByShiftTimes();
        }

        protected void setBackgroundColorByShiftTimes() {
            // Background color
            Pair<Double, Double> shiftTimes = BasePreference.getShiftTimes(context);
            if (shiftTimes.second >= 0) { // 2개가 지정된 경우
                if (BaseUtils.isBetweenByHm(record.salesDateTime, shiftTimes.first, shiftTimes.second)) {
                    container.setBackgroundColor(ColorType.Ivory.colorInt);
                } else {
                    container.setBackgroundColor(ColorType.Beige.colorInt);
                }
            } else if (shiftTimes.first >= 0) { // 1개가 지정된 경우
                if (record.salesDateTime.getDayOfYear() % 2 != 0) {
                    if (record.salesDateTime.getHourOfDay() < shiftTimes.first) {
                        container.setBackgroundColor(ColorType.Ivory.colorInt);
                    } else {
                        container.setBackgroundColor(ColorType.Beige.colorInt);
                    }
                } else {
                    if (record.salesDateTime.getHourOfDay() < shiftTimes.first) {
                        container.setBackgroundColor(ColorType.Beige.colorInt);
                    } else {
                        container.setBackgroundColor(ColorType.Ivory.colorInt);
                    }
                }
            } else {
                container.setBackgroundColor(ColorType.Ivory.colorInt);
            }
        }
    }

    public int getPosition(SlipRecord record) {
        int position = 0;
        for (SlipRecord _record : records) {
            if (_record.getSlipKey().toSID().equals(record.getSlipKey().toSID()))
                return position;
            position++;
        }
        return position;
    }

    public class SlipListViewHolder extends BaseViewHolder {
        protected TextView tvDate, tvCounterpart, tvAmount, tvStatus;
        protected SlipRecord record;
        protected LinearLayout container;

        @Override
        public int getThisView() {
            return R.layout.row_slip_state;
        }

        @Override
        public void init(View view) {
            container = (LinearLayout) view.findViewById(R.id.row_slip_state_container);
            tvDate = (TextView) view.findViewById(R.id.row_slip_state_date);
            tvCounterpart = (TextView) view.findViewById(R.id.row_slip_state_counterpart);
            tvAmount = (TextView) view.findViewById(R.id.row_slip_state_amount);
            tvStatus = (TextView) view.findViewById(R.id.row_slip_state_status);
            MyView.setTextViewByDeviceSize(context, tvDate, tvCounterpart, tvAmount, tvStatus);
        }

        @Override
        public void update(int position) {
            record = getRecord(position);
            UserRecord counterpart = UserDB.getInstance(context).getRecordWithNumber(record.counterpartPhoneNumber);
            SlipSummaryRecord summary = getSummaryRecord(record);

            if (BasePreference.isShowWeekday(context)) {
                tvDate.setText(record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format) + " " + BaseUtils.getDayOfWeekKor(record.salesDateTime));
            } else {
                tvDate.setText(record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format));
            }

            tvCounterpart.setText(counterpart.getNameWithRetail());
            tvAmount.setText(BaseUtils.toCurrencyOnlyStr(summary.amount));
            tvStatus.setText(record.confirmed ? "발행완료" : "작성중");
            tvStatus.setTextColor(record.confirmed ? ColorType.Black.colorInt : ColorType.Blue.colorInt);

            // Background color
            setBackgroundColorByShiftTimes();
        }

        protected void setBackgroundColorByShiftTimes() {
            // Background color
            Pair<Double, Double> shiftTimes = BasePreference.getShiftTimes(context);
            if (shiftTimes.second >= 0) { // 2개가 지정된 경우
                if (BaseUtils.isBetweenByHm(record.salesDateTime, shiftTimes.first, shiftTimes.second)) {
                    container.setBackgroundColor(ColorType.Ivory.colorInt);
                } else {
                    container.setBackgroundColor(ColorType.Beige.colorInt);
                }
            } else if (shiftTimes.first >= 0) { // 1개가 지정된 경우
                if (record.salesDateTime.getDayOfYear() % 2 != 0) {
                    if (record.salesDateTime.getHourOfDay() < shiftTimes.first) {
                        container.setBackgroundColor(ColorType.Ivory.colorInt);
                    } else {
                        container.setBackgroundColor(ColorType.Beige.colorInt);
                    }
                } else {
                    if (record.salesDateTime.getHourOfDay() < shiftTimes.first) {
                        container.setBackgroundColor(ColorType.Beige.colorInt);
                    } else {
                        container.setBackgroundColor(ColorType.Ivory.colorInt);
                    }
                }
            } else {
                container.setBackgroundColor(ColorType.Ivory.colorInt);
            }
        }
    }

}
