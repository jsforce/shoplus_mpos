package com.teampls.shoplus.lib.datatypes;

import org.joda.time.DateTime;

/**
 * 재고변동 개별로그 인터페이스
 *
 * @author lucidite
 */
public interface ItemStockChangeLogProtocol {
    String getLogId();
    DateTime getUpdatedDateTime();
    int getValue();
}
