package com.teampls.shoplus.lib.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BasePreference;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.database.DraftSlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MDraftSlipDB;
import com.teampls.shoplus.lib.enums.ColorType;
import com.teampls.shoplus.lib.view.MyView;

import java.util.Collections;

/**
 * Created by Medivh on 2017-08-22.
 */

public class DraftSlipDBAdapter extends BaseDBAdapter<DraftSlipRecord> {
    protected MDraftSlipDB draftSlipDB;

    public DraftSlipDBAdapter(Context context) {
        super(context);
        draftSlipDB = MDraftSlipDB.getInstance(context);
    }

    @Override
    public void generate() {
        generatedRecords = draftSlipDB.getRecords();
        Collections.sort(generatedRecords);
        if (doSetRecordWhenGenerated)
            applyGeneratedRecords();
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder();
    }

    class ViewHolder extends BaseViewHolder {
        protected TextView tvDate, tvCounterpart, tvAmount, tvStatus;

        @Override
        public int getThisView() {
            return R.layout.row_slip_state;
        }

        @Override
        public void init(View view) {
            tvDate = (TextView) view.findViewById(R.id.row_slip_state_date);
            tvCounterpart = (TextView) view.findViewById(R.id.row_slip_state_counterpart);
            tvAmount = (TextView) view.findViewById(R.id.row_slip_state_amount);
            tvStatus = (TextView) view.findViewById(R.id.row_slip_state_status);
            MyView.setTextViewByDeviceSize(context, tvDate, tvCounterpart, tvAmount, tvStatus);
        }

        @Override
        public void update(int position) {
            DraftSlipRecord record = getRecord(position);
            UserRecord counterpart = UserDB.getInstance(context).getRecord(record.recipientPhoneNumber);
            SlipSummaryRecord summary = draftSlipDB.items.getSummary(record.getSlipDataKey(context).toSID());
            if (BasePreference.isShowWeekday(context)) {
                tvDate.setText(record.createdDateTime.toString(BaseUtils.MD_FixedSize_Format)+String.format(" (%s)", BaseUtils.getDayOfWeekKor(record.createdDateTime)));
            } else {
                tvDate.setText(record.createdDateTime.toString(BaseUtils.MD_FixedSize_Format));
            }
            tvDate.setText(record.createdDateTime.toString(BaseUtils.MD_FixedSize_Format));

            tvCounterpart.setText(counterpart.name);
            tvAmount.setText(BaseUtils.toCurrencyOnlyStr(summary.amount));
            if (record.updatedDateTime.isAfter(record.createdDateTime)) {
                tvStatus.setText("수정됨");
                tvStatus.setTextColor(ColorType.Red.colorInt);
            } else {
                tvStatus.setText("공유중");
                tvStatus.setTextColor(ColorType.Black.colorInt);
            }
        }
    }
}
