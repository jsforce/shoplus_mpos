package com.teampls.shoplus.lib.awsservice.implementations;

import com.amazonaws.AmazonClientException;
import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.awsservice.ProjectMasterUserServiceProtocol;
import com.teampls.shoplus.lib.awsservice.apigateway.APIGatewayClient;
import com.teampls.shoplus.lib.awsservice.apigateway.datamodel.ContactJSONData;
import com.teampls.shoplus.lib.awsservice.cognito.UserHelper;
import com.teampls.shoplus.lib.datatypes.ShopOrganizationData;
import com.teampls.shoplus.lib.enums.UserLevelType;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 사장 관련 서비스 API
 *
 * @author lucidite
 */

public class ProjectMasterUserService implements ProjectMasterUserServiceProtocol {

    private String getMyShopOrganizationResourcePath(String phoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "shops");
    }

    private String getAMemberOfMyShopResourcePath(String phoneNumber, String shopPhoneNumber, String memberPhoneNumber) {
        return MyAWSConfigs.getResourcePath(phoneNumber, "shops", shopPhoneNumber, "members", memberPhoneNumber);
    }

    @Override
    public ShopOrganizationData getMyShopOrganization(String userPhoneNumber) throws MyServiceFailureException {
        try {
            String resource = this.getMyShopOrganizationResourcePath(userPhoneNumber);
            String response = APIGatewayClient.requestGET(resource, null, UserHelper.getIdentityToken());

            return new ShopOrganizationData(new JSONObject(response));
        } catch (JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void updateShopMember(String userPhoneNumber, String shopPhoneNumber, String memberPhoneNumber, UserLevelType userLevel, String memberName) throws MyServiceFailureException {
        try {
            String resource = this.getAMemberOfMyShopResourcePath(userPhoneNumber, shopPhoneNumber, memberPhoneNumber);
            JSONObject requestBodyObj = new JSONObject().put(
                    "data", new JSONObject().put("level", userLevel.toRemoteStr()).put("name", memberName)
            );
            APIGatewayClient.requestPUT(resource, requestBodyObj.toString(), UserHelper.getIdentityToken());
        } catch (JSONException e) {
            // e.printStackTrace();        // DEBUG
            throw new MyServiceFailureException(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void deleteShopMember(String userPhoneNumber, String shopPhoneNumber, String memberPhoneNumber) throws MyServiceFailureException {
        String resource = this.getAMemberOfMyShopResourcePath(userPhoneNumber, shopPhoneNumber, memberPhoneNumber);
        APIGatewayClient.requestDELETE(resource, null, UserHelper.getIdentityToken());
    }
}
