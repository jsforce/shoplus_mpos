package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.view.MyView;

/**
 * Created by Medivh on 2017-02-14.
 */

abstract public class MyEditTextDialog extends BaseDialog {
    private TextView tvComment;

    public MyEditTextDialog(final Context context, String title, String message) {
        super(context);
        setDialogWidth(0.9, 0.6);
        findTextViewById(R.id.dialog_confirm_title, title);
        findTextViewById(R.id.dialog_confirm_message, message);
        tvComment = findTextViewById(R.id.dialog_confirm_comment, "");
        setOnClick(R.id.dialog_confirm_cancel);
        setOnClick(R.id.dialog_confirm_apply);
        MyView.setTextViewByDeviceSizeScale(context, getView(), 0.5,
                R.id.dialog_confirm_title, R.id.dialog_confirm_message, R.id.dialog_confirm_comment);
        show();
        new MyDelayTask(context, 100) {
            @Override
            public void onFinish() {
                MyDevice.showKeyboard(context, tvComment);
            }
        };
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_text_edit;
    }

    @Override
    public void onClick(View view) {
        MyDevice.hideKeyboard(context, tvComment);
        if (view.getId() == R.id.dialog_confirm_apply) {
            yes(tvComment.getText().toString().trim());
        } else if (view.getId() == R.id.dialog_confirm_cancel) {
            no();
        }
    }

    abstract public void yes(String comment);

    public void no() {
        dismiss();
    }

}
