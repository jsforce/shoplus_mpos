package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.MyDelayTask;

/**
 * Created by Medivh on 2016-12-02.
 */

public class TransDialog extends BaseDialog {
    private TextView tvTitle, tvMessage;

    @Override
    public int getThisView() {
        return R.layout.dialog_trans;
    }

    public TransDialog(Context context, String title, String message, double durationTime) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        if (context == null) {
            dismiss();
            return;
        }
        Window window = this.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,  WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED);
        findTextViewById(R.id.dialog_trans_title, title);
        findTextViewById(R.id.dialog_trans_message, message);
        setCanceledOnTouchOutside(true);
        new MyDelayTask(context, (int) (durationTime*1000)) {
            @Override
            public void onFinish() {
                dismiss();
            }
        };
    }



    @Override
    public void onClick(View view) {

    }
}
