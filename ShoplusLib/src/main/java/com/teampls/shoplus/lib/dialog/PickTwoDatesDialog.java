package com.teampls.shoplus.lib.dialog;

import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import com.squareup.timessquare.CalendarPickerView;
import com.teampls.shoplus.R;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyUI;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Medivh on 2017-04-07.
 */

abstract public class PickTwoDatesDialog extends BaseDialog implements CalendarPickerView.OnDateSelectedListener, CalendarPickerView.OnInvalidDateSelectedListener {
    private CalendarPickerView calendarView;
    protected DateTime selectedFromDate = BaseUtils.createDay(), selectedToDate = BaseUtils.createDay();
    private int absoluteLimitDays = 365, relativeLimitDays = 30;

    public PickTwoDatesDialog(Context context, String title, int relativeLimitDays) {
        super(context);
        findTextViewById(R.id.dialog_pick_period_title, title);
        this.relativeLimitDays = relativeLimitDays;
        DateTime selectedDay = BaseUtils.createDay();
        DateTime nowDateTime = BaseUtils.createDay();
        DateTime fromDateTime = nowDateTime.minusDays(absoluteLimitDays);
        DateTime toDateTime = nowDateTime.plusYears(1);
        if (BaseUtils.isBetween(selectedDay, fromDateTime, toDateTime) == false)
            selectedDay = BaseUtils.createDay();
        calendarView = findViewById(R.id.dialog_pick_period_calendar);
        calendarView.init(fromDateTime.toDate(), toDateTime.toDate())
                .withSelectedDate(selectedDay.toDate())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
        calendarView.setOnDateSelectedListener(this);
        calendarView.setOnInvalidDateSelectedListener(this);
        setOnClick(R.id.dialog_pick_period_cancel, R.id.dialog_pick_period_apply, R.id.dialog_pick_period_fromTime_plus,
                R.id.dialog_pick_period_fromTime_minus, R.id.dialog_pick_period_toTime_plus, R.id.dialog_pick_period_toTime_minus);
        setVisibility(View.GONE, R.id.dialog_pick_period_time_container);
        setFromAndToDate();
        show();
    }

    @Override
    public void onDateSelected(Date date) {
        setFromAndToDate();
    }

    @Override
    public void onDateUnselected(Date date) {
        setFromAndToDate();
    }

    @Override
    public int getThisView() {
        return R.layout.dialog_pick_period;
    }

    private void setFromAndToDate() {
        List<Date> selectedDates = calendarView.getSelectedDates(); // 중간 날짜도 모두 선택되는 방식
        if (selectedDates.size() == 0) {
            // now
        } else if (selectedDates.size() == 1) {
            selectedFromDate = new DateTime(selectedDates.get(0));
            selectedToDate = selectedFromDate;
        } else if (selectedDates.size() >= 2) {
            selectedFromDate = new DateTime(selectedDates.get(0));
            selectedToDate = new DateTime(selectedDates.get(selectedDates.size()-1));
        }

        Duration duration = new Duration(selectedFromDate, selectedToDate);
        if (duration.getStandardDays() > relativeLimitDays) {
            selectedToDate = selectedFromDate;
            MyUI.toastSHORT(context, String.format("%d일 이내에서 선택해주세요", relativeLimitDays));
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_pick_period_cancel) {
            dismiss();

        } else if (view.getId() == R.id.dialog_pick_period_apply) {
            onApplyClick(selectedFromDate, selectedToDate);
            dismiss();
        }
    }

    abstract public void onApplyClick(DateTime fromDateTime, DateTime toDateTime);

    @Override
    public void onInvalidDateSelected(Date date) {
        MyUI.toastSHORT(context, String.format("%d일 이내만 선택이 가능합니다", absoluteLimitDays));
    }
}
