package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.enums.ItemStateType;

/**
 * 상품 목록 아이템 데이터 프로토콜
 *
 * @author lucidite
 */

public interface SimpleItemDataProtocol {
    int getItemId();
    String getSerialNumber();
    String getProviderPhoneNumber();
    ItemStateType getItemState();
    String getItemName();
    String getMainImagePath();
    int getPrice();
}
