package com.teampls.shoplus.lib.services.chatbot.protocol;

/**
 * 챗봇 이용고객 관련 기본 데이터 프로토콜.
 *
 * @author lucidite
 */
public interface ChatbotBuyerBaseDataProtocol {
    /**
     * 이용고객 고유 key를 반환한다. 관련 서비스 호출에 이용한다.
     *
     * @return
     */
    String getUserKey();

    /**
     * 이용고객이 등록한 이름을 반환한다.
     * [NOTE] 거래처에 등록된 이름과 다를 수 있으며, 거래처에 있는 경우 이름을 고객에게 묻지 않으므로 빈 문자열일 수 있다.
     *
     * @return
     */
    String getBuyerName();

    /**
     * 이용고객이 입력한 전화번호를 반환한다.
     * [NOTE] 인증되지 않은 경우 신뢰할 수 없는 정보일 가능성이 있음을 고려할 것
     *
     * @return
     */
    String getBuyerPhoneNumber();

    /**
     * 전화번호가 인증되었는지를 반환한다.
     *
     * @return
     */
    boolean isAuthed();
}
