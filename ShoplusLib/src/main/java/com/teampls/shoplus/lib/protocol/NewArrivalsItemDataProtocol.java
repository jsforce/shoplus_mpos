package com.teampls.shoplus.lib.protocol;

import com.teampls.shoplus.lib.enums.ItemCategoryType;

import org.joda.time.DateTime;

import java.util.List;

/**
 * 신상알림 데이터에 포함된 개별 아이템 데이터 프로토콜
 *
 * @author lucidite
 */

public interface NewArrivalsItemDataProtocol {
    int getItemId();

    /**
     * 아이템 발신자 정보를 반환한다.
     * @return 구매 아이템인 경우 판매자(거래기록 생성매장), 신상알림 아이템인 경우 신상알림 발신자
     */
    String getSender();

    /**
     * 아이템 날짜 정보를 반환한다.
     * @return 구매 아이템인 경우 구매일시(거래기록 날짜), 신상알림 아이템인 경우 신상알림 일시
     */
    DateTime getDateTime();
    ItemCategoryType getCategory();
    List<String> getImagePaths();
    String getItemName();
    String getItemDescription();
}
