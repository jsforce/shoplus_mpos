package com.teampls.shoplus.warspear.view_common;

import android.content.Intent;
import android.view.View;

import com.teampls.shoplus.lib.view_base.BaseIsRegistered;

/**
 * Created by Medivh on 2016-12-19.
 */

public class UserIsRegistered extends BaseIsRegistered {

    @Override
    public void onNewUserClick(View view) {
            startActivity(new Intent(context, UserReg.class), null);
            finish();
    }

    @Override
    public void onExistingUserClick(View view) {
        startActivity(new Intent(context, UserLogin.class), null);
        finish();
    }
}
