package com.teampls.shoplus.warspear.view;

import android.content.Context;
import android.content.Intent;

import com.teampls.shoplus.lib.common.UserSettingData;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.CounterpartTransView;
import com.teampls.shoplus.lib.common.SelectedItemRecord;
import com.teampls.shoplus.silverlib.view_silver.SilverSlipCreationView;
import com.teampls.shoplus.warspear.view_main.MainViewPager;

/**
 * Created by Medivh on 2017-02-06.
 */
// dalaran PosSlipCreationView와 유사하다
public class PosSlipCreationView extends SilverSlipCreationView {

    // ItemCartView 종료 후 생성 과정과 동일
    public static void startActivity(Context context, UserRecord counterpart) {
        slipRecord = new SlipRecord(UserSettingData.getInstance(context).getProviderRecord().phoneNumber, counterpart.phoneNumber);
        slipRecord.confirmed = false;
        PosSlipDB.getInstance(context).insert(slipRecord);  // slipDB에 저장
        context.startActivity(new Intent(context, PosSlipCreationView.class));
    }

    public static void startActivity(Context context, SlipRecord slipRecord) {
        PosSlipCreationView.slipRecord = slipRecord; // 이미 slipDB에 있는 내용
        context.startActivity(new Intent(context, PosSlipCreationView.class));
    }

    @Override
    public void onCreate() {
        itemsView.itemGridView.setSearchMores(false); // more 기능 OFF
    }

    public void startItemAndOptionSearch(ItemSearchType itemSearchType, MyOnClick<SelectedItemRecord> onTask) {
        ItemAndOptionSearch.startActivity(context, itemSearchType, slipRecord.counterpartPhoneNumber, false, onTask); // more 기능 OFF, 마지막 boolean은 상세페이지로 넘어갈지 결정
    }

    public void startPosSlipSharingView(SlipFullRecord newFullRecord) {
        PosSlipSharingView.startActivityNewRecord(context, newFullRecord);
    }

    public void refreshAfterCreation() {
        if (MainViewPager.adapter != null)
            MainViewPager.adapter.refreshFragments();
    }

    public void startItemFullViewPager(ItemRecord itemRecord, MyOnTask onTask) {
        ItemFullViewPager.startActivity(context, itemRecord.itemId, false, onTask); // Cart 기능 OFF
    }


}


