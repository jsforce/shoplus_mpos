package com.teampls.shoplus.warspear.common;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database_map.KeyValueDB;
import com.teampls.shoplus.silverlib.common.SilverPreference;
import com.teampls.shoplus.warspear.view_main.ItemsFrag;
import com.teampls.shoplus.warspear.view_main.MainViewPager;
import com.teampls.shoplus.warspear.view_main.SlipItemsFrag;
import com.teampls.shoplus.warspear.view_main.TransFrag;

import java.util.Arrays;
import java.util.List;

public class MyPreference extends SilverPreference {

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, MyPreference.class));
    }

    public static void init(Context context) {
        if (KeyValueDB.getInstance(context).getBool(KEY_PreferenceInitailized, false) == false) {
            initBase(context);
            KeyValueDB.getInstance(context).put(KEY_PreferenceInitailized, true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppWatcher.checkInstanceAndRestart(this) == false) return;
        defaultImageViewRatio = 1.2f;
        doShowWeekday = isShowWeekday(context);
        imageViewRatio = getImageViewRatio(context);

    }

    public void onShowCancelledChanged() {
        if (TransFrag.getInstance().adapter != null)
            TransFrag.getInstance().adapter.blockCancelled(!isShowCancelled(context));
        if (SlipItemsFrag.getInstance().adapter != null)
            SlipItemsFrag.getInstance().adapter.blockCancelled(!isShowCancelled(context));
        doRefresh = true;
        MyUI.toastSHORT(context, String.format("취소기록 보기 변경을 반영했습니다"));
    }

    public void onShiftTimeChanged() {
        doRefresh = true;
        MyUI.toastSHORT(context, String.format("교대시간 변경을 반영했습니다"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (imageViewRatio != getImageViewRatio(context)) {
            ItemsFrag.getInstance().itemGridComposition.adapter.setImageViewRatio(getImageViewRatio(context));
            doRefresh = true;
        }

        if (doShowWeekday != isShowWeekday(context))
            doRefresh = true;

        if (doShowHm.isChanged(context))
            doRefresh = true;

        if (doRefresh && MainViewPager.adapter != null)
            MainViewPager.adapter.refreshFragments();

        if (doRefreshMenu && MainViewPager.instance != null)
            MainViewPager.instance.onMyMenuCreate();
    }

    protected void createPreference() {
        createShiftTimes();
        createShowWeekDay();
        addSilverPreference();

        // UI - 거래기록
        createShowCancelled();

        // UI - 썸네일
        List<String> ratios = Arrays.asList("1.2", "1.0", "0.8");
        addList(KEY_ImageViewRatio, "이미지 비율", "첫화면 이미지들의 세로 길이를 지정합니다. 화면에 상품을 많이 표시할 수 있습니다", ratios, 0);


        // QR 기능
        addCheckBox("QR 스캔 사용", "QR 스캔으로 영수증을 작성합니다 (권한 필요)", KEY_QR_Enabled, false);
        createFontScale();
    }


}