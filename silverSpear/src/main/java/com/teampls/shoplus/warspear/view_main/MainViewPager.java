package com.teampls.shoplus.warspear.view_main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.teampls.shoplus.lib.adapter.MainViewAdapter;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseAwsTaskScheduler;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyDelayTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.PosSlipItemDB;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_old.OldPosSlipDB;
import com.teampls.shoplus.lib.datatypes.ShoplusServiceType;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.dialog.MyGuideDialog;
import com.teampls.shoplus.lib.enums.TaskState;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.services.chatbot.datatypes.ChatbotOrderData;
import com.teampls.shoplus.silverlib.database.MallOrderDB;
import com.teampls.shoplus.silverlib.view.ItemStateSettingView;
import com.teampls.shoplus.silverlib.view.MallOrdersView;
import com.teampls.shoplus.silverlib.view.StockHistoryView;
import com.teampls.shoplus.silverlib.view_silver.SilverMainViewPager;
import com.teampls.shoplus.warspear.common.AppWatcher;
import com.teampls.shoplus.warspear.common.MyPreference;
import com.teampls.shoplus.warspear.view.ItemFullViewPager;
import com.teampls.shoplus.warspear.view.PosSlipListView;
import com.teampls.shoplus.warspear.view.PreviousItemsView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2016-08-31.
 */
public class MainViewPager extends SilverMainViewPager implements ViewPager.OnPageChangeListener {
    public static MainViewPager instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    public void createAwsTaskScheduler() {
        awsTaskScheduler = new AwsTaskScheduler(context, adapter);
    }

    class AwsTaskScheduler extends BaseAwsTaskScheduler {
        public TaskState itemDownloading, transactionDownloading, slipDownloading;

        public AwsTaskScheduler(Context context, MainViewAdapter adapter) {
            super(context, adapter);
        }

        @Override
        public void init() {
            finished = false;
            userSettingDownloading = TaskState.beforeTasking;
            itemDownloading = TaskState.beforeTasking;
            transactionDownloading = TaskState.beforeTasking;
            slipDownloading = TaskState.beforeTasking;
        }

        protected List<TaskState> getTaskStates() {
            List<TaskState> taskStates = new ArrayList<>(0);
            taskStates.add(userSettingDownloading);
            taskStates.add(itemDownloading);
            taskStates.add(transactionDownloading);
            taskStates.add(slipDownloading);
            return taskStates;
        }

        protected void refreshFragments() {
            ItemsFrag.getInstance().refresh();
        }

        @Override
        protected void clearLocalDBs() {
            Log.w("DEBUG_JS", String.format("[AwsTaskScheduler.clearLocalDBs] ... "));
            userDB.clear();
            itemDB.clear();
            PosSlipDB.getInstance(context).clear();
        }

        protected void onTaskFinish() {
            if (getState().isFinished()) {
                mainProgressBar.stop(taskId);

                if (userSettingData.getAllowedShops().size() >= 1)
                    myActionBar.setTitle(userSettingData.getWorkingShop().getShopName(), true);

            } else if (getState().isOnError()) {
                mainProgressBar.stop(taskId);
            }
        }

        public void start() {
            updateUserSetting(new MyOnTask() {
                @Override
                public void onTaskDone(Object result) {
                    // 현재 근무 중인 매장이 있거나 아무 라이센스가 있다면 OK
                    if (userSettingData.hasWorkingShopNotMe() || userSettingData.hasExpireDateTime()) { // 권한을 받은 흔적이 있으면 통과
                        AppWatcher.setActivated(true); // MPOS 권한과 관계없이 여기로 진행이 되면 권한이 있는 것으로 본다
                        if (itemDownloading.isOnTasking())
                            return;
                        download();

                        // 카톡몰 사용시
                        if (userSettingData.isActivated(ShoplusServiceType.ChatBot)) {
                            downloadRecentOrders(new MyOnTask() {
                                @Override
                                public void onTaskDone(Object result) {
                                    List<ChatbotOrderData> requesteds = MallOrderDB.getInstance(context).getRequested();
                                    if (requesteds.size() > 0) {
                                        onMyMenuCreate();
                                        Set<String> requesters = new HashSet<String>();
                                        for (ChatbotOrderData record : requesteds) {
                                            UserRecord userRecord = userDB.getRecordWithNumber(record.getBuyerPhoneNumber());
                                            requesters.add(userRecord.getName());
                                        }
                                        MyUI.toast(context, String.format("새주문이 있습니다 (%s)", TextUtils.join(",", requesters)));
                                    }
                                }
                            });
                        }

                    } else { // 앱 전체에 대해 아무 권한이 없는 경우에만 가입 안내창이 뜬다
                        AppWatcher.setActivated(false);
                        new MPosRegistrationDialog(context).show();
                        itemDB.clear();
                        itemDownloading = TaskState.afterTasking;
                        onTaskFinish();
                    }
                }
            });
        }

        private void download() {
            downloadCounterparts(new MyOnTask() {
                @Override
                public void onTaskDone(Object result) { // 뭔가 새로 업데이트가 된 것이 있을때만 여기로 진입
                    TransFrag.getInstance().adapter.notifyDataSetChanged();
                    SlipItemsFrag.getInstance().adapter.notifyDataSetChanged();
                }
            });

            itemDownloading = TaskState.onTasking;
            ItemsFrag.getInstance().downloadItems(new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    if (result) {
                        itemDownloading = TaskState.afterTasking;
                        onTaskFinish();
                    } else {
                        itemDownloading = TaskState.onError;
                        errorMessage = "아이템 다운로드 중 에러가 발생했습니다";
                        onTaskFinish();
                    }
                }
            });

            transactionDownloading = TaskState.onTasking;
            TransFrag.getInstance().downloadRecentTrans(new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    if (result) {
                        transactionDownloading = TaskState.afterTasking;
                        onTaskFinish();
                    } else {
                        transactionDownloading = TaskState.onError;
                        errorMessage = "거래기록 다운로드 중 에러가 발생했습니다";
                        onTaskFinish();
                    }
                }
            });

            slipDownloading = TaskState.onTasking;
            SlipItemsFrag.getInstance().downloadSlips(new MyOnTask<Boolean>() {
                @Override
                public void onTaskDone(Boolean result) {
                    if (result) {
                        slipDownloading = TaskState.afterTasking;
                        onTaskFinish();
                    } else {
                        slipDownloading = TaskState.onError;
                        errorMessage = "거래기록 다운로드 중 에러가 발생했습니다";
                        onTaskFinish();
                    }
                }
            });
        }
    }

    protected void doFinish() {
        super.doFinish();
        stopService(new Intent(context, AppWatcher.class).setPackage(context.getPackageName()));
    }

    @Override
    public void startFullViewPager(int newItemId) {
        ItemFullViewPager.startActivity(context, newItemId);
    }

    @Override
    public void addTabs() {
        adapter.clearFragments();
        adapter.addFragment(ItemsFrag.getInstance(0));
        adapter.addFragment(TransFrag.getInstance(1));
        adapter.addFragment(SlipItemsFrag.getInstance(2));
        refreshTabs();
    }

    public void onMyMenuCreate() {
        if (AppWatcher.isNetworkConnected() == false) {
            onMyMenuCreateDisconnected();
            return;
        }

        PosSlipDB posSlipDB = PosSlipDB.getInstance(context);
        myActionBar.clear();
        myActionBar
                .add(MyMenu.addItemFolder)
                .add(posSlipDB.hasValue(PosSlipDB.Column.confirmed, false) ? MyMenu.makeSlipOnCreating : MyMenu.makeSlip)
                .add(MyPreference.mallMenuEnabled.getValue(context), MallOrderDB.getInstance(context).getRequested().size() == 0 ? MyMenu.mall : MyMenu.mallUncheck)
                .add(MyMenu.refresh)
                .add(MyMenu.more)

                .add(MyMenu.gridSetting)
                .add(MyMenu.gridMyInfo)
                .add(MyMenu.gridCustomers)
                .add(MyMenu.gridUserQuestion)

                .add(MyMenu.gridPreviousItems)
                .add(MyMenu.gridChangeState)
                .add(MyMenu.gridStockHistory)
                .add(MallOrderDB.getInstance(context).getRequested().size() == 0 ? MyMenu.gridMall : MyMenu.gridMallUnchecked)

                .add(MyMenu.gridReceiptSetting)
                .add(MyMenu.gridCustomColorNames)
                .add(MyMenu.gridUserGuide)
                .add(MyMenu.gridPrinterSetting)

                .add(MyMenu.gridPrintNameCards)
                .add(MyMenu.gridShoplusLink)
                .add(MyMenu.gridUpdateApp)
                .add(MyMenu.gridClose)
        ;
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {
        if (BaseAppWatcher.isLoggedIn() == false) {
            MyUI.toastSHORT(context, String.format("현재 로그인 중입니다. 잠시 후 사용해 주세요"));
            return;
        }
        super.onMyMenuSelected(myMenu);

        switch (myMenu) {
            case gridMallUnchecked:
            case gridMall:
            case mall:
            case mallUncheck:
                if (userSettingData.isActivated(ShoplusServiceType.ChatBot) == false) {
                    MyUI.toastSHORT(context, String.format("카톡몰 라이센스가 없습니다"));
                    return;
                }
                MallOrdersView.startActivity(context, new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        onMyMenuCreate();
                    }
                });
                break;

            case gridPreviousItems:
                PreviousItemsView.startActivity(context, ItemsFrag.getInstance().itemGridComposition.adapter.getCount());
                break;

            case gridChangeState:
                ItemStateSettingView.startActivity(context);
                break;

            case gridStockHistory:
                StockHistoryView.startActivity(context);
                break;

            case addItemFolder:
                if (ItemsFrag.getInstance().itemGridComposition.adapter.getCount() >= AppWatcher.getItemCountLimit()) {
                    new OnItemCountMaxDialog(context).show();
                    return;
                }

                int selectionLimit = AppWatcher.getItemCountLimit() - ItemsFrag.getInstance().itemGridComposition.adapter.getCount();
                newItemCreator.oneSelectImages(selectionLimit, new MyOnTask() {
                    @Override
                    public void onTaskDone(Object result) {
                        ItemsFrag.getInstance().refresh();
                        new MyDelayTask(context, 1000) {
                            @Override
                            public void onFinish() {
                                ItemsFrag.getInstance().itemGridComposition.adapter.notifyDataSetChanged(); // 다운 후 이미지가 엉키는 시간을 고려해서 새로 그려준다.
                            }
                        };
                    }
                });
                break;
            case makeSlip:
            case makeSlipOnCreating:
                if (awsTaskScheduler.counterpartDownloading.isFinished() == false)
                    MyUI.toastSHORT(context, String.format("고객 정보 확인중. 최근 고객은 전화번호로 표시될 수 있습니다."));
                PosSlipListView.startActivityForResult(context);
                break;
            case gridSetting:
                MyPreference.startActivity(context);
                break;
        }
    }

    class OnItemCountMaxDialog extends MyButtonsDialog {

        public OnItemCountMaxDialog(final Context context) {
            super(context, String.format("최대개수 %d개 도달", AppWatcher.getItemCountLimit()), "아래 방법 중 하나로 관리 하실 수 있습니다.");
            setDialogWidth(0.9, 0.6);

            addButton("판매완료로 처리 (추천)", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MyGuideDialog(context, "판매 완료로 처리", "더 이상 진행하지 않는 상품을 상품 상세 페이지에서 " +
                            "[판매완료]로 변경합니다.\n\n변경 후 해당 상품은 관리 목록에서 제외되지만 관련된 모든 데이터는 " +
                            "보존됩니다.\n\n완료된 상품은 [...] > [이전상품]에서\n다시 살릴 수 있습니다").show();
                }
            });


            addButton("기존 상품을 삭제 (비추천)", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MyGuideDialog(context, "기존 상품을 삭제", "해당 상품을 영구 삭제합니다. 판매 등 관련 데이터도 같이 삭제되므로 권장하지 않습니다.").show();
                }
            });

            addButton("카탈로그 앱 사용", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyUI.toast(context, String.format("대규모 상품은 카탈로그 앱에서 관리하실 수 있습니다. 문의 주세요"));
                    MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
                }
            });

        }
    }

    class MPosRegistrationDialog extends RegistrationDialog {

        public MPosRegistrationDialog(final Context context) {
            super(context, "무료 POS 등록 안내", "매장폰으로 등록하시고 무료 POS를 이용해 보세요. 동대문 도매가 아니시면 문의하기로 상담해 주세요.");
        }

        protected void createDialog() {
            addWorkingShops();
            addGetMPosLicenseByKakao();
            addGetMPosLicenseDirectly();
            addRestartApp();
            addContactUs();
            addHomepage();
        }

    }

}