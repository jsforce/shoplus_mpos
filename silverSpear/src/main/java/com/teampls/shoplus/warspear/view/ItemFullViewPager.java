package com.teampls.shoplus.warspear.view;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.dialog.DeleteSafelyDialog;
import com.teampls.shoplus.lib.dialog.MyButtonsDialog;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.view_silver.SilverItemFullViewPager;

import java.util.List;

/**
 * Created by Medivh on 2018-04-13.
 */

public class ItemFullViewPager extends SilverItemFullViewPager {

    public static void startActivity(Context context, List<ItemRecord> records, int position) {
        SilverItemFullViewPager.records = records;
        SilverItemFullViewPager.currentPosition = position;
        SilverItemFullViewPager.doEnableCart = true;
        ((Activity) context).startActivityForResult(new Intent(context, ItemFullViewPager.class), BaseGlobal.RequestRefresh);
        ItemDB.getInstance(context).addRecent(records.get(position).itemId, BaseGlobal.RecentItemLimit);
    }

    public static void startActivity(Context context, int itemId) {
        startActivity(context, itemId, true, null);
    }

    public static void startActivity(Context context, int itemId, boolean doEnableCart, MyOnTask onTask) {
        SilverItemFullViewPager.records.clear();
        SilverItemFullViewPager.records.add(ItemDB.getInstance(context).getRecordBy(itemId));
        SilverItemFullViewPager.currentPosition = 0;
        SilverItemFullViewPager.onTask = onTask;
        SilverItemFullViewPager.doEnableCart = doEnableCart;
        context.startActivity(new Intent(context, ItemFullViewPager.class), null);
        ItemDB.getInstance(context).addRecent(itemId, BaseGlobal.RecentItemLimit);
    }

    @Override
    protected void setViewAdapter() {
        onCancelClick = new MyOnCancelClick();
        onFunctionClick = new MyOnFunctionClick();
        viewAdapter = new ItemFullViewAdapter(context, records, ItemFullViewPager.doEnableCart);
        viewAdapter.setOnCancelClick(onCancelClick);
        viewAdapter.setOnFunctionClick(onFunctionClick);
    }

    public void onDeleteClick() {
        if (viewAdapter.getCount() <= 0) {
            MyUI.toastSHORT(context, String.format("삭제할 상품이 없습니다"));
            return;
        }
        new DeleteDialog(context).show();
    }

    class DeleteDialog extends MyButtonsDialog {

        public DeleteDialog(final Context context) {
            super(context, "상품 삭제", "삭제시 상품과 관련된 모든 자료를 삭제합니다. 더 이상 진행하지 않는 상품은 판매완료로 처리해 주세요.");

            addButton("판매 완료 (권장)", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();

                    final ItemRecord record = viewAdapter.getRecord(getCurrentPosition());
                    final int currentPosition = getCurrentPosition();

                    new MyServiceTask(context, "deleteDialog", "변경중...") {
                        @Override
                        public void doInBackground() throws MyServiceFailureException {
                            service.updateItemState(userSettingData.getUserShopInfo(), record.itemId, ItemStateType.SOLDOUT);
                            record.state = ItemStateType.SOLDOUT;
                            itemDB.updateOrInsert(record);
                            MyUI.toastSHORT(context, String.format("판매 완료"));
                        }

                        @Override
                        public void onPostExecutionUI() {
                            if (itemDB.getSize() == 0) {
                                onFinish();
                            } else {
                                removeView(currentPosition);
                                removeVisitedPosition(currentPosition);
                                if (viewAdapter.getCount() == 0)
                                    onFinish(); //  남아 있는 view가 없음
                            }
                        }
                    };
                }
            });

            addButton("영구 삭제", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                    new DeleteSafelyDialog(context, "상품 삭제") {
                        @Override
                        public void onDeleteClick() {
                            deleteItem();
                        }
                    };
                }
            });
        }
    }
}
