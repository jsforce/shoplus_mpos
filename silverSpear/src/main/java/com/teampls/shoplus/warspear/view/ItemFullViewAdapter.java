package com.teampls.shoplus.warspear.view;

import android.content.Context;
import android.graphics.Bitmap;

import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyGraphics;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.silverlib.view_silver.SilverItemFullViewAdapter;
import com.teampls.shoplus.warspear.common.MyPreference;

import java.util.List;

/**
 * Created by Medivh on 2017-03-27.
 */

public class ItemFullViewAdapter extends SilverItemFullViewAdapter  {

    public ItemFullViewAdapter(Context context, List<ItemRecord> records, boolean doEnableSlip) {
        super(context, records, doEnableSlip);
    }

    @Override
    public void startPreference() {
        MyPreference.startActivity(context);
    }

    @Override
    protected BaseViewHolder getFullViewHolder() {
        return new FullViewHolder();
    }

    //  ViewHolder가 실질적인 ItemFullView 역할을 수행 (메뉴 기능은 FullViewPager에 있음)
    class FullViewHolder extends SilverFullViewHolder {

        protected void onItemStateTypeChanged(ItemStateType state) {
            switch (state) {
                default:
                    MyUI.toastSHORT(context, String.format("상품 상태를 변경했습니다"));
                    break;
                case SOLDOUT:
                    MyUI.toastSHORT(context, String.format("판매완료 상품은 목록에서 제외됩니다"));
                    break;
            }
        }


        // MPOS에서는 Thumbnail 크기 이미지만 제공
        @Override
        protected void showImage(final ItemRecord itemRecord) {
            imageLoader.retrieveThumb(itemRecord, new MyOnTask<Bitmap>() {
                @Override
                public void onTaskDone(Bitmap result) {
                    bitmap = result;
                    imageView.setImageBitmap(bitmap);
                    if (Empty.isEmptyImage(itemRecord.mainImagePath))
                        imageView.getLayoutParams().height = imageView.getLayoutParams().height / 3;
                    else
                        imageView.getLayoutParams().height = MyGraphics.getHeightPx(context, 0, bitmap);
                    setYPos(0);
                }
            });
        }
    }


}
