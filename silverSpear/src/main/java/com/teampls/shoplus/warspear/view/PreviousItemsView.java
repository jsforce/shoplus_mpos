package com.teampls.shoplus.warspear.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.protocol.SimpleItemDataProtocol;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.view_silver.SilverPreviousItemsView;
import com.teampls.shoplus.warspear.R;
import com.teampls.shoplus.warspear.common.AppWatcher;

import org.joda.time.DateTime;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2018-11-12.
 */

public class PreviousItemsView extends SilverPreviousItemsView {
    private static int currentItemCount = 300;
    private Set<Integer> toOnSaleItems = new HashSet<>();

    public static void startActivity(Context context, int currentItemCount) {
        PreviousItemsView.currentItemCount = currentItemCount;
        ((Activity) context).startActivityForResult(new Intent(context, PreviousItemsView.class), BaseGlobal.RequestRefresh);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppWatcher.checkInstanceAndRestart(this) == false) return;
        navigator.setDateTime(BaseUtils.toMonth(DateTime.now())); // 이번달을 기본으로
        myEmptyGuide.setContactUsButtonText(String.format("%s검색", navigator.getDateTime().toString(BaseUtils.YM_Kor_Format)));
        setVisibility(View.GONE, R.id.items_header_container);
        tvNotice.setText("판매중으로 변경하면 복구됩니다");
        downloadSoldouts(navigator.getDateTime());
    }

    @Override
    public void startItemFullViewPager(ItemRecord itemRecord) {
        ItemFullViewPager.startActivity(context, itemRecord.toList(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (clickedRecord.state == ItemStateType.SOLDOUT)
            toOnSaleItems.remove((Integer) clickedRecord.itemId);
        else
            toOnSaleItems.add(clickedRecord.itemId);
        refreshHeader();
    }

    protected void onTargetMonthSelected(DateTime dateTime) {
        if (downloadedMonth.contains(dateTime)) {
            adapter.filterMonths(dateTime);
            refresh();
        } else {
            downloadSoldouts(dateTime);
        }
    }

    private int getAvailableCount() {
        return Math.max(AppWatcher.getItemCountLimit() -  currentItemCount - toOnSaleItems.size(), 0);
    }

    protected void refreshHeader() {
        tvSelectedCount.setText(String.format("최대 %d개까지", getAvailableCount()));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (getAvailableCount() <= 0) {
            MyUI.toastSHORT(context, String.format("먼저 기존 상품을 판매완료로 내려주세요"));
            return;
        }
        super.onItemClick(parent, view, position, id);
    }

    private void downloadSoldouts(final DateTime month) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(String.format("%s 상품 다운 중...", month.toString(BaseUtils.YM_Kor_Format)));
        progressDialog.show();

        new MyServiceTask(context, "downloadItems") {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<SimpleItemDataProtocol> protocolList = itemService.getMontlySoldoutItemList(userSettingData.getUserShopInfo(), month);
                mItemDB.deleteByMonth(month);
                for (SimpleItemDataProtocol  protocol : protocolList)
                    mItemDB.insert(new ItemRecord(protocol, month.plusSeconds(protocol.getItemId())));
                adapter.filterMonths(month);
                downloadedMonth.add(month);
                MyUI.toastSHORT(context, String.format("%d개 발견", protocolList.size()));
            }

            @Override
            public void onPostExecutionUI() {
                progressDialog.dismiss();
                refresh();
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                progressDialog.dismiss();
            }
        };
    }
}
