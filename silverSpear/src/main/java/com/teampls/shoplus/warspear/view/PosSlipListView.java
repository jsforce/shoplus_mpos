package com.teampls.shoplus.warspear.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.teampls.shoplus.lib.common.BaseAwsService;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyMenu;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.database_memory.MSlipDB;
import com.teampls.shoplus.lib.database_memory.MTransDB;
import com.teampls.shoplus.lib.datatypes.SlipDataKey;
import com.teampls.shoplus.lib.event.MyOnAsync;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.view.MyAllUsersView;
import com.teampls.shoplus.lib.view_base.BasePosSlipListView;
import com.teampls.shoplus.silverlib.database.PosSlipDBAdapter;
import com.teampls.shoplus.silverlib.view_silver.SilverSlipListView;
import com.teampls.shoplus.warspear.common.AppWatcher;
import com.teampls.shoplus.warspear.common.MyPreference;
import com.teampls.shoplus.warspear.view_main.MainViewPager;

import java.util.ArrayList;

/**
 * Created by Medivh on 2017-05-20.
 */

public class PosSlipListView extends SilverSlipListView {

    public static void startActivityForResult(Context context) {
        ((Activity) context).startActivityForResult(new Intent(context, PosSlipListView.class), BaseGlobal.REQUST_MENU_REFRESH);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppWatcher.checkInstanceAndRestart(this) == false) return;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        SlipRecord slipRecord = adapter.getRecord(position);
        if (slipRecord.confirmed) {
            PosSlipSharingView.startActivity(context, posSlipDB.getFullRecord(slipRecord.id));
        } else {
            if (slipRecord.counterpartPhoneNumber.isEmpty()) {
                MyAllUsersView.startActivity(context, new MyOnClick<UserRecord>() {
                    @Override
                    public void onMyClick(View view, UserRecord record) {
                        PosSlipCreationView.startActivity(context, record);
                    }
                });
            } else {
                PosSlipCreationView.startActivity(context, slipRecord);
            }
        }
    }

    public void onApplyClick() {
        MyAllUsersView.startActivity(context, new MyOnClick<UserRecord>() {
            @Override
            public void onMyClick(View view, UserRecord record) {
                if (posSlipDB.hasUnconfirmedRecord(record.phoneNumber)) {
                    MyUI.toastSHORT(context, String.format("거래 기록을 작성 중인 거래처입니다"));
                    SlipRecord slipRecord = posSlipDB.getUnconfirmedRecord(record.phoneNumber);
                    PosSlipCreationView.startActivity(context, slipRecord);

                } else {
                    PosSlipCreationView.startActivity(context, record);
                }
            }
        });
    }

    @Override
    protected void cancelSlipManually(final SlipDataKey slipDataKey, final boolean doCopy) {
        boolean doUpdateItemDB = true;
        BaseAwsService.cancelSlip(context, slipDataKey, doUpdateItemDB, new MyOnAsync<SlipFullRecord>() {
            @Override
            public void onSuccess(final SlipFullRecord copiedFullRecord) {
                SlipRecord slipRecord = posSlipDB.getRecordByKey(slipDataKey);
                if (slipRecord.id != 0) {
                    slipRecord.cancelled = true;
                    posSlipDB.update(slipRecord.id, slipRecord);
                    MTransDB.getInstance(context).updateOrInsert(slipRecord);
                    MSlipDB.getInstance(context).updateOrInsert(slipRecord);
                }

                if (doCopy) {
                    posSlipDB.insert(copiedFullRecord, new ArrayList<String>(), new ArrayList<String>());
                    MyUI.toastSHORT(context, String.format("재작성을 위해 복사하고 해당 거래를 취소했습니다"));
                } else {
                    MyUI.toastSHORT(context, String.format("해당 거래를 취소했습니다"));
                }
                onResume();
            }

            @Override
            public void onError(Exception e) {
            }

        }, new MyOnAsync() {
            @Override
            public void onSuccess(Object result) {
                if (MainViewPager.adapter != null) {
                    MainViewPager.adapter.refreshFragments(); // 전체 refreshDay
                    MyUI.toastSHORT(context, String.format("취소건 재고 반영"));
                }
            }

            @Override
            public void onError(Exception e) {

            }
        });

    }

}
