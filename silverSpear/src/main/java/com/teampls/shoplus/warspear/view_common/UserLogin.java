package com.teampls.shoplus.warspear.view_common;

import android.content.Intent;

import com.teampls.shoplus.lib.view_base.BaseLogin;
import com.teampls.shoplus.warspear.view_main.Welcome;

/**
 * Created by Medivh on 2016-08-31.
 */
public class UserLogin extends BaseLogin {

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, UserIsRegistered.class), null);
        finish();
    }

    public void onLogin() {
        startActivity(new Intent(context, Welcome.class));
        finish();
    }

    public void onResetPassword() {
        startActivity(new Intent(context, Welcome.class));
        finish();
    }

}