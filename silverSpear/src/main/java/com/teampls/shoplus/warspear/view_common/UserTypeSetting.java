package com.teampls.shoplus.warspear.view_common;

import android.content.Intent;
import android.view.View;

import com.teampls.shoplus.lib.view_base.BaseUserTypeSetting;

/**
 * Created by Medivh on 2016-12-19.
 */

public class UserTypeSetting extends BaseUserTypeSetting {

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, UserIsRegistered.class), null);
        finish();
    }

    @Override
    public void onProviderClick(View view) {
        UserReg.startActivity(context);
        finish();
    }

    @Override
    public void onEmployeeClick(View view) {
        UserReg.startActivity(context);
        finish();
    }

    @Override
    public void onBuyerClick(View view) {
        UserReg.startActivity(context);
        finish();
    }
}
