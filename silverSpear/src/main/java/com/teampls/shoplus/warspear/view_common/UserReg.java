package com.teampls.shoplus.warspear.view_common;

import android.content.Context;
import android.content.Intent;

import com.teampls.shoplus.lib.view_base.BaseReg;

/**
 * Created by Medivh on 2016-08-31.
 */
public class UserReg extends BaseReg {

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, UserReg.class));
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, UserIsRegistered.class), null);
        finish();
    }

    public void onRegister() {
        startActivity(new Intent(getApplicationContext(), UserConfirm.class));
        finish();
    }

}