package com.teampls.shoplus.warspear.common;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.common.BaseAppWatcher;
import com.teampls.shoplus.silverlib.common.SilverAppWatcher;

/**
 * Created by Medivh on 2016-12-09.
 */

public class AppWatcher extends SilverAppWatcher {
    private static int ItemLimit = 300;
    private static int ItemLimit_Test = 30;

    public static int getItemCountLimit() {
        return MyAWSConfigs.isProduction()? ItemLimit : ItemLimit_Test;
    }

}
