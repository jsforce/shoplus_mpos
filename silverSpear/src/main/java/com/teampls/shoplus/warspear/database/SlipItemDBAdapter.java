package com.teampls.shoplus.warspear.database;

import android.content.Context;

import com.teampls.shoplus.lib.adapter.BaseSlipItemDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.database_memory.MemorySlipDB;
import com.teampls.shoplus.warspear.common.MyPreference;

/**
 * Created by Medivh on 2018-04-17.
 */

public class SlipItemDBAdapter extends BaseSlipItemDBAdapter {

    public SlipItemDBAdapter(Context context, MemorySlipDB memorySlipDB) {
        super(context, memorySlipDB);
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new CommonSlipItemsViewHolder();
    }

    class CommonSlipItemsViewHolder extends SlipItemsViewHolder {
        @Override
        public void update(int position) {
            super.update(position);

            if (MyPreference.isShowWeekday(context)) {
                if (MyDevice.isTablet(context))
                    tvDate.setText(record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format)+String.format(" (%s)", BaseUtils.getDayOfWeekKor(record.salesDateTime)));
                else
                    tvDate.setText(record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format)+String.format("\n(%s)", BaseUtils.getDayOfWeekKor(record.salesDateTime)));
            } else {
                tvDate.setText(record.salesDateTime.toString(BaseUtils.MD_FixedSize_Format));
            }

            if (clickedPositions.contains(position) == false)
                setBackgroundColorByShiftTimes();
        }
    }
}
