package com.teampls.shoplus.warspear.view_main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.teampls.shoplus.lib.adapter.BaseDBAdapter;
import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.CommonServiceTask;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database.SlipSummaryRecord;
import com.teampls.shoplus.lib.database_memory.MSortingKey;
import com.teampls.shoplus.lib.database_memory.MTransDB;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.view.MainProgressBar;
import com.teampls.shoplus.lib.view_base.BaseTransFrag;
import com.teampls.shoplus.lib.view_module.BaseFloatingButtons;
import com.teampls.shoplus.warspear.R;
import com.teampls.shoplus.warspear.common.MyPreference;
import com.teampls.shoplus.warspear.database.SlipDBAdapter;
import com.teampls.shoplus.warspear.view.PosSlipListView;
import com.teampls.shoplus.warspear.view.PosSlipSharingView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2018-04-16.
 */

public class TransFrag extends BaseTransFrag {
    private static TransFrag instance = null;
    public SlipDBAdapter adapter;
    protected MTransDB mTransDB;
    private SlipsFloatingButtons floatingButtons;
    private TextView tvSummary;

    public static TransFrag getInstance(int tabIndex) {
        if (instance == null)
            instance = new TransFrag();
        TransFrag.tabIndex = tabIndex;
        return instance;
    }

    public static TransFrag getInstance() {
        if (instance == null)
            instance = new TransFrag();
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = "최근거래";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setVisibility(view, View.GONE, R.id.slips_datetime_header, R.id.slips_notice);
        setVisibility(view, View.VISIBLE, R.id.slips_counterpart_header);

        findTextViewById(view, R.id.slips_summaryByMonth, "전체거래");
        tvSummary = findTextViewById(view, R.id.slips_summary, "");

        myEmptyGuide.setContactUsButton("거래작성", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PosSlipListView.startActivityForResult(context);
            }
        });

        floatingButtons = new SlipsFloatingButtons(view);
        isViewCreated = true;
        return view;
    }

    @Override
    public BaseDBAdapter setAdapter() {
        mTransDB = MTransDB.getInstance(context);
        adapter = new SlipDBAdapter(context, mTransDB);
        adapter.setSortingKey(MSortingKey.Date);
        adapter.blockCancelled(!MyPreference.isShowCancelled(context));
        return adapter;
    }

    @Override
    protected void setAdapterByDateTime(DateTime fromDateTime, DateTime toDateTime) {
        adapter.resetFilters().blockCancelled(!MyPreference.isShowCancelled(context));
        // .filterPeriod(fromDateTime, toDateTime); 사용하지 않는다
    }

    @Override
    public void downloadTransactions(DateTime fromDateTime, DateTime toDateTime, MyOnTask onTask) {
        // Not Use
    }

    @Override
    public void downloadTransactions(final DateTime day, MyOnTask onTask) {
        // Not Use
    }

    @Override
    public void downloadCounterpartTransactions(String phoneNumber) {
        // Not Use
    }

    protected void downloadRecentTrans(final MyOnTask<Boolean> onTask) {
        final String taskId = "download.recent.slips";
        final MainProgressBar mainProgressBar = MainViewPager.mainProgressBar;
        mainProgressBar.start(taskId, "거래기록 다운로드", 2);

        new CommonServiceTask(context, "downloadRecentTrans") {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<SlipDataProtocol> protocols = transactionService.getRecentTransactions(userSettingData.getUserShopInfo());
                mainProgressBar.next(taskId);
                mTransDB.set(protocols, true);
                mainProgressBar.stop(taskId);
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone(true);
                refresh();
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                if (onTask != null)
                    onTask.onTaskDone(false);
            }

        };
    }

    @Override
    protected void onSummaryByMonthClick() {
        if (MyApp.has(context, MyApp.Exodar) == false)
            MyUI.toastSHORT(context, String.format("모든 거래기록은 샵플 거래내역에서 확인하실 수 있습니다"));
        MyApp.Exodar.open(context);
    }

    @Override
    public void refresh() {
        if (adapter == null) return;

        new MyAsyncTask(context, "refreshDay") {

            @Override
            public void doInBackground() {
                adapter.generate(false);
            }

            @Override
            public void onPostExecutionUI() {
                adapter.applyGeneratedRecords();
                adapter.notifyDataSetChanged();
                myEmptyGuide.refresh();
                if ((MainViewPager.tabActionBar == null) || (MainViewPager.tabActionBar.getTabCount() <= tabIndex))
                    return;
                    MainViewPager.tabActionBar.getTabAt(tabIndex).setText(String.format("%s (%s)", title, adapter.getCount()));

                Set<String> counterparts = new HashSet<>();
                List<SlipItemRecord> slipItemRecords = new ArrayList<>();
                int dayOfYear = DateTime.now().getDayOfYear();
                for (SlipRecord slipRecord : adapter.getRecords()) {
                    if (slipRecord.cancelled) // 취소건 제외
                        continue;
                    if (slipRecord.createdDateTime.getDayOfYear() != dayOfYear) // 오늘 아닌것 제외
                        continue;
                    slipItemRecords.addAll(mTransDB.items.getRecordsBy(slipRecord.getSlipKey().toSID()));
                    counterparts.add(slipRecord.getCounterpart(context));
                }
                SlipSummaryRecord slipSummaryRecord = new SlipSummaryRecord(slipItemRecords);
                tvSummary.setText(String.format("금일 : 거래처 %d곳,  %d건 %s", counterparts.size(), slipSummaryRecord.count,
                        BaseUtils.toCurrencyStr(slipSummaryRecord.amount)));
            }
        };
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        SlipRecord slipRecord = adapter.getRecord(position);
        PosSlipSharingView.startActivity(context, mTransDB.getFullRecord(slipRecord.getSlipKey().toSID()));
    }

    class SlipsFloatingButtons extends BaseFloatingButtons {

        public SlipsFloatingButtons(View view) {
            super(view);
            setVisibility(view, View.GONE, R.id.slips_resetCounterpart, R.id.slips_show_floating, R.id.slips_delete,
                    R.id.slips_hide_floating, R.id.slips_exportAsExcel);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
