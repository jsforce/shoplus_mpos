package com.teampls.shoplus.warspear.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.enums.ItemSearchType;
import com.teampls.shoplus.lib.event.MyOnClick;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.common.SelectedItemRecord;
import com.teampls.shoplus.silverlib.view_silver.SilverItemAndOptionSearch;
import com.teampls.shoplus.warspear.common.AppWatcher;

/**
 * Created by Medivh on 2017-04-23.
 */

public class ItemAndOptionSearch extends SilverItemAndOptionSearch {

    public static void startActivity(Context context, ItemSearchType itemSearchType, String buyerPhoneNumber, boolean searchMoreEnabled, MyOnClick<SelectedItemRecord> onApplyClicked) {
        ItemAndOptionSearch.itemSearchType = itemSearchType;
        ItemAndOptionSearch.buyerPhoneNumber = buyerPhoneNumber;
        ItemAndOptionSearch.onApplyClicked = onApplyClicked; // 마지막 boolean은 상세 페이지로 넘어갈지 결정
        ItemAndOptionSearch.searchMoreEnabled = searchMoreEnabled;
        ((Activity) context).startActivityForResult(new Intent(context, ItemAndOptionSearch.class), BaseGlobal.RequestRefresh);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppWatcher.checkInstanceAndRestart(this) == false) return;
    }

    public void startItemFullViewPager(final ItemRecord itemRecord) {
        ItemFullViewPager.startActivity(context, itemRecord.itemId, false, new MyOnTask() {
            @Override
            public void onTaskDone(Object result) {
                onMyItemClick(itemDB.getRecordBy(itemRecord.itemId));
            }
        });
    }

}
