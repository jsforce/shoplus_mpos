package com.teampls.shoplus.warspear.view_main;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.ItemRecord;
import com.teampls.shoplus.lib.datatypes.ItemDataBundle;
import com.teampls.shoplus.lib.enums.ItemStateType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.view.MainProgressBar;
import com.teampls.shoplus.lib.view_base.BaseItemsFrag;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.silverlib.database.ItemDBAdapter;
import com.teampls.shoplus.silverlib.dialog.IntroDialog;
import com.teampls.shoplus.warspear.common.MyPreference;
import com.teampls.shoplus.warspear.view.ItemFullViewPager;

public class ItemsFrag extends BaseItemsFrag {
    public static ItemsFrag instance = null;
    protected static int tabIndex = 0;
    private String title = "상품";
    private ItemDB itemDB;

    public static ItemsFrag getInstance(int tabIndex) {
        if (instance == null)
            instance = new ItemsFrag();
        ItemsFrag.tabIndex = tabIndex;
        return instance;
    }

    public static ItemsFrag getInstance() {
        if (instance == null)
            instance = new ItemsFrag();
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        itemDB = ItemDB.getInstance(context);
    }

    @Override
    public int getTabIndex() {
        return tabIndex;
    }

    @Override
    protected void setAdaptor() {
        if (itemGridComposition.adapter == null)
            itemGridComposition.adapter = new ItemDBAdapter(context);
        itemGridComposition.adapter.setViewType(ItemDBAdapter.ItemsFrag);
        itemGridComposition.setSortingKey("itemsfrag.sorting");
        itemGridComposition.adapter.setImageViewRatio(MyPreference.getImageViewRatio(context));
        itemGridComposition.setItemStateKey("itemsfrag.itemstate");
        itemGridComposition.adapter.filterStates(itemGridComposition.getItemStates());
        itemGridComposition.setFilterNameKey("itemsfrag.filtername");
    }

    protected void setUiComponents(View view) {
        itemGridComposition.myEmptyGuide.setMessage("내 상품들을 이미지로 보여줍니다" +
                "\n\n이미지가 없는 경우 메뉴바에서\n '상품추가'를 눌러주세요");
        itemGridComposition.myEmptyGuide.setContactUsButton("사용 방법 보기", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new IntroDialog(context).show();
            }
        });
        itemGridComposition.setSearchMores(false);
    }

    @Override
    public void refresh() {
        if (itemGridComposition == null) return;
        if (itemGridComposition.adapter == null) return;
        new MyAsyncTask(context, "ItemGridComposition") {
            int stockSum = 0;

            public void doInBackground() {
                for (ItemRecord record : itemDB.getRecords())
                    if (record.state == ItemStateType.SOLDOUT)
                        itemDB.delete(record.id);
                itemGridComposition.adapter.generate(false);
            }

            public void onPostExecutionUI() {
                itemGridComposition.adapter.applyGeneratedRecords();
                itemGridComposition.adapter.notifyDataSetChanged();
                for (ItemRecord record : itemGridComposition.adapter.getRecords())
                    stockSum = stockSum + Math.max(0, itemGridComposition.itemDB.options.getPositiveStockSum(record.itemId));
                itemGridComposition.onRefresh();
                if ((MainViewPager.tabActionBar == null) || (MainViewPager.tabActionBar.getTabCount() <= tabIndex))
                    return;
                String tabTitle = String.format("%d상품 %d개", itemGridComposition.adapter.getCount(), stockSum);
                MainViewPager.tabActionBar.getTabAt(tabIndex).setText(tabTitle);
            }
        };
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void onSelected() {
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        ItemFullViewPager.startActivity(context, itemGridComposition.adapter.getRecords(), position);
    }

    public void downloadItems(final MyOnTask<Boolean> onTask) {
        final MainProgressBar mainProgressBar = MainViewPager.mainProgressBar;
        mainProgressBar.start(MainViewPager.AwsTaskScheduler.taskId, "아이템 다운로드 중", 2);

        new MyServiceTask(context, "downloadItems") {
            @Override
            public void doInBackground() throws MyServiceFailureException {
                // mPos == True, soldout은 제외 == True
                ItemDataBundle itemDataBundle = itemService.getItemsWithStock(userSettingData.getUserShopInfo(), true, true);
                mainProgressBar.next(MainViewPager.AwsTaskScheduler.taskId);
                itemDB.refreshWithProgress(itemDataBundle, mainProgressBar, MainViewPager.AwsTaskScheduler.taskId);
                mainProgressBar.stop(MainViewPager.AwsTaskScheduler.taskId);
            }

            @Override
            public void onPostExecutionUI() {
                if (onTask != null)
                    onTask.onTaskDone(true);
                refresh();
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                if (onTask != null)
                    onTask.onTaskDone(false);
            }
        };
    }

}
