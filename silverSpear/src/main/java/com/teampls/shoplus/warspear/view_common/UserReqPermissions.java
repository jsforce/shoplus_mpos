package com.teampls.shoplus.warspear.view_common;

import com.teampls.shoplus.lib.view_base.BaseReqPermissions;

/**
 * Created by Medivh on 2017-06-28.
 */

public class UserReqPermissions extends BaseReqPermissions {
    @Override
    public Class<?> getUserClassification() {
        return UserIsRegistered.class;
    }
}
