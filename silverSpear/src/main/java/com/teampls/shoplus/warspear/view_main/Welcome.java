package com.teampls.shoplus.warspear.view_main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.teampls.shoplus.lib.awsservice.MyAWSConfigs;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemDB;
import com.teampls.shoplus.lib.database.PosSlipDB;
import com.teampls.shoplus.lib.database_global.UserDB;
import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.enums.LoginState;
import com.teampls.shoplus.lib.view_base.BaseWelcome;
import com.teampls.shoplus.warspear.R;
import com.teampls.shoplus.warspear.common.AppWatcher;
import com.teampls.shoplus.warspear.common.MyPreference;
import com.teampls.shoplus.warspear.view_common.UserConfirm;
import com.teampls.shoplus.warspear.view_common.UserIsRegistered;
import com.teampls.shoplus.warspear.view_common.UserReqPermissions;

public class Welcome extends BaseWelcome {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyAWSConfigs.isProduction();
    }

    @Override
    protected void init() {
        super.init();
        if (myDB.confirmed())
            MyPreference.init(context);
    }

    protected void onNetworkDisconnected() {
        new MyAlertDialog(context, "네트워크 연결 이상", "네트워크에 연결되어 있지 않습니다", "앱 종료", "오프라인 모드") {
            @Override
            public void yes() {
                finish();
            }

            public void no() {
                init();
                if (myDB.confirmed()) {
                    AppWatcher.setLoginState(context, LoginState.onSuccess);
                    startAppWatcher();
                    startActivity(BaseUtils.getIntent(context, getMainViewPager()));
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                } else {
                    MyUI.toastSHORT(context, String.format("아직 미가입 상태입니다. 네트워크가 정상일 때 가입을 완료해 주세요."));
                    finish();
                }
            }
        };
    }

    protected boolean manipulateDBs() {
        //ItemDB.getInstance(context).deleteDB();
        return false;
    }

    @Override
    public int getCoverImageResId() {
        return R.drawable.warspear_welcome;
    }

    @Override
    public void startAppWatcher() {
      startService(new Intent(context, AppWatcher.class).setPackage(context.getPackageName()));
    }

    @Override
    protected void onServerChange() {
        Log.w("DEBUG_JS", String.format("[Welcome.onServerChange] ...."));
        ItemDB.getInstance(context).clear();
        UserDB.getInstance(context).clear();
        PosSlipDB.getInstance(context).clear();
    }

    @Override
    public Class<?> getUserConfirm() {
        return UserConfirm.class;
    }

    @Override
    public Class<?> getUserIsRegistered() {
        return UserIsRegistered.class;
    }

    @Override
    public Class<?> getMainViewPager() {
        return MainViewPager.class;
    }

    @Override
    public Class<?> getUserReqPermissions() {
        return UserReqPermissions.class;
    }

}
