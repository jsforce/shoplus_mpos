package com.teampls.shoplus.warspear.database;

import android.content.Context;

import com.teampls.shoplus.lib.adapter.BaseSlipDBAdapter;
import com.teampls.shoplus.lib.adapter.BaseViewHolder;
import com.teampls.shoplus.lib.database.SlipDB;
import com.teampls.shoplus.lib.database_memory.MemorySlipDB;

/**
 * Created by Medivh on 2016-09-01.
 */
public class SlipDBAdapter extends BaseSlipDBAdapter {

    public SlipDBAdapter(Context context, MemorySlipDB memorySlipDB) {
        super(context, memorySlipDB);
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new SlipsViewHolder();
    }

    public class SlipsViewHolder extends ViewHolder {

        @Override
        public void update(int position) {
            super.update(position);
        }
    }

}