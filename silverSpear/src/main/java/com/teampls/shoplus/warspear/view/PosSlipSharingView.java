package com.teampls.shoplus.warspear.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.ItemOptionRecord;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_global.UserRecord;
import com.teampls.shoplus.lib.enums.DbActionType;
import com.teampls.shoplus.lib.view_base.BasePosSlipSharingView;
import com.teampls.shoplus.silverlib.view_silver.SilverSlipSharingView;
import com.teampls.shoplus.warspear.view_main.MainViewPager;

/**
 * Created by Medivh on 2017-10-19.
 */

public class PosSlipSharingView extends SilverSlipSharingView {

    public static void startActivity(Context context, SlipFullRecord slipFullRecord) {
        PosSlipSharingView.slipFullRecord = slipFullRecord;
        BasePosSlipSharingView.itemOptionRecord = new ItemOptionRecord();
        actionType = DbActionType.READ;
        ((Activity) context).startActivityForResult(new Intent(context, PosSlipSharingView.class), BaseGlobal.RequestRefresh);
    }

    public static void startActivityNewRecord(Context context, SlipFullRecord slipFullRecord) { // 생성하면서 호출시 (잔금 자동 합산때문에 구별함)
        PosSlipSharingView.slipFullRecord = slipFullRecord;
        actionType = DbActionType.INSERT;
        ((Activity) context).startActivityForResult(new Intent(context, PosSlipSharingView.class), BaseGlobal.RequestRefresh);
    }

    public void startPosSlipCreationView(SlipRecord slipRecord) {
        PosSlipCreationView.startActivity(context, slipRecord);
    }

    public void startPosSlipCreationView(UserRecord counterpart) {
        PosSlipCreationView.startActivity(context, counterpart);
    }

    public void refreshAfterCancelled() {
        if (MainViewPager.adapter != null) {
            MainViewPager.adapter.refreshFragments();
            MyUI.toastSHORT(context, String.format("취소건 반영"));
        }
    }

}
