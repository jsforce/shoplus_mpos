package com.teampls.shoplus.warspear.view_main;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.teampls.shoplus.lib.awsservice.MyServiceFailureException;
import com.teampls.shoplus.lib.common.BaseGlobal;
import com.teampls.shoplus.lib.common.BaseUtils;
import com.teampls.shoplus.lib.common.Empty;
import com.teampls.shoplus.lib.common.MyApp;
import com.teampls.shoplus.lib.common.MyAsyncTask;
import com.teampls.shoplus.lib.common.MyDevice;
import com.teampls.shoplus.lib.common.MyUI;
import com.teampls.shoplus.lib.database.SlipFullRecord;
import com.teampls.shoplus.lib.database.SlipItemRecord;
import com.teampls.shoplus.lib.database.SlipRecord;
import com.teampls.shoplus.lib.database_memory.MSlipDB;
import com.teampls.shoplus.lib.database_memory.MTransDB;
import com.teampls.shoplus.lib.dialog.BaseDialog;
import com.teampls.shoplus.lib.enums.SlipItemType;
import com.teampls.shoplus.lib.event.MyOnTask;
import com.teampls.shoplus.lib.protocol.SlipDataProtocol;
import com.teampls.shoplus.lib.view.MainProgressBar;
import com.teampls.shoplus.lib.view.MyView;
import com.teampls.shoplus.lib.view_base.BaseFragment;
import com.teampls.shoplus.lib.view_module.MyCheckBox;
import com.teampls.shoplus.lib.view_module.MyEmptyGuide;
import com.teampls.shoplus.silverlib.common.MyServiceTask;
import com.teampls.shoplus.warspear.R;
import com.teampls.shoplus.warspear.common.MyPreference;
import com.teampls.shoplus.warspear.database.SlipItemDBAdapter;
import com.teampls.shoplus.warspear.view.PosSlipSharingView;

import org.joda.time.DateTime;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2018-04-17.
 */

public class SlipItemsFrag extends BaseFragment implements AdapterView.OnItemClickListener {
    private static SlipItemsFrag instance = null;
    protected static int tabIndex = 0;
    private String title = "챙길상품";
    private final String KEY_ShowAll = "slipitemsfrag.show.all";
    private MyCheckBox cbShowAll;

    private int thisView = R.layout.base_listview;
    protected ListView listView;
    public SlipItemDBAdapter adapter;
    private MyEmptyGuide myEmptyGuide;
    protected MSlipDB slipDB;
    private TextView tvDalaran;

    public static SlipItemsFrag getInstance(int tabIndex) {
        if (instance == null)
            instance = new SlipItemsFrag();
        SlipItemsFrag.tabIndex = tabIndex;
        return instance;
    }

    public static SlipItemsFrag getInstance() {
        if (instance == null)
            instance = new SlipItemsFrag();
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slipDB = MSlipDB.getInstance(context);
        adapter = new SlipItemDBAdapter(context, slipDB);
        adapter.blockCancelled(!MyPreference.isShowCancelled(context));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(thisView, container, false);
        listView = (ListView) view.findViewById(R.id.listview_listview);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);

        setVisibility(view, View.VISIBLE, R.id.listview_headerContainer, R.id.listview_link);
        setVisibility(view, View.GONE, R.id.listview_notice);
        cbShowAll = new MyCheckBox(keyValueDB, view, R.id.listview_checkAll, KEY_ShowAll, false);
        cbShowAll.getCheckBox().setText("처리상품 보기");
        tvDalaran = findTextViewById(view, R.id.listview_link, "전체항목");

        myEmptyGuide = new MyEmptyGuide(context, view, adapter, listView,
                R.id.listview_empty_container, R.id.listview_empty_message,
                R.id.listview_empty_contactUs);

        myEmptyGuide.setMessage("챙길 상품이 없습니다\n\n" +
                "영수증 발생시 챙길물건이\n" +
                "포함되면 나타납니다.\n" +
                "ex) 미송, 주문, 샘플 등");
        myEmptyGuide.setContactUsButton("앱 문의하기", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDevice.openWeb(context, BaseGlobal.Kakao_Plus_Shoplus);
            }
        });

        MyView.setTextViewByDeviceSize(context, cbShowAll.getCheckBox(), tvDalaran);

        setOnClick(view, R.id.listview_checkAll, R.id.listview_link);

        refresh();
        isViewCreated = true;
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        SlipItemRecord slipItemRecord = adapter.getRecord(position);
        SlipRecord slipRecord = MSlipDB.getInstance(context).getRecordByKey(slipItemRecord.slipKey);
        new SlipItemClickedDialog(context, slipRecord, slipItemRecord).show();
    }

    @Override
    public void refresh() {
        if (adapter == null) return;
        new MyAsyncTask(context, "slipItems.refreshDay") {
            public void doInBackground() {
                if (cbShowAll.isChecked()) {
                    adapter.resetSlipItemTypes();
                } else {
                    adapter.setSlipItemTypes(true, SlipItemType.getPendings());
                }
                adapter.generate();
            }

            public void onPostExecutionUI() {
                adapter.notifyDataSetChanged();
                if ((MainViewPager.tabActionBar == null) || (MainViewPager.tabActionBar.getTabCount() <= tabIndex))
                    return;
                MainViewPager.tabActionBar.getTabAt(tabIndex).setText(String.format("%s (%s)", title, adapter.getCount()));
                myEmptyGuide.refresh();
            }
        };
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public int getTabIndex() {
        return tabIndex;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.listview_checkAll:
                refresh();
                break;
            case R.id.listview_link:
                if (MyApp.has(context, MyApp.Dalaran) == false)
                    MyUI.toastSHORT(context, String.format("챙길물건 관리는 미송/잔금앱에 합니다"));
                MyApp.Dalaran.open(context);
                break;
        }
    }

    public void downloadSlips(final MyOnTask onTask) {
        final String taskId = "download.slips";
        final MainProgressBar mainProgressBar = MainViewPager.mainProgressBar;
        mainProgressBar.start(taskId, "챙길물건 다운로드 중", 2);

        new MyServiceTask(context, "downloadSlips") {

            @Override
            public void doInBackground() throws MyServiceFailureException {
                List<SlipDataProtocol> protocols = service.getPeriodSpecificSlips(userSettingData.getUserShopInfo(),
                        BaseUtils.toDay(DateTime.now()).minusDays(7), DateTime.now(), userSettingData.isBuyer());
                mainProgressBar.next(taskId);
                slipDB.set(protocols, true);

                if (userSettingData.isBuyer()) {
                    Set<String> phoneNumbersNotCustomer = new HashSet<>();
                    for (SlipDataProtocol protocol : protocols) {
                        SlipFullRecord slipFullRecord = new SlipFullRecord(protocol);
                        if (userDB.has(slipFullRecord.record.getCounterpart(context)) == false)
                            phoneNumbersNotCustomer.add(slipFullRecord.record.getCounterpart(context));
                    }
                    if (phoneNumbersNotCustomer.isEmpty() == false) {
                        userDB.update(workingShopService.searchContactInfo(userSettingData.getUserShopInfo(), phoneNumbersNotCustomer));
                    }
                }

                mainProgressBar.stop(taskId);
            }

            @Override
            public void onPostExecutionUI() {
                refresh();
                if (onTask != null)
                    onTask.onTaskDone(true);
            }

            @Override
            public void catchException(MyServiceFailureException e) {
                super.catchException(e);
                if (onTask != null)
                    onTask.onTaskDone(false);
            }
        };
    }

    class SlipItemClickedDialog extends BaseDialog {
        private TextView tvName, tvCreatedDate, tvUpdatedDate;
        private ImageView ivItemImage;
        private SlipRecord slipRecord = new SlipRecord();

        public SlipItemClickedDialog(Context context, SlipRecord slipRecord, SlipItemRecord slipItemRecord) {
            super(context);
            this.slipRecord = slipRecord;
            ivItemImage = (ImageView) findViewById(R.id.dialog_slip_item_clicked_imageView);
            tvName = (TextView) findViewById(R.id.dialog_slip_item_clicked_name);
            tvName.setText(slipItemRecord.toNameOptionQuantityString(" "));
            tvCreatedDate = (TextView) findViewById(R.id.dialog_slip_item_clicked_created);
            tvUpdatedDate = (TextView) findViewById(R.id.dialog_slip_item_clicked_updated);
            tvCreatedDate.setText(String.format("생성 : %s", slipRecord.createdDateTime.toString(BaseUtils.MDHm_Format)));
            tvUpdatedDate.setText(String.format("수정 : %s", slipItemRecord.updatedDateStr));
            if (slipItemRecord.updatedDateStr.isEmpty()) {
                tvUpdatedDate.setVisibility(View.GONE);
            }

            setOnClick(R.id.dialog_slip_item_clicked_close,
                    R.id.dialog_slip_item_clicked_change, R.id.dialog_slip_item_clicked_fullView,
                    R.id.dialog_slip_item_clicked_dismiss);

            imageLoader.retrieveThumbAsync(slipItemRecord.getSlipKey().ownerPhoneNumber, slipItemRecord.itemId, new MyOnTask<Bitmap>() {
                @Override
                public void onTaskDone(Bitmap bitmap) {
                    if (Empty.isEmpty(bitmap)) {
                        setVisibility(View.GONE, R.id.dialog_slip_item_clicked_imageView);
                    } else {
                        ivItemImage.setImageBitmap(bitmap);
                    }
                }
            });
        }

        @Override
        public int getThisView() {
            return R.layout.dialog_slip_item_clicked;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.dialog_slip_item_clicked_change:
                    if (MyApp.has(context, MyApp.Dalaran) == false)
                        MyUI.toastSHORT(context, String.format("샵플 미송/잔금앱에서 관리할 수 있습니다"));
                    MyApp.Dalaran.open(context);
                    break;
                case R.id.dialog_slip_item_clicked_fullView: // 원본 영수증
                    if (MTransDB.getInstance(context).getInstance(context).has(slipRecord.getSlipKey().toSID())) {
                        PosSlipSharingView.startActivity(context, MTransDB.getInstance(context).getFullRecord(slipRecord.getSlipKey().toSID()));
                    } else {
                        MyUI.toastSHORT(context, String.format("원본 영수증을 찾지 못했습니다"));
                        PosSlipSharingView.startActivity(context, MSlipDB.getInstance(context).getFullRecord(slipRecord.getSlipKey().toSID()));
                    }
                    break;

                case R.id.dialog_slip_item_clicked_close:
                case R.id.dialog_slip_item_clicked_dismiss:
                    dismiss();
                    break;
            }
        }
    }

}
