package com.teampls.shoplus.warspear.view_common;

import android.content.Intent;
import android.os.Bundle;

import com.teampls.shoplus.lib.dialog.MyAlertDialog;
import com.teampls.shoplus.lib.view_base.BaseConfirm;
import com.teampls.shoplus.warspear.view_main.Welcome;

/**
 * Created by Medivh on 2016-08-31.
 */
public class UserConfirm extends BaseConfirm {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onConfirm() {
        startActivity(new Intent(context, Welcome.class));
        finish();
    }

    public void onExistingUserError() {
        startActivity(new Intent(context, Welcome.class), null);
        finishAffinity();
    }

    @Override
    public void onBackPressed() {
        new MyAlertDialog(context, "입력정보 삭제", "입력한 정보를 삭제하고 앱을 종료하시겠습니까?") {
            @Override
            public void yes() {
                myDB.logOut();
                finish();
            }

            @Override
            public void no() {
                finish();
            }
        };
    }
}
